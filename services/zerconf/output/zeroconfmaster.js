
var os = require("os");
var dgram = require("dgram");
var socket = dgram.createSocket({type: 'udp4', reuseAddr: true, toString: function () { return 'udp4' }});

var MULTICAST_HOST = "224.0.0.236";
var BROADCAST_HOST = "255.255.255.255";
var ALL_PORT = 60540;
var MULTICAST_TTL = 1; // Local network

module.exports = function(options){

	var instanceId = guid();

	var exports = {};
	var serviceInfos = {};
	var events = {};

	var options = options || {};

	var broadcast = !!options.broadcast;

	var multicastHost = options.host || MULTICAST_HOST;
	var port = options.port || ALL_PORT;
	var ttl = options.ttl || MULTICAST_TTL;
	var sendHost = (broadcast ? BROADCAST_HOST : multicastHost);

	// Services is a map (service.host+":"+service.port+":"+service.name) => Object serviceInfo
	// where serviceInfo is an object like
	// { isOurService : Boolean, service: Object }

	// =====
	// Set up UDP Broadcast/Multicast connection
	// =====

	socket.bind(port);
	socket.on('listening', function() {
		socket.setMulticastLoopback(true);
		socket.setMulticastTTL(ttl);
		socket.addMembership(multicastHost); // Tell the OS to listen for messages on the specified host and treat them as if they were meant for this host
		if(broadcast) {
			socket.setBroadcast(true);
		}
		queryForServices();
	});
	socket.on('message', parseMessage);

	// =====
	// Function to parse incoming messages
	// =====

	function parseMessage(message, rinfo) {
		try {
			var messageObject = JSON.parse(message);
			var eventType = messageObject.eventType;
			var fromDiontId = messageObject.fromDiontInstance;
			if (fromDiontId == instanceId) {
				return;
			}
			if (eventType == "query") {
				var serviceInfosToAnnounce = [];
				for(var index in serviceInfos) {
					serviceInfosToAnnounce.push(serviceInfos[index]);
				}
				sendAnnouncement(serviceInfosToAnnounce);
			} else {
				var receivedServiceInfos = messageObject.serviceInfos;
				for(var serviceInfoIndex in receivedServiceInfos) {
					var serviceInfo = receivedServiceInfos[serviceInfoIndex];
					if(!serviceInfo.service) {
						continue;
					}
					var service = serviceInfo.service;
					if (!service.host || !service.port || !service.name) {
						continue;
					}
					if (eventType == "announce") {
						var id = service.host + ":" + service.port + ":" + service.name;
						if(!serviceInfos[id]) {
							var serviceInfo = serviceInfos[id] = {
								isOurService: false,
								service: service
							}
							if (events["serviceAnnounced"]) {
								for(var callbackId in events["serviceAnnounced"]) {
									var callback = events["serviceAnnounced"][callbackId];
									callback(serviceInfo);
								}
							}
						}
					} else if (eventType == "renounce") {
						var id = service.host + ":" + service.port + ":" + service.name;
						if(serviceInfos[id]) {
							var serviceInfo = serviceInfos[id];
							delete serviceInfos[id];
							if (events["serviceRenounced"]) {
								for(var callbackId in events["serviceRenounced"]) {
									var callback = events["serviceRenounced"][callbackId];
									callback(serviceInfo);
								}
							}
						}
					}
				}
			}
		} catch(e) {
			// ignore...
		}
	};

	// =====
	// Exported functions
	// =====

	exports.announceService = function(service) {
		if (!service.host) {
			service.host = getNetworkIPAddress();
		}
		if (!service.host || !service.port || !service.name) {
			return false;
		}
		var id = service.host + ":" + service.port + ":" + service.name;
		if(!serviceInfos[id]) {
			var serviceInfo = serviceInfos[id] = {
				isOurService: true,
				service: service
			}
			sendAnnouncement(serviceInfo);
		}
		return id;
	}

	exports.renounceService = function(service) {
		var id;
		if (typeof service == 'string') {
			id = service;
		} else {
			if (!service.host || !service.port || !service.name) {
				return false;
			}
			id = service.host + ":" + service.port + ":" + service.name;
		}
		if(serviceInfos[id] && serviceInfos[id].isOurService) {
			sendRenouncement(serviceInfos[id]);
			delete serviceInfos[id];
		}
	}

	exports.repeatAnnouncements = function() {
		for(var id in serviceInfos) {
			var serviceInfo = serviceInfos[id];
			sendAnnouncement(serviceInfo);
		}
	}

	exports.queryForServices = function() {
		queryForServices();
	}

	exports.on = function(eventName, callback) {
		if(!events[eventName]) {
			events[eventName] = {};
		}
		var callbackId = guid();
		events[eventName][callbackId] = callback;
		return callbackId;
	}

	exports.off = function(eventName, callbackId) {
		if(!events[eventName]) {
			return false;
		}
		delete events[eventName][callbackId];
		return true;
	}

	exports.getServiceInfos = function() {
		return JSON.parse(JSON.stringify(serviceInfos));
	}

	// =====
	// Helper functions
	// =====

	function sendAnnouncement(serviceInfo) {
		var serviceInfosToAnnounce = [];
		if (serviceInfo instanceof Array) {
			serviceInfosToAnnounce = serviceInfo;
		} else {
			serviceInfosToAnnounce = [serviceInfo];
		}
		var messageObject = {
			eventType: "announce",
			fromDiontInstance: instanceId,
			serviceInfos: serviceInfosToAnnounce
		}
		var message = JSON.stringify(messageObject);
		var buffer = new Buffer(message);
		socket.send(buffer, 0, buffer.length, port, sendHost);
	}

	function sendRenouncement(serviceInfo) {
		var serviceInfosToRenounce = [];
		if (serviceInfo instanceof Array) {
			serviceInfosToRenounce = serviceInfo;
		} else {
			serviceInfosToRenounce = [serviceInfo];
		}
		var messageObject = {
			eventType: "renounce",
			fromDiontInstance: instanceId,
			serviceInfos: serviceInfosToRenounce
		}
		var message = JSON.stringify(messageObject);
		var buffer = new Buffer(message);
		socket.send(buffer, 0, buffer.length, port, sendHost);
	}

	function queryForServices() {
		var messageObject = {
			eventType: "query",
			fromDiontInstance: instanceId
		}
		var message = JSON.stringify(messageObject);
		var buffer = new Buffer(message);
		socket.send(buffer, 0, buffer.length, port, sendHost);
	}

	function guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}

	function getNetworkIPAddress() {
		var ifaces = os.networkInterfaces();
		var addresses = [];
		var localAddress;
		for (var dev in ifaces) {
			ifaces[dev].forEach(function(details){
				if (details.family=='IPv4' && details.internal === false) {
					addresses.push(details.address);
					if (details.address.indexOf('192.168.') === 0) {
						localAddress = details.address;
					}
				}
			});
		}
		// Return a 192.168.x.x address if possible, otherwise return the first address found
		if (localAddress) {
			return localAddress;
		}
		return addresses[0];
	}

	// =====
	// Export
	// =====

	return exports;
}
/*

    TODO : ( see also TODO in code)

        YModem download : remove CPMEOF's on files w/o filenames/sizes

    HOTSPOTATTR  (HOTSPOTHOVERATTR / HOTSPOTCLICKATTR)

        see CELLATTRS


    PAGEATTR

        bbbbbbbb BBBBBBBB

        b : border color
        B : background color

    ROWATTR - numbers stored in conRowAttr[num]

        00000000 00101100 00000000 00000000 - default

        00000000 00000000 00000000 00000000  - bits
        ------DD mww---bb ssssssss ffffffff
        00000000 00101100 00000000 00000000
           00       2C       00       00

        f : First Color (0-255)
        s : Second Color (0-255)
        b : Background Pattern
            00 - none
            01 - solid (first color)
            10 - horz grad (first -> second color)
            11 - vert grad (first -> second color)
        w : width scale
            00  - 50%
            01  - 100%
            10  - 150%
            11  - 200%
        m : marquee (0-1)
        D : row display row type
            00 - normal
            01 - concealed
            10 - top half of double height
            11 - bottom half of double height
        - : unused

    CELLATTRS - numbers stored in conCellAttr[row][col]

        00000000 00000000 00000000 00000000 - bits
        ZZZZdsDD fKktuibr BBBBBBBB FFFFFFFF

        00000000 00001000 00000010 11111111

        F : Foreground Color (0-255) using aixterm palette
        B : Background Color (0-255)  -''-
        r : reversed (0-1)
        b : bold (0-1)
        i : italics (0-1)
        u : underline (0-1)
        t : strikethrough (0-1)
        k : blink slow (0-1)
        K : blink fast (0-1)
        f : faint (0-1)
        D : display (0=normal,1=concealed,2=tophalf,3=bottomhalf)
        s : shadow (0-1)
        d : doublestrike (0-1)
        Z : font number 0-15 (10=teletext, 11=w/block, 12=w/separated block).

    CRSRATTRS

        00000000 00000000 00000000 00000000  - bits
        -------- -------- -----ozz cccccccc

        c : color (0-255)
        z : size
            00 : none   0%
            01 : thin  10%
            10 : thick 25%
            11 : full   100%
        o : orientation
            0 : horizontal
            1 : vertical
        - : unused


    ESC / CSI CODES - see vtx.txt

*/
"use strict";

vtx: {

// globals
const
    version = '0.93c beta',

    // ansi color lookup table (alteration. color 0=transparent, use 16 for true black`)
    ansiColors = [
        // VGA 0-15 - transparent will switch to #000000 when appropriate
        'transparent',  '#AA0000',      '#00AA00',      '#AA5500',
        '#0000AA',      '#AA00AA',      '#00AAAA',      '#AAAAAA',
        '#555555',      '#FF5555',      '#55FF55',      '#FFFF55',
        '#5555FF',      '#FF55FF',      '#55FFFF',      '#FFFFFF',
        // EXTENDED 16-231
        '#000000', '#00005F', '#000087', '#0000AF', '#0000D7', '#0000FF',
        '#005F00', '#005F5F', '#005F87', '#005FAF', '#005FD7', '#005FFF',
        '#008700', '#00875F', '#008787', '#0087AF', '#0087D7', '#0087FF',
        '#00AF00', '#00AF5F', '#00AF87', '#00AFAF', '#00AFD7', '#00AFFF',
        '#00D700', '#00D75F', '#00D787', '#00D7AF', '#00D7D7', '#00D7FF',
        '#00FF00', '#00FF5F', '#00FF87', '#00FFAF', '#00FFD7', '#00FFFF',
        '#5F0000', '#5F005F', '#5F0087', '#5F00AF', '#5F00D7', '#5F00FF',
        '#5F5F00', '#5F5F5F', '#5F5F87', '#5F5FAF', '#5F5FD7', '#5F5FFF',
        '#5F8700', '#5F875F', '#5F8787', '#5F87AF', '#5F87D7', '#5F87FF',
        '#5FAF00', '#5FAF5F', '#5FAF87', '#5FAFAF', '#5FAFD7', '#5FAFFF',
        '#5FD700', '#5FD75F', '#5FD787', '#5FD7AF', '#5FD7D7', '#5FD7FF',
        '#5FFF00', '#5FFF5F', '#5FFF87', '#5FFFAF', '#5FFFD7', '#5FFFFF',
        '#870000', '#87005F', '#870087', '#8700AF', '#8700D7', '#8700FF',
        '#875F00', '#875F5F', '#875F87', '#875FAF', '#875FD7', '#875FFF',
        '#878700', '#87875F', '#878787', '#8787AF', '#8787D7', '#8787FF',
        '#87AF00', '#87AF5F', '#87AF87', '#87AFAF', '#87AFD7', '#87AFFF',
        '#87D700', '#87D75F', '#87D787', '#87D7AF', '#87D7D7', '#87D7FF',
        '#87FF00', '#87FF5F', '#87FF87', '#87FFAF', '#87FFD7', '#87FFFF',
        '#AF0000', '#AF005F', '#AF0087', '#AF00AF', '#AF00D7', '#AF00FF',
        '#AF5F00', '#AF5F5F', '#AF5F87', '#AF5FAF', '#AF5FD7', '#AF5FFF',
        '#AF8700', '#AF875F', '#AF8787', '#AF87AF', '#AF87D7', '#AF87FF',
        '#AFAF00', '#AFAF5F', '#AFAF87', '#AFAFAF', '#AFAFD7', '#AFAFFF',
        '#AFD700', '#AFD75F', '#AFD787', '#AFD7AF', '#AFD7D7', '#AFD7FF',
        '#AFFF00', '#AFFF5F', '#AFFF87', '#AFFFAF', '#AFFFD7', '#AFFFFF',
        '#D70000', '#D7005F', '#D70087', '#D700AF', '#D700D7', '#D700FF',
        '#D75F00', '#D75F5F', '#D75F87', '#D75FAF', '#D75FD7', '#D75FFF',
        '#D78700', '#D7875F', '#D78787', '#D787AF', '#D787D7', '#D787FF',
        '#D7AF00', '#D7AF5F', '#D7AF87', '#D7AFAF', '#D7AFD7', '#D7AFFF',
        '#D7D700', '#D7D75F', '#D7D787', '#D7D7AF', '#D7D7D7', '#D7D7FF',
        '#D7FF00', '#D7FF5F', '#D7FF87', '#D7FFAF', '#D7FFD7', '#D7FFFF',
        '#FF0000', '#FF005F', '#FF0087', '#FF00AF', '#FF00D7', '#FF00FF',
        '#FF5F00', '#FF5F5F', '#FF5F87', '#FF5FAF', '#FF5FD7', '#FF5FFF',
        '#FF8700', '#FF875F', '#FF8787', '#FF87AF', '#FF87D7', '#FF87FF',
        '#FFAF00', '#FFAF5F', '#FFAF87', '#FFAFAF', '#FFAFD7', '#FFAFFF',
        '#FFD700', '#FFD75F', '#FFD787', '#FFD7AF', '#FFD7D7', '#FFD7FF',
        '#FFFF00', '#FFFF5F', '#FFFF87', '#FFFFAF', '#FFFFD7', '#FFFFFF',
        // GRAYS 232-255
        '#080808', '#121212', '#1C1C1C', '#262626', '#303030', '#3A3A3A',
        '#444444', '#4E4E4E', '#585858', '#626262', '#6C6C6C', '#767676',
        '#808080', '#8A8A8A', '#949494', '#9E9E9E', '#A8A8A8', '#B2B2B2',
        '#BCBCBC', '#C6C6C6', '#D0D0D0', '#DADADA', '#E4E4E4', '#EEEEEE'
    ],

    // commodore colors.
    vic20Colors = [ // vic-20 in 22 column mode
        '#000000', '#FFFFFF', '#782922', '#87D6DD', '#AA5FB6', '#55A049',
        '#40318D', '#BFCE72', '#AA7449', '#EAB489', '#B86962', '#C7FFFF',
        '#EA9FF6', '#94E089', '#8071CC', '#FFFFB2', '#000000' // fake black
    ],
    c64Colors = [   // C64/C128 in 40 column mode
        '#000000', '#FFFFFF', '#68372B', '#70A4B2', '#6F3D86', '#588D43',
        '#352879', '#B8C76F', '#6F4F25', '#433900', '#9A6759', '#444444',
        '#6C6C6C', '#9AD284', '#6C5EB5', '#959595', '#000000' // fake black
    ],
    c128Colors = [  // C128 colors in 80 column mode
        '#000000', '#303030', '#EA311B', '#FC601C', '#36C137', '#77EC7C',
        '#1C49D9', '#4487EF', '#BBC238', '#E9F491', '#D974DA', '#EECFED',
        '#68C8C2', '#B2F0EC', '#BECEBC', '#FFFFFF', '#000000' // fake black
    ],

    // atari tty colors.
    atariColors = [
        '#0141A3', '#64ACFF'
    ],

    // convert teletext colors to ansi
    ttxToAnsiColor = [ 16, 196, 46, 226, 21, 201, 51, 15 ],

    hex =   '0123456789ABCDEF',
    b64 =   'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

    // attributes
    A_CELL_FGCOLOR_MASK     = 0x000000FF,
    A_CELL_BGCOLOR_MASK     = 0x0000FF00,
    A_CELL_REVERSE          = 0x00010000,
    A_CELL_BOLD             = 0x00020000,
    A_CELL_ITALICS          = 0x00040000,
    A_CELL_UNDERLINE        = 0x00080000,
    A_CELL_STRIKETHROUGH    = 0x00100000,
    A_CELL_BLINKSLOW        = 0x00200000,
    A_CELL_BLINKFAST        = 0x00400000,
    A_CELL_FAINT            = 0x00800000,
    A_CELL_DISPLAY_MASK     = 0x03000000,
    A_CELL_DISPLAY_NORMAL   = 0x00000000,
    A_CELL_DISPLAY_CONCEAL  = 0x01000000,
    A_CELL_DISPLAY_TOP      = 0x02000000,
    A_CELL_DISPLAY_BOTTOM   = 0x03000000,
    A_CELL_SHADOW           = 0x04000000,
    A_CELL_DOUBLESTRIKE          = 0x08000000,
    A_CELL_FONT_MASK        = 0xF0000000,

    A_ROW_COLOR1_MASK       = 0x000000FF, // 0-255
    A_ROW_COLOR2_MASK       = 0x0000FF00, // 0-255
    A_ROW_PATTERN_MASK      = 0x00030000, // normal,solid,horz,vert
    A_ROW_PATTERN_NONE      = 0x00000000, // no background
    A_ROW_PATTERN_SOLID     = 0x00010000, // solid color1
    A_ROW_PATTERN_HORZ      = 0x00020000, // horizontal gradient color1 to color2
    A_ROW_PATTERN_VERT      = 0x00030000, // vertical gradient color1 to color2
    A_ROW_WIDTH_MASK        = 0x00600000, // 50,100,150,200
    A_ROW_WIDTH_50          = 0x00000000,
    A_ROW_WIDTH_100         = 0x00200000,
    A_ROW_WIDTH_150         = 0x00400000,
    A_ROW_WIDTH_200         = 0x00600000,
    A_ROW_MARQUEE           = 0x00800000,
    A_ROW_DISPLAY_MASK      = 0x03000000, // normal/conceal/top/bottom
    A_ROW_DISPLAY_NORMAL    = 0x00000000,
    A_ROW_DISPLAY_CONCEAL   = 0x01000000,
    A_ROW_DISPLAY_TOP       = 0x02000000,
    A_ROW_DISPLAY_BOTTOM    = 0x03000000,

    A_CRSR_COLOR_MASK       = 0x000000FF, // 0-255
    A_CRSR_STYLE_MASK       = 0x00000300, // none,thin,thick,full
    A_CRSR_STYLE_NONE       = 0x00000000,
    A_CRSR_STYLE_THIN       = 0x00000100,
    A_CRSR_STYLE_THICK      = 0x00000200,
    A_CRSR_STYLE_FULL       = 0x00000300,
    A_CRSR_ORIENTATION      = 0x00000400,   // 0 = horz, 1 = vert

    // special key commands
    DO_CAPLK =          -1,
    DO_NUMLK =          -2,
    DO_SCRLK =          -3,
    DO_SELECTALL =      -5,     // select all text on screen.

    // terminal states
    TS_OFFLINE =        -1, // not connected
    TS_NORMAL =         0,  // normal terminal mode. no xfers.
    TS_YMR_START =      1,  // ymodem download started. sending G's.
    TS_YMR_GETPACKET =  2,  // ymodem download packet
    TS_YMS_START =      3,  // ymodem send header of file.
    TS_YMS_PUTPACKET =  4,  // ymodem send packet
    TS_YMS_PUTWAIT =    5,  // ymodem wait for C on send

    // bitspersecond / 100 rates for speed emulation.
    bauds =     [ 0,3,6,12,24,48,96,192,384,576,768,1152 ],

    // ASCII C0 Codes
    _NUL     = 0x00,
    _SOH     = 0x01,
    _STX     = 0x02,
    _ETX     = 0x03,
    _EOT     = 0x04,
    _ENQ     = 0x05,
    _ACK     = 0x06,
    _BEL     = 0x07,
    _BS      = 0x08,
    _HT      = 0x09,
    _LF      = 0x0A,
    _VT      = 0x0B,
    _FF      = 0x0C,
    _CR      = 0x0D,
    _SO      = 0x0E,
    _SI      = 0x0F,
    _DLE     = 0x10,
    _DC1     = 0x11,  //   same
    _XON     = 0x11,  // values
    _DC2     = 0x12,
    _DC3     = 0x13,  //   same
    _XOFF    = 0x13,  // values
    _DC4     = 0x14,
    _NAK     = 0x15,
    _SYN     = 0x16,
    _ETB     = 0x17,
    _CAN     = 0x18,
    _EM      = 0x19,
    _SUB     = 0x1A,  //   same
    _CPMEOF  = 0x1A,  // values
    _ESC     = 0x1B,
    _FS      = 0x1C,
    _GS      = 0x1D,
    _RS      = 0x1E,
    _US      = 0x1F,
    _SPACE   = 0x20,
    _C       = 0x43,
    _G       = 0x47,
    _DEL     = 0x7F,
    _BLANK   = 0x0020,  // characters without a glyph (null etc).
    _SHY     = 0x2010,  // similar character to replace soft-hyphen

    // special char codes and sequences
    NUL =       '\x00',
    ESC =       '\x1B',
    CSI =       '\x1B[',
    CR =        '\x0D',
    LF =        '\x0A',
    CRLF =      '\x0D\x0A',

    // tables for converting byte to UTF16 (unicode)
    codePageAKAs = {
        CP790:      'CP667',
        CP991:      'CP667',
        CP916:      'CP862',
        CP1119:     'CP772',
        CP1118:     'CP774',
        CP900:      'CP866',
        CP895:      'CP866',
        CP65001:    'UTF8',
        CP819:      'ISO8859_1',
        CP28593:    'ISO8859_3',
        VIC20:      'RAW',
        C64:        'RAW',
        C128:       'RAW',
        ATASCII:    'RAW'
    },

    // codepage tables.
    // size     desc
    // 255      glyphs for all points (for doorway mode)
    // 128      80-FF (00-7F from ASCII)
    // 96       A0-FF (00-7F from ASCII, 80-9F from CP437)
    codePageData = {
        ASCII: new Uint16Array([
            _BLANK, 0x263A, 0x263B, 0x2665, 0x2666, 0x2663, 0x2660, 0x2022,
            0x25D8, 0x25CB, 0x25D9, 0x2642, 0x2640, 0x266A, 0x266B, 0x263C,
            0x25BA, 0x25C4, 0x2195, 0x203C, 0x00B6, 0x00A7, 0x25AC, 0x21A8,
            0x2191, 0x2193, 0x2192, 0x2190, 0x221F, 0x2194, 0x25B2, 0x25BC,
            0x0020, 0x0021, 0x0022, 0x0023, 0x0024, 0x0025, 0x0026, 0x0027,
            0x0028, 0x0029, 0x002A, 0x002B, 0x002C, 0x002D, 0x002E, 0x002F,
            0x0030, 0x0031, 0x0032, 0x0033, 0x0034, 0x0035, 0x0036, 0x0037,
            0x0038, 0x0039, 0x003A, 0x003B, 0x003C, 0x003D, 0x003E, 0x003F,
            0x0040, 0x0041, 0x0042, 0x0043, 0x0044, 0x0045, 0x0046, 0x0047,
            0x0048, 0x0049, 0x004A, 0x004B, 0x004C, 0x004D, 0x004E, 0x004F,
            0x0050, 0x0051, 0x0052, 0x0053, 0x0054, 0x0055, 0x0056, 0x0057,
            0x0058, 0x0059, 0x005A, 0x005B, 0x005C, 0x005D, 0x005E, 0x005F,
            0x0060, 0x0061, 0x0062, 0x0063, 0x0064, 0x0065, 0x0066, 0x0067,
            0x0068, 0x0069, 0x006A, 0x006B, 0x006C, 0x006D, 0x006E, 0x006F,
            0x0070, 0x0071, 0x0072, 0x0073, 0x0074, 0x0075, 0x0076, 0x0077,
            0x0078, 0x0079, 0x007A, 0x007B, 0x007C, 0x007D, 0x007E, 0x2302]),
        CP437: new Uint16Array([    // CP437
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x00FF, 0x00D6, 0x00DC, 0x00A2, 0x00A3, 0x00A5, 0x20A7, 0x0192,
            0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x00F1, 0x00D1, 0x00AA, 0x00BA,
            0x00BF, 0x2310, 0x00AC, 0x00BD, 0x00BC, 0x00A1, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
            0x256A, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x03B1, 0x00DF, 0x0393, 0x03C0, 0x03A3, 0x03C3, 0x00B5, 0x03C4,
            0x03A6, 0x0398, 0x03A9, 0x03B4, 0x221E, 0x03C6, 0x03B5, 0x2229,
            0x2261, 0x00B1, 0x2265, 0x2264, 0x2320, 0x2321, 0x00F7, 0x2248,
            0x00B0, 0x2219, 0x00B7, 0x221A, 0x207F, 0x00B2, 0x25A0, _BLANK]),
        CP667: new Uint16Array([    // CP667, CP790, CP991
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x0105, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x0107, 0x00C4, 0x0104,
            0x0118, 0x0119, 0x0142, 0x00F4, 0x00F6, 0x0106, 0x00FB, 0x00F9,
            0x015A, 0x00D6, 0x00DC, 0x00A2, 0x0141, 0x00A5, 0x015B, 0x0192,
            0x0179, 0x017B, 0x00F3, 0x00D3, 0x0144, 0x0143, 0x017A, 0x017C,
            0x00BF, 0x2310, 0x00AC, 0x00BD, 0x00BC, 0x00A1, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
            0x256A, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x03B1, 0x00DF, 0x0393, 0x03C0, 0x03A3, 0x03C3, 0x00B5, 0x03C4,
            0x03A6, 0x0398, 0x03A9, 0x03B4, 0x221E, 0x03C6, 0x03B5, 0x2229,
            0x2261, 0x00B1, 0x2265, 0x2264, 0x2320, 0x2321, 0x00F7, 0x2248,
            0x00B0, 0x2219, 0x00B7, 0x221A, 0x207F, 0x00B2, 0x25A0, 0x00A0]),
        CP668: new Uint16Array([    // CP668
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x0107, 0x00E7,
            0x0142, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x0179, 0x00C4, 0x0106,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x015A,
            0x015B, 0x00D6, 0x00DC, 0x00A2, 0x00A3, 0x0141, 0x00D3, 0x0192,
            0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x0104, 0x0105, 0x017B, 0x017C,
            0x0118, 0x0119, 0x00AC, 0x017A, 0x0143, 0x0144, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
            0x256A, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x03B1, 0x00DF, 0x0393, 0x03C0, 0x03A3, 0x03C3, 0x00B5, 0x03C4,
            0x03A6, 0x0398, 0x03A9, 0x03B4, 0x221E, 0x03C6, 0x03B5, 0x2229,
            0x2261, 0x00B1, 0x2265, 0x2264, 0x2320, 0x2321, 0x00F7, 0x2248,
            0x00B0, 0x2219, 0x00B7, 0x221A, 0x207F, 0x00B2, 0x25A0, 0x00A0]),
        CP737: new Uint16Array([    // CP737
            0x0391, 0x0392, 0x0393, 0x0394, 0x0395, 0x0396, 0x0397, 0x0398,
            0x0399, 0x039A, 0x039B, 0x039C, 0x039D, 0x039E, 0x039F, 0x03A0,
            0x03A1, 0x03A3, 0x03A4, 0x03A5, 0x03A6, 0x03A7, 0x03A8, 0x03A9,
            0x03B1, 0x03B2, 0x03B3, 0x03B4, 0x03B5, 0x03B6, 0x03B7, 0x03B8,
            0x03B9, 0x03BA, 0x03BB, 0x03BC, 0x03BD, 0x03BE, 0x03BF, 0x03C0,
            0x03C1, 0x03C3, 0x03C2, 0x03C4, 0x03C5, 0x03C6, 0x03C7, 0x03C8,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
            0x256A, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x03C9, 0x03AC, 0x03AD, 0x03AE, 0x03CA, 0x03AF, 0x03CC, 0x03CD,
            0x03CB, 0x03CE, 0x0386, 0x0388, 0x0389, 0x038A, 0x038C, 0x038E,
            0x038F, 0x00B1, 0x2265, 0x2264, 0x03AA, 0x03AB, 0x00F7, 0x2248,
            0x00B0, 0x2219, 0x00B7, 0x221A, 0x207F, 0x00B2, 0x25A0, 0x00A0]),
        CP770: new Uint16Array([    // CP770
            0x010C, 0x00FC, 0x0117, 0x0101, 0x00E4, 0x0105, 0x013C, 0x010D,
            0x0113, 0x0112, 0x0119, 0x0118, 0x012B, 0x012F, 0x00C4, 0x0104,
            0x0116, 0x017E, 0x017D, 0x00F5, 0x00F6, 0x00D5, 0x016B, 0x0173,
            0x0123, 0x00D6, 0x00DC, 0x00A2, 0x013B, 0x201E, 0x0161, 0x0160,
            0x0100, 0x012A, 0x0137, 0x0136, 0x0146, 0x0145, 0x016A, 0x0172,
            0x0122, 0x2310, 0x00AC, 0x00BD, 0x00BC, 0x012E, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
            0x256A, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x03B1, 0x00DF, 0x0393, 0x03C0, 0x03A3, 0x03C3, 0x00B5, 0x03C4,
            0x03A6, 0x0398, 0x03A9, 0x03B4, 0x221E, 0x03C6, 0x03B5, 0x2229,
            0x2261, 0x00B1, 0x2265, 0x2264, 0x2320, 0x2321, 0x00F7, 0x2248,
            0x00B0, 0x2219, 0x00B7, 0x221A, 0x207F, 0x00B2, 0x25A0, 0x00A0]),
        CP771: new Uint16Array([    // CP771
            0x0410, 0x0411, 0x0412, 0x0413, 0x0414, 0x0415, 0x0416, 0x0417,
            0x0418, 0x0419, 0x041A, 0x041B, 0x041C, 0x041D, 0x041E, 0x041F,
            0x0420, 0x0421, 0x0422, 0x0423, 0x0424, 0x0425, 0x0426, 0x0427,
            0x0428, 0x0429, 0x042A, 0x042B, 0x042C, 0x042D, 0x042E, 0x042F,
            0x0430, 0x0431, 0x0432, 0x0433, 0x0434, 0x0435, 0x0436, 0x0437,
            0x0438, 0x0439, 0x043A, 0x043B, 0x043C, 0x043D, 0x043E, 0x043F,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
            0x256A, 0x2518, 0x250C, 0x2588, 0x0104, 0x0105, 0x010C, 0x010D,
            0x0440, 0x0441, 0x0442, 0x0443, 0x0444, 0x0445, 0x0446, 0x0447,
            0x0448, 0x0449, 0x044A, 0x044B, 0x044C, 0x044D, 0x044E, 0x044F,
            0x0118, 0x0119, 0x0116, 0x0117, 0x012E, 0x012F, 0x0160, 0x0161,
            0x0172, 0x0173, 0x016A, 0x016B, 0x017D, 0x017E, 0x25A0, 0x00A0]),
        CP772: new Uint16Array([ // CP772, CP1119
            0x0410, 0x0411, 0x0412, 0x0413, 0x0414, 0x0415, 0x0416, 0x0417,
            0x0418, 0x0419, 0x041A, 0x041B, 0x041C, 0x041D, 0x041E, 0x041F,
            0x0420, 0x0421, 0x0422, 0x0423, 0x0424, 0x0425, 0x0426, 0x0427,
            0x0428, 0x0429, 0x042A, 0x042B, 0x042C, 0x042D, 0x042E, 0x042F,
            0x0430, 0x0431, 0x0432, 0x0433, 0x0434, 0x0435, 0x0436, 0x0437,
            0x0438, 0x0439, 0x043A, 0x043B, 0x043C, 0x043D, 0x043E, 0x043F,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x0104, 0x010C, 0x0118,
            0x0116, 0x2563, 0x2551, 0x2557, 0x255D, 0x012E, 0x0160, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x0172, 0x016A,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x017D,
            0x0105, 0x010D, 0x0119, 0x0117, 0x012F, 0x0161, 0x0173, 0x016B,
            0x017E, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x0440, 0x0441, 0x0442, 0x0443, 0x0444, 0x0445, 0x0446, 0x0447,
            0x0448, 0x0449, 0x044A, 0x044B, 0x044C, 0x044D, 0x044E, 0x044F,
            0x0401, 0x0451, 0x201E, 0x201C, 0x2320, 0x2321, 0x00F7, 0x2248,
            0x00B0, 0x2219, 0x00B7, 0x221A, 0x207F, 0x00B2, 0x25A0, 0x00A0]),
        CP773: new Uint16Array([ // CP773
            0x0106, 0x00FC, 0x00E9, 0x0101, 0x00E4, 0x0123, 0x00E5, 0x0107,
            0x0142, 0x0113, 0x0156, 0x0157, 0x012B, 0x0179, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x014D, 0x00F6, 0x0122, 0x00A2, 0x015A,
            0x015B, 0x00D6, 0x00DC, 0x00F8, 0x00A3, 0x00D8, 0x00D7, 0x00A4,
            0x0100, 0x012A, 0x00F3, 0x017B, 0x017C, 0x017A, 0x201D, 0x00A6,
            0x00A9, 0x00AE, 0x00AC, 0x00BD, 0x00BC, 0x0141, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
            0x256A, 0x2518, 0x250C, 0x2588, 0x0104, 0x0105, 0x010C, 0x010D,
            0x00D3, 0x00DF, 0x014C, 0x0143, 0x00F5, 0x00D5, 0x00B5, 0x0144,
            0x0136, 0x0137, 0x013B, 0x013C, 0x0146, 0x0112, 0x0145, 0x2019,
            0x0118, 0x0119, 0x0116, 0x0117, 0x012E, 0x012F, 0x0160, 0x0161,
            0x0172, 0x0173, 0x016A, 0x016B, 0x017D, 0x017E, 0x25A0, 0x00A0]),
        CP774: new Uint16Array([    // CP774, CP1118
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x00FF, 0x00D6, 0x00DC, 0x00A2, 0x00A3, 0x00A5, 0x20A7, 0x0192,
            0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x00F1, 0x00D1, 0x00AA, 0x00BA,
            0x00BF, 0x2310, 0x00AC, 0x00BD, 0x00BC, 0x00A1, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x0104, 0x010C, 0x0118,
            0x0116, 0x2563, 0x2551, 0x2557, 0x255D, 0x012E, 0x0160, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x0172, 0x016A,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x017D,
            0x0105, 0x010D, 0x0119, 0x0117, 0x012F, 0x0161, 0x0173, 0x016B,
            0x017E, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x03B1, 0x00DF, 0x0393, 0x03C0, 0x03A3, 0x03C3, 0x00B5, 0x03C4,
            0x03A6, 0x0398, 0x03A9, 0x03B4, 0x221E, 0x03C6, 0x03B5, 0x2229,
            0x2261, 0x00B1, 0x2265, 0x2264, 0x201E, 0x201C, 0x00F7, 0x2248,
            0x00B0, 0x2219, 0x00B7, 0x221A, 0x207F, 0x00B2, 0x25A0, 0x00A0]),
        CP775: new Uint16Array([    // CP775
            0x0106, 0x00FC, 0x00E9, 0x0101, 0x00E4, 0x0123, 0x00E5, 0x0107,
            0x0142, 0x0113, 0x0156, 0x0157, 0x012B, 0x0179, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x014D, 0x00F6, 0x0122, 0x00A2, 0x015A,
            0x015B, 0x00D6, 0x00DC, 0x00F8, 0x00A3, 0x00D8, 0x00D7, 0x00A4,
            0x0100, 0x012A, 0x00F3, 0x017B, 0x017C, 0x017A, 0x201D, 0x00A6,
            0x00A9, 0x00AE, 0x00AC, 0x00BD, 0x00BC, 0x0141, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x0104, 0x010C, 0x0118,
            0x0116, 0x2563, 0x2551, 0x2557, 0x255D, 0x012E, 0x0160, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x0172, 0x016A,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x017D,
            0x0105, 0x010D, 0x0119, 0x0117, 0x012F, 0x0161, 0x0173, 0x016B,
            0x017E, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x00D3, 0x00DF, 0x014C, 0x0143, 0x00F5, 0x00D5, 0x00B5, 0x0144,
            0x0136, 0x0137, 0x013B, 0x013C, 0x0146, 0x0112, 0x0145, 0x2019,
            _SHY  , 0x00B1, 0x201C, 0x00BE, 0x00B6, 0x00A7, 0x00F7, 0x201E,
            0x00B0, 0x2219, 0x00B7, 0x00B9, 0x00B3, 0x00B2, 0x25A0, 0x00A0]),
        CP808: new Uint16Array([    // CP808
            0x0410, 0x0411, 0x0412, 0x0413, 0x0414, 0x0415, 0x0416, 0x0417,
            0x0418, 0x0419, 0x041A, 0x041B, 0x041C, 0x041D, 0x041E, 0x041F,
            0x0420, 0x0421, 0x0422, 0x0423, 0x0424, 0x0425, 0x0426, 0x0427,
            0x0428, 0x0429, 0x042A, 0x042B, 0x042C, 0x042D, 0x042E, 0x042F,
            0x0430, 0x0431, 0x0432, 0x0433, 0x0434, 0x0435, 0x0436, 0x0437,
            0x0438, 0x0439, 0x043A, 0x043B, 0x043C, 0x043D, 0x043E, 0x043F,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
            0x256A, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x0440, 0x0441, 0x0442, 0x0443, 0x0444, 0x0445, 0x0446, 0x0447,
            0x0448, 0x0449, 0x044A, 0x044B, 0x044C, 0x044D, 0x044E, 0x044F,
            0x0401, 0x0451, 0x0404, 0x0454, 0x0407, 0x0457, 0x040E, 0x045E,
            0x00B0, 0x2219, 0x00B7, 0x221A, 0x2116, 0x20AC, 0x25A0, 0x00A0]),
        CP813: new Uint16Array([    // CP813
            0x00A0, 0x2018, 0x2019, 0x00A3, 0x20AC, _BLANK, 0x00A6, 0x00A7,
            0x00A8, 0x00A9, _BLANK, 0x00AB, 0x00AC, _SHY  , _BLANK, 0x2015,
            0x00B0, 0x00B1, 0x00B2, 0x00B3, 0x00B4, 0x0385, 0x0386, 0x0387,
            0x0388, 0x0389, 0x038A, 0x00BB, 0x038C, 0x00BD, 0x038E, 0x038F,
            0x0390, 0x0391, 0x0392, 0x0393, 0x0394, 0x0395, 0x0396, 0x0397,
            0x0398, 0x0399, 0x039A, 0x039B, 0x039C, 0x039D, 0x039E, 0x039F,
            0x03A0, 0x03A1, _BLANK, 0x03A3, 0x03A4, 0x03A5, 0x03A6, 0x03A7,
            0x03A8, 0x03A9, 0x03AA, 0x03AB, 0x03AC, 0x03AD, 0x03AE, 0x03AF,
            0x03B0, 0x03B1, 0x03B2, 0x03B3, 0x03B4, 0x03B5, 0x03B6, 0x03B7,
            0x03B8, 0x03B9, 0x03BA, 0x03BB, 0x03BC, 0x03BD, 0x03BE, 0x03BF,
            0x03C0, 0x03C1, 0x03C2, 0x03C3, 0x03C4, 0x03C5, 0x03C6, 0x03C7,
            0x03C8, 0x03C9, 0x03CA, 0x03CB, 0x03CC, 0x03CD, 0x03CE, _BLANK]),
        CP850: new Uint16Array([    // CP850
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x00FF, 0x00D6, 0x00DC, 0x00F8, 0x00A3, 0x00D8, 0x00D7, 0x0192,
            0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x00F1, 0x00D1, 0x00AA, 0x00BA,
            0x00BF, 0x00AE, 0x00AC, 0x00BD, 0x00BC, 0x00A1, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x00C1, 0x00C2, 0x00C0,
            0x00A9, 0x2563, 0x2551, 0x2557, 0x255D, 0x00A2, 0x00A5, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x00E3, 0x00C3,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x00A4,
            0x00F0, 0x00D0, 0x00CA, 0x00CB, 0x00C8, 0x0131, 0x00CD, 0x00CE,
            0x00CF, 0x2518, 0x250C, 0x2588, 0x2584, 0x00A6, 0x00CC, 0x2580,
            0x00D3, 0x00DF, 0x00D4, 0x00D2, 0x00F5, 0x00D5, 0x00B5, 0x00FE,
            0x00DE, 0x00DA, 0x00DB, 0x00D9, 0x00FD, 0x00DD, 0x00AF, 0x00B4,
            _SHY  , 0x00B1, 0x2017, 0x00BE, 0x00B6, 0x00A7, 0x00F7, 0x00B8,
            0x00B0, 0x00A8, 0x00B7, 0x00B9, 0x00B3, 0x00B2, 0x25A0, 0x00A0]),
        CP851: new Uint16Array([    // CP851
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x0386, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x0388, 0x00C4, 0x0389,
            0x038A, _BLANK, 0x038C, 0x00F4, 0x00F6, 0x038E, 0x00FB, 0x00F9,
            0x038F, 0x00D6, 0x00DC, 0x03AC, 0x00A3, 0x03AD, 0x03AE, 0x03AF,
            0x03CA, 0x0390, 0x03CC, 0x03CD, 0x0391, 0x0392, 0x0393, 0x0394,
            0x0395, 0x0396, 0x0397, 0x00BD, 0x0398, 0x0399, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x039A, 0x039B, 0x039C,
            0x039D, 0x2563, 0x2551, 0x2557, 0x255D, 0x039E, 0x039F, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x03A0, 0x03A1,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x03A3,
            0x03A4, 0x03A5, 0x03A6, 0x03A7, 0x03A8, 0x03A9, 0x03B1, 0x03B2,
            0x03B3, 0x2518, 0x250C, 0x2588, 0x2584, 0x03B4, 0x03B5, 0x2580,
            0x03B6, 0x03B7, 0x03B8, 0x03B9, 0x03BA, 0x03BB, 0x03BC, 0x03BD,
            0x03BE, 0x03BF, 0x03C0, 0x03C1, 0x03C3, 0x03C2, 0x03C4, 0x0384,
            _SHY  , 0x00B1, 0x03C5, 0x03C6, 0x03C7, 0x00A7, 0x03C8, 0x0385,
            0x00B0, 0x00A8, 0x03C9, 0x03CB, 0x03B0, 0x03CE, 0x25A0, 0x00A0]),
        CP852: new Uint16Array([    // CP852
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x016F, 0x0107, 0x00E7,
            0x0142, 0x00EB, 0x0150, 0x0151, 0x00EE, 0x0179, 0x00C4, 0x0106,
            0x00C9, 0x0139, 0x013A, 0x00F4, 0x00F6, 0x013D, 0x013E, 0x015A,
            0x015B, 0x00D6, 0x00DC, 0x0164, 0x0165, 0x0141, 0x00D7, 0x010D,
            0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x0104, 0x0105, 0x017D, 0x017E,
            0x0118, 0x0119, 0x00AC, 0x017A, 0x010C, 0x015F, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x00C1, 0x00C2, 0x011A,
            0x015E, 0x2563, 0x2551, 0x2557, 0x255D, 0x017B, 0x017C, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x0102, 0x0103,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x00A4,
            0x0111, 0x0110, 0x010E, 0x00CB, 0x010F, 0x0147, 0x00CD, 0x00CE,
            0x011B, 0x2518, 0x250C, 0x2588, 0x2584, 0x0162, 0x016E, 0x2580,
            0x00D3, 0x00DF, 0x00D4, 0x0143, 0x0144, 0x0148, 0x0160, 0x0161,
            0x0154, 0x00DA, 0x0155, 0x0170, 0x00FD, 0x00DD, 0x0163, 0x00B4,
            _SHY  , 0x02DD, 0x02DB, 0x02C7, 0x02D8, 0x00A7, 0x00F7, 0x00B8,
            0x00B0, 0x00A8, 0x02D9, 0x0171, 0x0158, 0x0159, 0x25A0, 0x00A0]),
        CP853: new Uint16Array([    // CP853
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x0109, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x0108,
            0x00C9, 0x010B, 0x010A, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x0130, 0x00D6, 0x00DC, 0x011D, 0x00A3, 0x011C, 0x00D7, 0x0135,
            0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x00F1, 0x00D1, 0x011E, 0x011F,
            0x0124, 0x0125, _BLANK, 0x00BD, 0x0134, 0x015F, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x00C1, 0x00C2, 0x00C0,
            0x015E, 0x2563, 0x2551, 0x2557, 0x255D, 0x017B, 0x017C, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x015C, 0x015D,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x00A4,
            _BLANK, _BLANK, 0x00CA, 0x00CB, 0x00C8, 0x0131, 0x00CD, 0x00CE,
            0x00CF, 0x2518, 0x250C, 0x2588, 0x2584, _BLANK, 0x00CC, 0x2580,
            0x00D3, 0x00DF, 0x00D4, 0x00D2, 0x0120, 0x0121, 0x00B5, 0x0126,
            0x0127, 0x00DA, 0x00DB, 0x00D9, 0x016C, 0x016D, 0x00B7, 0x00B4,
            _SHY  , _BLANK, 0x2113, 0x0149, 0x02D8, 0x00A7, 0x00F7, 0x00B8,
            0x00B0, 0x00A8, 0x02D9, _BLANK, 0x00B3, 0x00B2, 0x25A0, 0x00A0]),
        CP855: new Uint16Array([    // CP855
            0x0452, 0x0402, 0x0453, 0x0403, 0x0451, 0x0401, 0x0454, 0x0404,
            0x0455, 0x0405, 0x0456, 0x0406, 0x0457, 0x0407, 0x0458, 0x0408,
            0x0459, 0x0409, 0x045A, 0x040A, 0x045B, 0x040B, 0x045C, 0x040C,
            0x045E, 0x040E, 0x045F, 0x040F, 0x044E, 0x042E, 0x044A, 0x042A,
            0x0430, 0x0410, 0x0431, 0x0411, 0x0446, 0x0426, 0x0434, 0x0414,
            0x0435, 0x0415, 0x0444, 0x0424, 0x0433, 0x0413, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x0445, 0x0425, 0x0438,
            0x0418, 0x2563, 0x2551, 0x2557, 0x255D, 0x0439, 0x0419, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x043A, 0x041A,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x00A4,
            0x043B, 0x041B, 0x043C, 0x041C, 0x043D, 0x041D, 0x043E, 0x041E,
            0x043F, 0x2518, 0x250C, 0x2588, 0x2584, 0x041F, 0x044F, 0x2580,
            0x042F, 0x0440, 0x0420, 0x0441, 0x0421, 0x0442, 0x0422, 0x0443,
            0x0423, 0x0436, 0x0416, 0x0432, 0x0412, 0x044C, 0x042C, 0x2116,
            _SHY  , 0x044B, 0x042B, 0x0437, 0x0417, 0x0448, 0x0428, 0x044D,
            0x042D, 0x0449, 0x0429, 0x0447, 0x0427, 0x00A7, 0x25A0, 0x00A0]),
        CP857: new Uint16Array([    // CP857
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x0131, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x0130, 0x00D6, 0x00DC, 0x00F8, 0x00A3, 0x00D8, 0x015E, 0x015F,
            0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x00F1, 0x00D1, 0x011E, 0x011F,
            0x00BF, 0x00AE, 0x00AC, 0x00BD, 0x00BC, 0x00A1, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x00C1, 0x00C2, 0x00C0,
            0x00A9, 0x2563, 0x2551, 0x2557, 0x255D, 0x00A2, 0x00A5, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x00E3, 0x00C3,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x00A4,
            0x00BA, 0x00AA, 0x00CA, 0x00CB, 0x00C8, 0x20AC, 0x00CD, 0x00CE,
            0x00CF, 0x2518, 0x250C, 0x2588, 0x2584, 0x00A6, 0x00CC, 0x2580,
            0x00D3, 0x00DF, 0x00D4, 0x00D2, 0x00F5, 0x00D5, 0x00B5, _BLANK,
            0x00D7, 0x00DA, 0x00DB, 0x00D9, 0x00EC, 0x00FF, 0x00AF, 0x00B4,
            _SHY  , 0x00B1, _BLANK, 0x00BE, 0x00B6, 0x00A7, 0x00F7, 0x00B8,
            0x00B0, 0x00A8, 0x00B7, 0x00B9, 0x00B3, 0x00B2, 0x25A0, 0x00A0]),
        CP858: new Uint16Array([    // CP858
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x00FF, 0x00D6, 0x00DC, 0x00F8, 0x00A3, 0x00D8, 0x00D7, 0x0192,
            0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x00F1, 0x00D1, 0x00AA, 0x00BA,
            0x00BF, 0x00AE, 0x00AC, 0x00BD, 0x00BC, 0x00A1, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x00C1, 0x00C2, 0x00C0,
            0x00A9, 0x2563, 0x2551, 0x2557, 0x255D, 0x00A2, 0x00A5, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x00E3, 0x00C3,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x00A4,
            0x00F0, 0x00D0, 0x00CA, 0x00CB, 0x00C8, 0x20AC, 0x00CD, 0x00CE,
            0x00CF, 0x2518, 0x250C, 0x2588, 0x2584, 0x00A6, 0x00CC, 0x2580,
            0x00D3, 0x00DF, 0x00D4, 0x00D2, 0x00F5, 0x00D5, 0x00B5, 0x00FE,
            0x00DE, 0x00DA, 0x00DB, 0x00D9, 0x00FD, 0x00DD, 0x00AF, 0x00B4,
            _SHY  , 0x00B1, 0x2017, 0x00BE, 0x00B6, 0x00A7, 0x00F7, 0x00B8,
            0x00B0, 0x00A8, 0x00B7, 0x00B9, 0x00B3, 0x00B2, 0x25A0, 0x00A0]),
        CP859: new Uint16Array([    // CP859
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x00FF, 0x00D6, 0x00DC, 0x00F8, 0x00A3, 0x00D8, 0x00D7, 0x0192,
            0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x00F1, 0x00D1, 0x00AA, 0x00BA,
            0x00BF, 0x00AE, 0x00AC, 0x0153, 0x0152, 0x00A1, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x00C1, 0x00C2, 0x00C0,
            0x00A9, 0x2563, 0x2551, 0x2557, 0x255D, 0x00A2, 0x00A5, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x00E3, 0x00C3,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x00A4,
            0x00F0, 0x00D0, 0x00CA, 0x00CB, 0x00C8, 0x20AC, 0x00CD, 0x00CE,
            0x00CF, 0x2518, 0x250C, 0x2588, 0x2584, 0x0160, 0x00CC, 0x2580,
            0x00D3, 0x00DF, 0x00D4, 0x00D2, 0x00F5, 0x00D5, 0x00B5, 0x00FE,
            0x00DE, 0x00DA, 0x00DB, 0x00D9, 0x00FD, 0x00DD, 0x00AF, 0x017D,
            _SHY  , 0x00B1, _BLANK, 0x0178, 0x00B6, 0x00A7, 0x00F7, 0x017E,
            0x00B0, 0x0161, 0x00B7, 0x00B9, 0x00B3, 0x00B2, 0x25A0, 0x00A0]),
        CP860: new Uint16Array([    // CP860
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E3, 0x00E0, 0x00C1, 0x00E7,
            0x00EA, 0x00CA, 0x00E8, 0x00CD, 0x00D4, 0x00EC, 0x00C3, 0x00C2,
            0x00C9, 0x00C0, 0x00C8, 0x00F4, 0x00F5, 0x00F2, 0x00DA, 0x00F9,
            0x00CC, 0x00D5, 0x00DC, 0x00A2, 0x00A3, 0x00D9, 0x20A7, 0x00D3,
            0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x00F1, 0x00D1, 0x00AA, 0x00BA,
            0x00BF, 0x00D2, 0x00AC, 0x00BD, 0x00BC, 0x00A1, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
            0x256A, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x03B1, 0x00DF, 0x0393, 0x03C0, 0x03A3, 0x03C3, 0x00B5, 0x03C4,
            0x03A6, 0x0398, 0x03A9, 0x03B4, 0x221E, 0x03C6, 0x03B5, 0x2229,
            0x2261, 0x00B1, 0x2265, 0x2264, 0x2320, 0x2321, 0x00F7, 0x2248,
            0x00B0, 0x2219, 0x00B7, 0x221A, 0x207F, 0x00B2, 0x25A0, 0x00A0]),
        CP861: new Uint16Array([    // CP861
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00D0, 0x00F0, 0x00DE, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00FE, 0x00FB, 0x00DD,
            0x00FD, 0x00D6, 0x00DC, 0x00F8, 0x00A3, 0x00D8, 0x20A7, 0x0192,
            0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x00C1, 0x00CD, 0x00D3, 0x00DA,
            0x00BF, 0x2310, 0x00AC, 0x00BD, 0x00BC, 0x00A1, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
            0x256A, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x03B1, 0x00DF, 0x0393, 0x03C0, 0x03A3, 0x03C3, 0x00B5, 0x03C4,
            0x03A6, 0x0398, 0x03A9, 0x03B4, 0x221E, 0x03C6, 0x03B5, 0x2229,
            0x2261, 0x00B1, 0x2265, 0x2264, 0x2320, 0x2321, 0x00F7, 0x2248,
            0x00B0, 0x2219, 0x00B7, 0x221A, 0x207F, 0x00B2, 0x25A0, 0x00A0]),
        CP863: new Uint16Array([    // CP863
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00C2, 0x00E0, 0x00B6, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x2017, 0x00C0, 0x00A7,
            0x00C9, 0x00C8, 0x00CA, 0x00F4, 0x00CB, 0x00CF, 0x00FB, 0x00F9,
            0x00A4, 0x00D4, 0x00DC, 0x00A2, 0x00A3, 0x00D9, 0x00DB, 0x0192,
            0x00A6, 0x00B4, 0x00F3, 0x00FA, 0x00A8, 0x00B8, 0x00B3, 0x00AF,
            0x00CE, 0x2310, 0x00AC, 0x00BD, 0x00BC, 0x00BE, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
            0x256A, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x03B1, 0x00DF, 0x0393, 0x03C0, 0x03A3, 0x03C3, 0x00B5, 0x03C4,
            0x03A6, 0x0398, 0x03A9, 0x03B4, 0x221E, 0x03C6, 0x03B5, 0x2229,
            0x2261, 0x00B1, 0x2265, 0x2264, 0x2320, 0x2321, 0x00F7, 0x2248,
            0x00B0, 0x2219, 0x00B7, 0x221A, 0x207F, 0x00B2, 0x25A0, 0x00A0]),
        CP865: new Uint16Array([    // CP865
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x00FF, 0x00D6, 0x00DC, 0x00F8, 0x00A3, 0x00D8, 0x20A7, 0x0192,
            0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x00F1, 0x00D1, 0x00AA, 0x00BA,
            0x00BF, 0x2310, 0x00AC, 0x00BD, 0x00BC, 0x00A1, 0x00AB, 0x00A4,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
            0x256A, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x03B1, 0x00DF, 0x0393, 0x03C0, 0x03A3, 0x03C3, 0x00B5, 0x03C4,
            0x03A6, 0x0398, 0x03A9, 0x03B4, 0x221E, 0x03C6, 0x03B5, 0x2229,
            0x2261, 0x00B1, 0x2265, 0x2264, 0x2320, 0x2321, 0x00F7, 0x2248,
            0x00B0, 0x2219, 0x00B7, 0x221A, 0x207F, 0x00B2, 0x25A0, 0x00A0]),
        CP866: new Uint16Array([    // CP866, CP900
            0x0410, 0x0411, 0x0412, 0x0413, 0x0414, 0x0415, 0x0416, 0x0417,
            0x0418, 0x0419, 0x041A, 0x041B, 0x041C, 0x041D, 0x041E, 0x041F,
            0x0420, 0x0421, 0x0422, 0x0423, 0x0424, 0x0425, 0x0426, 0x0427,
            0x0428, 0x0429, 0x042A, 0x042B, 0x042C, 0x042D, 0x042E, 0x042F,
            0x0430, 0x0431, 0x0432, 0x0433, 0x0434, 0x0435, 0x0436, 0x0437,
            0x0438, 0x0439, 0x043A, 0x043B, 0x043C, 0x043D, 0x043E, 0x043F,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
            0x256A, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x0440, 0x0441, 0x0442, 0x0443, 0x0444, 0x0445, 0x0446, 0x0447,
            0x0448, 0x0449, 0x044A, 0x044B, 0x044C, 0x044D, 0x044E, 0x044F,
            0x0401, 0x0451, 0x0404, 0x0454, 0x0407, 0x0457, 0x040E, 0x045E,
            0x00B0, 0x2219, 0x00B7, 0x221A, 0x2116, 0x00A4, 0x25A0, 0x00A0]),
        CP867: new Uint16Array([    // CP867, CP895
            0x010C, 0x00FC, 0x00E9, 0x010F, 0x00E4, 0x010E, 0x0164, 0x010D,
            0x011B, 0x011A, 0x0139, 0x00CD, 0x013E, 0x013A, 0x00C4, 0x00C1,
            0x00C9, 0x017E, 0x017D, 0x00F4, 0x00F6, 0x00D3, 0x016F, 0x00DA,
            0x00FD, 0x00D6, 0x00DC, 0x0160, 0x013D, 0x00DD, 0x0158, 0x0165,
            0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x0148, 0x0147, 0x016E, 0x00D4,
            0x0161, 0x0159, 0x0155, 0x0154, 0x00BC, 0x00A7, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
            0x256A, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x03B1, 0x00DF, 0x0393, 0x03C0, 0x03A3, 0x03C3, 0x00B5, 0x03C4,
            0x03A6, 0x0398, 0x03A9, 0x03B4, 0x221E, 0x03C6, 0x03B5, 0x2229,
            0x2261, 0x00B1, 0x2265, 0x2264, 0x2320, 0x2321, 0x00F7, 0x2248,
            0x00B0, 0x2219, 0x00B7, 0x221A, 0x207F, 0x00B2, 0x25A0, 0x00A0]),
        CP869: new Uint16Array([    // CP869
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, 0x0386, _BLANK,
            0x00B7, 0x00AC, 0x00A6, 0x2018, 0x2019, 0x0388, 0x2015, 0x0389,
            0x038A, 0x03AA, 0x038C, _BLANK, _BLANK, 0x038E, 0x03AB, 0x00A9,
            0x038F, 0x00B2, 0x00B3, 0x03AC, 0x00A3, 0x03AD, 0x03AE, 0x03AF,
            0x03CA, 0x0390, 0x03CC, 0x03CD, 0x0391, 0x0392, 0x0393, 0x0394,
            0x0395, 0x0396, 0x0397, 0x00BD, 0x0398, 0x0399, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x039A, 0x039B, 0x039C,
            0x039D, 0x2563, 0x2551, 0x2557, 0x255D, 0x039E, 0x039F, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x03A0, 0x03A1,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x03A3,
            0x03A4, 0x03A5, 0x03A6, 0x03A7, 0x03A8, 0x03A9, 0x03B1, 0x03B2,
            0x03B3, 0x2518, 0x250C, 0x2588, 0x2584, 0x03B4, 0x03B5, 0x2580,
            0x03B6, 0x03B7, 0x03B8, 0x03B9, 0x03BA, 0x03BB, 0x03BC, 0x03BD,
            0x03BE, 0x03BF, 0x03C0, 0x03C1, 0x03C3, 0x03C2, 0x03C4, 0x0384,
            _SHY  , 0x00B1, 0x03C5, 0x03C6, 0x03C7, 0x00A7, 0x03C8, 0x0385,
            0x00B0, 0x00A8, 0x03C9, 0x03CB, 0x03B0, 0x03CE, 0x25A0, 0x00A0]),
        CP872: new Uint16Array([    // CP872
            0x0452, 0x0402, 0x0453, 0x0403, 0x0451, 0x0401, 0x0454, 0x0404,
            0x0455, 0x0405, 0x0456, 0x0406, 0x0457, 0x0407, 0x0458, 0x0408,
            0x0459, 0x0409, 0x045A, 0x040A, 0x045B, 0x040B, 0x045C, 0x040C,
            0x045E, 0x040E, 0x045F, 0x040F, 0x044E, 0x042E, 0x044A, 0x042A,
            0x0430, 0x0410, 0x0431, 0x0411, 0x0446, 0x0426, 0x0434, 0x0414,
            0x0435, 0x0415, 0x0444, 0x0424, 0x0433, 0x0413, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x0445, 0x0425, 0x0438,
            0x0418, 0x2563, 0x2551, 0x2557, 0x255D, 0x0439, 0x0419, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x043A, 0x041A,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x20AC,
            0x043B, 0x041B, 0x043C, 0x041C, 0x043D, 0x041D, 0x043E, 0x041E,
            0x043F, 0x2518, 0x250C, 0x2588, 0x2584, 0x041F, 0x044F, 0x2580,
            0x042F, 0x0440, 0x0420, 0x0441, 0x0421, 0x0442, 0x0422, 0x0443,
            0x0423, 0x0436, 0x0416, 0x0432, 0x0412, 0x044C, 0x042C, 0x2116,
            _SHY  , 0x044B, 0x042B, 0x0437, 0x0417, 0x0448, 0x0428, 0x044D,
            0x042D, 0x0449, 0x0429, 0x0447, 0x0427, 0x00A7, 0x25A0, 0x00A0]),
        CP878: new Uint16Array([    // CP878
            0x2500, 0x2502, 0x250C, 0x2510, 0x2514, 0x2518, 0x251C, 0x2524,
            0x252C, 0x2534, 0x253C, 0x2580, 0x2584, 0x2588, 0x258C, 0x2590,
            0x2591, 0x2592, 0x2593, 0x2320, 0x25A0, 0x2219, 0x221A, 0x2248,
            0x2264, 0x2265, 0x00A0, 0x2321, 0x00B0, 0x00B2, 0x00B7, 0x00F7,
            0x2550, 0x2551, 0x2552, 0x0451, 0x2553, 0x2554, 0x2555, 0x2556,
            0x2557, 0x2558, 0x2559, 0x255A, 0x255B, 0x255C, 0x255D, 0x255E,
            0x255F, 0x2560, 0x2561, 0x0401, 0x2562, 0x2563, 0x2564, 0x2565,
            0x2566, 0x2567, 0x2568, 0x2569, 0x256A, 0x256B, 0x256C, 0x00A9,
            0x044E, 0x0430, 0x0431, 0x0446, 0x0434, 0x0435, 0x0444, 0x0433,
            0x0445, 0x0438, 0x0439, 0x043A, 0x043B, 0x043C, 0x043D, 0x043E,
            0x043F, 0x044F, 0x0440, 0x0441, 0x0442, 0x0443, 0x0436, 0x0432,
            0x044C, 0x044B, 0x0437, 0x0448, 0x044D, 0x0449, 0x0447, 0x044A,
            0x042E, 0x0410, 0x0411, 0x0426, 0x0414, 0x0415, 0x0424, 0x0413,
            0x0425, 0x0418, 0x0419, 0x041A, 0x041B, 0x041C, 0x041D, 0x041E,
            0x041F, 0x042F, 0x0420, 0x0421, 0x0422, 0x0423, 0x0416, 0x0412,
            0x042C, 0x042B, 0x0417, 0x0428, 0x042D, 0x0429, 0x0427, 0x042A]),
        CP912: new Uint16Array([    // CP912
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2518, 0x250C, 0x2588,
            0x00A9, 0x2563, 0x2551, 0x2557, 0x255D, 0x00A2, 0x00A5, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x2584, 0x2580,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x00AE,
            0x00A0, 0x0104, 0x02D8, 0x0141, 0x00A4, 0x013D, 0x015A, 0x00A7,
            0x00A8, 0x0160, 0x015E, 0x0164, 0x0179, _SHY  , 0x017D, 0x017B,
            0x00B0, 0x0105, 0x02DB, 0x0142, 0x00B4, 0x013E, 0x015B, 0x02C7,
            0x00B8, 0x0161, 0x015F, 0x0165, 0x017A, 0x02DD, 0x017E, 0x017C,
            0x0154, 0x00C1, 0x00C2, 0x0102, 0x00C4, 0x0139, 0x0106, 0x00C7,
            0x010C, 0x00C9, 0x0118, 0x00CB, 0x011A, 0x00CD, 0x00CE, 0x010E,
            0x0110, 0x0143, 0x0147, 0x00D3, 0x00D4, 0x0150, 0x00D6, 0x00D7,
            0x0158, 0x016E, 0x00DA, 0x0170, 0x00DC, 0x00DD, 0x0162, 0x00DF,
            0x0155, 0x00E1, 0x00E2, 0x0103, 0x00E4, 0x013A, 0x0107, 0x00E7,
            0x010D, 0x00E9, 0x0119, 0x00EB, 0x011B, 0x00ED, 0x00EE, 0x010F,
            0x0111, 0x0144, 0x0148, 0x00F3, 0x00F4, 0x0151, 0x00F6, 0x00F7,
            0x0159, 0x016F, 0x00FA, 0x0171, 0x00FC, 0x00FD, 0x0163, 0x02D9]),
        CP915: new Uint16Array([    // CP915
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2518, 0x250C, 0x2588,
            0x00A9, 0x2563, 0x2551, 0x2557, 0x255D, 0x00A2, 0x00A5, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x2584, 0x2580,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x00A4,
            0x00A0, 0x0401, 0x0402, 0x0403, 0x0404, 0x0405, 0x0406, 0x0407,
            0x0408, 0x0409, 0x040A, 0x040B, 0x040C, _SHY  , 0x040E, 0x040F,
            0x0410, 0x0411, 0x0412, 0x0413, 0x0414, 0x0415, 0x0416, 0x0417,
            0x0418, 0x0419, 0x041A, 0x041B, 0x041C, 0x041D, 0x041E, 0x041F,
            0x0420, 0x0421, 0x0422, 0x0423, 0x0424, 0x0425, 0x0426, 0x0427,
            0x0428, 0x0429, 0x042A, 0x042B, 0x042C, 0x042D, 0x042E, 0x042F,
            0x0430, 0x0431, 0x0432, 0x0433, 0x0434, 0x0435, 0x0436, 0x0437,
            0x0438, 0x0439, 0x043A, 0x043B, 0x043C, 0x043D, 0x043E, 0x043F,
            0x0440, 0x0441, 0x0442, 0x0443, 0x0444, 0x0445, 0x0446, 0x0447,
            0x0448, 0x0449, 0x044A, 0x044B, 0x044C, 0x044D, 0x044E, 0x044F,
            0x2116, 0x0451, 0x0452, 0x0453, 0x0454, 0x0455, 0x0456, 0x0457,
            0x0458, 0x0459, 0x045A, 0x045B, 0x045C, 0x00A7, 0x045E, 0x045F]),
        CP920: new Uint16Array([    // CP920
            0x00A0, 0x00A1, 0x00A2, 0x00A3, 0x00A4, 0x00A5, 0x00A6, 0x00A7,
            0x00A8, 0x00A9, 0x00AA, 0x00AB, 0x00AC, _SHY  , 0x00AE, 0x00AF,
            0x00B0, 0x00B1, 0x00B2, 0x00B3, 0x00B4, 0x00B5, 0x00B6, 0x00B7,
            0x00B8, 0x00B9, 0x00BA, 0x00BB, 0x00BC, 0x00BD, 0x00BE, 0x00BF,
            0x00C0, 0x00C1, 0x00C2, 0x00C3, 0x00C4, 0x00C5, 0x00C6, 0x00C7,
            0x00C8, 0x00C9, 0x00CA, 0x00CB, 0x00CC, 0x00CD, 0x00CE, 0x00CF,
            0x011E, 0x00D1, 0x00D2, 0x00D3, 0x00D4, 0x00D5, 0x00D6, 0x00D7,
            0x00D8, 0x00D9, 0x00DA, 0x00DB, 0x00DC, 0x0130, 0x015E, 0x00DF,
            0x00E0, 0x00E1, 0x00E2, 0x00E3, 0x00E4, 0x00E5, 0x00E6, 0x00E7,
            0x00E8, 0x00E9, 0x00EA, 0x00EB, 0x00EC, 0x00ED, 0x00EE, 0x00EF,
            0x011F, 0x00F1, 0x00F2, 0x00F3, 0x00F4, 0x00F5, 0x00F6, 0x00F7,
            0x00F8, 0x00F9, 0x00FA, 0x00FB, 0x00FC, 0x0131, 0x015F, 0x00FF]),
        CP1117: new Uint16Array([    // CP1117
            0x0106, 0x00FC, 0x00E9, 0x0101, 0x00E4, 0x0123, 0x00E5, 0x0107,
            0x0142, 0x0113, 0x0117, 0x012F, 0x012B, 0x0179, 0x00C4, 0x00C5,
            0x00C9, 0x017B, 0x017C, 0x014D, 0x00F6, 0x0122, 0x016B, 0x015A,
            0x015B, 0x00D6, 0x00DC, 0x0144, 0x013B, 0x0141, 0x00D7, 0x010D,
            0x0100, 0x012A, 0x00F3, 0x0173, 0x0104, 0x0105, 0x017D, 0x017E,
            0x0118, 0x0119, 0x0116, 0x017A, 0x010C, 0x012E, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
            0x256A, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x00D3, 0x00DF, 0x014C, 0x0143, 0x00F5, 0x00D5, 0x0160, 0x0161,
            0x0136, 0x0137, 0x016A, 0x0172, 0x013C, 0x0112, 0x0145, 0x0146,
            _SHY  , 0x00B1, 0x00E6, 0x00C6, 0x00B6, 0x00A4, 0x00F7, 0x00F8,
            0x00B0, 0x00D8, 0x00B7, 0x0157, 0x0156, 0x201E, 0x201C, 0x00A0]),
        CPMIK: new Uint16Array([    // CPMIK
            0x0410, 0x0411, 0x0412, 0x0413, 0x0414, 0x0415, 0x0416, 0x0417,
            0x0418, 0x0419, 0x041A, 0x041B, 0x041C, 0x041D, 0x041E, 0x041F,
            0x0420, 0x0421, 0x0422, 0x0423, 0x0424, 0x0425, 0x0426, 0x0427,
            0x0428, 0x0429, 0x042A, 0x042B, 0x042C, 0x042D, 0x042E, 0x042F,
            0x0430, 0x0431, 0x0432, 0x0433, 0x0434, 0x0435, 0x0436, 0x0437,
            0x0438, 0x0439, 0x043A, 0x043B, 0x043C, 0x043D, 0x043E, 0x043F,
            0x0440, 0x0441, 0x0442, 0x0443, 0x0444, 0x0445, 0x0446, 0x0447,
            0x0448, 0x0449, 0x044A, 0x044B, 0x044C, 0x044D, 0x044E, 0x044F,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x2563, 0x2551,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2510,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2116, 0x00A7, 0x2557,
            0x255D, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x03B1, 0x00DF, 0x0393, 0x03C0, 0x03A3, 0x03C3, 0x00B5, 0x03C4,
            0x03A6, 0x0398, 0x03A9, 0x03B4, 0x221E, 0x03C6, 0x03B5, 0x2229,
            0x2261, 0x00B1, 0x2265, 0x2264, 0x2320, 0x2321, 0x00F7, 0x2248,
            0x00B0, 0x2219, 0x00B7, 0x221A, 0x207F, 0x00B2, 0x25A0, 0x00A0]),
        WIN1250: new Uint16Array([    // WIN1250
            0x20AC, _BLANK, 0x201A, _BLANK, 0x201E, 0x2026, 0x2020, 0x2021,
            _BLANK, 0x2030, 0x0160, 0x2039, 0x015A, 0x0164, 0x017D, 0x0179,
            _BLANK, 0x2018, 0x2019, 0x201C, 0x201D, 0x2022, 0x2013, 0x2014,
            _BLANK, 0x2122, 0x0161, 0x203A, 0x015B, 0x0165, 0x017E, 0x017A,
            0x00A0, 0x02C7, 0x02D8, 0x0141, 0x00A4, 0x0104, 0x00A6, 0x00A7,
            0x00A8, 0x00A9, 0x015E, 0x00AB, 0x00AC, _SHY  , 0x00AE, 0x017B,
            0x00B0, 0x00B1, 0x02DB, 0x0142, 0x00B4, 0x00B5, 0x00B6, 0x00B7,
            0x00B8, 0x0105, 0x015F, 0x00BB, 0x013D, 0x02DD, 0x013E, 0x017C,
            0x0154, 0x00C1, 0x00C2, 0x0102, 0x00C4, 0x0139, 0x0106, 0x00C7,
            0x010C, 0x00C9, 0x0118, 0x00CB, 0x011A, 0x00CD, 0x00CE, 0x010E,
            0x0110, 0x0143, 0x0147, 0x00D3, 0x00D4, 0x0150, 0x00D6, 0x00D7,
            0x0158, 0x016E, 0x00DA, 0x0170, 0x00DC, 0x00DD, 0x0162, 0x00DF,
            0x0155, 0x00E1, 0x00E2, 0x0103, 0x00E4, 0x013A, 0x0107, 0x00E7,
            0x010D, 0x00E9, 0x0119, 0x00EB, 0x011B, 0x00ED, 0x00EE, 0x010F,
            0x0111, 0x0144, 0x0148, 0x00F3, 0x00F4, 0x0151, 0x00F6, 0x00F7,
            0x0159, 0x016F, 0x00FA, 0x0171, 0x00FC, 0x00FD, 0x0163, 0x02D9]),
        WIN1251: new Uint16Array([    // WIN1251 [1]
            0x0402, 0x0403, 0x201A, 0x0453, 0x201E, 0x2026, 0x2020, 0x2021,
            0x20AC, 0x2030, 0x0409, 0x2039, 0x040A, 0x040C, 0x040B, 0x040F,
            0x0452, 0x2018, 0x2019, 0x201C, 0x201D, 0x2022, 0x2013, 0x2014,
            _BLANK, 0x2122, 0x0459, 0x203A, 0x045A, 0x045C, 0x045B, 0x045F,
            0x00A0, 0x040E, 0x045E, 0x0408, 0x00A4, 0x0490, 0x00A6, 0x00A7,
            0x0401, 0x00A9, 0x0404, 0x00AB, 0x00AC, _SHY  , 0x00AE, 0x0407,
            0x00B0, 0x00B1, 0x0406, 0x0456, 0x0491, 0x00B5, 0x00B6, 0x00B7,
            0x0451, 0x2116, 0x0454, 0x00BB, 0x0458, 0x0405, 0x0455, 0x0457,
            0x0410, 0x0411, 0x0412, 0x0413, 0x0414, 0x0415, 0x0416, 0x0417,
            0x0418, 0x0419, 0x041A, 0x041B, 0x041C, 0x041D, 0x041E, 0x041F,
            0x0420, 0x0421, 0x0422, 0x0423, 0x0424, 0x0425, 0x0426, 0x0427,
            0x0428, 0x0429, 0x042A, 0x042B, 0x042C, 0x042D, 0x042E, 0x042F,
            0x0430, 0x0431, 0x0432, 0x0433, 0x0434, 0x0435, 0x0436, 0x0437,
            0x0438, 0x0439, 0x043A, 0x043B, 0x043C, 0x043D, 0x043E, 0x043F,
            0x0440, 0x0441, 0x0442, 0x0443, 0x0444, 0x0445, 0x0446, 0x0447,
            0x0448, 0x0449, 0x044A, 0x044B, 0x044C, 0x044D, 0x044E, 0x044F]),
        WIN1253: new Uint16Array([    // WIN1253
            0x20AC, _BLANK, 0x201A, 0x0192, 0x201E, 0x2026, 0x2020, 0x2021,
            _BLANK, 0x2030, _BLANK, 0x2039, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, 0x2018, 0x2019, 0x201C, 0x201D, 0x2022, 0x2013, 0x2014,
            _BLANK, 0x2122, _BLANK, 0x203A, _BLANK, _BLANK, _BLANK, _BLANK,
            0x00A0, 0x0385, 0x0386, 0x00A3, 0x00A4, 0x00A5, 0x00A6, 0x00A7,
            0x00A8, 0x00A9, _BLANK, 0x00AB, 0x00AC, _SHY  , 0x00AE, 0x2015,
            0x00B0, 0x00B1, 0x00B2, 0x00B3, 0x0384, 0x00B5, 0x00B6, 0x00B7,
            0x0388, 0x0389, 0x038A, 0x00BB, 0x038C, 0x00BD, 0x038E, 0x038F,
            0x0390, 0x0391, 0x0392, 0x0393, 0x0394, 0x0395, 0x0396, 0x0397,
            0x0398, 0x0399, 0x039A, 0x039B, 0x039C, 0x039D, 0x039E, 0x039F,
            0x03A0, 0x03A1, _BLANK, 0x03A3, 0x03A4, 0x03A5, 0x03A6, 0x03A7,
            0x03A8, 0x03A9, 0x03AA, 0x03AB, 0x03AC, 0x03AD, 0x03AE, 0x03AF,
            0x03B0, 0x03B1, 0x03B2, 0x03B3, 0x03B4, 0x03B5, 0x03B6, 0x03B7,
            0x03B8, 0x03B9, 0x03BA, 0x03BB, 0x03BC, 0x03BD, 0x03BE, 0x03BF,
            0x03C0, 0x03C1, 0x03C2, 0x03C3, 0x03C4, 0x03C5, 0x03C6, 0x03C7,
            0x03C8, 0x03C9, 0x03CA, 0x03CB, 0x03CC, 0x03CD, 0x03CE, _BLANK]),
        WIN1254: new Uint16Array([    // WIN1254
            0x20AC, _BLANK, 0x201A, 0x0192, 0x201E, 0x2026, 0x2020, 0x2021,
            0x02C6, 0x2030, 0x0160, 0x2039, 0x0152, _BLANK, _BLANK, _BLANK,
            _BLANK, 0x2018, 0x2019, 0x201C, 0x201D, 0x2022, 0x2013, 0x2014,
            0x02DC, 0x2122, 0x0161, 0x203A, 0x0153, _BLANK, _BLANK, 0x0178,
            0x00A0, 0x00A1, 0x00A2, 0x00A3, 0x00A4, 0x00A5, 0x00A6, 0x00A7,
            0x00A8, 0x00A9, 0x00AA, 0x00AB, 0x00AC, _SHY  , 0x00AE, 0x00AF,
            0x00B0, 0x00B1, 0x00B2, 0x00B3, 0x00B4, 0x00B5, 0x00B6, 0x00B7,
            0x00B8, 0x00B9, 0x00BA, 0x00BB, 0x00BC, 0x00BD, 0x00BE, 0x00BF,
            0x00C0, 0x00C1, 0x00C2, 0x00C3, 0x00C4, 0x00C5, 0x00C6, 0x00C7,
            0x00C8, 0x00C9, 0x00CA, 0x00CB, 0x00CC, 0x00CD, 0x00CE, 0x00CF,
            0x011E, 0x00D1, 0x00D2, 0x00D3, 0x00D4, 0x00D5, 0x00D6, 0x00D7,
            0x00D8, 0x00D9, 0x00DA, 0x00DB, 0x00DC, 0x0130, 0x015E, 0x00DF,
            0x00E0, 0x00E1, 0x00E2, 0x00E3, 0x00E4, 0x00E5, 0x00E6, 0x00E7,
            0x00E8, 0x00E9, 0x00EA, 0x00EB, 0x00EC, 0x00ED, 0x00EE, 0x00EF,
            0x011F, 0x00F1, 0x00F2, 0x00F3, 0x00F4, 0x00F5, 0x00F6, 0x00F7,
            0x00F8, 0x00F9, 0x00FA, 0x00FB, 0x00FC, 0x0131, 0x015F, 0x00FF]),
        WIN1257: new Uint16Array([    // WIN1257
            0x20AC, _BLANK, 0x201A, _BLANK, 0x201E, 0x2026, 0x2020, 0x2021,
            _BLANK, 0x2030, _BLANK, 0x2039, _BLANK, 0x00A8, 0x02C7, 0x00B8,
            _BLANK, 0x2018, 0x2019, 0x201C, 0x201D, 0x2022, 0x2013, 0x2014,
            _BLANK, 0x2122, _BLANK, 0x203A, _BLANK, 0x00AF, 0x02DB, _BLANK,
            0x00A0, _BLANK, 0x00A2, 0x00A3, 0x00A4, _BLANK, 0x00A6, 0x00A7,
            0x00D8, 0x00A9, 0x0156, 0x00AB, 0x00AC, _SHY  , 0x00AE, 0x00C6,
            0x00B0, 0x00B1, 0x00B2, 0x00B3, 0x00B4, 0x00B5, 0x00B6, 0x00B7,
            0x00F8, 0x00B9, 0x0157, 0x00BB, 0x00BC, 0x00BD, 0x00BE, 0x00E6,
            0x0104, 0x012E, 0x0100, 0x0106, 0x00C4, 0x00C5, 0x0118, 0x0112,
            0x010C, 0x00C9, 0x0179, 0x0116, 0x0122, 0x0136, 0x012A, 0x013B,
            0x0160, 0x0143, 0x0145, 0x00D3, 0x014C, 0x00D5, 0x00D6, 0x00D7,
            0x0172, 0x0141, 0x015A, 0x016A, 0x00DC, 0x017B, 0x017D, 0x00DF,
            0x0105, 0x012F, 0x0101, 0x0107, 0x00E4, 0x00E5, 0x0119, 0x0113,
            0x010D, 0x00E9, 0x017A, 0x0117, 0x0123, 0x0137, 0x012B, 0x013C,
            0x0161, 0x0144, 0x0146, 0x00F3, 0x014D, 0x00F5, 0x00F6, 0x00F7,
            0x0173, 0x0142, 0x015B, 0x016B, 0x00FC, 0x017C, 0x017E, 0x02D9]),
        KOI8_R: new Uint16Array([   // KOI8-R [2]
            0x2500, 0x2502, 0x250C, 0x2510, 0x2514, 0x2518, 0x251C, 0x2524,
            0x252C, 0x2534, 0x253C, 0x2580, 0x2584, 0x2588, 0x258C, 0x2590,
            0x2591, 0x2592, 0x2593, 0x2320, 0x25A0, 0x2219, 0x221A, 0x2248,
            0x2264, 0x2265, 0x00A0, 0x2321, 0x00B0, 0x00B2, 0x00B7, 0x00F7,
            0x2550, 0x2551, 0x2552, 0x0451, 0x2553, 0x2554, 0x2555, 0x2556,
            0x2557, 0x2558, 0x2559, 0x255A, 0x255B, 0x255C, 0x255D, 0x255E,
            0x255F, 0x2560, 0x2561, 0x0401, 0x2562, 0x2563, 0x2564, 0x2565,
            0x2566, 0x2567, 0x2568, 0x2569, 0x256A, 0x256B, 0x256C, 0x00A9,
            0x044E, 0x0430, 0x0431, 0x0446, 0x0434, 0x0435, 0x0444, 0x0433,
            0x0445, 0x0438, 0x0439, 0x043A, 0x043B, 0x043C, 0x043D, 0x043E,
            0x043F, 0x044F, 0x0440, 0x0441, 0x0442, 0x0443, 0x0436, 0x0432,
            0x044C, 0x044B, 0x0437, 0x0448, 0x044D, 0x0449, 0x0447, 0x044A,
            0x042E, 0x0410, 0x0411, 0x0426, 0x0414, 0x0415, 0x0424, 0x0413,
            0x0425, 0x0418, 0x0419, 0x041A, 0x041B, 0x041C, 0x041D, 0x041E,
            0x041F, 0x042F, 0x0420, 0x0421, 0x0422, 0x0423, 0x0416, 0x0412,
            0x042C, 0x042B, 0x0417, 0x0428, 0x042D, 0x0429, 0x0427, 0x042A]),
        KOI8_U: new Uint16Array([   // KOI8-U
            0x2500, 0x2502, 0x250C, 0x2510, 0x2514, 0x2518, 0x251C, 0x2524,
            0x252C, 0x2534, 0x253C, 0x2580, 0x2584, 0x2588, 0x258C, 0x2590,
            0x2591, 0x2592, 0x2593, 0x2320, 0x25A0, 0x2219, 0x221A, 0x2248,
            0x2264, 0x2265, 0x00A0, 0x2321, 0x00B0, 0x00B2, 0x00B7, 0x00F7,
            0x2550, 0x2551, 0x2552, 0x0451, 0x0454, 0x2554, 0x0456, 0x0457,
            0x2557, 0x2558, 0x2559, 0x255A, 0x255B, 0x0491, 0x255D, 0x255E,
            0x255F, 0x2560, 0x2561, 0x0401, 0x0404, 0x2563, 0x0406, 0x0407,
            0x2566, 0x2567, 0x2568, 0x2569, 0x256A, 0x0490, 0x256C, 0x00A9,
            0x044E, 0x0430, 0x0431, 0x0446, 0x0434, 0x0435, 0x0444, 0x0433,
            0x0445, 0x0438, 0x0439, 0x043A, 0x043B, 0x043C, 0x043D, 0x043E,
            0x043F, 0x044F, 0x0440, 0x0441, 0x0442, 0x0443, 0x0436, 0x0432,
            0x044C, 0x044B, 0x0437, 0x0448, 0x044D, 0x0449, 0x0447, 0x044A,
            0x042E, 0x0410, 0x0411, 0x0426, 0x0414, 0x0415, 0x0424, 0x0413,
            0x0425, 0x0418, 0x0419, 0x041A, 0x041B, 0x041C, 0x041D, 0x041E,
            0x041F, 0x042F, 0x0420, 0x0421, 0x0422, 0x0423, 0x0416, 0x0412,
            0x042C, 0x042B, 0x0417, 0x0428, 0x042D, 0x0429, 0x0427, 0x042A]),
        ISO8859_1: new Uint16Array([    // ISO8859_1, CP819
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x00FF, 0x00D6, 0x00DC, 0x00A2, 0x00A3, 0x00A5, 0x20A7, 0x0192,
            0x00A0, 0x00A1, 0x00A2, 0x00A3, 0x00A4, 0x00A5, 0x00A6, 0x00A7,
            0x00A8, 0x00A9, 0x00AA, 0x00AB, 0x00AC, 0x2010, 0x00AE, 0x00AF,
            0x00B0, 0x00B1, 0x00B2, 0x00B3, 0x00B4, 0x00B5, 0x00B6, 0x00B7,
            0x00B8, 0x00B9, 0x00BA, 0x00BB, 0x00BC, 0x00BD, 0x00BE, 0x00BF,
            0x00C0, 0x00C1, 0x00C2, 0x00C3, 0x00C4, 0x00C5, 0x00C6, 0x00C7,
            0x00C8, 0x00C9, 0x00CA, 0x00CB, 0x00CC, 0x00CD, 0x00CE, 0x00CF,
            0x00D0, 0x00D1, 0x00D2, 0x00D3, 0x00D4, 0x00D5, 0x00D6, 0x00D7,
            0x00D8, 0x00D9, 0x00DA, 0x00DB, 0x00DC, 0x00DD, 0x00DE, 0x00DF,
            0x00E0, 0x00E1, 0x00E2, 0x00E3, 0x00E4, 0x00E5, 0x00E6, 0x00E7,
            0x00E8, 0x00E9, 0x00EA, 0x00EB, 0x00EC, 0x00ED, 0x00EE, 0x00EF,
            0x00F0, 0x00F1, 0x00F2, 0x00F3, 0x00F4, 0x00F5, 0x00F6, 0x00F7,
            0x00F8, 0x00F9, 0x00FA, 0x00FB, 0x00FC, 0x00FD, 0x00FE, 0x00FF]),
        ISO8859_2: new Uint16Array([    // ISO8859_2 [3]
            0x2500, 0x2502, 0x250C, 0x2510, 0x2514, 0x2518, 0x251C, 0x2524,
            0x252C, 0x2534, 0x253C, 0x2580, 0x2584, 0x2588, 0x258C, 0x2590,
            0x2591, 0x2592, 0x2593, 0x2320, 0x25A0, 0x2219, 0x221A, 0x2248,
            0x2264, 0x2265, 0x00A0, 0x2321, 0x00B0, 0x00B2, 0x00B7, 0x00F7,
            0x00A0, 0x0104, 0x02D8, 0x0141, 0x00A4, 0x013D, 0x015A, 0x00A7,
            0x00A8, 0x0160, 0x015E, 0x0164, 0x0179, 0x2010, 0x017D, 0x017B,
            0x00B0, 0x0105, 0x02DB, 0x0142, 0x00B4, 0x013E, 0x015B, 0x02C7,
            0x00B8, 0x0161, 0x015F, 0x0165, 0x017A, 0x02DD, 0x017E, 0x017C,
            0x0154, 0x00C1, 0x00C2, 0x0102, 0x00C4, 0x0139, 0x0106, 0x00C7,
            0x010C, 0x00C9, 0x0118, 0x00CB, 0x011A, 0x00CD, 0x00CE, 0x010E,
            0x0110, 0x0143, 0x0147, 0x00D3, 0x00D4, 0x0150, 0x00D6, 0x00D7,
            0x0158, 0x016E, 0x00DA, 0x0170, 0x00DC, 0x00DD, 0x0162, 0x00DF,
            0x0155, 0x00E1, 0x00E2, 0x0103, 0x00E4, 0x013A, 0x0107, 0x00E7,
            0x010D, 0x00E9, 0x0119, 0x00EB, 0x011B, 0x00ED, 0x00EE, 0x010F,
            0x0111, 0x0144, 0x0148, 0x00F3, 0x00F4, 0x0151, 0x00F6, 0x00F7,
            0x0159, 0x016F, 0x00FA, 0x0171, 0x00FC, 0x00FD, 0x0163, 0x02D9]),
        ISO8859_3: new Uint16Array([    // ISO8859_3, CP28593
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x00FF, 0x00D6, 0x00DC, 0x00A2, 0x00A3, 0x00A5, 0x20A7, 0x0192,
            0x00A0, 0x0126, 0x02D8, 0x00A3, 0x00A4, _BLANK, 0x0124, 0x00A7,
            0x00A8, 0x0130, 0x015E, 0x011E, 0x0134, 0x2010, _BLANK, 0x017B,
            0x00B0, 0x0127, 0x00B2, 0x00B3, 0x00B4, 0x00B5, 0x0125, 0x00B7,
            0x00B8, 0x0131, 0x015F, 0x011F, 0x0135, 0x00BD, _BLANK, 0x017C,
            0x00C0, 0x00C1, 0x00C2, _BLANK, 0x00C4, 0x010A, 0x0108, 0x00C7,
            0x00C8, 0x00C9, 0x00CA, 0x00CB, 0x00CC, 0x00CD, 0x00CE, 0x00CF,
            _BLANK, 0x00D1, 0x00D2, 0x00D3, 0x00D4, 0x0120, 0x00D6, 0x00D7,
            0x011C, 0x00D9, 0x00DA, 0x00DB, 0x00DC, 0x016C, 0x015C, 0x00DF,
            0x00E0, 0x00E1, 0x00E2, _BLANK, 0x00E4, 0x010B, 0x0109, 0x00E7,
            0x00E8, 0x00E9, 0x00EA, 0x00EB, 0x00EC, 0x00ED, 0x00EE, 0x00EF,
            _BLANK, 0x00F1, 0x00F2, 0x00F3, 0x00F4, 0x0121, 0x00F6, 0x00F7,
            0x011D, 0x00F9, 0x00FA, 0x00FB, 0x00FC, 0x016D, 0x015D, 0x02D9]),
        ISO8859_4: new Uint16Array([    // ISO8859_4, CP28594 [4,13]
            0x2500, 0x2502, 0x250C, 0x2510, 0x2514, 0x2518, 0x251C, 0x2524,
            0x252C, 0x2534, 0x253C, 0x2580, 0x2584, 0x2588, 0x258C, 0x2590,
            0x2591, 0x2592, 0x2593, 0x2320, 0x25A0, 0x2219, 0x221A, 0x2248,
            0x2264, 0x2265, 0x00A0, 0x2321, 0x00B0, 0x00B2, 0x00B7, 0x00F7,
            0x00A0, 0x0104, 0x0138, 0x0156, 0x00A4, 0x0128, 0x013B, 0x00A7,
            0x00A8, 0x0160, 0x0112, 0x0122, 0x0166, 0x2010, 0x017D, 0x00AF,
            0x00B0, 0x0105, 0x02DB, 0x0157, 0x00B4, 0x0129, 0x013C, 0x02C7,
            0x00B8, 0x0161, 0x0113, 0x0123, 0x0167, 0x014A, 0x017E, 0x014B,
            0x0100, 0x00C1, 0x00C2, 0x00C3, 0x00C4, 0x00C5, 0x00C6, 0x012E,
            0x010C, 0x00C9, 0x0118, 0x00CB, 0x0116, 0x00CD, 0x00CE, 0x012A,
            0x0110, 0x0145, 0x014C, 0x0136, 0x00D4, 0x00D5, 0x00D6, 0x00D7,
            0x00D8, 0x0172, 0x00DA, 0x00DB, 0x00DC, 0x0168, 0x016A, 0x00DF,
            0x0101, 0x00E1, 0x00E2, 0x00E3, 0x00E4, 0x00E5, 0x00E6, 0x012F,
            0x010D, 0x00E9, 0x0119, 0x00EB, 0x0117, 0x00ED, 0x00EE, 0x012B,
            0x0111, 0x0146, 0x014D, 0x0137, 0x00F4, 0x00F5, 0x00F6, 0x00F7,
            0x00F8, 0x0173, 0x00FA, 0x00FB, 0x00FC, 0x0169, 0x016B, 0x02D9]),
        ISO8859_5: new Uint16Array([        // ISO 8859_5 - cyrillic
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x00FF, 0x00D6, 0x00DC, 0x00A2, 0x00A3, 0x00A5, 0x20A7, 0x0192,
            0x00A0, 0x0401, 0x0402, 0x0403, 0x0404, 0x0405, 0x0406, 0x0407,
            0x0408, 0x0409, 0x040A, 0x040B, 0x040C, 0x2010, 0x040E, 0x040F,
            0x0410, 0x0411, 0x0412, 0x0413, 0x0414, 0x0415, 0x0416, 0x0417,
            0x0418, 0x0419, 0x041A, 0x041B, 0x041C, 0x041D, 0x041E, 0x041F,
            0x0420, 0x0421, 0x0422, 0x0423, 0x0424, 0x0425, 0x0426, 0x0427,
            0x0428, 0x0429, 0x042A, 0x042B, 0x042C, 0x042D, 0x042E, 0x042F,
            0x0430, 0x0431, 0x0432, 0x0433, 0x0434, 0x0435, 0x0436, 0x0437,
            0x0438, 0x0439, 0x043A, 0x043B, 0x043C, 0x043D, 0x043E, 0x043F,
            0x0440, 0x0441, 0x0442, 0x0443, 0x0444, 0x0445, 0x0446, 0x0447,
            0x0448, 0x0449, 0x044A, 0x044B, 0x044C, 0x044D, 0x044E, 0x044F,
            0x2116, 0x0451, 0x0452, 0x0453, 0x0454, 0x0455, 0x0456, 0x0457,
            0x0458, 0x0459, 0x045A, 0x045B, 0x045C, 0x00A7, 0x045E, 0x045F]),
        ISO8859_7: new Uint16Array([        // ISO 8859_7 - greek
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x00FF, 0x00D6, 0x00DC, 0x00A2, 0x00A3, 0x00A5, 0x20A7, 0x0192,
            0x00A0, 0x2018, 0x2019, 0x00A3, 0x20AC, 0x20AF, 0x00A6, 0x00A7,
            0x00A8, 0x00A9, 0x037A, 0x00AB, 0x00AC, 0x2010, _BLANK, 0x2015,
            0x00B0, 0x00B1, 0x00B2, 0x00B3, 0x0384, 0x0385, 0x0386, 0x00B7,
            0x0388, 0x0389, 0x038A, 0x00BB, 0x038C, 0x00BD, 0x038E, 0x038F,
            0x0390, 0x0391, 0x0392, 0x0393, 0x0394, 0x0395, 0x0396, 0x0397,
            0x0398, 0x0399, 0x039A, 0x039B, 0x039C, 0x039D, 0x039E, 0x039F,
            0x03A0, 0x03A1, _BLANK, 0x03A3, 0x03A4, 0x03A5, 0x03A6, 0x03A7,
            0x03A8, 0x03A9, 0x03AA, 0x03AB, 0x03AC, 0x03AD, 0x03AE, 0x03AF,
            0x03B0, 0x03B1, 0x03B2, 0x03B3, 0x03B4, 0x03B5, 0x03B6, 0x03B7,
            0x03B8, 0x03B9, 0x03BA, 0x03BB, 0x03BC, 0x03BD, 0x03BE, 0x03BF,
            0x03C0, 0x03C1, 0x03C2, 0x03C3, 0x03C4, 0x03C5, 0x03C6, 0x03C7,
            0x03C8, 0x03C9, 0x03CA, 0x03CB, 0x03CC, 0x03CD, 0x03CE, _BLANK]),
        ISO8859_9: new Uint16Array([        // ISO 8859_9 - turkish
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x00FF, 0x00D6, 0x00DC, 0x00A2, 0x00A3, 0x00A5, 0x20A7, 0x0192,
            0x00A0, 0x00A1, 0x00A2, 0x00A3, 0x00A4, 0x00A5, 0x00A6, 0x00A7,
            0x00A8, 0x00A9, 0x00AA, 0x00AB, 0x00AC, _SHY,   0x00AE, 0x00AF,
            0x00B0, 0x00B1, 0x00B2, 0x00B3, 0x00B4, 0x00B5, 0x00B6, 0x00B7,
            0x00B8, 0x00B9, 0x00BA, 0x00BB, 0x00BC, 0x00BD, 0x00BE, 0x00BF,
            0x00C0, 0x00C1, 0x00C2, 0x00C3, 0x00C4, 0x00C5, 0x00C6, 0x00C7,
            0x00C8, 0x00C9, 0x00CA, 0x00CB, 0x00CC, 0x00CD, 0x00CE, 0x00CF,
            0x011E, 0x00D1, 0x00D2, 0x00D3, 0x00D4, 0x00D5, 0x00D6, 0x00D7,
            0x00D8, 0x00D9, 0x00DA, 0x00DB, 0x00DC, 0x0130, 0x015E, 0x00DF,
            0x00E0, 0x00E1, 0x00E2, 0x00E3, 0x00E4, 0x00E5, 0x00E6, 0x00E7,
            0x00E8, 0x00E9, 0x00EA, 0x00EB, 0x00EC, 0x00ED, 0x00EE, 0x00EF,
            0x011F, 0x00F1, 0x00F2, 0x00F3, 0x00F4, 0x00F5, 0x00F6, 0x00F7,
            0x00F8, 0x00F9, 0x00FA, 0x00FB, 0x00FC, 0x0131, 0x015F, 0x00FF]),
        ISO8859_10: new Uint16Array([       // ISO 8859_10 - nordic
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x00FF, 0x00D6, 0x00DC, 0x00A2, 0x00A3, 0x00A5, 0x20A7, 0x0192,
            0x00A0, 0x0104, 0x0112, 0x0122, 0x012A, 0x0128, 0x0136, 0x00A7,
            0x013B, 0x0110, 0x0160, 0x0166, 0x017D, _SHY,   0x016A, 0x014A,
            0x00B0, 0x0105, 0x0113, 0x0123, 0x012B, 0x0129, 0x0137, 0x00B7,
            0x013C, 0x0111, 0x0161, 0x0167, 0x017E, 0x2015, 0x016B, 0x014B,
            0x0100, 0x00C1, 0x00C2, 0x00C3, 0x00C4, 0x00C5, 0x00C6, 0x012E,
            0x010C, 0x00C9, 0x0118, 0x00CB, 0x0116, 0x00CD, 0x00CE, 0x00CF,
            0x00D0, 0x0145, 0x014C, 0x00D3, 0x00D4, 0x00D5, 0x00D6, 0x0168,
            0x00D8, 0x0172, 0x00DA, 0x00DB, 0x00DC, 0x00DD, 0x00DE, 0x00DF,
            0x0101, 0x00E1, 0x00E2, 0x00E3, 0x00E4, 0x00E5, 0x00E6, 0x012F,
            0x010D, 0x00E9, 0x0119, 0x00EB, 0x0117, 0x00ED, 0x00EE, 0x00EF,
            0x00F0, 0x0146, 0x014D, 0x00F3, 0x00F4, 0x00F5, 0x00F6, 0x0169,
            0x00F8, 0x0173, 0x00FA, 0x00FB, 0x00FC, 0x00FD, 0x00FE, 0x0138]),
        ISO8859_13: new Uint16Array([       // ISO 8859_13 - baltic
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x00FF, 0x00D6, 0x00DC, 0x00A2, 0x00A3, 0x00A5, 0x20A7, 0x0192,
            0x00A0, 0x201D, 0x00A2, 0x00A3, 0x00A4, 0x201E, 0x00A6, 0x00A7,
            0x00D8, 0x00A9, 0x0156, 0x00AB, 0x00AC, _SHY,   0x00AE, 0x00C6,
            0x00B0, 0x00B1, 0x00B2, 0x00B3, 0x201C, 0x00B5, 0x00B6, 0x00B7,
            0x00F8, 0x00B9, 0x0157, 0x00BB, 0x00BC, 0x00BD, 0x00BE, 0x00E6,
            0x0104, 0x012E, 0x0100, 0x0106, 0x00C4, 0x00C5, 0x0118, 0x0112,
            0x010C, 0x00C9, 0x0179, 0x0116, 0x0122, 0x0136, 0x012A, 0x013B,
            0x0160, 0x0143, 0x0145, 0x00D3, 0x014C, 0x00D5, 0x00D6, 0x00D7,
            0x0172, 0x0141, 0x015A, 0x016A, 0x00DC, 0x017B, 0x017D, 0x00DF,
            0x0105, 0x012F, 0x0101, 0x0107, 0x00E4, 0x00E5, 0x0119, 0x0113,
            0x010D, 0x00E9, 0x017A, 0x0117, 0x0123, 0x0137, 0x012B, 0x013C,
            0x0161, 0x0144, 0x0146, 0x00F3, 0x014D, 0x00F5, 0x00F6, 0x00F7,
            0x0173, 0x0142, 0x015B, 0x016B, 0x00FC, 0x017C, 0x017E, 0x2019]),
        ISO8859_14: new Uint16Array([       // ISO 8859_14 - celtic
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x00FF, 0x00D6, 0x00DC, 0x00A2, 0x00A3, 0x00A5, 0x20A7, 0x0192,
            0x00A0, 0x1E02, 0x1E03, 0x00A3, 0x010A, 0x010B, 0x1E0A, 0x00A7,
            0x1E80, 0x00A9, 0x1E82, 0x1E0B, 0x1EF2, _SHY,   0x00AE, 0x0178,
            0x1E1E, 0x1E1F, 0x0120, 0x0121, 0x1E40, 0x1E41, 0x00B6, 0x1E56,
            0x1E81, 0x1E57, 0x1E83, 0x1E60, 0x1EF3, 0x1E84, 0x1E85, 0x1E61,
            0x00C0, 0x00C1, 0x00C2, 0x00C3, 0x00C4, 0x00C5, 0x00C6, 0x00C7,
            0x00C8, 0x00C9, 0x00CA, 0x00CB, 0x00CC, 0x00CD, 0x00CE, 0x00CF,
            0x0174, 0x00D1, 0x00D2, 0x00D3, 0x00D4, 0x00D5, 0x00D6, 0x1E6A,
            0x00D8, 0x00D9, 0x00DA, 0x00DB, 0x00DC, 0x00DD, 0x0176, 0x00DF,
            0x00E0, 0x00E1, 0x00E2, 0x00E3, 0x00E4, 0x00E5, 0x00E6, 0x00E7,
            0x00E8, 0x00E9, 0x00EA, 0x00EB, 0x00EC, 0x00ED, 0x00EE, 0x00EF,
            0x0175, 0x00F1, 0x00F2, 0x00F3, 0x00F4, 0x00F5, 0x00F6, 0x1E6B,
            0x00F8, 0x00F9, 0x00FA, 0x00FB, 0x00FC, 0x00FD, 0x0177, 0x00FF]),
        ISO8859_15: new Uint16Array([       // ISO 8859_15 - western european
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x00FF, 0x00D6, 0x00DC, 0x00A2, 0x00A3, 0x00A5, 0x20A7, 0x0192,
            0x00A0, 0x00A1, 0x00A2, 0x00A3, 0x20AC, 0x00A5, 0x0160, 0x00A7,
            0x0161, 0x00A9, 0x00AA, 0x00AB, 0x00AC, _SHY,   0x00AE, 0x00AF,
            0x00B0, 0x00B1, 0x00B2, 0x00B3, 0x017D, 0x00B5, 0x00B6, 0x00B7,
            0x017E, 0x00B9, 0x00BA, 0x00BB, 0x0152, 0x0153, 0x0178, 0x00BF,
            0x00C0, 0x00C1, 0x00C2, 0x00C3, 0x00C4, 0x00C5, 0x00C6, 0x00C7,
            0x00C8, 0x00C9, 0x00CA, 0x00CB, 0x00CC, 0x00CD, 0x00CE, 0x00CF,
            0x00D0, 0x00D1, 0x00D2, 0x00D3, 0x00D4, 0x00D5, 0x00D6, 0x00D7,
            0x00D8, 0x00D9, 0x00DA, 0x00DB, 0x00DC, 0x00DD, 0x00DE, 0x00DF,
            0x00E0, 0x00E1, 0x00E2, 0x00E3, 0x00E4, 0x00E5, 0x00E6, 0x00E7,
            0x00E8, 0x00E9, 0x00EA, 0x00EB, 0x00EC, 0x00ED, 0x00EE, 0x00EF,
            0x00F0, 0x00F1, 0x00F2, 0x00F3, 0x00F4, 0x00F5, 0x00F6, 0x00F7,
            0x00F8, 0x00F9, 0x00FA, 0x00FB, 0x00FC, 0x00FD, 0x00FE, 0x00FF]),
        ISO8859_16: new Uint16Array([       // ISO 8859_16 - southeaster european
            0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
            0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
            0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
            0x00FF, 0x00D6, 0x00DC, 0x00A2, 0x00A3, 0x00A5, 0x20A7, 0x0192,
            0x00A0, 0x0104, 0x0105, 0x0141, 0x20AC, 0x201E, 0x0160, 0x00A7,
            0x0161, 0x00A9, 0x0218, 0x00AB, 0x0179, _SHY,   0x017A, 0x017B,
            0x00B0, 0x00B1, 0x010C, 0x0142, 0x017D, 0x201D, 0x00B6, 0x00B7,
            0x017E, 0x010D, 0x0219, 0x00BB, 0x0152, 0x0153, 0x0178, 0x017C,
            0x00C0, 0x00C1, 0x00C2, 0x0102, 0x00C4, 0x0106, 0x00C6, 0x00C7,
            0x00C8, 0x00C9, 0x00CA, 0x00CB, 0x00CC, 0x00CD, 0x00CE, 0x00CF,
            0x0110, 0x0143, 0x00D2, 0x00D3, 0x00D4, 0x0150, 0x00D6, 0x015A,
            0x0170, 0x00D9, 0x00DA, 0x00DB, 0x00DC, 0x0118, 0x021A, 0x00DF,
            0x00E0, 0x00E1, 0x00E2, 0x0103, 0x00E4, 0x0107, 0x00E6, 0x00E7,
            0x00E8, 0x00E9, 0x00EA, 0x00EB, 0x00EC, 0x00ED, 0x00EE, 0x00EF,
            0x0111, 0x0144, 0x00F2, 0x00F3, 0x00F4, 0x0151, 0x00F6, 0x015B,
            0x0171, 0x00F9, 0x00FA, 0x00FB, 0x00FC, 0x0119, 0x021B, 0x00FF]),
        ARMSCII_8: new Uint16Array([    // ArmSCI-8
            0x00a0, 0x058E, 0x0587, 0x0589, 0x0029, 0x0028, 0x00bb, 0x00a8,
            0x2014, 0x00b7, 0x0559, 0x055d, 0x002d, 0x055f, 0x2026, 0x055c,
            0x055b, 0x055e, 0x0531, 0x0561, 0x0532, 0x0562, 0x0533, 0x0563,
            0x0534, 0x0564, 0x0535, 0x0565, 0x0536, 0x0566, 0x0537, 0x0567,
            0x0538, 0x0568, 0x0539, 0x0569, 0x053a, 0x056a, 0x053b, 0x056b,
            0x053c, 0x056c, 0x053d, 0x056d, 0x053e, 0x056e, 0x053f, 0x056f,
            0x0540, 0x0570, 0x0541, 0x0571, 0x0542, 0x0572, 0x0543, 0x0573,
            0x0544, 0x0574, 0x0545, 0x0575, 0x0546, 0x0576, 0x0547, 0x0577,
            0x0548, 0x0578, 0x0549, 0x0579, 0x054a, 0x057a, 0x054b, 0x057b,
            0x054c, 0x057c, 0x054d, 0x057d, 0x054e, 0x057e, 0x054f, 0x057f,
            0x0550, 0x0580, 0x0551, 0x0581, 0x0552, 0x0582, 0x0553, 0x0583,
            0x0554, 0x0584, 0x0555, 0x0585, 0x0556, 0x0586, 0x055a, _BLANK]),
        HAIK8: new Uint16Array([    // HAIK8
            0x00a0, 0x058E, 0x0587, 0x0589, 0x0029, 0x0028, 0x00bb, 0x00a8,
            0x2014, 0x00b7, 0x0559, 0x055d, 0x002d, 0x055f, 0x2026, 0x055c,
            0x055b, 0x055e, 0x0531, 0x0561, 0x0532, 0x0562, 0x0533, 0x0563,
            0x0534, 0x0564, 0x0535, 0x0565, 0x0536, 0x0566, 0x0537, 0x0567,
            0x0538, 0x0568, 0x0539, 0x0569, 0x053a, 0x056a, 0x053b, 0x056b,
            0x053c, 0x056c, 0x053d, 0x056d, 0x053e, 0x056e, 0x053f, 0x056f,
            0x0540, 0x0570, 0x0541, 0x0571, 0x0542, 0x0572, 0x0543, 0x0573,
            0x0544, 0x0574, 0x0545, 0x0575, 0x0546, 0x0576, 0x0547, 0x0577,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            0x0548, 0x0578, 0x0549, 0x0579, 0x054a, 0x057a, 0x054b, 0x057b,
            0x054c, 0x057c, 0x054d, 0x057d, 0x054e, 0x057e, 0x054f, 0x057f,
            0x0550, 0x0580, 0x0551, 0x0581, 0x0552, 0x0582, 0x0553, 0x0583,
            0x0554, 0x0584, 0x0555, 0x0585, 0x0556, 0x0586, 0x055a, _BLANK]),
        CP1131: new Uint16Array([   // CP1131 - Belarus
            0x0410, 0x0411, 0x0412, 0x0413, 0x0414, 0x0415, 0x0416, 0x0417,
            0x0418, 0x0419, 0x041a, 0x041b, 0x041c, 0x041d, 0x041e, 0x041f,
            0x0420, 0x0421, 0x0422, 0x0423, 0x0424, 0x0425, 0x0426, 0x0427,
            0x0428, 0x0429, 0x042a, 0x042b, 0x042c, 0x042d, 0x042e, 0x042f,
            0x0430, 0x0431, 0x0432, 0x0433, 0x0434, 0x0435, 0x0436, 0x0437,
            0x0438, 0x0439, 0x043a, 0x043b, 0x043c, 0x043d, 0x043e, 0x043f,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255d, 0x255c, 0x255b, 0x2510,
            0x2514, 0x2534, 0x252c, 0x251c, 0x2500, 0x253c, 0x255e, 0x255f,
            0x255a, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256c, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256b,
            0x256a, 0x2518, 0x250c, 0x2588, 0x2584, 0x258c, 0x2590, 0x2580,
            0x0440, 0x0441, 0x0442, 0x0443, 0x0444, 0x0445, 0x0446, 0x0447,
            0x0448, 0x0449, 0x044a, 0x044b, 0x044c, 0x044d, 0x044e, 0x044f,
            0x0401, 0x0451, 0x0404, 0x0454, 0x0407, 0x0457, 0x040e, 0x045e,
            0x0406, 0x0456, 0x00b7, 0x00a4, 0x0490, 0x0491, 0x2219, 0x00a0]),

        CP862: new Uint16Array([    //) / CP 862, CP916 - hebrew
            0x05D0, 0x05D1, 0x05D2, 0x05D3, 0x05D4, 0x05D5, 0x05D6, 0x05D7,
            0x05D8, 0x05D9, 0x05DA, 0x05DB, 0x05DC, 0x05DD, 0x05DE, 0x05DF,
            0x05E0, 0x05E1, 0x05E2, 0x05E3, 0x05E4, 0x05E5, 0x05E6, 0x05E7,
            0x05E8, 0x05E9, 0x05EA, 0x00A2, 0x00A3, 0x00A5, 0x20A7, 0x0192,
            0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x00F1, 0x00D1, 0x00AA, 0x00BA,
            0x00BF, 0x2310, 0x00AC, 0x00BD, 0x00BC, 0x00A1, 0x00AB, 0x00BB,
            0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
            0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
            0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
            0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
            0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
            0x256A, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
            0x03B1, 0x00DF, 0x0393, 0x03C0, 0x03A3, 0x03C3, 0x00B5, 0x03C4,
            0x03A6, 0x0398, 0x03A9, 0x03B4, 0x221E, 0x03C6, 0x03B5, 0x2229,
            0x2261, 0x00B1, 0x2265, 0x2264, 0x2320, 0x2321, 0x00F7, 0x2248,
            0x00B0, 0x2219, 0x00B7, 0x221A, 0x207F, 0x00B2, 0x25A0, 0x00A0]),
        ISO8859_8 : new Uint16Array([   // ISO 8859-8 : hebrew
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            0x00A0, _BLANK, 0x00A2, 0x00A3, 0x00A4, 0x00A5, 0x00A6, 0x00A7,
            0x00A8, 0x00A9, 0x00D7, 0x00AB, 0x00AC, _SHY,   0x00AE, 0x00AF,
            0x00B0, 0x00B1, 0x00B2, 0x00B3, 0x00B4, 0x00B5, 0x00B6, 0x00B7,
            0x00B8, 0x00B9, 0x00F7, 0x00BB, 0x00BC, 0x00BD, 0x00BE, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, 0x2017,
            0x05D0, 0x05D1, 0x05D2, 0x05D3, 0x05D4, 0x05D5, 0x05D6, 0x05D7,
            0x05D8, 0x05D9, 0x05DA, 0x05DB, 0x05DC, 0x05DD, 0x05DE, 0x05DF,
            0x05E0, 0x05E1, 0x05E2, 0x05E3, 0x05E4, 0x05E5, 0x05E6, 0x05E7,
            0x05E8, 0x05E9, 0x05EA, _BLANK, _BLANK, 0x200E, 0x200F, _BLANK]),
        WIN1255 : new Uint16Array([     // win 1255 - hebrew
            0x20AC, _BLANK, 0x201A, 0x0192, 0x201E, 0x2026, 0x2020, 0x2021,
            0x02C6, 0x2030, _BLANK, 0x2039, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, 0x2018, 0x2019, 0x201C, 0x201D, 0x2022, 0x2013, 0x2014,
            0x02DC, 0x2122, _BLANK, 0x203A, _BLANK, _BLANK, _BLANK, _BLANK,
            0x00A0, 0x00A1, 0x00A2, 0x00A3, 0x20AA, 0x00A5, 0x00A6, 0x00A7,
            0x00A8, 0x00A9, 0x00D7, 0x00AB, 0x00AC, _SHY,   0x00AE, 0x00AF,
            0x00B0, 0x00B1, 0x00B2, 0x00B3, 0x00B4, 0x00B5, 0x00B6, 0x00B7,
            0x00B8, 0x00B9, 0x00F7, 0x00BB, 0x00BC, 0x00BD, 0x00BE, 0x00BF,
            0x05B0, 0x05B1, 0x05B2, 0x05B3, 0x05B4, 0x05B5, 0x05B6, 0x05B7,
            0x05B8, 0x05B9, 0x05BA, 0x05BB, 0x05BC, 0x05BD, 0x05BE, 0x05BF,
            0x05C0, 0x05C1, 0x05C2, 0x05C3, 0x05F0, 0x05F1, 0x05F2, 0x05F3,
            0x05F4, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            0x05D0, 0x05D1, 0x05D2, 0x05D3, 0x05D4, 0x05D5, 0x05D6, 0x05D7,
            0x05D8, 0x05D9, 0x05DA, 0x05DB, 0x05DC, 0x05DD, 0x05DE, 0x05DF,
            0x05E0, 0x05E1, 0x05E2, 0x05E3, 0x05E4, 0x05E5, 0x05E6, 0x05E7,
            0x05E8, 0x05E9, 0x05EA, _BLANK, _BLANK, 0x200E, 0x200F, _BLANK]),
        WIN1256 : new Uint16Array([ // win 1256 - arabic
            0x20AC, 0x067E, 0x201A, 0x0192, 0x201E, 0x2026, 0x2020, 0x2021,
            0x02C6, 0x2030, 0x0679, 0x2039, 0x0152, 0x0686, 0x0698, 0x0688,
            0x06AF, 0x2018, 0x2019, 0x201C, 0x201D, 0x2022, 0x2013, 0x2014,
            0x06A9, 0x2122, 0x0691, 0x203A, 0x0153, 0x200C, 0x200D, 0x06BA,
            0x00A0, 0x060C, 0x00A2, 0x00A3, 0x00A4, 0x00A5, 0x00A6, 0x00A7,
            0x00A8, 0x00A9, 0x06BE, 0x00AB, 0x00AC, _SHY,   0x00AE, 0x00AF,
            0x00B0, 0x00B1, 0x00B2, 0x00B3, 0x00B4, 0x00B5, 0x00B6, 0x00B7,
            0x00B8, 0x00B9, 0x061B, 0x00BB, 0x00BC, 0x00BD, 0x00BE, 0x061F,
            0x06C1, 0x0621, 0x0622, 0x0623, 0x0624, 0x0625, 0x0626, 0x0627,
            0x0628, 0x0629, 0x062A, 0x062B, 0x062C, 0x062D, 0x062E, 0x062F,
            0x0630, 0x0631, 0x0632, 0x0633, 0x0634, 0x0635, 0x0636, 0x00D7,
            0x0637, 0x0638, 0x0639, 0x063A, 0x0640, 0x0641, 0x0642, 0x0643,
            0x00E0, 0x0644, 0x00E2, 0x0645, 0x0646, 0x0647, 0x0648, 0x00E7,
            0x00E8, 0x00E9, 0x00EA, 0x00EB, 0x0649, 0x064A, 0x00EE, 0x00EF,
            0x064B, 0x064C, 0x064D, 0x064E, 0x00F4, 0x064F, 0x0650, 0x00F7,
            0x0651, 0x00F9, 0x0652, 0x00FB, 0x00FC, 0x200E, 0x200F, 0x06D2]),
        ISO8859_6 : new Uint16Array([   // ISO 8859-6 arabic
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            0x00A0, _BLANK, _BLANK, _BLANK, 0x00A4, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, 0x060C, _SHY,   _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, 0x0618, _BLANK, _BLANK, _BLANK, 0x061F,
            _BLANK, 0x0621, 0x0622, 0x0623, 0x0624, 0x0625, 0x0626, 0x0627,
            0x0628, 0x0629, 0x062A, 0x062B, 0x062C, 0x062D, 0x062E, 0x062F,
            0x0630, 0x0631, 0x0632, 0x0633, 0x0634, 0x0635, 0x0636, 0x0637,
            0x0638, 0x0639, 0x063A, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            0x0640, 0x0641, 0x0642, 0x0643, 0x0644, 0x0645, 0x0646, 0x0647,
            0x0648, 0x0649, 0x064A, 0x064B, 0x064C, 0x064D, 0x064E, 0x064F,
            0x0650, 0x0651, 0x0652, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK]),

        CP864 : new Uint16Array([   // CP864 - arabic
            0x00B0, 0x00B7, 0x2219, 0x221A, 0x2592, 0x2500, 0x2502, 0x253C,
            0x2524, 0x252C, 0x251C, 0x2534, 0x2510, 0x250C, 0x2514, 0x2518,
            0x03B2, 0x221E, 0x03C6, 0x00B1, 0x00BD, 0x00BC, 0x2248, 0x00AB,
            0x00BB, 0xFEF7, 0xFEF8, _BLANK, _BLANK, 0xFEFB, 0xFEFC, _BLANK,
            0x00A0, _SHY,   0xFE82, 0x00A3, 0x00A4, 0xFE84, _BLANK, 0x20AC,
            0xFE8E, 0xFE8F, 0xFE95, 0xFE99, 0x060C, 0xFE9D, 0xFEA1, 0xFEA5,
            0x0660, 0x0661, 0x0662, 0x0663, 0x0664, 0x0665, 0x0666, 0x0667,
            0x0668, 0x0669, 0xFED1, 0x061B, 0xFEB1, 0xFEB5, 0xFEB9, 0x061F,
            0x00A2, 0xFE80, 0xFE81, 0xFE83, 0xFE85, 0xFECA, 0xFE8B, 0xFE8D,
            0xFE91, 0xFE93, 0xFE97, 0xFE9B, 0xFE9F, 0xFEA3, 0xFEA7, 0xFEA9,
            0xFEAB, 0xFEAD, 0xFEAF, 0xFEB3, 0xFEB7, 0xFEBB, 0xFEBF, 0xFEC1,
            0xFEC5, 0xFECB, 0xFECF, 0x00A6, 0x00AC, 0x00F7, 0x00D7, 0xFEC9,
            0x0640, 0xFED3, 0xFED7, 0xFEDB, 0xFEDF, 0xFEE3, 0xFEE7, 0xFEEB,
            0xFEED, 0xFEEF, 0xFEF3, 0xFEBD, 0xFECC, 0xFECE, 0xFECD, 0xFEE1,
            0xFE7D, 0x0651, 0xFEE5, 0xFEE9, 0xFEEC, 0xFEF0, 0xFEF2, 0xFED0,
            0xFED5, 0xFEF5, 0xFEF6, 0xFEDD, 0xFED9, 0xFEF1, 0x25A0, _BLANK]),

        TELETEXT: new Uint16Array([ // TELETEXT
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            0x0020, 0x0021, 0x0022, 0x0023, 0x0024, 0x0025, 0x0026, 0x0027,
            0x0028, 0x0029, 0x002A, 0x002B, 0x002C, 0x002D, 0x002E, 0x002F,
            0x0030, 0x0031, 0x0032, 0x0033, 0x0034, 0x0035, 0x0036, 0x0037,
            0x0038, 0x0039, 0x003A, 0x003B, 0x003C, 0x003D, 0x003E, 0x003F,
            0x0040, 0x0041, 0x0042, 0x0043, 0x0044, 0x0045, 0x0046, 0x0047,
            0x0048, 0x0049, 0x004A, 0x004B, 0x004C, 0x004D, 0x004E, 0x004F,
            0x0050, 0x0051, 0x0052, 0x0053, 0x0054, 0x0055, 0x0056, 0x0057,
            0x0058, 0x0059, 0x005A, 0x2190, 0x00BD, 0x2192, 0x2191, 0x2014,
            0x00A3, 0x0061, 0x0062, 0x0063, 0x0064, 0x0065, 0x0066, 0x0067,
            0x0068, 0x0069, 0x006A, 0x006B, 0x006C, 0x006D, 0x006E, 0x006F,
            0x0070, 0x0071, 0x0072, 0x0073, 0x0074, 0x0075, 0x0076, 0x0077,
            0x0078, 0x0079, 0x007A, 0x00BC, 0x2016, 0x00BE, 0x00F7, 0x25A0,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK,
            _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK, _BLANK
        ]),

        RAW: new Uint16Array([   // for RAW converted fonts - mapped to ASCII/PETSCII points
            0xE000, 0xE001, 0xE002, 0xE003, 0xE004, 0xE005, 0xE006, 0xE007,
            0xE008, 0xE009, 0xE00A, 0xE00B, 0xE00C, 0xE00D, 0xE00E, 0xE00F,
            0xE010, 0xE011, 0xE012, 0xE013, 0xE014, 0xE015, 0xE016, 0xE017,
            0xE018, 0xE019, 0xE01A, 0xE01B, 0xE01C, 0xE01D, 0xE01E, 0xE01F,
            0xE020, 0xE021, 0xE022, 0xE023, 0xE024, 0xE025, 0xE026, 0xE027,
            0xE028, 0xE029, 0xE02A, 0xE02B, 0xE02C, 0xE02D, 0xE02E, 0xE02F,
            0xE030, 0xE031, 0xE032, 0xE033, 0xE034, 0xE035, 0xE036, 0xE037,
            0xE038, 0xE039, 0xE03A, 0xE03B, 0xE03C, 0xE03D, 0xE03E, 0xE03F,
            0xE040, 0xE041, 0xE042, 0xE043, 0xE044, 0xE045, 0xE046, 0xE047,
            0xE048, 0xE049, 0xE04A, 0xE04B, 0xE04C, 0xE04D, 0xE04E, 0xE04F,
            0xE050, 0xE051, 0xE052, 0xE053, 0xE054, 0xE055, 0xE056, 0xE057,
            0xE058, 0xE059, 0xE05A, 0xE05B, 0xE05C, 0xE05D, 0xE05E, 0xE05F,
            0xE060, 0xE061, 0xE062, 0xE063, 0xE064, 0xE065, 0xE066, 0xE067,
            0xE068, 0xE069, 0xE06A, 0xE06B, 0xE06C, 0xE06D, 0xE06E, 0xE06F,
            0xE070, 0xE071, 0xE072, 0xE073, 0xE074, 0xE075, 0xE076, 0xE077,
            0xE078, 0xE079, 0xE07A, 0xE07B, 0xE07C, 0xE07D, 0xE07E, 0xE07F,
            0xE080, 0xE081, 0xE082, 0xE083, 0xE084, 0xE085, 0xE086, 0xE087,
            0xE088, 0xE089, 0xE08A, 0xE08B, 0xE08C, 0xE08D, 0xE08E, 0xE08F,
            0xE090, 0xE091, 0xE092, 0xE093, 0xE094, 0xE095, 0xE096, 0xE097,
            0xE098, 0xE099, 0xE09A, 0xE09B, 0xE09C, 0xE09D, 0xE09E, 0xE09F,
            0xE0A0, 0xE0A1, 0xE0A2, 0xE0A3, 0xE0A4, 0xE0A5, 0xE0A6, 0xE0A7,
            0xE0A8, 0xE0A9, 0xE0AA, 0xE0AB, 0xE0AC, 0xE0AD, 0xE0AE, 0xE0AF,
            0xE0B0, 0xE0B1, 0xE0B2, 0xE0B3, 0xE0B4, 0xE0B5, 0xE0B6, 0xE0B7,
            0xE0B8, 0xE0B9, 0xE0BA, 0xE0BB, 0xE0BC, 0xE0BD, 0xE0BE, 0xE0BF,
            0xE0C0, 0xE0C1, 0xE0C2, 0xE0C3, 0xE0C4, 0xE0C5, 0xE0C6, 0xE0C7,
            0xE0C8, 0xE0C9, 0xE0CA, 0xE0CB, 0xE0CC, 0xE0CD, 0xE0CE, 0xE0CF,
            0xE0D0, 0xE0D1, 0xE0D2, 0xE0D3, 0xE0D4, 0xE0D5, 0xE0D6, 0xE0D7,
            0xE0D8, 0xE0D9, 0xE0DA, 0xE0DB, 0xE0DC, 0xE0DD, 0xE0DE, 0xE0DF,
            0xE0E0, 0xE0E1, 0xE0E2, 0xE0E3, 0xE0E4, 0xE0E5, 0xE0E6, 0xE0E7,
            0xE0E8, 0xE0E9, 0xE0EA, 0xE0EB, 0xE0EC, 0xE0ED, 0xE0EE, 0xE0EF,
            0xE0F0, 0xE0F1, 0xE0F2, 0xE0F3, 0xE0F4, 0xE0F5, 0xE0F6, 0xE0F7,
            0xE0F8, 0xE0F9, 0xE0FA, 0xE0FB, 0xE0FC, 0xE0FD, 0xE0FE, 0xE0FF]),
        RAWHI: new Uint16Array([   // for RAW converted fonts - mapped to ASCII
            0x0080, 0x0081, 0x0082, 0x0083, 0x0084, 0x0085, 0x0086, 0x0087,
            0x0088, 0x0089, 0x008A, 0x008B, 0x008C, 0x008D, 0x008E, 0x008F,
            0x0090, 0x0091, 0x0092, 0x0093, 0x0094, 0x0095, 0x0096, 0x0097,
            0x0098, 0x0099, 0x009A, 0x009B, 0x009C, 0x009D, 0x009E, 0x009F,
            0x00A0, 0x00A1, 0x00A2, 0x00A3, 0x00A4, 0x00A5, 0x00A6, 0x00A7,
            0x00A8, 0x00A9, 0x00AA, 0x00AB, 0x00AC, _SHY,   0x00AE, 0x00AF,
            0x00B0, 0x00B1, 0x00B2, 0x00B3, 0x00B4, 0x00B5, 0x00B6, 0x00B7,
            0x00B8, 0x00B9, 0x00BA, 0x00BB, 0x00BC, 0x00BD, 0x00BE, 0x00BF,
            0x00C0, 0x00C1, 0x00C2, 0x00C3, 0x00C4, 0x00C5, 0x00C6, 0x00C7,
            0x00C8, 0x00C9, 0x00CA, 0x00CB, 0x00CC, 0x00CD, 0x00CE, 0x00CF,
            0x00D0, 0x00D1, 0x00D2, 0x00D3, 0x00D4, 0x00D5, 0x00D6, 0x00D7,
            0x00D8, 0x00D9, 0x00DA, 0x00DB, 0x00DC, 0x00DD, 0x00DE, 0x00DF,
            0x00E0, 0x00E1, 0x00E2, 0x00E3, 0x00E4, 0x00E5, 0x00E6, 0x00E7,
            0x00E8, 0x00E9, 0x00EA, 0x00EB, 0x00EC, 0x00ED, 0x00EE, 0x00EF,
            0x00F0, 0x00F1, 0x00F2, 0x00F3, 0x00F4, 0x00F5, 0x00F6, 0x00F7,
            0x00F8, 0x00F9, 0x00FA, 0x00FB, 0x00FC, 0x00FD, 0x00FE, 0x00FF])
    },

    // http://invisible-island.net/xterm/xterm-function-keys.html
    // http://ansi-bbs.org/ansi-bbs2/index.ssjs
    keysS0C0A0 = {  // Normal keys

        0:  0,                  // windows - ie
        8:  function (){        // backspace
                if (cbm)                return 0x14
                else if (atari)         return 0x7E
                else                    return _BS; },
        9:  function (){        // tab
                if (cbm) {
                    // toggle text / gfx modes
                    conFontNum ^= 0x1;
                    renderAll();
                    return 0;
                } else if (atari)       return 0x7F
                else                    return 0x09; },
        12: 0,                  // clear (numpad5 numlk off)
        13: function () {       // enter
                if (atari)              return 0x9B
                else                    return _CR;
            },
        16: 0,                  // shift
        17: null,               // ctrl
        18: 0,                  // alt
        19: 0,                  // pause/break
        20: DO_CAPLK,           // caps lock
        27: function () {       // esc
                if (cbm)                return 0x03 // run/stop
                else                    return _ESC; },
        32: ' ',                // spacebar
        33: function(){         // pgup
                if (modeDOORWAY)        return '\x00\x49'
                else                    return CSI+'V'; },
        34: function(){         // pgdn
                if (modeDOORWAY)        return '\x00\x51'
                else                    return CSI+'U'; },
        35: function(){         // end
                // text mode in cbm
                if (cbm)                return 0x0E
                else if (modeDOORWAY)   return '\x00\x4F'
                else                    return CSI+'K'; },
        36: function () {       // home
                if (cbm)                return 0x13
                else if (modeDOORWAY)   return '\x00\x47'
                else                    return CSI+'H'; },
        37: function () {       // left arrow
                if (cbm)                return 0x9D
                else if (atari)         return 0x1E
                else if (modeDOORWAY)   return '\x00\x4B'
                else                    return CSI+'D'; },
        38: function () {       // up arrow
                if (cbm)                return 0x91
                else if (atari)         return 0x1C
                else if (modeDOORWAY)   return '\x00\x48'
                else                    return CSI+'A'; },
        39: function () {       // right arrow
                if (cbm)                return 0x1D
                else if (atari)         return 0x1F
                else if (modeDOORWAY)   return '\x00\x4D'
                else                    return CSI+'C'; },
        40: function () {       // down arrow
                if (cbm)                return 0x11
                else if (atari)         return 0x1D
                else if (modeDOORWAY)   return '\x00\x50'
                else                    return CSI+'B'; },
        45: function (){        // insert
                if (cbm)                return 0x94
                else if (atari)         return 0xFF
                else if (modeDOORWAY)   return '\x00\x52'
                else                    return CSI+'@'; },
        46: function (){        // delete
                if (cbm)                return 0x14
                else if (atari)         return 0xFE
                else                    return 0x7f; },
        48: '0',                // 0
        49: '1',                // 1
        50: '2',                // 2
        51: '3',                // 3
        52: '4',                // 4
        53: '5',                // 5
        54: '6',                // 6
        55: '7',                // 7
        56: '8',                // 8
        57: '9',                // 9
        59: ';',                // ;: - firefox
        61: '=',                // =+ - firefox
        65: 'a',                // a
        66: 'b',                // b
        67: 'c',                // c - browser copy
        68: 'd',                // d
        69: 'e',                // e
        70: 'f',                // f
        71: 'g',                // g
        72: 'h',                // h
        73: 'i',                // i
        74: 'j',                // j
        75: 'k',                // k
        76: 'l',                // l
        77: 'm',                // m
        78: 'n',                // n - browser new window
        79: 'o',                // o
        80: 'p',                // p
        81: 'q',                // q
        82: 'r',                // r
        83: 's',                // s
        84: 't',                // t - browser new tab
        85: 'u',                // u
        86: 'v',                // v - browser paste
        87: 'w',                // w - browser close window
        88: 'x',                // x
        89: 'y',                // y
        90: 'z',                // z
        91: 0,                  // left win
        92: 0,                  // right win
        93: 0,                  // select
        96: '0',                // numpad0
        97: '1',                // numpad1
        98: '2',                // numpad2
        99: '3',                // numpad3
       100: '4',                // numpad4
       101: '5',                // numpad5
       102: '6',                // numpad6
       103: '7',                // numpad7
       104: '8',                // numpad8
       105: '9',                // numpad9
       106: '*',                // multiply
       107: '+',                // add (use for enter on VT modes)
       109: '-',                // subtract
       110: '.',                // decimal
       111: '/',                // divide
       112: function(){         // f1
                if (cbm)                return 0x85
                else if (modeDOORWAY)   return '\x00\x3B'
                else                    return ESC+'OP'; },
       113: function(){         // f2
                if (cbm)                return 0x89
                else if (modeDOORWAY)   return '\x00\x3C'
                else                    return ESC+'OQ'; },
       114: function(){         // f3
                if (cbm)                return 0x86
                else if (modeDOORWAY)   return '\x00\x3D'
                else                    return ESC+'OR'; },
       115: function(){         // f4
                if (cbm)                return 0x8A
                else if (modeDOORWAY)   return '\x00\x3E'
                else                    return ESC+'OS'; },
       116: function(){         // f5 - browser refresh
                if (cbm)                return 0x87
                else if (modeDOORWAY)   return '\x00\x3F'
                else                    return ESC+'Ot'; },
       117: function(){         // f6
                if (cbm)                return 0x8B
                else if (modeDOORWAY)   return '\x00\x40'
                else                    return CSI+'17~'; },
       118: function(){         // f7
                if (cbm)                return 0x88
                else if (modeDOORWAY)   return '\x00\x41'
                else                    return CSI+'18~'; },
       119: function(){         // f8
                if (cbm)                return 0x8C
                else if (modeDOORWAY)   return '\x00\x42'
                else                    return CSI+'19~'; },
       120: function(){         // f9
                if (modeDOORWAY)        return '\x00\x43'
                else                    return CSI+'20~'; },
       121: function(){         // f10
                if (modeDOORWAY)        return '\x00\x44'
                else                    return CSI+'21~'; },
       122: CSI+'23~',          // f11 - browser full screen
       123: CSI+'24~',          // f12

       124: 0,                  // gui F13
       125: 0,                  // gui F14
       126: 0,                  // gui F15 / Help
       127: 0,                  // gui F16 / Do
       128: 0,                  // gui F17
       129: 0,                  // gui F18
       130: 0,                  // gui F19
       131: 0,                  // gui F20
       132: 0,                  // gui F21
       133: 0,                  // gui F22
       134: 0,                  // gui F23
       135: 0,                  // gui F24
       144: DO_NUMLK,           // numlock
       145: DO_SCRLK,           // scrolllock
       173: '-',                // -_ (firefox)
       186: ';',                // ;:
       187: '=',                // =+
       188: ',',                // ,<
       189: '-',                // -
       190: '.',                // .
       191: '/',                // /
       192: '`',                // `
       219: '[',                // [
       220: '\\',               // '\'
       221: ']',                // ]
       222: '\'',               // '
       255: 0,                  // windows - chrome/opera
    },
    keysS1C0A0 = {  // SHIFTed keys.

         9: function(){         // tab
                if (atari)              return 0x9F     // set tabstop
                else if (modeDOORWAY)   return '\x00\x0F'
                else                    return 0; },
        13: function(){         // enter
                return cbm?0x8d:0; },
        32: '\xa0',             // spacebar

        35: function(){         // end
                // graphics cbm
                return cbm?0x8E:0; },
        36: function(){         // home
                // clr cbm
                if (cbm)            return 0x93
                else if (atari)     return 0x7D
                else                return 0; },

        45: function (){        // insert
                if (atari)              return 0x9D // insert row
                else                    return 0;; },
        46: function (){        // delete
                if (atari)              return 0x9C // delete row
                else                    return 0; },
        48: ')',                // 0
        49: '!',                // 1
        50: '@',                // 2
        51: '#',                // 3
        52: '$',                // 4
        53: '%',                // 5
        54: '^',                // 6
        55: '&',                // 7
        56: '*',                // 8
        57: '(',                // 9
        59: ':',                // ;: - firefox
        61: '+',                // =+ - firefox
        65: 'A',                // a
        66: 'B',                // b
        67: 'C',                // c - browser copy
        68: 'D',                // d
        69: 'E',                // e
        70: 'F',                // f
        71: 'G',                // g
        72: 'H',                // h
        73: 'I',                // i
        74: 'J',                // j
        75: 'K',                // k
        76: 'L',                // l
        77: 'M',                // m
        78: 'N',                // n - browser new window
        79: 'O',                // o
        80: 'P',                // p
        81: 'Q',                // q
        82: 'R',                // r
        83: 'S',                // s
        84: 'T',                // t - browser new tab
        85: 'U',                // u
        86: 'V',                // v - browser paste
        87: 'W',                // w - browser close window
        88: 'X',                // x
        89: 'Y',                // y
        90: 'Z',                // z
       112: function(){         // f1
                return modeDOORWAY?'\x00\x54':0; },
       113: function(){         // f2
                return modeDOORWAY?'\x00\x55':0; },
       114: function(){         // f3
                return modeDOORWAY?'\x00\x56':0; },
       115: function(){         // f4
                return modeDOORWAY?'\x00\x57':0; },
       116: null,               // f5 - browser refresh
       117: function(){         // f6
                return modeDOORWAY?'\x00\x59':0; },
       118: function(){         // f7
                return modeDOORWAY?'\x00\x5A':0; },
       119: function(){         // f8
                return modeDOORWAY?'\x00\x5B':0; },
       120: function(){         // f9
                return modeDOORWAY?'\x00\x5C':0; },
       121: function(){         // f10
                return modeDOORWAY?'\x00\x5D':0; },
       173: '_',                // -_ (firefox)
       186: ':',                // ;:
       187: '+',                // =+
       188: '<',                // ,<
       189: '_',                // -
       190: '>',                // .
       191: '?',                // /
       192: '~',                // `
       219: '{',                // [
       220: '|',                // '\'
       221: '}',                // ]
       222: '"'                 // '
    },
    keysS0C1A0 = {  // CTRLed keys.
         9: function(){         // tab
                if (atari)              return 0x9E // clear tabstop
                else                    return 0; },
        33: function(){         // pgup
                return modeDOORWAY?'\x00\x84':0; },
        34: function(){         // pgdn
                return modeDOORWAY?'\x00\x76':0; },
        35: function(){         // end
                return modeDOORWAY?'\x00\x75':0; },
        36: function(){         // home
                return modeDOORWAY?'/x00/x77':0; },
        37: function(){         // left
                return modeDOORWAY?'\x00\x73':0; },
        39: function(){         // right
                return modeDOORWAY?'\x00\x74':0; },
        48: function(){         // 0
                // rev off
                return cbm?0x92:0; },
        49: function(){          // 1
                // black
                return cbm?0x90:0; },
        50: function(){         // 2
                if (cbm)            return 0x05     // white
                else if (atari)     return 0xFD     // bell
                else                return 0; },
        51: function(){         // 3
                // red
                return cbm?0x1C:0; },
        52: function(){         // 4
                // cyan
                return cbm?0x9f:0; },
        53: function(){         // 5
                // purple
                return cbm?0x9c:0; },
        54: function(){         // 6
                // green
                return cbm?0x1e:0; },
        55: function(){         // 7
                // blue
                return cbm?0x1f:0; },
        56: function(){         // 8
                // yellow
                return cbm?0x9e:0; },
        57: function(){         // 9
                // rev on
                return cbm?0x12:0; },
        65: DO_SELECTALL,       // a
        66: 0x02,               // b
        67: null,               // c - browser copy
        68: 0x04,               // d
        69: 0x05,               // e
        70: 0x06,               // f
        71: 0x07,               // g
        72: 0x08,               // h
        73: 0x09,               // i
        74: 0x0a,               // j
        75: 0x0b,               // k
        76: 0x0c,               // l
        77: 0x0d,               // m
        78: null,               // n - browser new window
        79: 0x0f,               // o
        80: 0x10,               // p
        81: 0x11,               // q
        82: 0x12,               // r
        83: 0x13,               // s
        84: null,               // t - browser new tab
        85: 0x15,               // u
        86: null,               // v - browser paste
        87: null,               // w - browser close window
        88: 0x18,               // x
        89: 0x19,               // y
        90: 0x1a,               // z
       112: function(){         // f1
                return modeDOORWAY?'\x00\x5E':0; },
       113: function(){         // f2
                return modeDOORWAY?'\x00\x5F':0; },
       114: function(){         // f3
                return modeDOORWAY?'\x00\x60':0; },
       115: function(){         // f4
                return modeDOORWAY?'\x00\x61':0; },
       116: null,               // f5 - browser refresh
       117: function(){         // f6
                return modeDOORWAY?'\x00\x63':0; },
       118: function(){         // f7
                return modeDOORWAY?'\x00\x64':0; },
       119: function(){         // f8
                return modeDOORWAY?'\x00\x65':0; },
       120: function(){         // f9
                return modeDOORWAY?'\x00\x66':0; },
       121: function(){         // f10
                return modeDOORWAY?'\x00\x67':0; },
       219: 0x1b,               // [
       220: 0x1c,               // '\'
       221: 0x1d,               // ]
    },
    keysS1C1A0 = {  // SHIFT+CTRL keys. (currently empty)
    },
    keysS0C0A1 = {  // ALTed keys.
        49: function(){             // 1
                // orange
                return cbm?0x81:0; },
        50: function(){             // 2
                // brown
                return cbm?0x95:0; },
        51: function(){             // 3
                // lt red
                return cbm?0x96:0; },
        52: function(){             // 4
                // dk gray
                return cbm?0x97:0; },
        53: function(){             // 5
                // gray
                return cbm?0x98:0; },
        54: function(){             // 6
                // lt green
                return cbm?0x99:0; },
        55: function(){             // 7
                // lt blue
                return cbm?0x9a:0; },
        56: function(){             // 8
                // lt gray
                return cbm?0x9b:0; },
       112: function(){             // F1
                return modeDOORWAY?'\x00\x68':0; },
       113: function(){             // F2
                return modeDOORWAY?'\x00\x69':0; },
       114: function(){             // F3
                return modeDOORWAY?'\x00\x6A':0; },
       115: function(){             // F4
                return modeDOORWAY?'\x00\x6B':0; },
       116: function(){             // F5
                return modeDOORWAY?'\x00\x6C':0; },
       117: function(){             // f6
                return modeDOORWAY?'\x00\x6D':0; },
       118: function(){             // f7
                return modeDOORWAY?'\x00\x6E':0; },
       119: function(){             // f8
                return modeDOORWAY?'\x00\x6F':0; },
       120: function(){             // f9
                return modeDOORWAY?'\x00\x70':0; },
       121: function(){             // f10
                return modeDOORWAY?'\x00\x71':0; },
    },
    keysS1C0A1 = {  // SHIFT+ALTed keus. (currently empty)
    },
    keysS0C1A1 = {  // CTRL+ALTed keys.
        46: null        // delete
    },
    keysS1C1A1 = {  // SHIFT+CTRL+ALTed keys.  (currently empty)
    },

    keyVals = [
        keysS0C0A0, keysS1C0A0, keysS0C1A0, keysS1C1A0,
        keysS0C0A1, keysS1C0A1, keysS0C1A1, keysS1C1A1 ],

    // all available to vtx client
    vtxFonts = [
        'UVGA16', 'MICROKNIGHT', 'MICROKNIGHTPLUS',
        'MOSOUL', 'P0TNOODLE', 'TOPAZ', 'TOPAZPLUS',
        'VIC200', 'VIC201', 'C640', 'C641', 'C1280',
        'C1281', 'ATARI', 'TI994' ],

    // telnet commands
    TN_IS           = 0x00,
    TN_SEND         = 0x01,
    TN_BIN          = 0x00,
    TN_ECHO         = 0x01,
    TN_RECONNECT    = 0x02,
    TN_SGA          = 0x03,
    TN_STATUS       = 0x05,
    TN_TMARK        = 0x06,
    TN_SLOC         = 0x17,
    TN_TTYPE        = 0x18,
    TN_NAWS         = 0x1F,
    TN_TSPEED       = 0x20,
    TN_RFC          = 0x21,
    TN_LM           = 0x22,
    TN_XLOC         = 0x23,
    TN_EVARS        = 0x24,
    TN_AUTH         = 0x25,
    TN_NEWE         = 0x27,
    TN_SE           = 0xF0,
    TN_NOP          = 0xF1,
    TN_DM           = 0xF2,
    TN_BRK          = 0xF3,
    TN_IP           = 0xF4,
    TN_AO           = 0xF5,
    TN_AYT          = 0xF6,
    TN_EC           = 0xF7,
    TN_EL           = 0xF8,
    TN_GA           = 0xF9,
    TN_SB           = 0xFA,
    TN_WILL         = 0xFB,
    TN_WONT         = 0xFC,
    TN_DO           = 0xFD,
    TN_DONT         = 0xFE,
    TN_IAC          = 0xFF,

    // TELNET states for Q method
    // option is enabled ONLY IF state is TNS_YES
    TNQ_NO          = 0,
    TNQ_YES         = 1,
    TNQ_WANTNO      = 2,
    TNQ_WANTYES     = 3,
    TNQ_WANTNO_OP   = 4,
    TNQ_WANTYES_OP  = 5,

    // for ymodem crc16
    CRC_POLY        = 0x1021;

let
    // script path
    vtxPath = '',

    // strings that get transmogrified by the HTTP server.
    // only change these if you are not using the VTX HTTP server.
    codePage = vtxdata.codePage,
    wsConnect = vtxdata.wsConnect,
    crtCols = vtxdata.crtCols,
    crtRows = vtxdata.crtRows,
    xScale = vtxdata.xScale,
    term = vtxdata.term,
    cbm = (vtxdata.term == 'PETSCII'),
    atari = (vtxdata.term == 'ATASCII'),
    initStr = vtxdata.initStr,

    cbmColors,                      // the correct color palette for a PETSCII

    ws = null,                  // websocket connection.

    // timers / intevals
    irqWriteBuffer = null,      // print buffer (33ms)
    irqCheckResize = null,
    irqCursor = null,
    irqBlink = null,

    fontName,                   // font used
    fontSize,                   // font size to use
    vtxFontsLoaded,             // bools of loaded vtxFonts

    rowSize,                    // character size
    colSize,                    // cell width in pixels
    crtWidth,                   // crt width in pixels
    pageWidth,                  // with of html in pixels

    pagePos,
    pageLeft,                   // left position of page div.
    pageTop,                    // top position of page div.

    elPage = document.getElementsByTagName('html')[0],
    crsr,                       // cursor element
    crsrRow,                    // cursor position
    crsrCol,                    // ''
    crsrSaveRow = 0,            // saved position
    crsrSaveCol = 0,            // ''
    cellSaveAttr = 0,           // save attribute (ESC 7 / ESC 8)
    pageAttr,                   // current page attributes
    crsrAttr,                   // color of cursor (only fg used)
    crsrBlink,                  // cursor blink state
    crsrSkipTime,               // skip cursor draws on heavy character output
    cellAttr,                   // current active attributes
    cellBlinkSlow,              // text blink states
    cellBlinkFast,
    defPageAttr,                // default page
    defCrsrAttr,                // default crsr
    defCellAttr,                // default cell attributes.
    defRowAttr = 0x002c0000,    // def row attr
    lastChar,                   // last printable character outputed.
    lastHotSpot = null,         // last mouseover hotspot

    hotspotHoverAttr,           // uses same attrs as cell
    hotspotClickAttr,           // ''

    conBaud = 0,                // baud emulation speed.
    audioEl,                    // audio element
    copyEl,                     // clipboard copy element

    termState,                  // TS_...

    clientDiv = null,           // vtxclient div
    fxDiv = null,               // div for effects. (fade / scanlines)
    pageDiv = null,             // page contents div
    ctrlDiv = null,             // controls panel
    textDiv = null,             // text plane
    soundBell = null,           // bell sound
    soundKeyUp = [],            // press sound
    soundKeyDn = [],            // release sound
    textPos = null,             // ul x,y of textdiv

    // ansi parsing vars
    parms = '',                 // parameters for CSI
    interm = '',                // intermediate for CSI
    ansiState = 0,

    // mode switches
    modeVTXANSI = false,        // CSI ?50 h/l to switch out of old ANSI.SYS mode.
    modeBlinkBright = false,    // CSI ?33 h/l to switch blink for bright background.
    modeCursor = true,          // CSI ?25 h/l to turn cursor on / off.
    modeBoldFont = false,       // CSI ?31 h/l to use font 1 for bold.
    modeNoBold = false,         // CSI ?32 h/l to disallow bold.
    modeBlinkFont = false,      // CSI ?34 h/l to use font 2 for blink.
    modeNoBlink = false,        // CSI ?35 h/l to disallow blink.
    modeCBMShift = true,        // PETSCII shift enabled
    modeDOORWAY = false,        // DOORWAY mode
    modeAutoWrap = true,        // Autowrap Mode
    modeSaveAutoWrap,
    modeNextGlyph = false,      // if DOORWAY mode, print glyph associated with this byte!
    modeRegion = false,         // scrollable region?
    modeRegionOrigin = false,   // origin in region?
    modeTeletext = false,       // teletext bust mode?

    keysDn = [],                // keep track of auto repeat keys
    soundClicks = true,         // keyboard click sounds.
    soundSaveVol = 0,           // save volume on sound off
    soundOn = true,             // sound on?

    // scroll region info.
    regionTopRow,               // top row of scroll region.
    regionBottomRow,            // bottom row of scroll region.

    // display buffer.
    conBuffer = '',             // console output buffer.

    // Attrs are integer arrays, base 0 (i.e.: row 1 = index 0)
    conCanvas = [],             // row canvas array
    conRowAttr  = [],           // row attributes array of number
    conCellAttr = [],           // character attributes array of array or number
    conText = [],               // raw text - array of string
    conHotSpots = [],           // clickable hotspots
    spriteDefs = [],            // sprite definitions - contains url / or data url
    audioDefs = [],             // audio definitions - type 0 coded. 1,2 TODO
    conTabStop = [],            // programmable horizontal tab stops. (used in ATASCII for now)

    // array 0..15 (0-9:ANSI selectable,10/11:special,12-15:reserved)
    conFont = [],               // the 10 fonts used for CSI 10-19 m
    conFontCP = [],             // associated code page for font.
    conFontNum = 0,             // current font being used.

    ovl = {},                   // overlay dialog stuff for file transfers

    // keyboard meta key states
    shiftState, ctrlState, altState,
    numState, capState, scrState,

    tnState,        // telnet read state (IAC cmds)
    tnCmd,          // current telnet command
    tnQUs   = new Uint8Array(256),  // tables for telnet q method
    tnQHim  = new Uint8Array(256),

    // YModem rigmarole
    ymTimer,
    ymCCount,
    ymPacketSize,               // current size of packet.
    ymPacketPos,                // current position in buffer.
    ymPacketBuff = [],          // buffer for send / receive.
    ymFileName,
    ymFileSize,                 // file size. -1 if unknown.
    ymModifiedDate,             // secs since 1/1/1970 in octal
    ymFilePos,                  // current position in file.
    ymFileData = new Blob([], {type: 'application/octet-stream'}),
    ymEOTCount,
    ymNakCount,
    ymNextBlock,
    ymSendStartTime;            // timer for when to abort

// add event listener
function addListener(obj, eventName, listener) {
    if(obj.addEventListener)
        obj.addEventListener(eventName, listener, false)
    else
        obj.attachEvent("on" + eventName, listener);
}

// create an element
function domElement(type, options, styles, txt) {
    var
        e = document.createElement(type),
        i;

    if (options)
        for (i in options)
            e[i] = options[i];
    if (styles)
        for (i in styles)
            e.style[i] = styles[i];
    if (txt)
        e.appendChild(document.createTextNode(txt));
    return e;
}

// keeps google closure compiler from bitching.
function int(val) { return parseInt(val,10); }

// clamp value to range. with optional fallback if out of bounds.
function minMax(v, min, max, fallback) {
    if (fallback == null) {
        if (v < min) v = min;
        if (v > max) v = max;
    } else {
        if (v < min) v = fallback;
        if (v > max) v = fallback;
    }
    return v;
}

function between(mid, low, hi) {
    return (((mid >= low) && (mid <= hi)) ||
            ((mid >= hi) && (mid <= low)));
}

// load fonts and boot
function bootVTX() {
    var
        el, hd = document.getElementsByTagName('head')[0],
        testTimer,
        testDiv,
        i,
        path,
        scripts;

    // get path of script for access to images, etc
    vtxPath = '';
    scripts = document.getElementsByTagName('script');
    for (i = scripts.length - 1; i>=0; i--) {
        if (scripts[i].src.toLowerCase().indexOf('vtxclient') >= 0) {
            path = scripts[i].src.split('?')[0];
            vtxPath = path.split('/').slice(0,-1).join('/')+'/';
            break;
        }
    }

    // force load adobeblank
    el = domElement(
        'style',
        {   type:  'text/css',
            id:     'vtxfonts' }, {},
            '@font-face { \n'
                + '  font-family: "AdobeBlank"; '
                + '  src: url("'+vtxPath+'AdobeBlank.woff") format("woff"); }\n');
    hd.appendChild(el);

    testDiv = domElement('div',{},
                    {   fontFamily:         '"AdobeBlank", sans-serif',
                        fontSize:           '24px',
                        fontWeight:         'normal',
                        color:              'transparent',
                        display:            'inline-block',
                        border:             '0px',
                        padding:            '0px',
                        margin:             '0px' },
                    'Test 1..2..3..');
    document.body.appendChild(testDiv)
    testTimer = setInterval(
        function(){
            var w = testDiv.offsetWidth;
            if (w < 32) {
                document.body.removeChild(testDiv);
                clearInterval(testTimer);
                bootVTX1();
            }
        }, 50);
}

function bootVTX1() {
    var
        i, j, l, str,
        el,
        t = document.getElementsByTagName('title')[0],
        hd = document.getElementsByTagName('head')[0],
        bootFonts,
        bootTimer,
        bootDiv = [];

    // load fonts
    // set fonts to be loaded based on term
    l = vtxFonts.length;
    vtxFontsLoaded = new Array(l);
    for (i = 0; i < l; i++)
        vtxFontsLoaded[i] = false;

    bootFonts = [];
    switch (vtxdata.term) {
        case 'PETSCII':
            switch (vtxdata.codePage) {
                case 'VIC20':
                    bootFonts.push('VIC200');
                    bootFonts.push('VIC201');
                    break;


                case 'C128':
                    bootFonts.push('C1280');
                    bootFonts.push('C1281');
                    break;

                case 'C64':
                default:
                    bootFonts.push('C640');
                    bootFonts.push('C641');
                    break;
            }
            break;

        case 'ATASCII':
            bootFonts.push('ATARI');
            break;

        default:
            bootFonts.push('UVGA16');
            break;
    }

    // inject @font-faces
    el = document.getElementById('vtxfonts');
    str = '';
    for (i = 0, l = bootFonts.length; i < l; i++)
        str += '@font-face {\r\n '
            + '  font-family: "' + bootFonts[i] + '"; '
            + '  src: url("' + vtxPath + bootFonts[i] + '.woff") format("woff"); }\r\n';
    el.innerHTML += str;

    // loop through until all fonts needed at boot.
    for (i = 0, l = bootFonts.length; i < l; i++) {
        bootDiv[i] = domElement('div',{},
                    {   fontFamily:         bootFonts[i] + ', "AdobeBlank"',
                        fontSize:           '24px',
                        fontWeight:         'normal',
                        color:              'transparent',
                        display:            'inline-block',
                        border:             '0px',
                        padding:            '0px',
                        margin:             '0px' },
                    'Test 1..2..3..');
        document.body.appendChild(bootDiv[i]);
    }
    bootTimer = setInterval(
        function(){
            var
                i,
                count = bootFonts.length;

            for (i = bootFonts.length - 1; i >= 0; i--)
                if (bootDiv[i].offsetWidth > 12) {
                    // mark as font loaded.
                    for (j = 0; j < vtxFonts.length; j++)
                        if (vtxFonts[j] == bootFonts[i]) {
                            vtxFontsLoaded[j] = true;
                            break;
                        }
                    count--;
                }

            if (count == 0) {
                for (i = bootFonts.length - 1; i >= 0; i--)
                    document.body.removeChild(bootDiv[i]);
                clearInterval(bootTimer);
                bootVTX2();
            }
        }, 50);
}

function bootVTX2() {
    var
        t = document.getElementsByTagName('title')[0],
        hd = document.getElementsByTagName('head')[0];

    // wait for the required data.
    while (!vtxdata){};

    // format the TITLE tag - only if empty or missing.
    if (!t)
        hd.appendChild(domElement('title',{},{},vtxdata.sysName))
    else {
        if (!t.innerText || (t.innerText == ''))
            t.innerText = vtxdata.sysName
    }

    // when all fonts loaded call initDisplay
    window.setTimeout(initDisplay, 100);
}

function loadSingleFont(fname){
    var
        fn,
        el,
        str,
        hd = document.getElementsByTagName('head')[0],
        fontTimer,
        testDiv;

    // return if it's already loaded.
    fn = vtxFonts.indexOf(fname);
    if ((fn >= 0) && !vtxFontsLoaded[fn]) {
        // inject new @font-faces
        str = '@font-face {\r\n '
            + '  font-family: "' + fname + '"; '
            + '  src: url("' + vtxPath + fname + '.woff") format("woff"); }\r\n';


        el = document.getElementById('vtxfonts');
        if (el)
            el.innerHTML += str
        else
            el = domElement('style', { type: 'text/css' }, {}, str );
        hd.appendChild(el);

        testDiv = domElement('div',{},
                    {   fontFamily:         fname + ', "AdobeBlank"',
                        fontSize:           '24px',
                        fontWeight:         'normal',
                        color:              'transparent',
                        display:            'inline-block',
                        border:             '0px',
                        padding:            '0px',
                        margin:             '0px' },
                    'Test 1..2..3..');
        document.body.appendChild(testDiv);

        fontTimer = setInterval(
            function() {
                var w = testDiv.offsetWidth;
                if (w > 32) {
                    // mark as font loaded.
                    vtxFontsLoaded[fn] = true;
                    document.body.removeChild(testDiv);
                    clearInterval(fontTimer);

                    // redraw everything using this fomt.
                    setTimeout( function() {
                                    redrawFont(fname);
                                }, 250);
                }
            }, 250);
    }
}

// global redraw a font change based on font name (i.e. VIC200, etc)
function redrawFont(fname) {
    var
        fn,
        tfn,
        r, c;

    // convert fname to font number
    fn = conFont.indexOf(fname);
    if (fn >= 0) {
        for (r = conRowAttr.length-1; r >= 0; r--)
            for (c = conCellAttr[r].length - 1; c >= 0; c--) {
                tfn = (conCellAttr[r][c] & A_CELL_FONT_MASK) >>> 28;
                if (tfn == fn)
                    renderCell(r, c, 0, false);
            }
    }
}

// string splice - why this is not standard is beyond me.
// ECMA-262 / 15.4.4.12 - prototype.splice
if (!String.prototype.splice) {
    String.prototype.splice = function (start, deleteCount, item) {
        if (start < 0) {
            start = this.length + start;
            if (start < 0)
                start = 0;
        }
        return this.slice(0, start) + (item || '') + this.slice(start + deleteCount);
    }
}

// which row is the mouse on?
function getMouseCell(e) {
    var
        x, y, width, c, rh, ty, dt, i;

    if (modeFullScreen)
        ty = clientDiv.scrollTop || clientDiv.scrollTop
    else
        ty = document.documentElement.scrollTop || document.body.scrollTop

    x = e.clientX;
    y = e.clientY + ty;
    dt = textPos.top;
    if ((y >= dt) && between(x, textPos.left, textPos.left + crtWidth - 1)) {
        // on the page. find row
        for (i = 0; i < conRowAttr.length; i++) {
            width = getRowAttrWidth(conRowAttr[i]) / 100;
            rh = fontSize;
            if (between(y, dt, dt + rh - 1)) {
                // on this row. get col
                c = (x - textPos.left) / (xScale * colSize * width);
                return { row: i, col: Math.floor(c) };
            }
            dt += rh;
        }
    }
    // off the console
    return null;
}

// get the hotspot under x, y
function getHotSpot(e) {
    var
        i, mpos, hs;

    mpos = getMouseCell(e);
    if (mpos) {
        // adjust to base-1 ansi coords
        for (i = 0; i < conHotSpots.length; i++) {
            hs = conHotSpots[i];
            if (between(mpos.row, hs.row, hs.row + hs.height - 1) &&
                between(mpos.col, hs.col, hs.col + hs.width - 1))
                return conHotSpots[i];
        }
    }
    return null;
}

var
    selectStart =   {},
    selectEnd =     {},
    dragStart =     {},
    dragEnd =       {},
    dragPrev =      {},
    isSelect = false,
    isDrag = false;             // in drag mode?

function loc2Linear(loc) {
    if (loc)
        return (loc.row << 11) + loc.col;
    return null;
}

function linear2Loc(lin) {
    return {row: (lin >>> 11), col: (lin & 0x7FF) };
}

function betweenRCs(loc, loc1, loc2) {
    // is loc.row,loc.col between loc1 and loc2 inclusively?
    return between(loc2Linear(loc), loc2Linear(loc1), loc2Linear(loc2));
}

function refreshBetweenRCs(loc1, loc2) {
    var
        r1, r2, c1, c2;

    if (loc2Linear(loc2) < loc2Linear(loc1)) {
        r1 = loc2.row;
        c1 = loc2.col;
        r2 = loc1.row;
        c2 = loc1.col;
    } else {
        r1 = loc1.row;
        c1 = loc1.col;
        r2 = loc2.row;
        c2 = loc2.col;
    }
    while (true) {
        renderCell(r1, c1);
        if ((r1 == r2) && (c1 == c2))
            break;
        c1++;
        if (c1 >= crtCols) {
            c1 = 0;
            r1++;
        }
    }
}

function mouseUp(e) {
    // for now, just fix meta key states
    e = e || window.event;
    shiftState = e.shiftKey;
    ctrlState = e.ctrlKey;
    altState = e.altKey;

    if (e.button == 0) {
        // end drag
        isSelect = false;
        if (isDrag) {
            if (loc2Linear(dragEnd) > loc2Linear(dragStart)) {
                selectStart = dragStart;
                selectEnd = dragEnd;
            } else {
                selectStart = dragEnd;
                selectEnd = dragStart;
            }
            isSelect = true;
            isDrag = false;
        }
    }
}

function mouseDown(e) {
    var
        fromr, tor,
        p, dir,
        mloc;

    // for now, just fix meta key states
    e = e || window.event;
    shiftState = e.shiftKey;
    ctrlState = e.ctrlKey;
    altState = e.altKey;

    // get mouse pos
    if (e.button == 0) {
        // start drag
        isDrag = false;
        if (isSelect) {
            // erase old selection.
            isSelect = false;
            refreshBetweenRCs(selectStart, selectEnd);
        }
        mloc = getMouseCell(e);
        if (mloc && (mloc != {})) {
            dragStart = mloc;
            dragEnd = mloc;
            dragPrev = mloc;
            isDrag = true;
        }
    }
}

function mouseMove(e) {
    var
        x, y,
        mloc,
        dir, p,
        hs;

    // for now, just fix meta key states
    e = e || window.event;
    shiftState = e.shiftKey;
    ctrlState = e.ctrlKey;
    altState = e.altKey;

    if (termState > TS_NORMAL) return;

    if (isDrag) {
        // dragging
        mloc = getMouseCell(e);
        if (mloc != null) {
            // new location
            dragEnd = mloc;
            refreshBetweenRCs(dragEnd, dragPrev);
            dragPrev = dragEnd;
        }
    } else {
        // moving around
        // check if over a hotspot
        hs = getHotSpot(e);
        if (hs) {
            if (lastHotSpot != hs) {
                if (lastHotSpot) {
                    // erase old
                    for (y = 0; y < lastHotSpot.height; y++)
                        for (x = 0; x < lastHotSpot.width; x++)
                            renderCell(lastHotSpot.row+y, lastHotSpot.col+x);
                }
                // draw this one
                for (y = 0; y < hs.height; y++)
                    for (x = 0; x < hs.width; x++)
                        renderCell(hs.row+y, hs.col+x, hs.hilite?1:0);
            }
            document.body.style['cursor'] = 'pointer'
        } else {
            if (lastHotSpot) {
                // erase old
                for (y = 0; y < lastHotSpot.height; y++)
                    for (x = 0; x < lastHotSpot.width; x++)
                        renderCell(lastHotSpot.row+y, lastHotSpot.col+x);
            }
            document.body.style['cursor'] = 'default';
        }
        lastHotSpot = hs;
    }
}

function click(e) {
    var
        x, y,
        win,
        hs;

    // for now, just fix meta key states
    e = e || window.event;
    shiftState = e.shiftKey;
    ctrlState = e.ctrlKey;
    altState = e.altKey;

    if (termState > TS_NORMAL) return;

    hs = getHotSpot(e);
    if (hs) {
        // draw this one
        for (y = 0; y < hs.height; y++)
            for (x = 0; x < hs.width; x++)
                renderCell(hs.row+y, hs.col+x, hs.hilite?2:0);

        // clicked on hotspot.
        switch (hs.type) {
            case 0:
                sendData(hs.val);
                break;

            case 1:
                // url
                win = window.open(hs.val, '_blank');
                win.focus();
                break;
        }
    }
}

// process keyups (function, arrows, etc)
function keyUp(e) {
    var
        kc;

    e = e || window.event;
    kc = e.keyCode || e.which;

    shiftState = e.shiftKey;
    ctrlState = e.ctrlKey;
    altState = e.altKey;

    if (kc == 17) {
        if (isSelect) {
            isSelect = false;
            refreshBetweenRCs(selectStart, selectEnd);
        }
        copyEl.style['visibility'] = 'hidden';
    }

    // play key up sound
    if (soundClicks)
        soundKeyUp[kc % 11].play();
    keysDn[kc] = false;
}

// process keydowns (function, arrows, etc)
function keyDown(e) {
    var
        stateIdx,
        kc, ka,
        r, c,
        p, endl, str;

    e = e || window.event;
    kc = e.keyCode || e.which;

    shiftState = e.shiftKey;
    ctrlState = e.ctrlKey;
    altState = e.altKey;

    if (termState > TS_NORMAL) return;

    if ((kc == 17) && !keysDn[kc] && isSelect) {
        // copy selected text to clipboard.
        str = '';
        endl = loc2Linear(selectEnd);
        r = selectStart.row;
        c = selectStart.col;
        while (true) {
            if (c < conText[r].length) {
                str += conText[r].charAt(c);
            }
            if (loc2Linear({row:r,col:c}) >= endl)
                break;
            c++;
            if (c >= conText[r].length) {
                str += '\r\n';
                c = 0;
                r++;
            }
        }
        copyEl.style['visibility'] = 'visible';
        copyEl.value = str;
        copyEl.focus();
        copyEl.select();
    }

    // play key down sound
    if (soundClicks && !keysDn[kc])
        soundKeyDn[kc % 11].play();
    keysDn[kc] = true;

    stateIdx = (shiftState ? 1 : 0) + (ctrlState ? 2 : 0) + (altState ? 4 : 0);

    // translate for capslock
    if (between(kc, 65, 90) && (stateIdx < 2) && capState)
        stateIdx ^= 1;

    // reverse upper/lowers for PETSCII
    if (cbm) {
        if (between(kc, 65, 90))
            stateIdx ^= 1;
    }

    ka = keyVals[stateIdx][kc];
    if (ka == null) {
        // let browser handle it.
        return (e.returnValue = true);

    } else if (typeof ka == 'function') {
        ka = ka();
        if (ka == null)
            return (e.returnValue = true);
    }

    if (typeof ka == 'string') {
        // send string to console.
        sendData(ka);
        e.preventDefault();
        return (e.returnValue = false); // true

    } else if (typeof ka == 'number')
    {
        if (ka == 0) {
            // never do anything.
            e.preventDefault();
            return (e.returnValue = true);

        } else if (ka < 0) {
            // perform special action.
            switch (ka) {
                case DO_CAPLK:
                    capState = !capState;
                    setBulbs();
                    break;

                case DO_NUMLK:
                    numState = !numState;
                    setBulbs();
                    break;

                case DO_SCRLK:
                    scrState = !scrState;
                    setBulbs();
                    break;

                case DO_COPY:
                    // create string of selected text.
                    if (isSelect) {
                        isSelect = false;
                        refreshBetweenRCs(selectStart, selectEnd);
                    }
                    break;

                case DO_SELECTALL:
                    break;

                default:
                    // unknown action - pass to keyPress
                    return;
            }
            e.preventDefault();
            return (e.returnValue = false);

        } else if (ka > 0) {
            // send ascii if online, send to server. if offline, localecho
            sendData(ka);
            e.preventDefault();
            return (e.returnValue = false);
        }
    }
}

// process keypresses (alphas, numerics, etc)
function keyPress(e) {
    // normally, send to websocket server. only echo what is returned.
    var
        cc;

    e = e || window.event;
    cc = e.charCode;

    shiftState = e.shiftKey;
    ctrlState = e.ctrlKey;
    altState = e.altKey;

    capState = ((between(cc, 65, 90) && !shiftState)
            || (between(cc, 97, 112) && shiftState));
}

// delete row from storage and element from html
// TODO - check scroll region
function delRow(rownum) {
    var
        els, p;

    expandToRow(rownum);
    els = document.getElementsByClassName('vtx');
    p = els[rownum - 1].parentNode;
    p.removeChild(els[rownum]);
    conRowAttr.splice(rownum, 1);
    conText.splice(rownum, 1);
    conCellAttr.splice(rownum, 1);
    conCanvas.splice(rownum, 1);

    // move all hotspots below up one. remove hotspots on this row.
    clearHotSpotsRow(rownum, 0, 999);
    moveHotSpotsRows(rownum + 1, conRowAttr.length, -1);
}

// insert row into storage and element into html
// TODO - check scroll region
function insRow(rownum) {
    var
        els, p;

    els = document.getElementsByClassName('vtx');
    p = els[rownum].parentNode;
    p.insertBefore(createNewRow(), els[rownum]);
    conRowAttr.splice(rownum, 0, defRowAttr);
    conText.splice(rownum, 0, '');
    conCellAttr.splice(rownum, 0, []);
    conCanvas.splice(rownum, 0, null);

    // move all hotspots on this row and below down one.
    moveHotSpotsRows(rownum, conRowAttr.length, +1);
    trimHistory();
}

// remove excess rows from top.
function trimHistory(){
    var
        els,
        p,
        i,
        hs;

    while (conRowAttr.length > vtxdata.crtHistory) {
        clearHotSpotsRow(0, 0, 999);
        els = document.getElementsByClassName('vtx');
        p = els[0].parentNode;
        p.removeChild(els[0]);
        conRowAttr.splice(0, 1);
        conText.splice(0, 1);
        conCellAttr.splice(0, 1);
        conCanvas.splice(0, 1);
        crsrRow--;
    }
}

// delete a character at position
function delChar(rownum, colnum) {
    expandToRow(rownum);
    expandToCol(rownum, colnum);
    conText[rownum] = conText[rownum].splice(colnum, 1);
    conCellAttr[rownum].splice(colnum, 1);
    moveHotSpotsRow(rownum, colnum, 999, -1);
    redrawRow(rownum);
}

// insert a character at position. also sets attr to def
function insChar(rownum, colnum, chr) {
    expandToRow(rownum);
    expandToCol(rownum, colnum);
    conText[rownum] = conText[rownum].splice(colnum, 0, String.fromCharCode(chr));
    conCellAttr[rownum].splice(colnum, 0, defCellAttr);
    moveHotSpotsRow(rownum, colnum, 999, +1);
    redrawRow(rownum);
}

// create blank row
function createNewRow() {
    var
        cmv, el = domElement('div', { className: 'vtx' });
    //cnv = domElement('canvas');
    //el.appendChild(cnv);
    return el;
}

// compute number of visible cells on this row.
function colsOnRow(rownum) {
    var
        cols = crtCols,
        width = getRowAttrWidth(conRowAttr[rownum]) / 100;

    cols *= 1 / width;
    return cols;
}

// flush out parameter array with defaults.
// truncate to match defs length
function fixParams(parm, defs) {
    var
        i;
    for (i = parm.length; i < defs.length; i++)
        parm[i] = defs[i];
    return parm.slice(0, defs.length);
}

// get actual document position of element
function getElementPosition(obj) {
    var
        lpos = obj.offsetLeft,
        tpos = obj.offsetTop;

    while (obj.offsetParent) {
        obj = obj.offsetParent;
        lpos += obj.offsetLeft;
        tpos += obj.offsetTop;
    }
    return { left: lpos, top: tpos };
}

// redraw the cursor. - attempt to scroll
function crsrDraw() {
    var
        row, rpos,
        csize,
        dt, wh;

    expandToRow(crsrRow);
    row = getRowElement(crsrRow);
    if (row == null) return;

    // position of row in relation to parent.
    rpos = getElementPosition(row);

    // position of doc
    if (modeFullScreen) {
        dt = clientDiv.scrollTop;
        wh = clientDiv.clientHeight;
    } else {
        dt = document.documentElement.scrollTop || document.body.scrollTop;
        wh = window.innerHeight;
    }

    // character size for this row.
    csize = getRowFontSize(crsrRow);

    // set cursor siz / pos
    crsr.style['top'] =     (rpos.top - textPos.top) + 'px';
    crsr.style['left'] =    (xScale * crsrCol * csize.width) + 'px';
    crsr.style['width'] =   (xScale * csize.width) + 'px';
    crsr.style['height'] =  (csize.height) + 'px';

    if (modeFullScreen) {
        if (rpos.top < dt) {
            // cursor is above page - scroll up
            clientDiv.scrollTo = rpos.top - 8;
        } else if ((rpos.top + csize.height) > (dt + wh))  {
            // cursor is below page - scroll down
            clientDiv.scrollTop = rpos.top + csize.height + 8;
        }
    } else {
        if (rpos.top < dt) {
            // cursor is above page - scroll up
            window.scrollTo(0, rpos.top - 8);
        } else if ((rpos.top + csize.height) > (dt + wh)) {
            // cursor is below page - scroll down
            window.scrollTo(0, rpos.top + csize.height + 8);
        }
    }
}

// get default font size for page
function getDefaultFontSize() {
    // look for font-width: in body
    var
        cs, font, textSize, h, w,
        x, y, d, i,
        txtTop, txtBottom, txtLeft, txtRight,
        testString = '',
        data, ctx, canvas,
        bmpw = 2000,
        bmph = 64;

    testString += '\u2588\u2588';
    for (i = 32; i < 128; i++)
        testString += String.fromCharCode(i);
    testString += '\u2588\u2588';

    cs = document.defaultView.getComputedStyle(pageDiv, null);
    fontName = vtxdata.fontName || cs['font-family'];
    fontSize = int(vtxdata.fontSize) || int(cs['font-size']);
    font = fontSize + 'px ' + fontName;

    // interrogate font
    canvas = domElement(
        'canvas',
        {   width:  bmpw,
            height: bmph });
    ctx = canvas.getContext('2d');
    ctx.font = font;
    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    ctx.fillStyle = '#FFF';
    ctx.fillText(testString, 15, 15);

    data = ctx.getImageData(0, 0, bmpw, bmph).data;
    txtTop = txtLeft = bmpw;
    txtRight = txtBottom = 0;
    for (y = 0; y < bmph; y++)
        for (x = 0; x < bmpw; x++) {
            d = data[(y * bmpw+ x) * 4];
            if (d != 0) {
                if (y < txtTop)
                    txtTop = y;
                if (y > txtBottom)
                    txtBottom = y;
                if (x < txtLeft)
                    txtLeft = x;
                if (x > txtRight)
                    txtRight = x;
            }
        }
    //rowSize = Math.floor(txtBottom - txtTop) + 1; // this is normally same as fontSize
    rowSize = fontSize;
    colSize = Math.floor((txtRight - txtLeft) / testString.length);
}

// get maximum row on document.
function getMaxRow() {
    return conRowAttr.length - 1;
}

// get row length for a row
function getMaxCol(rownum) {
    return conText[rownum].length - 1;
}

// home cursor
function XcrsrHome() {
    crsrRow = crsrCol = 0; // base 0
    crsrDraw();
}

// move cursor
function XcrsrMove(rownum, colnum) {
    crsrRow = rownum;
    crsrCol = colnum;
    crsrDraw();
}

// cursor up
function XcrsrUp() {
    if (crsrRow > 0)
        crsrRow--;
    crsrDraw();
}

// cursor left
function XcrsrLeft() {
    if (crsrCol > 0)
        crsrCol--;
    crsrDraw();
}

// cursor right
function XcrsrRight() {
    crsrCol++;
    crsrDraw();
}

// return element for row
function getRowElement(row) {
    var
        els = document.getElementsByClassName('vtx');

    if (row > els.length)
        return null
    return els[row];
}

// called often to fix cursor on zoom / page resize
function doCheckResize() {
    // page resize?
    //textPos = textDiv.getBoundingClientRect();
    textPos = getElementPosition(textDiv);
    if (elPage.clientWidth != pageWidth) {
        pageWidth = elPage.clientWidth;
        crsrDraw(true);
    }
//    ctrlDiv.style['top'] = textPos.top + 'px';
//    ctrlDiv.style['left'] = (6 + textPos.left + (crtWidth*xScale)) + 'px';
}

// blink cursor (533ms is cursor blink speed based on DOS VGA).
function doCursor() {
    if (cbm)
        crsr.firstChild.style['background-color'] =
            ((crsrBlink = !crsrBlink) || (!modeCursor)) ?
            'transparent' :
            cbmColors[cellAttr & 0xF]
    else if(atari)
        crsr.firstChild.style['background-color'] =
            ((crsrBlink = !crsrBlink) || (!modeCursor)) ?
            'transparent' :
            atariColors[cellAttr & 0x1]
    else
        crsr.firstChild.style['background-color'] =
            ((crsrBlink = !crsrBlink) || (!modeCursor)) ?
            'transparent' :
            ansiColors[getCrsrAttrColor(crsrAttr)];
}

// animate blink (533ms)
function doBlink(){
    var
        r, c, y;

    if (!modeNoBlink && !modeBlinkBright) {
        // y of first row.
        y = textDiv.getBoundingClientRect().top;
        for (r = 0; r < conRowAttr.length; r++) {
            // check if row is visible
            if ((y + rowSize > 0) && (y < window.innerHeight)) {
                // look for blink
                // refresh blinkable text.
                for (c = 0; c < conCellAttr[r].length; c++) {
                    if (conCellAttr[r][c] & (A_CELL_BLINKSLOW | A_CELL_BLINKFAST))
                        renderCell(r, c);
                }
            }
            y += rowSize;
        }
    }
    cellBlinkFast = !cellBlinkFast;
    if (cellBlinkFast)
        cellBlinkSlow = !cellBlinkSlow;
}

// compute font size (width) for row - figure in scale (row is dom element)
function getRowFontSize(rownum) {
    var
        w, rattr;

    rattr = conRowAttr[rownum];
    w = colSize * (getRowAttrWidth(rattr) / 100);
    return { width: w, height: rowSize };
}

// concert integer to hex string.
function itoh(i, d) {
    var
        str = '';

    d = d || 0;
    for (; i; str = hex.charAt(i & 0x0F) + str, i >>= 4){};
    if (d > 0)
        while (str.length < d) str = '0' + str;
    return str;
}

// convert hex string to integer
function htoi(h) {
    var
        i, l, v;

    h = h || '0';
    for (h = h.toUpperCase(), v = 0, i = 0, l = h.length; i < l; i++) {
        v <<= 4;
        v += hex.indexOf(h.charAt(i));
    }
    return v;
}

// create row attribute
function makeRowAttr(c1, c2, bp, width, marquee) {
    bp = bp || 0;
    width = width || 100;
    marquee = marquee || false;

    width = minMax(Math.round(width / 50) - 1, 0, 3) << 21;
    return (c1 & 0xFF)
        | ((c2 & 0xFF) << 8)
        | bp
        | size
        | width
        | (marquee ? A_ROW_MARQUEE : 0);
}

function setPageAttrBorder(attr, color) {
    return (attr & 0x00FF) | ((color & 0xFF) << 8);
}
function setPageAttrBackground(attr, color) {
    return (attr & 0xFF00) | (color & 0xFF);
}

// set row attribute parts
function setRowAttrColor1(attr, color1) {
    return (attr & ~A_ROW_COLOR1_MASK) | (color1 & 0xFF);
}
function setRowAttrColor2(attr, color2) {
    return (attr & ~A_ROW_COLOR2_MASK) | ((color2 & 0xFF) << 8);
}
function setRowAttrPattern(attr, pattern) {
    return (attr & ~A_ROW_PATTERN_MASK) | pattern;
}
function setRowAttrWidth(attr, width) {
    // round width to nearest valid 50%
    width = Math.round(width / 50) - 1;
    if (width < 0) width = 0;
    if (width > 3) width = 3;
    width <<= 21;

    return (attr & ~A_ROW_WIDTH_MASK) | width;
}
function setRowAttrMarquee(attr, marquee) {
    return (attr & ~A_ROW_MARQUEE) | (marquee ? A_ROW_MARQUEE : 0);
}

// get row attribute parts
function getRowAttrColor1(attr) { return attr & 0xFF; }
function getRowAttrColor2(attr) { return (attr >>> 8) & 0xFF; }
function getRowAttrPattern(attr) { return attr & A_ROW_PATTERN_MASK; }
function getRowAttrWidth(attr) { return (((attr & A_ROW_WIDTH_MASK) >>> 21) + 1) * 50.0; }
function getRowAttrMarquee(attr) { return (attr & A_ROW_MARQUEE) != 0; }

// create cell attribute
function makeCellAttr(fg, bg, bold, italics, underline, blinkslow, shadow,
    strikethrough, doublestrike, blinkfast, faint, font) {
    fg = fg || 7;
    bg = bg || 0;
    bold = bold || false;
    italics = italics || false;
    underline = underline || false;
    blinkslow = blinkslow || false;
    shadow = shadow || false;
    strikethrough = strikethrough || false;
    doublestrike = online || false;
    blinkfast = blinkfast || false;
    faint = faint || false;
    font = font || 0;


    return (fg & 0xFF)
        | ((bg & 0xFF) << 8)
        | (bold ? A_CELL_BOLD : 0)
        | (italics ? A_CELL_ITALICS : 0)
        | (underline ? A_CELL_UNDERLINE : 0)
        | (blinkslow ? A_CELL_BLINKSLOW : 0)
        | (shadow ? A_CELL_SHADOW : 0)
        | (strikethrough ? A_CELL_STRIKETHROUGH : 0)
        | (doublestrike ? A_CELL_DOUBLESTRIKE : 0)
        | (blinkfast ? A_CELL_BLINKFAST : 0)
        | (faint ? A_CELL_FAINT : 0)
        | ((font & 0xF) << 28);
}

// set cell attribute parts
function setCellAttrFG(attr, color) {
    return (attr & ~A_CELL_FGCOLOR_MASK) | (color & 0xFF);
}
function setCellAttrBG(attr, color) {
    return (attr & ~A_CELL_BGCOLOR_MASK) | ((color & 0xFF) << 8);
}
function setCellAttrBold(attr, bold) {
    return (attr & ~A_CELL_BOLD) | (bold ? A_CELL_BOLD : 0);
}
function setCellAttrItalics(attr, italics) {
    return (attr & ~A_CELL_ITALICS) | (italics? A_CELL_ITALICS : 0);
}
function setCellAttrUnderline(attr, underline) {
    return (attr & ~A_CELL_UNDERLINE) | (underline? A_CELL_UNDERLINE : 0);
}
function setCellAttrBlinkSlow(attr, blink) {
    return (attr & ~A_CELL_BLINKSLOW) | (blink ? A_CELL_BLINKSLOW : 0);
}
function setCellAttrBlinkFast(attr, blink) {
    return (attr & ~A_CELL_BLINKFAST) | (blink ? A_CELL_BLINKFAST : 0);
}
function setCellAttrShadow(attr, shadow) {
    return (attr & ~A_CELL_SHADOW) | (shadow ? A_CELL_SHADOW : 0);
}
function setCellAttrStrikethrough(attr, strikethrough) {
    return (attr & ~A_CELL_STRIKETHROUGH) | (strikethrough ? A_CELL_STRIKETHROUGH : 0);
}
function setCellAttrDoublestrike(attr, doublestrike) {
    return (attr & ~A_CELL_DOUBLESTRIKE) | (doublestrike ? A_CELL_DOUBLESTRIKE: 0);
}
function setCellAttrReverse(attr, reverse) {
    return (attr & ~A_CELL_REVERSE) | (reverse ? A_CELL_REVERSE : 0);
}
function setCellAttrDisplay(attr, display) {
    return (attr & ~A_CELL_DISPLAY_MASK) | (display & A_CELL_DISPLAY_MASK);
}
function setCellAttrFaint(attr, faint) {
    return (attr & ~A_CELL_FAINT) | (faint ? A_CELL_FAINT : 0);
}
function setCellAttrFont(attr, font) {
    return (attr & ~A_CELL_FONT_MASK) | ((font & 0xF) << 28);
}

// get cell attribute parts
function getCellAttrFG(attr) { return attr & 0xFF; }
function getCellAttrBG(attr) { return (attr >>> 8) & 0xFF; }
function getCellAttrBold(attr) { return (attr & A_CELL_BOLD) != 0; }
function getCellAttrItalics(attr) { return (attr & A_CELL_ITALICS) != 0; }
function getCellAttrUnderline(attr) { return (attr & A_CELL_UNDERLINE) != 0; }
function getCellAttrBlinkSlow(attr) { return (attr & A_CELL_BLINKSLOW) != 0; }
function getCellAttrBlinkFast(attr) { return (attr & A_CELL_BLINKFAST) != 0; }
function getCellAttrShadow(attr) { return (attr & A_CELL_SHADOW) != 0; }
function getCellAttrStrikethrough(attr) { return (attr & A_CELL_STRIKETHROUGH) != 0; }
function getCellAttrDoublestrike(attr) { return (attr & A_CELL_DOUBLESTRIKE) != 0;}
function getCellAttrReverse(attr) { return (attr & A_CELL_REVERSE) != 0; }
function getCellAttrDisplay(attr) { return (attr & A_CELL_DISPLAY_MASK); }
function getCellAttrFaint(attr) { return (attr & A_CELL_FAINT) != 0; }
function getCellAttrFont(attr) { return (attr & A_CELL_FONT_MASK) >>> 28; }

// create cursor attributes
function makeCrsrAttr(color, size, orientation){

    color = color || 7;
    size = size || 2;
    orientation = orientation || 0;
    color &= 0xFF;
    size &= 0x03;
    orientation = (orientation ? 1 : 0)
    return (color & 0xFF) | (size << 8) | (orientation << 10);
}

// set cursor attributes
function setCrsrAttrColor(attr, color) {
    return (attr & ~A_CRSR_COLOR_MASK) | (color & 0xFF);
}
function setCrsrAttrSize(attr, size) {
    return (attr & ~A_CRSR_STYLE_MASK) | ((size << 8) & 0x0300);
}
function setCrsrAttrOrientation(attr, orient) {
    return (attr & ~A_CRSR_ORIENTATION) | (orient ? A_CRSR_ORIENTATION : 0x0000);
}

// get cursor attributes
function getCrsrAttrColor(attr) { return attr & 0xFF; }
function getCrsrAttrSize(attr) { return (attr & A_CRSR_STYLE_MASK) >>> 8; }
function getCrsrAttrOrientation(attr) { return (attr & A_CRSR_ORIENTATION) >>> 10; }

// if row size has changed, resize canvas, redraw row.
function adjustRow(rownum) {
    var
        row, width, w, x, cnv, i, nw;

    row = getRowElement(rownum);
    width = getRowAttrWidth(conRowAttr[rownum]) / 100;  // .5 - 2
    w = colSize * width;    // width of char

    // get current size
    cnv = row.firstChild;
    if (!cnv){
        // create it.
        cnv = document.createElement('canvas');
        row.appendChild(cnv);
        conCanvas[rownum] = cnv;
    }

    if (conRowAttr[rownum] & A_ROW_MARQUEE) {
        // marquee are normal width min or text length max
        nw = Math.max(conText[rownum].length * w, crtCols * colSize)
    } else {
        nw= crtCols * colSize;
    }

    if ((cnv.height != (rowSize + 16)) || (cnv.width != nw)) {
        // adjust for new height.
        row.style['height'] = rowSize + 'px';
        cnv.width = nw * xScale;
        cnv.height = (rowSize + 16);
    }
    // redraw this entire row
    redrawRow(rownum);
}

function redrawRow(rownum){
    var
        cnv, ctx, row, size, width, w, h, x, y, i, l;

    expandToRow(rownum);    // in case..

    // redraw this entire row
    l = conText[rownum].length;
    for (i = 0; i < l; i++)
        renderCell(rownum, i);

    // clear end of row
    //row = getRowElement(rownum);
    width = getRowAttrWidth(conRowAttr[rownum]) / 100;  // .5 - 2
    w = xScale * colSize * width;   // width of char
    x = w * l;                      // left pos of char on canv

    cnv = conCanvas[rownum];
    if (!cnv) {
        row = getRowElement(rownum);
        cnv = domElement(
            'canvas',
            {   width:  crtCols * w,
                height: (fontSize + 16) },
            {   zIndex: '50' });
        row.appendChild(cnv);
        conCanvas[rownum] = cnv;
    }
    ctx = cnv.getContext('2d');
    ctx.clearRect(x, 0, cnv.width - x, cnv.height);
}

// redraw all characters
function renderAll() {
    var
        r, c;
    for (r = 0; r < conRowAttr.length; r++)
        for (c = 0; c < conCellAttr[r].length; c++)
            renderCell(r, c);
}

// render an individual row, col. if forcerev, invert (twice if need be)
// This is the only place we need to convert a character to unicode for
// display!!
// hilight = 0:none, 1:mouse over, 2:mouse click

// force draw cells below on draw of top on tall cells. set bottom true.
// bottom = true if this is the bottom half of the row above.
function renderCell(rownum, colnum, hilight, bottom) {
        hilight = hilight || 0;
        bottom = bottom || false;
    var
        row,        // row element drawing to
        size,       // size factor (25%-200%)
        width,      // width factor (50%-200%)
        w, h,       // width / height of this character
        x,          // position on canvas
        drawtxt,    // flag for if text is drawn this cell
        cnv,        // canvas to draw upon
        ctx,        // canvas context
        attr,       // attributes for this character
        ch,         // character to draw
        tfg,        // this cell foreground color
        tbg,        // this cell background color
        tbold,      // this cell bold?
        stroke,     // thickness of underline / strikethrough
        tmp,        // swapper
        tblinks,    // this cell blinks?
        tfnt,       // font number for this cell
        i, j, l,    // index / length
        rowadj,     // row index adjustment used for double height bottoms
        tall,
        teletext,
        dir,
        xskew,      // skew amount for italics
        xadj,       // x adjustment to render character
        yadj,       // y adjustment to render character
        yScale;     // y scale for tall characters

    hilight = hilight || 0;     // flag for mouse over drawing. 0=normal,
                                // 1=mouseover,2=click
    bottom = bottom || false;   // force draw bottoms of double. if this is set
                                // draw the bottom haft of character in row
                                // above this row.
    rowadj = bottom?-1:0;       // adjustment for retreiving character info.

    // quick range check
    if (rownum + rowadj > conRowAttr.length)         return;
    if (colnum >= conText[rownum + rowadj].length)   return;

    // get size of this row
    width = getRowAttrWidth(conRowAttr[rownum + rowadj]) / 100; // .5 - 2

    w = xScale * colSize * width;   // width of char
    x = w * colnum;                 // left pos of char on canv

    // don't render off page unless marquee
    if ((x > w * crtCols) && !(conRowAttr[rownum] & A_ROW_MARQUEE))
        return;

    // compute height
//    row = getRowElement(rownum);    // <- speed this up.
    h = fontSize;                   // height of char
    stroke = h * 0.1;               // underline/strikethrough size

    cnv = conCanvas[rownum];
    if (!cnv) {
        // create new canvas if nonexistant
        row = getRowElement(rownum);
        cnv = domElement(
            'canvas',
            {   width:  crtCols * w,
                height: (h + 16) },
            {   zIndex: '50' });
        row.appendChild(cnv);
        conCanvas[rownum] = cnv;
    }
    ctx = cnv.getContext('2d');

    // get char and attributes
    ch = conText[rownum + rowadj].charAt(colnum);
    attr = conCellAttr[rownum + rowadj][colnum];

    // force highlight (for mouse selections)
    if (hilight == 1)
        attr = hotspotHoverAttr
    else if (hilight == 2)
        attr = hotspotClickAttr;

    // force highlight (for clipboard selection)
    if ((isSelect && betweenRCs({row:rownum, col:colnum}, selectStart, selectEnd)) ||
        (isDrag && betweenRCs({row:rownum, col:colnum}, dragStart, dragEnd))) {
        attr &= ~0xFFFF;
        attr |=  0x0F04;
    }

    // extract colors and font to use
    tfg = (attr & 0xFF);
    tbg = (attr >>> 8) & 0xff;
    tfnt = ((attr & A_CELL_FONT_MASK) >>> 28) & 0xF;

    // get bold and adust if not used
    tbold  = attr & A_CELL_BOLD;
    if (modeNoBold) // CSI ?32 h / l
        tbold = 0;

    // get blink attributes
    tblinks = attr & (A_CELL_BLINKSLOW | A_CELL_BLINKFAST);

    // move bold / blink to proper font if mode is on
    if (modeBoldFont && tbold)
        tfnt = 1;
    if (modeBlinkFont && tblinks)
        tfnt = 2;
    if (modeBoldFont && tbold && modeBlinkFont && tblinks)
        tfnt = 3;

    // reverse fg / bg for reverse on
    if (attr & A_CELL_REVERSE) {
        tmp = tfg;
        tfg = tbg;
        tbg = tmp;
    }

    // adjust fg color if in BBS ANSI and turn off bold.
    if (!cbm && !atari && !modeVTXANSI) {
        if (tbold && (tfg < 8)) {
            tfg += 8;
        }
        tbold = false;
    }

    // fix iCE colors
    if (modeBlinkBright
        && (tbg < 8)
        && tblinks) {
        // high intensity background / force blink off
        tbg += 8;
        attr &= ~(A_CELL_BLINKSLOW | A_CELL_BLINKFAST);
        tblinks = 0;
    }

    // fix transparents
    if (!cbm && !atari) {
        if (tfg == 0)
            tfg = 16;
        if ((tbg == 0) && !modeVTXANSI)
            tbg = 16;
    }

    // fix stupid ansi
    if (ch.charCodeAt(0) == 0x2588) {
        ch = ' ';
        tbg = tfg;
    }

    // set clipping region for this cell.
    ctx.save();
    ctx.beginPath();
    ctx.rect(x, 0, w, h);
    ctx.clip();

    // clear cell with background color
    if (cbm) {
        // PETSCII colors
        ctx.fillStyle = cbmColors[tbg]
        ctx.fillRect(x, 0, w, h);
    }  else if (atari) {
        ctx.fillStyle = atariColors[tbg]; // this should be 0 or 1
        ctx.fillRect(x, 0, w, h);
    } else {
        // ANSI colors
        if (tbg > 0) {
            ctx.fillStyle = ansiColors[tbg];
            ctx.fillRect(x, 0, w, h);
        } else
            ctx.clearRect(x, 0, w, h);
    }

    drawtxt = true;

    // don't draw if in blink state
    if (((attr & A_CELL_BLINKSLOW) && cellBlinkSlow)
        || ((attr & A_CELL_BLINKFAST) && cellBlinkFast)) {
        if (!modeNoBlink)
            drawtxt = false;
    }

    // row display takes precidence over cell
    // adjust scaling for row display type (normal,conceal,top,bottom)
    i = conRowAttr[rownum] & A_ROW_DISPLAY_MASK;
    switch (i) {
        case A_ROW_DISPLAY_NORMAL:
            // adjust scaling for cell display type (normal,conceal,top,bottom)
            j = attr & A_CELL_DISPLAY_MASK;
            switch (j) {
                case A_CELL_DISPLAY_NORMAL:
                    yadj = 0;
                    yScale = 1.0;
                    break;

                case A_CELL_DISPLAY_CONCEAL:
                    // don't draw if row is concealed.
                    drawtxt = false;
                    break;

                case A_CELL_DISPLAY_TOP:
                    yadj = 0;
                    yScale = 2.0;
                    break;

                case A_CELL_DISPLAY_BOTTOM:
                    yadj = -h;
                    yScale = 2.0;
                    break;
            }
            break;

        case A_ROW_DISPLAY_CONCEAL:
            // don't draw if row is concealed.
            drawtxt = false;
            break;

        case A_ROW_DISPLAY_TOP:
            yadj = 0;
            yScale = 2.0;
            break;

        case A_ROW_DISPLAY_BOTTOM:
            yadj = -h;
            yScale = 2.0;
            break;
    }

    if (drawtxt) {
        // select text color
        if (attr & A_CELL_FAINT)
            // darken faint color
            ctx.fillStyle = brightenRGB(ansiColors[tfg], -0.33);
        else if (cbm)
            // PETSCII color
            ctx.fillStyle = cbmColors[tfg]
        else if (atari)
            // ATASCII monochrome color
            ctx.fillStyle = atariColors[tfg]
        else
            // ANSI color
            ctx.fillStyle = ansiColors[tfg];

        // swap for special fonts.
        teletext = -1;
        if ((tfnt == 11) || (tfnt == 12)) {
            // special teletext block font
            if (((ch >= ' ') && (ch <= '?')) ||
                ((ch >= '`') && (ch <= '\x7f')))
                teletext = tfnt - 11;
            ctx.font = (tbold ? 'bold ' : '') + fontSize + 'px ' + conFont[tfnt];
        } else if (cbm)
            // PETSCII : render all text using conFontNum
            ctx.font = (tbold ? 'bold ' : '') + fontSize + 'px ' + conFont[conFontNum]
        else
            ctx.font = (tbold ? 'bold ' : '') + fontSize + 'px ' + conFont[tfnt];

        ctx.textAlign = 'start';
        ctx.textBaseline = 'top';

        // set shadowing for shadow effect
        if (attr & A_CELL_SHADOW) {
            ctx.shadowColor = '#000000';
            ctx.shadowOffsetX = h / rowSize;
            ctx.shadowOffsetY = h / rowSize;
            ctx.shadowBlur = 0;
        } else {
            ctx.shadowBlur = 0;
        }

        // set skew for italics
        xskew = 0;
        xadj = 0;
        if (attr & A_CELL_ITALICS) {
            xskew = -0.125;
            xadj  = 1;
        }

        // if drawomg the bottoms of teletext double tall, adjust
        //if (attr & A_CELL_DOUBLE)
        if (bottom) {
            yadj = -h;
            yScale = 2.0;
        }

        // convert to proper glyph # for font used by this char
        if (teletext == -1)
            ch = String.fromCharCode(getUnicode(conFontCP[tfnt], ch.charCodeAt(0)));

        // transmogrify
        ctx.setTransform(
            xScale * width,             // x scale
            0,                          // y skew
            xScale * xskew,             // x skew
            yScale,                     // y scale
            x + (xScale * xadj),        // x adj
            yadj);                      // y adj

        if (teletext >= 0)
            drawMosaicBlock(ctx, ch.charCodeAt(0), w / width, h, teletext)
        else
            ctx.fillText(ch, 0, 0);

        // draw underline / strikethough manually
        if (attr & A_CELL_UNDERLINE) {
            ctx.fillRect(0, h - stroke, w, stroke);
        }
        if (attr & A_CELL_STRIKETHROUGH) {
            ctx.fillRect(0, (h >> 1) - stroke, w, stroke);
        }
        if (attr & A_CELL_DOUBLESTRIKE) {
            ctx.fillRect(0, (h >> 2) - stroke, w, stroke);
            ctx.fillRect(0, ((h >> 1)+(h >> 2)) - stroke, w, stroke);
        }
    }
    ctx.restore();
}

// draw teletext style block graphic
// 0x20-0x3f/0x60-0x7f
function drawMosaicBlock(ctx, ch, w, h, separated) {
    var
        b,
        x, y,
        bit = 0x01,
        bw, bh,
        bx, by = 0,
        sep,
        xadj, yadj;

    w /= xScale;

    bw = Math.floor(w / 2);
    bh = Math.floor(h / 3);
    sep = (separated ? -(Math.floor(w/9)+1) : 0);
    xadj = w - (bw * 2);
    yadj = h - (bh * 3);

    b = ch - 0x20;
    if (b > 0x1f)
        b -= 0x20;
    for (y = 0; y < 3; y++) {
        bx = 0;
        for (x = 0; x < 2; x++) {
            if (b & bit) {
                // draw this mini block
                ctx.fillRect(
                    bx, by,
                    bw + (x ? xadj : 0) + sep,
                    bh + ((y == 2)? yadj : 0) + sep);
            }
            bx += bw;
            bit <<= 1;
        }
        by += bh;
    }
}

// brighten / darken a color. color is a 24bit value (#RRGGBB)
function brightenRGB(colorstr, factor) {
    var
        r, g, b, rgb;

    // catch transparent
    if (colorstr == 'transparent')
        return colorstr;

    rgb = htoi(colorstr.substring(1));
    r = (rgb >>> 16) & 0xFF;
    g = (rgb >>>  8) & 0xFF;
    b =  rgb        & 0xFF;
    if (factor < 0) {
        factor += 1.0;
        r *= factor;
        g *= factor;
        b *= factor;
    } else {
        r = (255 - r) * factor + r;
        g = (255 - g) * factor + g;
        b = (255 - b) * factor + b;
    }
    r = Math.floor(r) & 0xFF;
    g = Math.floor(g) & 0xFF;
    b = Math.floor(b) & 0xFF;
    return  '#' + itoh((r << 16) + (g << 8) + b, 6);
}

// grow this row to col
function expandToCol(rownum, colnum) {
    while (conText[rownum].length < colnum)
        conText[rownum] += ' ';
    while (conCellAttr[rownum].length < colnum)
        conCellAttr[rownum][conCellAttr[rownum].length] = defCellAttr;
}

// grow total rows to rownum. needs to add html dom elements as well.
function expandToRow(rownum) {
    if (rownum >= conRowAttr.length) {
        while (rownum > getMaxRow()) {
            textDiv.appendChild(createNewRow());

            conRowAttr[conRowAttr.length] = 0x002C0000;
            conCellAttr[conCellAttr.length] = [];
            conText[conText.length] = '';
        }
        trimHistory();
    }
}

// adjust a <div><img src='...svg'></div> element so svg fills dimensions of div
// this is an onload event for svg images inside a div
function fitSVGToDiv(e) {
    var
        str,
        r1, w1, h1, w2, h2,
        p = this.parentNode;

    r1 = p.getBoundingClientRect();     // size of bounding div
    w1 = r1.width;
    h1 = r1.height;
    w2 = this.naturalWidth;             // ration of image
    h2 = this.naturalHeight;
    str = 'scale(' + (w1 / h2) + ',' + (h1 / w2) + ')';
    this.style['transform-origin'] = 'top left';
    this.style['transform'] = str;
    this.style['visibility'] = 'visible';
}

// set indicators
function setBulbs() {
    var
        el;

    // set the online indicator button
    el = document.getElementById('osbulb');
    if (termState == TS_OFFLINE) {
        el.src = vtxPath + 'os0.png';
        el.title = 'Connect';
    } else {
        el.src = vtxPath + 'os1.png';
        el.title = 'Disconnect';
    }

    // set the indicators
    document.getElementById('fsbulb').src = vtxPath + (modeFullScreen ? 'fs1':'fs0') + '.png';
    document.getElementById('vobulb').src = vtxPath + (soundOn ? 'vo1':'vo0') + '.png';
    document.getElementById('kcbulb').src = vtxPath + (soundClicks ? 'kc1':'kc0') + '.png';
    document.getElementById('clbulb').src = vtxPath + (capState ? 'cl1':'cl0') + '.png';

    // set the ul/dl buttons
    if (termState == TS_NORMAL) {
        el = document.getElementById('ulbtn');
        el.src = vtxPath + 'ul1.png';
        el.style['visibility'] = 'visible';
        el = document.getElementById('dlbtn');
        el.src = vtxPath + 'dl1.png';
        el.style['visibility'] = 'visible';
    } else if (termState == TS_OFFLINE) {
        el = document.getElementById('ulbtn');
        el.src = vtxPath + 'ul0.png';
        el.style['visibility'] = 'visible';
        el = document.getElementById('dlbtn');
        el.src = vtxPath + 'dl0.png';
        el.style['visibility'] = 'visible';
    } else {
        // buttons not visible in file transfer mode.
        document.getElementById('ulbtn').style['visibility'] = 'hidden';
        document.getElementById('dlbtn').style['visibility'] = 'hidden';
    }
}

// set event for page resize check and cursor blink
function setTimers(onoff) {
    if (onoff) {
        if (irqWriteBuffer == null)
            irqWriteBuffer = setInterval(doWriteBuffer, 20);
        if (irqCheckResize == null)
            irqCheckResize = setInterval(doCheckResize, 50);
        if (irqCursor == null)
            irqCursor = setInterval(doCursor, 533);
        if (irqBlink == null)
            irqBlink = setInterval(doBlink, 533);
    } else {
        if (irqWriteBuffer != null)
            clearInterval(irqWriteBuffer);
        if (irqCheckResize != null)
            clearInterval(irqCheckResize);
        if (irqCursor != null)
            clearInterval(irqCursor);
        if (irqBlink != null)
            clearInterval(irqBlink);
        irqWriteBuffer = null;
        irqCheckResize = null;
        irqCursor = null;
        irqBlink = null;
    }
}

// event functions for ctrl+v text to console
function beforePaste(e) {
    e.returnValue = false;
}

// event functions for ctrl+v text to console
function paste(e) {
    var
        str, clipboardData;

    e = e || window.event;

    // Stop data actually being pasted into div
    e.stopPropagation();
    e.preventDefault();

    clipboardData = e.clipboardData || window.clipboardData;

    // need to update to send if connected
    // for now, local echo it.
    str = clipboardData.getData('Text');
    str = str.replace(/(?:\r\n|\r|\n)/g, '\x0D\x0A');
    sendData(str);
}

// update cursor from crsrAttr values
function newCrsr() {
    const
        sizes = [ '0%', '10%', '25%', '100%' ];
    var
        o, c, z, sz, ax1, ax2;

    if (crsr == null) {

        crsr = domElement(
            'div', {},
            {   position:   'absolute',
                display:    'block',
                zIndex:     '999' })

        o = domElement(
            'div',
            {   id:         'crsr' },
            {   position:   'absolute',
                display:    'block',
                bottom:     '0px',
                left:       '0px' });

        crsr.appendChild(o);
        pageDiv.appendChild(crsr);
        //textDiv.appendChild(crsr);
    } else
        o = crsr.firstChild;

    z = getCrsrAttrOrientation(crsrAttr);
    sz = getCrsrAttrSize(crsrAttr)
    switch (z) {
        case 0:
            o.style['width'] = '100%';
            o.style['height'] = sizes[sz];
            break;
        default:
            o.style['height'] = '100%';
            o.style['width'] = sizes[sz];
            break;
    }
    c = getCrsrAttrColor(crsrAttr);
    if (cbm)
        o.style['background-color'] = cbmColors[c & 0xF]
    else if (atari)
        o.style['background-color'] = atariColors[c & 0x1]
    else
        o.style['background-color'] = ansiColors[c];
}

// onmessage buffer - holds left over bytes from incomplete UTF8/16 chars
let inBuff = new Uint8Array(0);

function str2ab(str) {
    var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
    var bufView = new Uint16Array(buf);
    for (var i=0, strLen=str.length; i<strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return buf;
}

// connect / disconnect. called from connect UI element.
function termConnect() {

    if (termState == TS_OFFLINE) {

        tnState = 0;    // reset telnet options state

        if (vtxdata.hixie) {
            ws = new WebSocket(wsConnect, [ 'plain' ]);
        } else {
            ws = new WebSocket(wsConnect, [ 'telnet' ]);
            ws.binaryType = "arraybuffer";
        }

        ws.onmessage = function(e) {
            // binary data in.
            var
                i, j, str, data, edata;

            // incoming data to Uint8Array
            if (typeof e.data == 'string') {

                var l = e.data.length;
                edata = new Uint8Array(l);
                for  (i = 0; i < l; i++)
                    edata[i] = e.data.charCodeAt(i);

            } else if (typeof e.data == 'blob') {

                // umm . no
                ws.close();
                termState == TS_OFFLINE;
                setbulbs();
                conBufferOut('\r\rReceived blob binary. No.\r')
                return;

            } else {

                edata = new Uint8Array(e.data);
            }

            // append left over data from last message (incomplete UTF8/16)
            data = new Uint8Array(inBuff.length + edata.length);
            data.set(inBuff);
            data.set(edata, inBuff.length);

            // do telnet handshaking negotiations - return new data w/o IAC...'s
            if (vtxdata.telnet == 1)
                data = tnNegotiate(data);

//dump(data, 0, data.length);

            switch (termState) {
                case TS_NORMAL:
                    // convert from codepage into string data
                    switch (codePage){
                        case 'UTF8':
                            var utfdat = UTF8ArrayToStr(data);
                            str = utfdat.strData;
                            inBuff = utfdat.leftOver;
                            break;

                        case 'UTF16':
                            var utfdat = UTF16ArrayToStr(data);
                            str = utfdat.strData;
                            inBuff = utfdat.leftOver;
                            break;

                        default:
                            // straight string
                            str = '';
                            for (i = 0; i < data.length; i++) {
                                str += String.fromCharCode(data[i]);
                            }
                            break;
                    }
                    conBufferOut(str);
                    break;

                case TS_YMR_START:
                case TS_YMR_GETPACKET:
                    // handles binary data for ymodem
                    data = ymRStateMachine(data);
                    if (data.length > 0) {
                        // transfer ended midway. output the rest.
                        // todo - convert data to str.
                        conBufferOut(str);
                    }
                    break;

                case TS_YMS_START:
                case TS_YMS_PUTPACKET:
                case TS_YMS_PUTWAIT:
                    // handles binary data for ymodem
                    data = ymSStateMachine(data);
                    if (data.length > 0) {
                        // transfer ended midway. output the rest.
                        // todo - convert data to string.
                        conBufferOut(str);
                    }
                    break;
            }
        }

        ws.onopen = function() {
            if (vtxdata.telnet)
                tnInit();
            termState = TS_NORMAL;
            setBulbs();
        }
        ws.onclose = function(e) {
/*
            var reason;
            switch (e.code) {
                case 1000:
                    reason = "Normal closure, meaning that the purpose for which the connection was established has been fulfilled.";
                    break;

                case 1001:
                    reason = "An endpoint is \"going away\", such as a server going down or a browser having navigated away from a page.";
                    break;

                case 1002:
                    reason = "An endpoint is terminating the connection due to a protocol error";
                    break;

                case 1003:
                    reason = "An endpoint is terminating the connection because it has received a type of data it cannot accept (e.g., an endpoint that understands only text data MAY send this if it receives a binary message).";
                    break;

                case 1004:
                    reason = "Reserved. The specific meaning might be defined in the future.";
                    break;

                case 1005:
                    reason = "No status code was actually present.";
                    break;

                case 1006:
                    reason = "The connection was closed abnormally, e.g., without sending or receiving a Close control frame";
                    break;

                case 1007:
                    reason = "An endpoint is terminating the connection because it has received data within a message that was not consistent with the type of the message (e.g., non-UTF-8 [http://tools.ietf.org/html/rfc3629] data within a text message).";
                    break;

                case 1008:
                    reason = "An endpoint is terminating the connection because it has received a message that \"violates its policy\". This reason is given either if there is no other sutible reason, or if there is a need to hide specific details about the policy.";
                    break;

                case 1009:
                    reason = "An endpoint is terminating the connection because it has received a message that is too big for it to process.";
                    break;

                case 1010:
                    reason = "An endpoint (client) is terminating the connection because it has expected the server to negotiate one or more extension, but the server didn't return them in the response message of the WebSocket handshake. <br /> Specifically, the extensions that are needed are: " + e.reason;
                    break;

                case 1011:
                    reason = "A server is terminating the connection because it encountered an unexpected condition that prevented it from fulfilling the request.";
                    break;

                case 1015:
                    reason = "The connection was closed due to a failure to perform a TLS handshake (e.g., the server certificate can't be verified).";
                    break;

                default:
                    reason = "Unknown reason";
                    break;
            }
            conBufferOut('\r\r'+reason+'\r')
*/
            conBaud = 0;
            if (cbm)
                conBufferOut('\r\rDISCONNECTED.\r')
            else if (atari)
                conBufferOut('\x9B\x9BDisconnected.\x9B')
            else
                conBufferOut('\r\n\r\n\x1b[#9\x1b[0;91mDisconnected.\r\n');

            document.body.style['cursor'] = 'default';
            if (audioEl) {
                audioEl.pause();
            }
            termState = TS_OFFLINE;
            resetTerminal();
            setBulbs();
        }
        ws.onerror = function(error) {
            if (cbm)
                conBufferOut('\r\rERROR : ' + error + '\r')
            else if (atari)
                conBufferOut('\x9B\x9BError : ' + error + '\x09B')
            else
                conBufferOut('\r\n\r\n\x1b[#9\x1b[0;91mError : ' + error + '\r\n');
            setBulbs();
        }
    } else {
        ws.close();
    }
}

// convert str to ascii;ascii;ascii;ascii format.
function encodeAscii(str) {
    var i,l,strout = '';
    l=str.length;
    for (i=0;i<l;i++)
        strout+=';'+str.charCodeAt(i).toString();
    return strout.substring(1);
}

// turn sound on off
function toggleSound(){
    if (soundOn) {
        soundSaveVol = audioEl.volume;
        audioEl.volume = 0;
        createCookie('vtxSound', '0', 30);
    } else {
        audioEl.volume = soundSaveVol;
        createCookie('vtxSound', '1', 30);
    }
    soundOn = !soundOn;
    setBulbs();
};

// turn on / off special effects
function toggleFX(){
    if (soundClicks) {
        fxDiv.classList.remove('raster');
        createCookie('vtxFX', '0', 30);
    } else {
        fxDiv.classList.add('raster');
        createCookie('vtxFX', '1', 30);
    }
    soundClicks = !soundClicks;
    setBulbs();
};

function setAllCSS() {
    var
        style,
        css,
        head = document.head || document.getElementsByTagName('head')[0];

    // kill old one
    style = document.getElementById('vtxstyle');
    if (style)
        head.removeChild(style);

    // build new one
    style = document.createElement('style');
    style.id = 'vtxstyle';
    style.type = 'text/css';

    css = '#vtxpage {'
        + ' font-family: ' + fontName + ';'
        + ' font-size: ' + fontSize + 'px;'
        + ' min-height: ' + (fontSize * vtxdata.crtRows).toString() + 'px;'
        + ' margin: 0px auto;'
        + ' padding: 0px;'
        + ' border: 0px;'
        + ' overflow: hidden;'
        + ' text-overflow: clip;'
        + ' z-index: 0;'
        + ' position: relative;'
        + '}\n'
        + '#vtxtext {'
        + ' width: 100%;'
        + ' margin: 0px;'
        + ' padding: 0px;'
        + ' border: 0px;'
        + ' overflow: hidden;'
        + ' text-overflow: clip;'
        + ' z-index: 10;'
        + ' position: relative;'
        + '}\n'
        + '#vtxfx {'
        + ' display: none;'
        + ' position: absolute;'
        + ' top: 0px;'
        + ' left: 0px;'
        + ' width: 100%;'
        + ' height: 100%'
        + '}\n'
        + '.vtx {'
        + ' position: relative;'
        + ' left: 0px;'
        + ' line-height: ' + fontSize + 'px;'
        + ' height: ' + fontSize + 'px;'
        + ' vertical-align: top;'
        + ' overflow: hidden;'
        + ' text-overflow: clip;'
        + '}\n'
        + '.fade {'
        + ' opacity: .5;'
        + ' background: #000055;'
        + '}\n'
        + '.noselect {'
        + ' -webkit-touch-callout: none;'
        + ' -webkit-user-select: none;'
        + ' -khtml-user-select: none;'
        + ' -moz-user-select: none;'
        + ' -ms-user-select: none;'
        + ' user-select: none;'
        + '}\n'
        + '.raster{'
        + ' display: inline !important;'
        + ' height: 100%;'
        + ' position: relative;'
        + ' overflow: hidden;'
        + ' pointer-events: none;'
        + '}\n'
        + '.raster::after {'
        + ' content: " ";'
        + ' position: absolute;'
        + ' top: 0;'
        + ' left: 0;'
        + ' bottom: 0;'
        + ' right: 0;'
        + ' background: rgba(9, 8, 8, 0.1);'
        + ' opacity: 0;'
        + ' z-index: 2;'
        + ' pointer-events: none;'
        + ' animation: flicker 0.15s infinite;'
        + '}\n'
        + '.raster::before {'
        + ' content: " ";'
        + ' position: absolute;'
        + ' top: 0;'
        + ' left: 0;'
        + ' bottom: 0;'
        + ' right: 0;'
        + ' background:'
        + '  linear-gradient(rgba(9, 8, 8, 0) 50%, rgba(0, 0, 0, 0.25) 50%),'
        + '  linear-gradient(90deg, rgba(255, 0, 0, 0.06), rgba(0, 255, 0, 0.02), rgba(0, 0, 255, 0.06));'
        + ' z-index: 2;'
        + ' background-size: 100% 2px, 3px 100%;'
        + ' pointer-events: none;'
        + '}\n'
        + '@keyframes flicker {'
        + ' 0% { opacity: 0.119; }'
        + ' 5% { opacity: 0.02841; }'
        + ' 10% { opacity: 0.35748; }'
        + ' 15% { opacity: 0.88852; }'
        + ' 20% { opacity: 0.9408; }'
        + ' 25% { opacity: 0.35088; }'
        + ' 30% { opacity: 0.22426; }'
        + ' 35% { opacity: 0.26418; }'
        + ' 40% { opacity: 0.09249; }'
        + ' 45% { opacity: 0.35312; }'
        + ' 50% { opacity: 0.89436; }'
        + ' 55% { opacity: 0.9574; }'
        + ' 60% { opacity: 0.19754; }'
        + ' 65% { opacity: 0.05086; }'
        + ' 70% { opacity: 0.12137; }'
        + ' 75% { opacity: 0.75791; }'
        + ' 80% { opacity: 0.89617; }'
        + ' 85% { opacity: 0.90183; }'
        + ' 90% { opacity: 0.20657; }'
        + ' 95% { opacity: 0.64125; }'
        + ' 100% { opacity: 0.78042; }'
        + '}\n'
        + '.marquee {'
        + ' animation: marquee 12s linear infinite;'
        + '}\n'
        + '@keyframes marquee {'
        + ' 0% { transform: translate( ' + (xScale * crtCols * colSize) + 'px, 0); } '
        + ' 100% { transform: translate(-100%, 0); }'
        + '}\n';

    if (style.styleSheet)
        style.styleSheet.cssText = css
    else
        style.appendChild(document.createTextNode(css));
    head.appendChild(style);
}

function copyTextToClipboard(text) {
    var
        textArea, range, successful;

    textArea = domElement(
        'textarea',
        {   value:      text,
            id:         'vtxcopy' },
        {   position:   'fixed',
            top:        0,
            left:       0,
            width:      '2em',
            height:     '2em',
            padding:    0,
            border:     0,
            outline:    0,
            boxShadow:  'none',
            background: 'transparent',
            color:      'transparent' });
    document.body.appendChild(textArea);

    range = document.createRange();
    range.selectNode(textArea);
    textArea.select();
    window.getSelection().addRange(range);
    successful = document.execCommand('copy');
    document.body.removeChild(textArea);
    return successful;
}

// setup the crt and cursor
function initDisplay() {
    var
        i, o, p, pos,
        cssel,
        el, div1, div2,
        fsize,
        style,
        css,
        head,
        sound, fx,
        defattrs;

    // get cookie info
    sound = (readCookie('vtxSound') || '1');
    soundOn = (sound == '1');
    fx = (readCookie('vtxFX') || '1');
    soundClicks = (fx == '1');

    hotspotHoverAttr = 0x000800FF;
    hotspotClickAttr = 0x000000FF;

    // find where client is to go id='vtxclient'
    clientDiv = document.getElementById('vtxclient');
    if (!clientDiv) return;
    clientDiv.style['position'] = 'relative';
    clientDiv.style['padding'] = '32px';

    // special fx div
    fxDiv = domElement(
            'div',
            {   id:         'vtxfx' });
    if (soundClicks)
        fxDiv.classList.add('raster');
    clientDiv.appendChild(fxDiv);

    // build required inner divs
    pageDiv = domElement(
            'div',
            {   id:         'vtxpage',
                className:  'noselect' });
    textDiv = domElement(
            'div',
            {   id:         'vtxtext',
                className:  'noselect' });
    pageDiv.appendChild(textDiv);
    clientDiv.appendChild(pageDiv);

    // create clipboard copy element
    copyEl = domElement('textarea',
        {   id:         'vtxcopy' },
        {   position:   'fixed',
            visibility: 'hidden',
            left:       '0',
            top:        '0',
            width:      '1px',
            height:     '1px',
            color:      'transparent',
            background: 'transparent',
            whiteSpace: 'pre',
            border:     'none',
            padding:    0,
            margin:     0
        });
    clientDiv.appendChild(copyEl);

    textPos = getElementPosition(textDiv);

    // determine standard sized font width in pixels
    getDefaultFontSize(); // get fontName, colSize, rowSize
    crtWidth = colSize * crtCols;

    // codepage from AKAs - abort if invalid
    if (!codePageData[codePage]) {
        if (codePageAKAs[codePage])
            codePage = codePageAKAs[codePage];
    }

    // set fonts.
    if (cbm) {
        conFontNum = 0;
        switch (vtxdata.codePage) {
            case 'VIC20':
                conFont[0] = 'VIC200';
                conFont[1] = 'VIC201';
                cbmColors = vic20Colors;
                break;

            case 'C64':
                conFont[0] = 'C640';
                conFont[1] = 'C641';
                cbmColors = c64Colors;
                break;

            case 'C128':
                conFont[0] = 'C1280';
                conFont[1] = 'C1281';
                cbmColors = c128Colors;
                break;
        }
        conFontCP[0] = 'RAW';
        conFontCP[1] = 'RAW';
    } else if (atari) {
        conFontNum = 0;
        conFont[0] = 'ATARI';
        conFontCP[0] = 'RAW';
    } else {
        conFontNum = 0;                 // current font being used.
        for (i = 0; i < 10; i++) {      // set default font selects.
            conFont[i] = fontName;
            conFontCP[i] = codePage;
        }
        for (i = 10; i < 13; i++) {
            conFont[i] = fontName; // text w/separated blocks
            conFontCP[i] = 'TELETEXT';
        }
    }

    pageDiv.style['width'] = (crtWidth * xScale) + 'px';
    pagePos = getElementPosition(pageDiv);
    pageLeft = pagePos.left;
    pageTop = pagePos.top;

    // build CSS for VTX client
    setAllCSS();

    defCellAttr = vtxdata.defCellAttr;
    defCrsrAttr = vtxdata.defCrsrAttr;
    defPageAttr = vtxdata.defPageAttr;
    cellAttr =  defCellAttr;
    crsrAttr = defCrsrAttr;
    pageAttr = defPageAttr;

    // create cursor
    newCrsr();
    crsrSkipTime = 0;

    // load bell sound
    soundBell = new Audio();
    soundBell.src = vtxPath + 'bell.mp3';
    soundBell.type = 'audio/mp3';
    soundBell.volume = 1;
    soundBell.preload = 'auto';
    soundBell.load();

    // load keyboard sounds.
    for (i = 0; i < 11; i++) {
        soundKeyUp[i] = new Audio();
        soundKeyUp[i].src = vtxPath + 'ku' + i + '.mp3';
        soundKeyUp[i].type = 'audio/mp3';
        soundKeyUp[i].volume = 0.5;
        soundKeyUp[i].preload = 'auto';
        soundKeyUp[i].load();
        soundKeyDn[i] = new Audio();
        soundKeyDn[i].src = vtxPath + 'kd' + i + '.mp3';
        soundKeyDn[i].type = 'audio/mp3';
        soundKeyDn[i].volume = 0.5;
        soundKeyDn[i].preload = 'auto';
        soundKeyDn[i].load();
    }

    // set page attributes
    p = pageDiv.parentNode;
    if (cbm) {
        p.style['background-color'] = cbmColors[(pageAttr >>> 8) & 0xF];
        pageDiv.style['background-color'] = cbmColors[pageAttr & 0xF];
    } else if (atari) {
        p.style['background-color'] = atariColors[(pageAttr >>> 8) & 0x1];
        pageDiv.style['background-color'] = atariColors[pageAttr & 0x1];
    } else {
        p.style['background-color'] = ansiColors[(pageAttr >>> 8) & 0xFF];
        pageDiv.style['background-color'] = ansiColors[pageAttr & 0xFF];
    }
    // set event for page resize check and cursor blink
    pageWidth = elPage.clientWidth;

    // set initial states.
    shiftState = ctrlState = altState
        = capState = numState = scrState = false;
    crsrBlink = false;  // cursor blink state for intervaltimer
    cellBlinkSlow = true;
    cellBlinkFast = true;
    crsrRow = crsrCol = 0;
    crsrDraw();

    setTimers(true);

    crsrRow = crsrCol=0;
    termState = TS_OFFLINE; // set for standard terminal mode, not in file xfer mode

    // indicators and controls
    // ctrl for fixed ur
    ctrlDiv = domElement(
        'div', {},
        {   width:          (crtWidth * xScale / 2) + 'px',
            height:         '30px',
            padding:        '3px',
            position:       'absolute',
            top:            '0px',
            left:           '50%',
            textAlign:      'right',
            zIndex:         '10'});

    // connect / disconnect indicator / button.
    ctrlDiv.appendChild(domElement(
        'img',
        {   src:        vtxPath + 'os0.png',
            id:         'osbulb',
            display:    'inline-block',
            onclick:    termConnect,
            width:      24,
            height:     24,
            title:      'Connect/Disconnect' },
        {   cursor:     'pointer'}));

    // fullscreen on / off
    ctrlDiv.appendChild(domElement(
        'img',
        {   src:        vtxPath + 'fs0.png',
            id:         'fsbulb',
            display:    'inline-block',
            onclick:    toggleFullScreen,
            width:      24,
            height:     24,
            title:      'Fullscreen' },
            { cursor:'pointer'}));

    // sound on / off
    ctrlDiv.appendChild(domElement(
        'img',
        {   src:        vtxPath + (soundOn ? 'vo1.png' : 'vo0.png'),
            id:         'vobulb',
            display:    'inline-block',
            onclick:    toggleSound,
            width:      24,
            height:     24,
            title:      'Sound' },
        {   cursor:     'pointer'}));

    // FX on / off
    ctrlDiv.appendChild(domElement(
        'img',
        {   src:        vtxPath + (soundClicks ? 'kc1.png' : 'kc0.png'),
            id:         'kcbulb',
            display:    'inline-block',
            onclick:    toggleFX,
            width:      24,
            height:     24,
            title:      'Effects' },
        {   cursor:     'pointer'}));

    // capslock indicator.
    ctrlDiv.appendChild(domElement(
        'img',
        {   src:        vtxPath + 'cl0.png',
            id:         'clbulb',
            display:    'inline-block',
            width:      24,
            height:     24,
            title:      'CapsLk' }));

    // upload
    ctrlDiv.appendChild(domElement(
        'img',
        {   src:        vtxPath + 'ul0.png',
            id:         'ulbtn',
            display:    'inline-block',
            onclick:    ymSendStart,
            width:      24,
            height:     24,
            title:      'YModem Upload' },
        {   cursor:     'pointer'}));
    // download
    ctrlDiv.appendChild(domElement(
        'img',
        {   src:        vtxPath + 'dl0.png',
            id:         'dlbtn',
            display:    'inline-block',
            onclick:    ymRecvStart,
            width:      24,
            height:     24,
            title:      'YModem Download' },
        {   cursor:     'pointer'}));


    pageDiv.parentNode.appendChild(ctrlDiv);

    // add audio element for sound.
    audioEl = domElement(
        'audio',
        {   id:         'vtxaudio',
            preload:    'none',
            volume:     (soundOn ? '0.25' : '0.0'),
            src:        '/;' },
        {   width:      '0px',
            height:     '0px' });
    pageDiv.appendChild(audioEl);

    conBufferOut(initStr);

    if (vtxdata.autoConnect && (vtxdata.autoConnect > 0))
        // websocket connect
        termConnect()
    else {
        // let user know how to connect. heh-
        switch (vtxdata.term) {
            case 'PETSCII':
                conBufferOut('\x0Evtx tERMINAL cLIENT\r');
                conBufferOut('vERSION ' + version + '\r');
                conBufferOut('2017 dAN mECKLENBURG jR.\r');
                conBufferOut('GITHUB.COM/CODEWAR65/vtx\xA4cLIENTsERVER\r');
                conBufferOut('\rcLICK cONNECT...\r');
                break;

            case 'ATASCII':
                conBufferOut('VTX Terminal Client\x9B');
                conBufferOut('Version ' + version + '\x9B');
                conBufferOut('2017 Dan Mecklenburg Jr.\x9B');
                conBufferOut('github.com/codewar65/VTX_ClientServer\x9B');
                conBufferOut('\x9BClick Connect...\x9B');
                break;

            default:
                conBufferOut('\n\r\x1b#6\x1b[38;5;46;58mVTX\x1b[32;78m Terminal Client\n\r');
                conBufferOut('\x1b#6\x1b[38;5;46;59mVTX\x1b[32;79m Version ' + version + '\n\r');
                conBufferOut('\n\r\x1b[m2017 Dan Mecklenburg Jr.\n\r');
                conBufferOut('Visit \x1b[1;45;1;1;'
                    + encodeAscii('https://github.com/codewar65/VTX_ClientServer')
                    + '\\https://github.com/codewar65/VTX_ClientServer for more info.\n\r');
                conBufferOut('\n\r\x1b[38;5;46mCLICK CONNECT...\x1b[m\n\r');
                break;
        }
    }

    setBulbs();

    // key events
    addListener(document, 'keydown', keyDown);
    addListener(document, 'keyup', keyUp);
    addListener(document, 'keypress', keyPress);
    // mouse events
    addListener(document, 'mouseup', mouseUp);
    addListener(document, 'mousedown', mouseDown);
    addListener(document, 'click', click);
    addListener(document, 'mousemove', mouseMove);
    // copy paste events
    //addListener(document, 'beforepaste', beforePaste);
    addListener(document, 'paste', paste);
}

function UTF8ArrayToStr(ray) {
    var
        out, i, len, c,
        breakPos,
        char2, char3;

    out = '';
    len = ray.length;
    breakPos = len;
    i = 0;
    while(i < len) {
        c = ray[i++];
        switch(c >>> 4) {
            case 0: case 1: case 2:
            case 3: case 4: case 5: case 6: case 7:
                // 0xxxxxxx
                out += String.fromCharCode(c);
                break;

            case 12: case 13:
                // 110x xxxx   10xx xxxx
                if (i >= len - 1) {
                    // need 2 bytes. only have 1
                    breakPos = i - 1;
                    i = len;
                    break;
                }
                char2 = ray[i++];
                out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
                break;

            case 14:
                // 1110 xxxx  10xx xxxx  10xx xxxx
                if (i >= len - 2) {
                    // need 3 bytes. only have 1 or 2
                    breakPos = i - 1;
                    i = len;
                    break;
                }
                char2 = ray[i++];
                char3 = ray[i++];
                out += String.fromCharCode(((c & 0x0F) << 12) |
                       ((char2 & 0x3F) << 6) |
                       ((char3 & 0x3F) << 0));
                break;
        }
    }
    //return out;
    //var lo = ray.slice(breakPos);
    var lo = ray.subarray(breakPos);
    return { strData: out, leftOver: lo };
}

function UTF16ArrayToStr(ray) {
    var
        out, i, len, c,
        breakPos,
        char2;

    out = '';
    len = ray.length;
    breakPos = len;
    i = 0;
    while(i < len) {
        if (i >= len - 1) {
            // need 2 bytes, only have 1.
            breakPos = i - 1;
            i = len;
            break;
        }
        c = ray[i++];
        char2 = ray[i++];
        out += String.fromCharCode((c << 8) | char2);
    }
    //return out;
    var lo = ray.subarray(breakPos);
//    var lo = ray.slice(breakPos);
    return { strData: out, leftOver: lo };
}

function fadeScreen(fade) {
    var
        el, fntfm, cs;

    if (!fade) {
        clientDiv.classList.remove('fade');
        if (ovl['dialog'])
            document.body.removeChild(ovl['dialog']);
        ovl = {};

        // enable keys / cursor
        document.getElementById('ulbtn').style['visibility'] = 'visible';
        document.getElementById('dlbtn').style['visibility'] = 'visible';
        setTimers(true);
        document.body.style['cursor'] = 'default';
    } else {
        clientDiv.classList.add('fade')
        fntfm = 'san-serif';

        // add control overlay
        ovl['dialog'] = domElement(
            'div',
            {   className:  'noselect' },
            {   width:      '256px',
                height:     '128px',
                display:    'block',
                background: '#CCC',
                color:      '#000',
                border:     '2px outset #888',
                position:   'fixed',
                top:        ((window.innerHeight - 128) / 2) + 'px',
                left:       ((pageWidth - 256) / 2) + 'px',
                cursor:     'default',
                fontFamily: fntfm,
                fontSize:   '12px' });
        document.body.appendChild(ovl['dialog']);

        // add cancel button
        ovl['dialog'].appendChild(domElement(
            'input',
            {   type:       'button',
                onclick:    ymCancel,
                value:      'Cancel' },
            {   width:      '96px',
                height:     '24px',
                position:   'absolute',
                bottom:     '8px',
                right:      '8px' }));

        // dialog label
        ovl['title'] = domElement(
            'span',
            {   className:  'noselect' },
            {   width:      '228px',
                height:     '20px',
                background: '#039',
                border:     '1px inset #888',
                position:   'absolute',
                top:        '4px',
                left:       '4px',
                padding:    '0px 8px',
                fontSize:   '14px',
                fontWieght: 'bold',
                color:      'white' },
            'YModem Download' );
        ovl['dialog'].appendChild(ovl['title']);

        // filename
        ovl['dialog'].appendChild(domElement(
            'span',
            {   className:  'noselect' },
            {   position:   'absolute',
                top:        '32px',
                left:       '12px' },
            "File:" ));
        ovl['filename'] = domElement(
            'span', {},
            {   position:       'absolute',
                top:            '32px',
                left:           '80px',
                width:          '172px',
                overflow:       'hidden',
                textOverflow:   'ellipsis'
            },
            '{filename}' );
        ovl['dialog'].appendChild(ovl['filename']);

        // filesize
        ovl['dialog'].appendChild(domElement(
            'span',
            {   className:  'noselect' },
            {   position:   'absolute',
                top:        '52px',
                left:       '12px' },
            "Size:" ));
        ovl['filesize'] = domElement(
            'span', {},
            {   position:       'absolute',
                top:            '52px',
                left:           '80px',
                width:          '172px',
                overflow:       'hidden',
                textOverflow:   'ellipsis'
            },
            '{filesize}' );
        ovl['dialog'].appendChild(ovl['filesize']);

        // filesize
        ovl['dialog'].appendChild(domElement(
            'span',
            {   className:  'noselect' },
            {   position:   'absolute',
                top:        '72px',
                left:       '12px' },
            "Transferred:" ));
        ovl['transferred'] = domElement(
            'span', {},
            {   position:       'absolute',
                top:            '72px',
                left:           '80px',
                width:          '172px',
                overflow:       'hidden',
                textOverflow:   'ellipsis'
            },
            '{transferred}' );
        ovl['dialog'].appendChild(ovl['transferred']);

        // disable keys / cursor
        setBulbs();
        document.getElementById('ulbtn').style['visibility'] = 'hidden';
        document.getElementById('dlbtn').style['visibility'] = 'hidden';
        setTimers(false);
        document.body.style['cursor'] = 'wait';
    }
}

function updateCRC16(crcIn, byteIn) {
    var
        crc = crcIn,
        b = byteIn | 0x100;

    do {
        crc <<= 1;
        b <<= 1;
        if (b & 0x100)
            crc++;
        if (crc & 0x10000)
            crc ^= CRC_POLY;
    } while (!(b & 0x10000));
    return crc & 0xFFFF;
}

function calcCRC16(data, start, size) {
    var
        pos, end,
        crc = 0;

    pos = start;
    end = start + size;
    for (; pos < end; pos++)
        crc = updateCRC16(crc, data[pos]);
    crc = updateCRC16(crc, 0);
    crc = updateCRC16(crc, 0);
    return crc & 0xFFFF;
}

// concatinate two bytearrays
function combineArrays(first, second) {
    var
        firstLength = first.length,
        result = new Uint8Array(firstLength + second.length);

    result.set(first);
    result.set(second, firstLength);

    return result;
}

// https://stackoverflow.com/questions/23451726/saving-binary-data-as-file-using-javascript-from-a-browser
function saveAs(blob, fileName) {
    var
        el,
        url = window.URL.createObjectURL(blob);

    el = domElement(
        'a',
        {   href:       url,
            download:   fileName },
        {   display:    'none' });

    document.body.appendChild(el);
    el.click();
    document.body.removeChild(el);

    // On Edge, revokeObjectURL should be called only after
    // a.click() has completed, atleast on EdgeHTML 15.15048
    setTimeout( function() {
        window.URL.revokeObjectURL(url);
    }, 1000);
}

// data in / out is Uint8Array
// the idea is to process one byte at a time. pass in chunks of string data
// and process all of it until done. return any remaining string data if
// terminates before end of string.
function ymRStateMachine(data){
    var
        i, j, result = [],
        block, block2,
        crc16A, crc16B,
        tmpblob,
        str, b, tmp, bytes = [];

    if (!data) return result;

    for (i = 0; i < data.length; i++) {
        b = data[i];
        switch (termState) {
            case TS_YMR_START:
                // start recieving packets. if SOH, get 128. if STX then 1024
                // need SOH 00 FF foo.c NUL[123] CRC CRC
                ymPacketPos = 0;
                switch (b) {
                    case _SOH:
                        clearInterval(ymTimer);
                        ymEOTCount = 0;
                        termState = TS_YMR_GETPACKET;   // advance to get line num
                        ymPacketSize = 128 + 4;         // 128 bytes + line, line inv + crc16
                        break;

                    case _STX:
                        clearInterval(ymTimer);
                        ymEOTCount = 0;
                        termState = TS_YMR_GETPACKET;   // advance to get line num
                        ymPacketSize = 1024 + 4;        // 128 bytes + line, line inv + crc16
                        break;

                    case _EOT:
                        // end of transmittion. save file.
                        clearInterval(ymTimer);
                        ymEOTCount++;
                        if (ymEOTCount == 1) {
                            sendData(_ACK);

                            termState = TS_NORMAL;
                            fadeScreen(false);

                            if (ymFileSize > -1) {
                                ymFileData = ymFileData.slice(0, ymFileSize);
                            } else {
                                // TODO : count CPMEOFs on end (work on this)
                                throw 'EOT look for CMPEOF';
//                              j = ymFileData.size - 1;
//                              while ((ymFileData[j] == _CPMEOF) && (j > 0))
//                                 j--;
//                              ymFileData = ymFileData.slice(0, j + 1);
                            }
                            saveAs(ymFileData, ymFileName);
                            result = data.slice(i + 1);
                            ymCancel();
                            break;
                        }
                        sendData(_ACK);
                        break;

                    case _CAN:
                        // cancel from remote.
                        clearInterval(ymTimer);
                        ymEOTCount = 0;
                        termState = TS_NORMAL;
                        fadeScreen(false);
                        ymFileData = new Blob([], {type: 'application/octet-stream'});
                        result = data.slice(i+1);
                        break;
                }
                break;

            case TS_YMR_GETPACKET:
                // read ymPacketSize bytes for packet
                ymPacketBuff[ymPacketPos++] = b;
                if (ymPacketPos == ymPacketSize) {
                    // entire packet recieved. test lines and crc16
                    block =     ymPacketBuff[0];
                    block2 =    ymPacketBuff[1];

                    // out of sequence
                    if (block != ymNextBlock) {
                        if (block == (ymNextBlock - 1))
                            // resending last block.
                            sendData(_ACK)
                        else
                            sendData(_NAK);
                        termState = TS_YMR_START;
                        break;
                    }

                    // check block number
                    if (block != (~block2 & 0xFF)) {
                        sendData(_NAK);
                        termState = TS_YMR_START;
                        break;
                    }
                    // check crc16
                    // uncertain the order for crc16 - swap if needed.
                    crc16A = (ymPacketBuff[ymPacketPos - 2] << 8) |
                              ymPacketBuff[ymPacketPos - 1];
                    crc16B = calcCRC16(ymPacketBuff, 2, ymPacketSize - 4);

                    if (crc16A != crc16B) {
                        //dump(ymPacketBuff, 2, ymPacketSize - 4);
                        sendData(_NAK);
                        termState = TS_YMR_START;
                        break;
                    }
                    if ((block == 0) && (ymFileData.size == 0)) {
                        // first packet. get filename and optional filesize
                        //dump(ymPacketBuff, 0, ymPacketBuff.length);
                        ymFileName = '';
                        j = 2;
                        while (ymPacketBuff[j])
                            ymFileName += String.fromCharCode(ymPacketBuff[j++]);
                        ovl['filename'].innerHTML = ymFileName;

                        j++; // skip over null.

                        // check for filesize
                        if (ymPacketBuff[j]) {
                            str = '';
                            while ((ymPacketBuff[j] != _SPACE) && (ymPacketBuff[j] != _NUL))
                                str += String.fromCharCode(ymPacketBuff[j++]);
                            ymFileSize = int(str); // known size
                            ovl['filesize'].innerHTML = formatSize(ymFileSize);
                        } else {
                            ymFileSize = -1;            // unknown size
                            ovl['filesize'].innerHTML = 'Unknown';
                        }

                        // initialize blob to empty
                        ymFileData =  new Blob([], {type: 'application/octet-stream'});
                        ovl['transferred'].innerHTML = '0 bytes';

                        // send ACK + C
                        sendData(_ACK);
                        sendData(_C);
                        termState = TS_YMR_START;

                    } else {
                        // append data to blob
                        tmp = new Uint8Array(ymPacketSize - 4);
                        for (j = 0; j < ymPacketSize - 4; j++)
                            tmp[j] = ymPacketBuff[2 + j];
                        ymPacketBuff = [];
                        tmpblob = new Blob([ymFileData, tmp]);
                        ymFileData = tmpblob;
                        ovl['transferred'].innerHTML = formatSize(ymFileData.size);

                        // send ACK
                        sendData(_ACK);
                        termState = TS_YMR_START;
                    }
                    ymNextBlock = (block + 1) & 0xFF;
                }
                break;
        }
        if (termState == TS_NORMAL)
            break;
    }
    return result;
}

// return human readable text for filesize
function formatSize(size) {
    var
        mag,
        mags = [ 'B','KB','MB','GB' ];

    for (mag = 0; size >= 1000; size /= 1000, mag++){};
    return size.toFixed(2) + ' ' + mags[mag];
}

// cancel ymodem transfer
function ymCancel() {
    clearInterval(ymTimer);
    sendData(_CAN);
    sendData(_CAN);
    sendData(_CAN);
    sendData(_CAN);
    sendData(_CAN);
    sendData(_CAN);
    sendData(_CAN);
    sendData(_CAN);
    termState = TS_NORMAL;
    fadeScreen(false);
}

// send C's to remote to start download.
function ymSendC() {
    sendData(_C);
    ymCCount++;
    if (ymCCount > 8) {
        // abort after 8 tries.
        termState = TS_NORMAL;
        clearInterval(ymTimer);
        fadeScreen(false);
    }
}

// receive file from remote.
function ymRecvStart() {

    // already have session started.
    if (ovl.length) return;

    // send starting C's
    termState = TS_YMR_START;

    // fade terminal - build file xfer ui.
    fadeScreen(true);
    ovl['title'].innerHTML = 'YModem Download';
    ovl['filename'].innerHTML = 'Waiting for Remote...';
    ovl['filesize'].innerHTML = '';
    ovl['transferred'].innerHTML = '';

    ymNextBlock = 0;
    ymCCount = 0;
    ymSendC();
    if (termState != TS_NORMAL)
        ymTimer = setInterval(ymSendC, 3000);
}

// wait for ack / nak. if no response, resend last packet.
function ymTimeOut() {
    sendData(ymPacketBuff);
    ymNakCount++;
}

function twosCompliment(v) {
    return (255 - v) & 0xff;
}

function highByte(v) {
    return (v >>> 8) & 0xff;
}

function lowByte(v) {
    return v & 0xff;
}

// state machine for sending.
// entire file in ymFileData blob
// current
function ymSStateMachine(data) {
    var
        tmp = [],
        str,
        crc16,
        i, j,
        pos,
        packetSize,
        b;

    for (i = 0; i < data.length; i++) {
        clearInterval(ymTimer);
        b = data[i];
        switch (termState) {
            case TS_YMS_START:
                // wait for 'C'
                if (b == _C) {
                    // build header packet

                    // 128 byte header
                    packetSize = 128;

                    ymPacketBuff = new Uint8Array(5 + packetSize);
                    ymPacketBuff.fill(_NUL);

                    // block packet prefix - size / block num
                    ymPacketBuff[0] = ((packetSize == 128) ? _SOH : _STX);
                    ymPacketBuff[1] = ymNextBlock;        // block number 0
                    ymPacketBuff[2] = 255 - ymNextBlock;  // inverse block number

                    // filename charactes are ascii 33-126 via cleanFileName()
                    str =   ymFileName + NUL +
                            ymFileSize.toString() + ' ' +
                            ymModifiedDate + ' 0 0';     // mode + serial #
                    for (j = 0; j < str.length; j++)
                        ymPacketBuff[3 + j] = str.charCodeAt(j);

                    // block packet suffix - crc16
                    crc16 = calcCRC16(ymPacketBuff, 3, packetSize);
                    ymPacketBuff[3 + packetSize] = highByte(crc16);
                    ymPacketBuff[4 + packetSize] = lowByte(crc16);

                    //console.log('Header Packet');
                    //dump(ymPacketBuff, 0, ymPacketBuff.length);
                    sendData(ymPacketBuff);

                    termState = TS_YMS_PUTPACKET;
                    ymFilePos = 0;
                    ymNakCount = 0;
                    ymEOTCount = 0;
                    ymNextBlock = (ymNextBlock + 1) & 0xFF;
                    // now need ACK + C

                } else {
                    // wait for 10 seconds before abort
                    if (new Date().getTime() > ymSendStartTime + 10000)
                        ymCancel();
                }
                break;

            case TS_YMS_PUTWAIT:
                // need a C to get past header ACK
                if (b == _C) {
                    ymNakCount = 0;

                    // send first block
                    packetSize = 1024;

                    ymPacketBuff = new Uint8Array(5 + packetSize);
                    ymPacketBuff.fill(_CPMEOF);

                    // block packet prefix - size / block num
                    ymPacketBuff[0] = ((packetSize == 128) ? _SOH : _STX);
                    ymPacketBuff[1] = ymNextBlock;        // block number 0
                    ymPacketBuff[2] = 255 - ymNextBlock;  // inverse block number

                    // copy chunk of file data
                    pos = ymFilePos;
                    for (j = 0; j < packetSize; j++, pos++) {
                        if (pos < ymFileSize)
                            ymPacketBuff[3 + j] = ymFileData[pos]
                        else
                            break;
                    }

                    // block packet suffix - crc16
                    crc16 = calcCRC16(ymPacketBuff, 3, packetSize);
                    ymPacketBuff[3 + packetSize] = highByte(crc16);
                    ymPacketBuff[4 + packetSize] = lowByte(crc16);

                    // send data
                    sendData(ymPacketBuff);
                    ymFilePos += packetSize;
                    if (ymFilePos > ymFileSize) ymFilePos = ymFileSize;

                    termState = TS_YMS_PUTPACKET;
                    ymNextBlock = (ymNextBlock + 1) & 0xFF;
                    // back to normal data-> + ACK/NAK<-

                } else
                    // ?!
                break;

            case TS_YMS_PUTPACKET:
                if (b == _NAK) {
                    // resend last packet.
                    sendData(ymPacketBuff);
                    ymNakCount++;
                    // abort after 10 NAKs
                    if (ymNakCount > 10)
                        ymCancel();
                } else if (b == _ACK) {
                    // last packet good.
                    ymNakCount = 0;
                    if ((ymFilePos == 0) && (ymNextBlock == 1)) {
                        // wait for C.
                        ovl['transferred'].innerHTML = 'Sent header.';
                        termState = TS_YMS_PUTWAIT
                    } else if (ymFilePos == ymFileSize) {
                        // send EOT
                        ovl['transferred'].innerHTML = formatSize(ymFileSize);
                        if (ymEOTCount) {
                            // done. transferred.
                            termState = TS_NORMAL;
                            ymCancel();
                        }
                        ymEOTCount++;
                        ymPacketBuff = new Uint8Array(1);
                        ymPacketBuff[0] = _EOT;
                        sendData(ymPacketBuff);

                    } else {
                        // send data packet
                        ovl['transferred'].innerHTML = formatSize(ymFilePos);

                        // send data packet 2+
                        packetSize = 1024;

                        ymPacketBuff = new Uint8Array(5 + packetSize);
                        ymPacketBuff.fill(_CPMEOF);

                        // block packet prefix - size / block num
                        ymPacketBuff[0] = ((packetSize == 128) ? _SOH : _STX);
                        ymPacketBuff[1] = ymNextBlock;        // block number 0
                        ymPacketBuff[2] = 255 - ymNextBlock;  // inverse block number

                        // copy chunk of file data
                        pos = ymFilePos;
                        for (j = 0; j < packetSize; j++, pos++) {
                            if (pos < ymFileSize)
                                ymPacketBuff[3 + j] = ymFileData[pos]
                            else
                                break;
                        }

                        // block packet suffix - crc16
                        crc16 = calcCRC16(ymPacketBuff, 3, packetSize);
                        ymPacketBuff[3 + packetSize] = highByte(crc16);
                        ymPacketBuff[4 + packetSize] = lowByte(crc16);

                        sendData(ymPacketBuff);
                        ymFilePos += packetSize;
                        if (ymFilePos > ymFileSize) ymFilePos = ymFileSize;

                        ymNextBlock = (ymNextBlock + 1) & 0xFF;
                    }
                } else if (b == _CAN) {
                    ymCancel();
                } else {
                    // ?!
                }
                break;
        }
        if (termState == TS_NORMAL)
            break;
    }
    return [];
}

// file loaded. commence upload.
function ymFileLoaded(e) {
    var
        str,
        i, pos;

    ymFileData = new Uint8Array(e.target.result);
    ymFileSize = ymFileData.byteLength;
    ovl['filesize'].innerHTML = formatSize(ymFileSize);
    ovl['transferred'].innerHTML = formatSize(0);
    termState = TS_YMS_START;       // wait for 'G'.
    ymNextBlock = 0;
    ymFilePos = 0;
    ymSendStartTime = new Date().getTime();
}

// file selected. load it up.
function ymFileLoad(e) {
    var
        i,
        file,
        reader = new FileReader();

    file = e.target.files[0],
    name = file.name;
    i = name.lastIndexOf('/');
    if (i == -1) i = name.lastIndexOf('\\');
    ymFileName = cleanFileName(name.substring(i + 1));
    ymModifiedDate = Math.floor(file.lastModified / 1000).toString(8);
    ovl['filename'].innerHTML = ymFileName;
    reader.onload = ymFileLoaded;
    reader.readAsArrayBuffer(file);
}

// remove spaces and stuff.
function cleanFileName(str) {
    var
        i, l, c, ch,
        strout = '',
        badchrs = '/\\?%*:|"<> ';

    for (i = 0, l = str.length; i < l; i++){
        c = str.charAt(i);
        ch = str.charCodeAt(i);
        if (between(ch, 32, 126) &&
            (badchrs.indexOf(c) == -1))
            strout += c;
    }
    if (!strout.length)
        strout = 'untitled';
    return strout;
}

// send file to remove
function ymSendStart() {
    var
        el;

    // already have session started.
    if (ovl.length) return;

    // fade terminal - build file xfer ui.
    fadeScreen(true);
    ovl['title'].innerHTML = 'YModem Upload';
    ovl['filename'].innerHTML = '';
    ovl['filesize'].innerHTML = '';
    ovl['transferred'].innerHTML = '';

    // fetch file.
    el = domElement(
        'input',
        {   type:       'file',
            onchange:   ymFileLoad },
        {   position:   'relative',
            left:       '1px',
            top:        '1px',
            width:      '1px',
            height:     '1px'
        });

    ovl['dialog'].appendChild(el);
    el.click();
    ovl['dialog'].removeChild(el);
}

function dump(buff, start, len){
    var
        i,
        str,
        alpha,
        count = 0;

    str = itoh(start, 4) + ':';
    alpha = '';
    for (i = start; i < start + len; i++) {

        str += ' ' + itoh(buff[i], 2);
        if (between(buff[i], 32, 126))
            alpha += String.fromCharCode(buff[i])
        else
            alpha += '.';

        count = (count + 1) & 0xf;
        if (!count) {
            str += ': ' + alpha + '\r\n' + itoh(i + 1, 4) + ':';
            alpha = '';
        }
    }
    for (i = count; i < 16; i++) str += '   ';
    str += ': ' + alpha;
    console.log(str);
}
function nop(){};

// convert character ch (0-255) to unicode
function getUnicode(cp, ch) {

    if ((cp == 'UTF8') || (cp == 'UTF16')) return ch;

    var
        d = 0,
        cplut = codePageData[cp] || codePageData[codePageAKAs[cp]];

    if (cplut) {
        switch (cplut.length) {
            case 256:
                d = cplut[ch];
                break;

            case 128:
                d = (ch < 128) ? codePageData['ASCII'][ch] : cplut[ch - 128];
                break;

            case 96:
                d = (ch < 160) ? codePageData['ASCII'][ch] : cplut[ch - 160];
                break;

            default:
                // default to 437 if not found.
                d = codePageData['CP437'][ch];
                break;
        }
    }
    return d;
}

// because IE and Safari are still in the stone age.
function Uint8indexOf(ba, val) {
    var i;
    for (i = 0; i < ba.length; i++) {
        if (ba[i] == val)
            return i;
    }
    return -1;
}

// send data to remote (or echo local if not connected)
// only send arraybuffer data
// add convert from native to node's codepage.
// need to escape 0xFF if in telnet mode!
function sendData(data) {
    var
        i, l,
        str,
        pos,
        len,
        tmp;

    if (typeof data === 'string') {
        // string to aray buffer
        str = data;
        l = str.length;
        data = new Uint8Array(l);
        for (i = 0; i < l; i++)
            data[i] = str.charCodeAt(i);
    } else if (typeof data === 'number') {
        // byte/character to aray buffer
        str = String.fromCharCode(data);
        data = new Uint8Array([data]);
    }

    if (!(data instanceof Uint8Array))
        throw 'Invalid data in sendData().';

    if (vtxdata.telnet) {
        // escape 0xFF's

        if (Uint8indexOf(data, 0xFF) >= 0) {
//        if (data.indexOf(0xFF) >= 0) {
            // spilt up / recombine
            tmp = [];
            len = 0;
            while ((pos = Uint8indexOf(data, 0xFF)) >= 0) {
                tmp.push(data.slice(0, pos));
                len += pos;
                tmp.push(new Uint8Array([0xFF,0xFF]));
                len += 2;
                data = data.slice(pos + 1);
            }
            len += data.length;
            tmp.push(data);

            // join tmp into new data
            data = new Uint8Array(len);
            for (pos = 0, i = 0; i < tmp.length; i++){
                data.set(tmp[i], pos);
                pos += tmp[i].length;
            }
        }
    }

    // check code page conversions.
    if (ws && (ws.readyState == 1) && (termState != TS_OFFLINE)) {
        ws.send(data.buffer);
    } else {
        conBufferOut(str);
        crsrDraw();
    }
}

// poke a character
function conPutChar(rownum, colnum, chr, attr) {
    // expand if needed
    expandToRow(rownum);
    expandToCol(rownum, colnum);
    if (chr == 127)
        nop();
    conText[rownum] = conText[rownum].splice(colnum, 1, String.fromCharCode(chr));
    conCellAttr[rownum][colnum] = attr;
    renderCell(rownum, colnum);
}

// output character using current attribute at cursor position.
function conPrintChar(chr) {

    crsrSkipTime = new Date().getTime();
    conPutChar(crsrRow, crsrCol, chr, cellAttr);
    crsrCol++;
    if (!modeAutoWrap) {
        if (crsrCol >= crtCols)
            crsrCol--;
    } else {
        if (crsrCol == colsOnRow(crsrRow)) {
            crsrCol = 0;
            crsrRow++
        }
    }
    lastChar = chr;
}

// erase hotspots on row from col1 to col2
function clearHotSpotsRow(row, col1, col2) {
    var
        hs, i;
    for (i = conHotSpots.length - 1; i >= 0; i--) {
        hs = conHotSpots[i];
        if ((hs.row == row) && (hs.col >= col1) && (hs.col <= col2))
            conHotSpots.splice(i,1);
    }
}

// erase hotspots from row1 to row2
function clearHotSpotsRows(row1, row2) {
    var
        hs, i;
    for (i = conHotSpots.length - 1; i >= 0; i--) {
        hs = conHotSpots[i];
        if (between(hs.row, row1, row2))
            conHotSpots.splice(i,1);
    }
}

// move hotspots on row from col1 to col2 by coladj cols
function moveHotSpotsRow(row, col1, col2, coladj) {
    var
        hs, i;
    for (i = conHotSpots.length - 1; i >= 0; i--) {
        hs = conHotSpots[i];
        if ((hs.row == row) && between(hs.col, col1, col2))
            hs.col += coladj;
    }
}

// move hotspots from row1 to row2 by rowadj rows
function moveHotSpotsRows(row1, row2, rowadj) {
    var
        hs, i;
    for (i = conHotSpots.length - 1; i >= 0; i--) {
        hs = conHotSpots[i];
        if (between(hs.row, row1, row2))
            hs.row += rowadj;
    }
}

function copyRow(fromrow, torow) {
    var
        rowf, rowt,         // row elements
        size, width, w, h,  // dimensions
        cnvf, cnvt,         // canvases to move from / to
        ctxt;               // canvas context

//    rowf = getRowElement(fromrow);
//    rowt = getRowElement(torow);

    width = getRowAttrWidth(conRowAttr[fromrow]) / 100;
    w = xScale * colSize * width;
    h = fontSize;

    // copy canvas's
    // TODO : check row size changes and adjust canvas
    cnvf = conCanvas[fromrow]    ;
    if (!cnvf) {
        rowf = getRowElement(fromrow);
        cnvf = domElement(
            'canvas',
            {   width:  crtCols * w,
                height: (h + 16) },
            {   zIndex: '50' });
        rowf.appendChild(cnvf);
        conCanvas[fromrow] = cnvf;
    }

    cnvt = conCanvas[torow];
    if (!cnvt) {
        rowt = getRowElement(torow);
        cnvt = domElement(
            'canvas',
            {   width:  crtCols * w,
                height: (h + 16) },
            {   zIndex: '50' });
        rowt.appendChild(cnvt);
        conCanvas[torow] = cnvt;
    }
    ctxt = cnvt.getContext('2d');
    ctxt.clearRect(0,0,crtCols*w,h+16);
    ctxt.drawImage(cnvf, 0, 0);
}

// scroll screen up 1 row
function scrollUp() {
    var
        rownumf, rownumt,
        rowf, rowt,
        size, width, w, h, cnvf, cnvt, ctxt,
        fromRow, toRow,
        j, hs;

    if (modeRegion) {
        fromRow = regionTopRow;
        toRow = regionBottomRow;
    } else {
        fromRow = 0;
        toRow = conRowAttr.length - 1;
    }

    expandToRow(crtRows);
    for (j = fromRow; j < toRow; j++) {
        conText[j] = conText[j + 1];
        conCellAttr[j] = conCellAttr[j + 1];
        conRowAttr[j] = conRowAttr[j + 1];

        copyRow(j+1, j);
    }
    // clear bottow row
    conText[toRow] = '';
    conCellAttr[toRow] = [];
    conRowAttr[toRow] = defRowAttr;
    redrawRow(toRow);

    // move / clear hotspots
    clearHotSpotsRow(fromRow, 0, 999);
    moveHotSpotsRows(fromRow + 1, toRow, -1);
}

// scroll screen down 1 row
function scrollDown() {
    var
        fromRow, toRow,
        j, hs;

    if (modeRegion) {
        fromRow = regionTopRow;
        toRow = regionBottomRow;
    } else {
        fromRow = 0;
        toRow = conRowAttr.length - 1;
    }

    expandToRow(crtRows);
    for (j = toRow; j > fromRow; j--) {
        conText[j] = conText[j - 1];
        conCellAttr[j] = conCellAttr[j - 1];
        conRowAttr[j] = conRowAttr[j - 1];

        copyRow(j-1,j);
    }
    // clear top row
    conText[fromRow] = '';
    conCellAttr[fromRow] = [];
    conRowAttr[fromRow] = defRowAttr;
    redrawRow(fromRow);

    // move / clear hotspots
    clearHotSpotsRow(toRow, 0, 999);
    moveHotSpotsRows(fromRow, toRow - 1, +1);
}

function resetTerminal() {
    var
        i;

    modeVTXANSI = false;
    modeBlinkBright = false;
    modeCursor = true;
    modeBoldFont = false;
    modeNoBold = false;
    modeBlinkFont = false;
    modeNoBlink = false;
    conBaud = 0;
    modeDOORWAY = false;
    cellAttr = defCellAttr;
    pageAttr = defPageAttr;
    crsrAttr = defCrsrAttr;

    pageDiv.parentNode.style['background-color'] = ansiColors[(pageAttr >>> 8) & 0xFF];
    pageDiv.style['background-color'] = ansiColors[pageAttr & 0xFF];
    conFontNum = 0;                 // current font being used.
    for (i = 0; i < 10; i++) {      // set default font selects.
        conFont[i] = fontName;
        conFontCP[i] = codePage;
    }
    for (i = 10; i < 13; i++) {
        conFont[i] = fontName; // text w/separated blocks
        conFontCP[i] = 'TELETEXT';
    }
}

var
    ttxGraphics,    // text or graphics
    ttxSeparated,   // separeted blocks?
    ttxDouble,      // in double height text
    ttxBottom,      // drawing bottom halfs flag
    ttxHeld;        // held / release?

// the big function - ansi sequence state machine. ###CALL conBufferOut!###
function conCharOut(chr) {
    var
        def,
        i, j, l,            // generic idx, length
        r, c, v,            // row, col idx
        crsrrender = false, // redraw cursor?
        doCSI = false,      // execute compiled CSI at end?
        parm,               // parameters
        str,
        els, div, img,      // for svg sprite creation
        fromRow, toRow,     // indexes for scrolling.
        hs = {},            // hotspot
        row, c1, c2,
        rpos,
        csize,
        spriteTop,
        spriteLeft,
        el;

    if (modeTeletext) {
        // teletext burst mode - ESC to exit to ANSI
        // if in col 0, set row for teletext
        if (chr == _ESC) {
            // exit
            cellAttr = cellSaveAttr; // restore attributes
            modeAutoWrap = modeSaveAutoWrap;
            modeTeletext = false;
        } else {
            if (crsrCol == 0) {
                // all rows are 40 cols.
                if (crtCols >= 80) {
                    conRowAttr[crsrRow] &= ~(A_ROW_DISPLAY_MASK | A_ROW_WIDTH_MASK);
                    conRowAttr[crsrRow] |= (A_ROW_WIDTH_200);
                    adjustRow(crsrRow);
                }
                // reset attributes for new row
                cellAttr = 0xA0000000
                    | (ttxToAnsiColor[0] << 8)
                    | (ttxToAnsiColor[7]);
                ttxGraphics = false;
                ttxSeparated = false;
                ttxBottom = false;
                if (ttxDouble)
                    ttxBottom = true;
                ttxDouble = false;
                ttxHeld = false;
            }
            if (ttxBottom) {
                // ignore all text this row. only drop bottoms of previous row
                // get text from above row.
                if (chr == 0x0A) {
                    crsrCol = 0;
                    crsrRow++;
                    expandToRow(crsrRow);
                } else {
                    chr = conText[crsrRow - 1].charCodeAt(crsrCol);
                    cellAttr = conCellAttr[crsrRow - 1][crsrCol];
                    var disp = getCellAttrDisplay(cellAttr);
                    if (disp == A_CELL_DISPLAY_TOP)
                        // this is a bottom.
                        cellAttr = setCellAttrDisplay(cellAttr, A_CELL_DISPLAY_BOTTOM)
                    else {
                        // else no show.
                        cellAttr = setCellAttrDisplay(cellAttr, A_CELL_DISPLAY_NORMAL)
                        chr = 0x20;
                    }
                    conPrintChar(chr);
                }
            } else {
                switch (chr) {
                    case 0x01:  // red alpha
                    case 0x02:  // green alpha
                    case 0x03:  // yellow alpha
                    case 0x04:  // blue alpha
                    case 0x05:  // magenta alpha
                    case 0x06:  // cyan alpha
                    case 0x07:  // white alpha **
                        ttxGraphics = false;
                        cellAttr = setCellAttrFG(cellAttr, ttxToAnsiColor[chr]);
                        conPrintChar(0x20);
                        break;

                    case 0x08:  // blink
                    case 0x09:  // blink off **
                        cellAttr = setCellAttrBlinkFast(cellAttr, (chr == 0x08));
                        conPrintChar(0x20);
                        break;

                    case 0x0A:  // box end (NL?)
                        crsrCol = 0;
                        crsrRow++;
                        expandToRow(crsrRow);
                        break;

                    case 0x0B:  // box start (ignore)
                        break;

                    case 0x0C:  // normal height **
                        cellAttr = setCellAttrDisplay(cellAttr, A_CELL_DISPLAY_NORMAL);
                        conPrintChar(0x20);
                        break;

                    case 0x0D:  // double height
                        cellAttr = setCellAttrDisplay(cellAttr, A_CELL_DISPLAY_TOP);
                        ttxDouble = true;
                        conPrintChar(0x20);
                        break;

                    case 0x0E:  // S0 (ignore)
                    case 0x0F:  // S1 (ignore)
                    case 0x10:  // DLE (ignore)
                        break;

                    case 0x11:  // red gfx
                    case 0x12:  // green gfx
                    case 0x13:  // yellow gfx
                    case 0x14:  // blue gfx
                    case 0x15:  // magenta gfx
                    case 0x16:  // cyan gfx
                    case 0x17:  // white gfx
                        ttxGraphics = true;
                        cellAttr = setCellAttrFG(cellAttr, ttxToAnsiColor[chr-0x10]);
                        conPrintChar(0x20);
                        break;

                    case 0x18:  // conceal (ignore)
                        break;

                    case 0x19:  // solid block gfx **
                    case 0x1A:  // separated block gfx
                        ttxSeparated = (chr == 0x1A);
                        conPrintChar(0x20);
                        break;

                    case 0x1C:  // black background
                        cellAttr = setCellAttrBG(cellAttr, ttxToAnsiColors[0]);
                        conPrintChar(0x20);
                        break;

                    case 0x1D:  // new background
                        cellAttr = setCellAttrBG(cellAttr,
                            getCellAttrFG(cellAttr));
                        conPrintChar(0x20);
                        break;

                    case 0x1E:  // hold gfx
                    case 0x1F:  // release gfx **
                        txtHeld = (chr == 0x1E);
                        conPrintChar(0x20);
                        break;

                    default:
                        // print it.
                        chr &= 0x7F;    // only use 7 bits
                        // set font base on ttxGraphics / ttxSeparated
                        var fnt = ttxGraphics ? (ttxSeparated ? 12 : 11) : 10;
                        cellAttr = setCellAttrFont(cellAttr, fnt);
                        conPrintChar(chr);
                        break;
                }
            }
            crsrrender = true;
        }
    } else if (cbm) {
        // PETSCII ------------------------------------------------------------
        switch (chr) {
            case 3:  // run/stop
                // ignore
                break;

            case 5:  // white
                cellAttr = setCellAttrFG(cellAttr, 1);
                break;

            case 7:     // bell
                soundBell.pause();
                soundBell.play();
                break;

            case 8:  // shift disable
                modeCBMShift = false;
                break;

            case 9:  // shift enable
                modeCBMShift = true;
                break;

            case 13: // cr
            case 141: // lf
                crsrCol = 0;
                crsrRow++;
                crsrrender = true;
                cellAttr = setCellAttrReverse(cellAttr, false);
                break;

            case 14: // text mode
                conFontNum = 1;
                renderAll();
                break;

            case 17: // down
                crsrRow++;
                crsrrender = true;
                break;

            case 18: // rev on
                cellAttr = setCellAttrReverse(cellAttr, true);
                break;

            case 19: // home
                crsrRow = 0;
                crsrCol = 0;
                crsrrender = true;
                break;

            case 20: // del
                if (crsrCol > 0)
                    crsrCol--;
                delChar(crsrRow, crsrCol);
                redrawRow(crsrRow);
                crsrrender = true;
                break;

            case 28: // red
                cellAttr = setCellAttrFG(cellAttr, 2);
                crsrAttr = setCrsrAttrColor(2);
                break;

            case 29: // right
                crsrCol++;
                if (crsrCol > crtCols){
                    crsrCol = 0;
                    crsrRow++;
                }
                crsrrender = true;
                break;

            case 30: // green
                cellAttr = setCellAttrFG(cellAttr, 5);
                crsrAttr = setCrsrAttrColor(5);
                break;

            case 31: // blue
                cellAttr = setCellAttrFG(cellAttr, 6);
                crsrAttr = setCrsrAttrColor(6);
                break;

            case 129: // orange
                cellAttr = setCellAttrFG(cellAttr, 8);
                crsrAttr = setCrsrAttrColor(8);
                break;

            case 133: case 134: case 135: case 136:
            case 137: case 138: case 139: case 140:
                // function keys.
                // ignore
                break;

            case 142: // graphics mode
                conFontNum = 0;
                renderAll();
                break;

            case 144: // black
                cellAttr = setCellAttrFG(cellAttr, 0);
                crsrAttr = setCrsrAttrColor(0);
                break;

            case 145: // up
                if (crsrRow > 0)
                    crsrRow--;
                crsrrender = true;
                break;

            case 146: // rev off
                cellAttr = setCellAttrReverse(cellAttr, false);
                break;

            case 147: // clr
                // clear entire screen.
                // remove html rows
                els = document.getElementsByClassName('vtx');
                for (l = els.length, r = l - 1; r >= 0; r--)
                    els[r].parentNode.removeChild(els[r]);
                conRowAttr = [];
                conCellAttr = [];
                conText = [];
                document.body.style['cursor'] = 'default';
                crsrRow = crsrCol = 0;
                crsrrender = true;
                break;

            case 148: // insert
                insChar(crsrRow, crsrCol, 0x20);
                redrawRow(crsrRow);
                break;

            case 149: // brown
                cellAttr = setCellAttrFG(cellAttr, 9);
                crsrAttr = setCrsrAttrColor(9);
                break;

            case 150: // lt red
                cellAttr = setCellAttrFG(cellAttr, 10);
                crsrAttr = setCrsrAttrColor(10);
                break;

            case 151: // dk gray
                cellAttr = setCellAttrFG(cellAttr, 11);
                crsrAttr = setCrsrAttrColor(11);
                break;

            case 152: // gray
                cellAttr = setCellAttrFG(cellAttr, 12);
                crsrAttr = setCrsrAttrColor(12);
                break;

            case 153: // lt green
                cellAttr = setCellAttrFG(cellAttr, 13);
                crsrAttr = setCrsrAttrColor(13);
                break;

            case 154: // lt blue
                cellAttr = setCellAttrFG(cellAttr, 14);
                crsrAttr = setCrsrAttrColor(14);
                break;

            case 155: // lt gray
                cellAttr = setCellAttrFG(cellAttr, 15);
                crsrAttr = setCrsrAttrColor(15);
                break;

            case 156: // purple
                cellAttr = setCellAttrFG(cellAttr, 4);
                crsrAttr = setCrsrAttrColor(4);
                break;

            case 157: // left
                if (crsrCol > 0)
                    crsrCol--;
                crsrrender = true;
                break;

            case 158: // yellow
                cellAttr = setCellAttrFG(cellAttr, 7);
                crsrAttr = setCrsrAttrColor(7);
                break;

            case 159: // cyan
                cellAttr = setCellAttrFG(cellAttr, 3);
                crsrAttr = setCrsrAttrColor(3);
                break;

            default:
                // try to print it.
                if ((chr & 0x7F) >= 0x20) {
                    conPrintChar(chr);
                    crsrrender = true;
                }
                break;
        }
    } else if (atari) {
        // ATASCII ------------------------------------------------------------
        switch (chr) {
            case 0x1C: // cursor up.
                if (crsrRow > 0)
                    crsrRow--;
                crsrrender = true;
                break;

            case 0x1D: // cursor down.
                crsrRow++;
                crsrrender = true;
                break;

            case 0x1E: // cursor left.
                if (crsrCol > 0)
                    crsrCol--;
                crsrrender = true;
                break;

            case 0x1F: // cursor right.
                crsrCol++;
                if (crsrCol > crtCols){
                    crsrCol = 0;
                    crsrRow++;
                }
                crsrrender = true;
                break;

            case 0x7D: // clear screen.
                // clear entire screen.
                // remove html rows
                els = document.getElementsByClassName('vtx');
                for (l = els.length, r = l - 1; r >= 0; r--)
                    els[r].parentNode.removeChild(els[r]);
                conRowAttr = [];
                conCellAttr = [];
                conText = [];
                document.body.style['cursor'] = 'default';
                crsrRow = crsrCol = 0;
                crsrrender = true;
                break;

            case 0x7E: // backspace - TODO : find out if destructive
                if (crsrCol > 0)
                    crsrCol--;
                crsrrender = true;
                break;

            case 0x7F: // tab
                // find next tabstop
                for (i = crsrCol+1; i < crtCols; i++){
                    if (conTabStop[i]) {
                        crsrCol = i;
                        crsrrender = true;
                        break;
                    }
                }
                break;

            case 0x9B: // return
                crsrCol = 0;
                crsrRow++;
                crsrrender = true;
                cellAttr = setCellAttrReverse(cellAttr, false);
                break;

            case 0x9C: // delete row
                delRow(crsrRow);
                break;

            case 0x9D: // insert row
                insRow(crsrRow);
                break;

            case 0x9E: // clear tabstop
            case 0x9F: // set tabstop
                conTabStop[crsrCol] = (chr == 0x9F);
                break;

            case 0xFD: // buzzer
                soundBell.pause();
                soundBell.play();
                break;

            case 0xFE: // delete char
                expandToRow(crsrRow);
                expandToCol(crsrRow, crsrCol);
                delChar(crsrRow, crsrCol);
                redrawRow(crsrRow);
                crsrrender = true;
                break;

            case 0xFF: // insert char
                break;

            default:
                // set reverse on off for each character in atari mode
                cellAttr = setCellAttrReverse(cellAttr, (chr & 0x80));
                conPrintChar(chr & 0x7F);
                crsrrender = true;
                break;
        }
    } else {
        // ANSI ---------------------------------------------------------------

        if (modeNextGlyph){
            // print glyph AS IS
            conPrintChar(chr);
            crsrrender = true;
            modeNextGlyph = false;
        } else {
            // do all normal ctrls first
            switch (chr) {
                case _NUL:     // null
                    if (modeDOORWAY)
                        modeNextGlyph = true;
                    break;

                case _BEL:     // bell
                    soundBell.pause();
                    soundBell.play();
                    break;

                case _BS:     // backspace
                    if (crsrCol > 0) {
                        expandToRow(crsrRow);
                        expandToCol(crsrRow, crsrCol);
                        crsrCol--;
                        //delChar(crsrRow, crsrCol);  // backspace is not destructive
                        crsrrender = true;
                    }
                    break;

                case _HT:     // horz tab
                    crsrCol = ((crsrCol >>> 3) + 1) << 3;
                    if (crsrCol > colsOnRow(crsrRow))
                        crsrCol = colsOnRow(crsrRow);
                    crsrrender = true;
                    break;

                case _LF:    // linefeed
//                    if (!modeVTXANSI)  // LF dont CR!  lol
//                        crsrCol = 0;    // for BBS/ANSI.SYS mode
                    crsrRow++;
                    crsrrender = true;
                    break;

                case _CR:    // carriage return
                    crsrCol = 0;
                    crsrrender = true;
                    break;

                case _DEL:   // delete (not on font 10 or 11)
                    if (between(conFontNum, 10, 12)) {
                        conPrintChar(chr);
                        crsrrender = true;
                    } else {
                        expandToRow(crsrRow);
                        expandToCol(crsrRow, crsrCol);
                        delChar(crsrRow, crsrCol);
                        redrawRow(crsrRow);
                        crsrrender = true;
                    }
                    break;

                default:
                    switch (ansiState) {
                        case 0:
                            // not in an sequence.
                            if (chr == _ESC)
                                ansiState = 1
                            else {
                                // convert to codepage
                                conPrintChar(chr);
                                crsrrender = true;
                            }
                            break;

                        case 1:
                            // start of ansi sequence
                            if (chr == 0x5B) {
                                // ESC [ - CSI
                                parms = '';
                                interm = '';
                                ansiState = 2
                            }
                            else if (chr == 0x23)
                                // ESC # - row attr
                                ansiState = 3
                            else if (chr == 0x37) {
                                // ESC 7 - Save crsr pos and attrs
                                crsrSaveRow = crsrRow;
                                crsrSaveCol = crsrCol;
                                cellSaveAttr = cellAttr;
                                ansiState = 0;
                            } else if (chr == 0x38) {
                                // ESC 8 - restore crsr pos and attrs
                                crsrRow = crsrSaveRow;
                                crsrCol = crsrSaveCol;
                                cellAttr = cellSaveAttr;
                                ansiState = 0;
                            } else if (chr == 0x44) {
                                // ESC D - Scroll up 1
                                scrollUp();
                                ansiState = 0;
                            } else if (chr == 0x45) {
                                // ESC E - Move to next line
                                crsrRow++;
                                crsrrender = true;
                                ansiState = 0;
                            } else if (chr == 0x4D) {
                                // ESC M - Scroll down 1
                                scrollDown();
                                ansiState = 0;
                            } else if (chr == 0x63) {
                                // ESC c - reset terminal
                                resetTerminal();
                            } else
                                // unrecognized - abort sequence
                                ansiState = 0;
                            break;

                        case 2:
                            // start of CSI (ESC [)
                            // collect parameters until either intermediate or final
                            if (between(chr, 0x30, 0x3F))
                                parms += String.fromCharCode(chr)
                            else if (between(chr, 0x20, 0x2F)) {
                                // intermediate byte
                                interm = String.fromCharCode(chr);
                                ansiState = 5;
                            } else if (between(chr, 0x40, 0x7E)) {
                                // final byte
                                ansiState = 0;
                                doCSI = true;
                            } else
                                // unrecognized - abort sequence
                                ansiState = 0;
                            break

                        case 3:
                            // start of row attr (ESC #)
                            // get single byte (0,1,9)
                            switch (chr) {
                                case 0x30:
                                    // ESC # 0 - reset row
                                    conRowAttr[crsrRow] = defRowAttr;
                                    break;

                                case 0x31:
                                    // ESC # 1 - single wide, double high, top
                                    conRowAttr[crsrRow] &=
                                        ~(A_ROW_DISPLAY_MASK | A_ROW_WIDTH_MASK);
                                    conRowAttr[crsrRow] |= (A_ROW_DISPLAY_TOP | A_ROW_WIDTH_100);
                                    break;

                                case 0x32:
                                    // ESC # 2 - single wide, double high, bottom
                                    conRowAttr[crsrRow] &=
                                        ~(A_ROW_DISPLAY_MASK | A_ROW_WIDTH_MASK);
                                    conRowAttr[crsrRow] |= (A_ROW_DISPLAY_BOTTOM | A_ROW_WIDTH_100);
                                    break;

                                case 0x33:
                                    // ESC # 3 - double wide/high, top
                                    conRowAttr[crsrRow] &=
                                        ~(A_ROW_DISPLAY_MASK | A_ROW_WIDTH_MASK);
                                    conRowAttr[crsrRow] |= (A_ROW_DISPLAY_TOP | A_ROW_WIDTH_200);
                                    break;

                                case 0x34:
                                    // ESC # 4 - double wide/high, bottom
                                    conRowAttr[crsrRow] &=
                                        ~(A_ROW_DISPLAY_MASK | A_ROW_WIDTH_MASK);
                                    conRowAttr[crsrRow] |= (A_ROW_DISPLAY_BOTTOM | A_ROW_WIDTH_200);
                                    break;

                                case 0x35:
                                    // ESC # 5 - single wide/high
                                    conRowAttr[crsrRow] &=
                                        ~(A_ROW_DISPLAY_MASK | A_ROW_WIDTH_MASK);
                                    conRowAttr[crsrRow] |= (A_ROW_WIDTH_100);
                                    break;

                                case 0x36:
                                    // ESC # 6 - single high / double wide
                                    conRowAttr[crsrRow] &=
                                        ~(A_ROW_DISPLAY_MASK | A_ROW_WIDTH_MASK);
                                    conRowAttr[crsrRow] |= (A_ROW_WIDTH_200);
                                    break;

                                case 0x37:
                                    // marquee off
                                    // ESC # 7
                                    conRowAttr[crsrRow] &= ~A_ROW_MARQUEE;
                                    getRowElement(crsrRow).firstChild.classList.remove('marquee')
                                    break;

                                case 0x38:
                                    // marquee on
                                    // ESC # 8
                                    conRowAttr[crsrRow] |= A_ROW_MARQUEE;
                                    getRowElement(crsrRow).firstChild.classList.add('marquee');
                                    break;
                            }
                            adjustRow(crsrRow);
                            ansiState = 0;
                            break;

                        case 5:
                            // collecting intermediate bytes
                            if (between(chr, 0x20, 0x2F))
                                interm += String.fromCharCode(chr)
                            else if (between(chr, 0x40, 0x7E)) {
                                // command?
                                ansiState = 0;
                                doCSI = true;
                            } else
                                // unrecognized - abort sequence
                                ansiState = 0;
                            break;
                    }
                    break;
                }
        }

        if (doCSI) {
            // chr = command / final byte
            // params = optional parameters
            // interm = optional intermediate (not using any for this term emulation - ignore)

            parm = parms.split(';');
            if (parm[0] == '') parm = [];

            // for our purposes, all parameters are integers. if not, leave as string.
            l = parm.length;
            for (i = 0; i < l; i++) {
                v = int(parm[i]);
                if (v.toString() == parm[i])
                    parm[i] = v;
            }

            switch (chr) {
                case 0x40:  // @ - ICH - insert characters
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    for (i = 0; i < parm[0]; i++)
                        insChar(crsrRow, crsrCol, 32);
                    redrawRow(crsrRow);
                    break;

                case 0x41:  // A - Cursor Up
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    crsrRow -= parm[0];
                    if (modeRegion) {
                        if (crsrRow < regionTopRow)
                            crsrRow = regionTopRow;
                    } else {
                        if (crsrRow < 0)
                            crsrRow = 0;
                    }
                    crsrrender = true;
                    break;

                case 0x42:  // B - Cursor Down
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    crsrRow += parm[0];
                    if (modeRegion) {
                        if (crsrRow > regionBottomRow)
                            crsrRow = regionBottomRow;
                    }
                    crsrrender = true;
                    break;

                case 0x43:  // C - Cursor Forward
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    crsrCol += parm[0];
                    if (crsrCol >= colsOnRow(crsrRow) - 1)
                        crsrCol = colsOnRow(crsrRow) - 1;
                    crsrrender = true;
                    break;

                case 0x44:  // D - Cursor Backward / Font Selection
                    if (interm == ' ') {
                        // set font
                        parm = fixParams(parm, [0, 0]);
                        switch (parm[1]) {
                            case  0: // Codepage 437 English
                            case 26: // Codepage 437 English, (thin)
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'CP437';
                                break;

                            case  5: // Codepage 866 (c) Russian
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'CP866';
                                break;

                            case 17: // Codepage 850 Multilingual Latin I, (thin)
                            case 18: // Codepage 850 Multilingual Latin I
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'CP850';
                                break;

                            case 25: // Codepage 866 Russian
                            case 27: // Codepage 866 (b) Russian
                            case 29: // Ukrainian font cp866u
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'CP866';
                                break;

                            case 19: // Codepage 885 Norwegian, (thin)
                            case 28: // Codepage 885 Norwegian
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'CP885';
                                break;

                            case 31: // Codepage 1131 Belarusian, (swiss)
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'CP1131';
                                break;

                            case  1: // Codepage 1251 Cyrillic, (swiss)
                            case 20: // Codepage 1251 Cyrillic
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'WIN1251';
                                break;

                            case  2: // Russian koi8-r
                            case 12: // Russian koi8-r (b)
                            case 22: // Russian koi8-r (c)
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'KOI8_R';
                                break;

                            case  9: // Ukrainian font koi8-u
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'KOI8_U';
                                break;

                            case 24: // ISO-8859-1 West European
                            case 30: // ISO-8859-1 West European, (thin)
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'ISO8859_1';
                                break;

                            case  3: // ISO-8859-2 Central European
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'ISO8859_2';
                                break;

                            case  4: // ISO-8859-4 Baltic wide (VGA 9bit mapped)
                            case 11: // ISO-8859-4 Baltic (VGA 9bit mapped)
                            case 13: // ISO-8859-4 Baltic wide
                            case 23: // ISO-8859-4 Baltic
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'ISO8859_4';
                                break;

                            case 14: // ISO-8859-5 Cyrillic
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'ISO8859_5';
                                break;

                            case 21: // ISO-8859-7 Greek
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'ISO8859_7';
                                break;

                            case  6: // ISO-8859-9 Turkish
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'ISO8859_9';
                                break;

                            case 10: // ISO-8859-15 West European, (thin)
                            case 16: // ISO-8859-15 West European
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'ISO8859_15';
                                break;

                            case 15: // ARMSCII-8 Character set
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'ARMSCII_8';
                                break;

                            case  7: // haik8 codepage (use only with armscii8 screenmap)
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'HAIK8';
                                break;

                            case  8: // ISO-8859-8 Hebrew
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'ISO8859_8';
                                break;

                            // CONVERTED FONTS - USE E000-E0FF for map
                            case 32: // Commodore 64 (UPPER)
                                conFont[parm[0]] =  'C640';
                                conFontCP[parm[0]] = 'RAW';
                                break;

                            case 33: // Commodore 64 (Lower)
                                conFont[parm[0]] =  'C641';
                                conFontCP[parm[0]] = 'RAW';
                                break;

                            case 34: // Commodore 128 (UPPER)
                                conFont[parm[0]] =  'C1280';
                                conFontCP[parm[0]] = 'RAW';
                                break;

                            case 35: // Commodore 128 (Lower)
                                conFont[parm[0]] =  'C1281';
                                conFontCP[parm[0]] = 'RAW';
                                break;

                            case 36: // Atari
                                conFont[parm[0]] =  'ATARI';
                                conFontCP[parm[0]] = 'RAW';
                                break;

                            case 37: // P0T NOoDLE (Amiga)
                                conFont[parm[0]] = 'P0TNOODLE';
                                conFontCP[parm[0]] = 'RAW';
                                break;

                            case 38: // mO'sOul (Amiga)
                                conFont[parm[0]] = 'MOSOUL';
                                conFontCP[parm[0]] = 'RAW';
                                break;

                            case 39: // MicroKnight Plus (Amiga)
                                conFont[parm[0]] = 'MICROKNIGHTPLUS';
                                conFontCP[parm[0]] = 'RAW';
                                break;

                            case 40: // Topaz Plus (Amiga)
                                conFont[parm[0]] = 'TOPAZPLUS';
                                conFontCP[parm[0]] = 'RAW';
                                break;

                            case 41: // MicroKnight (Amiga)
                                conFont[parm[0]] = 'MICROKNIGHT';
                                conFontCP[parm[0]] = 'RAW';
                                break;

                            case 42: // Topaz (Amiga)
                                conFont[parm[0]] = 'TOPAZ';
                                conFontCP[parm[0]] = 'RAW';
                                break;

                            case 100: // TI994 (TI-99/4)
                                conFont[parm[0]] = 'TI994';
                                conFontCP[parm[0]] = 'RAWHI';
                                break;

                            case 101: // Teletext
                                conFont[parm[0]] = fontName;
                                conFontCP[parm[0]] = 'TELETEXT';
                                break;
                        }
                        // load it if it's needed.
                        loadSingleFont(conFont[parm[0]]);

                    } else {
                        // move backwards
                        parm = fixParams(parm, [1]);
                        parm[0] = minMax(parm[0], 1, 999);
                        crsrCol -= parm[0];
                        if (crsrCol < 0)
                            crsrCol = 0;
                        crsrrender = true;
                    }
                    break;

                case 0x45:  // E - Next Line
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    crsrCol = 0;
                    crsrRow += parm[0];
                    crsrrender = true;
                    break;

                case 0x46:  // F - Previous Line
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    crsrCol = 0;
                    crsrRow -= parm[0];
                    if (crsrRow < 0)
                        crsrRow = 0;
                    crsrrender = true;
                    break;

                case 0x47:  // G - To Column
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    crsrCol = parm[0] - 1;
                    crsrrender = true;
                    break;

                    // H - see f

                case 0x49:  // I - CFT - forward tab
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    for (i = 0; i < parm[0]; i++) {
                        crsrCol = (((crsrCol >>> 3) + 1) << 3);
                        if (crsrCol > colsOnRow(crsrRow)) {
                            crsrCol = colsOnRow(crsrRow);
                            break;
                        }
                    }
                    crsrrender = true;
                    break;

                case 0x4A:  // J - Erase in Screen (0=EOS,1=SOS,2=ALL)
                    parm = fixParams(parm, [0]);
                    parm[0] = minMax(parm[0], 0, 2, 0);
                    expandToRow(crsrRow);
                    expandToCol(crsrRow, crsrCol);
                    switch (parm[0]) {
                        case 0:
                            // clear EOL first
                            clearHotSpotsRow(crsrRow, crsrCol, 999);
                            conCellAttr[crsrRow].length = crsrCol;
                            conText[crsrRow] = conText[crsrRow].substring(0, crsrCol);
                            // clear EOS
                            clearHotSpotsRows(crsrRow + 1, conRowAttr.length);
                            for (r = getMaxRow(); r > crsrRow; r--) {
                                row = getRowElement(r);
                                row.parentNode.removeChild(row);
                                conRowAttr.length = crsrRow + 1;
                                conCellAttr.length = crsrRow + 1;
                                conText.length = crsrRow + 1;
                                conCanvas.length = crsrRow + 1;
                            }
                            break;

                        case 1:
                            // clear SOL first
                            clearHotSpotsRow(crsrRow, 0, crsrCol-1);
                            for (c = 0; c <= crsrCol; c++)
                                conPutChar(crsrRow, c, 32, defCellAttr);
                            redrawRow(crsrRow);

                            // clear SOS
                            clearHotSpotsRows(0, crsrRow-1);
                            for (r = 0; r < crsrRow; r++) {
                                conRowAttr[r] = defRowAttr;
                                conCellAttr[r] = [];
                                conText[r] = '';
                                adjustRow(crsrRow);
                                redrawRow(crsrRow);
                            }
                            break;

                        case 2:
                            // clear entire screen.
                            // remove html rows
                            els = document.getElementsByClassName('vtx');
                            for (l = els.length, r = l - 1; r >= 0; r--)
                                els[r].parentNode.removeChild(els[r]);
                            // remove sprites
                            els = document.getElementsByClassName('sprite');
                            for (l = els.length, r = l - 1; r >= 0; r--)
                                els[r].parentNode.removeChild(els[r]);
                            // reset console
                            conRowAttr = [];
                            conCellAttr = [];
                            conText = [];
                            conHotSpots = [];
                            conCanvas = [];
                            lastHotSpot = null;
                            document.body.style['cursor'] = 'default';
                            crsrRow = crsrCol = 0
                            crsrrender = true;
                            expandToRow(crsrRow);
                            expandToCol(crsrRow, crsrCol);
                            if (modeVTXANSI)
                                cellAttr = defCellAttr;
                            isSelect = false;
                            break;
                    }
                    break;

                case 0x4B:  // K - Erase in Line
                    parm = fixParams(parm, [0]);
                    parm[0] = minMax(parm[0], 0, 2, 0);
                    expandToRow(crsrRow);
                    expandToCol(crsrRow, crsrCol);
                    switch (parm[0]) {
                        case 0:
                            // clear EOL
                            clearHotSpotsRow(crsrRow, crsrCol, 999);
                            conCellAttr[crsrRow].length = crsrCol;
                            conText[crsrRow] = conText[crsrRow].substring(0, crsrCol);
                            redrawRow(crsrRow);
                            break;

                        case 1:
                            // clear SOL
                            clearHotSpotsRow(crsrRow, 0, crsrCol);
                            for (c = 0; c <= crsrCol; c++)
                                conPutChar(crsrRow, c, 32, defCellAttr);
                            redrawRow(crsrRow);
                            break;

                        case 2:
                            // clear row.
                            clearHotSpotsRow(crsrRow, 0, 999);
                            conText[crsrRow] = '';
                            conCellAttr[crsrRow] = [];
                            redrawRow(crsrRow);
                            break;
                    }
                    break;

                case 0x4C:  // L - EL - insert lines
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    for (i = 0; i < parm[0]; i++)
                        insRow(crsrRow);
                    break;

                case 0x4D:  // M - DL - delete lines
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    for (i = 0; i < parm[0]; i++)
                        delRow(crsrRow);
                    break;

                case 0x50:  // P - DCH - delete character
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    for (i = 0; i < parm[0]; i++)
                        delChar(crsrRow, crsrCol);
                    redrawRow(crsrRow);
                    break;

                case 0x53:  // S - Scroll Up (SU). Scroll up.
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    for (i = 0; i < parm[0]; i++)
                        scrollUp();
                    break;

                case 0x54:  // T - Scroll Down (SD). Scroll down.
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    for (i = 0; i < parm[0]; i++)
                        scrollDown();
                    break;

                case 0x58:  // X - ECH - erase n characters
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    for (i = 0; i < parm[0]; i++) {
                        conPutChar(crsrRow, crsrCol + i, 0x20, defCellAttr);
                    }
                    break;

                case 0x5A:  // Z - CBT - back tab
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    for (i = 0; i < parm[0]; i++) {
                        crsrCol = (((crsrCol >>> 3) + 1) << 3) - 16;
                        if (crsrCol <= 0) {
                            crsrCol = 0;
                            break;
                        }
                    }
                     crsrrender = true;
                     break;

                /* special VTX sequences start */
                case 0x5B:  // [ - Row Size
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 0, 3, 1);
                    conRowAttr[crsrRow] = setRowAttrWidth(conRowAttr[crsrRow], (parm[0] + 1) * 50);
                    adjustRow(crsrRow);
                    crsrrender = true;
                    break;

                case 0x5C:  // \ - hotspots
                    if (l < 2) {
                        // reset all hotspots
                        conHotSpots = [];
                    } else {
                        switch (parm[0]) {
                            case 0: // string binds
                            case 1: // url binds
                                if (l >= 4) {
                                    // need all the parts.
                                    hs = {
                                        type:   parm[0],
                                        row:    crsrRow,
                                        col:    crsrCol,
                                        width:  parm[1],
                                        height: parm[2],
                                        hilite: parm[3],
                                        val:    ''
                                    };
                                    for (i = 4; i < l; i++)
                                        hs.val += String.fromCharCode(parm[i]);
                                    conHotSpots.push(hs);
                                }
                                break;
                        }
                    }
                    break;

                case 0x5D:  // ] - Row Modes / background
                    parm = fixParams(parm, [0,0,0]);
                    parm[0] = minMax(parm[0], 0, 255, 0);
                    parm[1] = minMax(parm[1], 0, 255, 0);
                    parm[2] = minMax(parm[2], 0, 3, 0);
                    conRowAttr[crsrRow] = setRowAttrColor1(conRowAttr[crsrRow], parm[0]);
                    conRowAttr[crsrRow] = setRowAttrColor2(conRowAttr[crsrRow], parm[1]);
                    conRowAttr[crsrRow] = setRowAttrPattern(conRowAttr[crsrRow], parm[2] << 16);

                    // set row attrs here
                    row = getRowElement(crsrRow);
                    c1 = ansiColors[parm[0]];
                    c2 = ansiColors[parm[1]];
                    switch (parm[2] << 16) {
                        case A_ROW_PATTERN_SOLID:
                            row.style['background'] = c1;
                            break;

                        case A_ROW_PATTERN_HORZ:
                            row.style['background'] = 'linear-gradient(to bottom,' + c1 + ',' + c2 + ')';
                            break;

                        case A_ROW_PATTERN_VERT:
                            row.style['background'] = 'linear-gradient(to right,' + c1 + ',' + c2 + ')';
                            break;
                    }
                    break;

                case 0x5E:  // ^ - Cursor / Page Modes
                    if (!parm.length){
                        // no paremeters - reset to default
                    } else {
                        switch (parm[0]) {
                            case 0:// cursor color
                                if (l == 1)
                                    crsrAttr = setCrsrAttrColor(
                                        crsrAttr,
                                        getCrsrAttrColor(defCrsrAttr))
                                else
                                    crsrAttr = setCrsrAttrColor(
                                        crsrAttr,
                                        (parm[1] & 0xFF));
                                newCrsr();
                                break;

                            case 1:// cursor size
                                if (l == 1)
                                    crsrAttr = setCrsrAttrSize(
                                        crsrAttr,
                                        getCrsrAttrSize(defCrsrAttr))
                                else
                                    crsrAttr = setCrsrAttrSize(
                                        crsrAttr,
                                        (parm[1] & 0x03));
                                newCrsr();
                                break;

                            case 2:// cursor orientation
                                if (l == 1)
                                    crsrAttr = setCrsrAttrOrientation(
                                        crsrAttr,
                                        (parm[1] ? A_CRSR_ORIENTATION : 0))
                                else
                                    crsrAttr = setCrsrAttrOrientation(
                                        crsrAttr,
                                        getCrsrAttrOrientation(defCrsrAttr));
                                newCrsr();
                                break;

                            case 3:// page border color
                                if (l == 1)
                                    pageAttr = setPageAttrBorder(
                                        pageAttr,
                                        getPageAttrBorder(defPageAttr))
                                else
                                    pageAttr = setPageAttrBorder(
                                        pageAttr,
                                        (parm[1] & 0xFF));
                                if (cbm)
                                    i = cbmColors[(pageAttr >>> 8) & 0xF]
                                else if (atari)
                                    i = atariColors[(pageAttr >>> 8) & 0x1]
                                else
                                    i = ansiColors[(pageAttr >>> 8) & 0xFF];
                                pageDiv.parentNode.style['background-color'] = i;
                                break;

                            case 4:// page background color
                                if (l == 1)
                                    pageAttr = setPageAttrBackground(
                                        pageAttr,
                                        getOageAttrBackground(defPageAttr))
                                else
                                    pageAttr = setPageAttrBackground(
                                        pageAttr,
                                        (parm[1] & 0xFF));
                                if (cbm)
                                    i = cbmColors[pageAttr & 0xF]
                                else if (atari)
                                    i = atariColors[pageAttr & 0x1]
                                else
                                    i = ansiColors[pageAttr & 0xFF];
                                pageDiv.style['background-color'] = i;
                                break;

                            case 5: // CSI '5^' : Set hotspot mouseover colors.
                                hotspotHoverAttr = cellAttr;
                                break;

                            case 6: // CSI '6^' : Set hotspot click colors.
                                hotspotClickAttr = cellAttr;
                                break;
                        }
                    }
                    crsrrender = true;
                    break;

                case 0x5F:  // _ - VTX Media Codes. Sprites & Audio
                    if (parms.length == 0) {
                        // syncronet got here how?
                        //console.log('CSI '+ parms + interm + String.fromCharCode(chr));
                        break;
                    }
                    else if (parm[0] == 0) {
                        // sprite commands
                        switch (parm[1]) {
                            case 0:
                                // define / clear sprite object.
                                // parm2 = num
                                // parm3 = type
                                // parm4.. = hex3 data (rejoin with ;)
                                if (l == 2) {
                                    // clear all sprite objects.
                                    spriteDefs = [];
                                } else if (l == 3) {
                                    // clear single sprite object.
                                    spriteDefs[parm[2]] = null;
                                } else if (l > 4) {
                                    // define a sprite object
                                    str = '';
                                    switch (parm[3]){
                                        case 0:
                                            // url : unicode encoded characters
                                            for (i = 4; i < l; i++)
                                                str += String.fromCharCode(int(parm[i]));
                                            spriteDefs[parm[2]] = stripNL(str);
                                            break;

                                        case 1:
                                            // UTF8 url : hex3 encoded
                                            for (i = 4; i < l; i++)
                                                str += ';'+parm[i];
                                            str = str.substring(1);
                                            spriteDefs[parm[2]] = stripNL(UTF8ArrayToStr(decodeHex3(str)).strData);
                                            break;

                                        case 2:
                                            // raw UTF8 svg : hex3 encoded
                                            for (i = 4; i < l; i++)
                                                str += ';'+parm[i];
                                            str = str.substring(1);
                                            spriteDefs[parm[2]] = 'data:image/svg+xml;charset=utf-8,'
                                                + encodeURIComponent(stripNL(UTF8ArrayToStr(decodeHex3(str)).strData));
                                            break;

                                        case 3:
                                            // raw UTF8 svg deflated : hex3 encoded
                                            for (i = 4; i < l; i++)
                                                str += ';'+parm[i];
                                            str = str.substring(1);
                                            spriteDefs[parm[2]] = 'data:image/svg+xml;charset=utf-8,'
                                                + encodeURIComponent(stripNL(UTF8ArrayToStr(inflateRaw(decodeHex3(str))).strData));
                                            break;

                                        case 4:
                                            // raw UTF8 svg Base64: hex3 encoded
                                            for (i = 4; i < l; i++)
                                                str += ';'+parm[i];
                                            str = str.substring(1);
                                            spriteDefs[parm[2]] = 'data:image/svg+xml;base64,'
                                                + stripNL(UTF8ArrayToStr(decodeHex3(str)).strData);
                                            break;
                                    }
                                }
                                break;

                            case 1:
                                // display / remove sprite from display.
                                if (l == 2) {
                                    // remove all sprints from display
                                    els = document.getElementsByClassName('sprite');
                                    for (i = els.length - 1; i >= 0; i--)
                                        els[i].parentNode.removeChild(els[i]);
                                } else if (l == 3) {
                                    // remove sprite s from display
                                    div = document.getElementById('sprite' + parm[2]);
                                    if (div != null)
                                        div.parentNode.removeChild(div);
                                } else if (l == 7) {
                                    // display a new sprite
                                    // remove old one if it exists first
                                    div = document.getElementById('sprite' + parm[2]);
                                    if (div != null)
                                        div.parentNode.removeChild(div);

                                    rpos = getElementPosition(getRowElement(crsrRow));
                                    csize = getRowFontSize(crsrRow);
                                    spriteTop = rpos.top - pageTop;
                                    spriteLeft = crsrCol * csize.width;

                                    // make a new one.
                                    div = domElement(
                                        'div',
                                        {   className:  'sprite',
                                            row :       crsrRow,
                                            col :       crsrCol,
                                            id :        'sprite' + parm[2] },
                                        {   position:   'absolute',
                                            left:       spriteLeft + 'px',
                                            top:        spriteTop + 'px',
                                            backgroundColor: 'green',
                                            width:      (colSize * parm[4]) + 'px',
                                            height:     (rowSize * parm[5]) + 'px',
                                            overflow:   'hidden'});

                                    img = domElement(
                                        'img',
                                        {   onload:     fitSVGToDiv,
                                            src:        spriteDefs[parm[3]] },
                                        {   visibility: 'hidden',
                                            position:   'relative',
                                            left:'0px',
                                            top:'0px'
                                        });

                                    div.appendChild(img);
                                    if (parm[6] == 0)
                                        pageDiv.insertBefore(div, textDiv)
                                    else
                                        textDiv.appendChild(div);
                                }
                                break;

                            case 2:
                                // move sprite to new r c.
                                el = document.getElementById('sprite'+parm[2]);
                                if ((el) && (l == 6))
                                    moveSprite(el, parm[3], parm[4], parm[5]);
                                break;

                            case 3:
                                // move sprite to new z.
                                // TODO
                                /*
                                    CSI 0 ; 3 ; s ; z '_' : Move sprite s to new z-plane.
                                        s : Sprite number to assign to this sprite. {0}
                                        z : Z-plane. 0 = below text plane, 1 : above text plane. {0}
                                */
                                break;
                        }

                    } else if (parm[0] == 1) {
                        // audio commands
                        switch (parm[1]) {
                            case 0:
                                // define / clear audio object.
                                // parm2 = num
                                // parm3 = type
                                // parm4.. = hex3 data (rejoin with ;)
                                if (l == 2) {
                                    // clear all audio objects.
                                    audioDefs = [];
                                } else if (l == 3) {
                                    // clear audio object num
                                    audioDefs[parm[2]] = null;
                                } else if (l > 4) {
                                    // define audio object
                                    str = '';
                                    switch (parm[3]) {
                                        case 0:
                                            // url : unicode encoded characters
                                            for (i = 4; i < l; i++)
                                                str += String.fromCharCode(int(parm[i]));
                                            audioDefs[parm[2]] = str;
                                            break;

                                        case 1:
                                            // UTF8 url : hex3 encoded
                                            for (i = 4; i < l; i++)
                                                str += ';'+parm[i];
                                            str = str.substring(1);
                                            audioDefs[parm[2]] = UTF8ArrayToStr(decodeHex3(str)).strData;
                                            break;

                                        case 2:
                                            // raw mp3 : hex3 encoded
                                            // TODO
                                            audioDefs[parm[2]] =
                                                'data:audio/mp3;base64,' +
                                                btoa(decodeHex3(str));
                                            break;

                                        case 3:
                                            // raw mp3 deflated : hex3 encoded
                                            // TODO
                                            audioDefs[parm[2]] =
                                                'data:audio/mp3;base64,' +
                                                btoa(inflateRaw(decodeHex3(str)));
                                            break;

                                        case 4:
                                            // raw MP3 Base64: hex3 encoded
                                            // TODO
                                            audioDefs[parm[2]] = 'data:audio/mp3;base64,' +
                                                + stripNL(UTF8ArrayToStr(decodeHex3(str)).strData);
                                            break;
                                    }
                                }
                                break;

                            case 1:
                                // select audio object to player.
                                if (l > 2) {
                                    audioEl.src = audioDefs[parm[2]];
                                }
                                break;

                            case 2:
                                // play / pause / stop-rewind
                                switch (parm[2]) {
                                    case 0:
                                        // stop/rewind
                                        audioEl.pause();
                                        audioEl.load();
                                        break;

                                    case 1:
                                        // play
                                        audioEl.play();
                                        break;

                                    case 2:
                                        // pause
                                        audioEl.pause();
                                        break;
                                }
                                break;

                            case 3:
                                // set volume (0-100)
                                audioEl.volume = ((parm[2]!=null)?(parm[2]/100):0.25);
                                break;
                        }
                    }
                    break;
                /* special VTX sequences end */

                case 0x62:  // b - repeat last char
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    for (i = 0; i < parm[0]; i++) {
                        conPrintChar(lastChar);
                    }
                    crsrrender = true;
                    break;

                case 0x63:  // c - device attributes
                    parm = fixParams(parm, [0]);
                    if (parm[0] == 0) {
                        // request device
                        sendData(CSI + '?50;86;84;88c'); // reply for VTX
                    }
                    break;

                case 0x66:  // f - Cursor Position
                case 0x48:  // H - Cursor Position
                    // set missing to defaults of 1
                    parm = fixParams(parm, [ 1, 1 ]);
                    parm[0] = minMax(parm[0], 1, 999);
                    parm[1] = minMax(parm[1], 1, 999);
                    while (l < 2)
                        parm[l++] = 1;
                    if (modeRegionOrigin)
                        crsrRow = regionTopRow + parm[0] - 1
                    else
                        crsrRow = parm[0] - 1;
                    crsrCol = parm[1] - 1;
                    expandToRow(crsrRow);
                    crsrrender = true;
                    break;

                case 0x68:  // h - set mode
                case 0x6C:  // l - reset mode
                    if (parm.length == 0) break;
                    parm[0] = parm[0].toString();
                    switch (parm[0]) {
                        case '?6':
                            // origin in region?
                            modeRegionOrigin = (chr == 0x68);
                            modeRegion = true;
                            break;

                        case '?7':
                            // autowrap mode
                            modeAutoWrap = (chr == 0x68);
                            break;

                        case '?25':
                            // hide / show cursor
                            modeCursor = (chr == 0x68);
                            break;

                        case '?31':
                            // bright as font 1
                            modeBoldFont = (chr == 0x68);
                            break;

                        case '?32':
                            // bright enable/disable
                            modeNoBold = (chr == 0x68);
                            break;

                        case '?33':
                            // blink to high intensity background
                            modeBlinkBright = (chr == 0x68);
                            break;

                        case '?34':
                            //  blink as font 2
                            modeBlinkFont = (chr == 0x68);
                            break;

                        case '?35':
                            // '?35' : blink disabled
                            modeNoBlink = (chr == 0x68);
                            break;

                        case '?50':
                            // VTX / ANSIBBS mode flip
                            modeVTXANSI = (chr == 0x68);
                            break;

                        case '?51':
                            // Teletext burst mode. (ESC to exit mode)
                            crsrCol = 0;
                            cellSaveAttr = cellAttr; // save attributes
                            modeSaveAutoWrap = modeAutoWrap;
                            modeAutoWrap = false;
                            ttxBottom = false;
                            modeTeletext = true;
                            break;

                        case '=255':
                            // =255 : DOORWAY mode
                            modeDOORWAY = (chr == 0x68);
                            break;
                    }
                    break;

                case 0x6D:  // m - Character Attr
                    // don't use fixparms. variable parameters.
                    if (l == 0) parm[l++] = 0;
                    parm[0] = minMax(parm[0], 0, 255);
                    for (i = 0; i < l; i++) {
                        switch (parm[i]) {
                            case 0:     // reset
                                cellAttr = defCellAttr;
                                break;

                            case 1:     // bold on / off
                            case 21:
                                cellAttr =
                                    setCellAttrBold(cellAttr, (parm[i] < 20));
                                break;

                            case 2:     // faint on / off
                            case 22:
                                cellAttr =
                                    setCellAttrFaint(cellAttr, (parm[i] < 20));
                                break;

                            case 3:     // italics on/off
                            case 23:
                                cellAttr =
                                    setCellAttrItalics(cellAttr, (parm[i] < 20));
                                break;

                            case 4:     // underline
                            case 24:
                                cellAttr =
                                    setCellAttrUnderline(cellAttr, (parm[i] < 20));
                                break;

                            case 5:     // blink slow
                                cellAttr &= ~(A_CELL_BLINKSLOW | A_CELL_BLINKFAST);
                                cellAttr |= A_CELL_BLINKSLOW;
                                break;

                            case 6:     // blink fast
                                cellAttr &= ~(A_CELL_BLINKSLOW | A_CELL_BLINKFAST);
                                cellAttr |= A_CELL_BLINKFAST;
                                break;

                            case 25:    // all blink off
                            case 26:    // all blink off (reserved but unblink)
                                cellAttr &= ~(A_CELL_BLINKSLOW | A_CELL_BLINKFAST);
                                break;

                            case 7:     // reverse video
                            case 27:
                                cellAttr =
                                    setCellAttrReverse(cellAttr, (parm[i] < 20));
                                break;

                            case 8:     // conceal
                            case 28:
                                cellAttr = setCellAttrDisplay(cellAttr,
                                    (parm[i] < 20) ? A_CELL_DISPLAY_CONCEAL : A_CELL_DISPLAY_NORMAL);
                                break;

                            case 9:     // strikethrough
                            case 29:
                                cellAttr =
                                    setCellAttrStrikethrough(cellAttr, (parm[i] < 20));
                                break;

                            // select font
                            case 10: case 11: case 12: case 13: case 14:
                            case 15: case 16: case 17: case 18: case 19:
                                conFontNum = (parm[i] - 10);
                                cellAttr = setCellAttrFont(cellAttr, (parm[i] - 10));
                                break;

                            case 56:    // doublestrike
                            case 76:
                                cellAttr =
                                    setCellAttrDoublestrike(cellAttr, (parm[i] < 70));
                                break;

                            case 57:    // shadow
                            case 77:
                                cellAttr =
                                    setCellAttrShadow(cellAttr, (parm[i] < 70));
                                break;

                            case 58:    // top half
                            case 78:
                                cellAttr = setCellAttrDisplay(cellAttr,
                                    (parm[i] < 70) ? A_CELL_DISPLAY_TOP : A_CELL_DISPLAY_NORMAL);
                                break;

                            case 59:    // bottom half
                            case 79:
                                // turn this character attribute off.
                                cellAttr = setCellAttrDisplay(cellAttr,
                                    (parm[i] < 70) ? A_CELL_DISPLAY_BOTTOM : A_CELL_DISPLAY_NORMAL);
                                break;

                            // special built in fonts
                            case 80: // teletext codepage text only
                            case 81: // teletext codepage contiguous blocks
                            case 82: // teletext codepage separated blocks
                            case 83: // reserved
                            case 84: // reserved
                            case 85: // reserved
                                conFontNum = (parm[i] - 70);
                                cellAttr = setCellAttrFont(cellAttr, (parm[i] - 70));
                                break;

                            // text foreground colors
                            case 30: case 31: case 32: case 33:
                            case 34: case 35: case 36: case 37:
                                // foreground color (0-7)
                                cellAttr = setCellAttrFG(cellAttr, parm[i] - 30)
                                break;

                            case 38:
                                // check for 5 ; color
                                if (++i < l)
                                    if (parm[i] == 5)
                                        if (++i < l) {
                                            parm[i] = minMax(parm[i], 0, 255, 7);
                                            cellAttr = setCellAttrFG(cellAttr, parm[i]);
                                        }
                                break;

                            case 39:
                                // default
                                cellAttr =
                                    setCellAttrFG(cellAttr, getCellAttrFG(defCellAttr));
                                break;

                            case 90: case 91: case 92: case 93:
                            case 94: case 95: case 96: case 97:
                                // foreground color (8-15)
                                cellAttr = setCellAttrFG(cellAttr, parm[i] - 90 + 8);
                                break;

                            // text background colors
                            case 40: case 41: case 42: case 43:
                            case 44: case 45: case 46: case 47:
                                // background color (0-7)
                                cellAttr = setCellAttrBG(cellAttr, parm[i] - 40)
                                break;

                            case 48:
                                // check for 5 ; color
                                if (++i < l)
                                    if (parm[i] == 5)
                                        if (++i < l) {
                                            parm[i] = minMax(parm[i], 0, 255, 7);
                                            cellAttr = setCellAttrBG(cellAttr, parm[i]);
                                        }
                                break;

                            case 49:
                                // default
                                cellAttr =
                                    setCellAttrBG(cellAttr, getCellAttrBG(defCellAttr));
                                break;

                            case 100: case 101: case 102: case 103:
                            case 104: case 105: case 106: case 107:
                                // background color (8-15)
                                cellAttr = setCellAttrBG(cellAttr, parm[i] - 100 + 8);
                                break;
                        }
                    }
                    break;

                case 0x6E:  // n DSR - device status report
                    parm = fixParams(parm, [1]);
                    parm[0] = minMax(parm[0], 1, 999);
                    switch (parm[0]) {
                        case 5:
                            sendData(CSI + '0n');
                            break;

                        case 6:
                            sendData(CSI + (crsrRow+1) + ';' + (crsrCol+1) + 'R');
                            break;

                        case 255:
                            sendData(CSI + (crtRows) + ';' + (crtCols) + 'R');
                            break;
                    }
                    break;

                case 0x72:  // r
                    if (interm == '*') {
                        // *r - emulate baud
                        // assuming if p1 < 2 then use p2, else reset to full speed.
                        // ps1 : nil,0,1 = host transmit, 2=host recieve, 3=printer
                        //      4=modem hi, 5=modem lo
                        // ps2 : nil,0=full speed, 1=300, 2=600,3=1200,4=2400,5=4800,
                        //      6=9600,7=19200,8=38400,9=57600,10=76800,11=115200
                        parm = fixParams(parm, [ 0, 0 ]);
                        if (parm[0] < 2) {
                            setTimers(0);
                            conBaud = bauds[parm[1]] * 100;
                            setTimeout(
                                function(){
                                    setTimers(1);
                                }, 25);
                        }
                    } else if (interm == '') {
                        // CSI t ; b 'r' : Set scroll window (DECSTBM).
                        if (parm.length == 0) {
                            modeRegion = false;
                            regionTopRow = 0;
                            regionBottomRow = crtRows - 1;
                        } else {
                            modeRegion = true;
                            parm = fixParams(parm, [ 1, 1 ]);
                            parm[0] = minMax(parm[0], 1, crtRows);
                            parm[1] = minMax(parm[1], parm[0], crtRows);
                            regionTopRow = parm[0] - 1;
                            regionBottomRow = parm[1] - 1;
                        }
                    }
                    break;

                case 0x73:  // s - Save Position
                    crsrSaveRow = crsrRow;
                    crsrSaveCol = crsrCol;
                    break;

                case 0x75:  // u - Restore Position
                    crsrRow = crsrSaveRow;
                    crsrCol = crsrSaveCol;
                    crsrrender = true;
                    break;

                default:
                    // unsupported - ignore
                    //console.log('unsupported ansi : CSI ' + String.fromCharCode(chr));
                    break;
            }
        }
    }
    if (crsrrender)
        crsrDraw();
}

// animate move sprite to nr, nc over t time.
function moveSprite(el, nr, nc, t){
    var
        rpos = getElementPosition(getRowElement(nr)),
        csize = getRowFontSize(nr),
        cx = int(el.style['left']),
        cy = int(el.style['top']),
        steps, tmr,
        sel,
        nx, ny;

    if (t < 0) t = 0;
    steps = Math.round(t / 15);
    nx = nc * csize.width;
    ny = rpos.top - pageTop;

    // generate new keyframe
    sel = domElement('style',{},{},
        '@keyframes ' + el.id + ' { '
        + 'from { left: ' + cx + 'px; top: ' + cy + 'px; } '
        + 'to { left : ' + nx + 'px; top: ' + ny + 'px;}} ');
    el.appendChild(sel);
    el.style['animation-duration'] = t + 'ms';
    el.style['animation-name'] = el.id;
    el.style['animation-iteration-count'] = '1';
    el.style['animation-fill-mode'] = 'forwards';
    setTimeout(function(){
        el.style['left'] = nx + 'px';
        el.style['top'] = ny + 'px';
        el.style['animation-name'] = null;
        el.style['animation-duration'] = null;
        el.style['animation-iteration-count'] = null;
        el.style['animation-fill-mode'] = null;
        el.removeChild(sel);
    }, t + 5);
}

function getAnsiLength(str) {
    var str2 =  str.replace(/\e\[.*[@-_]/g, '');
    return str.length;
}



// remove all NL from string. (https://www.chromestatus.com/features/5735596811091968)
function stripNL(strin) {
    var strout = strin.replace(/\n/g,' ').replace(/\s+/g, ' ');
    return strout;
}

// decode hex3 string, return as Uint8Array
// remove whitespaces. Note: string data in Hex3 is UTF8
function decodeHex3(strin) {
    var
        ret,    // decoded result
        i, l,   // idx / length
        p;      // ptr into strin

    strin = strin.replace(/\s/g,'');
    ret = new Uint8Array(strin.length >>> 1);
    for (p = 0, i = 0, l = ret.length; i < l; i++) {
        ret[i] = ((strin.charCodeAt(p    ) & 0x0F) << 4)
               | ((strin.charCodeAt(p + 1) & 0x0F));
        p += 2;
    }
    return ret;
}

// call once every 20 ms
function doWriteBuffer() {
    var
        strOut,
        bytes;

    if (conBuffer.length > 0) {
        // how many bytes to send since last call.
        if (conBaud == 0) {
            strOut = conBuffer;
            conBuffer = '';
        } else {
            bytes = conBaud / 500;
            if (conBuffer.length < bytes){
                strOut = conBuffer;
                conBuffer = '';
            } else {
                strOut = conBuffer.substring(0,bytes);
                conBuffer = conBuffer.substring(bytes);
            }
        }
        conStrOut(strOut);
    }
}

// do all writing here.
function conBufferOut(data) {
    conBuffer += data;
    if (conBaud == 0) {
        conStrOut(conBuffer);
        conBuffer = '';
    }
}

// write string using current attributes at cursor position. ###CALL conBufferOut!###
function conStrOut(str) {
    var
        oldSpeed,
        l, i;

    str = str || '';
    l = str.length;
    for (i = 0; i < l; i++) {
        oldSpeed = conBaud;
        conCharOut(str.charCodeAt(i));
        if (conBaud != oldSpeed) {
            // move rest back to buffer!
            conBuffer = str.substring(i+1) + conBuffer;
            break;
        }
    }
}

// perform telnet handshaking - return data with telnet commands removed.
// must be state machine.
// data is Uint8Array. return UInt8Array
function tnNegotiate(data) {
    var
        v0, v1, v2, v3,
        i, l, b, outdata, outp;

    outdata = new Uint8Array(data.length);
    outp = 0;   // pointer into output
    l = data.length;
    for (i = 0; i < l; i++) {
        b = data[i];
        switch (tnState) {
            case 0:
                // not in any state. looking for IAC's
                if (b == TN_IAC)
                    tnState++
                else
                    outdata[outp++] = b;
                break;

            case 1:
                // recieved a TN_IAC
                switch (b) {
                    case TN_IAC:
                        // escaped 0xFF.
                        outdata[outp++] = b;
                        tnState = 0;
                        break;

                    case TN_SE:    // subneg end
                    case TN_NOP:   // no operation
                    case TN_DM:    // data mark
                    case TN_BRK:   // break
                    case TN_IP:    // interrupt process
                    case TN_AO:    // abort output
                    case TN_AYT:   // are you there? try responding with NUL
                    case TN_EC:    // erase char
                    case TN_EL:    // erase line
                    case TN_GA:    // go ahead
                        // ignore for now.
                        tnState = 0;
                        break;

                    case TN_SB:
                        // subneg begin
                        tnState = 3;
                        break;

                    case TN_WILL:
                    case TN_WONT:
                    case TN_DO:
                    case TN_DONT:
                        tnCmd = b;
                        tnState = 2;
                        break;

                    default:
                        // ??
                        outdata[outp++] = b;
                        tnState = 0;
                        break;
                }
                break;

            case 2:
                // have IAC + cmd so far
                switch (tnCmd) {
                    case TN_WILL:
                        // server would like to do something. send DO or DONT
                        if (tnQUs[b] == TNQ_WANTYES)
                            tnQUs[b] = TNQ_YES
                        else {
                            switch (b) {
                                case TN_BIN:    // binary
                                case TN_SGA:    // suppress go ahead
                                case TN_ECHO:   // echo
                                case TN_NAWS:   // negotiate about window size
                                case TN_TTYPE:  // terminal type
                                case TN_TSPEED: // terminal speed
case TN_NEWE:
                                    tnSendCmd(TN_DO, b);
                                    tnQHim[b] = TNQ_YES;
                                    break;

                                default:
                                    tnSendCmd(TN_DONT, b);
                                    tnQHim[b] = TNQ_NO;
                                    break;
                            }
                        }
                        break;

                    case TN_WONT:
                        if ((tnQUs[b] == TNQ_WANTYES) || (tnQUs[b] == TNQ_WANTNO))
                            // response to my request to do or dont
                            tnQUs[b] = TNQ_NO
                        else {
                            // server wants to not do
                            tnSendCmd(TN_DONT, b);
                            tnQHim[b] = TNQ_NO;
                        }
                        break;

                    case TN_DO:
                        if (tnQUs[b] == TNQ_WANTYES) {
                            // response to my request to will
                            tnQUs[b] = TNQ_YES;

                            // send some SB stuff now
                            if (b == TN_NAWS) {
                                v0 = crtCols & 0xFF;
                                v1 = (crtCols >>> 8) & 0xFF;
                                v2 = crtRows & 0xFF;
                                v3 = (crtRows >>> 8) & 0xFF;
                                tnSendCmd(TN_SB, TN_NAWS, v1, v0, v3, v2);
                                tnSendCmd(TN_SE);
                            }
                        } else {
                            // server wants us to do
                            switch (b) {
                                case TN_BIN:
                                case TN_SGA:
                                case TN_ECHO:
                                case TN_NAWS:
                                case TN_TTYPE:
                                case TN_TSPEED:
case TN_NEWE:
                                    tnSendCmd(TN_WILL, b);
                                    tnQUs[b] = TNQ_YES;
                                    break;

                                default:
                                    tnSendCmd(TN_WONT, b);
                                    tnQUs[b] = TNQ_NO;
                                    break;
                            }
                        }
                        break;

                    case TN_DONT:
                        // server wants me to not do something. send WONT
                        if (tnQUs[b] == TNQ_WANTYES)
                            // response to my request to will
                            tnQUs[b] = TNQ_NO
                        else {
                            // server wants us to not do. respond WONT
                            tnSendCmd(TN_WONT, b);
                            tnQUs[b] = TNQ_NO;
                        }
                        break;
                }
                tnState = 0;
                break;

            case 3:
                // have IAC SB
                tnCmd = b;
                tnState = 4;
                break;

            case 4:
                // this should be SEND (1)
                if (b == TN_SEND)
                    switch (tnCmd) {
                        case TN_TTYPE:
                            // will neg term type;
                            tnSendCmd(TN_SB, tnCmd, TN_IS, vtxdata.term);
                            tnSendCmd(TN_SE);
                            break;

                        case TN_TSPEED:
                            // will neg terminal speed
                            tnSendCmd(TN_SB, tnCmd, TN_IS, '921600,921600');
                            tnSendCmd(TN_SE);
                            break;

                        case TN_NEWE:
                            // new environment
                            tnSendCmd(TN_SB, tnCmd, TN_IS, 0);
                            tnSendCmd(TN_SE);
                            break;

                        default:
                            // ? why are we being asked this? server on drugs?
                            tnSendCmd(TN_DONT, tnCmd);
                            outdata[outp++] = b;
                            break;
                    }
                tnState = 0;
                break;
        }
    }
    // truncate outdata at outp.
    return outdata.slice(0, outp);
}

// send telnet commands.. send bytes or strings as parameters
function tnSendCmd() {
    var
        i, j,
        l1, l2,
        outbuff,
        bytebuff = [];

    bytebuff.push(TN_IAC);
    l1 = arguments.length;
    for (i = 0; i < l1; i++)
        if (typeof arguments[i] === 'string') {
            // put string into bytebuff
            l2 = arguments[i].length;
            for (j = 0; j < l2; j++)
                bytebuff.push(arguments[i].charCodeAt(j));
        } else if (typeof arguments[i] === 'number')
            // put this byte into bytebuff
            bytebuff.push(arguments[i]);

    // send bytebuff
    l1 = bytebuff.length;
    outbuff = new Uint8Array(l1);
    for (i = 0; i < l1; i++)
        outbuff[i] = bytebuff[i];
    ws.send(outbuff);
}

// send initial barrage of settings.
function tnInit() {
    var
        i;

    tnQHim.fill(TNQ_NO);
    tnQUs.fill(TNQ_NO);

    // set initial telnet options
    tnQUs[TN_BIN] =     TNQ_WANTYES;
    tnQHim[TN_BIN] =    TNQ_WANTYES;
    tnQUs[TN_SGA] =     TNQ_WANTYES;
    tnQHim[TN_SGA] =    TNQ_WANTYES;
    tnQHim[TN_ECHO] =   TNQ_WANTYES;
    tnQUs[TN_NAWS] =    TNQ_WANTYES;
    tnQUs[TN_TTYPE] =   TNQ_WANTYES;
    tnQUs[TN_TSPEED] =  TNQ_WANTYES;

    for (i = 0; i < 256; i++) {
        if (tnQUs[i] == TNQ_WANTYES)
            tnSendCmd(TN_WILL, i)
        else if (tnQUs[i] == TNQ_WANTNO)
            tnSendCmd(TN_WONT, i);

        if (tnQHim[i] == TNQ_WANTYES)
            tnSendCmd(TN_DO, i)
        else if (tnQHim[i] == TNQ_WANTNO)
            tnSendCmd(TN_DONT, i);
    }
}

addListener(window, 'load', bootVTX);

var
    fsClientParent,
    fsClientStyle,
    fsClientPlaceHolder,
    fsFontSize,
    modeFullScreen = false;

// find required font size given a destop width / height
// crtRows and crtCols must fill best area.
function toggleFullScreen() {
    var
        i, l,
        ch,
        cw,
        st,
        fs,
        el = document.body;

    if (modeFullScreen) {
        // turn if off. restore to original state.
        // restore style
        clientDiv.style.cssText = fsClientStyle;
        fxDiv.style['position'] = 'absolute';

        if (document.cancelFullscreen)
            document.cancelFullscreen()
        if (document.exitFullscreen)
            document.exitFullscreen()
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen()
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen()
        else if (document.msExitFullscreen)
            document.msExitFullscreen();

        // move it back
        fsClientParent.insertBefore(clientDiv, fsClientPlaceHolder);
        fsClientParent.removeChild(fsClientPlaceHolder);

        modeFullScreen = false;

        vtxdata.fontSize = fsFontSize;
        fontSize = fsFontSize;
    } else {

        // turn it on.
        if (el.requestFullscreen)
            el.requestFullscreen()
        else if(el.mozRequestFullScreen)
            el.mozRequestFullScreen()
        else if(el.webkitRequestFullscreen)
            el.webkitRequestFullscreen()
        else if(el.msRequestFullscreen)
            el.msRequestFullscreen();

        // mark our place.
        fsClientParent = clientDiv.parentNode;
        fsClientPlaceHolder = domElement('div', {}, { width: '0', height: '0' });
        fsClientParent.insertBefore(fsClientPlaceHolder, clientDiv);

        // save styles
        fsClientStyle = clientDiv.style.cssText;
        fsFontSize = vtxdata.fontSize;

        // set full screen styles
        clientDiv.style['position'] = 'absolute';
        clientDiv.style['top'] = '0';
        clientDiv.style['left'] = '0';
        clientDiv.style['right'] = '0';
        clientDiv.style['bottom'] = '0';
        clientDiv.style['overflow-y'] = 'auto';
        fxDiv.style['position'] = 'fixed';
        fxDiv.style['z-index'] = '9';

        // move client
        document.body.appendChild(clientDiv);

        modeFullScreen = true;

        // compute new font size to fit screen
        ch = screen.height - 64;
        cw = screen.width - 64;
        fs = Math.floor(ch / crtRows / 8) * 8;

        // shrink to width if needed
        while (((fs / (fontSize / colSize)) * crtCols * xScale) > cw)
            fs -= 2;

        // some figuring ..
        vtxdata.fontSize = fs;
        fontSize = fs;
    }
    setBulbs();

    // determine standard sized font width in pixels
    getDefaultFontSize(); // get fontName, colSize, rowSize
    crtWidth = colSize * crtCols;

    // redraw everything!
    pageDiv.style['width'] = (crtWidth * xScale) + 'px';
    ctrlDiv.style['width'] = (crtWidth * xScale / 2) + 'px',

    pagePos = getElementPosition(pageDiv);
    pageLeft = pagePos.left;
    pageTop = pagePos.top;

    // alter the css.
    setAllCSS();
    l = conRowAttr.length;
    for (i = 0; i < l; i++) {
        adjustRow(i);
    }
    crsrDraw();
}

function createCookie(name, val, days) {
    var
        date,
        expires = "";
    const
        MSTODAYS = 24 * 60 * 60 * 1000;
    if (days) {
        date = new Date();
        date.setTime(date.getTime() + (days * MSTODAYS));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + val + expires + "; path=/";
}

function readCookie(name) {
    var
        i, c,
        nameEQ = name + "=",
        ca = document.cookie.split(';');
    for (i = 0; i < ca.length; i++) {
        c = ca[i];
        while (c.charAt(0)==' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}

};
var $R = [
	"Method %s in class %s threw exception [%s]",
	"Procedure %s threw exception [%s]",
	"Host classtype was rejected as unsuitable",
	"Invalid handle for operation, reference was null error",
	"Invalid stream style for operation, expected memorystream",
	"Method not implemented",
	"stream operation failed, system threw exception: %s",
	"write failed, system threw exception: %s",
	"read failed, system threw exception: %s",
	"operation failed, invalid handle error",
	"Invalid length, %s bytes exceeds storage medium error",
	"Read failed, invalid signature error [%s]",
	"Invalid length, %s bytes exceeds storage boundaries error",
	"Write failed, invalid signature error [%s]",
	"Write failed, invalid datasize [%d] error",
	"File operation [%s] failed, system threw exception: %s",
	"Structure %s error, method %s.%s threw exception [%s] error",
	"Structure storage failed, structure contains function reference error",
	"Structure storage failed, structure contains symbol reference error",
	"Structure storage failed, structure contains uknown datatype error",
	"Failed to read structure, method %s.%s threw exception [%s] error",
	"Failed to write structure, method %s.%s threw exception [%s] error",
	"Structure data contains invalid or damaged data signature error",
	"Invalid dictionary key",
	"Invalid array of dictionary keys",
	"Filesystem is nil error",
	"Filesystem not mounted error",
	"Read failed, invalid offset [%d], expected %d..%d",
	"Write operation failed, target stream is nil error",
	"Read operation failed, source stream is nil error",
	"'Invalid handle for object (%s), reference rejected error",
	"Failed to convert typed-array: expected %d bytes, read %d. Insufficient data error",
	"Failed to process data, reference value was nil or unassigned error",
	"0123456789",
	"0123456789ABCDEF"];
function Trim$_String_(s) { return s.replace(/^\s\s*/, "").replace(/\s\s*$/, "") }
var TObject={
	$ClassName: "TObject",
	$Parent: null,
	ClassName: function (s) { return s.$ClassName },
	ClassType: function (s) { return s },
	ClassParent: function (s) { return s.$Parent },
	$Init: function (s) {},
	Create: function (s) { return s },
	Destroy: function (s) { for (var prop in s) if (s.hasOwnProperty(prop)) delete s[prop] },
	Destroy$: function(s) { return s.ClassType.Destroy(s) },
	Free: function (s) { if (s!==null) s.ClassType.Destroy(s) }
}
function StrReplace(s,o,n) { return o?s.replace(new RegExp(StrRegExp(o), "g"), n):s }
function StrRegExp(s) { return s.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&") }
function StrEndsWith(s,e) { return s.substr(s.length-e.length)==e }
function StrBeginsWith(s,b) { return s.substr(0, b.length)==b }
function SameText(a,b) { return a.toUpperCase()==b.toUpperCase() }
function RightStr(s,n) { return s.substr(s.length-n) }
function Now() { var d=new Date(); return d.getTime()/864e5+25569 }
function IntToHex2(v) { var r=v.toString(16); return (r.length==1)?"0"+r:r }
/**
sprintf() for JavaScript 0.7-beta1
http://www.diveintojavascript.com/projects/javascript-sprintf

Copyright (c) Alexandru Marasteanu <alexaholic [at) gmail (dot] com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of sprintf() for JavaScript nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Alexandru Marasteanu BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

var sprintf = (function() {
	function get_type(variable) {
		return Object.prototype.toString.call(variable).slice(8, -1).toLowerCase();
	}
	function str_repeat(input, multiplier) {
		for (var output = []; multiplier > 0; output[--multiplier] = input) {/* do nothing */}
		return output.join('');
	}

	var str_format = function() {
		if (!str_format.cache.hasOwnProperty(arguments[0])) {
			str_format.cache[arguments[0]] = str_format.parse(arguments[0]);
		}
		return str_format.format.call(null, str_format.cache[arguments[0]], arguments);
	};

	str_format.format = function(parse_tree, argv) {
		var cursor = 1, tree_length = parse_tree.length, node_type = '', arg, output = [], i, k, match, pad, pad_character, pad_length;
		for (i = 0; i < tree_length; i++) {
			node_type = get_type(parse_tree[i]);
			if (node_type === 'string') {
				output.push(parse_tree[i]);
			}
			else if (node_type === 'array') {
				match = parse_tree[i]; // convenience purposes only
				if (match[2]) { // keyword argument
					arg = argv[cursor];
					for (k = 0; k < match[2].length; k++) {
						if (!arg.hasOwnProperty(match[2][k])) {
							throw(sprintf('[sprintf] property "%s" does not exist', match[2][k]));
						}
						arg = arg[match[2][k]];
					}
				}
				else if (match[1]) { // positional argument (explicit)
					arg = argv[match[1]];
				}
				else { // positional argument (implicit)
					arg = argv[cursor++];
				}

				if (/[^s]/.test(match[8]) && (get_type(arg) != 'number')) {
					throw(sprintf('[sprintf] expecting number but found %s', get_type(arg)));
				}
				switch (match[8]) {
					case 'b': arg = arg.toString(2); break;
					case 'c': arg = String.fromCharCode(arg); break;
					case 'd': arg = String(parseInt(arg, 10)); if (match[7]) { arg = str_repeat('0', match[7]-arg.length)+arg } break;
					case 'e': arg = match[7] ? arg.toExponential(match[7]) : arg.toExponential(); break;
					case 'f': arg = match[7] ? parseFloat(arg).toFixed(match[7]) : parseFloat(arg); break;
					case 'o': arg = arg.toString(8); break;
					case 's': arg = ((arg = String(arg)) && match[7] ? arg.substring(0, match[7]) : arg); break;
					case 'u': arg = Math.abs(arg); break;
					case 'x': arg = arg.toString(16); break;
					case 'X': arg = arg.toString(16).toUpperCase(); break;
				}
				arg = (/[def]/.test(match[8]) && match[3] && arg >= 0 ? '+'+ arg : arg);
				pad_character = match[4] ? match[4] == '0' ? '0' : match[4].charAt(1) : ' ';
				pad_length = match[6] - String(arg).length;
				pad = match[6] ? str_repeat(pad_character, pad_length) : '';
				output.push(match[5] ? arg + pad : pad + arg);
			}
		}
		return output.join('');
	};

	str_format.cache = {};

	str_format.parse = function(fmt) {
		var _fmt = fmt, match = [], parse_tree = [], arg_names = 0;
		while (_fmt) {
			if ((match = /^[^\x25]+/.exec(_fmt)) !== null) {
				parse_tree.push(match[0]);
			}
			else if ((match = /^\x25{2}/.exec(_fmt)) !== null) {
				parse_tree.push('%');
			}
			else if ((match = /^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(_fmt)) !== null) {
				if (match[2]) {
					arg_names |= 1;
					var field_list = [], replacement_field = match[2], field_match = [];
					if ((field_match = /^([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
						field_list.push(field_match[1]);
						while ((replacement_field = replacement_field.substring(field_match[0].length)) !== '') {
							if ((field_match = /^\.([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else if ((field_match = /^\[(\d+)\]/.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else {
								throw('[sprintf] huh?');
							}
						}
					}
					else {
						throw('[sprintf] huh?');
					}
					match[2] = field_list;
				}
				else {
					arg_names |= 2;
				}
				if (arg_names === 3) {
					throw('[sprintf] mixing positional and named placeholders is not (yet) supported');
				}
				parse_tree.push(match);
			}
			else {
				throw('[sprintf] huh?');
			}
			_fmt = _fmt.substring(match[0].length);
		}
		return parse_tree;
	};

	return str_format;
})();
function Format(f,a) { a.unshift(f); return sprintf.apply(null,a) }
var Exception={
	$ClassName: "Exception",
	$Parent: TObject,
	$Init: function (s) { FMessage="" },
	Create: function (s,Msg) { s.FMessage=Msg; return s }
}
function Delete(s,i,n) { var v=s.v; if ((i<=0)||(i>v.length)||(n<=0)) return; s.v=v.substr(0,i-1)+v.substr(i+n-1); }
function ClampInt(v,mi,ma) { return v<mi ? mi : v>ma ? ma : v }
function $W(e) { return e.ClassType?e:Exception.Create($New(Exception),e.constructor.name+", "+e.message) }
// inspired from 
// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/String/charCodeAt
function $uniCharAt(str, idx) {
    var c = str.charCodeAt(idx);
    if (0xD800 <= c && c <= 0xDBFF) { // High surrogate
        return str.substr(idx, 2);
    }
    if (0xDC00 <= c && c <= 0xDFFF) { // Low surrogate
        return null;
    }
    return str.charAt(idx);
}function $SetIn(s,v,m,n) { v-=m; return (v<0 && v>=n)?false:(s[v>>5]&(1<<(v&31)))!=0 }
Array.prototype.pusha = function (e) { this.push.apply(this, e); return this }
function $NewDyn(c,z) {
	if (c==null) throw Exception.Create($New(Exception),"ClassType is nil"+z);
	var i={ClassType:c};
	c.$Init(i);
	return i
}
function $New(c) { var i={ClassType:c}; c.$Init(i); return i }
function $Is(o,c) {
	if (o===null) return false;
	return $Inh(o.ClassType,c);
}
;
function $Inh(s,c) {
	if (s===null) return false;
	while ((s)&&(s!==c)) s=s.$Parent;
	return (s)?true:false;
}
;
function $Extend(base, sub, props) {
	function F() {};
	F.prototype = base.prototype;
	sub.prototype = new F();
	sub.prototype.constructor = sub;
	for (var n in props) {
		if (props.hasOwnProperty(n)) {
			sub.prototype[n]=props[n];
		}
	}
}
function $Event2(i,f) {
	var li=i,lf=f;
	return function(a,b) {
		return lf.call(li,li,a,b)
	}
}
function $Event0(i,f) {
	var li=i,lf=f;
	return function() {
		return lf.call(li,li)
	}
}
function $AsIntf(o,i) {
	if (o===null) return null;
	var r = o.ClassType.$Intf[i].map(function (e) {
		return function () {
			var arg=Array.prototype.slice.call(arguments);
			arg.splice(0,0,o);
			return e.apply(o, arg);
		}
	});
	r.O = o;
	return r;
}
;
function $As(o,c) {
	if ((o===null)||$Is(o,c)) return o;
	throw Exception.Create($New(Exception),"Cannot cast instance of type \""+o.ClassType.$ClassName+"\" to class \""+c.$ClassName+"\"");
}
function $ArraySetLenC(a,n,d) {
	var o=a.length;
	if (o==n) return;
	if (o>n) a.length=n; else for (;o<n;o++) a.push(d());
}
/// TNodeProgram = class (TObject)
///  [line: 27, column: 3, file: Unit1]
var TNodeProgram = {
   $ClassName:"TNodeProgram",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// procedure TNodeProgram.Execute()
   ///  [line: 50, column: 24, file: Unit1]
   ,Execute:function(Self) {
      var NetAdapters,
         Adapter = "",
         netIntf = "",
         address$9;
      NetAdapters = TApplication.GetOSApi(Application()).networkInterfaces();
      for (Adapter in NetAdapters) {
         if (((Adapter).indexOf("Loopback")>=0)) {
            continue;
         } else {
            WriteLn(Adapter);
         }
         for (netIntf in NetAdapters[Adapter]) {
            address$9 = NetAdapters[Adapter][netIntf];
            if (!address$9.internal) {
               if (address$9.address) {
                  WriteLn(address$9.address);
               }
            }
         }
      }
   }
   /// constructor TNodeProgram.Create()
   ///  [line: 40, column: 26, file: Unit1]
   ,Create$3:function(Self) {
      TObject.Create(Self);
      return Self
   }
   /// destructor TNodeProgram.Destroy()
   ///  [line: 45, column: 25, file: Unit1]
   ,Destroy:function(Self) {
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// function TW3VariantHelper.DataType(const Self: Variant) : TW3VariantDataType
///  [line: 1826, column: 27, file: System.Types]
function TW3VariantHelper$DataType(Self$1) {
   var Result = 1;
   var LType = "";
   if (TW3VariantHelper$Valid$2(Self$1)) {
      LType = typeof(Self$1);
      {var $temp1 = (LType).toLocaleLowerCase();
         if ($temp1=="object") {
            if (!Self$1.length) {
               Result = 8;
            } else {
               Result = 9;
            }
         }
          else if ($temp1=="function") {
            Result = 7;
         }
          else if ($temp1=="symbol") {
            Result = 6;
         }
          else if ($temp1=="boolean") {
            Result = 2;
         }
          else if ($temp1=="string") {
            Result = 5;
         }
          else if ($temp1=="number") {
            if (Math.round(Number(Self$1))!=Self$1) {
               Result = 4;
            } else {
               Result = 3;
            }
         }
          else if ($temp1=="array") {
            Result = 9;
         }
          else {
            Result = 1;
         }
      }
   } else if (Self$1==null) {
      Result = 10;
   } else {
      Result = 1;
   }
   return Result
}
/// function TW3VariantHelper.IsObject(const Self: Variant) : Boolean
///  [line: 1873, column: 27, file: System.Types]
function TW3VariantHelper$IsObject(Self$2) {
   var Result = false;
   Result = ((Self$2) !== undefined)
      && (Self$2 !== null)
      && (typeof Self$2  === "object")
      && ((Self$2).length === undefined);
   return Result
}
/// function TW3VariantHelper.IsUInt8Array(const Self: Variant) : Boolean
///  [line: 1812, column: 27, file: System.Types]
function TW3VariantHelper$IsUInt8Array(Self$3) {
   var Result = false;
   var LTypeName = "";
   Result = false;
   if (TW3VariantHelper$Valid$2(Self$3)) {
      LTypeName = Object.prototype.toString.call(Self$3);
      Result = LTypeName=="[object Uint8Array]";
   }
   return Result
}
/// function TW3VariantHelper.Valid(const Self: Variant) : Boolean
///  [line: 1784, column: 27, file: System.Types]
function TW3VariantHelper$Valid$2(Self$4) {
   var Result = false;
   Result = !( (Self$4 == undefined) || (Self$4 == null) );
   return Result
}
/// TW3VariantDataType enumeration
///  [line: 559, column: 3, file: System.Types]
var TW3VariantDataType = { 1:"vdUnknown", 2:"vdBoolean", 3:"vdinteger", 4:"vdfloat", 5:"vdstring", 6:"vdSymbol", 7:"vdFunction", 8:"vdObject", 9:"vdArray", 10:"vdVariant" };
/// TW3OwnedObject = class (TObject)
///  [line: 357, column: 3, file: System.Types]
var TW3OwnedObject = {
   $ClassName:"TW3OwnedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FOwner = null;
   }
   /// function TW3OwnedObject.GetOwner() : TObject
   ///  [line: 1177, column: 26, file: System.Types]
   ,GetOwner:function(Self) {
      return Self.FOwner;
   }
   /// procedure TW3OwnedObject.SetOwner(const NewOwner: TObject)
   ///  [line: 1187, column: 26, file: System.Types]
   ,SetOwner:function(Self, NewOwner) {
      if (NewOwner!==Self.FOwner) {
         if (TW3OwnedObject.AcceptOwner$(Self,NewOwner)) {
            Self.FOwner = NewOwner;
         } else {
            throw EW3Exception.CreateFmt($New(EW3OwnedObject),$R[0],["TW3OwnedObject.SetOwner", TObject.ClassName(Self.ClassType), $R[2]]);
         }
      }
   }
   /// function TW3OwnedObject.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 1182, column: 25, file: System.Types]
   ,AcceptOwner:function(Self, CandidateObject) {
      return true;
   }
   /// constructor TW3OwnedObject.Create(const AOwner: TObject)
   ///  [line: 1171, column: 28, file: System.Types]
   ,Create$18:function(Self, AOwner) {
      TObject.Create(Self);
      TW3OwnedObject.SetOwner(Self,AOwner);
      return Self
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$18$:function($){return $.ClassType.Create$18.apply($.ClassType, arguments)}
};
TW3OwnedObject.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3OwnedLockedObject = class (TW3OwnedObject)
///  [line: 371, column: 3, file: System.Types]
var TW3OwnedLockedObject = {
   $ClassName:"TW3OwnedLockedObject",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.OnObjectUnLocked = null;
      $.OnObjectLocked = null;
      $.FLocked = 0;
   }
   /// procedure TW3OwnedLockedObject.DisableAlteration()
   ///  [line: 1133, column: 32, file: System.Types]
   ,DisableAlteration:function(Self) {
      ++Self.FLocked;
      if (Self.FLocked==1) {
         TW3OwnedLockedObject.ObjectLocked(Self);
      }
   }
   /// procedure TW3OwnedLockedObject.EnableAlteration()
   ///  [line: 1140, column: 32, file: System.Types]
   ,EnableAlteration:function(Self) {
      if (Self.FLocked>0) {
         --Self.FLocked;
         if (!Self.FLocked) {
            TW3OwnedLockedObject.ObjectUnLocked(Self);
         }
      }
   }
   /// function TW3OwnedLockedObject.GetLockState() : Boolean
   ///  [line: 1150, column: 31, file: System.Types]
   ,GetLockState:function(Self) {
      return Self.FLocked>0;
   }
   /// procedure TW3OwnedLockedObject.ObjectLocked()
   ///  [line: 1155, column: 32, file: System.Types]
   ,ObjectLocked:function(Self) {
      if (Self.OnObjectLocked) {
         Self.OnObjectLocked(Self);
      }
   }
   /// procedure TW3OwnedLockedObject.ObjectUnLocked()
   ///  [line: 1161, column: 32, file: System.Types]
   ,ObjectUnLocked:function(Self) {
      if (Self.OnObjectUnLocked) {
         Self.OnObjectUnLocked(Self);
      }
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$18:TW3OwnedObject.Create$18
};
TW3OwnedLockedObject.$Intf={
   IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3LockedObject = class (TObject)
///  [line: 389, column: 3, file: System.Types]
var TW3LockedObject = {
   $ClassName:"TW3LockedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.OnObjectUnLocked = null;
      $.OnObjectLocked = null;
      $.FLocked$1 = 0;
   }
   /// procedure TW3LockedObject.DisableAlteration()
   ///  [line: 1094, column: 27, file: System.Types]
   ,DisableAlteration$1:function(Self) {
      ++Self.FLocked$1;
      if (Self.FLocked$1==1) {
         TW3LockedObject.ObjectLocked$1$(Self);
      }
   }
   /// procedure TW3LockedObject.EnableAlteration()
   ///  [line: 1101, column: 27, file: System.Types]
   ,EnableAlteration$1:function(Self) {
      if (Self.FLocked$1>0) {
         --Self.FLocked$1;
         if (!Self.FLocked$1) {
            TW3LockedObject.ObjectUnLocked$1$(Self);
         }
      }
   }
   /// function TW3LockedObject.GetLockState() : Boolean
   ///  [line: 1111, column: 26, file: System.Types]
   ,GetLockState$1:function(Self) {
      return Self.FLocked$1>0;
   }
   /// procedure TW3LockedObject.ObjectLocked()
   ///  [line: 1116, column: 27, file: System.Types]
   ,ObjectLocked$1:function(Self) {
      if (Self.OnObjectLocked) {
         Self.OnObjectLocked(Self);
      }
   }
   /// procedure TW3LockedObject.ObjectUnLocked()
   ///  [line: 1122, column: 27, file: System.Types]
   ,ObjectUnLocked$1:function(Self) {
      if (Self.OnObjectUnLocked) {
         Self.OnObjectUnLocked(Self);
      }
   }
   ,Destroy:TObject.Destroy
   ,ObjectLocked$1$:function($){return $.ClassType.ObjectLocked$1($)}
   ,ObjectUnLocked$1$:function($){return $.ClassType.ObjectUnLocked$1($)}
};
TW3LockedObject.$Intf={
   IW3LockObject:[TW3LockedObject.DisableAlteration$1,TW3LockedObject.EnableAlteration$1,TW3LockedObject.GetLockState$1]
}
/// TW3Identifiers = class (TObject)
///  [line: 278, column: 3, file: System.Types]
var TW3Identifiers = {
   $ClassName:"TW3Identifiers",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TW3Identifiers.GenerateUniqueObjectId() : String
   ///  [line: 1712, column: 31, file: System.Types]
   ,GenerateUniqueObjectId:function(Self) {
      var Result = "";
      ++__UNIQUE;
      Result = "OBJ"+__UNIQUE.toString();
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TW3FilePermissionMask enumeration
///  [line: 79, column: 3, file: System.Types]
var TW3FilePermissionMask = { 0:"fpNone", 111:"fpExecute", 222:"fpWrite", 333:"fpWriteExecute", 444:"fpRead", 555:"fpReadExecute", 666:"fpDefault", 777:"fpReadWriteExecute", 740:"fpRWEGroupReadOnly" };
/// TVariant = class (TObject)
///  [line: 511, column: 3, file: System.Types]
var TVariant = {
   $ClassName:"TVariant",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TVariant.AsBool(const aValue: Variant) : Boolean
   ///  [line: 2232, column: 25, file: System.Types]
   ,AsBool:function(aValue) {
      var Result = false;
      if (aValue!=undefined&&aValue!=null) {
         Result = (aValue?true:false);
      }
      return Result
   }
   /// function TVariant.AsFloat(const aValue: Variant) : Float
   ///  [line: 2215, column: 25, file: System.Types]
   ,AsFloat:function(aValue$1) {
      var Result = 0;
      if (aValue$1!=undefined&&aValue$1!=null) {
         Result = Number(aValue$1);
      }
      return Result
   }
   /// function TVariant.AsInteger(const aValue: Variant) : Integer
   ///  [line: 2201, column: 25, file: System.Types]
   ,AsInteger:function(aValue$2) {
      var Result = 0;
      if (aValue$2!=undefined&&aValue$2!=null) {
         Result = parseInt(aValue$2,10);
      }
      return Result
   }
   /// function TVariant.AsObject(const aValue: Variant) : TObject
   ///  [line: 2222, column: 25, file: System.Types]
   ,AsObject:function(aValue$3) {
      var Result = null;
      if (aValue$3!=undefined&&aValue$3!=null) {
         Result = aValue$3;
      }
      return Result
   }
   /// function TVariant.AsString(const aValue: Variant) : String
   ///  [line: 2208, column: 25, file: System.Types]
   ,AsString:function(aValue$4) {
      var Result = "";
      if (aValue$4!=undefined&&aValue$4!=null) {
         Result = String(aValue$4);
      }
      return Result
   }
   /// function TVariant.CreateObject() : Variant
   ///  [line: 2239, column: 25, file: System.Types]
   ,CreateObject:function() {
      var Result = undefined;
      Result = new Object();
      return Result
   }
   /// procedure TVariant.ForEachProperty(const Data: Variant; const CallBack: TW3ObjectKeyCallback)
   ///  [line: 2322, column: 26, file: System.Types]
   ,ForEachProperty:function(Data$1, CallBack) {
      var LObj,
         Keys$1 = [],
         a$238 = 0;
      var LName = "";
      if (CallBack) {
         Keys$1 = TVariant.Properties(Data$1);
         var $temp2;
         for(a$238=0,$temp2=Keys$1.length;a$238<$temp2;a$238++) {
            LName = Keys$1[a$238];
            LObj = Keys$1[LName];
            if ((~CallBack(LName,LObj))==1) {
               break;
            }
         }
      }
   }
   /// function TVariant.Properties(const Data: Variant) : TStrArray
   ///  [line: 2341, column: 25, file: System.Types]
   ,Properties:function(Data$2) {
      var Result = [];
      if (Data$2) {
         if (!(Object.keys === undefined)) {
        Result = Object.keys(Data$2);
        return Result;
      }
         if (!(Object.getOwnPropertyNames === undefined)) {
          Result = Object.getOwnPropertyNames(Data$2);
          return Result;
      }
         for (var qtxenum in Data$2) {
        if ( (Data$2).hasOwnProperty(qtxenum) == true )
          (Result).push(qtxenum);
      }
      return Result;
      }
      return Result
   }
   /// function TVariant.ValidRef(const aValue: Variant) : Boolean
   ///  [line: 2196, column: 25, file: System.Types]
   ,ValidRef:function(aValue$5) {
      return aValue$5!=undefined&&aValue$5!=null;
   }
   ,Destroy:TObject.Destroy
};
/// TTextFormation enumeration
///  [line: 200, column: 3, file: System.Types]
var TTextFormation = { 256:"tfHex", 257:"tfOrdinal", 258:"tfFloat", 259:"tfQuote" };
/// function TStringHelper.ContainsHex(const Self: String) : Boolean
///  [line: 1651, column: 24, file: System.Types]
function TStringHelper$ContainsHex(Self$5) {
   var Result = false;
   var x$1 = 0;
   var LStart = 0;
   var LItem = "";
   var LLen = 0;
   Result = false;
   LLen = Self$5.length;
   if (LLen>=1) {
      LStart = 1;
      if (Self$5.charAt(0)=="$") {
         ++LStart;
         --LLen;
      } else {
         LItem = (Self$5.substr(0,1)).toLocaleUpperCase();
         Result = ($R[34].indexOf(LItem)+1)>0;
         if (!Result) {
            return Result;
         }
      }
      if (LLen>=1) {
         var $temp3;
         for(x$1=LStart,$temp3=Self$5.length;x$1<=$temp3;x$1++) {
            LItem = (Self$5.charAt(x$1-1)).toLocaleUpperCase();
            Result = ($R[34].indexOf(LItem)+1)>0;
            if (!Result) {
               break;
            }
         }
      }
   }
   return Result
}
/// function TStringHelper.ContainsOrdinal(const Self: String) : Boolean
///  [line: 1635, column: 24, file: System.Types]
function TStringHelper$ContainsOrdinal(Self$6) {
   var Result = false;
   var LLen$1 = 0,
      x$2 = 0;
   var LItem$1 = "";
   Result = false;
   LLen$1 = Self$6.length;
   if (LLen$1>=1) {
      var $temp4;
      for(x$2=1,$temp4=LLen$1;x$2<=$temp4;x$2++) {
         LItem$1 = Self$6.charAt(x$2-1);
         Result = ($R[33].indexOf(LItem$1)+1)>0;
         if (!Result) {
            break;
         }
      }
   }
   return Result
}
/// function TStringHelper.ContainsFloat(const Self: String) : Boolean
///  [line: 1580, column: 24, file: System.Types]
function TStringHelper$ContainsFloat(Self$7) {
   var Result = false;
   var x$3 = 0;
   var LItem$2 = "";
   var LLen$2 = 0;
   var LLine = false;
   Result = false;
   LLen$2 = Self$7.length;
   if (LLen$2>=1) {
      LLine = false;
      var $temp5;
      for(x$3=1,$temp5=LLen$2;x$3<=$temp5;x$3++) {
         LItem$2 = Self$7.charAt(x$3-1);
         if (LItem$2==".") {
            if (x$3==1&&LLen$2==1) {
               break;
            }
            if (x$3==1&&LLen$2>1) {
               LLine = true;
               continue;
            }
            if (x$3>1&&x$3<LLen$2) {
               if (LLine) {
                  break;
               } else {
                  LLine = true;
                  continue;
               }
            } else {
               break;
            }
         }
         Result = ("0123456789".indexOf(LItem$2)+1)>0;
         if (!Result) {
            break;
         }
      }
   }
   return Result
}
/// function TStringHelper.ContainsQuote(const Self: String) : Boolean
///  [line: 1499, column: 24, file: System.Types]
function TStringHelper$ContainsQuote(Self$8) {
   var Result = false;
   var LLen$3 = 0;
   var LStart$1 = 0;
   var LFound = false;
   var LQuote = ["",""];
   Result = false;
   LLen$3 = Self$8.length;
   if (LLen$3>=2) {
      LStart$1 = 1;
      while (LStart$1<=LLen$3) {
         if (Self$8.charAt(LStart$1-1)==" ") {
            ++LStart$1;
            continue;
         } else {
            break;
         }
      }
      LQuote[false?1:0] = "'";
      LQuote[true?1:0] = "\"";
      if (Self$8.charAt(LStart$1-1)!=LQuote[true?1:0]||Self$8.charAt(LStart$1-1)!=LQuote[false?1:0]) {
         return Result;
      }
      if (LStart$1>=LLen$3) {
         return Result;
      }
      ++LStart$1;
      LFound = false;
      while (LStart$1<=LLen$3) {
         if (Self$8.charAt(LStart$1-1)!=LQuote[true?1:0]||Self$8.charAt(LStart$1-1)!=LQuote[false?1:0]) {
            LFound = true;
         }
         ++LStart$1;
      }
      if (!LFound) {
         return Result;
      }
      if (LStart$1==LLen$3) {
         Result = true;
         return Result;
      }
      while (LStart$1<=LLen$3) {
         if (Self$8.charAt(LStart$1-1)!=" ") {
            LFound = false;
            break;
         } else {
            ++LStart$1;
         }
      }
      Result = LFound;
   }
   return Result
}
/// TString = class (TObject)
///  [line: 235, column: 3, file: System.Types.Convert]
var TString = {
   $ClassName:"TString",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TString.CharCodeFor(const Character: Char) : Integer
   ///  [line: 1220, column: 24, file: System.Types]
   ,CharCodeFor:function(Self, Character) {
      var Result = 0;
      Result = (Character).charCodeAt(0);
      return Result
   }
   /// function TString.CreateGUID() : String
   ///  [line: 1991, column: 24, file: System.Types.Convert]
   ,CreateGUID:function(Self) {
      var Result = "";
      var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = "-";

    Result = s.join("");
      Result = (Result).toUpperCase();
      return Result
   }
   /// function TString.DecodeBase64(TextToDecode: String) : String
   ///  [line: 2014, column: 24, file: System.Types.Convert]
   ,DecodeBase64:function(Self, TextToDecode) {
      return TBase64EncDec.Base64ToString(TBase64EncDec,TextToDecode);
   }
   /// function TString.DecodeUTF8(const BytesToDecode: TByteArray) : String
   ///  [line: 1295, column: 24, file: System.Types]
   ,DecodeUTF8:function(Self, BytesToDecode) {
      var Result = "";
      var i = 0,
         c = 0,
         c2 = 0,
         c2$1 = 0,
         c3 = 0;
      i = 0;
      while (i<BytesToDecode.length) {
         c = BytesToDecode[i];
         if (c<128) {
            Result+=TString.FromCharCode(TString,c);
            ++i;
         } else if (c>191&&c<224) {
            c2 = BytesToDecode[i+1];
            Result+=TString.FromCharCode(TString,(((c&31)<<6)|(c2&63)));
            (i+= 2);
         } else {
            c2$1 = BytesToDecode[i+1];
            c3 = BytesToDecode[i+2];
            Result+=TString.FromCharCode(TString,(((c&15)<<12)|(((c2$1&63)<<6)|(c3&63))));
            (i+= 3);
         }
      }
      return Result
   }
   /// function TString.EncodeBase64(TextToEncode: String) : String
   ///  [line: 2009, column: 24, file: System.Types.Convert]
   ,EncodeBase64:function(Self, TextToEncode) {
      return TBase64EncDec.StringToBase64(TBase64EncDec,TextToEncode);
   }
   /// function TString.EncodeUTF8(TextToEncode: String) : TByteArray
   ///  [line: 1269, column: 24, file: System.Types]
   ,EncodeUTF8:function(Self, TextToEncode$1) {
      var Result = [];
      var n = 0;
      var c$1 = 0;
      TextToEncode$1 = StrReplace(TextToEncode$1,"\r\n","\r");
      if (TextToEncode$1.length>0) {
         var $temp6;
         for(n=1,$temp6=TextToEncode$1.length;n<=$temp6;n++) {
            c$1 = TString.CharCodeFor(TString,TextToEncode$1.charAt(n-1));
            if (c$1<128) {
               Result.push(c$1);
            } else if (c$1>127&&c$1<2048) {
               Result.push(((c$1>>>6)|192));
               Result.push(((c$1&63)|128));
            } else {
               Result.push(((c$1>>>12)|224));
               Result.push((((c$1>>>6)&63)|128));
               Result.push(((c$1&63)|128));
            }
         }
      }
      return Result
   }
   /// function TString.FromCharCode(const CharCode: Byte) : Char
   ///  [line: 1243, column: 24, file: System.Types]
   ,FromCharCode:function(Self, CharCode) {
      var Result = "";
      Result = String.fromCharCode(CharCode);
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TInteger = class (TObject)
///  [line: 460, column: 3, file: System.Types]
var TInteger = {
   $ClassName:"TInteger",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TInteger.Diff(const Primary: Integer; const Secondary: Integer) : Integer
   ///  [line: 2085, column: 25, file: System.Types]
   ,Diff:function(Primary, Secondary) {
      var Result = 0;
      if (Primary!=Secondary) {
         if (Primary>Secondary) {
            Result = Primary-Secondary;
         } else {
            Result = Secondary-Primary;
         }
         if (Result<0) {
            Result = (Result-1)^(-1);
         }
      } else {
         Result = 0;
      }
      return Result
   }
   /// function TInteger.EnsureRange(const aValue: Integer; const aMin: Integer; const aMax: Integer) : Integer
   ///  [line: 2039, column: 25, file: System.Types]
   ,EnsureRange:function(aValue$6, aMin, aMax) {
      return ClampInt(aValue$6,aMin,aMax);
   }
   /// procedure TInteger.SetBit(index: Integer; aValue: Boolean; var buffer: Integer)
   ///  [line: 1954, column: 26, file: System.Types]
   ,SetBit:function(index, aValue$7, buffer$1) {
      if (index>=0&&index<=31) {
         if (aValue$7) {
            buffer$1.v = buffer$1.v|(1<<index);
         } else {
            buffer$1.v = buffer$1.v&(~(1<<index));
         }
      } else {
         throw Exception.Create($New(Exception),"Invalid bit index, expected 0..31");
      }
   }
   /// function TInteger.SubtractSmallest(const First: Integer; const Second: Integer) : Integer
   ///  [line: 2011, column: 25, file: System.Types]
   ,SubtractSmallest:function(First, Second) {
      var Result = 0;
      if (First<Second) {
         Result = Second-First;
      } else {
         Result = First-Second;
      }
      return Result
   }
   /// function TInteger.ToNearest(const Value: Integer; const Factor: Integer) : Integer
   ///  [line: 2070, column: 25, file: System.Types]
   ,ToNearest:function(Value, Factor) {
      var Result = 0;
      var FTemp = 0;
      Result = Value;
      FTemp = Value%Factor;
      if (FTemp>0) {
         (Result+= (Factor-FTemp));
      }
      return Result
   }
   /// function TInteger.WrapRange(const aValue: Integer; const aLowRange: Integer; const aHighRange: Integer) : Integer
   ///  [line: 2053, column: 25, file: System.Types]
   ,WrapRange:function(aValue$8, aLowRange, aHighRange) {
      var Result = 0;
      if (aValue$8>aHighRange) {
         Result = aLowRange+TInteger.Diff(aHighRange,(aValue$8-1));
         if (Result>aHighRange) {
            Result = TInteger.WrapRange(Result,aLowRange,aHighRange);
         }
      } else if (aValue$8<aLowRange) {
         Result = aHighRange-TInteger.Diff(aLowRange,(aValue$8+1));
         if (Result<aLowRange) {
            Result = TInteger.WrapRange(Result,aLowRange,aHighRange);
         }
      } else {
         Result = aValue$8;
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// function THandleHelper.Defined(const Self: THandle) : Boolean
///  [line: 1748, column: 24, file: System.Types]
function THandleHelper$Defined(Self$9) {
   var Result = false;
   Result = !(Self$9 == undefined);
   return Result
}
/// TFileAccessMode enumeration
///  [line: 134, column: 3, file: System.Types]
var TFileAccessMode = [ "fmOpenRead", "fmOpenWrite", "fmOpenReadWrite" ];
/// TEnumState enumeration
///  [line: 128, column: 3, file: System.Types]
var TEnumState = { 1:"esContinue", 0:"esAbort" };
/// TEnumResult enumeration
///  [line: 109, column: 3, file: System.Types]
var TEnumResult = { 160:"erContinue", 16:"erBreak" };
/// TDataTypeMap = record
///  [line: 501, column: 3, file: System.Types]
function Copy$TDataTypeMap(s,d) {
   d.Boolean=s.Boolean;
   d.Number$1=s.Number$1;
   d.String$1=s.String$1;
   d.Object$2=s.Object$2;
   d.Undefined=s.Undefined;
   d.Function$1=s.Function$1;
   return d;
}
function Clone$TDataTypeMap($) {
   return {
      Boolean:$.Boolean,
      Number$1:$.Number$1,
      String$1:$.String$1,
      Object$2:$.Object$2,
      Undefined:$.Undefined,
      Function$1:$.Function$1
   }
}
function GetIsRunningInBrowser() {
   var Result = false;
   Result = (!(typeof window === 'undefined'));
   return Result
};
/// EW3Exception = class (Exception)
///  [line: 229, column: 3, file: System.Types]
var EW3Exception = {
   $ClassName:"EW3Exception",$Parent:Exception
   ,$Init:function ($) {
      Exception.$Init($);
   }
   /// constructor EW3Exception.CreateFmt(aText: String; const aValues: array of const)
   ///  [line: 1766, column: 26, file: System.Types]
   ,CreateFmt:function(Self, aText, aValues) {
      Exception.Create(Self,Format(aText,aValues.slice(0)));
      return Self
   }
   /// constructor EW3Exception.Create(const MethodName: String; const Instance: TObject; const ErrorText: String)
   ///  [line: 1771, column: 26, file: System.Types]
   ,Create$25:function(Self, MethodName, Instance$3, ErrorText) {
      var LCallerName = "";
      LCallerName = (Instance$3)?TObject.ClassName(Instance$3.ClassType):"Anonymous";
      EW3Exception.CreateFmt(Self,$R[0],[MethodName, LCallerName, ErrorText]);
      return Self
   }
   ,Destroy:Exception.Destroy
};
/// EW3OwnedObject = class (EW3Exception)
///  [line: 348, column: 3, file: System.Types]
var EW3OwnedObject = {
   $ClassName:"EW3OwnedObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EW3LockError = class (EW3Exception)
///  [line: 340, column: 3, file: System.Types]
var EW3LockError = {
   $ClassName:"EW3LockError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function SetupTypeLUT() {
   __TYPE_MAP.Boolean = typeof(true);
   __TYPE_MAP.Number$1 = typeof(0);
   __TYPE_MAP.String$1 = typeof("");
   __TYPE_MAP.Object$2 = typeof(TVariant.CreateObject());
   __TYPE_MAP.Undefined = typeof(undefined);
   __TYPE_MAP.Function$1 = typeof(function () {
      /* null */
   });
};
/// TValuePrefixType enumeration
///  [line: 52, column: 3, file: System.Types.Convert]
var TValuePrefixType = [ "vpNone", "vpHexPascal", "vpHexC", "vpBinPascal", "vpBinC", "vpString" ];
/// TSystemEndianType enumeration
///  [line: 66, column: 3, file: System.Types.Convert]
var TSystemEndianType = [ "stDefault", "stLittleEndian", "stBigEndian" ];
/// TRTLDatatype enumeration
///  [line: 37, column: 3, file: System.Types.Convert]
var TRTLDatatype = [ "itUnknown", "itBoolean", "itByte", "itChar", "itWord", "itLong", "itInt16", "itInt32", "itFloat32", "itFloat64", "itString" ];
/// TDataTypeConverter = class (TObject)
///  [line: 73, column: 3, file: System.Types.Convert]
var TDataTypeConverter = {
   $ClassName:"TDataTypeConverter",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.OnEndianChanged = null;
      $.FBuffer = $.FView = null;
      $.FEndian = 0;
      $.FTyped = null;
   }
   /// function TDataTypeConverter.BooleanToBytes(const Value: Boolean) : TByteArray
   ///  [line: 1042, column: 35, file: System.Types.Convert]
   ,BooleanToBytes:function(Self, Value$1) {
      var Result = [];
      Result.push((Value$1)?1:0);
      return Result
   }
   /// function TDataTypeConverter.BytesToBase64(const Bytes: TByteArray) : String
   ///  [line: 1283, column: 35, file: System.Types.Convert]
   ,BytesToBase64:function(Self, Bytes$1) {
      return TBase64EncDec.BytesToBase64$2(TBase64EncDec,Bytes$1);
   }
   /// function TDataTypeConverter.BytesToBoolean(const Data: TByteArray) : Boolean
   ///  [line: 1288, column: 29, file: System.Types.Convert]
   ,BytesToBoolean:function(Self, Data$3) {
      return Data$3[0]>0;
   }
   /// function TDataTypeConverter.BytesToFloat32(const Data: TByteArray) : Float
   ///  [line: 1233, column: 29, file: System.Types.Convert]
   ,BytesToFloat32:function(Self, Data$4) {
      var Result = 0;
      Self.FView.setUint8(0,Data$4[0]);
      Self.FView.setUint8(1,Data$4[1]);
      Self.FView.setUint8(2,Data$4[2]);
      Self.FView.setUint8(3,Data$4[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getFloat32(0);
            break;
         case 1 :
            Result = Self.FView.getFloat32(0,true);
            break;
         case 2 :
            Result = Self.FView.getFloat32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToFloat64(const Data: TByteArray) : Float
   ///  [line: 991, column: 29, file: System.Types.Convert]
   ,BytesToFloat64:function(Self, Data$5) {
      var Result = 0;
      Self.FView.setUint8(0,Data$5[0]);
      Self.FView.setUint8(1,Data$5[1]);
      Self.FView.setUint8(2,Data$5[2]);
      Self.FView.setUint8(3,Data$5[3]);
      Self.FView.setUint8(4,Data$5[4]);
      Self.FView.setUint8(5,Data$5[5]);
      Self.FView.setUint8(6,Data$5[6]);
      Self.FView.setUint8(7,Data$5[7]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getFloat64(0);
            break;
         case 1 :
            Result = Self.FView.getFloat64(0,true);
            break;
         case 2 :
            Result = Self.FView.getFloat64(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToInt16(const Data: TByteArray) : SmallInt
   ///  [line: 1009, column: 29, file: System.Types.Convert]
   ,BytesToInt16:function(Self, Data$6) {
      var Result = 0;
      Self.FView.setUint8(0,Data$6[0]);
      Self.FView.setUint8(1,Data$6[1]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getInt16(0);
            break;
         case 1 :
            Result = Self.FView.getInt16(0,true);
            break;
         case 2 :
            Result = Self.FView.getInt16(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToInt32(const Data: TByteArray) : Integer
   ///  [line: 947, column: 29, file: System.Types.Convert]
   ,BytesToInt32:function(Self, Data$7) {
      var Result = 0;
      Self.FView.setUint8(0,Data$7[0]);
      Self.FView.setUint8(1,Data$7[1]);
      Self.FView.setUint8(2,Data$7[2]);
      Self.FView.setUint8(3,Data$7[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getInt32(0);
            break;
         case 1 :
            Result = Self.FView.getInt32(0,true);
            break;
         case 2 :
            Result = Self.FView.getInt32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToString(const Data: TByteArray) : String
   ///  [line: 1324, column: 29, file: System.Types.Convert]
   ,BytesToString:function(Self, Data$8) {
      var Result = "";
      var x$4 = 0;
      if (Data$8.length>0) {
         var $temp7;
         for(x$4=0,$temp7=Data$8.length;x$4<$temp7;x$4++) {
            Result+=TString.FromCharCode(TString,Data$8[x$4]);
         }
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToTypedArray(const Values: TByteArray) : TMemoryHandle
   ///  [line: 1021, column: 29, file: System.Types.Convert]
   ,BytesToTypedArray:function(Self, Values$6) {
      var Result = undefined;
      var LLen$4 = 0;
      LLen$4 = Values$6.length;
      Result = new Uint8Array(LLen$4);
      (Result).set(Values$6, 0);
      return Result
   }
   /// function TDataTypeConverter.BytesToUInt16(const Data: TByteArray) : Word
   ///  [line: 1091, column: 29, file: System.Types.Convert]
   ,BytesToUInt16:function(Self, Data$9) {
      var Result = 0;
      Self.FView.setUint8(0,Data$9[0]);
      Self.FView.setUint8(1,Data$9[1]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getUint16(0);
            break;
         case 1 :
            Result = Self.FView.getUint16(0,true);
            break;
         case 2 :
            Result = Self.FView.getUint16(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToUInt32(const Data: TByteArray) : Longword
   ///  [line: 893, column: 29, file: System.Types.Convert]
   ,BytesToUInt32:function(Self, Data$10) {
      var Result = 0;
      Self.FView.setUint8(0,Data$10[0]);
      Self.FView.setUint8(1,Data$10[1]);
      Self.FView.setUint8(2,Data$10[2]);
      Self.FView.setUint8(3,Data$10[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getUint32(0);
            break;
         case 1 :
            Result = Self.FView.getUint32(0,true);
            break;
         case 2 :
            Result = Self.FView.getUint32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToVariant(Data: TByteArray) : Variant
   ///  [line: 1116, column: 29, file: System.Types.Convert]
   ,BytesToVariant:function(Self, Data$11) {
      var Result = undefined;
      var LType$1 = 0;
      LType$1 = Data$11[0];
      Data$11.shift();
      switch (LType$1) {
         case 161 :
            Result = TDataTypeConverter.BytesToBoolean(Self,Data$11);
            break;
         case 162 :
            Result = Data$11[0];
            break;
         case 168 :
            Result = TDataTypeConverter.BytesToUInt16(Self,Data$11);
            break;
         case 169 :
            Result = TDataTypeConverter.BytesToUInt32(Self,Data$11);
            break;
         case 163 :
            Result = TDataTypeConverter.BytesToInt16(Self,Data$11);
            break;
         case 164 :
            Result = TDataTypeConverter.BytesToInt32(Self,Data$11);
            break;
         case 165 :
            Result = TDataTypeConverter.BytesToFloat32(Self,Data$11);
            break;
         case 166 :
            Result = TDataTypeConverter.BytesToFloat64(Self,Data$11);
            break;
         case 167 :
            Result = TString.DecodeUTF8(TString,Data$11);
            break;
         default :
            throw EW3Exception.CreateFmt($New(EDatatype),"Failed to convert bytes[] to intrinsic type, unknown identifier [%s] error",[IntToHex2(LType$1)]);
      }
      return Result
   }
   /// function TDataTypeConverter.ByteToChar(const Value: Byte) : Char
   ///  [line: 1335, column: 35, file: System.Types.Convert]
   ,ByteToChar:function(Self, Value$2) {
      var Result = "";
      Result = String.fromCharCode(Value$2);
      return Result
   }
   /// function TDataTypeConverter.CharToByte(const Value: Char) : Word
   ///  [line: 1065, column: 35, file: System.Types.Convert]
   ,CharToByte:function(Self, Value$3) {
      var Result = 0;
      Result = (Value$3).charCodeAt(0);
      return Result
   }
   /// constructor TDataTypeConverter.Create()
   ///  [line: 631, column: 32, file: System.Types.Convert]
   ,Create$4:function(Self) {
      TObject.Create(Self);
      Self.FBuffer = new ArrayBuffer(16);
    Self.FView   = new DataView(Self.FBuffer);
      Self.FTyped = new Uint8Array(Self.FBuffer,0,15);
      return Self
   }
   /// destructor TDataTypeConverter.Destroy()
   ///  [line: 641, column: 31, file: System.Types.Convert]
   ,Destroy:function(Self) {
      Self.FTyped = null;
      Self.FView = null;
      Self.FBuffer = null;
      TObject.Destroy(Self);
   }
   /// function TDataTypeConverter.Float32ToBytes(const Value: float32) : TByteArray
   ///  [line: 1103, column: 29, file: System.Types.Convert]
   ,Float32ToBytes:function(Self, Value$4) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setFloat32(0,Value$4);
            break;
         case 1 :
            Self.FView.setFloat32(0,Value$4,true);
            break;
         case 2 :
            Self.FView.setFloat32(0,Value$4,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   /// function TDataTypeConverter.Float64ToBytes(const Value: float64) : TByteArray
   ///  [line: 919, column: 29, file: System.Types.Convert]
   ,Float64ToBytes:function(Self, Value$5) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setFloat64(0,Number(Value$5));
            break;
         case 1 :
            Self.FView.setFloat64(0,Number(Value$5),true);
            break;
         case 2 :
            Self.FView.setFloat64(0,Number(Value$5),false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 8 );
      return Result
   }
   /// function TDataTypeConverter.InitFloat32(const Value: float32) : float32
   ///  [line: 715, column: 35, file: System.Types.Convert]
   ,InitFloat32:function(Value$6) {
      var Result = 0;
      var temp = null;
      temp = new Float32Array(1);
      temp[0]=Value$6;
      Result = temp[0];
      temp = null;
      return Result
   }
   /// function TDataTypeConverter.InitFloat64(const Value: float64) : float64
   ///  [line: 723, column: 35, file: System.Types.Convert]
   ,InitFloat64:function(Value$7) {
      var Result = undefined;
      var temp$1 = null;
      temp$1 = new Float64Array(1);
      temp$1[0]=(Number(Value$7));
      Result = temp$1[0];
      temp$1 = null;
      return Result
   }
   /// function TDataTypeConverter.InitInt08(const Value: Byte) : Byte
   ///  [line: 747, column: 35, file: System.Types.Convert]
   ,InitInt08:function(Value$8) {
      var Result = 0;
      var temp$2 = null;
      temp$2 = new Int8Array(1);
      temp$2[0]=((Value$8<-128)?-128:(Value$8>127)?127:Value$8);
      Result = temp$2[0];
      temp$2 = null;
      return Result
   }
   /// function TDataTypeConverter.InitInt16(const Value: SmallInt) : SmallInt
   ///  [line: 739, column: 35, file: System.Types.Convert]
   ,InitInt16:function(Value$9) {
      var Result = 0;
      var temp$3 = null;
      temp$3 = new Int16Array(1);
      temp$3[0]=((Value$9<-32768)?-32768:(Value$9>32767)?32767:Value$9);
      Result = temp$3[0];
      temp$3 = null;
      return Result
   }
   /// function TDataTypeConverter.InitInt32(const Value: Integer) : Integer
   ///  [line: 731, column: 35, file: System.Types.Convert]
   ,InitInt32:function(Value$10) {
      var Result = 0;
      var temp$4 = null;
      temp$4 = new Int32Array(1);
      temp$4[0]=((Value$10<-2147483648)?-2147483648:(Value$10>2147483647)?2147483647:Value$10);
      Result = temp$4[0];
      temp$4 = null;
      return Result
   }
   /// function TDataTypeConverter.InitUint08(const Value: Byte) : Byte
   ///  [line: 771, column: 35, file: System.Types.Convert]
   ,InitUint08:function(Value$11) {
      var Result = 0;
      var LTemp = null;
      LTemp = new Uint8Array(1);
      LTemp[0]=((Value$11<0)?0:(Value$11>255)?255:Value$11);
      Result = LTemp[0];
      LTemp = null;
      return Result
   }
   /// function TDataTypeConverter.InitUint16(const Value: Word) : Word
   ///  [line: 763, column: 35, file: System.Types.Convert]
   ,InitUint16:function(Value$12) {
      var Result = 0;
      var temp$5 = null;
      temp$5 = new Uint16Array(1);
      temp$5[0]=((Value$12<0)?0:(Value$12>65536)?65536:Value$12);
      Result = temp$5[0];
      temp$5 = null;
      return Result
   }
   /// function TDataTypeConverter.InitUint32(const Value: Longword) : Longword
   ///  [line: 755, column: 35, file: System.Types.Convert]
   ,InitUint32:function(Value$13) {
      var Result = 0;
      var temp$6 = null;
      temp$6 = new Uint32Array(1);
      temp$6[0]=((Value$13<0)?0:(Value$13>4294967295)?4294967295:Value$13);
      Result = temp$6[0];
      temp$6 = null;
      return Result
   }
   /// function TDataTypeConverter.Int16ToBytes(const Value: SmallInt) : TByteArray
   ///  [line: 1259, column: 29, file: System.Types.Convert]
   ,Int16ToBytes:function(Self, Value$14) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setInt16(0,Value$14);
            break;
         case 1 :
            Self.FView.setInt16(0,Value$14,true);
            break;
         case 2 :
            Self.FView.setInt16(0,Value$14,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 2 );
      return Result
   }
   /// function TDataTypeConverter.Int32ToBytes(const Value: Integer) : TByteArray
   ///  [line: 868, column: 29, file: System.Types.Convert]
   ,Int32ToBytes:function(Self, Value$15) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setInt32(0,Value$15);
            break;
         case 1 :
            Self.FView.setInt32(0,Value$15,true);
            break;
         case 2 :
            Self.FView.setInt32(0,Value$15,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   /// procedure TDataTypeConverter.SetEndian(const NewEndian: TSystemEndianType)
   ///  [line: 649, column: 30, file: System.Types.Convert]
   ,SetEndian:function(Self, NewEndian) {
      if (NewEndian!=Self.FEndian) {
         Self.FEndian = NewEndian;
         if (Self.OnEndianChanged) {
            Self.OnEndianChanged(Self);
         }
      }
   }
   /// function TDataTypeConverter.SizeOfType(const Kind: TRTLDatatype) : Integer
   ///  [line: 685, column: 35, file: System.Types.Convert]
   ,SizeOfType:function(Self, Kind) {
      return __SIZES[Kind];
   }
   /// function TDataTypeConverter.StringToBytes(const Value: String) : TByteArray
   ///  [line: 1293, column: 29, file: System.Types.Convert]
   ,StringToBytes:function(Self, Value$16) {
      var Result = [];
      var x$5 = 0;
      var LCode = 0;
      var $temp8;
      for(x$5=0,$temp8=Value$16.length;x$5<$temp8;x$5++) {
         LCode = 0;
         LCode = (Value$16).charCodeAt(x$5);
         Result.push(LCode);
      }
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToBytes(const Value: TMemoryHandle) : TByteArray
   ///  [line: 907, column: 29, file: System.Types.Convert]
   ,TypedArrayToBytes:function(Self, Value$17) {
      var Result = [];
      if (!TVariant.ValidRef(Value$17)) {
         throw EW3Exception.Create$25($New(EConvertError),"TDataTypeConverter.TypedArrayToBytes",Self,$R[32]);
      }
      Result = Array.prototype.slice.call(Value$17);
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToStr(const Value: TMemoryHandle) : String
   ///  [line: 931, column: 29, file: System.Types.Convert]
   ,TypedArrayToStr:function(Self, Value$18) {
      var Result = "";
      var x$6 = 0;
      if (TVariant.ValidRef(Value$18)) {
         if (Value$18.length>0) {
            var $temp9;
            for(x$6=0,$temp9=Value$18.length-1;x$6<=$temp9;x$6++) {
               Result += String.fromCharCode((Value$18)[x$6]);
            }
         }
      }
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToUInt32(const Value: TMemoryHandle) : Longword
   ///  [line: 832, column: 29, file: System.Types.Convert]
   ,TypedArrayToUInt32:function(Self, Value$19) {
      var Result = 0;
      var LBuffer = null,
         LBytes = 0,
         LTypeSize = 0,
         LView = null;
      if (!TVariant.ValidRef(Value$19)) {
         throw EW3Exception.Create$25($New(EConvertError),"TDataTypeConverter.TypedArrayToUInt32",Self,$R[32]);
      }
      LBuffer = Value$19.buffer;
      LBytes = LBuffer.byteLength;
      LTypeSize = TDataTypeConverter.SizeOfType(Self.ClassType,7);
      if (LBytes<LTypeSize) {
         throw EW3Exception.Create$25($New(EConvertError),"TDataTypeConverter.TypedArrayToUInt32",Self,Format($R[31],["Int32", LTypeSize, LBytes]));
      }
      if (LBytes>LTypeSize) {
         LBytes = LTypeSize;
      }
      LView = new DataView(LBuffer);
      switch (Self.FEndian) {
         case 0 :
            Result = LView.getUint32(0);
            break;
         case 1 :
            Result = LView.getUint32(0,true);
            break;
         case 2 :
            Result = LView.getUint32(0,false);
            break;
      }
      LView = null;
      return Result
   }
   /// function TDataTypeConverter.UInt16ToBytes(const Value: Word) : TByteArray
   ///  [line: 1271, column: 29, file: System.Types.Convert]
   ,UInt16ToBytes:function(Self, Value$20) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setUint16(0,Value$20);
            break;
         case 1 :
            Self.FView.setUint16(0,Value$20,true);
            break;
         case 2 :
            Self.FView.setUint16(0,Value$20,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 2 );
      return Result
   }
   /// function TDataTypeConverter.UInt32ToBytes(const Value: Longword) : TByteArray
   ///  [line: 881, column: 29, file: System.Types.Convert]
   ,UInt32ToBytes:function(Self, Value$21) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setUint32(0,Value$21);
            break;
         case 1 :
            Self.FView.setUint32(0,Value$21,true);
            break;
         case 2 :
            Self.FView.setUint32(0,Value$21,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   /// function TDataTypeConverter.VariantToBytes(Value: Variant) : TByteArray
   ///  [line: 1141, column: 29, file: System.Types.Convert]
   ,VariantToBytes:function(Self, Value$22) {
      var Result = [];
      var LType$2 = 0;
      function GetUnSignedIntType() {
         var Result = 0;
         if (Value$22<=255) {
            Result = 162;
            return Result;
         }
         if (Value$22<=65536) {
            Result = 168;
            return Result;
         }
         if (Value$22<=2147483647) {
            Result = 169;
         }
         return Result
      };
      function GetSignedIntType() {
         var Result = 0;
         if (Value$22>-32768) {
            Result = 163;
            return Result;
         }
         if (Value$22>-2147483648) {
            Result = 164;
         }
         return Result
      };
      function IsFloat32(x$7) {
         var Result = false;
         Result = isFinite(x$7) && x$7 == Math.fround(x$7);
         return Result
      };
      switch (TW3VariantHelper$DataType(Value$22)) {
         case 2 :
            Result = [161].slice();
            Result.pusha(TDataTypeConverter.BooleanToBytes(Self.ClassType,(Value$22?true:false)));
            break;
         case 3 :
            if (Value$22<0) {
               LType$2 = GetSignedIntType();
            } else {
               LType$2 = GetUnSignedIntType();
            }
            if (LType$2) {
               Result = [LType$2].slice();
               switch (LType$2) {
                  case 162 :
                     Result.push(TDataTypeConverter.InitInt08(parseInt(Value$22,10)));
                     break;
                  case 168 :
                     Result.pusha(TDataTypeConverter.UInt16ToBytes(Self,TDataTypeConverter.InitUint16(parseInt(Value$22,10))));
                     break;
                  case 169 :
                     Result.pusha(TDataTypeConverter.UInt32ToBytes(Self,TDataTypeConverter.InitUint32(parseInt(Value$22,10))));
                     break;
                  case 163 :
                     Result.pusha(TDataTypeConverter.Int16ToBytes(Self,TDataTypeConverter.InitInt16(parseInt(Value$22,10))));
                     break;
                  case 164 :
                     Result.pusha(TDataTypeConverter.Int32ToBytes(Self,TDataTypeConverter.InitInt32(parseInt(Value$22,10))));
                     break;
               }
            } else {
               throw Exception.Create($New(EDatatype),"Invalid datatype, failed to identify number [integer] type error");
            }
            break;
         case 4 :
            if (IsFloat32(Value$22)) {
               Result = [165].slice();
               Result.pusha(TDataTypeConverter.Float32ToBytes(Self,(Number(Value$22))));
            } else {
               Result = [166].slice();
               Result.pusha(TDataTypeConverter.Float64ToBytes(Self,(Number(Value$22))));
            }
            break;
         case 5 :
            Result = [167].slice();
            Result.pusha(TString.EncodeUTF8(TString,String(Value$22)));
            break;
         default :
            throw Exception.Create($New(EDatatype),"Invalid datatype, byte conversion failed error");
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$4$:function($){return $.ClassType.Create$4($)}
};
/// TDatatype = class (TObject)
///  [line: 158, column: 3, file: System.Types.Convert]
var TDatatype = {
   $ClassName:"TDatatype",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TDatatype.BytesToBase64(const Bytes: TByteArray) : String
   ///  [line: 1453, column: 26, file: System.Types.Convert]
   ,BytesToBase64$1:function(Self, Bytes$2) {
      return TDataTypeConverter.BytesToBase64(__Def_Converter.ClassType,Bytes$2);
   }
   /// function TDatatype.BytesToTypedArray(const Values: TByteArray) : TMemoryHandle
   ///  [line: 1463, column: 26, file: System.Types.Convert]
   ,BytesToTypedArray$1:function(Self, Values$7) {
      return TDataTypeConverter.BytesToTypedArray(__Def_Converter,Values$7);
   }
   /// function TDatatype.TypedArrayToBytes(const Value: TDefaultBufferType) : TByteArray
   ///  [line: 1361, column: 26, file: System.Types.Convert]
   ,TypedArrayToBytes$1:function(Self, Value$23) {
      return TDataTypeConverter.TypedArrayToBytes(__Def_Converter,Value$23);
   }
   /// function TDatatype.TypedArrayToUInt32(const Value: TDefaultBufferType) : Longword
   ///  [line: 1366, column: 26, file: System.Types.Convert]
   ,TypedArrayToUInt32$1:function(Self, Value$24) {
      return TDataTypeConverter.TypedArrayToUInt32(__Def_Converter,Value$24);
   }
   ,Destroy:TObject.Destroy
};
/// TBase64EncDec = class (TObject)
///  [line: 292, column: 3, file: System.Types.Convert]
var TBase64EncDec = {
   $ClassName:"TBase64EncDec",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TBase64EncDec.Base64ToString(const b64: String) : String
   ///  [line: 410, column: 30, file: System.Types.Convert]
   ,Base64ToString:function(Self, b64) {
      var Result = "";
      Result = atob(b64);
      return Result
   }
   /// function TBase64EncDec.BytesToBase64(const Data: TByteArray) : String
   ///  [line: 515, column: 30, file: System.Types.Convert]
   ,BytesToBase64$2:function(Self, Data$12) {
      var Result = "";
      var LLen$5 = 0,
         LExtra = 0,
         LStrideLen = 0,
         LMaxChunkLength = 0,
         i$1 = 0,
         Ahead = 0,
         SegSize = 0,
         output = "",
         LTemp$1 = 0,
         LTemp$2 = 0;
      LLen$5 = Data$12.length;
      if (LLen$5>0) {
         LExtra = Data$12.length%3;
         LStrideLen = LLen$5-LExtra;
         LMaxChunkLength = 16383;
         i$1 = 0;
         while (i$1<LStrideLen) {
            Ahead = i$1+LMaxChunkLength;
            SegSize = (Ahead>LStrideLen)?LStrideLen:Ahead;
            Result+=TBase64EncDec.EncodeChunk(Self,Data$12,i$1,SegSize);
            (i$1+= LMaxChunkLength);
         }
         if (LExtra>0) {
            --LLen$5;
         }
         output = "";
         switch (LExtra) {
            case 1 :
               LTemp$1 = Data$12[LLen$5];
               output+=__B64_Lookup[LTemp$1>>>2];
               output+=__B64_Lookup[(LTemp$1<<4)&63];
               output+="==";
               break;
            case 2 :
               LTemp$2 = (Data$12[LLen$5-1]<<8)+Data$12[LLen$5];
               output+=__B64_Lookup[LTemp$2>>>10];
               output+=__B64_Lookup[(LTemp$2>>>4)&63];
               output+=__B64_Lookup[(LTemp$2<<2)&63];
               output+="=";
               break;
         }
         Result+=output;
      }
      return Result
   }
   /// function TBase64EncDec.EncodeChunk(const Data: TByteArray; startpos: Integer; endpos: Integer) : String
   ///  [line: 425, column: 30, file: System.Types.Convert]
   ,EncodeChunk:function(Self, Data$13, startpos, endpos) {
      var Result = "";
      var temp$7 = 0;
      while (startpos<endpos) {
         temp$7 = (Data$13[startpos]<<16)+(Data$13[startpos+1]<<8)+Data$13[startpos+2];
         Result+=__B64_Lookup[(temp$7>>>18)&63]+__B64_Lookup[(temp$7>>>12)&63]+__B64_Lookup[(temp$7>>>6)&63]+__B64_Lookup[temp$7&63];
         (startpos+= 3);
      }
      return Result
   }
   /// function TBase64EncDec.StringToBase64(const Text: String) : String
   ///  [line: 403, column: 30, file: System.Types.Convert]
   ,StringToBase64:function(Self, Text$1) {
      var Result = "";
      Result = btoa(Text$1);
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// EDatatype = class (EW3Exception)
///  [line: 22, column: 3, file: System.Types.Convert]
var EDatatype = {
   $ClassName:"EDatatype",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EConvertError = class (EW3Exception)
///  [line: 23, column: 3, file: System.Types.Convert]
var EConvertError = {
   $ClassName:"EConvertError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function SetupConversionLUT() {
   try {
      __CONV_BUFFER = new ArrayBuffer(16);
      __CONV_VIEW   = new DataView(__CONV_BUFFER);
      __CONV_ARRAY = new Uint8Array(__CONV_BUFFER,0,15);
   } catch ($e) {
      var e = $W($e);
      /* null */
   }
};
function SetupBase64() {
   var i$2 = 0;
   var $temp10;
   for(i$2=1,$temp10=CNT_B64_CHARSET.length;i$2<=$temp10;i$2++) {
      __B64_Lookup[i$2-1] = CNT_B64_CHARSET.charAt(i$2-1);
      __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,CNT_B64_CHARSET.charAt(i$2-1))] = i$2-1;
   }
   __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,"-")] = 62;
   __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,"_")] = 63;
};
/// TUnManaged = class (TObject)
///  [line: 106, column: 3, file: System.Memory]
var TUnManaged = {
   $ClassName:"TUnManaged",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TUnManaged.AllocMemA(const Size: Integer) : TMemoryHandle
   ///  [line: 245, column: 27, file: System.Memory]
   ,AllocMemA:function(Self, Size$5) {
      var Result = undefined;
      if (Size$5>0) {
         Result = new Uint8Array(Size$5);
      } else {
         Result = null;
      }
      return Result
   }
   /// function TUnManaged.ReAllocMemA(const Memory: TMemoryHandle; Size: Integer) : TMemoryHandle
   ///  [line: 264, column: 27, file: System.Memory]
   ,ReAllocMemA:function(Self, Memory$1, Size$6) {
      var Result = undefined;
      if (Memory$1) {
         if (Size$6>0) {
            Result = new Uint8Array(Size$6);
            TMarshal.Move$1(TMarshal,Memory$1,0,Result,0,Size$6);
         }
      } else {
         Result = TUnManaged.AllocMemA(Self,Size$6);
      }
      return Result
   }
   /// function TUnManaged.ReadMemoryA(const Memory: TMemoryHandle; const Offset: Integer; Size: Integer) : TMemoryHandle
   ///  [line: 347, column: 27, file: System.Memory]
   ,ReadMemoryA:function(Self, Memory$2, Offset, Size$7) {
      var Result = undefined;
      var LTotal = 0;
      if (Memory$2&&Offset>=0) {
         LTotal = Offset+Size$7;
         if (LTotal>Memory$2.length) {
            Size$7 = parseInt((Memory$2.length-LTotal),10);
         }
         if (Size$7>0) {
            Result = new Uint8Array(Memory$2.buffer.slice(Offset,Size$7));
         }
      }
      return Result
   }
   /// function TUnManaged.WriteMemoryA(const Memory: TMemoryHandle; const Offset: Integer; const Data: TMemoryHandle) : Integer
   ///  [line: 313, column: 27, file: System.Memory]
   ,WriteMemoryA:function(Self, Memory$3, Offset$1, Data$14) {
      var Result = 0;
      var mTotal,
         mChunk = null,
         mTemp = null;
      if (Memory$3) {
         if (Data$14) {
            mTotal = Offset$1+Data$14.length;
            if (mTotal>Memory$3.length) {
               Result = parseInt((Memory$3.length-mTotal),10);
            } else {
               Result = parseInt(Data$14.length,10);
            }
            if (Result>0) {
               if (Offset$1+Data$14.length<=Memory$3.length) {
                  Memory$3.set(Data$14,Offset$1);
               } else {
                  mChunk = Data$14.buffer.slice(0,Result-1);
                  mTemp = new Uint8Array(mChunk);
                  Memory$3.set(mTemp,Offset$1);
               }
            }
         }
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// function TMemoryHandleHelper.Valid(const Self: TMemoryHandle) : Boolean
///  [line: 212, column: 30, file: System.Memory]
function TMemoryHandleHelper$Valid$3(Self$10) {
   var Result = false;
   Result = !( (Self$10 == undefined) || (Self$10 == null) );
   return Result
}
/// function TMemoryHandleHelper.Defined(const Self: TMemoryHandle) : Boolean
///  [line: 219, column: 30, file: System.Memory]
function TMemoryHandleHelper$Defined$1(Self$11) {
   var Result = false;
   Result = !(Self$11 == undefined);
   return Result
}
/// TMarshal = class (TObject)
///  [line: 133, column: 3, file: System.Memory]
var TMarshal = {
   $ClassName:"TMarshal",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// procedure TMarshal.Move(const Source: TMemoryHandle; const SourceStart: Integer; const Target: TMemoryHandle; const TargetStart: Integer; const Size: Integer)
   ///  [line: 553, column: 26, file: System.Memory]
   ,Move$1:function(Self, Source, SourceStart, Target, TargetStart, Size$8) {
      var LRef = null;
      if (TMemoryHandleHelper$Defined$1(Source)&&TMemoryHandleHelper$Valid$3(Source)&&SourceStart>=0) {
         if (TMemoryHandleHelper$Defined$1(Target)&&TMemoryHandleHelper$Valid$3(Target)&&TargetStart>=0) {
            LRef = Source.subarray(SourceStart,SourceStart+Size$8);
            Target.set(LRef,TargetStart);
         }
      }
   }
   /// procedure TMarshal.Move(const Source: TAddress; const Target: TAddress; const Size: Integer)
   ///  [line: 570, column: 26, file: System.Memory]
   ,Move:function(Self, Source$1, Target$1, Size$9) {
      if (Source$1!==null) {
         if (Target$1!==null) {
            if (Size$9>0) {
               TMarshal.Move$1(Self,Source$1.FBuffer$1,Source$1.FOffset$1,Target$1.FBuffer$1,Target$1.FOffset$1,Size$9);
            }
         }
      }
   }
   ,Destroy:TObject.Destroy
};
/// function TBufferHandleHelper.Valid(const Self: TBufferHandle) : Boolean
///  [line: 180, column: 30, file: System.Memory]
function TBufferHandleHelper$Valid$4(Self$12) {
   var Result = false;
   Result = !( (Self$12 == undefined) || (Self$12 == null) );
   return Result
}
/// TAddress = class (TObject)
///  [line: 71, column: 3, file: System.Streams]
var TAddress = {
   $ClassName:"TAddress",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FBuffer$1 = undefined;
      $.FOffset$1 = 0;
   }
   /// constructor TAddress.Create(const Memory: TBinaryData)
   ///  [line: 259, column: 22, file: System.Memory.Buffer]
   ,Create$28:function(Self, Memory$4) {
      if (Memory$4!==null) {
         if (TAllocation.GetSize$3(Memory$4)>0) {
            TAddress.Create$26(Self,TAllocation.GetHandle(Memory$4),0);
         } else {
            throw Exception.Create($New(Exception),"Invalid memory object error");
         }
      } else {
         throw Exception.Create($New(Exception),"Invalid memory object error");
      }
      return Self
   }
   /// constructor TAddress.Create(const Stream: TStream)
   ///  [line: 228, column: 22, file: System.Streams]
   ,Create$27:function(Self, Stream) {
      var LRef$1 = undefined;
      if ($Is(Stream,TMemoryStream)) {
         LRef$1 = TAllocation.GetHandle($As(Stream,TMemoryStream).FBuffer$2);
         if (LRef$1) {
            TAddress.Create$26(Self,LRef$1,0);
         } else {
            throw Exception.Create($New(EAddress),$R[3]);
         }
      } else {
         throw Exception.Create($New(EAddress),$R[4]);
      }
      return Self
   }
   /// constructor TAddress.Create(const Segment: TBufferHandle; const Offset: Integer)
   ///  [line: 621, column: 22, file: System.Memory]
   ,Create$26:function(Self, Segment$1, Offset$2) {
      TObject.Create(Self);
      if (Segment$1&&TBufferHandleHelper$Valid$4(Segment$1)) {
         Self.FBuffer$1 = Segment$1;
      } else {
         throw Exception.Create($New(EAddress),"Failed to derive address, invalid segment error");
      }
      if (Offset$2>=0) {
         Self.FOffset$1 = Offset$2;
      } else {
         throw Exception.Create($New(EAddress),"Failed to derive address, invalid offset error");
      }
      return Self
   }
   /// destructor TAddress.Destroy()
   ///  [line: 635, column: 21, file: System.Memory]
   ,Destroy:function(Self) {
      Self.FBuffer$1 = null;
      Self.FOffset$1 = 0;
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// EAddress = class (EW3Exception)
///  [line: 83, column: 3, file: System.Memory]
var EAddress = {
   $ClassName:"EAddress",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3OwnedErrorObject = class (TW3OwnedObject)
///  [line: 78, column: 3, file: System.Objects]
var TW3OwnedErrorObject = {
   $ClassName:"TW3OwnedErrorObject",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.FLastError = "";
      $.FOptions$3 = null;
   }
   /// procedure TW3OwnedErrorObject.ClearLastError()
   ///  [line: 279, column: 31, file: System.Objects]
   ,ClearLastError:function(Self) {
      Self.FLastError = "";
   }
   /// constructor TW3OwnedErrorObject.Create(const AOwner: TObject)
   ///  [line: 227, column: 33, file: System.Objects]
   ,Create$18:function(Self, AOwner$1) {
      TW3OwnedObject.Create$18(Self,AOwner$1);
      Self.FOptions$3 = TW3ErrorObjectOptions.Create$37($New(TW3ErrorObjectOptions));
      return Self
   }
   /// destructor TW3OwnedErrorObject.Destroy()
   ///  [line: 233, column: 32, file: System.Objects]
   ,Destroy:function(Self) {
      TObject.Free(Self.FOptions$3);
      TObject.Destroy(Self);
   }
   /// function TW3OwnedErrorObject.GetFailed() : Boolean
   ///  [line: 244, column: 30, file: System.Objects]
   ,GetFailed:function(Self) {
      return Self.FLastError.length>0;
   }
   /// procedure TW3OwnedErrorObject.SetLastError(const ErrorText: String)
   ///  [line: 249, column: 31, file: System.Objects]
   ,SetLastError:function(Self, ErrorText$1) {
      Self.FLastError = Trim$_String_(ErrorText$1);
      if (Self.FLastError.length>0) {
         if (Self.FOptions$3.AutoWriteToConsole) {
            if (console) {
          console.log( (Self.FLastError) );
        }
         }
         if (Self.FOptions$3.ThrowExceptions) {
            throw Exception.Create($New(EW3ErrorObject),Self.FLastError);
         }
      }
   }
   /// procedure TW3OwnedErrorObject.SetLastErrorF(const ErrorText: String; const Values: array of const)
   ///  [line: 273, column: 31, file: System.Objects]
   ,SetLastErrorF:function(Self, ErrorText$2, Values$8) {
      Self.FLastError = Format(ErrorText$2,Values$8.slice(0));
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$18$:function($){return $.ClassType.Create$18.apply($.ClassType, arguments)}
};
TW3OwnedErrorObject.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3OwnedLockedErrorObject = class (TW3OwnedErrorObject)
///  [line: 96, column: 3, file: System.Objects]
var TW3OwnedLockedErrorObject = {
   $ClassName:"TW3OwnedLockedErrorObject",$Parent:TW3OwnedErrorObject
   ,$Init:function ($) {
      TW3OwnedErrorObject.$Init($);
      $.OnObjectUnLocked = null;
      $.OnObjectLocked = null;
      $.FLocked$2 = 0;
   }
   /// procedure TW3OwnedLockedErrorObject.DisableAlteration()
   ///  [line: 189, column: 37, file: System.Objects]
   ,DisableAlteration$2:function(Self) {
      ++Self.FLocked$2;
      if (Self.FLocked$2==1) {
         TW3OwnedLockedErrorObject.ObjectLocked$2(Self);
      }
   }
   /// procedure TW3OwnedLockedErrorObject.EnableAlteration()
   ///  [line: 196, column: 37, file: System.Objects]
   ,EnableAlteration$2:function(Self) {
      if (Self.FLocked$2>0) {
         --Self.FLocked$2;
         if (!Self.FLocked$2) {
            TW3OwnedLockedErrorObject.ObjectUnLocked$2(Self);
         }
      }
   }
   /// function TW3OwnedLockedErrorObject.GetLockState() : Boolean
   ///  [line: 206, column: 36, file: System.Objects]
   ,GetLockState$2:function(Self) {
      return Self.FLocked$2>0;
   }
   /// procedure TW3OwnedLockedErrorObject.ObjectLocked()
   ///  [line: 211, column: 37, file: System.Objects]
   ,ObjectLocked$2:function(Self) {
      if (Self.OnObjectLocked) {
         Self.OnObjectLocked(Self);
      }
   }
   /// procedure TW3OwnedLockedErrorObject.ObjectUnLocked()
   ///  [line: 217, column: 37, file: System.Objects]
   ,ObjectUnLocked$2:function(Self) {
      if (Self.OnObjectUnLocked) {
         Self.OnObjectUnLocked(Self);
      }
   }
   ,Destroy:TW3OwnedErrorObject.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$18:TW3OwnedErrorObject.Create$18
};
TW3OwnedLockedErrorObject.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3HandleBasedObject = class (TObject)
///  [line: 115, column: 3, file: System.Objects]
var TW3HandleBasedObject = {
   $ClassName:"TW3HandleBasedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FHandle$1 = undefined;
   }
   /// procedure TW3HandleBasedObject.ObjectHandleChanged(const PreviousHandle: THandle; const NewHandle: THandle)
   ///  [line: 160, column: 32, file: System.Objects]
   ,ObjectHandleChanged:function(Self, PreviousHandle, NewHandle) {
      /* null */
   }
   /// function TW3HandleBasedObject.AcceptObjectHandle(const CandidateHandle: THandle) : Boolean
   ///  [line: 154, column: 31, file: System.Objects]
   ,AcceptObjectHandle:function(Self, CandidateHandle) {
      return true;
   }
   /// procedure TW3HandleBasedObject.SetObjectHandle(const NewHandle: THandle)
   ///  [line: 173, column: 32, file: System.Objects]
   ,SetObjectHandle:function(Self, NewHandle$1) {
      var LTemp$3 = undefined;
      if (TW3HandleBasedObject.AcceptObjectHandle(Self,NewHandle$1)) {
         LTemp$3 = Self.FHandle$1;
         Self.FHandle$1 = NewHandle$1;
         TW3HandleBasedObject.ObjectHandleChanged(Self,LTemp$3,Self.FHandle$1);
      } else {
         throw EW3Exception.CreateFmt($New(EW3HandleBasedObject),$R[30],["TW3HandleBasedObject.SetObjectHandle"]);
      }
   }
   /// function TW3HandleBasedObject.GetObjectHandle() : THandle
   ///  [line: 168, column: 31, file: System.Objects]
   ,GetObjectHandle:function(Self) {
      return Self.FHandle$1;
   }
   ,Destroy:TObject.Destroy
};
/// TW3ErrorObjectOptions = class (TObject)
///  [line: 27, column: 3, file: System.Objects]
var TW3ErrorObjectOptions = {
   $ClassName:"TW3ErrorObjectOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.AutoWriteToConsole = $.ThrowExceptions = $.AutoResetError = false;
   }
   /// constructor TW3ErrorObjectOptions.Create()
   ///  [line: 139, column: 35, file: System.Objects]
   ,Create$37:function(Self) {
      Self.AutoResetError = true;
      Self.AutoWriteToConsole = false;
      Self.ThrowExceptions = false;
      return Self
   }
   ,Destroy:TObject.Destroy
};
/// TW3ErrorObject = class (TObject)
///  [line: 56, column: 3, file: System.Objects]
var TW3ErrorObject = {
   $ClassName:"TW3ErrorObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FLastError$1 = "";
      $.FOptions$4 = null;
   }
   /// procedure TW3ErrorObject.ClearLastError()
   ///  [line: 351, column: 26, file: System.Objects]
   ,ClearLastError$1:function(Self) {
      Self.FLastError$1 = "";
   }
   /// constructor TW3ErrorObject.Create()
   ///  [line: 288, column: 28, file: System.Objects]
   ,Create$38:function(Self) {
      TObject.Create(Self);
      Self.FOptions$4 = TW3ErrorObjectOptions.Create$37($New(TW3ErrorObjectOptions));
      return Self
   }
   /// destructor TW3ErrorObject.Destroy()
   ///  [line: 294, column: 27, file: System.Objects]
   ,Destroy:function(Self) {
      TObject.Free(Self.FOptions$4);
      TObject.Destroy(Self);
   }
   /// function TW3ErrorObject.GetExceptionClass() : TW3ExceptionClass
   ///  [line: 356, column: 25, file: System.Objects]
   ,GetExceptionClass:function(Self) {
      return EW3ErrorObject;
   }
   /// function TW3ErrorObject.GetFailed() : Boolean
   ///  [line: 305, column: 25, file: System.Objects]
   ,GetFailed$1:function(Self) {
      return Self.FLastError$1.length>0;
   }
   /// function TW3ErrorObject.GetLastError() : String
   ///  [line: 300, column: 25, file: System.Objects]
   ,GetLastError$1:function(Self) {
      return Self.FLastError$1;
   }
   /// procedure TW3ErrorObject.SetLastError(const ErrorText: String)
   ///  [line: 310, column: 26, file: System.Objects]
   ,SetLastError$1:function(Self, ErrorText$3) {
      var ErrClass = null;
      Self.FLastError$1 = Trim$_String_(ErrorText$3);
      if (Self.FLastError$1.length>0) {
         if (Self.FOptions$4.AutoWriteToConsole) {
            if (console) {
          console.log( (Self.FLastError$1) );
       }
         }
         if (Self.FOptions$4.ThrowExceptions) {
            ErrClass = TW3ErrorObject.GetExceptionClass$(Self);
            if (!ErrClass) {
               ErrClass = EW3ErrorObject;
            }
            throw Exception.Create($NewDyn(ErrClass,""),Self.FLastError$1);
         }
      }
   }
   /// procedure TW3ErrorObject.SetLastErrorF(const ErrorText: String; const Values: array of const)
   ///  [line: 345, column: 26, file: System.Objects]
   ,SetLastErrorF$1:function(Self, ErrorText$4, Values$9) {
      TW3ErrorObject.SetLastError$1(Self,Format(ErrorText$4,Values$9.slice(0)));
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$38$:function($){return $.ClassType.Create$38($)}
   ,GetExceptionClass$:function($){return $.ClassType.GetExceptionClass($)}
};
TW3ErrorObject.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// EW3HandleBasedObject = class (EW3Exception)
///  [line: 113, column: 3, file: System.Objects]
var EW3HandleBasedObject = {
   $ClassName:"EW3HandleBasedObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EW3ErrorObject = class (EW3Exception)
///  [line: 21, column: 3, file: System.Objects]
var EW3ErrorObject = {
   $ClassName:"EW3ErrorObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3CustomReader = class (TDataTypeConverter)
///  [line: 38, column: 3, file: System.Reader]
var TW3CustomReader = {
   $ClassName:"TW3CustomReader",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FAccess$2 = null;
      $.FOffset$2 = $.FTotalSize$1 = 0;
      $.FOptions$2 = [0];
   }
   /// constructor TW3CustomReader.Create(const Access: IBinaryTransport)
   ///  [line: 113, column: 29, file: System.Reader]
   ,Create$34:function(Self, Access$2) {
      TDataTypeConverter.Create$4(Self);
      Self.FOptions$2 = [1];
      Self.FAccess$2 = Access$2;
      Self.FOffset$2 = Self.FAccess$2[0]();
      Self.FTotalSize$1 = Self.FAccess$2[1]();
      return Self
   }
   /// function TW3CustomReader.GetReadOffset() : Integer
   ///  [line: 156, column: 26, file: System.Reader]
   ,GetReadOffset:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions$2,0,0,1)) {
         Result = Self.FOffset$2;
      } else {
         Result = Self.FAccess$2[0]();
      }
      return Result
   }
   /// function TW3CustomReader.GetTotalSize() : Integer
   ///  [line: 148, column: 26, file: System.Reader]
   ,GetTotalSize$2:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions$2,0,0,1)) {
         Result = Self.FTotalSize$1;
      } else {
         Result = Self.FAccess$2[1]();
      }
      return Result
   }
   /// function TW3CustomReader.QueryBreachOfBoundary(const NumberOfBytes: Integer) : Boolean
   ///  [line: 164, column: 26, file: System.Reader]
   ,QueryBreachOfBoundary$1:function(Self, NumberOfBytes) {
      return TW3CustomReader.GetTotalSize$2(Self)-TW3CustomReader.GetReadOffset(Self)<NumberOfBytes;
   }
   /// function TW3CustomReader.Read(const BytesToRead: Integer) : TByteArray
   ///  [line: 169, column: 26, file: System.Reader]
   ,Read$1:function(Self, BytesToRead) {
      var Result = [];
      if (BytesToRead>0) {
         Result = Self.FAccess$2[2](TW3CustomReader.GetReadOffset(Self),BytesToRead);
         if ($SetIn(Self.FOptions$2,0,0,1)) {
            (Self.FOffset$2+= Result.length);
         }
      } else {
         throw EW3Exception.Create$25($New(EW3ReadError),"TW3CustomReader.Read",Self,("Invalid read length ("+BytesToRead.toString()+")"));
      }
      return Result
   }
   /// function TW3CustomReader.ReadInteger() : Integer
   ///  [line: 246, column: 26, file: System.Reader]
   ,ReadInteger:function(Self) {
      var Result = 0;
      var LBytesToRead = 0;
      LBytesToRead = TDataTypeConverter.SizeOfType(TDataTypeConverter,7);
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,LBytesToRead)) {
         throw EW3Exception.Create$25($New(EW3ReadError),"TW3CustomReader.ReadInteger",Self,Format($R[10],[LBytesToRead]));
      } else {
         Result = TDataTypeConverter.BytesToInt32(Self,TW3CustomReader.Read$1(Self,LBytesToRead));
      }
      return Result
   }
   /// function TW3CustomReader.ReadStr(const BytesToRead: Integer) : String
   ///  [line: 306, column: 26, file: System.Reader]
   ,ReadStr:function(Self, BytesToRead$1) {
      var Result = "";
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,BytesToRead$1)) {
         throw EW3Exception.Create$25($New(EW3ReadError),"TW3CustomReader.ReadStr",Self,Format($R[10],[BytesToRead$1]));
      } else if (BytesToRead$1>0) {
         Result = TDataTypeConverter.BytesToString(Self,TW3CustomReader.Read$1(Self,BytesToRead$1));
      }
      return Result
   }
   /// function TW3CustomReader.ReadString() : String
   ///  [line: 317, column: 26, file: System.Reader]
   ,ReadString$1:function(Self) {
      var Result = "";
      var LSignature = 0,
         LLength = 0;
      LSignature = TW3CustomReader.ReadInteger(Self);
      if (LSignature==3131756270) {
         LLength = TW3CustomReader.ReadInteger(Self);
         if (LLength>0) {
            Result = TW3CustomReader.ReadStr(Self,LLength);
         }
      } else {
         throw EW3Exception.Create$25($New(EW3ReadError),"TW3CustomReader.ReadString",Self,Format($R[11],["string"]));
      }
      return Result
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$4:TDataTypeConverter.Create$4
};
/// TReader = class (TW3CustomReader)
///  [line: 82, column: 3, file: System.Reader]
var TReader = {
   $ClassName:"TReader",$Parent:TW3CustomReader
   ,$Init:function ($) {
      TW3CustomReader.$Init($);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$4:TDataTypeConverter.Create$4
};
/// EW3ReadError = class (EW3Exception)
///  [line: 34, column: 3, file: System.Reader]
var EW3ReadError = {
   $ClassName:"EW3ReadError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TStream = class (TDataTypeConverter)
///  [line: 80, column: 3, file: System.Streams]
var TStream = {
   $ClassName:"TStream",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
   }
   /// function TStream.CopyFrom(const Source: TStream; Count: Integer) : Integer
   ///  [line: 467, column: 18, file: System.Streams]
   ,CopyFrom:function(Self, Source$2, Count$5) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.DataGetSize() : Integer
   ///  [line: 433, column: 18, file: System.Streams]
   ,DataGetSize:function(Self) {
      return TStream.GetSize$1$(Self);
   }
   /// function TStream.DataOffset() : Integer
   ///  [line: 427, column: 18, file: System.Streams]
   ,DataOffset:function(Self) {
      return TStream.GetPosition$(Self);
   }
   /// function TStream.DataRead(const Offset: Integer; const ByteCount: Integer) : TByteArray
   ///  [line: 439, column: 19, file: System.Streams]
   ,DataRead:function(Self, Offset$3, ByteCount) {
      return TStream.ReadBuffer$(Self,Offset$3,ByteCount);
   }
   /// procedure TStream.DataWrite(const Offset: Integer; const Bytes: TByteArray)
   ///  [line: 445, column: 19, file: System.Streams]
   ,DataWrite:function(Self, Offset$4, Bytes$3) {
      TStream.WriteBuffer$(Self,Bytes$3,Offset$4);
   }
   /// function TStream.GetPosition() : Integer
   ///  [line: 472, column: 18, file: System.Streams]
   ,GetPosition:function(Self) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.GetSize() : Integer
   ///  [line: 492, column: 18, file: System.Streams]
   ,GetSize$1:function(Self) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.Read(const Count: Integer) : TByteArray
   ///  [line: 450, column: 18, file: System.Streams]
   ,Read:function(Self, Count$6) {
      return TStream.ReadBuffer$(Self,TStream.GetPosition$(Self),Count$6);
   }
   /// function TStream.ReadBuffer(Offset: Integer; Count: Integer) : TByteArray
   ///  [line: 502, column: 18, file: System.Streams]
   ,ReadBuffer:function(Self, Offset$5, Count$7) {
      var Result = [];
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.Seek(const Offset: Integer; Origin: TStreamSeekOrigin) : Integer
   ///  [line: 487, column: 18, file: System.Streams]
   ,Seek:function(Self, Offset$6, Origin) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// procedure TStream.SetPosition(NewPosition: Integer)
   ///  [line: 477, column: 19, file: System.Streams]
   ,SetPosition:function(Self, NewPosition) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   /// procedure TStream.SetSize(NewSize: Integer)
   ///  [line: 482, column: 19, file: System.Streams]
   ,SetSize:function(Self, NewSize) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   /// function TStream.Skip(Amount: Integer) : Integer
   ///  [line: 497, column: 18, file: System.Streams]
   ,Skip:function(Self, Amount) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.Write(const Buffer: TByteArray) : Integer
   ///  [line: 455, column: 18, file: System.Streams]
   ,Write$1:function(Self, Buffer$1) {
      var Result = 0;
      TStream.WriteBuffer$(Self,Buffer$1,TStream.GetPosition$(Self));
      Result = Buffer$1.length;
      return Result
   }
   /// procedure TStream.WriteBuffer(const Buffer: TByteArray; Offset: Integer)
   ///  [line: 507, column: 19, file: System.Streams]
   ,WriteBuffer:function(Self, Buffer$2, Offset$7) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$4:TDataTypeConverter.Create$4
   ,CopyFrom$:function($){return $.ClassType.CopyFrom.apply($.ClassType, arguments)}
   ,GetPosition$:function($){return $.ClassType.GetPosition($)}
   ,GetSize$1$:function($){return $.ClassType.GetSize$1($)}
   ,ReadBuffer$:function($){return $.ClassType.ReadBuffer.apply($.ClassType, arguments)}
   ,Seek$:function($){return $.ClassType.Seek.apply($.ClassType, arguments)}
   ,SetPosition$:function($){return $.ClassType.SetPosition.apply($.ClassType, arguments)}
   ,SetSize$:function($){return $.ClassType.SetSize.apply($.ClassType, arguments)}
   ,Skip$:function($){return $.ClassType.Skip.apply($.ClassType, arguments)}
   ,WriteBuffer$:function($){return $.ClassType.WriteBuffer.apply($.ClassType, arguments)}
};
TStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// TMemoryStream = class (TStream)
///  [line: 122, column: 3, file: System.Streams]
var TMemoryStream = {
   $ClassName:"TMemoryStream",$Parent:TStream
   ,$Init:function ($) {
      TStream.$Init($);
      $.FBuffer$2 = null;
      $.FPos = 0;
   }
   /// function TMemoryStream.CopyFrom(const Source: TStream; Count: Integer) : Integer
   ///  [line: 258, column: 24, file: System.Streams]
   ,CopyFrom:function(Self, Source$3, Count$8) {
      var Result = 0;
      var LData = [];
      LData = TStream.ReadBuffer$(Source$3,TStream.GetPosition$(Source$3),Count$8);
      TStream.WriteBuffer$(Self,LData,TStream.GetPosition$(Self));
      Result = LData.length;
      return Result
   }
   /// constructor TMemoryStream.Create()
   ///  [line: 246, column: 27, file: System.Streams]
   ,Create$4:function(Self) {
      TDataTypeConverter.Create$4(Self);
      Self.FBuffer$2 = TDataTypeConverter.Create$4$($New(TAllocation));
      return Self
   }
   /// destructor TMemoryStream.Destroy()
   ///  [line: 252, column: 26, file: System.Streams]
   ,Destroy:function(Self) {
      TObject.Free(Self.FBuffer$2);
      TDataTypeConverter.Destroy(Self);
   }
   /// function TMemoryStream.GetPosition() : Integer
   ///  [line: 265, column: 24, file: System.Streams]
   ,GetPosition:function(Self) {
      return Self.FPos;
   }
   /// function TMemoryStream.GetSize() : Integer
   ///  [line: 337, column: 24, file: System.Streams]
   ,GetSize$1:function(Self) {
      return TAllocation.GetSize$3(Self.FBuffer$2);
   }
   /// function TMemoryStream.ReadBuffer(Offset: Integer; Count: Integer) : TByteArray
   ///  [line: 355, column: 24, file: System.Streams]
   ,ReadBuffer:function(Self, Offset$8, Count$9) {
      var Result = [];
      var LLen$6 = 0,
         LBytesToMove = 0,
         LTemp$4 = null;
      LLen$6 = 0;
      if (TStream.GetPosition$(Self)<TStream.GetSize$1$(Self)) {
         LLen$6 = TStream.GetSize$1$(Self)-TStream.GetPosition$(Self);
      }
      if (LLen$6>0) {
         try {
            LBytesToMove = Count$9;
            if (LBytesToMove>LLen$6) {
               LBytesToMove = LLen$6;
            }
            LTemp$4 = new Uint8Array(LBytesToMove);
            TMarshal.Move$1(TMarshal,TAllocation.GetHandle(Self.FBuffer$2),Offset$8,LTemp$4,0,LBytesToMove);
            Result = TDataTypeConverter.TypedArrayToBytes(Self,LTemp$4);
            TStream.SetPosition$(Self,Offset$8+Result.length);
         } catch ($e) {
            var e$1 = $W($e);
            throw EW3Exception.CreateFmt($New(EStreamReadError),$R[8],[e$1.FMessage]);
         }
      }
      return Result
   }
   /// function TMemoryStream.Seek(const Offset: Integer; Origin: TStreamSeekOrigin) : Integer
   ///  [line: 309, column: 24, file: System.Streams]
   ,Seek:function(Self, Offset$9, Origin$1) {
      var Result = 0;
      var LSize = 0;
      LSize = TStream.GetSize$1$(Self);
      if (LSize>0) {
         switch (Origin$1) {
            case 0 :
               if (Offset$9>-1) {
                  TStream.SetPosition$(Self,Offset$9);
                  Result = Offset$9;
               }
               break;
            case 1 :
               Result = TInteger.EnsureRange((TStream.GetPosition$(Self)+Offset$9),0,LSize);
               TStream.SetPosition$(Self,Result);
               break;
            case 2 :
               Result = TInteger.EnsureRange((TStream.GetSize$1$(Self)-Math.abs(Offset$9)),0,LSize);
               TStream.SetPosition$(Self,Result);
               break;
         }
      }
      return Result
   }
   /// procedure TMemoryStream.SetPosition(NewPosition: Integer)
   ///  [line: 270, column: 25, file: System.Streams]
   ,SetPosition:function(Self, NewPosition$1) {
      var LSize$1 = 0;
      LSize$1 = TStream.GetSize$1$(Self);
      if (LSize$1>0) {
         Self.FPos = TInteger.EnsureRange(NewPosition$1,0,LSize$1);
      }
   }
   /// procedure TMemoryStream.SetSize(NewSize: Integer)
   ///  [line: 277, column: 25, file: System.Streams]
   ,SetSize:function(Self, NewSize$1) {
      var LSize$2 = 0,
         LDiff = 0,
         LDiff$1 = 0;
      LSize$2 = TStream.GetSize$1$(Self);
      if (NewSize$1>0) {
         if (NewSize$1>LSize$2) {
            LDiff = NewSize$1-LSize$2;
            if (TAllocation.GetSize$3(Self.FBuffer$2)+LDiff>0) {
               TAllocation.Grow(Self.FBuffer$2,LDiff);
            } else {
               TAllocation.Release$1(Self.FBuffer$2);
            }
         } else {
            LDiff$1 = LSize$2-NewSize$1;
            if (TAllocation.GetSize$3(Self.FBuffer$2)-LDiff$1>0) {
               TAllocation.Shrink(Self.FBuffer$2,LDiff$1);
            } else {
               TAllocation.Release$1(Self.FBuffer$2);
            }
         }
      } else {
         TAllocation.Release$1(Self.FBuffer$2);
      }
      Self.FPos = TInteger.EnsureRange(Self.FPos,0,TStream.GetSize$1$(Self));
   }
   /// function TMemoryStream.Skip(Amount: Integer) : Integer
   ///  [line: 342, column: 24, file: System.Streams]
   ,Skip:function(Self, Amount$1) {
      var Result = 0;
      var LSize$3 = 0,
         LTotal$1 = 0;
      LSize$3 = TStream.GetSize$1$(Self);
      if (LSize$3>0) {
         LTotal$1 = TStream.GetPosition$(Self)+Amount$1;
         if (LTotal$1>LSize$3) {
            LTotal$1 = LSize$3-LTotal$1;
         }
         (Self.FPos+= LTotal$1);
         Result = LTotal$1;
      }
      return Result
   }
   /// procedure TMemoryStream.WriteBuffer(const Buffer: TByteArray; Offset: Integer)
   ///  [line: 383, column: 25, file: System.Streams]
   ,WriteBuffer:function(Self, Buffer$3, Offset$10) {
      var mData = undefined,
         mData$1 = undefined;
      try {
         if (TAllocation.a$26(Self.FBuffer$2)&&Offset$10<1) {
            TAllocation.Allocate(Self.FBuffer$2,Buffer$3.length);
            mData = TDataTypeConverter.BytesToTypedArray(Self,Buffer$3);
            TMarshal.Move$1(TMarshal,mData,0,TAllocation.GetHandle(Self.FBuffer$2),0,Buffer$3.length);
         } else {
            if (TStream.GetPosition$(Self)==TStream.GetSize$1$(Self)) {
               TAllocation.Grow(Self.FBuffer$2,Buffer$3.length);
               mData$1 = TDataTypeConverter.BytesToTypedArray(Self,Buffer$3);
               TMarshal.Move$1(TMarshal,mData$1,0,TAllocation.GetHandle(Self.FBuffer$2),Offset$10,Buffer$3.length);
            } else {
               TMarshal.Move$1(TMarshal,TDataTypeConverter.BytesToTypedArray(Self,Buffer$3),0,TAllocation.GetHandle(Self.FBuffer$2),Offset$10,Buffer$3.length);
            }
         }
         TStream.SetPosition$(Self,Offset$10+Buffer$3.length);
      } catch ($e) {
         var e$2 = $W($e);
         throw EW3Exception.CreateFmt($New(EStreamWriteError),$R[7],[e$2.FMessage]);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$4$:function($){return $.ClassType.Create$4($)}
   ,CopyFrom$:function($){return $.ClassType.CopyFrom.apply($.ClassType, arguments)}
   ,GetPosition$:function($){return $.ClassType.GetPosition($)}
   ,GetSize$1$:function($){return $.ClassType.GetSize$1($)}
   ,ReadBuffer$:function($){return $.ClassType.ReadBuffer.apply($.ClassType, arguments)}
   ,Seek$:function($){return $.ClassType.Seek.apply($.ClassType, arguments)}
   ,SetPosition$:function($){return $.ClassType.SetPosition.apply($.ClassType, arguments)}
   ,SetSize$:function($){return $.ClassType.SetSize.apply($.ClassType, arguments)}
   ,Skip$:function($){return $.ClassType.Skip.apply($.ClassType, arguments)}
   ,WriteBuffer$:function($){return $.ClassType.WriteBuffer.apply($.ClassType, arguments)}
};
TMemoryStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// TStreamSeekOrigin enumeration
///  [line: 48, column: 3, file: System.Streams]
var TStreamSeekOrigin = [ "soFromBeginning", "soFromCurrent", "soFromEnd" ];
/// TCustomFileStream = class (TStream)
///  [line: 151, column: 3, file: System.Streams]
var TCustomFileStream = {
   $ClassName:"TCustomFileStream",$Parent:TStream
   ,$Init:function ($) {
      TStream.$Init($);
      $.FAccess$1 = 0;
      $.FFilename = "";
   }
   /// procedure TCustomFileStream.SetAccessMode(const Value: TFileAccessMode)
   ///  [line: 214, column: 29, file: System.Streams]
   ,SetAccessMode:function(Self, Value$25) {
      Self.FAccess$1 = Value$25;
   }
   /// procedure TCustomFileStream.SetFilename(const Value: String)
   ///  [line: 219, column: 29, file: System.Streams]
   ,SetFilename:function(Self, Value$26) {
      Self.FFilename = Value$26;
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$4:TDataTypeConverter.Create$4
   ,CopyFrom:TStream.CopyFrom
   ,GetPosition:TStream.GetPosition
   ,GetSize$1:TStream.GetSize$1
   ,ReadBuffer:TStream.ReadBuffer
   ,Seek:TStream.Seek
   ,SetPosition:TStream.SetPosition
   ,SetSize:TStream.SetSize
   ,Skip:TStream.Skip
   ,WriteBuffer:TStream.WriteBuffer
   ,Create$30$:function($){return $.ClassType.Create$30.apply($.ClassType, arguments)}
};
TCustomFileStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// EStream = class (EW3Exception)
///  [line: 56, column: 3, file: System.Streams]
var EStream = {
   $ClassName:"EStream",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EStreamWriteError = class (EStream)
///  [line: 58, column: 3, file: System.Streams]
var EStreamWriteError = {
   $ClassName:"EStreamWriteError",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EStreamReadError = class (EStream)
///  [line: 57, column: 3, file: System.Streams]
var EStreamReadError = {
   $ClassName:"EStreamReadError",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EStreamNotImplemented = class (EStream)
///  [line: 59, column: 3, file: System.Streams]
var EStreamNotImplemented = {
   $ClassName:"EStreamNotImplemented",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TAllocationOptions = class (TW3OwnedObject)
///  [line: 100, column: 3, file: System.Memory.Allocation]
var TAllocationOptions = {
   $ClassName:"TAllocationOptions",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.FCacheSize = 0;
      $.FUseCache = false;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 113, column: 41, file: System.Memory.Allocation]
   ,a$25:function(Self) {
      return $As(TW3OwnedObject.GetOwner(Self),TAllocation);
   }
   /// function TAllocationOptions.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 134, column: 29, file: System.Memory.Allocation]
   ,AcceptOwner:function(Self, CandidateObject$1) {
      return CandidateObject$1!==null&&$Is(CandidateObject$1,TAllocation);
   }
   /// constructor TAllocationOptions.Create(const AOwner: TAllocation)
   ///  [line: 127, column: 32, file: System.Memory.Allocation]
   ,Create$31:function(Self, AOwner$2) {
      TW3OwnedObject.Create$18(Self,AOwner$2);
      Self.FCacheSize = 4096;
      Self.FUseCache = true;
      return Self
   }
   /// function TAllocationOptions.GetCacheFree() : Integer
   ///  [line: 140, column: 29, file: System.Memory.Allocation]
   ,GetCacheFree:function(Self) {
      return Self.FCacheSize-TAllocationOptions.GetCacheUsed(Self);
   }
   /// function TAllocationOptions.GetCacheUsed() : Integer
   ///  [line: 145, column: 29, file: System.Memory.Allocation]
   ,GetCacheUsed:function(Self) {
      var Result = 0;
      if (Self.FUseCache) {
         Result = parseInt((Self.FCacheSize-(TAllocation.GetHandle(TAllocationOptions.a$25(Self)).length-TAllocation.GetSize$3(TAllocationOptions.a$25(Self)))),10);
      }
      return Result
   }
   /// procedure TAllocationOptions.SetCacheSize(const NewCacheSize: Integer)
   ///  [line: 156, column: 30, file: System.Memory.Allocation]
   ,SetCacheSize:function(Self, NewCacheSize) {
      Self.FCacheSize = (NewCacheSize<1024)?1024:(NewCacheSize>1024000)?1024000:NewCacheSize;
   }
   /// procedure TAllocationOptions.SetUseCache(const NewUseValue: Boolean)
   ///  [line: 151, column: 30, file: System.Memory.Allocation]
   ,SetUseCache:function(Self, NewUseValue) {
      Self.FUseCache = NewUseValue;
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$18:TW3OwnedObject.Create$18
};
TAllocationOptions.$Intf={
   IW3OwnedObjectAccess:[TAllocationOptions.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TAllocation = class (TDataTypeConverter)
///  [line: 51, column: 3, file: System.Memory.Allocation]
var TAllocation = {
   $ClassName:"TAllocation",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FHandle = undefined;
      $.FOptions$1 = null;
      $.FSize = 0;
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 73, column: 37, file: System.Memory.Allocation]
   ,a$26:function(Self) {
      return ((!Self.FHandle)?true:false);
   }
   /// procedure TAllocation.Allocate(const NumberOfBytes: Integer)
   ///  [line: 258, column: 23, file: System.Memory.Allocation]
   ,Allocate:function(Self, NumberOfBytes$1) {
      var NewSize$2 = 0;
      if (Self.FHandle) {
         TAllocation.Release$1(Self);
      }
      if (NumberOfBytes$1>0) {
         NewSize$2 = 0;
         if (Self.FOptions$1.FUseCache) {
            NewSize$2 = TInteger.ToNearest(NumberOfBytes$1,Self.FOptions$1.FCacheSize);
         } else {
            NewSize$2 = NumberOfBytes$1;
         }
         Self.FHandle = TUnManaged.AllocMemA(TUnManaged,NewSize$2);
         Self.FSize = NumberOfBytes$1;
         TAllocation.HandleAllocated$(Self);
      }
   }
   /// constructor TAllocation.Create(const ByteSize: Integer)
   ///  [line: 179, column: 25, file: System.Memory.Allocation]
   ,Create$33:function(Self, ByteSize) {
      TDataTypeConverter.Create$4$(Self);
      if (ByteSize>0) {
         TAllocation.Allocate(Self,ByteSize);
      }
      return Self
   }
   /// constructor TAllocation.Create()
   ///  [line: 173, column: 25, file: System.Memory.Allocation]
   ,Create$4:function(Self) {
      TDataTypeConverter.Create$4(Self);
      Self.FOptions$1 = TAllocationOptions.Create$31($New(TAllocationOptions),Self);
      return Self
   }
   /// function TAllocation.DataGetSize() : Integer
   ///  [line: 230, column: 22, file: System.Memory.Allocation]
   ,DataGetSize$1:function(Self) {
      return TAllocation.GetSize$3(Self);
   }
   /// function TAllocation.DataOffset() : Integer
   ///  [line: 224, column: 22, file: System.Memory.Allocation]
   ,DataOffset$1:function(Self) {
      return 0;
   }
   /// function TAllocation.DataRead(const Offset: Integer; const ByteCount: Integer) : TByteArray
   ///  [line: 236, column: 22, file: System.Memory.Allocation]
   ,DataRead$1:function(Self, Offset$11, ByteCount$1) {
      return TDataTypeConverter.TypedArrayToBytes(Self,TUnManaged.ReadMemoryA(TUnManaged,TAllocation.GetHandle(Self),Offset$11,ByteCount$1));
   }
   /// procedure TAllocation.DataWrite(const Offset: Integer; const Bytes: TByteArray)
   ///  [line: 243, column: 23, file: System.Memory.Allocation]
   ,DataWrite$1:function(Self, Offset$12, Bytes$4) {
      TUnManaged.WriteMemoryA(TUnManaged,TAllocation.GetHandle(Self),Offset$12,TDataTypeConverter.BytesToTypedArray(Self,Bytes$4));
   }
   /// destructor TAllocation.Destroy()
   ///  [line: 186, column: 24, file: System.Memory.Allocation]
   ,Destroy:function(Self) {
      if (Self.FHandle) {
         TAllocation.Release$1(Self);
      }
      TObject.Free(Self.FOptions$1);
      TDataTypeConverter.Destroy(Self);
   }
   /// function TAllocation.GetBufferHandle() : TBufferHandle
   ///  [line: 430, column: 22, file: System.Memory.Allocation]
   ,GetBufferHandle:function(Self) {
      var Result = undefined;
      if (Self.FHandle) {
         Result = Self.FHandle.buffer;
      }
      return Result
   }
   /// function TAllocation.GetHandle() : TMemoryHandle
   ///  [line: 425, column: 22, file: System.Memory.Allocation]
   ,GetHandle:function(Self) {
      return Self.FHandle;
   }
   /// function TAllocation.GetSize() : Integer
   ///  [line: 420, column: 22, file: System.Memory.Allocation]
   ,GetSize$3:function(Self) {
      return Self.FSize;
   }
   /// function TAllocation.GetTotalSize() : Integer
   ///  [line: 414, column: 22, file: System.Memory.Allocation]
   ,GetTotalSize$1:function(Self) {
      var Result = 0;
      if (Self.FHandle) {
         Result = parseInt(Self.FHandle.length,10);
      }
      return Result
   }
   /// function TAllocation.GetTransport() : IBinaryTransport
   ///  [line: 195, column: 22, file: System.Memory.Allocation]
   ,GetTransport:function(Self) {
      return $AsIntf(Self,"IBinaryTransport");
   }
   /// procedure TAllocation.Grow(const NumberOfBytes: Integer)
   ///  [line: 300, column: 23, file: System.Memory.Allocation]
   ,Grow:function(Self, NumberOfBytes$2) {
      var ExactNewSize = 0,
         TotalNewSize = 0;
      if (Self.FHandle) {
         ExactNewSize = Self.FSize+NumberOfBytes$2;
         if (Self.FOptions$1.FUseCache) {
            if (NumberOfBytes$2<TAllocationOptions.GetCacheFree(Self.FOptions$1)) {
               (Self.FSize+= NumberOfBytes$2);
            } else {
               TotalNewSize = TInteger.ToNearest(ExactNewSize,Self.FOptions$1.FCacheSize);
               TAllocation.ReAllocate(Self,TotalNewSize);
               Self.FSize = ExactNewSize;
            }
         } else {
            TAllocation.ReAllocate(Self,ExactNewSize);
         }
      } else {
         TAllocation.Allocate(Self,NumberOfBytes$2);
      }
   }
   /// procedure TAllocation.HandleAllocated()
   ///  [line: 248, column: 23, file: System.Memory.Allocation]
   ,HandleAllocated:function(Self) {
      /* null */
   }
   /// procedure TAllocation.HandleReleased()
   ///  [line: 253, column: 23, file: System.Memory.Allocation]
   ,HandleReleased:function(Self) {
      /* null */
   }
   /// procedure TAllocation.ReAllocate(const NewSize: Integer)
   ///  [line: 379, column: 23, file: System.Memory.Allocation]
   ,ReAllocate:function(Self, NewSize$3) {
      var LSizeToSet = 0;
      if (NewSize$3>0) {
         if (Self.FHandle) {
            if (NewSize$3!=Self.FSize) {
               TAllocation.HandleReleased$(Self);
               LSizeToSet = 0;
               if (Self.FOptions$1.FUseCache) {
                  LSizeToSet = TInteger.ToNearest(NewSize$3,Self.FOptions$1.FCacheSize);
               } else {
                  LSizeToSet = TInteger.ToNearest(NewSize$3,16);
               }
               Self.FHandle = TUnManaged.ReAllocMemA(TUnManaged,Self.FHandle,LSizeToSet);
               Self.FSize = NewSize$3;
            }
         } else {
            TAllocation.Allocate(Self,NewSize$3);
         }
         TAllocation.HandleAllocated$(Self);
      } else {
         TAllocation.Release$1(Self);
      }
   }
   /// procedure TAllocation.Release()
   ///  [line: 287, column: 23, file: System.Memory.Allocation]
   ,Release$1:function(Self) {
      if (Self.FHandle) {
         try {
            Self.FHandle = null;
            Self.FSize = 0;
         } finally {
            TAllocation.HandleReleased$(Self);
         }
      }
   }
   /// procedure TAllocation.Shrink(const NumberOfBytes: Integer)
   ///  [line: 332, column: 23, file: System.Memory.Allocation]
   ,Shrink:function(Self, NumberOfBytes$3) {
      var ExactNewSize$1 = 0,
         Spare = 0,
         AlignedSize = 0;
      if (Self.FHandle) {
         ExactNewSize$1 = TInteger.EnsureRange((Self.FSize-NumberOfBytes$3),0,2147483647);
         if (Self.FOptions$1.FUseCache) {
            if (ExactNewSize$1>0) {
               Spare = ExactNewSize$1%Self.FOptions$1.FCacheSize;
               if (Spare>0) {
                  AlignedSize = ExactNewSize$1;
                  (AlignedSize+= (Self.FOptions$1.FCacheSize-Spare));
                  TAllocation.ReAllocate(Self,AlignedSize);
                  Self.FSize = ExactNewSize$1;
               } else {
                  Self.FSize = ExactNewSize$1;
               }
            } else {
               TAllocation.Release$1(Self);
            }
         } else if (ExactNewSize$1>0) {
            TAllocation.ReAllocate(Self,ExactNewSize$1);
         } else {
            TAllocation.Release$1(Self);
         }
      }
   }
   /// procedure TAllocation.Transport(const Target: IBinaryTransport)
   ///  [line: 200, column: 23, file: System.Memory.Allocation]
   ,Transport:function(Self, Target$2) {
      var Data$15 = [];
      if (Target$2===null) {
         throw Exception.Create($New(EAllocation),"Invalid transport interface, reference was NIL error");
      } else {
         if (!TAllocation.a$26(Self)) {
            try {
               Data$15 = TDataTypeConverter.TypedArrayToBytes(Self,TAllocation.GetHandle(Self));
               Target$2[3](Target$2[0](),Data$15);
            } catch ($e) {
               var e$3 = $W($e);
               throw EW3Exception.CreateFmt($New(EAllocation),"Data transport failed, mechanism threw exception %s with error [%s]",[TObject.ClassName(e$3.ClassType), e$3.FMessage]);
            }
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$4$:function($){return $.ClassType.Create$4($)}
   ,HandleAllocated$:function($){return $.ClassType.HandleAllocated($)}
   ,HandleReleased$:function($){return $.ClassType.HandleReleased($)}
};
TAllocation.$Intf={
   IAllocation:[TAllocation.GetHandle,TAllocation.GetTotalSize$1,TAllocation.GetSize$3,TAllocation.GetTransport,TAllocation.Allocate,TAllocation.Grow,TAllocation.Shrink,TAllocation.ReAllocate,TAllocation.Transport,TAllocation.Release$1]
   ,IBinaryTransport:[TAllocation.DataOffset$1,TAllocation.DataGetSize$1,TAllocation.DataRead$1,TAllocation.DataWrite$1]
}
function a$239(Self) {
   return ((!Self[0]())?true:false);
}/// EAllocation = class (EW3Exception)
///  [line: 22, column: 3, file: System.Memory.Allocation]
var EAllocation = {
   $ClassName:"EAllocation",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3CustomWriter = class (TDataTypeConverter)
///  [line: 39, column: 3, file: System.Writer]
var TW3CustomWriter = {
   $ClassName:"TW3CustomWriter",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FAccess = null;
      $.FOffset = $.FTotalSize = 0;
      $.FOptions = [0];
   }
   /// constructor TW3CustomWriter.Create(const Access: IBinaryTransport)
   ///  [line: 102, column: 29, file: System.Writer]
   ,Create$5:function(Self, Access$3) {
      TDataTypeConverter.Create$4(Self);
      Self.FAccess = Access$3;
      Self.FOffset = Self.FAccess[0]();
      Self.FTotalSize = Self.FAccess[1]();
      Self.FOptions = [3];
      return Self
   }
   /// function TW3CustomWriter.GetOffset() : Integer
   ///  [line: 125, column: 26, file: System.Writer]
   ,GetOffset:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions,0,0,2)) {
         Result = Self.FOffset;
      } else {
         Result = Self.FAccess[0]();
      }
      return Result
   }
   /// function TW3CustomWriter.GetTotalFree() : Integer
   ///  [line: 146, column: 26, file: System.Writer]
   ,GetTotalFree:function(Self) {
      return Self.FAccess[1]()-TW3CustomWriter.GetOffset(Self);
   }
   /// function TW3CustomWriter.GetTotalSize() : Integer
   ///  [line: 116, column: 26, file: System.Writer]
   ,GetTotalSize:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions,0,0,2)) {
         Result = 2147483647;
      } else {
         Result = Self.FAccess[1]();
      }
      return Result
   }
   /// function TW3CustomWriter.QueryBreachOfBoundary(const BytesToFit: Integer) : Boolean
   ///  [line: 133, column: 26, file: System.Writer]
   ,QueryBreachOfBoundary:function(Self, BytesToFit) {
      var Result = false;
      if (BytesToFit>=1) {
         if ($SetIn(Self.FOptions,1,0,2)) {
            Result = false;
         } else {
            Result = TW3CustomWriter.GetTotalFree(Self)<BytesToFit;
         }
      }
      return Result
   }
   /// procedure TW3CustomWriter.SetAccessOptions(const NewOptions: TW3WriterOptions)
   ///  [line: 111, column: 27, file: System.Writer]
   ,SetAccessOptions:function(Self, NewOptions) {
      Self.FOptions = NewOptions.slice(0);
   }
   /// function TW3CustomWriter.Write(const Data: TByteArray) : Integer
   ///  [line: 151, column: 26, file: System.Writer]
   ,Write:function(Self, Data$16) {
      var Result = 0;
      var LBytesToWrite = 0,
         LBytesLeft = 0,
         LBytesMissing = 0;
      LBytesToWrite = Data$16.length;
      if (LBytesToWrite>0) {
         if ($SetIn(Self.FOptions,1,0,2)) {
            Self.FAccess[3](TW3CustomWriter.GetOffset(Self),Data$16);
            if ($SetIn(Self.FOptions,0,0,2)) {
               (Self.FOffset+= Data$16.length);
            }
         } else {
            if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite)) {
               LBytesLeft = TW3CustomWriter.GetTotalSize(Self)-TW3CustomWriter.GetOffset(Self);
               LBytesMissing = Math.abs(LBytesLeft-LBytesToWrite);
               (LBytesToWrite-= LBytesMissing);
               $ArraySetLenC(Data$16,LBytesToWrite,function (){return 0});
            }
            if (LBytesToWrite>1) {
               Self.FAccess[3](TW3CustomWriter.GetOffset(Self),Data$16);
               if ($SetIn(Self.FOptions,0,0,2)) {
                  (Self.FOffset+= Data$16.length);
               }
            } else {
               throw EW3Exception.Create$25($New(EW3WriteError),"TW3CustomWriter.Write",Self,Format($R[12],[Data$16.length]));
            }
         }
         Result = Data$16.length;
      } else {
         throw EW3Exception.Create$25($New(EW3WriteError),"TW3CustomWriter.Write",Self,Format($R[14],[LBytesToWrite]));
      }
      return Result
   }
   /// procedure TW3CustomWriter.WriteInteger(const Value: Integer)
   ///  [line: 294, column: 27, file: System.Writer]
   ,WriteInteger:function(Self, Value$27) {
      var LBytesToWrite$1 = 0;
      LBytesToWrite$1 = TDataTypeConverter.SizeOfType(TDataTypeConverter,7);
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite$1)) {
         throw EW3Exception.Create$25($New(EW3WriteError),"TW3CustomWriter.WriteInteger",Self,Format($R[12],[LBytesToWrite$1]));
      } else {
         TW3CustomWriter.Write(Self,TDataTypeConverter.Int32ToBytes(Self,Value$27));
      }
   }
   /// procedure TW3CustomWriter.WriteString(const Value: String)
   ///  [line: 325, column: 27, file: System.Writer]
   ,WriteString:function(Self, Value$28) {
      var LTotal$2 = 0,
         LBytes$1 = [];
      LTotal$2 = TDataTypeConverter.SizeOfType(TDataTypeConverter,7);
      (LTotal$2+= TDataTypeConverter.SizeOfType(TDataTypeConverter,7));
      LBytes$1 = TDataTypeConverter.StringToBytes(Self,Value$28);
      (LTotal$2+= LBytes$1.length);
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LTotal$2)) {
         throw EW3Exception.Create$25($New(EW3WriteError),"TW3CustomWriter.WriteString",Self,Format($R[12],[LTotal$2]));
      } else {
         try {
            TW3CustomWriter.WriteInteger(Self,3131756270);
            TW3CustomWriter.WriteInteger(Self,LBytes$1.length);
            if (LBytes$1.length>0) {
               TW3CustomWriter.Write(Self,LBytes$1);
            }
         } catch ($e) {
            var e$4 = $W($e);
            throw EW3Exception.Create$25($New(EW3WriteError),"TW3CustomWriter.WriteString",Self,e$4.FMessage);
         }
      }
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$4:TDataTypeConverter.Create$4
};
/// TWriter = class (TW3CustomWriter)
///  [line: 76, column: 3, file: System.Writer]
var TWriter = {
   $ClassName:"TWriter",$Parent:TW3CustomWriter
   ,$Init:function ($) {
      TW3CustomWriter.$Init($);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$4:TDataTypeConverter.Create$4
};
/// TStreamWriter = class (TW3CustomWriter)
///  [line: 79, column: 3, file: System.Writer]
var TStreamWriter = {
   $ClassName:"TStreamWriter",$Parent:TW3CustomWriter
   ,$Init:function ($) {
      TW3CustomWriter.$Init($);
   }
   /// constructor TStreamWriter.Create(const Stream: TStream)
   ///  [line: 92, column: 27, file: System.Writer]
   ,Create$6:function(Self, Stream$1) {
      TW3CustomWriter.Create$5(Self,$AsIntf(Stream$1,"IBinaryTransport"));
      TW3CustomWriter.SetAccessOptions(Self,[3]);
      return Self
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$4:TDataTypeConverter.Create$4
};
/// EW3WriteError = class (EW3Exception)
///  [line: 31, column: 3, file: System.Writer]
var EW3WriteError = {
   $ClassName:"EW3WriteError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TJSONObject = class (TObject)
///  [line: 36, column: 3, file: System.JSON]
var TJSONObject = {
   $ClassName:"TJSONObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FInstance = undefined;
      $.FOptions$5 = [0];
   }
   /// function TJSONObject.AddOrSet(const PropertyName: String; const Data: Variant) : TJSONObject
   ///  [line: 404, column: 22, file: System.JSON]
   ,AddOrSet:function(Self, PropertyName, Data$17) {
      var Result = null;
      Result = Self;
      if (TJSONObject.Exists(Self,PropertyName)) {
         if ($SetIn(Self.FOptions$5,3,0,4)) {
            Self.FInstance[PropertyName] = Data$17;
         } else {
            throw EW3Exception.CreateFmt($New(EJSONObject),"Failed to set value[%s], instance does not allow alteration",[PropertyName]);
         }
      } else if ($SetIn(Self.FOptions$5,1,0,4)) {
         Self.FInstance[PropertyName] = Data$17;
      } else {
         throw EW3Exception.CreateFmt($New(EJSONObject),"Failed to add value [%s], instance does not allow new properties",[PropertyName]);
      }
      return Result
   }
   /// procedure TJSONObject.Clear()
   ///  [line: 335, column: 23, file: System.JSON]
   ,Clear$1:function(Self) {
      Self.FInstance = TVariant.CreateObject();
   }
   /// constructor TJSONObject.Create(const Instance: TJsInstance; const Options: TJSONObjectOptions; Clone: Boolean)
   ///  [line: 177, column: 25, file: System.JSON]
   ,Create$42:function(Self, Instance$4, Options$2, Clone$1) {
      TObject.Create(Self);
      Self.FOptions$5 = Options$2.slice(0);
      if (TW3VariantHelper$Valid$2(Instance$4)) {
         if (TW3VariantHelper$IsObject(Instance$4)) {
            if (Clone$1) {
               Self.FInstance = TVariant.CreateObject();
               TVariant.ForEachProperty(Instance$4,function (Name$5, Data$18) {
                  var Result = 1;
                  TJSONObject.AddOrSet(Self,Name$5,Data$18);
                  Result = 1;
                  return Result
               });
            } else {
               Self.FInstance = Instance$4;
            }
         } else {
            throw Exception.Create($New(EJSONObject),"Failed to clone instance, reference is not an object");
         }
      } else {
         if ($SetIn(Self.FOptions$5,0,0,4)) {
            Self.FInstance = TVariant.CreateObject();
         } else {
            throw Exception.Create($New(EJSONObject),"Instance was nil, provided options does not allow initialization error");
         }
      }
      return Self
   }
   /// constructor TJSONObject.Create(const Instance: TJsInstance; const Options: TJSONObjectOptions)
   ///  [line: 153, column: 25, file: System.JSON]
   ,Create$41:function(Self, Instance$5, Options$3) {
      TObject.Create(Self);
      Self.FOptions$5 = Options$3.slice(0);
      if (TW3VariantHelper$Valid$2(Instance$5)&&TW3VariantHelper$IsObject(Instance$5)) {
         Self.FInstance = Instance$5;
      } else {
         if ($SetIn(Self.FOptions$5,0,0,4)) {
            Self.FInstance = TVariant.CreateObject();
         } else {
            throw Exception.Create($New(EJSONObject),"Instance was nil, provided options does not allow initialization error");
         }
      }
      return Self
   }
   /// constructor TJSONObject.Create(const Instance: TJsInstance)
   ///  [line: 133, column: 25, file: System.JSON]
   ,Create$40:function(Self, Instance$6) {
      TObject.Create(Self);
      Self.FOptions$5 = [15];
      if (TW3VariantHelper$Valid$2(Instance$6)) {
         if (TW3VariantHelper$IsObject(Instance$6)) {
            Self.FInstance = Instance$6;
         } else {
            throw Exception.Create($New(EJSONObject),"Failed to inspect instance, reference is not an object");
         }
      } else {
         Self.FInstance = TVariant.CreateObject();
      }
      return Self
   }
   /// constructor TJSONObject.Create()
   ///  [line: 126, column: 25, file: System.JSON]
   ,Create$39:function(Self) {
      TObject.Create(Self);
      Self.FOptions$5 = [15];
      Self.FInstance = TVariant.CreateObject();
      return Self
   }
   /// destructor TJSONObject.Destroy()
   ,Destroy$10:function(Self) {
      Self.FInstance = null;
      TObject.Destroy(Self);
   }
   /// function TJSONObject.Exists(const PropertyName: String) : Boolean
   ///  [line: 423, column: 22, file: System.JSON]
   ,Exists:function(Self, PropertyName$1) {
      return (Object.hasOwnProperty.call(Self.FInstance,PropertyName$1)?true:false);
   }
   /// procedure TJSONObject.FromJSON(const Text: String)
   ///  [line: 301, column: 23, file: System.JSON]
   ,FromJSON:function(Self, Text$2) {
      Self.FInstance = JSON.parse(Text$2);
   }
   /// procedure TJSONObject.LoadFromStream(const Stream: TStream)
   ///  [line: 318, column: 23, file: System.JSON]
   ,LoadFromStream:function(Self, Stream$2) {
      var LReader = null;
      LReader = TW3CustomReader.Create$34($New(TReader),$AsIntf(Stream$2,"IBinaryTransport"));
      try {
         Self.FInstance = JSON.parse(TString.DecodeBase64(TString,TW3CustomReader.ReadString$1(LReader)));
      } finally {
         TObject.Free(LReader);
      }
   }
   /// function TJSONObject.Read(const PropertyName: String; var Data: Variant) : TJSONObject
   ///  [line: 380, column: 22, file: System.JSON]
   ,Read$2:function(Self, PropertyName$2, Data$19) {
      var Result = null;
      Result = Self;
      if (TJSONObject.Exists(Self,PropertyName$2)) {
         Data$19.v = Self.FInstance[PropertyName$2];
      } else {
         throw EW3Exception.CreateFmt($New(EJSONObject),"Failed to read value, property [%s] not found error",[PropertyName$2]);
      }
      return Result
   }
   /// procedure TJSONObject.SaveToStream(const Stream: TStream)
   ///  [line: 306, column: 23, file: System.JSON]
   ,SaveToStream:function(Self, Stream$3) {
      var LWriter = null;
      LWriter = TW3CustomWriter.Create$5($New(TWriter),$AsIntf(Stream$3,"IBinaryTransport"));
      try {
         TW3CustomWriter.WriteString(LWriter,TString.EncodeBase64(TString,JSON.stringify(Self.FInstance)));
      } finally {
         TObject.Free(LWriter);
      }
   }
   /// function TJSONObject.ToJSON() : String
   ///  [line: 296, column: 22, file: System.JSON]
   ,ToJSON:function(Self) {
      return JSON.stringify(Self.FInstance,null,2);
   }
   ,Destroy:TObject.Destroy
};
/// EJSONObject = class (EW3Exception)
///  [line: 34, column: 3, file: System.JSON]
var EJSONObject = {
   $ClassName:"EJSONObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3DirectoryParser = class (TW3ErrorObject)
///  [line: 49, column: 3, file: System.IOUtils]
var TW3DirectoryParser = {
   $ClassName:"TW3DirectoryParser",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
   }
   /// function TW3DirectoryParser.GetErrorObject() : IW3ErrorAccess
   ///  [line: 176, column: 29, file: System.IOUtils]
   ,GetErrorObject:function(Self) {
      return $AsIntf(Self,"IW3ErrorAccess");
   }
   /// function TW3DirectoryParser.IsPathRooted(FilePath: String) : Boolean
   ///  [line: 181, column: 29, file: System.IOUtils]
   ,IsPathRooted:function(Self, FilePath) {
      var Result = false;
      var LMoniker = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      FilePath = (Trim$_String_(FilePath)).toLocaleLowerCase();
      if (FilePath.length>0) {
         LMoniker = TW3DirectoryParser.GetRootMoniker$(Self);
         Result = StrBeginsWith(FilePath,LMoniker);
      }
      return Result
   }
   /// function TW3DirectoryParser.IsRelativePath(FilePath: String) : Boolean
   ///  [line: 194, column: 29, file: System.IOUtils]
   ,IsRelativePath:function(Self, FilePath$1) {
      var Result = false;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TW3DirectoryParser.IsValidPath$(Self,FilePath$1)) {
         Result = !StrBeginsWith(FilePath$1,TW3DirectoryParser.GetRootMoniker$(Self));
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$38:TW3ErrorObject.Create$38
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3DirectoryParser.$Intf={
   IW3DirectoryParser:[TW3DirectoryParser.GetPathSeparator,TW3DirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3DirectoryParser.IsValidPath,TW3DirectoryParser.HasValidPathChars,TW3DirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3DirectoryParser.GetFileNameWithoutExtension,TW3DirectoryParser.GetPathName,TW3DirectoryParser.GetDevice,TW3DirectoryParser.GetFileName,TW3DirectoryParser.GetExtension,TW3DirectoryParser.GetDirectoryName,TW3DirectoryParser.IncludeTrailingPathDelimiter,TW3DirectoryParser.IncludeLeadingPathDelimiter,TW3DirectoryParser.ExcludeLeadingPathDelimiter,TW3DirectoryParser.ExcludeTrailingPathDelimiter,TW3DirectoryParser.ChangeFileExt]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TW3UnixDirectoryParser = class (TW3DirectoryParser)
///  [line: 81, column: 3, file: System.IOUtils]
var TW3UnixDirectoryParser = {
   $ClassName:"TW3UnixDirectoryParser",$Parent:TW3DirectoryParser
   ,$Init:function ($) {
      TW3DirectoryParser.$Init($);
   }
   /// function TW3UnixDirectoryParser.ChangeFileExt(const FilePath: String; NewExt: String) : String
   ///  [line: 670, column: 33, file: System.IOUtils]
   ,ChangeFileExt:function(Self, FilePath$2, NewExt) {
      NewExt={v:NewExt};
      var Result = "";
      var Separator = "",
         x$8 = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      Separator = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrEndsWith(FilePath$2,Separator)) {
         TW3ErrorObject.SetLastError$1(Self,"Failed to change extension, path has no filename error");
         Result = FilePath$2;
         return Result;
      }
      NewExt.v = Trim$_String_(NewExt.v);
      while ((NewExt.v.charAt(0)==".")) {
         Delete(NewExt,1,1);
         if (NewExt.v.length<1) {
            break;
         }
      }
      if (NewExt.v.length>0) {
         NewExt.v = "."+NewExt.v;
      }
      for(x$8=FilePath$2.length;x$8>=1;x$8--) {
         {var $temp11 = FilePath$2.charAt(x$8-1);
            if ($temp11==".") {
               Result = FilePath$2.substr(0,(x$8-1))+NewExt.v;
               break;
            }
             else if ($temp11==Separator) {
               Result = FilePath$2+NewExt.v;
               break;
            }
         }
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter(const FilePath: String) : String
   ///  [line: 723, column: 33, file: System.IOUtils]
   ,ExcludeLeadingPathDelimiter:function(Self, FilePath$3) {
      var Result = "";
      if (StrBeginsWith(FilePath$3,TW3DirectoryParser.GetPathSeparator$(Self))) {
         Result = FilePath$3.substr(1);
      } else {
         Result = FilePath$3;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter(const FilePath: String) : String
   ///  [line: 739, column: 33, file: System.IOUtils]
   ,ExcludeTrailingPathDelimiter:function(Self, FilePath$4) {
      var Result = "";
      if (StrEndsWith(FilePath$4,TW3DirectoryParser.GetPathSeparator$(Self))) {
         Result = (FilePath$4).substr(0,(FilePath$4.length-1));
      } else {
         Result = FilePath$4;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetDevice(const FilePath: String) : String
   ///  [line: 517, column: 33, file: System.IOUtils]
   ,GetDevice:function(Self, FilePath$5) {
      var Result = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$5.length>0) {
         if (StrBeginsWith(FilePath$5,TW3DirectoryParser.GetRootMoniker$(Self))) {
            Result = TW3DirectoryParser.GetRootMoniker$(Self);
         } else {
            Result = "";
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract device, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetDirectoryName(const FilePath: String) : String
   ///  [line: 604, column: 33, file: System.IOUtils]
   ,GetDirectoryName:function(Self, FilePath$6) {
      var Result = "";
      var Separator$1 = "",
         NameParts = [];
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$6.length>0) {
         Separator$1 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(FilePath$6,Separator$1)) {
            Result = FilePath$6;
            return Result;
         }
         NameParts = (FilePath$6).split(Separator$1);
         NameParts.splice((NameParts.length-1),1)
         ;
         Result = (NameParts).join(Separator$1)+Separator$1;
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract directory, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetExtension(const Filename: String) : String
   ///  [line: 627, column: 33, file: System.IOUtils]
   ,GetExtension:function(Self, Filename$1) {
      var Result = "";
      var Separator$2 = "",
         x$9 = 0;
      var dx = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Filename$1.length>0) {
         Separator$2 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Filename$1,Separator$2)) {
            TW3ErrorObject.SetLastError$1(Self,"Failed to extract extension, path contains no filename error");
         } else {
            for(x$9=Filename$1.length;x$9>=1;x$9--) {
               {var $temp12 = Filename$1.charAt(x$9-1);
                  if ($temp12==".") {
                     dx = Filename$1.length;
                     (dx-= x$9);
                     ++dx;
                     Result = RightStr(Filename$1,dx);
                     break;
                  }
                   else if ($temp12==Separator$2) {
                     break;
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract extension, filename was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetFileName(const FilePath: String) : String
   ///  [line: 585, column: 33, file: System.IOUtils]
   ,GetFileName:function(Self, FilePath$7) {
      var Result = "";
      var Temp$1 = "",
         Separator$3 = "",
         Parts = [];
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      Temp$1 = Trim$_String_(FilePath$7);
      if (Temp$1.length>0) {
         Separator$3 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Temp$1,Separator$3)) {
            TW3ErrorObject.SetLastError$1(Self,"No filename part in path error");
         } else {
            Parts = (Temp$1).split(Separator$3);
            Result = Parts[(Parts.length-1)];
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract filename, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetFileNameWithoutExtension(const Filename: String) : String
   ///  [line: 575, column: 33, file: System.IOUtils]
   ,GetFileNameWithoutExtension:function(Self, Filename$2) {
      var Result = "";
      var temp$8 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      temp$8 = TW3DirectoryParser.GetFileName$(Self,Filename$2);
      if (!TW3ErrorObject.GetFailed$1(Self)) {
         Result = TW3DirectoryParser.ChangeFileExt$(Self,temp$8,"");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetPathName(const FilePath: String) : String
   ///  [line: 532, column: 33, file: System.IOUtils]
   ,GetPathName:function(Self, FilePath$8) {
      var Result = "";
      var Temp$2 = "",
         Parts$1 = [],
         Separator$4 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      Temp$2 = Trim$_String_(FilePath$8);
      if (Temp$2.length>0) {
         Separator$4 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Temp$2,Separator$4)) {
            if (Temp$2==TW3DirectoryParser.GetRootMoniker$(Self)) {
               TW3ErrorObject.SetLastError$1(Self,"Failed to get directory name, path is root");
               return "";
            }
            Temp$2 = (Temp$2).substr(0,(Temp$2.length-1));
            Parts$1 = (Temp$2).split(Separator$4);
            Result = Parts$1[(Parts$1.length-1)];
            return Result;
         }
         Parts$1 = (Temp$2).split(Separator$4);
         if (Parts$1.length>1) {
            Result = Parts$1[(Parts$1.length-1)-1];
         } else {
            Result = Parts$1[(Parts$1.length-1)];
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract directory name, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetPathSeparator() : Char
   ///  [line: 398, column: 33, file: System.IOUtils]
   ,GetPathSeparator:function(Self) {
      return "\/";
   }
   /// function TW3UnixDirectoryParser.GetRootMoniker() : String
   ///  [line: 403, column: 33, file: System.IOUtils]
   ,GetRootMoniker:function(Self) {
      return "~\/";
   }
   /// function TW3UnixDirectoryParser.HasValidFileNameChars(FileName: String) : Boolean
   ///  [line: 408, column: 33, file: System.IOUtils]
   ,HasValidFileNameChars:function(Self, FileName) {
      var Result = false;
      var el = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FileName.length>0) {
         if ((FileName.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \" \" in filename \"%s\" error",[FileName]);
         } else {
            for (var $temp13=0;$temp13<FileName.length;$temp13++) {
               el=$uniCharAt(FileName,$temp13);
               if (!el) continue;
               Result = (((el>="A")&&(el<="Z"))||((el>="a")&&(el<="z"))||((el>="0")&&(el<="9"))||(el=="-")||(el=="_")||(el==".")||(el==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \"%s\" in filename \"%s\" error",[el, FileName]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.HasValidPathChars(FolderName: String) : Boolean
   ///  [line: 437, column: 33, file: System.IOUtils]
   ,HasValidPathChars:function(Self, FolderName) {
      var Result = false;
      var el$1 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if ((FolderName.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \" \" in foldername \"%s\" error",[FolderName]);
      } else {
         if (FolderName.length>0) {
            for (var $temp14=0;$temp14<FolderName.length;$temp14++) {
               el$1=$uniCharAt(FolderName,$temp14);
               if (!el$1) continue;
               Result = (((el$1>="A")&&(el$1<="Z"))||((el$1>="a")&&(el$1<="z"))||((el$1>="0")&&(el$1<="9"))||(el$1=="-")||(el$1=="_")||(el$1==".")||(el$1==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \"%s\" in foldername \"%s\" error",[el$1, FolderName]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.IncludeLeadingPathDelimiter(const FilePath: String) : String
   ///  [line: 714, column: 33, file: System.IOUtils]
   ,IncludeLeadingPathDelimiter:function(Self, FilePath$9) {
      var Result = "";
      var Separator$5 = "";
      Separator$5 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$9,Separator$5)) {
         Result = FilePath$9;
      } else {
         Result = Separator$5+FilePath$9;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.IncludeTrailingPathDelimiter(const FilePath: String) : String
   ///  [line: 731, column: 33, file: System.IOUtils]
   ,IncludeTrailingPathDelimiter:function(Self, FilePath$10) {
      var Result = "";
      var LSeparator = "";
      LSeparator = TW3DirectoryParser.GetPathSeparator$(Self);
      Result = FilePath$10;
      if (!StrEndsWith(Result,LSeparator)) {
         Result+=LSeparator;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.IsValidPath(FilePath: String) : Boolean
   ///  [line: 466, column: 33, file: System.IOUtils]
   ,IsValidPath:function(Self, FilePath$11) {
      var Result = false;
      var Separator$6 = "",
         PathParts = [],
         Index = 0,
         a$240 = 0;
      var part = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if ((FilePath$11.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \" \" in path \"%s\" error",[FilePath$11]);
      } else {
         if (FilePath$11.length>0) {
            Separator$6 = TW3DirectoryParser.GetPathSeparator$(Self);
            PathParts = (FilePath$11).split(Separator$6);
            Index = 0;
            var $temp15;
            for(a$240=0,$temp15=PathParts.length;a$240<$temp15;a$240++) {
               part = PathParts[a$240];
               {var $temp16 = part;
                  if ($temp16=="") {
                     TW3ErrorObject.SetLastErrorF$1(Self,"Path has multiple separators (%s) error",[FilePath$11]);
                     return false;
                  }
                   else if ($temp16=="~") {
                     if (Index>0) {
                        TW3ErrorObject.SetLastErrorF$1(Self,"Path has misplaced root moniker (%s) error",[FilePath$11]);
                        return false;
                     }
                  }
                   else {
                     if (Index==(PathParts.length-1)) {
                        if (!TW3DirectoryParser.HasValidFileNameChars$(Self,part)) {
                           return false;
                        }
                     } else if (!TW3DirectoryParser.HasValidPathChars$(Self,part)) {
                        return false;
                     }
                  }
               }
               Index+=1;
            }
            Result = true;
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$38:TW3ErrorObject.Create$38
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3UnixDirectoryParser.$Intf={
   IW3DirectoryParser:[TW3UnixDirectoryParser.GetPathSeparator,TW3UnixDirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3UnixDirectoryParser.IsValidPath,TW3UnixDirectoryParser.HasValidPathChars,TW3UnixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3UnixDirectoryParser.GetFileNameWithoutExtension,TW3UnixDirectoryParser.GetPathName,TW3UnixDirectoryParser.GetDevice,TW3UnixDirectoryParser.GetFileName,TW3UnixDirectoryParser.GetExtension,TW3UnixDirectoryParser.GetDirectoryName,TW3UnixDirectoryParser.IncludeTrailingPathDelimiter,TW3UnixDirectoryParser.IncludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter,TW3UnixDirectoryParser.ChangeFileExt]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TPath = class (TObject)
///  [line: 107, column: 3, file: System.IOUtils]
var TPath = {
   $ClassName:"TPath",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TPath.GetDevice(const FilePath: String) : String
   ///  [line: 268, column: 22, file: System.IOUtils]
   ,GetDevice$2:function(FilePath$12) {
      var Result = "";
      var Access$4 = null,
         Error$1 = null;
      Access$4 = GetDirectoryParser();
      Error$1 = Access$4[2]();
      Result = Access$4[10](FilePath$12);
      if (Error$1[0]()) {
         throw Exception.Create($New(EPathError),Error$1[1]());
      }
      return Result
   }
   /// function TPath.IsPathRooted(const FilePath: String) : Boolean
   ///  [line: 257, column: 22, file: System.IOUtils]
   ,IsPathRooted$1:function(FilePath$13) {
      var Result = false;
      var Access$5 = null,
         Error$2 = null;
      Access$5 = GetDirectoryParser();
      Error$2 = Access$5[2]();
      Result = Access$5[7](FilePath$13);
      if (Error$2[0]()) {
         throw Exception.Create($New(EPathError),Error$2[1]());
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
function InstallDirectoryParser(NewParser) {
   if (__Parser!==null) {
      TObject.Free(__Parser);
      __Parser = null;
   }
   __Parser = NewParser;
};
function GetDirectoryParser() {
   var Result = null;
   if (__Parser===null) {
      if (GetIsRunningInBrowser()) {
         __Parser = TW3ErrorObject.Create$38$($New(TW3UnixDirectoryParser));
      }
   }
   if (__Parser!==null) {
      Result = $AsIntf(__Parser,"IW3DirectoryParser");
   } else {
      Result = null;
   }
   return Result
};
/// EPathError = class (EW3Exception)
///  [line: 105, column: 3, file: System.IOUtils]
var EPathError = {
   $ClassName:"EPathError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3StorageObjectType enumeration
///  [line: 37, column: 3, file: System.Device.Storage]
var TW3StorageObjectType = [ "otUnknown", "otFile", "otFolder", "otBlockDevice", "otCharacterDevice", "otSymbolicLink", "otFIFO", "otSocket" ];
/// TW3Component = class (TW3OwnedLockedErrorObject)
///  [line: 19, column: 3, file: system.widget]
var TW3Component = {
   $ClassName:"TW3Component",$Parent:TW3OwnedLockedErrorObject
   ,$Init:function ($) {
      TW3OwnedLockedErrorObject.$Init($);
      $.FInitialized = false;
   }
   /// constructor TW3Component.Create(AOwner: TW3Component)
   ///  [line: 73, column: 26, file: system.widget]
   ,Create$43:function(Self, AOwner$3) {
      TW3OwnedErrorObject.Create$18(Self,AOwner$3);
      Self.FInitialized = true;
      TW3Component.InitializeObject$(Self);
      return Self
   }
   /// constructor TW3Component.CreateEx(AOwner: TObject)
   ///  [line: 88, column: 26, file: system.widget]
   ,CreateEx:function(Self, AOwner$4) {
      TW3OwnedErrorObject.Create$18(Self,AOwner$4);
      Self.FInitialized = false;
      return Self
   }
   /// destructor TW3Component.Destroy()
   ///  [line: 99, column: 25, file: system.widget]
   ,Destroy:function(Self) {
      if (Self.FInitialized) {
         TW3Component.FinalizeObject$(Self);
      }
      TW3OwnedErrorObject.Destroy(Self);
   }
   /// procedure TW3Component.FinalizeObject()
   ///  [line: 129, column: 24, file: system.widget]
   ,FinalizeObject:function(Self) {
      /* null */
   }
   /// procedure TW3Component.InitializeObject()
   ///  [line: 119, column: 24, file: system.widget]
   ,InitializeObject:function(Self) {
      /* null */
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$18:TW3OwnedErrorObject.Create$18
   ,Create$43$:function($){return $.ClassType.Create$43.apply($.ClassType, arguments)}
   ,FinalizeObject$:function($){return $.ClassType.FinalizeObject($)}
   ,InitializeObject$:function($){return $.ClassType.InitializeObject($)}
};
TW3Component.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3StorageDevice = class (TW3Component)
///  [line: 120, column: 3, file: System.Device.Storage]
var TW3StorageDevice = {
   $ClassName:"TW3StorageDevice",$Parent:TW3Component
   ,$Init:function ($) {
      TW3Component.$Init($);
      $.FActive = false;
      $.FIdentifier = $.FName = "";
      $.FOptions$6 = [0];
   }
   /// procedure TW3StorageDevice.FinalizeObject()
   ///  [line: 186, column: 28, file: System.Device.Storage]
   ,FinalizeObject:function(Self) {
      if (Self.FActive) {
         TW3StorageDevice.UnMount$(Self,null);
      }
      TW3Component.FinalizeObject(Self);
   }
   /// function TW3StorageDevice.GetActive() : Boolean
   ///  [line: 278, column: 27, file: System.Device.Storage]
   ,GetActive:function(Self) {
      return Self.FActive;
   }
   /// procedure TW3StorageDevice.GetDeviceId(CB: TW3DeviceIdCallback)
   ///  [line: 236, column: 28, file: System.Device.Storage]
   ,GetDeviceId:function(Self, CB) {
      if (CB) {
         CB(Self.FIdentifier,true);
      }
   }
   /// procedure TW3StorageDevice.GetDeviceOptions(CB: TW3DeviceOptionsCallback)
   ///  [line: 193, column: 28, file: System.Device.Storage]
   ,GetDeviceOptions:function(Self, CB$1) {
      if (CB$1) {
         CB$1(Self.FOptions$6.slice(0),true);
      }
   }
   /// procedure TW3StorageDevice.GetName(CB: TW3DeviceNameCallback)
   ///  [line: 230, column: 28, file: System.Device.Storage]
   ,GetName:function(Self, CB$2) {
      if (CB$2) {
         CB$2(Self.FName,true);
      }
   }
   /// procedure TW3StorageDevice.InitializeObject()
   ///  [line: 178, column: 28, file: System.Device.Storage]
   ,InitializeObject:function(Self) {
      TW3Component.InitializeObject(Self);
      Self.FOptions$6 = [2];
      Self.FIdentifier = TString.CreateGUID(TString);
      Self.FName = "dh0";
   }
   /// procedure TW3StorageDevice.SetActive(const NewActiveState: Boolean)
   ///  [line: 283, column: 28, file: System.Device.Storage]
   ,SetActive:function(Self, NewActiveState) {
      Self.FActive = NewActiveState;
   }
   ,Destroy:TW3Component.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$18:TW3OwnedErrorObject.Create$18
   ,Create$43:TW3Component.Create$43
   ,FinalizeObject$:function($){return $.ClassType.FinalizeObject($)}
   ,InitializeObject$:function($){return $.ClassType.InitializeObject($)}
   ,CdUp$:function($){return $.ClassType.CdUp.apply($.ClassType, arguments)}
   ,ChDir$:function($){return $.ClassType.ChDir.apply($.ClassType, arguments)}
   ,DirExists$:function($){return $.ClassType.DirExists.apply($.ClassType, arguments)}
   ,Examine$:function($){return $.ClassType.Examine.apply($.ClassType, arguments)}
   ,FileExists$:function($){return $.ClassType.FileExists.apply($.ClassType, arguments)}
   ,GetFileSize$:function($){return $.ClassType.GetFileSize.apply($.ClassType, arguments)}
   ,GetPath$:function($){return $.ClassType.GetPath.apply($.ClassType, arguments)}
   ,GetStorageObjectType$:function($){return $.ClassType.GetStorageObjectType.apply($.ClassType, arguments)}
   ,Load$:function($){return $.ClassType.Load.apply($.ClassType, arguments)}
   ,MakeDir$:function($){return $.ClassType.MakeDir.apply($.ClassType, arguments)}
   ,Mount$:function($){return $.ClassType.Mount.apply($.ClassType, arguments)}
   ,RemoveDir$:function($){return $.ClassType.RemoveDir.apply($.ClassType, arguments)}
   ,Save$:function($){return $.ClassType.Save.apply($.ClassType, arguments)}
   ,UnMount$:function($){return $.ClassType.UnMount.apply($.ClassType, arguments)}
};
TW3StorageDevice.$Intf={
   IW3StorageDevice:[TW3StorageDevice.GetActive,TW3StorageDevice.GetName,TW3StorageDevice.GetDeviceOptions,TW3StorageDevice.GetDeviceId,TW3StorageDevice.GetPath,TW3StorageDevice.GetFileSize,TW3StorageDevice.FileExists,TW3StorageDevice.DirExists,TW3StorageDevice.MakeDir,TW3StorageDevice.RemoveDir,TW3StorageDevice.Examine,TW3StorageDevice.ChDir,TW3StorageDevice.CdUp,TW3StorageDevice.GetStorageObjectType,TW3StorageDevice.Load,TW3StorageDevice.Save]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3DeviceAuthenticationData = class (TObject)
///  [line: 49, column: 3, file: System.Device.Storage]
var TW3DeviceAuthenticationData = {
   $ClassName:"TW3DeviceAuthenticationData",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TBinaryData = class (TAllocation)
///  [line: 158, column: 3, file: System.Memory.Buffer]
var TBinaryData = {
   $ClassName:"TBinaryData",$Parent:TAllocation
   ,$Init:function ($) {
      TAllocation.$Init($);
      $.FDataView = null;
   }
   /// procedure TBinaryData.AppendBuffer(const Raw: TMemoryHandle)
   ///  [line: 1185, column: 23, file: System.Memory.Buffer]
   ,AppendBuffer:function(Self, Raw) {
      var LOffset = 0;
      if (Raw) {
         if (Raw.length>0) {
            LOffset = TAllocation.GetSize$3(Self);
            TAllocation.Grow(Self,Raw.length);
            TBinaryData.WriteBuffer$2(Self,LOffset,Raw);
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Append failed, invalid source handle error");
      }
   }
   /// procedure TBinaryData.AppendBytes(const Bytes: TByteArray)
   ///  [line: 1241, column: 23, file: System.Memory.Buffer]
   ,AppendBytes:function(Self, Bytes$5) {
      var LLen$7 = 0,
         LOffset$1 = 0;
      LLen$7 = Bytes$5.length;
      if (LLen$7>0) {
         LOffset$1 = TAllocation.GetSize$3(Self);
         TAllocation.Grow(Self,LLen$7);
         TAllocation.GetHandle(Self).set(Bytes$5,LOffset$1);
      }
   }
   /// procedure TBinaryData.AppendFloat32(const Value: float32)
   ///  [line: 1171, column: 23, file: System.Memory.Buffer]
   ,AppendFloat32:function(Self, Value$29) {
      var LOffset$2 = 0;
      LOffset$2 = TAllocation.GetSize$3(Self);
      TAllocation.Grow(Self,TDataTypeConverter.SizeOfType(TDataTypeConverter,8));
      TBinaryData.WriteFloat32(Self,LOffset$2,Value$29);
   }
   /// procedure TBinaryData.AppendFloat64(const Value: float64)
   ///  [line: 1178, column: 23, file: System.Memory.Buffer]
   ,AppendFloat64:function(Self, Value$30) {
      var LOffset$3 = 0;
      LOffset$3 = TAllocation.GetSize$3(Self);
      TAllocation.Grow(Self,TDataTypeConverter.SizeOfType(TDataTypeConverter,9));
      TBinaryData.WriteFloat64(Self,LOffset$3,Value$30);
   }
   /// procedure TBinaryData.AppendMemory(const Buffer: TBinaryData; const ReleaseBufferOnExit: Boolean)
   ///  [line: 1199, column: 23, file: System.Memory.Buffer]
   ,AppendMemory:function(Self, Buffer$4, ReleaseBufferOnExit) {
      var LOffset$4 = 0;
      if (Buffer$4!==null) {
         try {
            if (TAllocation.GetSize$3(Buffer$4)>0) {
               LOffset$4 = TAllocation.GetSize$3(Self);
               TAllocation.Grow(Self,TAllocation.GetSize$3(Buffer$4));
               TBinaryData.WriteBinaryData(Self,LOffset$4,Buffer$4);
            }
         } finally {
            if (ReleaseBufferOnExit) {
               TObject.Free(Buffer$4);
            }
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Append failed, Invalid source buffer error");
      }
   }
   /// procedure TBinaryData.AppendStr(const Text: String)
   ///  [line: 1219, column: 23, file: System.Memory.Buffer]
   ,AppendStr:function(Self, Text$3) {
      var LLen$8 = 0,
         LOffset$5 = 0,
         LTemp$5 = [],
         x$10 = 0;
      LLen$8 = Text$3.length;
      if (LLen$8>0) {
         LOffset$5 = TAllocation.GetSize$3(Self);
         LTemp$5 = TString.EncodeUTF8(TString,Text$3);
         TAllocation.Grow(Self,LTemp$5.length);
         var $temp17;
         for(x$10=0,$temp17=LTemp$5.length;x$10<$temp17;x$10++) {
            Self.FDataView.setInt8(LOffset$5,LTemp$5[x$10]);
            ++LOffset$5;
         }
      }
   }
   /// function TBinaryData.Clone() : TBinaryData
   ///  [line: 1144, column: 22, file: System.Memory.Buffer]
   ,Clone:function(Self) {
      return TBinaryData.Create$52($New(TBinaryData),TBinaryData.ToTypedArray(Self));
   }
   /// function TBinaryData.Copy(const Offset: Integer; const ByteLen: Integer) : TByteArray
   ///  [line: 434, column: 22, file: System.Memory.Buffer]
   ,Copy$1:function(Self, Offset$13, ByteLen) {
      var Result = [];
      var LSize$4 = 0,
         LTemp$6 = null;
      LSize$4 = TAllocation.GetSize$3(Self);
      if (LSize$4>0) {
         if (Offset$13>=0&&Offset$13<LSize$4) {
            if (Offset$13+ByteLen<=LSize$4) {
               LTemp$6 = new Uint8Array(Self.FDataView.buffer,Offset$13,ByteLen);
               Result = Array.prototype.slice.call(LTemp$6);
               LTemp$6.buffer = null;
               LTemp$6 = null;
            }
         }
      }
      return Result
   }
   /// procedure TBinaryData.CopyFrom(const Buffer: TBinaryData; const Offset: Integer; const ByteLen: Integer)
   ///  [line: 1149, column: 23, file: System.Memory.Buffer]
   ,CopyFrom$2:function(Self, Buffer$5, Offset$14, ByteLen$1) {
      if (Buffer$5!==null) {
         TBinaryData.CopyFromMemory(Self,TAllocation.GetHandle(Buffer$5),Offset$14,ByteLen$1);
      } else {
         throw Exception.Create($New(EBinaryData),"CopyFrom failed, source instance was NIL error");
      }
   }
   /// procedure TBinaryData.CopyFromMemory(const Raw: TMemoryHandle; Offset: Integer; ByteLen: Integer)
   ///  [line: 1157, column: 23, file: System.Memory.Buffer]
   ,CopyFromMemory:function(Self, Raw$1, Offset$15, ByteLen$2) {
      if (TMemoryHandleHelper$Valid$3(Raw$1)) {
         if (TBinaryData.OffsetInRange(Self,Offset$15)) {
            if (ByteLen$2>0) {
               TMarshal.Move$1(TMarshal,Raw$1,0,TAllocation.GetHandle(Self),Offset$15,ByteLen$2);
            }
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"Cut memory failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$15]);
         }
      } else {
         throw Exception.Create($New(EBinaryData),"CopyFrom failed, invalid source handle error");
      }
   }
   /// constructor TBinaryData.Create(aHandle: TMemoryHandle)
   ///  [line: 283, column: 25, file: System.Memory.Buffer]
   ,Create$52:function(Self, aHandle) {
      var LSignature$1;
      TDataTypeConverter.Create$4$(Self);
      if (TMemoryHandleHelper$Defined$1(aHandle)&&TMemoryHandleHelper$Valid$3(aHandle)) {
         if (aHandle.toString) {
            LSignature$1 = aHandle.toString();
            if (SameText(String(LSignature$1),"[object Uint8Array]")||SameText(String(LSignature$1),"[object Uint8ClampedArray]")) {
               TAllocation.Allocate(Self,parseInt(aHandle.length,10));
               TMarshal.Move$1(TMarshal,aHandle,0,TAllocation.GetHandle(Self),0,parseInt(aHandle.length,10));
            } else {
               throw Exception.Create($New(EBinaryData),"Invalid buffer type, expected handle of type Uint8[clamped]Array");
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Invalid buffer type, expected handle of type Uint8[clamped]Array");
         }
      }
      return Self
   }
   /// function TBinaryData.CutBinaryData(const Offset: Integer; const ByteLen: Integer) : TBinaryData
   ///  [line: 1123, column: 22, file: System.Memory.Buffer]
   ,CutBinaryData:function(Self, Offset$16, ByteLen$3) {
      var Result = null;
      var LSize$5 = 0,
         LNewBuffer = null;
      if (ByteLen$3>0) {
         LSize$5 = TAllocation.GetSize$3(Self);
         if (LSize$5>0) {
            if (TBinaryData.OffsetInRange(Self,Offset$16)) {
               LNewBuffer = TAllocation.GetHandle(Self).subarray(Offset$16,Offset$16+ByteLen$3-1);
               Result = TBinaryData.Create$52($New(TBinaryData),LNewBuffer);
            } else {
               throw EW3Exception.CreateFmt($New(EBinaryData),"Cut memory failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$16]);
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Cut memory failed, buffer is empty error");
         }
      } else {
         Result = TBinaryData.Create$52($New(TBinaryData),null);
      }
      return Result
   }
   /// function TBinaryData.CutStream(const Offset: Integer; const ByteLen: Integer) : TStream
   ///  [line: 1101, column: 22, file: System.Memory.Buffer]
   ,CutStream:function(Self, Offset$17, ByteLen$4) {
      return TBinaryData.ToStream(TBinaryData.CutBinaryData(Self,Offset$17,ByteLen$4));
   }
   /// function TBinaryData.CutTypedArray(Offset: Integer; ByteLen: Integer) : TMemoryHandle
   ///  [line: 1106, column: 22, file: System.Memory.Buffer]
   ,CutTypedArray:function(Self, Offset$18, ByteLen$5) {
      var Result = undefined;
      var LTemp$7 = null;
      if (ByteLen$5>0) {
         if (TBinaryData.OffsetInRange(Self,Offset$18)) {
            if (TAllocation.GetSize$3(Self)-Offset$18>0) {
               LTemp$7 = Self.FDataView.buffer.slice(Offset$18,Offset$18+ByteLen$5);
               Result = new Uint8Array(LTemp$7);
            }
         }
      }
      return Result
   }
   /// procedure TBinaryData.FromBase64(FileData: String)
   ///  [line: 593, column: 23, file: System.Memory.Buffer]
   ,FromBase64:function(Self, FileData) {
      var LRaw = "",
         x$11 = 0;
      TAllocation.Release$1(Self);
      if (FileData.length>0) {
         LRaw = TBase64EncDec.Base64ToString(TBase64EncDec,FileData);
         if (LRaw.length>0) {
            TAllocation.Allocate(Self,LRaw.length);
            var $temp18;
            for(x$11=0,$temp18=LRaw.length;x$11<$temp18;x$11++) {
               TBinaryData.SetByte(Self,x$11,TDataTypeConverter.CharToByte(TDataTypeConverter,LRaw.charAt(x$11-1)));
            }
         }
      }
   }
   /// procedure TBinaryData.FromNodeBuffer(const NodeBuffer: JNodeBuffer)
   ///  [line: 93, column: 23, file: SmartNJ.Streams]
   ,FromNodeBuffer:function(Self, NodeBuffer) {
      var LTypedAccess = undefined;
      if (!TAllocation.a$26(Self)) {
         TAllocation.Release$1(Self);
      }
      if (NodeBuffer!==null) {
         LTypedAccess = new Uint8Array(NodeBuffer);
         TBinaryData.AppendBuffer(Self,LTypedAccess);
      }
   }
   /// function TBinaryData.GetBit(const BitIndex: Integer) : Boolean
   ///  [line: 357, column: 22, file: System.Memory.Buffer]
   ,GetBit$1:function(Self, BitIndex) {
      var Result = false;
      var LOffset$6 = 0;
      LOffset$6 = BitIndex>>>3;
      if (TBinaryData.OffsetInRange(Self,LOffset$6)) {
         Result = TBitAccess.Get(TBitAccess,(BitIndex%8),TBinaryData.GetByte(Self,LOffset$6));
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, BitIndex]);
      }
      return Result
   }
   /// function TBinaryData.GetBitCount() : Integer
   ///  [line: 317, column: 22, file: System.Memory.Buffer]
   ,GetBitCount:function(Self) {
      return TAllocation.GetSize$3(Self)<<3;
   }
   /// function TBinaryData.GetByte(const Index: Integer) : Byte
   ///  [line: 661, column: 22, file: System.Memory.Buffer]
   ,GetByte:function(Self, Index$1) {
      var Result = 0;
      if (TAllocation.GetHandle(Self)) {
         if (TBinaryData.OffsetInRange(Self,Index$1)) {
            Result = Self.FDataView.getUint8(Index$1);
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, Index$1]);
         }
      }
      return Result
   }
   /// procedure TBinaryData.HandleAllocated()
   ///  [line: 309, column: 23, file: System.Memory.Buffer]
   ,HandleAllocated:function(Self) {
      var LRef$2 = undefined;
      LRef$2 = TAllocation.GetBufferHandle(Self);
      (Self.FDataView) = new DataView(LRef$2);
   }
   /// procedure TBinaryData.HandleReleased()
   ///  [line: 322, column: 23, file: System.Memory.Buffer]
   ,HandleReleased:function(Self) {
      Self.FDataView = null;
   }
   /// procedure TBinaryData.LoadFromStream(const Stream: TStream)
   ///  [line: 515, column: 23, file: System.Memory.Buffer]
   ,LoadFromStream$1:function(Self, Stream$4) {
      var BytesToRead$2 = 0;
      TAllocation.Release$1(Self);
      if (Stream$4!==null) {
         if (TStream.GetSize$1$(Stream$4)>0) {
            BytesToRead$2 = TStream.GetSize$1$(Stream$4)-TStream.GetPosition$(Stream$4);
            if (BytesToRead$2>0) {
               TBinaryData.AppendBytes(Self,TStream.ReadBuffer$(Stream$4,TStream.GetPosition$(Stream$4),BytesToRead$2));
            }
         }
      } else {
         throw Exception.Create($New(EBinaryData),$R[29]);
      }
   }
   /// function TBinaryData.OffsetInRange(const Offset: Integer) : Boolean
   ///  [line: 834, column: 22, file: System.Memory.Buffer]
   ,OffsetInRange:function(Self, Offset$19) {
      var Result = false;
      var LSize$6 = 0;
      LSize$6 = TAllocation.GetSize$3(Self);
      if (LSize$6>0) {
         Result = Offset$19>=0&&Offset$19<=LSize$6;
      } else {
         Result = (Offset$19==0);
      }
      return Result
   }
   /// function TBinaryData.ReadBool(Offset: Integer) : Boolean
   ///  [line: 826, column: 22, file: System.Memory.Buffer]
   ,ReadBool:function(Self, Offset$20) {
      var Result = false;
      if (TBinaryData.OffsetInRange(Self,Offset$20)) {
         Result = Self.FDataView.getUint8(Offset$20)>0;
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[27],[Offset$20, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadBytes(Offset: Integer; ByteLen: Integer) : TByteArray
   ///  [line: 809, column: 22, file: System.Memory.Buffer]
   ,ReadBytes:function(Self, Offset$21, ByteLen$6) {
      var Result = [];
      var LSize$7 = 0,
         x$12 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$21)) {
         LSize$7 = TAllocation.GetSize$3(Self);
         if (Offset$21+ByteLen$6<=LSize$7) {
            var $temp19;
            for(x$12=0,$temp19=ByteLen$6;x$12<$temp19;x$12++) {
               Result.push(Self.FDataView.getUint8(Offset$21+x$12));
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[27],[Offset$21, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadFloat32(Offset: Integer) : float32
   ///  [line: 703, column: 22, file: System.Memory.Buffer]
   ,ReadFloat32:function(Self, Offset$22) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$22)) {
         if (Offset$22+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getFloat32(Offset$22);
                  break;
               case 1 :
                  Result = Self.FDataView.getFloat32(Offset$22,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getFloat32(Offset$22,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[27],[Offset$22, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadFloat64(Offset: Integer) : float64
   ///  [line: 685, column: 22, file: System.Memory.Buffer]
   ,ReadFloat64:function(Self, Offset$23) {
      var Result = undefined;
      if (TBinaryData.OffsetInRange(Self,Offset$23)) {
         if (Offset$23+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getFloat64(Offset$23);
                  break;
               case 1 :
                  Result = Self.FDataView.getFloat64(Offset$23,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getFloat64(Offset$23,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[27],[Offset$23, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadInt(Offset: Integer) : Integer
   ///  [line: 771, column: 22, file: System.Memory.Buffer]
   ,ReadInt:function(Self, Offset$24) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$24)) {
         if (Offset$24+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getInt32(Offset$24);
                  break;
               case 1 :
                  Result = Self.FDataView.getInt32(Offset$24,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getInt32(Offset$24,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[27],[Offset$24, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadInt16(Offset: Integer) : SmallInt
   ///  [line: 720, column: 22, file: System.Memory.Buffer]
   ,ReadInt16:function(Self, Offset$25) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$25)) {
         if (Offset$25+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getInt16(Offset$25);
                  break;
               case 1 :
                  Result = Self.FDataView.getInt16(Offset$25,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getInt16(Offset$25,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[27],[Offset$25, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadLong(Offset: Integer) : Longword
   ///  [line: 754, column: 22, file: System.Memory.Buffer]
   ,ReadLong$1:function(Self, Offset$26) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$26)) {
         if (Offset$26+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getUint32(Offset$26);
                  break;
               case 1 :
                  Result = Self.FDataView.getUint32(Offset$26,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getUint32(Offset$26,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[27],[Offset$26, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadStr(Offset: Integer; ByteLen: Integer) : String
   ///  [line: 788, column: 22, file: System.Memory.Buffer]
   ,ReadStr$1:function(Self, Offset$27, ByteLen$7) {
      var Result = "";
      var LSize$8 = 0,
         LFetch = [],
         x$13 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$27)) {
         LSize$8 = TAllocation.GetSize$3(Self);
         if (Offset$27+ByteLen$7<=LSize$8) {
            var $temp20;
            for(x$13=0,$temp20=ByteLen$7;x$13<$temp20;x$13++) {
               LFetch.push(TBinaryData.GetByte(Self,(Offset$27+x$13)));
            }
            Result = TString.DecodeUTF8(TString,LFetch);
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[27],[Offset$27, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadWord(Offset: Integer) : Word
   ///  [line: 737, column: 22, file: System.Memory.Buffer]
   ,ReadWord$1:function(Self, Offset$28) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$28)) {
         if (Offset$28+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)<=TAllocation.GetSize$3(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getUint16(Offset$28);
                  break;
               case 1 :
                  Result = Self.FDataView.getUint16(Offset$28,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getUint16(Offset$28,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),$R[27],[Offset$28, 0, TAllocation.GetSize$3(Self)-1]);
      }
      return Result
   }
   /// procedure TBinaryData.SetBit(const BitIndex: Integer; const value: Boolean)
   ///  [line: 352, column: 23, file: System.Memory.Buffer]
   ,SetBit$1:function(Self, BitIndex$1, value) {
      TBinaryData.SetByte(Self,(BitIndex$1>>>3),TBitAccess.Set$4(TBitAccess,(BitIndex$1%8),TBinaryData.GetByte(Self,(BitIndex$1>>>3)),value));
   }
   /// procedure TBinaryData.SetByte(const Index: Integer; const Value: Byte)
   ///  [line: 673, column: 23, file: System.Memory.Buffer]
   ,SetByte:function(Self, Index$2, Value$31) {
      if (TAllocation.GetHandle(Self)) {
         if (TBinaryData.OffsetInRange(Self,Index$2)) {
            Self.FDataView.setUint8(Index$2,Value$31);
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"Invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, Index$2]);
         }
      }
   }
   /// function TBinaryData.ToBase64() : String
   ///  [line: 619, column: 22, file: System.Memory.Buffer]
   ,ToBase64:function(Self) {
      var Result = "";
      var mText = "";
      var mRef = undefined;
      var CHUNK_SIZE = 32768;
      var index$1 = 0;
      var mLength = 0;
      var slice$2;
      if (TAllocation.GetHandle(Self)) {
         mRef = TAllocation.GetHandle(Self);
         mLength = (mRef).length;
      while (index$1 < mLength)
      {
        slice$2 = (mRef).subarray(index$1, Math.min(index$1 + CHUNK_SIZE, mLength));
        mText += String.fromCharCode.apply(null, slice$2);
        index$1 += CHUNK_SIZE;
      }
      Result = btoa(mText);
      }
      return Result
   }
   /// function TBinaryData.ToBytes() : TByteArray
   ///  [line: 464, column: 22, file: System.Memory.Buffer]
   ,ToBytes:function(Self) {
      var Result = [];
      var LSize$9 = 0,
         LTemp$8 = null;
      LSize$9 = TAllocation.GetSize$3(Self);
      if (LSize$9>0) {
         LTemp$8 = new Uint8Array(Self.FDataView.buffer,0,LSize$9);
         Result = Array.prototype.slice.call(LTemp$8);
         LTemp$8.buffer = null;
         LTemp$8 = null;
      } else {
         Result = [];
      }
      return Result
   }
   /// function TBinaryData.ToHexDump(BytesPerRow: Integer; Options: TBufferHexDumpOptions) : String
   ///  [line: 367, column: 22, file: System.Memory.Buffer]
   ,ToHexDump:function(Self, BytesPerRow, Options$4) {
      var Result = "";
      var mDump = [],
         mCount = 0,
         x$14 = 0;
      var y = 0;
      var mPad = 0,
         x$15 = 0;
      if (TAllocation.GetHandle(Self)) {
         mCount = 0;
         BytesPerRow = TInteger.EnsureRange(BytesPerRow,2,64);
         var $temp21;
         for(x$14=0,$temp21=TAllocation.GetSize$3(Self);x$14<$temp21;x$14++) {
            mDump.push(TBinaryData.GetByte(Self,x$14));
            if ($SetIn(Options$4,0,0,2)) {
               Result+="$"+(IntToHex2(TBinaryData.GetByte(Self,x$14))).toLocaleUpperCase();
            } else {
               Result+=(IntToHex2(TBinaryData.GetByte(Self,x$14))).toLocaleUpperCase();
            }
            ++mCount;
            if (mCount>=BytesPerRow) {
               if (mDump.length>0) {
                  Result+=" ";
                  var $temp22;
                  for(y=0,$temp22=mDump.length;y<$temp22;y++) {
                     if (function(v$){return (((v$>="A")&&(v$<="Z"))||((v$>="a")&&(v$<="z"))||((v$>="0")&&(v$<="9"))||(v$==",")||(v$==";")||(v$=="<")||(v$==">")||(v$=="{")||(v$=="}")||(v$=="[")||(v$=="]")||(v$=="-")||(v$=="_")||(v$=="#")||(v$=="$")||(v$=="%")||(v$=="&")||(v$=="\/")||(v$=="(")||(v$==")")||(v$=="!")||(v$=="§")||(v$=="^")||(v$==":")||(v$==",")||(v$=="?"))}(TDataTypeConverter.ByteToChar(TDataTypeConverter,mDump[y]))) {
                        Result+=TDataTypeConverter.ByteToChar(TDataTypeConverter,mDump[y]);
                     } else {
                        Result+="_";
                     }
                  }
               }
               mDump.length=0;
               Result+="\r\n";
               mCount = 0;
            } else {
               Result+=" ";
            }
         }
         if ($SetIn(Options$4,1,0,2)&&mCount>0) {
            mPad = BytesPerRow-mCount;
            var $temp23;
            for(x$15=1,$temp23=mPad;x$15<=$temp23;x$15++) {
               if ($SetIn(Options$4,0,0,2)) {
                  Result+="$";
               }
               Result+="00";
               ++mCount;
               if (mCount>=BytesPerRow) {
                  Result+="\r\n";
                  mCount = 0;
               } else {
                  Result+=" ";
               }
            }
         }
      }
      return Result
   }
   /// function TBinaryData.ToNodeBuffer() : JNodeBuffer
   ///  [line: 83, column: 22, file: SmartNJ.Streams]
   ,ToNodeBuffer:function(Self) {
      var Result = null;
      if (TAllocation.a$26(Self)) {
         Result = new Buffer();
      } else {
         Result = new Buffer(TBinaryData.ToTypedArray(Self));
      }
      return Result
   }
   /// function TBinaryData.ToStream() : TStream
   ///  [line: 537, column: 22, file: System.Memory.Buffer]
   ,ToStream:function(Self) {
      var Result = null;
      var LSize$10 = 0,
         LCache = [],
         LTemp$9 = null;
      Result = TDataTypeConverter.Create$4$($New(TMemoryStream));
      LSize$10 = TAllocation.GetSize$3(Self);
      if (LSize$10>0) {
         LTemp$9 = new Uint8Array(Self.FDataView.buffer,0,LSize$10);
         LCache = Array.prototype.slice.call(LTemp$9);
         LTemp$9 = null;
         if (LCache.length>0) {
            try {
               TStream.Write$1(Result,LCache);
               TStream.SetPosition$(Result,0);
            } catch ($e) {
               var e$5 = $W($e);
               TObject.Free(Result);
               Result = null;
               throw $e;
            }
         }
      }
      return Result
   }
   /// function TBinaryData.ToString() : String
   ///  [line: 644, column: 22, file: System.Memory.Buffer]
   ,ToString$1:function(Self) {
      var Result = "";
      var CHUNK_SIZE$1 = 32768;
      var LRef$3 = undefined;
      if (TAllocation.GetHandle(Self)) {
         LRef$3 = TAllocation.GetHandle(Self);
         var c = [];
    for (var i=0; i < (LRef$3).length; i += CHUNK_SIZE$1) {
      c.push(String.fromCharCode.apply(null, (LRef$3).subarray(i, i + CHUNK_SIZE$1)));
    }
    Result = c.join("");
      }
      return Result
   }
   /// function TBinaryData.ToTypedArray() : TMemoryHandle
   ///  [line: 573, column: 22, file: System.Memory.Buffer]
   ,ToTypedArray:function(Self) {
      var Result = undefined;
      var LLen$9 = 0,
         LTemp$10 = null;
      LLen$9 = TAllocation.GetSize$3(Self);
      if (LLen$9>0) {
         LTemp$10 = Self.FDataView.buffer.slice(0,LLen$9);
         Result = new Uint8Array(LTemp$10);
      }
      return Result
   }
   /// procedure TBinaryData.WriteBinaryData(const Offset: Integer; const Data: TBinaryData)
   ///  [line: 866, column: 23, file: System.Memory.Buffer]
   ,WriteBinaryData:function(Self, Offset$29, Data$20) {
      var LGrowth = 0;
      if (Data$20!==null) {
         if (TAllocation.GetSize$3(Data$20)>0) {
            if (TBinaryData.OffsetInRange(Self,Offset$29)) {
               LGrowth = 0;
               if (Offset$29+TAllocation.GetSize$3(Data$20)>TAllocation.GetSize$3(Self)-1) {
                  LGrowth = Offset$29+TAllocation.GetSize$3(Data$20)-TAllocation.GetSize$3(Self);
               }
               if (LGrowth>0) {
                  TAllocation.Grow(Self,LGrowth);
               }
               TMarshal.Move$1(TMarshal,TAllocation.GetHandle(Data$20),0,TAllocation.GetHandle(Self),0,TAllocation.GetSize$3(Data$20));
            } else {
               throw EW3Exception.CreateFmt($New(EBinaryData),"Write string failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$29]);
            }
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Write failed, invalid source buffer [nil] error");
      }
   }
   /// procedure TBinaryData.WriteBool(const Offset: Integer; const Data: Boolean)
   ///  [line: 953, column: 23, file: System.Memory.Buffer]
   ,WriteBool:function(Self, Offset$30, Data$21) {
      var LGrowth$1 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$30)) {
         LGrowth$1 = 0;
         if (Offset$30+TDataTypeConverter.SizeOfType(TDataTypeConverter,2)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$1 = Offset$30+TDataTypeConverter.SizeOfType(TDataTypeConverter,2)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$1>0) {
            TAllocation.Grow(Self,LGrowth$1);
         }
         if (Data$21) {
            Self.FDataView.setUint8(Offset$30,1);
         } else {
            Self.FDataView.setUint8(Offset$30,0);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write boolean failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$30]);
      }
   }
   /// procedure TBinaryData.WriteBuffer(const Offset: Integer; const Data: TMemoryHandle)
   ///  [line: 890, column: 23, file: System.Memory.Buffer]
   ,WriteBuffer$2:function(Self, Offset$31, Data$22) {
      var LBuffer$1 = null,
         LGrowth$2 = 0;
      if (Data$22) {
         if (Data$22.buffer) {
            LBuffer$1 = Data$22.buffer;
            if (LBuffer$1.byteLength>0) {
               if (TBinaryData.OffsetInRange(Self,Offset$31)) {
                  LGrowth$2 = 0;
                  if (Offset$31+Data$22.length>TAllocation.GetSize$3(Self)-1) {
                     LGrowth$2 = Offset$31+Data$22.length-TAllocation.GetSize$3(Self);
                  }
                  if (LGrowth$2>0) {
                     TAllocation.Grow(Self,LGrowth$2);
                  }
                  TMarshal.Move$1(TMarshal,Data$22,0,TAllocation.GetHandle(Self),Offset$31,parseInt(TAllocation.GetHandle(Self).length,10));
               } else {
                  throw EW3Exception.CreateFmt($New(EBinaryData),"Write typed-handle failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$31]);
               }
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Write failed, invalid handle or unassigned buffer");
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Write failed, source handle was nil error");
      }
   }
   /// procedure TBinaryData.WriteBytes(const Offset: Integer; const Data: TByteArray)
   ///  [line: 847, column: 23, file: System.Memory.Buffer]
   ,WriteBytes:function(Self, Offset$32, Data$23) {
      var LGrowth$3 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$32)) {
         if (Data$23.length>0) {
            LGrowth$3 = 0;
            if (Offset$32+Data$23.length>TAllocation.GetSize$3(Self)-1) {
               LGrowth$3 = Offset$32+Data$23.length-TAllocation.GetSize$3(Self);
            }
            if (LGrowth$3>0) {
               TAllocation.Grow(Self,LGrowth$3);
            }
            TAllocation.GetHandle(Self).set(Data$23,Offset$32);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write bytearray failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$32]);
      }
   }
   /// procedure TBinaryData.WriteFloat32(const Offset: Integer; const Data: float32)
   ///  [line: 973, column: 23, file: System.Memory.Buffer]
   ,WriteFloat32:function(Self, Offset$33, Data$24) {
      var LGrowth$4 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$33)) {
         LGrowth$4 = 0;
         if (Offset$33+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$4 = Offset$33+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$4>0) {
            TAllocation.Grow(Self,LGrowth$4);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setFloat32(Offset$33,Data$24);
               break;
            case 1 :
               Self.FDataView.setFloat32(Offset$33,Data$24,true);
               break;
            case 2 :
               Self.FDataView.setFloat32(Offset$33,Data$24,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write float failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$33]);
      }
   }
   /// procedure TBinaryData.WriteFloat64(const Offset: Integer; const Data: float64)
   ///  [line: 996, column: 23, file: System.Memory.Buffer]
   ,WriteFloat64:function(Self, Offset$34, Data$25) {
      var LGrowth$5 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$34)) {
         LGrowth$5 = 0;
         if (Offset$34+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$5 = Offset$34+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$5>0) {
            TAllocation.Grow(Self,LGrowth$5);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setFloat64(Offset$34,Number(Data$25));
               break;
            case 1 :
               Self.FDataView.setFloat64(Offset$34,Number(Data$25),true);
               break;
            case 2 :
               Self.FDataView.setFloat64(Offset$34,Number(Data$25),false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write float failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$34]);
      }
   }
   /// procedure TBinaryData.WriteInt16(const Offset: Integer; const Data: SmallInt)
   ///  [line: 1017, column: 23, file: System.Memory.Buffer]
   ,WriteInt16:function(Self, Offset$35, Data$26) {
      var LGrowth$6 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$35)) {
         LGrowth$6 = 0;
         if (Offset$35+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$6 = Offset$35+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$6>0) {
            TAllocation.Grow(Self,LGrowth$6);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setInt16(Offset$35,Data$26);
               break;
            case 1 :
               Self.FDataView.setInt16(Offset$35,Data$26,true);
               break;
            case 2 :
               Self.FDataView.setInt16(Offset$35,Data$26,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write int16 failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$35]);
      }
   }
   /// procedure TBinaryData.WriteInt32(const Offset: Integer; const Data: Integer)
   ///  [line: 1080, column: 23, file: System.Memory.Buffer]
   ,WriteInt32:function(Self, Offset$36, Data$27) {
      var LGrowth$7 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$36)) {
         LGrowth$7 = 0;
         if (Offset$36+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$7 = Offset$36+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$7>0) {
            TAllocation.Grow(Self,LGrowth$7);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setInt32(Offset$36,Data$27);
               break;
            case 1 :
               Self.FDataView.setInt32(Offset$36,Data$27,true);
               break;
            case 2 :
               Self.FDataView.setInt32(Offset$36,Data$27,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write integer failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$36]);
      }
   }
   /// procedure TBinaryData.WriteLong(const Offset: Integer; const Data: Longword)
   ///  [line: 1059, column: 23, file: System.Memory.Buffer]
   ,WriteLong:function(Self, Offset$37, Data$28) {
      var LGrowth$8 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$37)) {
         LGrowth$8 = 0;
         if (Offset$37+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$8 = Offset$37+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$8>0) {
            TAllocation.Grow(Self,LGrowth$8);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setUint32(Offset$37,Data$28);
               break;
            case 1 :
               Self.FDataView.setUint32(Offset$37,Data$28,true);
               break;
            case 2 :
               Self.FDataView.setUint32(Offset$37,Data$28,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write longword failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$37]);
      }
   }
   /// procedure TBinaryData.WriteStr(const Offset: Integer; const Data: String)
   ///  [line: 927, column: 23, file: System.Memory.Buffer]
   ,WriteStr$1:function(Self, Offset$38, Data$29) {
      var LTemp$11 = [],
         LGrowth$9 = 0,
         x$16 = 0;
      if (Data$29.length>0) {
         if (TBinaryData.OffsetInRange(Self,Offset$38)) {
            LTemp$11 = TString.EncodeUTF8(TString,Data$29);
            LGrowth$9 = 0;
            if (Offset$38+LTemp$11.length>TAllocation.GetSize$3(Self)-1) {
               LGrowth$9 = Offset$38+LTemp$11.length-TAllocation.GetSize$3(Self);
            }
            if (LGrowth$9>0) {
               TAllocation.Grow(Self,LGrowth$9);
            }
            var $temp24;
            for(x$16=0,$temp24=LTemp$11.length;x$16<$temp24;x$16++) {
               Self.FDataView.setUint8(Offset$38+x$16,LTemp$11[x$16]);
            }
         } else {
            throw EW3Exception.CreateFmt($New(EBinaryData),"Write string failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$38]);
         }
      }
   }
   /// procedure TBinaryData.WriteWord(const Offset: Integer; const Data: Word)
   ///  [line: 1038, column: 23, file: System.Memory.Buffer]
   ,WriteWord$1:function(Self, Offset$39, Data$30) {
      var LGrowth$10 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$39)) {
         LGrowth$10 = 0;
         if (Offset$39+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)>TAllocation.GetSize$3(Self)-1) {
            LGrowth$10 = Offset$39+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)-TAllocation.GetSize$3(Self);
         }
         if (LGrowth$10>0) {
            TAllocation.Grow(Self,LGrowth$10);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setUint16(Offset$39,Data$30);
               break;
            case 1 :
               Self.FDataView.setUint16(Offset$39,Data$30,true);
               break;
            case 2 :
               Self.FDataView.setUint16(Offset$39,Data$30,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt($New(EBinaryData),"Write word [uint16] failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3(Self)-1, Offset$39]);
      }
   }
   ,Destroy:TAllocation.Destroy
   ,Create$4:TAllocation.Create$4
   ,HandleAllocated$:function($){return $.ClassType.HandleAllocated($)}
   ,HandleReleased$:function($){return $.ClassType.HandleReleased($)}
};
TBinaryData.$Intf={
   IBinaryDataWriteAccess:[TBinaryData.AppendBytes,TBinaryData.AppendStr,TBinaryData.AppendMemory,TBinaryData.AppendBuffer,TBinaryData.AppendFloat32,TBinaryData.AppendFloat64,TBinaryData.WriteFloat32,TBinaryData.WriteFloat64,TBinaryData.WriteStr$1,TBinaryData.WriteBytes,TBinaryData.WriteBinaryData,TBinaryData.WriteBuffer$2,TBinaryData.WriteInt32,TBinaryData.WriteLong,TBinaryData.WriteInt16,TBinaryData.WriteWord$1,TBinaryData.WriteBool,TBinaryData.Copy$1,TBinaryData.CopyFrom$2,TBinaryData.CopyFromMemory,TBinaryData.CutBinaryData,TBinaryData.CutStream,TBinaryData.CutTypedArray]
   ,IBinaryDataBitAccess:[TBinaryData.GetBitCount,TBinaryData.GetBit$1,TBinaryData.SetBit$1]
   ,IBinaryDataReadWriteAccess:[TBinaryData.ReadFloat32,TBinaryData.ReadFloat64,TBinaryData.ReadBool,TBinaryData.ReadInt,TBinaryData.ReadLong$1,TBinaryData.ReadInt16,TBinaryData.ReadWord$1,TBinaryData.ReadStr$1,TBinaryData.ReadBytes,TBinaryData.AppendBytes,TBinaryData.AppendStr,TBinaryData.AppendMemory,TBinaryData.AppendBuffer,TBinaryData.AppendFloat32,TBinaryData.AppendFloat64,TBinaryData.WriteFloat32,TBinaryData.WriteFloat64,TBinaryData.WriteStr$1,TBinaryData.WriteBytes,TBinaryData.WriteBinaryData,TBinaryData.WriteBuffer$2,TBinaryData.WriteInt32,TBinaryData.WriteLong,TBinaryData.WriteInt16,TBinaryData.WriteWord$1,TBinaryData.WriteBool,TBinaryData.Copy$1,TBinaryData.CopyFrom$2,TBinaryData.CopyFromMemory,TBinaryData.CutBinaryData,TBinaryData.CutStream,TBinaryData.CutTypedArray]
   ,IBinaryDataImport:[TBinaryData.FromBase64]
   ,IBinaryDataExport:[TBinaryData.Copy$1,TBinaryData.ToBase64,TBinaryData.ToString$1,TBinaryData.ToTypedArray,TBinaryData.ToBytes,TBinaryData.ToHexDump,TBinaryData.ToStream,TBinaryData.Clone]
   ,IBinaryDataReadAccess:[TBinaryData.Copy$1,TBinaryData.ReadFloat32,TBinaryData.ReadFloat64,TBinaryData.ReadBool,TBinaryData.ReadInt,TBinaryData.ReadLong$1,TBinaryData.ReadInt16,TBinaryData.ReadWord$1,TBinaryData.ReadStr$1,TBinaryData.ReadBytes]
   ,IAllocation:[TAllocation.GetHandle,TAllocation.GetTotalSize$1,TAllocation.GetSize$3,TAllocation.GetTransport,TAllocation.Allocate,TAllocation.Grow,TAllocation.Shrink,TAllocation.ReAllocate,TAllocation.Transport,TAllocation.Release$1]
   ,IBinaryTransport:[TAllocation.DataOffset$1,TAllocation.DataGetSize$1,TAllocation.DataRead$1,TAllocation.DataWrite$1]
}
/// EBinaryData = class (EW3Exception)
///  [line: 157, column: 3, file: System.Memory.Buffer]
var EBinaryData = {
   $ClassName:"EBinaryData",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TBitAccess = class (TObject)
///  [line: 20, column: 3, file: System.Types.Bits]
var TBitAccess = {
   $ClassName:"TBitAccess",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TBitAccess.Get(const index: Integer; const Value: Byte) : Boolean
   ///  [line: 114, column: 27, file: System.Types.Bits]
   ,Get:function(Self, index$2, Value$32) {
      var Result = false;
      var mMask = 0;
      if (index$2>=0&&index$2<8) {
         mMask = 1<<index$2;
         Result = ((Value$32&mMask)!=0);
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),"Invalid bit index, expected 0..7 not %d",[index$2]);
      }
      return Result
   }
   /// function TBitAccess.Set(const Index: Integer; const Value: Byte; const Data: Boolean) : Byte
   ///  [line: 127, column: 27, file: System.Types.Bits]
   ,Set$4:function(Self, Index$3, Value$33, Data$31) {
      var Result = 0;
      var mSet = false;
      var mMask$1 = 0;
      Result = Value$33;
      if (Index$3>=0&&Index$3<8) {
         mMask$1 = 1<<Index$3;
         mSet = ((Value$33&mMask$1)!=0);
         if (mSet!=Data$31) {
            if (Data$31) {
               Result = Result|mMask$1;
            } else {
               Result = (Result&(~mMask$1));
            }
         }
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
var CNT_BitBuffer_ByteTable = [0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8];
function NodeJSOsAPI() {
   return require("os");
};
function WriteLn(Text$4) {
   util().log(String(Text$4));
};
/// TW3Win32DirectoryParser = class (TW3UnixDirectoryParser)
///  [line: 62, column: 3, file: SmartNJ.System]
var TW3Win32DirectoryParser = {
   $ClassName:"TW3Win32DirectoryParser",$Parent:TW3UnixDirectoryParser
   ,$Init:function ($) {
      TW3UnixDirectoryParser.$Init($);
   }
   /// function TW3Win32DirectoryParser.GetPathSeparator() : Char
   ///  [line: 609, column: 34, file: SmartNJ.System]
   ,GetPathSeparator:function(Self) {
      return "\\";
   }
   /// function TW3Win32DirectoryParser.GetRootMoniker() : String
   ///  [line: 614, column: 34, file: SmartNJ.System]
   ,GetRootMoniker:function(Self) {
      var Result = "";
      function GetDriveFrom(ThisPath) {
         var Result = "";
         var xpos = 0;
         xpos = (ThisPath.indexOf(":\\")+1);
         if (xpos>=2) {
            ++xpos;
            Result = ThisPath.substr(0,xpos);
         }
         return Result
      };
      Result = (GetDriveFrom(ParamStr$1(1))).toLocaleLowerCase();
      if (Result.length<2) {
         Result = GetDriveFrom(ParamStr$1(0));
         if (Result.length<2) {
            throw Exception.Create($New(Exception),"Failed to extract root moniker from script path error");
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$38:TW3ErrorObject.Create$38
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,ChangeFileExt:TW3UnixDirectoryParser.ChangeFileExt
   ,ExcludeLeadingPathDelimiter:TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter
   ,ExcludeTrailingPathDelimiter:TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter
   ,GetDevice:TW3UnixDirectoryParser.GetDevice
   ,GetDirectoryName:TW3UnixDirectoryParser.GetDirectoryName
   ,GetExtension:TW3UnixDirectoryParser.GetExtension
   ,GetFileName:TW3UnixDirectoryParser.GetFileName
   ,GetFileNameWithoutExtension:TW3UnixDirectoryParser.GetFileNameWithoutExtension
   ,GetPathName:TW3UnixDirectoryParser.GetPathName
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars:TW3UnixDirectoryParser.HasValidFileNameChars
   ,HasValidPathChars:TW3UnixDirectoryParser.HasValidPathChars
   ,IncludeLeadingPathDelimiter:TW3UnixDirectoryParser.IncludeLeadingPathDelimiter
   ,IncludeTrailingPathDelimiter:TW3UnixDirectoryParser.IncludeTrailingPathDelimiter
   ,IsValidPath:TW3UnixDirectoryParser.IsValidPath
};
TW3Win32DirectoryParser.$Intf={
   IW3DirectoryParser:[TW3Win32DirectoryParser.GetPathSeparator,TW3Win32DirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3UnixDirectoryParser.IsValidPath,TW3UnixDirectoryParser.HasValidPathChars,TW3UnixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3UnixDirectoryParser.GetFileNameWithoutExtension,TW3UnixDirectoryParser.GetPathName,TW3UnixDirectoryParser.GetDevice,TW3UnixDirectoryParser.GetFileName,TW3UnixDirectoryParser.GetExtension,TW3UnixDirectoryParser.GetDirectoryName,TW3UnixDirectoryParser.IncludeTrailingPathDelimiter,TW3UnixDirectoryParser.IncludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter,TW3UnixDirectoryParser.ChangeFileExt]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TW3PosixDirectoryParser = class (TW3DirectoryParser)
///  [line: 68, column: 3, file: SmartNJ.System]
var TW3PosixDirectoryParser = {
   $ClassName:"TW3PosixDirectoryParser",$Parent:TW3DirectoryParser
   ,$Init:function ($) {
      TW3DirectoryParser.$Init($);
   }
   /// function TW3PosixDirectoryParser.ChangeFileExt(const FilePath: String; NewExt: String) : String
   ///  [line: 479, column: 34, file: SmartNJ.System]
   ,ChangeFileExt:function(Self, FilePath$14, NewExt$1) {
      var Result = "";
      var LName$1 = "";
      var Separator$7 = "",
         x$17 = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$14.length>0) {
         if ((FilePath$14.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FilePath$14]);
         } else {
            if (StrEndsWith(FilePath$14," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[FilePath$14]);
            } else {
               Separator$7 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(LName$1,Separator$7)) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Path (%s) has no filename error",[FilePath$14]);
               } else {
                  if ((FilePath$14.indexOf(Separator$7)+1)>0) {
                     LName$1 = TW3DirectoryParser.GetFileName$(Self,FilePath$14);
                     if (TW3ErrorObject.GetFailed$1(Self)) {
                        return Result;
                     }
                  } else {
                     LName$1 = FilePath$14;
                  }
                  if (LName$1.length>0) {
                     if ((LName$1.indexOf(".")+1)>0) {
                        for(x$17=LName$1.length;x$17>=1;x$17--) {
                           if (LName$1.charAt(x$17-1)==".") {
                              Result = LName$1.substr(0,(x$17-1))+NewExt$1;
                              break;
                           }
                        }
                     } else {
                        Result = LName$1+NewExt$1;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty filename (%s) error",[FilePath$14]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.ExcludeLeadingPathDelimiter(const FilePath: String) : String
   ///  [line: 596, column: 34, file: SmartNJ.System]
   ,ExcludeLeadingPathDelimiter:function(Self, FilePath$15) {
      var Result = "";
      var Separator$8 = "";
      Separator$8 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$15,Separator$8)) {
         Result = FilePath$15.substr((1+Separator$8.length)-1);
      } else {
         Result = FilePath$15;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.ExcludeTrailingPathDelimiter(const FilePath: String) : String
   ///  [line: 577, column: 34, file: SmartNJ.System]
   ,ExcludeTrailingPathDelimiter:function(Self, FilePath$16) {
      var Result = "";
      var Separator$9 = "";
      Separator$9 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrEndsWith(FilePath$16,Separator$9)) {
         Result = FilePath$16.substr(0,(FilePath$16.length-Separator$9.length));
      } else {
         Result = FilePath$16;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetDevice(const FilePath: String) : String
   ///  [line: 327, column: 34, file: SmartNJ.System]
   ,GetDevice:function(Self, FilePath$17) {
      var Result = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$17.length>0) {
         if (StrBeginsWith(FilePath$17,TW3DirectoryParser.GetRootMoniker$(Self))) {
            Result = TW3DirectoryParser.GetRootMoniker$(Self);
         } else {
            Result = "";
         }
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Failed to extract device, path was empty error");
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetDirectoryName(const FilePath: String) : String
   ///  [line: 534, column: 34, file: SmartNJ.System]
   ,GetDirectoryName:function(Self, FilePath$18) {
      var Result = "";
      var Separator$10 = "",
         NameParts$1 = [];
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$18.length>0) {
         if ((FilePath$18.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FilePath$18]);
         } else {
            if (StrEndsWith(FilePath$18," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[FilePath$18]);
            } else {
               Separator$10 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(FilePath$18,Separator$10)) {
                  Result = FilePath$18;
                  return Result;
               }
               NameParts$1 = (FilePath$18).split(Separator$10);
               NameParts$1.splice((NameParts$1.length-1),1)
               ;
               Result = (NameParts$1).join(Separator$10)+Separator$10;
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty filename (%s) error",[FilePath$18]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetExtension(const Filename: String) : String
   ///  [line: 419, column: 34, file: SmartNJ.System]
   ,GetExtension:function(Self, Filename$3) {
      var Result = "";
      var LName$2 = "";
      var Separator$11 = "",
         x$18 = 0;
      var dx$1 = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Filename$3.length>0) {
         if ((Filename$3.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[Filename$3]);
         } else {
            if (StrEndsWith(Filename$3," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[Filename$3]);
            } else {
               Separator$11 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(LName$2,Separator$11)) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Path (%s) has no filename error",[Filename$3]);
               } else {
                  if ((Filename$3.indexOf(Separator$11)+1)>0) {
                     LName$2 = TW3DirectoryParser.GetFileName$(Self,Filename$3);
                  } else {
                     LName$2 = Filename$3;
                  }
                  if (!TW3ErrorObject.GetFailed$1(Self)) {
                     for(x$18=Filename$3.length;x$18>=1;x$18--) {
                        {var $temp25 = Filename$3.charAt(x$18-1);
                           if ($temp25==".") {
                              dx$1 = Filename$3.length;
                              (dx$1-= x$18);
                              ++dx$1;
                              Result = RightStr(Filename$3,dx$1);
                              break;
                           }
                            else if ($temp25==Separator$11) {
                              break;
                           }
                        }
                     }
                     if (Result.length<1) {
                        TW3ErrorObject.SetLastErrorF$1(Self,"Failed to extract extension, filename (%s) contained no postfix",[TW3DirectoryParser.GetFileName$(Self,Filename$3)]);
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty filename (%s) error",[Filename$3]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetFileName(const FilePath: String) : String
   ///  [line: 342, column: 34, file: SmartNJ.System]
   ,GetFileName:function(Self, FilePath$19) {
      var Result = "";
      var Separator$12 = "",
         x$19 = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$19.length>0) {
         if ((FilePath$19.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$19]);
         } else {
            if (StrEndsWith(FilePath$19," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in path (\"%s\") error",[FilePath$19]);
            } else {
               Separator$12 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (!StrEndsWith(FilePath$19,Separator$12)) {
                  for(x$19=FilePath$19.length;x$19>=1;x$19--) {
                     if (FilePath$19.charAt(x$19-1)!=Separator$12) {
                        Result = FilePath$19.charAt(x$19-1)+Result;
                     } else {
                        break;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty path (%s) error",[FilePath$19]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetFileNameWithoutExtension(const Filename: String) : String
   ///  [line: 372, column: 34, file: SmartNJ.System]
   ,GetFileNameWithoutExtension:function(Self, Filename$4) {
      var Result = "";
      var Separator$13 = "",
         LName$3 = "";
      var x$20 = 0;
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Filename$4.length>0) {
         if ((Filename$4.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[Filename$4]);
         } else {
            if (StrEndsWith(Filename$4," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[Filename$4]);
            } else {
               Separator$13 = TW3DirectoryParser.GetPathSeparator$(Self);
               if ((Filename$4.indexOf(Separator$13)+1)>0) {
                  LName$3 = TW3DirectoryParser.GetFileName$(Self,Filename$4);
               } else {
                  LName$3 = Filename$4;
               }
               if (!TW3ErrorObject.GetFailed$1(Self)) {
                  if (LName$3.length>0) {
                     if ((LName$3.indexOf(".")+1)>0) {
                        for(x$20=LName$3.length;x$20>=1;x$20--) {
                           if (LName$3.charAt(x$20-1)==".") {
                              Result = LName$3.substr(0,(x$20-1));
                              break;
                           }
                        }
                     } else {
                        Result = LName$3;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty filename (%s) error",[Filename$4]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetPathName(const FilePath: String) : String
   ///  [line: 274, column: 34, file: SmartNJ.System]
   ,GetPathName:function(Self, FilePath$20) {
      var Result = "";
      var LParts = [],
         LTemp$12 = "";
      var Separator$14 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FilePath$20.length>0) {
         if ((FilePath$20.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$20]);
         } else {
            if (StrEndsWith(FilePath$20," ")) {
               TW3ErrorObject.SetLastErrorF$1(Self,"Invalid trailing character (\" \") in path (\"%s\") error",[FilePath$20]);
            } else {
               Separator$14 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(FilePath$20,Separator$14)) {
                  if (FilePath$20==TW3DirectoryParser.GetRootMoniker$(Self)) {
                     TW3ErrorObject.SetLastError$1(Self,"Failed to get directory name, path is root");
                     return Result;
                  }
                  LTemp$12 = (FilePath$20).substr(0,(FilePath$20.length-Separator$14.length));
                  LParts = (LTemp$12).split(Separator$14);
                  Result = LParts[(LParts.length-1)];
                  return Result;
               }
               LParts = (FilePath$20).split(Separator$14);
               if (LParts.length>1) {
                  Result = LParts[(LParts.length-1)-1];
               } else {
                  Result = LParts[(LParts.length-1)];
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid or empty path (%s) error",[FilePath$20]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetPathSeparator() : Char
   ///  [line: 155, column: 34, file: SmartNJ.System]
   ,GetPathSeparator:function(Self) {
      return "\/";
   }
   /// function TW3PosixDirectoryParser.GetRootMoniker() : String
   ///  [line: 160, column: 35, file: SmartNJ.System]
   ,GetRootMoniker:function(Self) {
      return "\/";
   }
   /// function TW3PosixDirectoryParser.HasValidFileNameChars(FileName: String) : Boolean
   ///  [line: 165, column: 34, file: SmartNJ.System]
   ,HasValidFileNameChars:function(Self, FileName$1) {
      var Result = false;
      var el$2 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (FileName$1.length>0) {
         if ((FileName$1.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FileName$1]);
         } else {
            for (var $temp26=0;$temp26<FileName$1.length;$temp26++) {
               el$2=$uniCharAt(FileName$1,$temp26);
               if (!el$2) continue;
               Result = (((el$2>="A")&&(el$2<="Z"))||((el$2>="a")&&(el$2<="z"))||((el$2>="0")&&(el$2<="9"))||(el$2=="-")||(el$2=="_")||(el$2==".")||(el$2==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \"%s\" in filename \"%s\" error",[el$2, FileName$1]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.HasValidPathChars(FolderName: String) : Boolean
   ///  [line: 194, column: 34, file: SmartNJ.System]
   ,HasValidPathChars:function(Self, FolderName$1) {
      var Result = false;
      var el$3 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if ((FolderName$1.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in directory-name (\"%s\") error",[FolderName$1]);
      } else {
         if (FolderName$1.length>0) {
            for (var $temp27=0;$temp27<FolderName$1.length;$temp27++) {
               el$3=$uniCharAt(FolderName$1,$temp27);
               if (!el$3) continue;
               Result = (((el$3>="A")&&(el$3<="Z"))||((el$3>="a")&&(el$3<="z"))||((el$3>="0")&&(el$3<="9"))||(el$3=="-")||(el$3=="_")||(el$3==".")||(el$3==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF$1(Self,"Unexpected character \"%s\" in foldername \"%s\" error",[el$3, FolderName$1]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.IncludeLeadingPathDelimiter(const FilePath: String) : String
   ///  [line: 587, column: 34, file: SmartNJ.System]
   ,IncludeLeadingPathDelimiter:function(Self, FilePath$21) {
      var Result = "";
      var Separator$15 = "";
      Separator$15 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$21,Separator$15)) {
         Result = FilePath$21;
      } else {
         Result = Separator$15+FilePath$21;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.IncludeTrailingPathDelimiter(const FilePath: String) : String
   ///  [line: 567, column: 34, file: SmartNJ.System]
   ,IncludeTrailingPathDelimiter:function(Self, FilePath$22) {
      var Result = "";
      var Separator$16 = "";
      Separator$16 = TW3DirectoryParser.GetPathSeparator$(Self);
      Result = FilePath$22;
      if (!StrEndsWith(Result,Separator$16)) {
         Result+=Separator$16;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.IsValidPath(FilePath: String) : Boolean
   ///  [line: 223, column: 34, file: SmartNJ.System]
   ,IsValidPath:function(Self, FilePath$23) {
      var Result = false;
      var Separator$17 = "",
         PathParts$1 = [],
         Index$4 = 0,
         a$241 = 0;
      var part$1 = "";
      if (TW3ErrorObject.GetFailed$1(Self)) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if ((FilePath$23.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF$1(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$23]);
      } else {
         if (FilePath$23.length>0) {
            Separator$17 = TW3DirectoryParser.GetPathSeparator$(Self);
            PathParts$1 = (FilePath$23).split(Separator$17);
            Index$4 = 0;
            var $temp28;
            for(a$241=0,$temp28=PathParts$1.length;a$241<$temp28;a$241++) {
               part$1 = PathParts$1[a$241];
               {var $temp29 = part$1;
                  if ($temp29=="") {
                     TW3ErrorObject.SetLastErrorF$1(Self,"Path has multiple separators (%s) error",[FilePath$23]);
                     return false;
                  }
                   else if ($temp29=="~") {
                     if (Index$4>0) {
                        TW3ErrorObject.SetLastErrorF$1(Self,"Path has misplaced root moniker (%s) error",[FilePath$23]);
                        return Result;
                     }
                  }
                   else {
                     if (Index$4==(PathParts$1.length-1)) {
                        if (!TW3DirectoryParser.HasValidFileNameChars$(Self,part$1)) {
                           return Result;
                        }
                     } else if (!TW3DirectoryParser.HasValidPathChars$(Self,part$1)) {
                        return Result;
                     }
                  }
               }
               Index$4+=1;
            }
            Result = true;
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$38:TW3ErrorObject.Create$38
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3PosixDirectoryParser.$Intf={
   IW3DirectoryParser:[TW3PosixDirectoryParser.GetPathSeparator,TW3PosixDirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3PosixDirectoryParser.IsValidPath,TW3PosixDirectoryParser.HasValidPathChars,TW3PosixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3PosixDirectoryParser.GetFileNameWithoutExtension,TW3PosixDirectoryParser.GetPathName,TW3PosixDirectoryParser.GetDevice,TW3PosixDirectoryParser.GetFileName,TW3PosixDirectoryParser.GetExtension,TW3PosixDirectoryParser.GetDirectoryName,TW3PosixDirectoryParser.IncludeTrailingPathDelimiter,TW3PosixDirectoryParser.IncludeLeadingPathDelimiter,TW3PosixDirectoryParser.ExcludeLeadingPathDelimiter,TW3PosixDirectoryParser.ExcludeTrailingPathDelimiter,TW3PosixDirectoryParser.ChangeFileExt]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TW3EnvVariables = class (TObject)
///  [line: 92, column: 3, file: SmartNJ.System]
var TW3EnvVariables = {
   $ClassName:"TW3EnvVariables",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TRunPlatform enumeration
///  [line: 35, column: 3, file: SmartNJ.System]
var TRunPlatform = [ "rpUnknown", "rpWindows", "rpLinux", "rpMac", "rpEspruino" ];
function PathSeparator() {
   return NodePathAPI().sep;
};
function ParamStr$1(index$3) {
   var Result = "";
   Result = process.argv[index$3];
   return Result
};
function IncludeTrailingPathDelimiter$4(PathName) {
   var Result = "";
   Result = Trim$_String_(PathName);
   if (!StrEndsWith(Result,PathSeparator())) {
      Result+=PathSeparator();
   }
   return Result
};
function GetPlatform() {
   var Result = 0;
   var token = "";
   token = process.platform;
   token = (Trim$_String_(token)).toLocaleLowerCase();
   {var $temp30 = token;
      if ($temp30=="darwin") {
         Result = 3;
      }
       else if ($temp30=="win32") {
         Result = 1;
      }
       else if ($temp30=="linux") {
         Result = 2;
      }
       else if ($temp30=="espruino") {
         Result = 4;
      }
       else {
         Result = 0;
      }
   }
   return Result
};
/// TW3RepeatResult enumeration
///  [line: 55, column: 3, file: System.Time]
var TW3RepeatResult = { 241:"rrContinue", 242:"rrStop", 243:"rrDispose" };
/// TW3CustomRepeater = class (TObject)
///  [line: 71, column: 3, file: System.Time]
var TW3CustomRepeater = {
   $ClassName:"TW3CustomRepeater",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FActive$1 = false;
      $.FDelay$1 = 0;
      $.FHandle$2 = undefined;
   }
   /// procedure TW3CustomRepeater.AllocTimer()
   ///  [line: 446, column: 29, file: System.Time]
   ,AllocTimer:function(Self) {
      if (Self.FHandle$2) {
         TW3CustomRepeater.FreeTimer(Self);
      }
      Self.FHandle$2 = TW3Dispatch.SetInterval(TW3Dispatch,$Event0(Self,TW3CustomRepeater.CBExecute$),Self.FDelay$1);
   }
   /// destructor TW3CustomRepeater.Destroy()
   ///  [line: 400, column: 30, file: System.Time]
   ,Destroy:function(Self) {
      if (Self.FActive$1) {
         TW3CustomRepeater.SetActive$1(Self,false);
      }
      TObject.Destroy(Self);
   }
   /// procedure TW3CustomRepeater.FreeTimer()
   ///  [line: 455, column: 29, file: System.Time]
   ,FreeTimer:function(Self) {
      if (Self.FHandle$2) {
         TW3Dispatch.ClearInterval(TW3Dispatch,Self.FHandle$2);
         Self.FHandle$2 = undefined;
      }
   }
   /// procedure TW3CustomRepeater.SetActive(const NewActive: Boolean)
   ///  [line: 414, column: 29, file: System.Time]
   ,SetActive$1:function(Self, NewActive) {
      if (NewActive!=Self.FActive$1) {
         try {
            if (Self.FActive$1) {
               TW3CustomRepeater.FreeTimer(Self);
            } else {
               TW3CustomRepeater.AllocTimer(Self);
            }
         } finally {
            Self.FActive$1 = NewActive;
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,CBExecute$:function($){return $.ClassType.CBExecute($)}
};
/// TW3Dispatch = class (TObject)
///  [line: 135, column: 3, file: System.Time]
var TW3Dispatch = {
   $ClassName:"TW3Dispatch",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// procedure TW3Dispatch.ClearInterval(const Handle: TW3DispatchHandle)
   ///  [line: 234, column: 29, file: System.Time]
   ,ClearInterval:function(Self, Handle$6) {
      clearInterval(Handle$6);
   }
   /// function TW3Dispatch.Execute(const EntryPoint: TProcedureRef; const WaitForInMs: Integer) : TW3DispatchHandle
   ///  [line: 264, column: 28, file: System.Time]
   ,Execute$1:function(Self, EntryPoint$1, WaitForInMs) {
      var Result = undefined;
      Result = setTimeout(EntryPoint$1,WaitForInMs);
      return Result
   }
   /// procedure TW3Dispatch.RepeatExecute(const Entrypoint: TProcedureRef; const RepeatCount: Integer; const IntervalInMs: Integer)
   ///  [line: 272, column: 29, file: System.Time]
   ,RepeatExecute:function(Self, Entrypoint, RepeatCount, IntervalInMs) {
      if (Entrypoint) {
         if (RepeatCount>0) {
            Entrypoint();
            if (RepeatCount>1) {
               TW3Dispatch.Execute$1(Self,function () {
                  TW3Dispatch.RepeatExecute(Self,Entrypoint,(RepeatCount-1),IntervalInMs);
               },IntervalInMs);
            }
         } else {
            Entrypoint();
            TW3Dispatch.Execute$1(Self,function () {
               TW3Dispatch.RepeatExecute(Self,Entrypoint,(-1),IntervalInMs);
            },IntervalInMs);
         }
      }
   }
   /// function TW3Dispatch.SetInterval(const Entrypoint: TProcedureRef; const IntervalDelayInMS: Integer) : TW3DispatchHandle
   ///  [line: 226, column: 28, file: System.Time]
   ,SetInterval:function(Self, Entrypoint$1, IntervalDelayInMS) {
      var Result = undefined;
      Result = setInterval(Entrypoint$1,IntervalDelayInMS);
      return Result
   }
   ,Destroy:TObject.Destroy
};
function JDateToDateTime(Obj) {
   return Obj.getTime()/86400000+25569;
};
function DateTimeToJDate(Present) {
   var Result = null;
   Result = new Date();
   Result.setTime(Math.round((Present-25569)*86400000));
   return Result
};
var CNT_DaysInMonthData = [[31,28,31,30,31,30,31,31,30,31,30,31],[31,29,31,30,31,30,31,31,30,31,30,31]];
/// TW3NodeStorageDevice = class (TW3StorageDevice)
///  [line: 62, column: 3, file: SmartNJ.Device.Storage]
var TW3NodeStorageDevice = {
   $ClassName:"TW3NodeStorageDevice",$Parent:TW3StorageDevice
   ,$Init:function ($) {
      TW3StorageDevice.$Init($);
      $.FCurrent$1 = $.FRootPath = "";
      $.FFileSys = null;
   }
   /// procedure TW3NodeStorageDevice.CdUp(CB: TW3DeviceCdUpCallback)
   ///  [line: 675, column: 32, file: SmartNJ.Device.Storage]
   ,CdUp:function(Self, CB$3) {
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         TW3StorageDevice.GetPath$(Self,function (Sender, Path$1, Success) {
            var xCurrentPath = "",
               DirParser = null,
               Moniker = "",
               Separator$18 = "",
               Parts$2 = [];
            if (Success) {
               xCurrentPath = Path$1;
               DirParser = GetDirectoryParser();
               Moniker = DirParser[1]();
               Separator$18 = DirParser[0]();
               xCurrentPath = TW3NodeStorageDevice.Normalize(Self,xCurrentPath);
               if ((xCurrentPath).toLocaleLowerCase()==(Moniker).toLocaleLowerCase()) {
                  if (CB$3) {
                     CB$3(Self,false);
                  }
                  return;
               }
               if (StrEndsWith(xCurrentPath,Separator$18)) {
                  xCurrentPath = (xCurrentPath).substr(0,(xCurrentPath.length-1));
               }
               Parts$2 = (xCurrentPath).split(Separator$18);
               Parts$2.splice((Parts$2.length-1),1)
               ;
               xCurrentPath = (Parts$2).join(Separator$18);
               TW3StorageDevice.ChDir$(Self,xCurrentPath,function (Sender$1, Path$2, Success$1) {
                  if (CB$3) {
                     CB$3(Self,Success$1);
                  }
               });
            } else if (CB$3) {
               CB$3(Self,false);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.CdUp"]);
         if (CB$3) {
            CB$3(Self,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.ChDir(FolderPath: String; CB: TW3DeviceChDirCallback)
   ///  [line: 375, column: 32, file: SmartNJ.Device.Storage]
   ,ChDir:function(Self, FolderPath, CB$4) {
      var Access$6 = null,
         workingpath = "";
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         FolderPath = TW3NodeStorageDevice.Normalize(Self,FolderPath);
         Access$6 = process;
         if (Access$6!==null) {
            workingpath = "";
            try {
               Access$6.chdir(FolderPath);
               workingpath = Access$6.cwd();
            } catch ($e) {
               var e$6 = $W($e);
               TW3OwnedErrorObject.SetLastError(Self,e$6.FMessage);
               if (CB$4) {
                  CB$4(Self,FolderPath,false);
               }
               return;
            }
            Self.FCurrent$1 = workingpath;
            if (CB$4) {
               CB$4(Self,workingpath,workingpath.length>0);
            }
         } else {
            TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, unable to access process error",["TW3NodeStorageDevice.ChDir"]);
            if (CB$4) {
               CB$4(Self,FolderPath,false);
            }
         }
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.ChDir"]);
         if (CB$4) {
            CB$4(Self,FolderPath,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.DirExists(FolderName: String; CB: TW3DeviceGetFileExistsCallback)
   ///  [line: 533, column: 32, file: SmartNJ.Device.Storage]
   ,DirExists:function(Self, FolderName$2, CB$5) {
      var PROCNAME = "";
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         PROCNAME = "TW3NodeStorageDevice.DirExists";
         FolderName$2 = TW3NodeStorageDevice.Normalize(Self,FolderName$2);
         Self.FFileSys.lstat(FolderName$2,function (err, stats) {
            if (err) {
               TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed with message %s",[PROCNAME, err.message]);
               if (CB$5) {
                  CB$5(Self,FolderName$2,false);
               }
               return;
            }
            if (CB$5) {
               CB$5(Self,FolderName$2,stats.isDirectory());
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.DirExists"]);
         if (CB$5) {
            CB$5(Self,FolderName$2,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.Examine(FolderPath: String; CB: TW3FileOperationExamineCallBack)
   ///  [line: 642, column: 32, file: SmartNJ.Device.Storage]
   ,Examine:function(Self, FolderPath$1, CB$6) {
      var PROCNAME$1 = "";
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         PROCNAME$1 = "TW3NodeStorageDevice.Examine";
         FolderPath$1 = TW3NodeStorageDevice.Normalize(Self,FolderPath$1);
         Self.FFileSys.readdir(FolderPath$1,function (err$1, files) {
            if (err$1) {
               TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed with message: %s",[PROCNAME$1, err$1.message]);
               if (CB$6) {
                  CB$6(Self,FolderPath$1,[],false);
               }
            } else if (CB$6) {
               CB$6(Self,FolderPath$1,files,true);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.Examine"]);
         if (CB$6) {
            CB$6(Self,FolderPath$1,[],false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.FileExists(Filename: String; CB: TW3DeviceGetFileExistsCallback)
   ///  [line: 426, column: 32, file: SmartNJ.Device.Storage]
   ,FileExists:function(Self, Filename$5, CB$7) {
      var PROCNAME$2 = "";
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         PROCNAME$2 = "TW3NodeStorageDevice.FileExists";
         Filename$5 = TW3NodeStorageDevice.Normalize(Self,Filename$5);
         Self.FFileSys.exists(Filename$5,function (exists$1) {
            if (exists$1) {
               Self.FFileSys.lstat(Filename$5,function (err$2, stats$1) {
                  if (err$2) {
                     TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed with message %s",[PROCNAME$2, err$2.message]);
                     if (CB$7) {
                        CB$7(Self,Filename$5,false);
                     }
                     return;
                  }
                  if (CB$7) {
                     CB$7(Self,Filename$5,stats$1.isFile());
                  }
               });
            } else if (CB$7) {
               CB$7(Self,Filename$5,false);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.FileExists"]);
         if (CB$7) {
            CB$7(Self,"",false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.GetFileSize(Filename: String; CB: TW3DeviceGetFileSizeCallback)
   ///  [line: 305, column: 32, file: SmartNJ.Device.Storage]
   ,GetFileSize:function(Self, Filename$6, CB$8) {
      var PROCNAME$3 = "";
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         PROCNAME$3 = "TW3NodeStorageDevice.GetFileSize";
         Filename$6 = TW3NodeStorageDevice.Normalize(Self,Filename$6);
         Self.FFileSys.lstat(Filename$6,function (err$3, stats$2) {
            if (err$3) {
               TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed with message %s",[PROCNAME$3, err$3.message]);
               if (CB$8) {
                  CB$8(Self,Filename$6,0,false);
               }
               return;
            }
            if (CB$8) {
               CB$8(Self,Filename$6,stats$2.size,true);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.GetFileSize"]);
         if (CB$8) {
            CB$8(Self,"",0,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.GetPath(CB: TW3DeviceGetPathCallback)
   ///  [line: 342, column: 32, file: SmartNJ.Device.Storage]
   ,GetPath:function(Self, CB$9) {
      var Access$7 = null;
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         try {
            Access$7 = process;
            if (Access$7!==null) {
               if (CB$9) {
                  CB$9(Self,Access$7.cwd(),true);
               }
            }
         } catch ($e) {
            var e$7 = $W($e);
            TW3OwnedErrorObject.SetLastError(Self,e$7.FMessage);
            CB$9(Self,"",false);
            return;
         }
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.GetPath"]);
         if (CB$9) {
            CB$9(Self,"",false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.GetStorageObjectType(ObjName: String; CB: TW3DeviceObjTypeCallback)
   ///  [line: 473, column: 32, file: SmartNJ.Device.Storage]
   ,GetStorageObjectType:function(Self, ObjName, CB$10) {
      var PROCNAME$4 = "";
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         PROCNAME$4 = "TW3NodeStorageDevice.GetStorageObjectType";
         ObjName = TW3NodeStorageDevice.Normalize(Self,ObjName);
         Self.FFileSys.lstat(ObjName,function (err$4, stats$3) {
            if (err$4) {
               TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed with message %s",[PROCNAME$4, err$4.message]);
               if (CB$10) {
                  CB$10(Self,ObjName,0,false);
               }
               return;
            }
            if (CB$10) {
               if (stats$3.isDirectory()) {
                  CB$10(Self,ObjName,2,true);
               } else if (stats$3.isFile()) {
                  CB$10(Self,ObjName,1,true);
               } else if (stats$3.isBlockDevice()) {
                  CB$10(Self,ObjName,3,true);
               } else if (stats$3.isCharacterDevice()) {
                  CB$10(Self,ObjName,4,true);
               } else if (stats$3.isSymbolicLink()) {
                  CB$10(Self,ObjName,5,true);
               } else if (stats$3.isFIFO()) {
                  CB$10(Self,ObjName,6,true);
               } else if (stats$3.isSocket()) {
                  CB$10(Self,ObjName,7,true);
               } else {
                  CB$10(Self,ObjName,0,false);
               }
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.GetStorageObjectType"]);
         if (CB$10) {
            CB$10(Self,ObjName,0,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.Load(Filename: String; CB: TW3DeviceLoadCallback)
   ///  [line: 736, column: 32, file: SmartNJ.Device.Storage]
   ,Load:function(Self, Filename$7, CB$11) {
      var PROCNAME$5 = "";
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         PROCNAME$5 = "TW3NodeStorageDevice.Load";
         Filename$7 = TW3NodeStorageDevice.Normalize(Self,Filename$7);
         Self.FFileSys.readFile(Filename$7,function (err$5, data) {
            var LTypedAccess$1 = null;
            var LBytes$2 = [],
               DataStream = null;
            if (err$5) {
               TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed: %s",[PROCNAME$5, err$5.message]);
               if (CB$11) {
                  CB$11(Self,Filename$7,null,false);
               }
               return;
            }
            if (CB$11) {
               LTypedAccess$1 = new Uint8Array(data);
               LBytes$2 = TDatatype.TypedArrayToBytes$1(TDatatype,LTypedAccess$1);
               DataStream = TDataTypeConverter.Create$4$($New(TMemoryStream));
               if (LBytes$2.length>0) {
                  TStream.Write$1(DataStream,LBytes$2);
                  TStream.SetPosition$(DataStream,0);
               }
               CB$11(Self,Filename$7,DataStream,true);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.Load"]);
         if (CB$11) {
            CB$11(Self,Filename$7,null,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.MakeDir(FolderName: String; Mode: TW3FilePermissionMask; CB: TW3DeviceMakeDirCallback)
   ///  [line: 571, column: 32, file: SmartNJ.Device.Storage]
   ,MakeDir:function(Self, FolderName$3, Mode, CB$12) {
      var PROCNAME$6 = "",
         ModeStr = "";
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         PROCNAME$6 = "TW3NodeStorageDevice.MakeDir";
         FolderName$3 = TW3NodeStorageDevice.Normalize(Self,FolderName$3);
         ModeStr = Mode.toString();
         Self.FFileSys.mkdir(FolderName$3,ModeStr,function (err$6) {
            if (err$6) {
               TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed with message: %s",[PROCNAME$6, err$6.message]);
               if (CB$12) {
                  CB$12(Self,FolderName$3,false);
               }
            } else if (CB$12) {
               CB$12(Self,FolderName$3,true);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.MakeDir"]);
         if (CB$12) {
            CB$12(Self,FolderName$3,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.Mount(Authentication: TW3DeviceAuthenticationData; CB: TW3StorageDeviceMountEvent)
   ///  [line: 245, column: 32, file: SmartNJ.Device.Storage]
   ,Mount:function(Self, Authentication, CB$13) {
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         TW3StorageDevice.UnMount$(Self,null);
      }
      Self.FFileSys = NodeFsAPI();
      try {
         Self.FRootPath = process.cwd();
      } catch ($e) {
         var e$8 = $W($e);
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.Mount"]);
         if (CB$13) {
            CB$13(Self,false);
         }
         return;
      }
      Self.FCurrent$1 = Self.FRootPath;
      TW3StorageDevice.SetActive(Self,true);
      if (CB$13) {
         CB$13(Self,true);
      }
   }
   /// function TW3NodeStorageDevice.Normalize(NameOrPath: String) : String
   ///  [line: 300, column: 31, file: SmartNJ.Device.Storage]
   ,Normalize:function(Self, NameOrPath) {
      return NodePathAPI().normalize(NameOrPath);
   }
   /// procedure TW3NodeStorageDevice.RemoveDir(FolderName: String; CB: TW3DeviceRemoveDirCallback)
   ///  [line: 608, column: 32, file: SmartNJ.Device.Storage]
   ,RemoveDir:function(Self, FolderName$4, CB$14) {
      /* null */
   }
   /// procedure TW3NodeStorageDevice.Save(Filename: String; Data: TStream; CB: TW3DeviceSaveCallback)
   ///  [line: 783, column: 32, file: SmartNJ.Device.Storage]
   ,Save:function(Self, Filename$8, Data$32, CB$15) {
      var PROCNAME$7 = "",
         buffer$2 = null;
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         PROCNAME$7 = "TW3NodeStorageDevice.Save";
         Filename$8 = TW3NodeStorageDevice.Normalize(Self,Filename$8);
         TStream.SetPosition$(Data$32,0);
         buffer$2 = Buffer.from(TStream.Read(Data$32,TStream.GetSize$1$(Data$32)));
         Self.FFileSys.writeFile(Filename$8,buffer$2,function (err$7) {
            if (err$7) {
               TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed: %s",[PROCNAME$7, err$7.message]);
               if (CB$15) {
                  CB$15(Self,Filename$8,false);
               }
               return;
            }
            if (CB$15) {
               CB$15(Self,Filename$8,true);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF(Self,"%s failed, device not active error",["TW3NodeStorageDevice.Save"]);
         if (CB$15) {
            CB$15(Self,Filename$8,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.UnMount(CB: TW3StorageDeviceUnMountEvent)
   ///  [line: 281, column: 32, file: SmartNJ.Device.Storage]
   ,UnMount:function(Self, CB$16) {
      if (TW3OwnedErrorObject.GetFailed(Self)) {
         TW3OwnedErrorObject.ClearLastError(Self);
      }
      if (TW3StorageDevice.GetActive(Self)) {
         try {
            TW3StorageDevice.SetActive(Self,false);
            if (CB$16) {
               CB$16(Self,true);
            }
         } finally {
            Self.FFileSys = null;
            Self.FRootPath = "";
            Self.FCurrent$1 = "";
         }
      }
   }
   ,Destroy:TW3Component.Destroy
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$18:TW3OwnedErrorObject.Create$18
   ,Create$43:TW3Component.Create$43
   ,FinalizeObject:TW3StorageDevice.FinalizeObject
   ,InitializeObject:TW3StorageDevice.InitializeObject
   ,CdUp$:function($){return $.ClassType.CdUp.apply($.ClassType, arguments)}
   ,ChDir$:function($){return $.ClassType.ChDir.apply($.ClassType, arguments)}
   ,DirExists$:function($){return $.ClassType.DirExists.apply($.ClassType, arguments)}
   ,Examine$:function($){return $.ClassType.Examine.apply($.ClassType, arguments)}
   ,FileExists$:function($){return $.ClassType.FileExists.apply($.ClassType, arguments)}
   ,GetFileSize$:function($){return $.ClassType.GetFileSize.apply($.ClassType, arguments)}
   ,GetPath$:function($){return $.ClassType.GetPath.apply($.ClassType, arguments)}
   ,GetStorageObjectType$:function($){return $.ClassType.GetStorageObjectType.apply($.ClassType, arguments)}
   ,Load$:function($){return $.ClassType.Load.apply($.ClassType, arguments)}
   ,MakeDir$:function($){return $.ClassType.MakeDir.apply($.ClassType, arguments)}
   ,Mount$:function($){return $.ClassType.Mount.apply($.ClassType, arguments)}
   ,RemoveDir$:function($){return $.ClassType.RemoveDir.apply($.ClassType, arguments)}
   ,Save$:function($){return $.ClassType.Save.apply($.ClassType, arguments)}
   ,UnMount$:function($){return $.ClassType.UnMount.apply($.ClassType, arguments)}
};
TW3NodeStorageDevice.$Intf={
   IW3StorageDevice:[TW3StorageDevice.GetActive,TW3StorageDevice.GetName,TW3StorageDevice.GetDeviceOptions,TW3StorageDevice.GetDeviceId,TW3NodeStorageDevice.GetPath,TW3NodeStorageDevice.GetFileSize,TW3NodeStorageDevice.FileExists,TW3NodeStorageDevice.DirExists,TW3NodeStorageDevice.MakeDir,TW3NodeStorageDevice.RemoveDir,TW3NodeStorageDevice.Examine,TW3NodeStorageDevice.ChDir,TW3NodeStorageDevice.CdUp,TW3NodeStorageDevice.GetStorageObjectType,TW3NodeStorageDevice.Load,TW3NodeStorageDevice.Save]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TWriteFileOptions = record
///  [line: 71, column: 3, file: NodeJS.fs]
function Copy$TWriteFileOptions(s,d) {
   return d;
}
function Clone$TWriteFileOptions($) {
   return {

   }
}
/// TWatchOptions = record
///  [line: 93, column: 3, file: NodeJS.fs]
function Copy$TWatchOptions(s,d) {
   return d;
}
function Clone$TWatchOptions($) {
   return {

   }
}
/// TWatchFileOptions = record
///  [line: 88, column: 3, file: NodeJS.fs]
function Copy$TWatchFileOptions(s,d) {
   return d;
}
function Clone$TWatchFileOptions($) {
   return {

   }
}
/// TWatchFileListenerObject = record
///  [line: 83, column: 3, file: NodeJS.fs]
function Copy$TWatchFileListenerObject(s,d) {
   return d;
}
function Clone$TWatchFileListenerObject($) {
   return {

   }
}
/// TReadFileSyncOptions = record
///  [line: 66, column: 3, file: NodeJS.fs]
function Copy$TReadFileSyncOptions(s,d) {
   return d;
}
function Clone$TReadFileSyncOptions($) {
   return {

   }
}
/// TReadFileOptions = record
///  [line: 61, column: 3, file: NodeJS.fs]
function Copy$TReadFileOptions(s,d) {
   return d;
}
function Clone$TReadFileOptions($) {
   return {

   }
}
/// TCreateWriteStreamOptions = class (TObject)
///  [line: 122, column: 3, file: NodeJS.fs]
var TCreateWriteStreamOptions = {
   $ClassName:"TCreateWriteStreamOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TCreateWriteStreamOptionsEx = class (TCreateWriteStreamOptions)
///  [line: 131, column: 3, file: NodeJS.fs]
var TCreateWriteStreamOptionsEx = {
   $ClassName:"TCreateWriteStreamOptionsEx",$Parent:TCreateWriteStreamOptions
   ,$Init:function ($) {
      TCreateWriteStreamOptions.$Init($);
      $.start = 0;
   }
   ,Destroy:TObject.Destroy
};
/// TCreateReadStreamOptions = class (TObject)
///  [line: 102, column: 3, file: NodeJS.fs]
var TCreateReadStreamOptions = {
   $ClassName:"TCreateReadStreamOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TCreateReadStreamOptionsEx = class (TCreateReadStreamOptions)
///  [line: 112, column: 3, file: NodeJS.fs]
var TCreateReadStreamOptionsEx = {
   $ClassName:"TCreateReadStreamOptionsEx",$Parent:TCreateReadStreamOptions
   ,$Init:function ($) {
      TCreateReadStreamOptions.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TAppendFileOptions = record
///  [line: 77, column: 3, file: NodeJS.fs]
function Copy$TAppendFileOptions(s,d) {
   return d;
}
function Clone$TAppendFileOptions($) {
   return {

   }
}
function NodeFsAPI() {
   return require("fs");
};
function NodePathAPI() {
   return require("path");
};
/// JPathParseData = record
///  [line: 20, column: 3, file: NodeJS.Path]
function Copy$JPathParseData(s,d) {
   return d;
}
function Clone$JPathParseData($) {
   return {

   }
}
/// TNJFileWalker = class (TW3ErrorObject)
///  [line: 48, column: 3, file: SmartNJ.FileWalker]
var TNJFileWalker = {
   $ClassName:"TNJFileWalker",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.OnIncludeFile = null;
      $.OnAfterWalk = null;
      $.FBusy = $.FCancel = false;
      $.FCurrent$2 = $.FPath$1 = "";
      $.FDelay$2 = 0;
      $.FFilesystem = $.FFinished = $.FListData = null;
      $.FItemStack = [];
   }
   /// procedure TNJFileWalker.Cancel()
   ///  [line: 163, column: 25, file: SmartNJ.FileWalker]
   ,Cancel:function(Self) {
      if (Self.FBusy) {
         Self.FCancel = true;
         Self.FItemStack.length=0;
      }
   }
   /// constructor TNJFileWalker.Create(FileSystem: TW3StorageDevice)
   ///  [line: 116, column: 27, file: SmartNJ.FileWalker]
   ,Create$72:function(Self, FileSystem) {
      TW3ErrorObject.Create$38(Self);
      if (FileSystem===null) {
         throw Exception.Create($New(ENJFileWalker),$R[25]);
      }
      if (!TW3StorageDevice.GetActive(FileSystem)) {
         throw Exception.Create($New(ENJFileWalker),$R[26]);
      }
      Self.FFilesystem = FileSystem;
      Self.FListData = new TNJFileItemList();
      Self.FDelay$2 = 20;
      return Self
   }
   /// destructor TNJFileWalker.Destroy()
   ///  [line: 131, column: 26, file: SmartNJ.FileWalker]
   ,Destroy:function(Self) {
      try {
         try {
            if (Self.FBusy) {
               TNJFileWalker.Cancel(Self);
            }
         } finally {
            Self.FItemStack.length=0;
            Self.FListData.dlPath = "";
            Self.FListData.dlItems.length=0;
            Self.FListData = null;
            Self.FFinished = null;
         }
      } catch ($e) {
         var e$9 = $W($e);
         /* null */
      }
      TW3ErrorObject.Destroy(Self);
   }
   /// procedure TNJFileWalker.DoAfterWalk()
   ///  [line: 201, column: 25, file: SmartNJ.FileWalker]
   ,DoAfterWalk:function(Self) {
      try {
         if (Self.FFinished) {
            Self.FFinished(Self,true);
         }
      } finally {
         if (Self.OnAfterWalk) {
            Self.OnAfterWalk(Self);
         }
      }
   }
   /// procedure TNJFileWalker.NextOrDone()
   ///  [line: 228, column: 25, file: SmartNJ.FileWalker]
   ,NextOrDone:function(Self) {
      function SignalDone() {
         Self.FBusy = false;
         TW3Dispatch.Execute$1(TW3Dispatch,$Event0(Self,TNJFileWalker.DoAfterWalk),Self.FDelay$2);
      };
      if (Self.FCancel) {
         SignalDone();
         return;
      }
      if (Self.FItemStack.length>0) {
         TW3Dispatch.Execute$1(TW3Dispatch,$Event0(Self,TNJFileWalker.ProcessFromStack),Self.FDelay$2);
      } else {
         SignalDone();
      }
   }
   /// procedure TNJFileWalker.ProcessFromStack()
   ///  [line: 251, column: 25, file: SmartNJ.FileWalker]
   ,ProcessFromStack:function(Self) {
      var LKeep = {v:false},
         FileObjName = "";
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Self.FCancel) {
         TNJFileWalker.NextOrDone(Self);
         return;
      }
      if (Self.FItemStack.length>0) {
         LKeep.v = true;
         FileObjName = Self.FItemStack.pop();
         Self.FCurrent$2 = NodePathAPI().normalize(IncludeTrailingPathDelimiter$4(Self.FPath$1)+FileObjName);
         NodeFsAPI().stat(Self.FCurrent$2,function (err$8, stats$4) {
            var LItem$3 = null,
               LTemp$13 = "";
            if (err$8) {
               TW3ErrorObject.SetLastError$1(Self,err$8.message);
               TNJFileWalker.NextOrDone(Self);
               return;
            }
            LItem$3 = new TNJFileItem();
            LItem$3.diFileName = FileObjName;
            LItem$3.diFileType = (stats$4.isFile())?0:1;
            LItem$3.diFileSize = (stats$4.isFile())?stats$4.size:0;
            LItem$3.diCreated = (stats$4.ctime!==null)?JDateToDateTime(stats$4.ctime):Now();
            LItem$3.diModified = (stats$4.mtime!==null)?JDateToDateTime(stats$4.mtime):Now();
            if (stats$4.isFile()) {
               LTemp$13 = "";
               LTemp$13 = '0' + ((stats$4).mode & parseInt('777', 8)).toString(8);
               LItem$3.diFileMode = LTemp$13;
            }
            if (Self.OnIncludeFile) {
               Self.OnIncludeFile(Self,LItem$3,LKeep);
            }
            if (LKeep.v) {
               Self.FListData.dlItems.push(LItem$3);
            } else {
               LItem$3 = null;
            }
            Self.FCurrent$2 = "";
            TW3Dispatch.Execute$1(TW3Dispatch,$Event0(Self,TNJFileWalker.NextOrDone),Self.FDelay$2);
         });
      } else {
         TNJFileWalker.NextOrDone(Self);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$38:TW3ErrorObject.Create$38
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
};
TNJFileWalker.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TNJFileItemType enumeration
///  [line: 23, column: 3, file: SmartNJ.FileWalker]
var TNJFileItemType = [ "wtFile", "wtFolder" ];
/// TNJFileItemList = class (JObject)
///  [line: 38, column: 3, file: SmartNJ.FileWalker]
function TNJFileItemList() {
   this.dlItems = [];
}
$Extend(Object,TNJFileItemList,
   {
      "dlPath" : ""
   });

/// TNJFileItem = class (JObject)
///  [line: 28, column: 3, file: SmartNJ.FileWalker]
function TNJFileItem() {
}
$Extend(Object,TNJFileItem,
   {
      "diFileName" : "",
      "diFileType" : 0,
      "diFileSize" : 0,
      "diFileMode" : "",
      "diCreated" : undefined,
      "diModified" : undefined
   });

/// ENJFileWalker = class (EW3Exception)
///  [line: 18, column: 3, file: SmartNJ.FileWalker]
var ENJFileWalker = {
   $ClassName:"ENJFileWalker",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TApplication = class (TObject)
///  [line: 29, column: 3, file: SmartNJ.Application]
var TApplication = {
   $ClassName:"TApplication",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TApplication.GetOSApi() : Jos_Exports
   ///  [line: 109, column: 23, file: SmartNJ.Application]
   ,GetOSApi:function(Self) {
      return NodeJSOsAPI();
   }
   ,Destroy:TObject.Destroy
};
function Application() {
   var Result = null;
   if (__App===null) {
      __App = TObject.Create($New(TApplication));
   }
   Result = __App;
   return Result
};
/// Tlisteners_result_object = class (TObject)
///  [line: 36, column: 3, file: NodeJS.cluster]
var Tlisteners_result_object = {
   $ClassName:"Tlisteners_result_object",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
function NodeJSClusterAPI() {
   return require("cluster");
};
/// TNJUdpVersion enumeration
///  [line: 63, column: 3, file: SmartNJ.Udp]
var TNJUdpVersion = [ "udV4", "udv6", "udV7" ];
/// TNJNetworkService = class (TW3HandleBasedObject)
///  [line: 35, column: 3, file: SmartNJ.Network]
var TNJNetworkService = {
   $ClassName:"TNJNetworkService",$Parent:TW3HandleBasedObject
   ,$Init:function ($) {
      TW3HandleBasedObject.$Init($);
      $.OnAfterStopped = null;
      $.OnBeforeStopped = null;
      $.OnAfterStarted = null;
      $.FActive$2 = false;
      $.FHost = "";
      $.FPort = 0;
   }
   /// procedure TNJNetworkService.AfterStart()
   ///  [line: 109, column: 29, file: SmartNJ.Network]
   ,AfterStart:function(Self) {
      if (Self.OnAfterStarted) {
         Self.OnAfterStarted(Self);
      }
   }
   /// procedure TNJNetworkService.AfterStop()
   ///  [line: 121, column: 29, file: SmartNJ.Network]
   ,AfterStop:function(Self) {
      if (Self.OnAfterStopped) {
         Self.OnAfterStopped(Self);
      }
   }
   /// procedure TNJNetworkService.BeforeStop()
   ///  [line: 115, column: 29, file: SmartNJ.Network]
   ,BeforeStop:function(Self) {
      if (Self.OnBeforeStopped) {
         Self.OnBeforeStopped(Self);
      }
   }
   /// destructor TNJNetworkService.Destroy()
   ///  [line: 86, column: 30, file: SmartNJ.Network]
   ,Destroy:function(Self) {
      if (TNJNetworkService.GetActive$1(Self)) {
         try {
            try {
               TNJNetworkService.FinalizeService$(Self);
            } catch ($e) {
               var e$10 = $W($e);
               /* null */
            }
         } finally {
            TW3HandleBasedObject.SetObjectHandle(Self,undefined);
         }
      }
      TObject.Destroy(Self);
   }
   /// function TNJNetworkService.GetActive() : Boolean
   ///  [line: 157, column: 28, file: SmartNJ.Network]
   ,GetActive$1:function(Self) {
      return Self.FActive$2;
   }
   /// function TNJNetworkService.GetAddress() : String
   ///  [line: 127, column: 28, file: SmartNJ.Network]
   ,GetAddress:function(Self) {
      return Self.FHost;
   }
   /// function TNJNetworkService.GetPort() : Integer
   ///  [line: 142, column: 28, file: SmartNJ.Network]
   ,GetPort:function(Self) {
      return Self.FPort;
   }
   /// procedure TNJNetworkService.SetActiveEx(const NewValue: Boolean)
   ///  [line: 162, column: 29, file: SmartNJ.Network]
   ,SetActiveEx:function(Self, NewValue) {
      Self.FActive$2 = NewValue;
   }
   /// procedure TNJNetworkService.SetAddress(const NewHost: String)
   ///  [line: 132, column: 29, file: SmartNJ.Network]
   ,SetAddress:function(Self, NewHost) {
      if (TNJNetworkService.GetActive$1(Self)) {
         throw Exception.Create($New(ENJNetworkError),"Address cannot be altered while object is active error");
      } else {
         Self.FHost = Trim$_String_(NewHost);
      }
   }
   /// procedure TNJNetworkService.SetPort(const NewPort: Integer)
   ///  [line: 147, column: 29, file: SmartNJ.Network]
   ,SetPort:function(Self, NewPort) {
      if (TNJNetworkService.GetActive$1(Self)) {
         throw Exception.Create($New(ENJNetworkError),"Port cannot be altered while object is active error");
      } else {
         Self.FPort = NewPort;
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,FinalizeService$:function($){return $.ClassType.FinalizeService($)}
   ,InitializeService$:function($){return $.ClassType.InitializeService($)}
};
/// ENJNetworkError = class (EW3Exception)
///  [line: 27, column: 3, file: SmartNJ.Network]
var ENJNetworkError = {
   $ClassName:"ENJNetworkError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3Structure = class (TObject)
///  [line: 92, column: 3, file: System.Structure]
var TW3Structure = {
   $ClassName:"TW3Structure",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TW3Structure.ReadBool(const Name: String) : Boolean
   ///  [line: 203, column: 23, file: System.Structure]
   ,ReadBool$1:function(Self, Name$6) {
      return (TW3Structure.Read$3$(Self,Name$6)?true:false);
   }
   /// function TW3Structure.ReadBytes(const Name: String) : TByteArray
   ///  [line: 154, column: 23, file: System.Structure]
   ,ReadBytes$1:function(Self, Name$7) {
      return TW3Structure.Read$3$(Self,Name$7);
   }
   /// function TW3Structure.ReadDateTime(const Name: String) : TDateTime
   ///  [line: 213, column: 23, file: System.Structure]
   ,ReadDateTime$2:function(Self, Name$8) {
      return TW3Structure.Read$3$(Self,Name$8);
   }
   /// function TW3Structure.ReadFloat(const Name: String) : Float
   ///  [line: 208, column: 23, file: System.Structure]
   ,ReadFloat$1:function(Self, Name$9) {
      return Number(TW3Structure.Read$3$(Self,Name$9));
   }
   /// function TW3Structure.ReadInt(const Name: String) : Integer
   ///  [line: 198, column: 23, file: System.Structure]
   ,ReadInt$1:function(Self, Name$10) {
      return parseInt(TW3Structure.Read$3$(Self,Name$10),10);
   }
   /// function TW3Structure.ReadString(const Name: String; const Decode: Boolean) : String
   ///  [line: 190, column: 23, file: System.Structure]
   ,ReadString$3:function(Self, Name$11, Decode) {
      var Result = "";
      if (Decode) {
         Result = TString.DecodeBase64(TString,String(TW3Structure.Read$3$(Self,Name$11)));
      } else {
         Result = String(TW3Structure.Read$3$(Self,Name$11));
      }
      return Result
   }
   /// procedure TW3Structure.WriteBool(const Name: String; value: Boolean)
   ///  [line: 175, column: 24, file: System.Structure]
   ,WriteBool$1:function(Self, Name$12, value$1) {
      TW3Structure.Write$3$(Self,Name$12,value$1);
   }
   /// procedure TW3Structure.WriteBytes(const Name: String; const Value: TByteArray)
   ///  [line: 126, column: 24, file: System.Structure]
   ,WriteBytes$1:function(Self, Name$13, Value$34) {
      try {
         TW3Structure.Write$3$(Self,Name$13,TDatatype.BytesToBase64$1(TDatatype,Value$34));
      } catch ($e) {
         var e$11 = $W($e);
         throw EW3Exception.CreateFmt($New(EW3Structure),"Failed to write bytes to structure, system threw exception [%s] with message [%s]",[TObject.ClassName(e$11.ClassType), e$11.FMessage]);
      }
   }
   /// procedure TW3Structure.WriteDateTime(const Name: String; value: TDateTime)
   ///  [line: 185, column: 24, file: System.Structure]
   ,WriteDateTime$1:function(Self, Name$14, value$2) {
      TW3Structure.Write$3$(Self,Name$14,value$2);
   }
   /// procedure TW3Structure.WriteFloat(const Name: String; value: Float)
   ///  [line: 180, column: 24, file: System.Structure]
   ,WriteFloat:function(Self, Name$15, value$3) {
      TW3Structure.Write$3$(Self,Name$15,value$3);
   }
   /// procedure TW3Structure.WriteInt(const Name: String; value: Integer)
   ///  [line: 170, column: 24, file: System.Structure]
   ,WriteInt:function(Self, Name$16, value$4) {
      TW3Structure.Write$3$(Self,Name$16,value$4);
   }
   /// procedure TW3Structure.WriteStream(const Name: String; const Value: TStream)
   ///  [line: 138, column: 24, file: System.Structure]
   ,WriteStream:function(Self, Name$17, Value$35) {
      var LBytes$3 = [];
      if (Value$35!==null) {
         if (TStream.GetSize$1$(Value$35)>0) {
            TStream.SetPosition$(Value$35,0);
            LBytes$3 = TStream.Read(Value$35,TStream.GetSize$1$(Value$35));
         }
         TW3Structure.WriteBytes$1(Self,Name$17,LBytes$3);
      } else {
         throw Exception.Create($New(EW3Structure),"Failed to write stream to structure, stream was nil error");
      }
   }
   /// procedure TW3Structure.WriteString(Name: String; Value: String; const Encode: Boolean)
   ///  [line: 159, column: 24, file: System.Structure]
   ,WriteString$2:function(Self, Name$18, Value$36, Encode) {
      if (Encode) {
         Value$36 = TString.EncodeBase64(TString,Value$36);
      }
      TW3Structure.Write$3$(Self,Name$18,Value$36);
   }
   ,Destroy:TObject.Destroy
   ,Clear$2$:function($){return $.ClassType.Clear$2($)}
   ,Exists$1$:function($){return $.ClassType.Exists$1.apply($.ClassType, arguments)}
   ,LoadFromStream$2$:function($){return $.ClassType.LoadFromStream$2.apply($.ClassType, arguments)}
   ,Read$3$:function($){return $.ClassType.Read$3.apply($.ClassType, arguments)}
   ,SaveToStream$2$:function($){return $.ClassType.SaveToStream$2.apply($.ClassType, arguments)}
   ,Write$3$:function($){return $.ClassType.Write$3.apply($.ClassType, arguments)}
};
TW3Structure.$Intf={
   IW3Structure:[TW3Structure.WriteString$2,TW3Structure.WriteInt,TW3Structure.WriteBool$1,TW3Structure.WriteFloat,TW3Structure.WriteDateTime$1,TW3Structure.ReadString$3,TW3Structure.ReadInt$1,TW3Structure.ReadBool$1,TW3Structure.ReadFloat$1,TW3Structure.ReadDateTime$2,TW3Structure.Read$3,TW3Structure.Write$3]
   ,IW3StructureWriteAccess:[TW3Structure.WriteString$2,TW3Structure.WriteInt,TW3Structure.WriteBool$1,TW3Structure.WriteFloat,TW3Structure.WriteDateTime$1,TW3Structure.Write$3,TW3Structure.WriteBytes$1,TW3Structure.WriteStream]
   ,IW3StructureReadAccess:[TW3Structure.Exists$1,TW3Structure.ReadString$3,TW3Structure.ReadInt$1,TW3Structure.ReadBool$1,TW3Structure.ReadFloat$1,TW3Structure.ReadDateTime$2,TW3Structure.Read$3,TW3Structure.ReadBytes$1]
}
/// EW3Structure = class (EW3Exception)
///  [line: 91, column: 3, file: System.Structure]
var EW3Structure = {
   $ClassName:"EW3Structure",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TJSONStructure = class (TW3Structure)
///  [line: 25, column: 3, file: System.Structure.JSON]
var TJSONStructure = {
   $ClassName:"TJSONStructure",$Parent:TW3Structure
   ,$Init:function ($) {
      TW3Structure.$Init($);
      $.FObj = null;
   }
   /// procedure TJSONStructure.Clear()
   ///  [line: 131, column: 26, file: System.Structure.JSON]
   ,Clear$2:function(Self) {
      TJSONObject.Clear$1(Self.FObj);
   }
   /// constructor TJSONStructure.Create()
   ///  [line: 59, column: 28, file: System.Structure.JSON]
   ,Create$54:function(Self) {
      TObject.Create(Self);
      Self.FObj = TJSONObject.Create$39($New(TJSONObject));
      return Self
   }
   /// destructor TJSONStructure.Destroy()
   ///  [line: 65, column: 27, file: System.Structure.JSON]
   ,Destroy:function(Self) {
      TObject.Free(Self.FObj);
      TObject.Destroy(Self);
   }
   /// function TJSONStructure.Exists(const Name: String) : Boolean
   ///  [line: 136, column: 25, file: System.Structure.JSON]
   ,Exists$1:function(Self, Name$19) {
      return TJSONObject.Exists(Self.FObj,Name$19);
   }
   /// procedure TJSONStructure.LoadFromStream(const Stream: TStream)
   ///  [line: 111, column: 26, file: System.Structure.JSON]
   ,LoadFromStream$2:function(Self, Stream$5) {
      TJSONObject.LoadFromStream(Self.FObj,Stream$5);
   }
   /// function TJSONStructure.Read(const Name: String) : Variant
   ///  [line: 146, column: 25, file: System.Structure.JSON]
   ,Read$3:function(Self, Name$20) {
      var Result = {v:undefined};
      try {
         if (TJSONObject.Exists(Self.FObj,Name$20)) {
            TJSONObject.Read$2(Self.FObj,Name$20,Result);
         } else {
            Result.v = undefined;
         }
      } finally {return Result.v}
   }
   /// procedure TJSONStructure.SaveToStream(const Stream: TStream)
   ///  [line: 106, column: 26, file: System.Structure.JSON]
   ,SaveToStream$2:function(Self, Stream$6) {
      TJSONObject.SaveToStream(Self.FObj,Stream$6);
   }
   /// function TJSONStructure.ToJSon() : String
   ///  [line: 76, column: 25, file: System.Structure.JSON]
   ,ToJSon:function(Self) {
      return TJSONObject.ToJSON(Self.FObj);
   }
   /// procedure TJSONStructure.Write(const Name: String; const Value: Variant)
   ///  [line: 141, column: 26, file: System.Structure.JSON]
   ,Write$3:function(Self, Name$21, Value$37) {
      TJSONObject.AddOrSet(Self.FObj,Name$21,Value$37);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Clear$2$:function($){return $.ClassType.Clear$2($)}
   ,Exists$1$:function($){return $.ClassType.Exists$1.apply($.ClassType, arguments)}
   ,LoadFromStream$2$:function($){return $.ClassType.LoadFromStream$2.apply($.ClassType, arguments)}
   ,Read$3$:function($){return $.ClassType.Read$3.apply($.ClassType, arguments)}
   ,SaveToStream$2$:function($){return $.ClassType.SaveToStream$2.apply($.ClassType, arguments)}
   ,Write$3$:function($){return $.ClassType.Write$3.apply($.ClassType, arguments)}
};
TJSONStructure.$Intf={
   IW3Structure:[TW3Structure.WriteString$2,TW3Structure.WriteInt,TW3Structure.WriteBool$1,TW3Structure.WriteFloat,TW3Structure.WriteDateTime$1,TW3Structure.ReadString$3,TW3Structure.ReadInt$1,TW3Structure.ReadBool$1,TW3Structure.ReadFloat$1,TW3Structure.ReadDateTime$2,TJSONStructure.Read$3,TJSONStructure.Write$3]
   ,IW3StructureWriteAccess:[TW3Structure.WriteString$2,TW3Structure.WriteInt,TW3Structure.WriteBool$1,TW3Structure.WriteFloat,TW3Structure.WriteDateTime$1,TJSONStructure.Write$3,TW3Structure.WriteBytes$1,TW3Structure.WriteStream]
   ,IW3StructureReadAccess:[TJSONStructure.Exists$1,TW3Structure.ReadString$3,TW3Structure.ReadInt$1,TW3Structure.ReadBool$1,TW3Structure.ReadFloat$1,TW3Structure.ReadDateTime$2,TJSONStructure.Read$3,TW3Structure.ReadBytes$1]
}
function util() {
   return require("util");
};
/// dgram_bindinfo = class (JObject)
///  [line: 24, column: 3, file: NodeJS.dgram]
function dgram_bindinfo() {
}
$Extend(Object,dgram_bindinfo,
   {
      "reuseAddr" : false,
      "port" : 0,
      "address" : "",
      "exclusive" : false
   });

function dgram() {
   return require("dgram");
};
/// JServerAddressResult = class (TObject)
///  [line: 64, column: 3, file: NodeJS.net]
var JServerAddressResult = {
   $ClassName:"JServerAddressResult",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TNJAddressBindings = class (TW3LockedObject)
///  [line: 78, column: 3, file: SmartNJ.Network.Bindings]
var TNJAddressBindings = {
   $ClassName:"TNJAddressBindings",$Parent:TW3LockedObject
   ,$Init:function ($) {
      TW3LockedObject.$Init($);
      $.FItems = [];
   }
   /// procedure TNJAddressBindings.Clear()
   ///  [line: 172, column: 30, file: SmartNJ.Network.Bindings]
   ,Clear$4:function(Self) {
      var x$21 = 0;
      if (TW3LockedObject.GetLockState$1(Self)) {
         throw Exception.Create($New(EW3LockError),"Bindings cannot be altered while active error");
      } else {
         try {
            var $temp31;
            for(x$21=0,$temp31=Self.FItems.length;x$21<$temp31;x$21++) {
               TObject.Free(Self.FItems[x$21]);
               Self.FItems[x$21]=null;
            }
         } finally {
            Self.FItems.length=0;
         }
      }
   }
   /// destructor TNJAddressBindings.Destroy()
   ///  [line: 114, column: 31, file: SmartNJ.Network.Bindings]
   ,Destroy:function(Self) {
      if (Self.FItems.length>0) {
         TNJAddressBindings.Clear$4(Self);
      }
      TObject.Destroy(Self);
   }
   /// procedure TNJAddressBindings.ObjectLocked()
   ///  [line: 243, column: 30, file: SmartNJ.Network.Bindings]
   ,ObjectLocked$1:function(Self) {
      var x$22 = 0;
      var LAccess = null;
      var $temp32;
      for(x$22=0,$temp32=Self.FItems.length;x$22<$temp32;x$22++) {
         LAccess = $AsIntf(Self.FItems[x$22],"IW3LockObject");
         LAccess[0]();
      }
   }
   /// procedure TNJAddressBindings.ObjectUnLocked()
   ///  [line: 252, column: 30, file: SmartNJ.Network.Bindings]
   ,ObjectUnLocked$1:function(Self) {
      var x$23 = 0;
      var LAccess$1 = null;
      var $temp33;
      for(x$23=0,$temp33=Self.FItems.length;x$23<$temp33;x$23++) {
         LAccess$1 = $AsIntf(Self.FItems[x$23],"IW3LockObject");
         LAccess$1[1]();
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,ObjectLocked$1$:function($){return $.ClassType.ObjectLocked$1($)}
   ,ObjectUnLocked$1$:function($){return $.ClassType.ObjectUnLocked$1($)}
};
TNJAddressBindings.$Intf={
   IW3LockObject:[TW3LockedObject.DisableAlteration$1,TW3LockedObject.EnableAlteration$1,TW3LockedObject.GetLockState$1]
}
/// TNJAddressBinding = class (TW3OwnedLockedObject)
///  [line: 42, column: 3, file: SmartNJ.Network.Bindings]
var TNJAddressBinding = {
   $ClassName:"TNJAddressBinding",$Parent:TW3OwnedLockedObject
   ,$Init:function ($) {
      TW3OwnedLockedObject.$Init($);
   }
   /// function TNJAddressBinding.AcceptOwner(const CandidateObject: TObject) : Boolean
   ///  [line: 270, column: 28, file: SmartNJ.Network.Bindings]
   ,AcceptOwner:function(Self, CandidateObject$2) {
      return (CandidateObject$2!==null)&&$Is(CandidateObject$2,TNJAddressBindings);
   }
   /// constructor TNJAddressBinding.Create(const AOwner: TNJAddressBindings)
   ///  [line: 265, column: 31, file: SmartNJ.Network.Bindings]
   ,Create$64:function(Self, AOwner$5) {
      TW3OwnedObject.Create$18(Self,AOwner$5);
      return Self
   }
   ,Destroy:TObject.Destroy
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$18:TW3OwnedObject.Create$18
};
TNJAddressBinding.$Intf={
   IW3OwnedObjectAccess:[TNJAddressBinding.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// TNJCustomServer = class (TW3ErrorObject)
///  [line: 57, column: 3, file: SmartNJ.Server]
var TNJCustomServer = {
   $ClassName:"TNJCustomServer",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.OnBeforeServerStopped = null;
      $.OnBeforeServerStarted = null;
      $.OnAfterServerStopped = null;
      $.OnAfterServerStarted = null;
      $.FActive$3 = false;
      $.FHandle$4 = undefined;
      $.FPort$2 = 0;
   }
   /// procedure TNJCustomServer.AfterStart()
   ///  [line: 164, column: 27, file: SmartNJ.Server]
   ,AfterStart$1:function(Self) {
      if (Self.OnAfterServerStarted) {
         Self.OnAfterServerStarted(Self);
      }
   }
   /// procedure TNJCustomServer.AfterStop()
   ///  [line: 170, column: 27, file: SmartNJ.Server]
   ,AfterStop$1:function(Self) {
      if (Self.OnAfterServerStopped) {
         Self.OnAfterServerStopped(Self);
      }
   }
   /// procedure TNJCustomServer.BeforeStart()
   ///  [line: 158, column: 27, file: SmartNJ.Server]
   ,BeforeStart$1:function(Self) {
      if (Self.OnBeforeServerStarted) {
         Self.OnBeforeServerStarted(Self);
      }
   }
   /// procedure TNJCustomServer.BeforeStop()
   ///  [line: 152, column: 27, file: SmartNJ.Server]
   ,BeforeStop$1:function(Self) {
      if (Self.OnBeforeServerStopped) {
         Self.OnBeforeServerStopped(Self);
      }
   }
   /// function TNJCustomServer.GetActive() : Boolean
   ///  [line: 192, column: 26, file: SmartNJ.Server]
   ,GetActive$2:function(Self) {
      return Self.FActive$3;
   }
   /// function TNJCustomServer.GetHandle() : THandle
   ///  [line: 202, column: 26, file: SmartNJ.Server]
   ,GetHandle$1:function(Self) {
      return Self.FHandle$4;
   }
   /// function TNJCustomServer.GetPort() : Integer
   ///  [line: 176, column: 26, file: SmartNJ.Server]
   ,GetPort$1:function(Self) {
      return Self.FPort$2;
   }
   /// procedure TNJCustomServer.SetActive(const Value: Boolean)
   ///  [line: 197, column: 27, file: SmartNJ.Server]
   ,SetActive$3:function(Self, Value$38) {
      Self.FActive$3 = Value$38;
   }
   /// procedure TNJCustomServer.SetHandle(const Value: THandle)
   ///  [line: 207, column: 27, file: SmartNJ.Server]
   ,SetHandle:function(Self, Value$39) {
      Self.FHandle$4 = Value$39;
   }
   /// procedure TNJCustomServer.SetPort(const Value: Integer)
   ///  [line: 181, column: 27, file: SmartNJ.Server]
   ,SetPort$2:function(Self, Value$40) {
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (TNJCustomServer.GetActive$2(Self)) {
         TW3ErrorObject.SetLastError$1(Self,"Port cannot be altered while server is active error");
      } else {
         Self.FPort$2 = Value$40;
      }
   }
   /// procedure TNJCustomServer.StartServer()
   ///  [line: 142, column: 27, file: SmartNJ.Server]
   ,StartServer:function(Self) {
      TNJCustomServer.SetActive$3$(Self,true);
   }
   /// procedure TNJCustomServer.StopServer()
   ///  [line: 147, column: 27, file: SmartNJ.Server]
   ,StopServer:function(Self) {
      TNJCustomServer.SetActive$3$(Self,false);
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$38:TW3ErrorObject.Create$38
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetActive$3$:function($){return $.ClassType.SetActive$3.apply($.ClassType, arguments)}
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,StopServer$:function($){return $.ClassType.StopServer($)}
};
TNJCustomServer.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TNJServerChildClass = class (TW3HandleBasedObject)
///  [line: 43, column: 3, file: SmartNJ.Server]
var TNJServerChildClass = {
   $ClassName:"TNJServerChildClass",$Parent:TW3HandleBasedObject
   ,$Init:function ($) {
      TW3HandleBasedObject.$Init($);
      $.FParent = null;
   }
   /// constructor TNJServerChildClass.Create(Server: TNJCustomServer)
   ///  [line: 108, column: 33, file: SmartNJ.Server]
   ,Create$68:function(Self, Server$3) {
      TObject.Create(Self);
      Self.FParent = Server$3;
      return Self
   }
   ,Destroy:TObject.Destroy
};
/// TNJCustomServerResponse = class (TNJServerChildClass)
///  [line: 54, column: 3, file: SmartNJ.Server]
var TNJCustomServerResponse = {
   $ClassName:"TNJCustomServerResponse",$Parent:TNJServerChildClass
   ,$Init:function ($) {
      TNJServerChildClass.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TNJCustomServerRequest = class (TNJServerChildClass)
///  [line: 51, column: 3, file: SmartNJ.Server]
var TNJCustomServerRequest = {
   $ClassName:"TNJCustomServerRequest",$Parent:TNJServerChildClass
   ,$Init:function ($) {
      TNJServerChildClass.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// ENJServerError = class (EW3Exception)
///  [line: 36, column: 3, file: SmartNJ.Server]
var ENJServerError = {
   $ClassName:"ENJServerError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
function http() {
   return require("http");
};
/// TNJHTTPServer = class (TNJCustomServer)
///  [line: 101, column: 3, file: SmartNJ.Server.Http]
var TNJHTTPServer = {
   $ClassName:"TNJHTTPServer",$Parent:TNJCustomServer
   ,$Init:function ($) {
      TNJCustomServer.$Init($);
      $.OnRequest = null;
   }
   /// procedure TNJHTTPServer.Dispatch(request: JServerRequest; response: JServerResponse)
   ///  [line: 144, column: 25, file: SmartNJ.Server.Http]
   ,Dispatch$2:function(Self, request$1, response) {
      var LRequest = null,
         LResponse = null;
      if (Self.OnRequest) {
         LRequest = TNJHttpRequest.Create$70($New(TNJHttpRequest),Self,request$1);
         try {
            LResponse = TNJHttpResponse.Create$69($New(TNJHttpResponse),Self,response);
            try {
               try {
                  Self.OnRequest(Self,LRequest,LResponse);
               } catch ($e) {
                  var e$12 = $W($e);
                  throw EW3Exception.CreateFmt($New(ENJHttpServerError),"Dispatch failed, system threw exception %s with message [%s]",[TObject.ClassName(e$12.ClassType), e$12.FMessage]);
               }
            } finally {
               TObject.Free(LResponse);
            }
         } finally {
            TObject.Free(LRequest);
         }
      }
   }
   /// procedure TNJHTTPServer.InternalSetActive(const Value: Boolean)
   ///  [line: 205, column: 25, file: SmartNJ.Server.Http]
   ,InternalSetActive$1:function(Self, Value$41) {
      TNJCustomServer.SetActive$3(Self,Value$41);
   }
   /// procedure TNJHTTPServer.SetActive(const Value: Boolean)
   ///  [line: 124, column: 25, file: SmartNJ.Server.Http]
   ,SetActive$3:function(Self, Value$42) {
      if (Value$42!=TNJCustomServer.GetActive$2(Self)) {
         TNJCustomServer.SetActive$3(Self,Value$42);
         try {
            if (TNJCustomServer.GetActive$2(Self)) {
               TNJCustomServer.StartServer$(Self);
            } else {
               TNJCustomServer.StopServer$(Self);
            }
         } catch ($e) {
            var e$13 = $W($e);
            TNJCustomServer.SetActive$3(Self,(!Value$42))         }
      }
   }
   /// procedure TNJHTTPServer.StartServer()
   ///  [line: 171, column: 25, file: SmartNJ.Server.Http]
   ,StartServer:function(Self) {
      var LServer = null;
      try {
         LServer = http().createServer($Event2(Self,TNJHTTPServer.Dispatch$2));
      } catch ($e) {
         var e$14 = $W($e);
         throw EW3Exception.CreateFmt($New(ENJHttpServerError),"Failed to create NodeJS server object, system threw exception %s with message [%s]",[TObject.ClassName(e$14.ClassType), e$14.FMessage]);
      }
      try {
         LServer.listen(TNJCustomServer.GetPort$1(Self),"");
      } catch ($e) {
         var e$15 = $W($e);
         LServer = null;
         throw EW3Exception.CreateFmt($New(ENJHttpServerError),"Failed to start server, system threw exception %s with message %s",[TObject.ClassName(e$15.ClassType), e$15.FMessage]);
      }
      TNJCustomServer.SetHandle(Self,LServer);
      TNJCustomServer.AfterStart$1(Self);
   }
   /// procedure TNJHTTPServer.StopServer()
   ///  [line: 210, column: 25, file: SmartNJ.Server.Http]
   ,StopServer:function(Self) {
      var cb = null;
      cb = function () {
         TNJHTTPServer.InternalSetActive$1(Self,false);
         TNJCustomServer.AfterStop$1(Self);
      };
      TNJCustomServer.GetHandle$1(Self).close(cb);
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$38:TW3ErrorObject.Create$38
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetActive$3$:function($){return $.ClassType.SetActive$3.apply($.ClassType, arguments)}
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,StopServer$:function($){return $.ClassType.StopServer($)}
};
TNJHTTPServer.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TNJHttpResponse = class (TNJCustomServerResponse)
///  [line: 67, column: 3, file: SmartNJ.Server.Http]
var TNJHttpResponse = {
   $ClassName:"TNJHttpResponse",$Parent:TNJCustomServerResponse
   ,$Init:function ($) {
      TNJCustomServerResponse.$Init($);
   }
   /// constructor TNJHttpResponse.Create(const Server: TNJCustomServer; const ResponseObject: JServerResponse)
   ///  [line: 225, column: 29, file: SmartNJ.Server.Http]
   ,Create$69:function(Self, Server$4, ResponseObject) {
      TNJServerChildClass.Create$68(Self,Server$4);
      TW3HandleBasedObject.SetObjectHandle(Self,ResponseObject);
      return Self
   }
   ,Destroy:TObject.Destroy
};
/// TNJHttpRequest = class (TNJCustomServerRequest)
///  [line: 40, column: 3, file: SmartNJ.Server.Http]
var TNJHttpRequest = {
   $ClassName:"TNJHttpRequest",$Parent:TNJCustomServerRequest
   ,$Init:function ($) {
      TNJCustomServerRequest.$Init($);
   }
   /// constructor TNJHttpRequest.Create(const Server: TNJCustomServer; const RequestObject: JServerRequest)
   ///  [line: 317, column: 28, file: SmartNJ.Server.Http]
   ,Create$70:function(Self, Server$5, RequestObject) {
      TNJServerChildClass.Create$68(Self,Server$5);
      TW3HandleBasedObject.SetObjectHandle(Self,RequestObject);
      return Self
   }
   ,Destroy:TObject.Destroy
};
/// ENJHttpServerError = class (ENJServerError)
///  [line: 37, column: 3, file: SmartNJ.Server.Http]
var ENJHttpServerError = {
   $ClassName:"ENJHttpServerError",$Parent:ENJServerError
   ,$Init:function ($) {
      ENJServerError.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TNJWebSocketSocket = class (TW3ErrorObject)
///  [line: 67, column: 3, file: SmartNJ.Server.Websocket]
var TNJWebSocketSocket = {
   $ClassName:"TNJWebSocketSocket",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.RemoteAddress = "";
      $.FReq = $.FServer = $.FSocket = null;
   }
   /// constructor TNJWebSocketSocket.Create(const Server: TNJWebSocketServer; const WsSocket: JWsSocket)
   ///  [line: 591, column: 32, file: SmartNJ.Server.Websocket]
   ,Create$65:function(Self, Server$6, WsSocket) {
      TW3ErrorObject.Create$38(Self);
      if (Server$6!==null) {
         Self.FServer = Server$6;
         if (WsSocket!==null) {
            Self.FSocket = WsSocket;
            Self.FOptions$4.AutoWriteToConsole = true;
            Self.FOptions$4.ThrowExceptions = true;
            Self.FSocket["name"] = TW3Identifiers.GenerateUniqueObjectId(TW3Identifiers);
         } else {
            throw Exception.Create($New(Exception),"Failed to create socket, socket was nil error");
         }
      } else {
         throw Exception.Create($New(Exception),"Failed to create socket, server was nil error");
      }
      return Self
   }
   /// destructor TNJWebSocketSocket.Destroy()
   ///  [line: 613, column: 31, file: SmartNJ.Server.Websocket]
   ,Destroy:function(Self) {
      if (Self.FReq!==null) {
         TObject.Free(Self.FReq);
      }
      Self.FServer = null;
      Self.FSocket = null;
      TW3ErrorObject.Destroy(Self);
   }
   /// function TNJWebSocketSocket.GetExceptionClass() : TW3ExceptionClass
   ///  [line: 623, column: 30, file: SmartNJ.Server.Websocket]
   ,GetExceptionClass:function(Self) {
      return ENJWebSocketSocket;
   }
   /// function TNJWebSocketSocket.GetSocketName() : String
   ///  [line: 711, column: 29, file: SmartNJ.Server.Websocket]
   ,GetSocketName:function(Self) {
      return String(Self.FSocket["name"]);
   }
   /// procedure TNJWebSocketSocket.SetSocketName(NewName: String)
   ///  [line: 667, column: 30, file: SmartNJ.Server.Websocket]
   ,SetSocketName:function(Self, NewName) {
      var OldName = "";
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      NewName = Trim$_String_(NewName);
      OldName = TNJWebSocketSocket.GetSocketName(Self);
      if (NewName!=OldName) {
         if (NewName.length>0) {
            Self.FSocket["name"] = Trim$_String_(NewName);
            TNJWebSocketServer.DoClientNameChange(Self.FServer,Self,OldName);
         } else {
            TW3ErrorObject.SetLastError$1(Self,"Invalid socket rename, string was empty error");
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$38:TW3ErrorObject.Create$38
   ,GetExceptionClass$:function($){return $.ClassType.GetExceptionClass($)}
};
TNJWebSocketSocket.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TNJWebSocketServer = class (TNJCustomServer)
///  [line: 132, column: 3, file: SmartNJ.Server.Websocket]
var TNJWebSocketServer = {
   $ClassName:"TNJWebSocketServer",$Parent:TNJCustomServer
   ,$Init:function ($) {
      TNJCustomServer.$Init($);
      $.OnBinMessage = null;
      $.OnTextMessage = null;
      $.OnClientDisconnected = null;
      $.OnClientConnected = null;
      $.OnError = null;
      $.FPath = "";
      $.FPayload = 0;
      $.FSocketLUT = null;
      $.FSockets = [];
      $.FTrack = false;
   }
   /// constructor TNJWebSocketServer.Create()
   ///  [line: 214, column: 32, file: SmartNJ.Server.Websocket]
   ,Create$38:function(Self) {
      TW3ErrorObject.Create$38(Self);
      Self.FSocketLUT = TW3CustomDictionary.Create$67($New(TW3ObjDictionary));
      Self.FOptions$4.AutoWriteToConsole = true;
      Self.FOptions$4.ThrowExceptions = true;
      Self.FPayload = 41943040;
      return Self
   }
   /// destructor TNJWebSocketServer.Destroy()
   ///  [line: 226, column: 31, file: SmartNJ.Server.Websocket]
   ,Destroy:function(Self) {
      TObject.Free(Self.FSocketLUT);
      TW3ErrorObject.Destroy(Self);
   }
   /// procedure TNJWebSocketServer.Dispatch(Socket: TNJWebSocketSocket; Data: TNJWebsocketMessage)
   ///  [line: 313, column: 30, file: SmartNJ.Server.Websocket]
   ,Dispatch$1:function(Self, Socket$3, Data$33) {
      switch (Data$33.wiType) {
         case 0 :
            if (Self.OnTextMessage) {
               Self.OnTextMessage(Self,Socket$3,Clone$TNJWebsocketMessage(Data$33));
            }
            break;
         case 1 :
            if (Self.OnBinMessage) {
               Self.OnBinMessage(Self,Socket$3,Clone$TNJWebsocketMessage(Data$33));
            }
            break;
      }
   }
   /// procedure TNJWebSocketServer.DoClientConnected(Socket: THandle; Request: THandle)
   ///  [line: 400, column: 30, file: SmartNJ.Server.Websocket]
   ,DoClientConnected:function(Self, Socket$4, Request$1) {
      var NJSocket = null;
      NJSocket = TNJWebSocketSocket.Create$65($New(TNJWebSocketSocket),Self,Socket$4);
      TW3ObjDictionary.a$136(Self.FSocketLUT,TNJWebSocketSocket.GetSocketName(NJSocket),NJSocket);
      Self.FSockets.push(NJSocket);
      if (Request$1) {
         if (Request$1.connection) {
            NJSocket.RemoteAddress = String(Request$1.connection.remoteAddress);
         }
      }
      if (Self.OnClientConnected) {
         Self.OnClientConnected(Self,NJSocket);
      }
      Socket$4.on("error",function (error$3) {
         var NJSocket$1 = null,
            SocketName;
         if (Self.OnError) {
            NJSocket$1 = null;
            SocketName = Socket$4["name"];
            if (TW3CustomDictionary.Contains(Self.FSocketLUT,(String(SocketName)))) {
               NJSocket$1 = $As(TW3ObjDictionary.a$137(Self.FSocketLUT,(String(SocketName))),TNJWebSocketSocket);
            } else {
               NJSocket$1 = TNJWebSocketSocket.Create$65($New(TNJWebSocketSocket),Self,Socket$4);
            }
            Self.OnError(Self,NJSocket$1,error$3);
         }
      });
      Socket$4.on("message",function (message$1) {
         var LInfo = {wiType:0,wiBuffer:null,wiText:""};
         var NJSocket$2 = null,
            SocketName$1;
         NJSocket$2 = null;
         if (TW3VariantHelper$IsUInt8Array(message$1)) {
            LInfo.wiType = 1;
            LInfo.wiBuffer = message$1;
         } else {
            LInfo.wiType = 0;
            LInfo.wiText = String(message$1);
         }
         SocketName$1 = Socket$4["name"];
         if (TW3CustomDictionary.Contains(Self.FSocketLUT,(String(SocketName$1)))) {
            NJSocket$2 = $As(TW3ObjDictionary.a$137(Self.FSocketLUT,(String(SocketName$1))),TNJWebSocketSocket);
         } else {
            NJSocket$2 = TNJWebSocketSocket.Create$65($New(TNJWebSocketSocket),Self,Socket$4);
         }
         TNJWebSocketServer.Dispatch$1(Self,NJSocket$2,Clone$TNJWebsocketMessage(LInfo));
      });
      Socket$4.on("close",function (code$1, reason) {
         TNJWebSocketServer.DoClientDisconnected(Self,Socket$4,code$1,reason);
      });
   }
   /// procedure TNJWebSocketServer.DoClientDisconnected(Socket: THandle; Code: Integer; Reason: String)
   ///  [line: 473, column: 30, file: SmartNJ.Server.Websocket]
   ,DoClientDisconnected:function(Self, Socket$5, Code, Reason) {
      var RawSocket$1 = null,
         SocketId = "",
         NJSocket$3 = null,
         index$4 = 0;
      RawSocket$1 = Socket$5;
      SocketId = TVariant.AsString(Socket$5["name"]);
      NJSocket$3 = null;
      if (SocketId.length>0) {
         if (TW3CustomDictionary.Contains(Self.FSocketLUT,SocketId)) {
            WriteLn("Found in name lookup!");
            NJSocket$3 = $As(TW3ObjDictionary.a$137(Self.FSocketLUT,SocketId),TNJWebSocketSocket);
         }
      }
      try {
         if (Self.OnClientDisconnected) {
            Self.OnClientDisconnected(Self,NJSocket$3);
         }
      } finally {
         if (NJSocket$3!==null) {
            index$4 = Self.FSockets.indexOf(NJSocket$3);
            Self.FSockets.splice(index$4,1)
            ;
            TW3CustomDictionary.Delete$3(Self.FSocketLUT,SocketId);
            TObject.Free(NJSocket$3);
         }
      }
   }
   /// procedure TNJWebSocketServer.DoClientNameChange(Client: TNJWebSocketSocket; OldName: String)
   ///  [line: 372, column: 30, file: SmartNJ.Server.Websocket]
   ,DoClientNameChange:function(Self, Client, OldName$1) {
      var NewName$1 = "";
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      NewName$1 = Trim$_String_(TNJWebSocketSocket.GetSocketName(Client));
      if (NewName$1.length>0) {
         if (TW3CustomDictionary.Contains(Self.FSocketLUT,NewName$1)) {
            TW3ErrorObject.SetLastErrorF$1(Self,"Invalid socket rename, a socket with that name [%s] already exists",[NewName$1]);
            return;
         }
         OldName$1 = Trim$_String_(OldName$1);
         if (OldName$1.length>0) {
            if (TW3CustomDictionary.Contains(Self.FSocketLUT,OldName$1)) {
               TW3CustomDictionary.Delete$3(Self.FSocketLUT,OldName$1);
            }
         }
         TW3ObjDictionary.a$136(Self.FSocketLUT,TNJWebSocketSocket.GetSocketName(Client),Client);
      } else {
         TW3ErrorObject.SetLastError$1(Self,"Invalid socket rename, string was empty errror");
      }
   }
   /// function TNJWebSocketServer.GetExceptionClass() : TW3ExceptionClass
   ///  [line: 232, column: 29, file: SmartNJ.Server.Websocket]
   ,GetExceptionClass:function(Self) {
      return ENJWebSocketServer;
   }
   /// procedure TNJWebSocketServer.InternalSetActive(const Value: Boolean)
   ///  [line: 515, column: 30, file: SmartNJ.Server.Websocket]
   ,InternalSetActive:function(Self, Value$43) {
      TNJCustomServer.SetActive$3(Self,Value$43);
   }
   /// procedure TNJWebSocketServer.SetActive(const Value: Boolean)
   ///  [line: 287, column: 30, file: SmartNJ.Server.Websocket]
   ,SetActive$3:function(Self, Value$44) {
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      if (Value$44!=TNJCustomServer.GetActive$2(Self)) {
         TNJCustomServer.SetActive$3(Self,Value$44);
         try {
            if (TNJCustomServer.GetActive$2(Self)) {
               TNJCustomServer.StartServer$(Self);
            } else {
               TNJCustomServer.StopServer$(Self);
            }
         } catch ($e) {
            var e$16 = $W($e);
            TNJCustomServer.SetActive$3(Self,(!Value$44));
            TW3ErrorObject.SetLastErrorF$1(Self,"Failed to start server, system threw exception %s with message [%s]",[TObject.ClassName(e$16.ClassType), e$16.FMessage]);
         }
      }
   }
   /// procedure TNJWebSocketServer.StartServer()
   ///  [line: 525, column: 30, file: SmartNJ.Server.Websocket]
   ,StartServer:function(Self) {
      var LOptions,
         LServer$1;
      if (Self.FOptions$4.AutoResetError) {
         TW3ErrorObject.ClearLastError$1(Self);
      }
      TW3CustomDictionary.Clear$5(Self.FSocketLUT);
      LOptions = TVariant.CreateObject();
      LOptions["clientTracking "] = Self.FTrack;
      if (Self.FPath.length>0) {
         LOptions["path"] = Self.FPath;
      }
      LOptions["port"] = TNJCustomServer.GetPort$1(Self);
      LOptions["maxPayload"] = Self.FPayload;
      LServer$1 = null;
      try {
         LServer$1 = WebSocketCreateServer(LOptions);
      } catch ($e) {
         var e$17 = $W($e);
         TW3ErrorObject.SetLastErrorF$1(Self,"Failed to create websocket server object, system threw exception %s with message [%s]",[TObject.ClassName(e$17.ClassType), e$17.FMessage]);
         return;
      }
      TNJCustomServer.SetHandle(Self,LServer$1);
      LServer$1.on("error",function (error$4) {
         if (Self.OnError) {
            Self.OnError(Self,null,error$4);
         }
      });
      LServer$1.on("connection",$Event2(Self,TNJWebSocketServer.DoClientConnected));
      TNJCustomServer.AfterStart$1(Self);
   }
   /// procedure TNJWebSocketServer.StopServer()
   ///  [line: 573, column: 30, file: SmartNJ.Server.Websocket]
   ,StopServer:function(Self) {
      var LHandle = undefined;
      LHandle = TNJCustomServer.GetHandle$1(Self);
      if (LHandle) {
         LHandle.close(function () {
            TNJWebSocketServer.InternalSetActive(Self,false);
            TNJCustomServer.AfterStop$1(Self);
            TW3CustomDictionary.Clear$5(Self.FSocketLUT);
         });
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$38$:function($){return $.ClassType.Create$38($)}
   ,GetExceptionClass$:function($){return $.ClassType.GetExceptionClass($)}
   ,SetActive$3$:function($){return $.ClassType.SetActive$3.apply($.ClassType, arguments)}
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,StopServer$:function($){return $.ClassType.StopServer($)}
};
TNJWebSocketServer.$Intf={
   IW3ErrorObject:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.SetLastErrorF$1,TW3ErrorObject.SetLastError$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed$1,TW3ErrorObject.GetLastError$1,TW3ErrorObject.ClearLastError$1]
}
/// TNJWebsocketMessageType enumeration
///  [line: 52, column: 3, file: SmartNJ.Server.Websocket]
var TNJWebsocketMessageType = [ "mtText", "mtBinary" ];
/// TNJWebsocketMessage = record
///  [line: 61, column: 3, file: SmartNJ.Server.Websocket]
function Copy$TNJWebsocketMessage(s,d) {
   d.wiType=s.wiType;
   d.wiBuffer=s.wiBuffer;
   d.wiText=s.wiText;
   return d;
}
function Clone$TNJWebsocketMessage($) {
   return {
      wiType:$.wiType,
      wiBuffer:$.wiBuffer,
      wiText:$.wiText
   }
}
/// JWsReadyState enumeration
///  [line: 54, column: 3, file: SmartNJ.Server.Websocket]
var JWsReadyState = [ "rsConnecting", "rsOpen", "rsClosing", "rsClosed" ];
/// ENJWebSocketSocket = class (EW3Exception)
///  [line: 45, column: 3, file: SmartNJ.Server.Websocket]
var ENJWebSocketSocket = {
   $ClassName:"ENJWebSocketSocket",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// ENJWebSocketServer = class (EW3Exception)
///  [line: 46, column: 3, file: SmartNJ.Server.Websocket]
var ENJWebSocketServer = {
   $ClassName:"ENJWebSocketServer",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// TW3CustomDictionary = class (TObject)
///  [line: 23, column: 3, file: System.Dictionaries]
var TW3CustomDictionary = {
   $ClassName:"TW3CustomDictionary",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FLUT = undefined;
   }
   /// procedure TW3CustomDictionary.Clear()
   ///  [line: 105, column: 31, file: System.Dictionaries]
   ,Clear$5:function(Self) {
      Self.FLUT = TVariant.CreateObject();
   }
   /// function TW3CustomDictionary.Contains(const ItemKey: String) : Boolean
   ///  [line: 165, column: 30, file: System.Dictionaries]
   ,Contains:function(Self, ItemKey) {
      var Result = false;
      if (ItemKey.length>0) {
         Result = (Self.FLUT.hasOwnProperty(ItemKey)?true:false);
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.Contains", TObject.ClassName(Self.ClassType), $R[23]]);
      }
      return Result
   }
   /// constructor TW3CustomDictionary.Create()
   ///  [line: 92, column: 33, file: System.Dictionaries]
   ,Create$67:function(Self) {
      TObject.Create(Self);
      Self.FLUT = TVariant.CreateObject();
      return Self
   }
   /// procedure TW3CustomDictionary.Delete(const ItemKey: String)
   ///  [line: 230, column: 31, file: System.Dictionaries]
   ,Delete$3:function(Self, ItemKey$1) {
      if (ItemKey$1.length>0) {
         try {
            delete (Self.FLUT[ItemKey$1]);
         } catch ($e) {
            var e$18 = $W($e);
            throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.Delete", TObject.ClassName(Self.ClassType), e$18.FMessage]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.Delete", TObject.ClassName(Self.ClassType), $R[23]]);
      }
   }
   /// destructor TW3CustomDictionary.Destroy()
   ///  [line: 98, column: 32, file: System.Dictionaries]
   ,Destroy:function(Self) {
      TW3CustomDictionary.Clear$5(Self);
      Self.FLUT = undefined;
      TObject.Destroy(Self);
   }
   /// function TW3CustomDictionary.GetItem(const ItemKey: String) : Variant
   ///  [line: 198, column: 30, file: System.Dictionaries]
   ,GetItem$1:function(Self, ItemKey$2) {
      var Result = undefined;
      if (ItemKey$2.length>0) {
         try {
            Result = Self.FLUT[ItemKey$2];
         } catch ($e) {
            var e$19 = $W($e);
            throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.GetItem", TObject.ClassName(Self.ClassType), e$19.FMessage]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.GetItem", TObject.ClassName(Self.ClassType), $R[23]]);
      }
      return Result
   }
   /// procedure TW3CustomDictionary.SetItem(const ItemKey: String; const KeyValue: Variant)
   ///  [line: 214, column: 31, file: System.Dictionaries]
   ,SetItem:function(Self, ItemKey$3, KeyValue) {
      if (ItemKey$3.length>0) {
         try {
            Self.FLUT[ItemKey$3] = KeyValue;
         } catch ($e) {
            var e$20 = $W($e);
            throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.SetItem", TObject.ClassName(Self.ClassType), e$20.FMessage]);
         }
      } else {
         throw EW3Exception.CreateFmt($New(EW3Exception),$R[0],["TW3CustomDictionary.SetItem", TObject.ClassName(Self.ClassType), $R[23]]);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// TW3ObjDictionary = class (TW3CustomDictionary)
///  [line: 46, column: 3, file: System.Dictionaries]
var TW3ObjDictionary = {
   $ClassName:"TW3ObjDictionary",$Parent:TW3CustomDictionary
   ,$Init:function ($) {
      TW3CustomDictionary.$Init($);
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 49, column: 13, file: System.Dictionaries]
   ,a$137:function(Self, ItemKey$4) {
      return TVariant.AsObject(TW3CustomDictionary.GetItem$1(Self,ItemKey$4));
   }
   /// anonymous TSourceMethodSymbol
   ///  [line: 50, column: 13, file: System.Dictionaries]
   ,a$136:function(Self, ItemKey$5, Value$45) {
      TW3CustomDictionary.SetItem(Self,ItemKey$5,Value$45);
   }
   ,Destroy:TW3CustomDictionary.Destroy
};
function WebSocketCreateServer(Options$5) {
   var Result = undefined;
   var Access = null;
   Access = WebSocketAPI();
   Result = new (Access).Server(Options$5);
   return Result
};
function WebSocketAPI() {
   return require("ws");
};
/// JWsVerifyClientInfo = class (TObject)
///  [line: 44, column: 3, file: Nodejs.websocket]
var JWsVerifyClientInfo = {
   $ClassName:"JWsVerifyClientInfo",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.origin = "";
      $.req = null;
      $.secure = false;
   }
   ,Destroy:TObject.Destroy
};
var __CSUniqueId = 0;
var __UNIQUE = 0;
var a$10 = 0;
var a$13 = 0;
var a$12 = 0;
var a$11 = 0;
var a$14 = 0;
var a$15 = 0;
var a$24 = null;
var CRC_Table_Ready = false;
var CRC_Table = function () {
      for (var r=[],i=0; i<513; i++) r.push(0);
      return r
   }();
var a$182 = null;
var NodeProgram = null,
   _UDPVersionStr = ["","",""];
var _UDPVersionStr = ["udp4", "udp6", "udp7"];
var __App = null;
var __Parser = null;
var __RESERVED = [];
var __RESERVED = ["$ClassName", "$Parent", "$Init", "toString", "toLocaleString", "valueOf", "indexOf", "hasOwnProperty", "isPrototypeOf", "propertyIsEnumerable", "constructor", "destructor"].slice();
var __Def_Converter = null;
var __CONV_BUFFER = null;
var __CONV_VIEW = null;
var __CONV_ARRAY = null;
var __SIZES = [0,0,0,0,0,0,0,0,0,0,0],
   _NAMES = ["","","","","","","","","","",""],
   __B64_Lookup = function () {
      for (var r=[],i=0; i<257; i++) r.push("");
      return r
   }(),
   __B64_RevLookup = function () {
      for (var r=[],i=0; i<257; i++) r.push(0);
      return r
   }(),
   CNT_B64_CHARSET = "";
var __SIZES = [0, 1, 1, 2, 2, 4, 2, 4, 4, 8, 8];
var _NAMES = ["Unknown", "Boolean", "Byte", "Char", "Word", "Longword", "Smallint", "Integer", "Single", "Double", "String"];
var CNT_B64_CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+\/";
var __UniqueNumber = 0;
var __TYPE_MAP = {Boolean:undefined,Number$1:undefined,String$1:undefined,Object$2:undefined,Undefined:undefined,Function$1:undefined};
var pre_binary = [0,0],
   pre_OnOff = ["",""],
   pre_YesNo = ["",""],
   pre_StartStop = ["",""],
   pre_RunPause = ["",""];
var pre_binary = [0, 1];
var pre_OnOff = ["off", "on"];
var pre_YesNo = ["no", "yes"];
var pre_StartStop = ["stop", "start"];
var pre_RunPause = ["paused", "running"];
SetupTypeLUT();
var __Def_Converter = TDataTypeConverter.Create$4$($New(TDataTypeConverter));
SetupConversionLUT();
SetupBase64();
if (typeof btoa === 'undefined') {
      global.btoa = function (str) {
        return new Buffer(str).toString('base64');
      };
    }

    if (typeof atob === 'undefined') {
      global.atob = function (b64Encoded) {
        return new Buffer(b64Encoded, 'base64').toString();
      };
    }
;
switch (GetPlatform()) {
   case 1 :
      InstallDirectoryParser(TW3ErrorObject.Create$38$($New(TW3Win32DirectoryParser)));
      break;
   case 2 :
      InstallDirectoryParser(TW3ErrorObject.Create$38$($New(TW3PosixDirectoryParser)));
      break;
   default :
      throw Exception.Create($New(Exception),"Unsupported OS, no directory-parser for platform error");
}
;
var $Application = function() {
   NodeProgram = TNodeProgram.Create$3($New(TNodeProgram));
   TNodeProgram.Execute(NodeProgram);
}
$Application();

