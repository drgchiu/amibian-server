var $R = [
	"Method %s in class %s threw exception [%s]",
	"Procedure %s threw exception [%s]",
	"Host classtype was rejected as unsuitable",
	"Invalid handle for operation, reference was null error",
	"Invalid stream style for operation, expected memorystream",
	"Method not implemented",
	"stream operation failed, system threw exception: %s",
	"write failed, system threw exception: %s",
	"read failed, system threw exception: %s",
	"operation failed, invalid handle error",
	"Invalid length, %s bytes exceeds storage medium error",
	"Read failed, invalid signature error [%s]",
	"Invalid length, %s bytes exceeds storage boundaries error",
	"Write failed, invalid signature error [%s]",
	"Write failed, invalid datasize [%d] error",
	"File operation [%s] failed, system threw exception: %s",
	"Structure %s error, method %s.%s threw exception [%s] error",
	"Structure storage failed, structure contains function reference error",
	"Structure storage failed, structure contains symbol reference error",
	"Structure storage failed, structure contains uknown datatype error",
	"Failed to read structure, method %s.%s threw exception [%s] error",
	"Failed to write structure, method %s.%s threw exception [%s] error",
	"Structure data contains invalid or damaged data signature error",
	"Failed to register message, classtype was nil error",
	"Failed to register message, handler as nil error",
	"Failed to register message, a handler for that message already exists error",
	"Failed to write value, property [%s] not found error",
	"Failed to read value, property [%s] not found error",
	"Failed to locate object, property [%s] not found error",
	"Database not active error",
	"Prepare failed, database was nil error",
	"Prepare failed, database not active error",
	"Failed to alter value, field is read-only error",
	"Operation failed, dataset is not active error",
	"Invalid state for operation error",
	"Failed to match field class for datatype error",
	"Field definition cannot be altered in a live dataset error",
	"Failed to read field value, invalid field signature error",
	"A field with that name already exists error",
	"Failed to add field definition, invalid name error",
	"Delete operation failed, reference was NIL error",
	"Read of field-definitions failed, invalid header signature error",
	"Loading dataset failed, invalid signature error",
	"Loading dataset failed, invalid header identity error",
	"Failed to store dataset, system threw exception: %s",
	"Failed to load dataset, system threw exception: %s",
	"Failed to load dataset, invalid header signature error",
	"Failed to load dataset, invalid source error",
	"Failed to create dataset, no field definitions error",
	"Delete failed, dataset is empty error",
	"Delete failed, misaligned RecNo [%s]",
	"Unknown field datatype, table structure is corrupt or damaged error",
	"Invalid table structure, expected field signature error",
	"Failed to read dataset fields, invalid signature error",
	"Unknown datatype, binary extraction failed error",
	"Buffer is empty error",
	"Offset at BOF error",
	"Offset at EOF error",
	"Comment not closed error",
	"Expected EOF error",
	"Invalid length error",
	"Read failed, invalid offset [%d], expected %d..%d",
	"Write operation failed, target stream is nil error",
	"Read operation failed, source stream is nil error",
	"'Invalid handle for object (%s), reference rejected error",
	"Invalid dictionary key",
	"Invalid array of dictionary keys",
	"Failed to convert typed-array: expected %d bytes, read %d. Insufficient data error",
	"Failed to process data, reference value was nil or unassigned error",
	"0123456789",
	"0123456789ABCDEF"];
function Trunc(v) { return (v>=0)?Math.floor(v):Math.ceil(v) }
function Trim$_String_(s) { return s.replace(/^\s\s*/, "").replace(/\s\s*$/, "") }
var TObject={
	$ClassName: "TObject",
	$Parent: null,
	ClassName: function (s) { return s.$ClassName },
	ClassType: function (s) { return s },
	ClassParent: function (s) { return s.$Parent },
	$Init: function (s) {},
	Create: function (s) { return s },
	Destroy: function (s) { for (var prop in s) if (s.hasOwnProperty(prop)) delete s[prop] },
	Destroy$: function(s) { return s.ClassType.Destroy(s) },
	Free: function (s) { if (s!==null) s.ClassType.Destroy(s) }
}
function TimeToStr(v) { return FormatDateTime($fmt.LongTimeFormat, v) }
function StrReplace(s,o,n) { return o?s.replace(new RegExp(StrRegExp(o), "g"), n):s }
function StrRegExp(s) { return s.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&") }
function StrEndsWith(s,e) { return s.substr(s.length-e.length)==e }
function StrBeginsWith(s,b) { return s.substr(0, b.length)==b }
function SetLength(s,n) { if (s.v.length>n) s.v=s.v.substring(0,n);else while (s.v.length<n) s.v+=" "; }
function SameText(a,b) { return a.toUpperCase()==b.toUpperCase() }
function RightStr(s,n) { return s.substr(s.length-n) }
function Now() { var d=new Date(); var utcnow=d.getTime()/864e5+25569; var dt = new Date(); var n = dt.getTimezoneOffset(); return utcnow-n/1440 }
function IsLeapYear(y) { return !(y % 4) && (y % 100) || !(y % 400) ? true : false; }
function IntToHex2(v) { var r=v.toString(16); return (r.length==1)?"0"+r:r }
function IntToHex(v,d) { var r=v.toString(16); return "00000000".substr(0, d-r.length)+r }
function FormatDateTime(fmt, v) {
   function pad2(v) { return (v<10)?"0"+v:v; };
   function pad3(v) { return (v<100)?"0"+pad2(v):v; };
   var res="", i=0, c, tok, wasHour, prevWasHour=0, p, dt;
   v = Math.round((v-25569)*864e5);
   dt = new Date(v);
   if (!(dt instanceof Date && isFinite(dt))) return "Invalid Date";
   while (i<fmt.length) {
      c=fmt.charAt(i);
      tok="";
      do {
         tok+=c;
         i++;
      } while (fmt.charAt(i)===c);
	  wasHour=0;
      switch (tok.toLowerCase()) {
         case "d": res+=(dt.getUTCDate()); break;
         case "dd": res+=pad2(dt.getUTCDate()); break;
         case "ddd": res+=$fmt.ShortDayNames[dt.getUTCDay()]; break;
         case "dddd": res+=$fmt.LongDayNames[dt.getUTCDay()]; break;
         case "m": 
			if (prevWasHour) {
				res+=dt.getUTCHours();
			} else {
				res+=dt.getUTCMonth()+1; 
			};
			break;
         case "mm": 
			if (prevWasHour) {
				res+=pad2(dt.getUTCHours());
			} else {
				res+=pad2(dt.getUTCMonth()+1); 
			};
			break;
         case "mmm": res+=$fmt.ShortMonthNames[dt.getUTCMonth()]; break;
         case "mmmm": res+=$fmt.LongMonthNames[dt.getUTCMonth()]; break;
         case "yy": res+=pad2(dt.getUTCFullYear()%100); break;
         case "yyyy": res+=dt.getUTCFullYear(); break;
         case "h": res+=dt.getUTCHours(); wasHour=1; break;
         case "hh": res+=pad2(dt.getUTCHours()); wasHour=1; break;
         case "n": res+=dt.getUTCMinutes(); break;
         case "nn": res+=pad2(dt.getUTCMinutes()); break;
         case "s": res+=dt.getUTCSeconds(); break;
         case "ss": res+=pad2(dt.getUTCSeconds()); break;
         case "z": res+=dt.getUTCMilliseconds(); break;
         case "zzz": res+=pad3(dt.getUTCMilliseconds()); break;
         default: res+=tok;
      }
	  prevWasHour=wasHour;
   }
   return res;
}
/**
sprintf() for JavaScript 0.7-beta1
http://www.diveintojavascript.com/projects/javascript-sprintf

Copyright (c) Alexandru Marasteanu <alexaholic [at) gmail (dot] com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of sprintf() for JavaScript nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Alexandru Marasteanu BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

var sprintf = (function() {
	function get_type(variable) {
		return Object.prototype.toString.call(variable).slice(8, -1).toLowerCase();
	}
	function str_repeat(input, multiplier) {
		for (var output = []; multiplier > 0; output[--multiplier] = input) {/* do nothing */}
		return output.join('');
	}

	var str_format = function() {
		if (!str_format.cache.hasOwnProperty(arguments[0])) {
			str_format.cache[arguments[0]] = str_format.parse(arguments[0]);
		}
		return str_format.format.call(null, str_format.cache[arguments[0]], arguments);
	};

	str_format.format = function(parse_tree, argv) {
		var cursor = 1, tree_length = parse_tree.length, node_type = '', arg, output = [], i, k, match, pad, pad_character, pad_length;
		for (i = 0; i < tree_length; i++) {
			node_type = get_type(parse_tree[i]);
			if (node_type === 'string') {
				output.push(parse_tree[i]);
			}
			else if (node_type === 'array') {
				match = parse_tree[i]; // convenience purposes only
				if (match[2]) { // keyword argument
					arg = argv[cursor];
					for (k = 0; k < match[2].length; k++) {
						if (!arg.hasOwnProperty(match[2][k])) {
							throw(sprintf('[sprintf] property "%s" does not exist', match[2][k]));
						}
						arg = arg[match[2][k]];
					}
				}
				else if (match[1]) { // positional argument (explicit)
					arg = argv[match[1]];
				}
				else { // positional argument (implicit)
					arg = argv[cursor++];
				}

				if (/[^s]/.test(match[8]) && (get_type(arg) != 'number')) {
					throw(sprintf('[sprintf] expecting number but found %s', get_type(arg)));
				}
				switch (match[8]) {
					case 'b': arg = arg.toString(2); break;
					case 'c': arg = String.fromCharCode(arg); break;
					case 'd': arg = String(parseInt(arg, 10)); if (match[7]) { arg = str_repeat('0', match[7]-arg.length)+arg } break;
					case 'e': arg = match[7] ? arg.toExponential(match[7]) : arg.toExponential(); break;
					case 'f': arg = match[7] ? parseFloat(arg).toFixed(match[7]) : parseFloat(arg); break;
					case 'o': arg = arg.toString(8); break;
					case 's': arg = ((arg = String(arg)) && match[7] ? arg.substring(0, match[7]) : arg); break;
					case 'u': arg = Math.abs(arg); break;
					case 'x': arg = arg.toString(16); break;
					case 'X': arg = arg.toString(16).toUpperCase(); break;
				}
				arg = (/[def]/.test(match[8]) && match[3] && arg >= 0 ? '+'+ arg : arg);
				pad_character = match[4] ? match[4] == '0' ? '0' : match[4].charAt(1) : ' ';
				pad_length = match[6] - String(arg).length;
				pad = match[6] ? str_repeat(pad_character, pad_length) : '';
				output.push(match[5] ? arg + pad : pad + arg);
			}
		}
		return output.join('');
	};

	str_format.cache = {};

	str_format.parse = function(fmt) {
		var _fmt = fmt, match = [], parse_tree = [], arg_names = 0;
		while (_fmt) {
			if ((match = /^[^\x25]+/.exec(_fmt)) !== null) {
				parse_tree.push(match[0]);
			}
			else if ((match = /^\x25{2}/.exec(_fmt)) !== null) {
				parse_tree.push('%');
			}
			else if ((match = /^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(_fmt)) !== null) {
				if (match[2]) {
					arg_names |= 1;
					var field_list = [], replacement_field = match[2], field_match = [];
					if ((field_match = /^([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
						field_list.push(field_match[1]);
						while ((replacement_field = replacement_field.substring(field_match[0].length)) !== '') {
							if ((field_match = /^\.([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else if ((field_match = /^\[(\d+)\]/.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else {
								throw('[sprintf] huh?');
							}
						}
					}
					else {
						throw('[sprintf] huh?');
					}
					match[2] = field_list;
				}
				else {
					arg_names |= 2;
				}
				if (arg_names === 3) {
					throw('[sprintf] mixing positional and named placeholders is not (yet) supported');
				}
				parse_tree.push(match);
			}
			else {
				throw('[sprintf] huh?');
			}
			_fmt = _fmt.substring(match[0].length);
		}
		return parse_tree;
	};

	return str_format;
})();
function Format(f,a) { a.unshift(f); return sprintf.apply(null,a) }
function FloatToStr$_Float_(i) { return i.toString() }
function FloatToStr$_Float_Integer_(i,p) { return (p==99)?i.toString():i.toFixed(p) }
var Exception={
	$ClassName: "Exception",
	$Parent: TObject,
	$Init: function (s) { FMessage="" },
	Create: function (s,Msg) { s.FMessage=Msg; return s }
}
function DivMod$_Integer_Integer_Integer_Integer_(d1,d2,r1,r2) {var r=$Div(d1, d2); r1.v=r;r2.v=d1-r*d2; }
function Delete(s,i,n) { var v=s.v; if ((i<=0)||(i>v.length)||(n<=0)) return; s.v=v.substr(0,i-1)+v.substr(i+n-1); }
function DateTimeToStr(v, u) { return FormatDateTime($fmt.ShortDateFormat+" "+$fmt.LongTimeFormat, v, u) }
function ClampInt(v,mi,ma) { return v<mi ? mi : v>ma ? ma : v }
function BoolToStr(b) { return b?"True":"False" }
function $W(e) { return e.ClassType?e:Exception.Create($New(Exception),e.constructor.name+", "+e.message) }
// inspired from 
// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/String/charCodeAt
function $uniCharAt(str, idx) {
    var c = str.charCodeAt(idx);
    if (0xD800 <= c && c <= 0xDBFF) { // High surrogate
        return str.substr(idx, 2);
    }
    if (0xDC00 <= c && c <= 0xDFFF) { // Low surrogate
        return null;
    }
    return str.charAt(idx);
}var $TZ = 1;
var $fmt = { 
	ShortDayNames : [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
	LongDayNames : [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ],
	ShortMonthNames : [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
	LongMonthNames : [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
	ShortDateFormat : "yyyy-mm-dd",
	ShortTimeFormat : "hh:nn",
	LongTimeFormat : "hh:nn:ss"
}
function $Sign(v) { return v<0?-1:v>0?1:0 }
function $SetInc(s,v,m,n) { v-=m; if (v>=0 && v<n) s[v>>5]|=1<<(v&31) }
function $SetIn(s,v,m,n) { v-=m; return (v<0 && v>=n)?false:(s[v>>5]&(1<<(v&31)))!=0 }
function $SetExc(s,v,m,n) { v-=m; if (v>=0 && v<n) s[v>>5]&=~(1<<(v&31)) }
Array.prototype.pusha = function (e) { this.push.apply(this, e); return this }
function $Peek(a,z) {
	var n=a.length;
	if (n>0) return a[n-1];
	throw Exception.Create($New(Exception),"Upper bound exceeded! Index 0"+z);
}
function $NewDyn(c,z) {
	if (c==null) throw Exception.Create($New(Exception),"ClassType is nil"+z);
	var i={ClassType:c};
	c.$Init(i);
	return i
}
function $New(c) { var i={ClassType:c}; c.$Init(i); return i }
function $Is(o,c) {
	if (o===null) return false;
	return $Inh(o.ClassType,c);
}
;
function $Inh(s,c) {
	if (s===null) return false;
	while ((s)&&(s!==c)) s=s.$Parent;
	return (s)?true:false;
}
;
function $Extend(base, sub, props) {
	function F() {};
	F.prototype = base.prototype;
	sub.prototype = new F();
	sub.prototype.constructor = sub;
	for (var n in props) {
		if (props.hasOwnProperty(n)) {
			sub.prototype[n]=props[n];
		}
	}
}
function $Event3(i,f) {
	var li=i,lf=f;
	return function(a,b,c) {
		return lf.call(li,li,a,b,c)
	}
}
function $Event2(i,f) {
	var li=i,lf=f;
	return function(a,b) {
		return lf.call(li,li,a,b)
	}
}
function $Event1(i,f) {
	var li=i,lf=f;
	return function(a) {
		return lf.call(li,li,a)
	}
}
function $Event0(i,f) {
	var li=i,lf=f;
	return function() {
		return lf.call(li,li)
	}
}
function $Event(i,f) {
	var li=i,lf=f;
	return function(){
		var arg=Array.prototype.slice.call(arguments);
		arg.unshift(li);
		return lf.apply(li,arg)
	}
}
function $Div(a,b) { var r=a/b; return (r>=0)?Math.floor(r):Math.ceil(r) }
function $CmpNum(a,b) { return a-b }
function $AsIntf(o,i) {
	if (o===null) return null;
	var r = o.ClassType.$Intf[i].map(function (e) {
		return function () {
			var arg=Array.prototype.slice.call(arguments);
			arg.splice(0,0,o);
			return e.apply(o, arg);
		}
	});
	r.O = o;
	return r;
}
;
function $As(o,c) {
	if ((o===null)||$Is(o,c)) return o;
	throw Exception.Create($New(Exception),"Cannot cast instance of type \""+o.ClassType.$ClassName+"\" to class \""+c.$ClassName+"\"");
}
function $ArraySetLenC(a,n,d) {
	var o=a.length;
	if (o==n) return;
	if (o>n) a.length=n; else for (;o<n;o++) a.push(d());
}
/// TRagnarokServerBase = class (TObject)
var TRagnarokServerBase = {
   $ClassName:"TRagnarokServerBase",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// procedure TRagnarokServerBase.InitializeMessages()
   ,InitializeMessages:function(Self) {
      /* null */
   }
   ,Destroy:TObject.Destroy
   ,InitializeMessages$:function($){return $.ClassType.InitializeMessages($)}
};
/// TCustomRagnarokServer = class (TRagnarokServerBase)
var TCustomRagnarokServer = {
   $ClassName:"TCustomRagnarokServer",$Parent:TRagnarokServerBase
   ,$Init:function ($) {
      TRagnarokServerBase.$Init($);
      $.OnLogMessage = null;
      $.OnClientMessage = null;
      $.OnClientDisconnected = null;
      $.OnClientConnected = null;
      $.OnServerStopped = null;
      $.OnServerStarted = null;
      $.FFactory = $.FMessages = $.FServer = null;
      $.FSecure = false;
   }
   /// constructor TCustomRagnarokServer.Create(const Secure: Boolean)
   ,Create$63:function(Self, Secure$1) {
      TObject.Create(Self);
      Self.FSecure = Secure$1;
      Self.FFactory = TMessageFactory.Create$97$($New(TRagnarokMessageFactory));
      Self.FMessages = TRagnarokMessageDispatcher.Create$105$($New(TRagnarokMessageDispatcher));
      try {
         if (Self.FSecure) {
            Self.FServer = TW3ErrorObject.Create$3$($New(TNJWebSocketServerSecure));
         } else {
            Self.FServer = TW3ErrorObject.Create$3$($New(TNJWebSocketServer));
         }
      } catch ($e) {
         var e = $W($e);
         WriteLnF("Failed to create websocket server: %s",[e.FMessage]);
         WriteLn("Please make sure module ws is installed via npm");
         TApplication.Terminate(Application());
         return Self;
      }
      TNJRawWebSocketServer.SetTracking$(Self.FServer,true);
      TNJCustomServer.SetPort$(Self.FServer,1881);
      Self.FServer.OnAfterServerStarted = $Event1(Self,TCustomRagnarokServer.HandleServerStarted);
      Self.FServer.OnAfterServerStopped = $Event1(Self,TCustomRagnarokServer.HandleServerStopped);
      Self.FServer.OnClientConnected = $Event2(Self,TCustomRagnarokServer.HandleConnected);
      Self.FServer.OnClientDisconnected = $Event2(Self,TCustomRagnarokServer.HandleDisconnected);
      Self.FServer.OnTextMessage = $Event3(Self,TCustomRagnarokServer.HandleTextMessage);
      TRagnarokServerBase.InitializeMessages$(Self);
      return Self
   }
   /// destructor TCustomRagnarokServer.Destroy()
   ,Destroy:function(Self) {
      try {
         if (Self.FServer!==null) {
            try {
               if (TNJCustomServer.GetActive$2$(Self.FServer)) {
                  TNJCustomServer.Stop$(Self.FServer);
               }
            } finally {
               TObject.Free(Self.FServer);
               Self.FServer = null;
            }
         }
      } finally {
         TObject.Free(Self.FMessages);
         TObject.Free(Self.FFactory);
      }
      TObject.Destroy(Self);
   }
   /// procedure TCustomRagnarokServer.Dispatch(const Socket: TNJWebSocketSocket; const Envelope: TQTXBaseMessage)
   ,Dispatch:function(Self, Socket$1, Envelope) {
      var LInfo = null;
      var LSession = null;
      if (Envelope!==null) {
         TCustomRagnarokServer.WriteToLogF(Self,"Recieved message @ %s of type %s",[TimeToStr(Now(),0), TObject.ClassName(Envelope.ClassType)]);
      } else {
         TCustomRagnarokServer.WriteToLogF(Self,"Recieved unknown message @ %s",[TimeToStr(Now(),0)]);
         return;
      }
      try {
         LInfo = TRagnarokMessageDispatcher.GetMessageInfoForName(Self.FMessages,TObject.ClassName(Envelope.ClassType));
      } catch ($e) {
         var e$1 = $W($e);
         TCustomRagnarokServer.WriteToLog(Self,("Failed to locate message-handler info:"+e$1.FMessage));
         TCustomRagnarokServer.SendInternalError(Self,Socket$1,Envelope.Ticket,("Failed to locate message-handler info:"+e$1.FMessage),false);
      }
      if (LInfo===null) {
         TCustomRagnarokServer.WriteToLog(Self,("Failed to find messagehandler for:"+TObject.ClassName(Envelope.ClassType)));
         TCustomRagnarokServer.SendInternalError(Self,Socket$1,Envelope.Ticket,"No handler is registered for this message-type",false);
         return;
      }
      TCustomRagnarokServer.WriteToLog(Self,"Handler-Info found");
      if (!LInfo.MessageHandler) {
         TCustomRagnarokServer.WriteToLog(Self,("Invalid messagehandler for:"+TObject.ClassName(Envelope.ClassType)));
         TCustomRagnarokServer.SendInternalError(Self,Socket$1,Envelope.Ticket,"Invalid messagehandler for",false);
         return;
      }
      TCustomRagnarokServer.WriteToLog(Self,"Handler-Info is valid");
      try {
         LSession = TUserSessionManager.GetSessionbyId(SessionManager(),String(Socket$1.TagData));
      } catch ($e) {
         var e$2 = $W($e);
         TCustomRagnarokServer.WriteToLog(Self,("Failed to locate session object:"+e$2.FMessage));
         TCustomRagnarokServer.SendInternalError(Self,Socket$1,Envelope.Ticket,e$2.FMessage,false);
         return;
      }
      if (LSession===null) {
         TCustomRagnarokServer.WriteToLog(Self,("Session could not be found:"+SessionManager().FLastError));
         return;
      }
      try {
         LInfo.MessageHandler(Self,Socket$1,LSession,$As(Envelope,TQTXClientMessage));
      } catch ($e) {
         var e$3 = $W($e);
         TCustomRagnarokServer.WriteToLog(Self,("Failed to execute messagehandler:"+e$3.FMessage));
         TCustomRagnarokServer.SendRejection(Self,Socket$1,Envelope.Ticket,e$3.FMessage,false);
         return;
      }
      TCustomRagnarokServer.WriteToLog(Self,"Message-Handler Executed");
   }
   /// procedure TCustomRagnarokServer.HandleConnected(Sender: TObject; Socket: TNJWebSocketSocket)
   ,HandleConnected:function(Self, Sender, Socket$2) {
      var LSession$1 = null;
      LSession$1 = TUserSessionManager.Add$5(SessionManager(),Socket$2.RemoteAddress);
      if (LSession$1!==null) {
         Socket$2.TagData = TUserSession.GetIdentifier(LSession$1);
         TCustomRagnarokServer.WriteToLogF(Self,"Session %s is delegated for socket [%s] @ %s",[TUserSession.GetIdentifier(LSession$1), TNJWebSocketSocket.GetSocketName$(Socket$2), Socket$2.RemoteAddress]);
      } else {
         throw EW3Exception.CreateFmt$($New(ERagnarokServer),"Failed to create session: %s",[SessionManager().FLastError]);
      }
      if (Self.OnClientConnected) {
         Self.OnClientConnected(Self,Socket$2);
      }
   }
   /// procedure TCustomRagnarokServer.HandleDisconnected(Sender: TObject; Socket: TNJWebSocketSocket)
   ,HandleDisconnected:function(Self, Sender$1, Socket$3) {
      var LConnectionsFromSame = 0,
         x$1 = 0;
      var Item = null,
         LSession$2 = null;
      if (Self.OnClientDisconnected) {
         Self.OnClientDisconnected(Self,Socket$3);
      }
      TCustomRagnarokServer.WriteToLogF(Self,"Counting connections from same host: %d",[TNJRawWebSocketServer.GetCount(Self.FServer)]);
      LConnectionsFromSame = 0;
      var $temp1;
      for(x$1=0,$temp1=TNJRawWebSocketServer.GetCount(Self.FServer);x$1<$temp1;x$1++) {
         Item = TNJRawWebSocketServer.GetClient(Self.FServer,x$1);
         if ((Item.RemoteAddress).toLocaleLowerCase()==(Socket$3.RemoteAddress).toLocaleLowerCase()) {
            ++LConnectionsFromSame;
         }
      }
      TCustomRagnarokServer.WriteToLogF(Self,"Counted %d for host %s",[LConnectionsFromSame, Socket$3.RemoteAddress]);
      LSession$2 = TUserSessionManager.GetSessionbyId(SessionManager(),String(Socket$3.TagData));
      if (LSession$2!==null) {
         TCustomRagnarokServer.WriteToLogF(Self,"Session %s for socket %s has disconnected",[TUserSession.GetIdentifier(LSession$2), TNJWebSocketSocket.GetSocketName$(Socket$3)]);
         if (LConnectionsFromSame<1) {
            TCustomRagnarokServer.WriteToLog(Self,"Last connection for host, killing session object");
            TObject.Free(LSession$2);
         }
      }
   }
   /// procedure TCustomRagnarokServer.HandleServerStarted(Sender: TObject)
   ,HandleServerStarted:function(Self, Sender$2) {
      if (Self.OnServerStarted) {
         Self.OnServerStarted(Self);
      }
      TCustomRagnarokServer.WriteToLogF(Self,"Server started, listening on port %d",[TNJCustomServer.GetPort$(Self.FServer)]);
   }
   /// procedure TCustomRagnarokServer.HandleServerStopped(Sender: TObject)
   ,HandleServerStopped:function(Self, Sender$3) {
      if (Self.OnServerStopped) {
         Self.OnServerStopped(Self);
      }
      TCustomRagnarokServer.WriteToLog(Self,"Server stopped.");
   }
   /// procedure TCustomRagnarokServer.HandleTextMessage(Sender: TObject; Socket: TNJWebSocketSocket; Info: TNJWebsocketMessage)
   ,HandleTextMessage:function(Self, Sender$4, Socket$4, Info$2) {
      var LIdentifier = "";
      var MsgEnvelope = {};
      MsgEnvelope.v = null;
      var MsgObject,
         LReject = {v:false},
         LReason = {v:""},
         LSession$3 = null;
      MsgObject = JSON.parse(Info$2.wiText);
      if (MsgObject["identifier"]) {
         LIdentifier = String(MsgObject["identifier"]);
      }
      if (LIdentifier.length>=1) {
         if (TMessageFactory.Build(Self.FFactory,LIdentifier,MsgEnvelope)) {
            try {
               TQTXBaseMessage.Parse$3(MsgEnvelope.v,Info$2.wiText);
            } catch ($e) {
               var e$4 = $W($e);
               WriteLn("Parsing failed with:"+e$4.FMessage)            }
            if (Self.OnClientMessage) {
               LReject.v = false;
               LReason.v = "";
               LSession$3 = TUserSessionManager.GetSessionbyId(SessionManager(),String(Socket$4.TagData));
               Self.OnClientMessage(Self,MsgEnvelope.v,LSession$3,LReject,LReason);
               if (LReject.v) {
                  TCustomRagnarokServer.SendRejection(Self,Socket$4,MsgEnvelope.v.Ticket,LReason.v,true);
                  return;
               }
            }
            TCustomRagnarokServer.Dispatch(Self,Socket$4,MsgEnvelope.v);
         } else {
            TCustomRagnarokServer.SendUnSupported(Self,Socket$4,"");
         }
      } else {
         TCustomRagnarokServer.SendUnSupported(Self,Socket$4,"");
      }
   }
   /// procedure TCustomRagnarokServer.SendInternalError(const Socket: TNJWebSocketSocket; Ticket: String; const Cause: String; const Close: Boolean)
   ,SendInternalError:function(Self, Socket$5, Ticket$1, Cause, Close$8) {
      var reply = null;
      if (Socket$5!==null) {
         reply = TQTXBaseMessage.Create$100$($New(TQTXErrorMessage),Ticket$1);
         try {
            reply.Code = 500;
            reply.Response = ("Invalid or unsupported message ["+Cause.toString()+"] error");
            if (TNJWebSocketSocket.GetReadyState$(Socket$5)==1) {
               TNJWebSocketSocket.Send$1(Socket$5,TQTXBaseMessage.Serialize(reply));
               if (Close$8) {
                  TNJWebSocketSocket.Close$2(Socket$5);
               }
            }
         } finally {
            TObject.Free(reply);
         }
      }
   }
   /// procedure TCustomRagnarokServer.SendRejection(const Socket: TNJWebSocketSocket; Ticket: String; const Cause: String; const Close: Boolean)
   ,SendRejection:function(Self, Socket$6, Ticket$2, Cause$1, Close$9) {
      var reply$1 = null;
      if (Socket$6!==null) {
         reply$1 = TQTXBaseMessage.Create$100$($New(TQTXErrorMessage),Ticket$2);
         try {
            reply$1.Code = 501;
            reply$1.Response = ("The message was rejected by the server ("+Cause$1.toString()+")");
            if (TNJWebSocketSocket.GetReadyState$(Socket$6)==1) {
               TNJWebSocketSocket.Send$1(Socket$6,TQTXBaseMessage.Serialize(reply$1));
               if (Close$9) {
                  TNJWebSocketSocket.Close$2(Socket$6);
               }
            }
         } finally {
            TObject.Free(reply$1);
         }
      }
   }
   /// procedure TCustomRagnarokServer.SendUnSupported(const Socket: TNJWebSocketSocket; Ticket: String)
   ,SendUnSupported:function(Self, Socket$7, Ticket$3) {
      var reply$2 = null;
      if (Socket$7!==null) {
         reply$2 = TQTXBaseMessage.Create$100$($New(TQTXErrorMessage),Ticket$3);
         try {
            reply$2.Code = 500;
            reply$2.Response = "Invalid or unsupported message [%s] error";
            if (TNJWebSocketSocket.GetReadyState$(Socket$7)==1) {
               TNJWebSocketSocket.Send$1(Socket$7,TQTXBaseMessage.Serialize(reply$2));
            }
         } finally {
            TObject.Free(reply$2);
         }
      }
   }
   /// procedure TCustomRagnarokServer.WriteToLog(const Text: String)
   ,WriteToLog:function(Self, Text$1) {
      if (Self.OnLogMessage) {
         Self.OnLogMessage(Self,Text$1);
      }
   }
   /// procedure TCustomRagnarokServer.WriteToLogF(const Text: String; const Values: array of const)
   ,WriteToLogF:function(Self, Text$2, Values$9) {
      if (Self.OnLogMessage) {
         TCustomRagnarokServer.WriteToLog(Self,Format(Text$2,Values$9.slice(0)));
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,InitializeMessages:TRagnarokServerBase.InitializeMessages
   ,Create$63$:function($){return $.ClassType.Create$63.apply($.ClassType, arguments)}
};
/// TRagnarokServer = class (TCustomRagnarokServer)
var TRagnarokServer = {
   $ClassName:"TRagnarokServer",$Parent:TCustomRagnarokServer
   ,$Init:function ($) {
      TCustomRagnarokServer.$Init($);
      $.OnGetStorageDevice = null;
      $.OnInitAfterLogin = null;
      $.OnAuthenticate = null;
   }
   /// procedure TRagnarokServer.DoAuthenticate(Sender: TObject; Socket: TNJWebSocketSocket; Session: TUserSession; Request: TQTXClientMessage)
   ,DoAuthenticate:function(Self, Sender$5, Socket$8, Session, Request$1) {
      var LValid = false,
         LReq = null,
         Allow = {v:false},
         LFileSystem = null,
         Response$1 = null,
         reply$3 = null;
      LValid = false;
      LReq = $As(Request$1,TQTXLoginRequest);
      TUserSession.a$415(Session,LReq.Username);
      if (Self.OnAuthenticate) {
         Allow.v = true;
         Self.OnAuthenticate(Self,LReq,Session,Allow);
         LValid = Allow.v==true;
      } else {
         TCustomRagnarokServer.SendInternalError(Self,Socket$8,Request$1.Ticket,"Server authentication not available",true);
         return;
      }
      try {
         if (LValid) {
            LFileSystem = TRagnarokServer.GetFileSystem$(Self);
            TUserSession.a$419(Session,Now());
            TUserSession.SetState$2(Session,0);
            TUserSession.SetPrivileges(Session,[63]);
            if (Self.OnInitAfterLogin) {
               Self.OnInitAfterLogin(Self,Session);
            }
         }
      } finally {
         if (LValid) {
            Response$1 = TQTXBaseMessage.Create$100$($New(TQTXLoginResponse),Request$1.Ticket);
            try {
               Response$1.Username = LReq.Username;
               Response$1.SessionID = String(Socket$8.TagData);
               Response$1.Code = 200;
               Response$1.Response = "OK";
               TNJWebSocketSocket.Send$1(Socket$8,TQTXBaseMessage.Serialize(Response$1));
            } finally {
               TObject.Free(Response$1);
               Response$1 = null;
            }
         } else {
            reply$3 = TQTXBaseMessage.Create$100$($New(TQTXErrorMessage),Request$1.Ticket);
            reply$3.Code = 500;
            reply$3.Response = "Login failed, invalid username or password";
            TNJWebSocketSocket.Send$1(Socket$8,TQTXBaseMessage.Serialize(reply$3));
         }
      }
   }
   /// procedure TRagnarokServer.DoCmdDir(FileSystem: TW3NodeStorageDevice; Command: TWbCMDInfo; Socket: TNJWebSocketSocket; Session: TUserSession; Request: TQTXFileIORequest)
   ,DoCmdDir:function(Self, FileSystem, Command$1, Socket$9, Session$1, Request$2) {
      var Filesys = null,
         LPath = "",
         LInitialPath = "",
         temp = {v:""},
         LServicePath = "";
      Filesys = TRagnarokServer.GetFileSystem$(Self);
      if (Filesys===null) {
         TCustomRagnarokServer.SendUnSupported(Self,Socket$9,Request$2.Ticket);
         return;
      }
      if (!TW3StorageDevice.GetActive$(Filesys)) {
         TCustomRagnarokServer.SendInternalError(Self,Socket$9,Request$2.Ticket,"FileIO failed: filesystem not mounted error",false);
         return;
      }
      if (TUserSession.GetState$1(Session$1)) {
         TCustomRagnarokServer.SendInternalError(Self,Socket$9,Request$2.Ticket,"FileIO failed: user not authenticated error",false);
         return;
      }
      if (!$SetIn(TUserSession.GetPrivileges(Session$1),1,0,6)) {
         TCustomRagnarokServer.SendInternalError(Self,Socket$9,Request$2.Ticket,"FileIO failed: user lacks read privilege error",false);
         return;
      }
      LPath = IncludeTrailingPathDelimiter$4(TUserSession.a$420(Session$1));
      LInitialPath = (Command$1.ciParams).join("");
      if (Command$1.ciParams.length>0) {
         temp.v = (Command$1.ciParams).join("");
         if ((LInitialPath.substr(0,2)=="~\/")) {
            Delete(temp,1,2);
         }
         LPath = IncludeTrailingPathDelimiter$4(LPath)+temp.v;
      }
      LPath = NodePathAPI().normalize(LPath);
      LServicePath = IncludeTrailingPathDelimiter$4(".\\services");
      LServicePath = IncludeTrailingPathDelimiter$4(LServicePath)+"directoryReader.js";
      TRagnarokServer.ExecuteExternalJS(Self,[LServicePath, LPath].slice(),LInitialPath,function (Success$1, TagValue$4, Data$3) {
         var LResponse = null;
         if (Success$1) {
            LResponse = TQTXBaseMessage.Create$100$($New(TQTXFileIODirResponse),Request$2.Ticket);
            try {
               TQTXFileIODirResponse.LoadDirListFromString(LResponse,Data$3);
               LResponse.Code = 200;
               LResponse.Response = "OK";
               LResponse.FDirList.dlPath = String(TagValue$4);
               TNJWebSocketSocket.Send$1(Socket$9,TQTXBaseMessage.Serialize(LResponse));
            } finally {
               TObject.Free(LResponse);
            }
         } else {
            TCustomRagnarokServer.SendInternalError(Self,Socket$9,Request$2.Ticket,("Directory enumeration failed, "+Data$3.toString()),false);
         }
      });
   }
   /// procedure TRagnarokServer.DoCmdRead(FileSystem: TW3NodeStorageDevice; Command: TWbCMDInfo; Socket: TNJWebSocketSocket; Session: TUserSession; Request: TQTXFileIORequest)
   ,DoCmdRead:function(Self, FileSystem$1, Command$2, Socket$10, Session$2, Request$3) {
      var Filesys$1 = null,
         LPath$1 = "",
         temp$1 = {v:""},
         LInitialPath$1 = "",
         LServicePath$1 = "";
      if (Command$2.ciParams.length<1) {
         TCustomRagnarokServer.SendRejection(Self,Socket$10,Request$3.Ticket,"No filename error",false);
         return;
      }
      Filesys$1 = TRagnarokServer.GetFileSystem$(Self);
      if (Filesys$1===null) {
         TCustomRagnarokServer.SendUnSupported(Self,Socket$10,Request$3.Ticket);
         return;
      }
      if (!TW3StorageDevice.GetActive$(Filesys$1)) {
         TCustomRagnarokServer.SendInternalError(Self,Socket$10,Request$3.Ticket,"FileIO failed: filesystem not mounted error",false);
         return;
      }
      if (TUserSession.GetState$1(Session$2)) {
         TCustomRagnarokServer.SendInternalError(Self,Socket$10,Request$3.Ticket,"FileIO failed: user not authenticated error",false);
         return;
      }
      if (!$SetIn(TUserSession.GetPrivileges(Session$2),1,0,6)) {
         TCustomRagnarokServer.SendInternalError(Self,Socket$10,Request$3.Ticket,"FileIO failed: user lacks read privilege error",false);
         return;
      }
      LPath$1 = IncludeTrailingPathDelimiter$4(TUserSession.a$420(Session$2));
      temp$1.v = (Command$2.ciParams).join("");
      LInitialPath$1 = temp$1.v;
      if ((temp$1.v.substr(0,2)=="~\/")) {
         Delete(temp$1,1,2);
      }
      LPath$1 = IncludeTrailingPathDelimiter$4(LPath$1)+temp$1.v;
      LPath$1 = NodePathAPI().normalize(LPath$1);
      LServicePath$1 = IncludeTrailingPathDelimiter$4(".\\services");
      LServicePath$1 = IncludeTrailingPathDelimiter$4(LServicePath$1)+"messageConstructor.js";
      WriteLn("Loading file:"+LPath$1);
      TRagnarokServer.ExecuteExternalJS(Self,[LServicePath$1, Request$3.Ticket, LPath$1].slice(),LInitialPath$1,function (Success$2, TagValue$5, Data$4) {
         if (Success$2) {
            WriteLnF("Reading file-data was successfull [%s], %s",[TagValue$5, Request$3.Ticket]);
            TNJWebSocketSocket.Send$1(Socket$10,Data$4);
            return;
         } else {
            WriteLnF("Reading file-data failed [%s], %s",[TagValue$5, Request$3.Ticket]);
            TCustomRagnarokServer.SendInternalError(Self,Socket$10,Request$3.Ticket,("Failed to read file, "+Data$4.toString()+", "+Request$3.Ticket.toString()),false);
         }
      });
   }
   /// procedure TRagnarokServer.DoCmdRun(FileSystem: TW3NodeStorageDevice; Command: TWbCMDInfo; Socket: TNJWebSocketSocket; Session: TUserSession; Request: TQTXFileIORequest)
   ,DoCmdRun:function(Self, FileSystem$2, Command$3, Socket$11, Session$3, Request$4) {
      var xsend = "";
      xsend = Command$3.ciCommand+" "+(Command$3.ciParams).join(",");
      child_process().exec(xsend,function (error$3, stdout$2, stderr$2) {
         var ErrText = "",
            LText = "",
            Response$2 = null;
         if (error$3) {
            ErrText = ("Failed to execute ["+xsend.toString()+"], system failed with: "+error$3.message.toString());
            TCustomRagnarokServer.WriteToLog(Self,ErrText);
            TCustomRagnarokServer.WriteToLog(Self,"Sending internal error to client");
            TCustomRagnarokServer.SendInternalError(Self,Socket$11,Request$4.Ticket,ErrText,false);
            return;
         }
         LText = stdout$2.toString("utf8");
         Response$2 = TQTXBaseMessage.Create$100$($New(TQTXFileIOResponse),Request$4.Ticket);
         try {
            TAllocation.Release$1(Response$2.FBinary);
            TBinaryData.AppendBytes$(Response$2.FBinary,TDataTypeConverter.StringToBytes(Response$2.FBinary,LText));
            Response$2.Code = 200;
            Response$2.Response = "OK";
            TNJWebSocketSocket.Send$1(Socket$11,TQTXBaseMessage.Serialize(Response$2));
         } finally {
            TCustomRagnarokServer.WriteToLog(Self,TQTXBaseMessage.Serialize(Response$2));
            TObject.Free(Response$2);
         }
      });
   }
   /// procedure TRagnarokServer.DoFileIO(Sender: TObject; Socket: TNJWebSocketSocket; Session: TUserSession; Request: TQTXClientMessage)
   ,DoFileIO:function(Self, Sender$6, Socket$12, Session$4, Request$5) {
      var LReq$1 = null,
         Filesys$2 = null,
         LText$1 = "",
         LParser = null,
         x$2 = 0;
      var cm = {ciCommand:"",ciParams:[]};
      LReq$1 = $As(Request$5,TQTXFileIORequest);
      Filesys$2 = TRagnarokServer.GetFileSystem$(Self);
      if (Filesys$2===null) {
         TCustomRagnarokServer.SendUnSupported(Self,Socket$12,Request$5.Ticket);
         return;
      }
      if (!TW3StorageDevice.GetActive$(Filesys$2)) {
         TCustomRagnarokServer.SendInternalError(Self,Socket$12,Request$5.Ticket,"FileIO failed: filesystem not mounted error",false);
         return;
      }
      if (TUserSession.GetState$1(Session$4)) {
         TCustomRagnarokServer.SendInternalError(Self,Socket$12,Request$5.Ticket,"FileIO failed: user not authenticated error",false);
         return;
      }
      if (!$SetIn(TUserSession.GetPrivileges(Session$4),1,0,6)) {
         TCustomRagnarokServer.SendInternalError(Self,Socket$12,Request$5.Ticket,"FileIO failed: user lacks read privilege error",false);
         return;
      }
      LText$1 = Trim$_String_(LReq$1.Command);
      if (LText$1.length<1) {
         TCustomRagnarokServer.SendInternalError(Self,Socket$12,Request$5.Ticket,"FileIO failed, command-text was empty",false);
         return;
      }
      if (!StrEndsWith(LText$1,"\r")) {
         LText$1+="\r";
      }
      LParser = TWbCMDLineParser.Create$106$($New(TWbCMDLineParser));
      try {
         TTextBuffer.LoadBufferText(LParser.FContext.FBuffer$3,LText$1);
         if (!TCustomParser.Parse$(LParser)) {
            TCustomRagnarokServer.SendInternalError(Self,Socket$12,Request$5.Ticket,("FileIO failed, "+LParser.FLastError.toString()),false);
            return;
         }
         var $temp2;
         for(x$2=0,$temp2=TWbCMDLineParser.a$440(LParser);x$2<$temp2;x$2++) {
            cm = TWbCMDLineParser.a$441(LParser,x$2);
            {var $temp3 = (cm.ciCommand).toLocaleLowerCase();
               if ($temp3=="read") {
                  WriteLn("Executing the READ handler");
                  TRagnarokServer.DoCmdRead(Self,Filesys$2,Clone$TWbCMDInfo(cm),Socket$12,Session$4,LReq$1);
               }
                else if ($temp3=="dir") {
                  WriteLn("Executing the DIR handler");
                  TRagnarokServer.DoCmdDir(Self,Filesys$2,Clone$TWbCMDInfo(cm),Socket$12,Session$4,LReq$1);
               }
                else if ($temp3=="run") {
                  WriteLn("Executing the RUN handler");
                  TRagnarokServer.DoCmdRun(Self,Filesys$2,Clone$TWbCMDInfo(cm),Socket$12,Session$4,LReq$1);
               }
                else {
                  TCustomRagnarokServer.SendInternalError(Self,Socket$12,Request$5.Ticket,("FileIO failed, unknown command ["+cm.ciCommand.toString()+"]"),false);
                  break;
               }
            }
         }
      } finally {
         TObject.Free(LParser);
      }
   }
   /// procedure TRagnarokServer.ExecuteExternalJS(Params: array of String; TagValue: Variant; const CB: TRagnarokServerExecuteCB)
   ,ExecuteExternalJS:function(Self, Params, TagValue$6, CB) {
      var LTask = null;
      Params.splice(0,0,"--no-warnings");
      try {
         LTask = child_process().spawn("node",Params);
      } catch ($e) {
         var e$5 = $W($e);
         if (CB) {
            CB(false,TagValue$6,e$5.FMessage);
         }
         return;
      }
      LTask.on("error",function (error$4) {
         WriteLn("error->"+error$4.toString());
         TCustomRagnarokServer.WriteToLog(Self,(String(error$4.toString())));
      });
      LTask.stdout.on("data",function (data) {
         if (CB) {
            CB(true,TagValue$6,String(data.toString()));
         }
      });
      LTask.stderr.on("data",function (error$5) {
         WriteLn("stdErr->"+error$5.toString());
         TCustomRagnarokServer.WriteToLog(Self,(String(error$5.toString())));
      });
   }
   /// function TRagnarokServer.GetFileSystem() : TW3NodeStorageDevice
   ,GetFileSystem:function(Self) {
      var Result = null;
      var LDevice = {};
      LDevice.v = null;
      if (Self.OnGetStorageDevice) {
         Self.OnGetStorageDevice(Self,LDevice);
         Result = LDevice.v;
         return Result;
      }
      return Result
   }
   /// procedure TRagnarokServer.InitializeMessages()
   ,InitializeMessages:function(Self) {
      TRagnarokServerBase.InitializeMessages(Self);
      TRagnarokMessageDispatcher.RegisterMessage(Self.FMessages,TQTXLoginRequest,$Event(Self,TRagnarokServer.DoAuthenticate));
      TRagnarokMessageDispatcher.RegisterMessage(Self.FMessages,TQTXFileIORequest,$Event(Self,TRagnarokServer.DoFileIO));
   }
   ,Destroy:TCustomRagnarokServer.Destroy
   ,InitializeMessages$:function($){return $.ClassType.InitializeMessages($)}
   ,Create$63:TCustomRagnarokServer.Create$63
   ,GetFileSystem$:function($){return $.ClassType.GetFileSystem($)}
};
/// TMessageServer = class (TRagnarokServer)
var TMessageServer = {
   $ClassName:"TMessageServer",$Parent:TRagnarokServer
   ,$Init:function ($) {
      TRagnarokServer.$Init($);
      $.FDatabase = $.FDevice = $.FLog = $.FPrefs = null;
      $.FLogName = "";
   }
   /// constructor TMessageServer.Create(const Secure: Boolean)
   ,Create$63:function(Self, Secure$2) {
      TCustomRagnarokServer.Create$63(Self,Secure$2);
      Self.FPrefs = TObject.Create($New(TInifile));
      Self.FDevice = TW3Component.Create$44$($New(TW3NodeStorageDevice),null);
      Self.FDatabase = TW3Component.Create$44$($New(TSQLite3Database),null);
      Self.FLog = TW3ErrorObject.Create$3$($New(TNJLogFile));
      Self.FLog.OnLogOpen = $Event1(Self,TMessageServer.DoLogFileOpen);
      Self.OnLogMessage = $Event2(Self,TMessageServer.DoEmitLogMessage);
      Self.OnGetStorageDevice = $Event2(Self,TMessageServer.DoGetStorageDevice);
      Self.OnAuthenticate = $Event(Self,TMessageServer.DoAuthenticate$1);
      Self.OnInitAfterLogin = $Event2(Self,TMessageServer.DoInitSession);
      return Self
   }
   /// destructor TMessageServer.Destroy()
   ,Destroy:function(Self) {
      if (TW3Database.GetActive$1$(Self.FDatabase)) {
         TW3Database.Close(Self.FDatabase,null);
      }
      TObject.Free(Self.FLog);
      TObject.Free(Self.FDevice);
      TObject.Free(Self.FPrefs);
      TObject.Free(Self.FDatabase);
      TCustomRagnarokServer.Destroy(Self);
   }
   /// procedure TMessageServer.DoAuthenticate(Sender: TObject; Message: TQTXLoginRequest; Session: TUserSession; var Allow: Boolean)
   ,DoAuthenticate$1:function(Self, Sender$7, Message$1, Session$5, Allow$1) {
      Allow$1.v = false;
      if ((Message$1.Username).toLocaleLowerCase()=="admin") {
         Allow$1.v = (Message$1.Password).toLocaleLowerCase()=="admin";
      }
      if (Allow$1.v) {
         WriteLn("Login from "+Session$5.FRemoteIP+" accepted");
      } else {
         WriteLn("Login failed somehow");
      }
   }
   /// procedure TMessageServer.DoEmitLogMessage(Sender: TObject; LogText: String)
   ,DoEmitLogMessage:function(Self, Sender$8, LogText) {
      if (TNJLogFile.a$345(Self.FLog)) {
         TNJLogFile.Write$11(Self.FLog,LogText);
      } else {
         WriteLn(LogText);
      }
   }
   /// procedure TMessageServer.DoGetStorageDevice(Sender: TObject; var Device: TW3NodeStorageDevice)
   ,DoGetStorageDevice:function(Self, Sender$9, Device) {
      Device.v = Self.FDevice;
   }
   /// procedure TMessageServer.DoInitSession(Sender: TObject; Session: TUserSession)
   ,DoInitSession:function(Self, Sender$10, Session$6) {
      var LTemp = "";
      LTemp = IncludeTrailingPathDelimiter$4(Self.FDevice.FRootPath)+"userdata";
      LTemp = IncludeTrailingPathDelimiter$4(LTemp)+(TUserSession.a$414(Session$6)).toLocaleLowerCase();
      TUserSession.a$421(Session$6,IncludeTrailingPathDelimiter$4(LTemp));
   }
   /// procedure TMessageServer.DoLogFileOpen(Sender: TObject)
   ,DoLogFileOpen:function(Self, Sender$11) {
      TNJLogFile.Write$11(Self.FLog,"Logging begins @ "+DateTimeToStr(Now(),0));
      TNJLogFile.Write$11(Self.FLog,"====================================");
   }
   /// procedure TMessageServer.HandleHttpRequest(Sender: TObject; const Request: TNJHttpRequest; const Response: TNJHttpResponse)
   ,HandleHttpRequest:function(Self, Sender$12, Request$6, Response$3) {
      var LFilename = {v:""},
         LTarget = "",
         stream$1 = null;
      LFilename.v = Trim$_String_(TNJHttpRequest.a$198(Request$6));
      if (LFilename.v=="\/") {
         LFilename.v = "index.html";
      } else if ((LFilename.v.charAt(0)=="\/")) {
         Delete(LFilename,1,1);
      }
      LTarget = TPath.IncludeTrailingPathDelimiter$3(GetCurrentDir());
      LTarget+=TPath.IncludeTrailingPathDelimiter$3("webfiles");
      LTarget+=LFilename.v;
      LTarget = NormalizePath(LTarget);
      stream$1 = NodeFsAPI().createReadStream(LTarget);
      stream$1.on("error",function (error$6) {
         TNJHttpResponse.a$194(Response$3,400);
         TNJHttpResponse.End$1(Response$3,"<html><head><title>File not found<\/title><\/head><body>File not found error<\/body><\/html>");
      });
      TNJHttpResponse.a$194(Response$3,200);
      stream$1.pipe(TNJHttpResponse.a$193(Response$3));
   }
   /// procedure TMessageServer.Run()
   ,Run:function(Self) {
      TApplication.GetNodeProcess(Application()).on("uncaughtException",function (err) {
         console.log('Uncaught exception: ' + err);
      });
      TMessageServer.SetupPreferences(Self,function (Success$3) {
         if (Success$3) {
            TMessageServer.SetupDatabase(Self,function (Success$4) {
               if (Success$4) {
                  Self.FLogName = IncludeTrailingPathDelimiter$4(ExtractFilePath(TApplication.GetExeName(Application())));
                  Self.FLogName+=TCustomInifile.ReadString$4(Self.FPrefs,"log","logfile","log.txt");
                  TMessageServer.SetupLogfile(Self,Self.FLogName,function (Success$5) {
                     if (Success$5) {
                        TMessageServer.SetupStorageDevice(Self,function (Success$6) {
                           var LHttps = null;
                           if (Success$6) {
                              Self.FServer.OnError = function (Sender$13, Socket$13, Error$1) {
                                 WriteLn("WS-server crapped out: "+Error$1.message);
                              };
                              try {
                                 if (Self.FSecure) {
                                    LHttps = $As(TNJWebSocketHybridServer.GetHttpServer$(Self.FServer),TNJHTTPSServer);
                                    TNJHTTPSServer.SetMode(LHttps,0);
                                    TNJHTTPSCertificates.LoadCa(LHttps.FCerts,"SmartMobileStudio.ca");
                                    TNJHTTPSCertificates.LoadCert(LHttps.FCerts,"SmartMobileStudio.crt");
                                    TNJHTTPSCertificates.LoadKey(LHttps.FCerts,"SmartMobileStudio.key");
                                    TNJHTTPSServer.SetOptions(LHttps,[8]);
                                    TNJHttpsCiphers.AddCommonCiphers(LHttps.FCiphers,true);
                                    WriteLn("HTTPS\/WSS Initialized");
                                 } else {
                                    WriteLn("HTTP\/WS Initialized");
                                 }
                                 if (Self.FSecure) {
                                    $As(TNJWebSocketHybridServer.GetHttpServer$(Self.FServer),TNJHTTPSServer).OnRequest = $Event3(Self,TMessageServer.HandleHttpRequest);
                                 } else {
                                    $As(TNJWebSocketHybridServer.GetHttpServer$(Self.FServer),TNJHTTPServer).OnRequest = $Event3(Self,TMessageServer.HandleHttpRequest);
                                 }
                                 TNJCustomServer.SetPort$(Self.FServer,TCustomInifile.ReadInteger$2(Self.FPrefs,"master","port",8090));
                                 TNJCustomServer.SetPort$(TNJWebSocketHybridServer.GetHttpServer$(Self.FServer),TNJCustomServer.GetPort$(Self.FServer));
                                 TNJCustomServer.SetActive$3$(TNJWebSocketHybridServer.GetHttpServer$(Self.FServer),true);
                                 TNJCustomServer.SetActive$3$(Self.FServer,true);
                              } catch ($e) {
                                 var e$6 = $W($e);
                                 WriteLn(e$6.FMessage)                              }
                           } else {
                              TApplication.Terminate(Application());
                           }
                        });
                     } else {
                        TApplication.Terminate(Application());
                     }
                  });
               } else {
                  WriteLn("Failed to setup database error");
                  TApplication.Terminate(Application());
               }
            });
         } else {
            WriteLn("Failed to setup preferences error");
            TApplication.Terminate(Application());
         }
      });
   }
   /// procedure TMessageServer.SetupDatabase(const CB: TStdCallback)
   ,SetupDatabase:function(Self, CB$1) {
      var LDbFileToOpen = "",
         LBasePath = "";
      LDbFileToOpen = TCustomInifile.ReadString$4(Self.FPrefs,"database","database_name","");
      LDbFileToOpen = Trim$_String_(LDbFileToOpen);
      if (LDbFileToOpen.length<1) {
         LBasePath = TPath.GetDirectoryName$3(TApplication.GetExeName(Application()));
         LDbFileToOpen = TPath.IncludeTrailingPathDelimiter$3(LBasePath)+"amisys.db";
      }
      TSQLite3Database.SetAccessMode$1$(Self.FDatabase,3);
      TW3Database.Open$1(Self.FDatabase,LDbFileToOpen,function (Sender$14, Success$7) {
         if (Success$7) {
            WriteLnF("Database opened @ %s",[TW3Database.GetLocation(Self.FDatabase)]);
         } else {
            WriteLn("Failed to open database @ "+TW3Database.GetLocation(Self.FDatabase));
            WriteLn(Self.FDatabase.FLastError$1);
            if (CB$1) {
               CB$1(false);
            }
            return;
         }
         TSQLite3Database.Execute$5$(Self.FDatabase,"create table if not exists profiles\r\n    (\r\n      id integer primary key AUTOINCREMENT,\r\n      username  text,\r\n      password  text\r\n    );\r\n\r\ncreate table if not exists sessions\r\n    (\r\n      id integer primary key AUTOINCREMENT,\r\n      socketname text,\r\n      userid integer NOT NULL, FOREIGN KEY (id) REFERENCES profiles(id)\r\n    );\r\n    ",[],function (Sender$15, Success$8) {
            if (Success$8) {
               /* null */
            }
            if (CB$1) {
               CB$1(Success$8);
            }
         });
      });
   }
   /// procedure TMessageServer.SetupLogfile(LogFileName: String; const CB: TStdCallback)
   ,SetupLogfile:function(Self, LogFileName, CB$2) {
      try {
         TNJLogFile.Open$2(Self.FLog,LogFileName);
         if (TW3ErrorObject.GetFailed(Self.FLog)) {
            WriteLnF("Failed to setup logfile (%s): %s",[LogFileName, Self.FLog.FLastError]);
         } else {
            Self.FLogName = LogFileName;
            Self.FLog.OnTextWritten = function (sender, Text$3) {
               WriteLn(Text$3);
            };
         }
      } finally {
         if (CB$2) {
            CB$2(!TW3ErrorObject.GetFailed(Self.FLog));
         }
      }
   }
   /// procedure TMessageServer.SetupPreferences(const CB: TStdCallback)
   ,SetupPreferences:function(Self, CB$3) {
      var LBasePath$1 = "",
         LPrefsFile = "";
      LBasePath$1 = TPath.GetDirectoryName$3(TApplication.GetExeName(Application()));
      LPrefsFile = TPath.IncludeTrailingPathDelimiter$3(LBasePath$1)+"prefs.ini";
      if (FileExists$2(LPrefsFile)) {
         try {
            TCustomInifile.LoadFromFile(Self.FPrefs,LPrefsFile);
         } catch ($e) {
            var e$7 = $W($e);
            WriteLnF("Failed to load preferences file (%s): %s",[LPrefsFile, e$7.FMessage]);
            if (CB$3) {
               CB$3(false);
            }
            return;
         }
         if (CB$3) {
            CB$3(true);
         }
      } else if (CB$3) {
         CB$3(false);
      }
   }
   /// procedure TMessageServer.SetupStorageDevice(const CB: TStdCallback)
   ,SetupStorageDevice:function(Self, CB$4) {
      TNJLogFile.Write$11(Self.FLog,"Mounting filesystem");
      try {
         TW3StorageDevice.Mount$(Self.FDevice,null,function (Sender$16, Success$9) {
            if (Success$9) {
               TW3StorageDevice.GetPath$1$(Self.FDevice,function (Sender$17, Path$1, Success$10) {
                  if (Success$10) {
                     TNJLogFile.WriteF(Self.FLog,"Filesystem root mapped to: %s",[Path$1]);
                  }
               });
            } else {
               TNJLogFile.WriteF(Self.FLog,"Failed to mount (%s).",[Self.FDevice.FLastError$1]);
            }
            if (CB$4) {
               CB$4(Success$9);
            }
         });
      } catch ($e) {
         var e$8 = $W($e);
         if (CB$4) {
            CB$4(false);
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,InitializeMessages:TRagnarokServer.InitializeMessages
   ,Create$63$:function($){return $.ClassType.Create$63.apply($.ClassType, arguments)}
   ,GetFileSystem:TRagnarokServer.GetFileSystem
};
/// function TW3VariantHelper.DataType(const Self: Variant) : TW3VariantDataType
function TW3VariantHelper$DataType$1(Self$1) {
   var Result = 1;
   var LType = "";
   if (TW3VariantHelper$Valid$2(Self$1)) {
      LType = typeof(Self$1);
      {var $temp4 = (LType).toLocaleLowerCase();
         if ($temp4=="object") {
            if (!Self$1.length) {
               Result = 8;
            } else {
               Result = 9;
            }
         }
          else if ($temp4=="function") {
            Result = 7;
         }
          else if ($temp4=="symbol") {
            Result = 6;
         }
          else if ($temp4=="boolean") {
            Result = 2;
         }
          else if ($temp4=="string") {
            Result = 5;
         }
          else if ($temp4=="number") {
            if (Math.round(Number(Self$1))!=Self$1) {
               Result = 4;
            } else {
               Result = 3;
            }
         }
          else if ($temp4=="array") {
            Result = 9;
         }
          else {
            Result = 1;
         }
      }
   } else if (Self$1==null) {
      Result = 10;
   } else {
      Result = 1;
   }
   return Result
}
/// function TW3VariantHelper.Defined(const Self: Variant) : Boolean
function TW3VariantHelper$Defined(Self$2) {
   var Result = false;
   Result = !(Self$2 == undefined);
   return Result
}
/// function TW3VariantHelper.Equals(const Self: Variant; const Reference: Variant) : Boolean
function TW3VariantHelper$Equals$5(Self$3, Reference) {
   var Result = false;
   Result = (Self$3 == Reference);
   return Result
}
/// function TW3VariantHelper.IsArray(const Self: Variant) : Boolean
function TW3VariantHelper$IsArray(Self$4) {
   var Result = false;
   Result = ((Self$4) !== undefined)
      && (Self$4 !== null)
      && (typeof Self$4 === "object")
      && ((Self$4).length !== undefined);
   return Result
}
/// function TW3VariantHelper.IsBoolean(const Self: Variant) : Boolean
function TW3VariantHelper$IsBoolean(Self$5) {
   var Result = false;
   Result = ((Self$5) !== undefined)
      && (Self$5 !== null)
      && (typeof Self$5 === "boolean");
   return Result
}
/// function TW3VariantHelper.Isfloat(const Self: Variant) : Boolean
function TW3VariantHelper$Isfloat(Self$6) {
   var Result = false;
   Result = ((Self$6) !== undefined)
      && (Self$6 !== null)
      && (typeof Self$6  === "number")
      && (Math.round(Self$6) != Self$6);
   return Result
}
/// function TW3VariantHelper.IsFunction(const Self: Variant) : Boolean
function TW3VariantHelper$IsFunction(Self$7) {
   var Result = false;
   Result = ((Self$7) !== undefined)
      && (Self$7 !== null)
      && (typeof Self$7 === "function");
   return Result
}
/// function TW3VariantHelper.Isinteger(const Self: Variant) : Boolean
function TW3VariantHelper$Isinteger(Self$8) {
   var Result = false;
   Result = ((Self$8) !== undefined)
      && (Self$8 !== null)
      && (typeof Self$8  === "number")
      && (Math.round(Self$8) === Self$8);
   return Result
}
/// function TW3VariantHelper.IsObject(const Self: Variant) : Boolean
function TW3VariantHelper$IsObject(Self$9) {
   var Result = false;
   Result = ((Self$9) !== undefined)
      && (Self$9 !== null)
      && (typeof Self$9  === "object")
      && ((Self$9).length === undefined);
   return Result
}
/// function TW3VariantHelper.Isstring(const Self: Variant) : Boolean
function TW3VariantHelper$Isstring(Self$10) {
   var Result = false;
   Result = (Self$10 !== undefined)
      && (Self$10 !== null)
      && (typeof Self$10  === "string");
   return Result
}
/// function TW3VariantHelper.IsSymbol(const Self: Variant) : Boolean
function TW3VariantHelper$IsSymbol(Self$11) {
   var Result = false;
   Result = ((Self$11) !== undefined)
      && (Self$11 !== null)
      && (typeof Self$11 === "symbol");
   return Result
}
/// function TW3VariantHelper.IsUInt8Array(const Self: Variant) : Boolean
function TW3VariantHelper$IsUInt8Array(Self$12) {
   var Result = false;
   var LTypeName = "";
   Result = false;
   if (TW3VariantHelper$Valid$2(Self$12)) {
      LTypeName = Object.prototype.toString.call(Self$12);
      Result = LTypeName=="[object Uint8Array]";
   }
   return Result
}
/// function TW3VariantHelper.UnDefined(const Self: Variant) : Boolean
function TW3VariantHelper$UnDefined(Self$13) {
   var Result = false;
   Result = (Self$13 == undefined);
   return Result
}
/// function TW3VariantHelper.Valid(const Self: Variant) : Boolean
function TW3VariantHelper$Valid$2(Self$14) {
   var Result = false;
   Result = !( (Self$14 == undefined) || (Self$14 == null) );
   return Result
}
/// TW3VariantDataType enumeration
var TW3VariantDataType = { 1:"vdUnknown", 2:"vdBoolean", 3:"vdinteger", 4:"vdfloat", 5:"vdstring", 6:"vdSymbol", 7:"vdFunction", 8:"vdObject", 9:"vdArray", 10:"vdVariant" };
/// TW3OwnedObject = class (TObject)
var TW3OwnedObject = {
   $ClassName:"TW3OwnedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FOwner = null;
   }
   /// function TW3OwnedObject.GetOwner() : TObject
   ,GetOwner:function(Self) {
      return Self.FOwner;
   }
   /// procedure TW3OwnedObject.SetOwner(const NewOwner: TObject)
   ,SetOwner:function(Self, NewOwner) {
      if (NewOwner!==Self.FOwner) {
         if (TW3OwnedObject.AcceptOwner$(Self,NewOwner)) {
            Self.FOwner = NewOwner;
         } else {
            throw EW3Exception.CreateFmt$($New(EW3OwnedObject),$R[0],["TW3OwnedObject.SetOwner", TObject.ClassName(Self.ClassType), $R[2]]);
         }
      }
   }
   /// function TW3OwnedObject.AcceptOwner(const CandidateObject: TObject) : Boolean
   ,AcceptOwner:function(Self, CandidateObject) {
      return true;
   }
   /// constructor TW3OwnedObject.Create(const AOwner: TObject)
   ,Create$16:function(Self, AOwner) {
      TObject.Create(Self);
      TW3OwnedObject.SetOwner$(Self,AOwner);
      return Self
   }
   ,Destroy:TObject.Destroy
   ,GetOwner$:function($){return $.ClassType.GetOwner($)}
   ,SetOwner$:function($){return $.ClassType.SetOwner.apply($.ClassType, arguments)}
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16$:function($){return $.ClassType.Create$16.apply($.ClassType, arguments)}
};
TW3OwnedObject.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3OwnedLockedObject = class (TW3OwnedObject)
var TW3OwnedLockedObject = {
   $ClassName:"TW3OwnedLockedObject",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.OnObjectUnLocked = null;
      $.OnObjectLocked = null;
      $.FLocked = 0;
   }
   /// procedure TW3OwnedLockedObject.DisableAlteration()
   ,DisableAlteration:function(Self) {
      ++Self.FLocked;
      if (Self.FLocked==1) {
         TW3OwnedLockedObject.ObjectLocked$(Self);
      }
   }
   /// procedure TW3OwnedLockedObject.EnableAlteration()
   ,EnableAlteration:function(Self) {
      if (Self.FLocked>0) {
         --Self.FLocked;
         if (!Self.FLocked) {
            TW3OwnedLockedObject.ObjectUnLocked$(Self);
         }
      }
   }
   /// function TW3OwnedLockedObject.GetLockState() : Boolean
   ,GetLockState:function(Self) {
      return Self.FLocked>0;
   }
   /// procedure TW3OwnedLockedObject.ObjectLocked()
   ,ObjectLocked:function(Self) {
      if (Self.OnObjectLocked) {
         Self.OnObjectLocked(Self);
      }
   }
   /// procedure TW3OwnedLockedObject.ObjectUnLocked()
   ,ObjectUnLocked:function(Self) {
      if (Self.OnObjectUnLocked) {
         Self.OnObjectUnLocked(Self);
      }
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked$:function($){return $.ClassType.ObjectLocked($)}
   ,ObjectUnLocked$:function($){return $.ClassType.ObjectUnLocked($)}
};
TW3OwnedLockedObject.$Intf={
   IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3LockedObject = class (TObject)
var TW3LockedObject = {
   $ClassName:"TW3LockedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.OnObjectUnLocked = null;
      $.OnObjectLocked = null;
      $.FLocked$1 = 0;
   }
   /// procedure TW3LockedObject.DisableAlteration()
   ,DisableAlteration$1:function(Self) {
      ++Self.FLocked$1;
      if (Self.FLocked$1==1) {
         TW3LockedObject.ObjectLocked$1$(Self);
      }
   }
   /// procedure TW3LockedObject.EnableAlteration()
   ,EnableAlteration$1:function(Self) {
      if (Self.FLocked$1>0) {
         --Self.FLocked$1;
         if (!Self.FLocked$1) {
            TW3LockedObject.ObjectUnLocked$1$(Self);
         }
      }
   }
   /// function TW3LockedObject.GetLockState() : Boolean
   ,GetLockState$1:function(Self) {
      return Self.FLocked$1>0;
   }
   /// procedure TW3LockedObject.ObjectLocked()
   ,ObjectLocked$1:function(Self) {
      if (Self.OnObjectLocked) {
         Self.OnObjectLocked(Self);
      }
   }
   /// procedure TW3LockedObject.ObjectUnLocked()
   ,ObjectUnLocked$1:function(Self) {
      if (Self.OnObjectUnLocked) {
         Self.OnObjectUnLocked(Self);
      }
   }
   ,Destroy:TObject.Destroy
   ,ObjectLocked$1$:function($){return $.ClassType.ObjectLocked$1($)}
   ,ObjectUnLocked$1$:function($){return $.ClassType.ObjectUnLocked$1($)}
};
TW3LockedObject.$Intf={
   IW3LockObject:[TW3LockedObject.DisableAlteration$1,TW3LockedObject.EnableAlteration$1,TW3LockedObject.GetLockState$1]
}
/// TW3Identifiers = class (TObject)
var TW3Identifiers = {
   $ClassName:"TW3Identifiers",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TW3Identifiers.GenerateUniqueComponentName() : String
   ,GenerateUniqueComponentName$1:function(Self) {
      var Result = "";
      ++__UNIQUE;
      Result = "Component"+__UNIQUE.toString();
      return Result
   }
   /// function TW3Identifiers.GenerateUniqueComponentName(const Prefix: String) : String
   ,GenerateUniqueComponentName:function(Self, Prefix) {
      var Result = "";
      ++__UNIQUE;
      Result = Prefix+"Component"+__UNIQUE.toString();
      return Result
   }
   /// function TW3Identifiers.GenerateUniqueAnimationName() : String
   ,GenerateUniqueAnimationName:function(Self) {
      var Result = "";
      ++__UNIQUE;
      Result = "SmartAnim"+__UNIQUE.toString();
      return Result
   }
   /// function TW3Identifiers.GenerateUniqueObjectId() : String
   ,GenerateUniqueObjectId:function(Self) {
      var Result = "";
      ++__UNIQUE;
      Result = "OBJ"+__UNIQUE.toString();
      return Result
   }
   /// function TW3Identifiers.GenerateUniqueNumber() : Integer
   ,GenerateUniqueNumber:function(Self) {
      var Result = 0;
      ++__UNIQUE;
      Result = __UNIQUE;
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TW3FilePermissionMask enumeration
var TW3FilePermissionMask = { 0:"fpNone", 111:"fpExecute", 222:"fpWrite", 333:"fpWriteExecute", 444:"fpRead", 555:"fpReadExecute", 666:"fpDefault", 777:"fpReadWriteExecute", 740:"fpRWEGroupReadOnly" };
/// TW3CustomDeviceCapabilities = class (TObject)
var TW3CustomDeviceCapabilities = {
   $ClassName:"TW3CustomDeviceCapabilities",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
   ,GetGamePadSupport$:function($){return $.ClassType.GetGamePadSupport($)}
   ,GetKeyboardSupported$:function($){return $.ClassType.GetKeyboardSupported($)}
   ,GetMouseSupport$:function($){return $.ClassType.GetMouseSupport($)}
   ,GetTouchSupport$:function($){return $.ClassType.GetTouchSupport($)}
};
/// TVariant = class (TObject)
var TVariant = {
   $ClassName:"TVariant",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TVariant.AsBool(const aValue: Variant) : Boolean
   ,AsBool:function(aValue) {
      var Result = false;
      if (aValue!=undefined&&aValue!=null) {
         Result = (aValue?true:false);
      }
      return Result
   }
   /// function TVariant.AsFloat(const aValue: Variant) : Float
   ,AsFloat:function(aValue$1) {
      var Result = 0;
      if (aValue$1!=undefined&&aValue$1!=null) {
         Result = Number(aValue$1);
      }
      return Result
   }
   /// function TVariant.AsInteger(const aValue: Variant) : Integer
   ,AsInteger:function(aValue$2) {
      var Result = 0;
      if (aValue$2!=undefined&&aValue$2!=null) {
         Result = parseInt(aValue$2,10);
      }
      return Result
   }
   /// function TVariant.AsObject(const aValue: Variant) : TObject
   ,AsObject:function(aValue$3) {
      var Result = null;
      if (aValue$3!=undefined&&aValue$3!=null) {
         Result = aValue$3;
      }
      return Result
   }
   /// function TVariant.AsString(const aValue: Variant) : String
   ,AsString:function(aValue$4) {
      var Result = "";
      if (aValue$4!=undefined&&aValue$4!=null) {
         Result = String(aValue$4);
      }
      return Result
   }
   /// function TVariant.ClassInstance(const instance: Variant) : Boolean
   ,ClassInstance:function(instance) {
      var Result = false;
      if (typeof instance === 'object') {
      Result = Object.prototype.hasOwnProperty.call(instance, "ClassType")  === true;
    } else {
      Result = false;
    }
      return Result
   }
   /// function TVariant.CreateArray() : Variant
   ,CreateArray:function() {
      var Result = undefined;
      Result = new Array();
      return Result
   }
   /// function TVariant.CreateObject() : Variant
   ,CreateObject:function() {
      var Result = undefined;
      Result = new Object();
      return Result
   }
   /// procedure TVariant.ForEachProperty(const Data: Variant; const CallBack: TW3ObjectKeyCallback)
   ,ForEachProperty:function(Data$5, CallBack) {
      var LObj,
         Keys$1 = [],
         a$451 = 0;
      var LName = "";
      if (CallBack) {
         Keys$1 = TVariant.Properties(Data$5);
         var $temp5;
         for(a$451=0,$temp5=Keys$1.length;a$451<$temp5;a$451++) {
            LName = Keys$1[a$451];
            LObj = Keys$1[LName];
            if ((~CallBack(LName,LObj))==1) {
               break;
            }
         }
      }
   }
   /// function TVariant.FromObject(const Object: TObject) : Variant
   ,FromObject:function(Object$5) {
      return Object$5;
   }
   /// function TVariant.GetKeys(const aValue: Variant) : TStrArray
   ,GetKeys:function(aValue$5) {
      var Result = [];
      if (Object.keys) {
      Result = Object.keys(aValue$5);
    } else {
      Result = [];
    }
      return Result
   }
   /// function TVariant.GetOwnPropertyByEnum(const aValue: Variant) : TStrArray
   ,GetOwnPropertyByEnum:function(aValue$6) {
      var Result = [];
      var LName$1 = "";
      for (LName$1 in aValue$6) {
      if ( (aValue$6).hasOwnProperty(LName$1) == true )
        (Result).push(LName$1);
    }
      return Result
   }
   /// function TVariant.GetOwnPropertyNames(const aValue: Variant) : TStrArray
   ,GetOwnPropertyNames:function(aValue$7) {
      var Result = [];
      Result = Object.getOwnPropertyNames(aValue$7);
      return Result
   }
   /// function TVariant.IsBool(const aValue: Variant) : Boolean
   ,IsBool:function(aValue$8) {
      return typeof(aValue$8)==__TYPE_MAP.Boolean;
   }
   /// function TVariant.IsInteger(const aValue: Variant) : Boolean
   ,IsInteger:function(aValue$9) {
      var Result = false;
      if (aValue$9 == null) return false;
    if (aValue$9 == undefined) return false;
    if (typeof(aValue$9) === "number") if (parseInt(aValue$9) === aValue$9) return true;
      return Result
   }
   /// function TVariant.IsNAN(const aValue: Variant) : Boolean
   ,IsNAN:function(aValue$10) {
      return isNaN(Number(aValue$10));
   }
   /// function TVariant.IsNull(const aValue: Variant) : Boolean
   ,IsNull:function(aValue$11) {
      return aValue$11==null;
   }
   /// function TVariant.IsNumber(const aValue: Variant) : Boolean
   ,IsNumber:function(aValue$12) {
      return typeof(aValue$12)==__TYPE_MAP.Number$1;
   }
   /// function TVariant.IsString(const aValue: Variant) : Boolean
   ,IsString:function(aValue$13) {
      return typeof(aValue$13)==__TYPE_MAP.String$1;
   }
   /// function TVariant.IsUnassigned(const aValue: Variant) : Boolean
   ,IsUnassigned:function(aValue$14) {
      return typeof(aValue$14)==__TYPE_MAP.Undefined;
   }
   /// function TVariant.Properties(const Data: Variant) : TStrArray
   ,Properties:function(Data$6) {
      var Result = [];
      if (Data$6) {
         if (!(Object.keys === undefined)) {
        Result = Object.keys(Data$6);
        return Result;
      }
         if (!(Object.getOwnPropertyNames === undefined)) {
          Result = Object.getOwnPropertyNames(Data$6);
          return Result;
      }
         for (var qtxenum in Data$6) {
        if ( (Data$6).hasOwnProperty(qtxenum) == true )
          (Result).push(qtxenum);
      }
      return Result;
      }
      return Result
   }
   /// function TVariant.PropertyDelete(const Data: Variant; const Id: String) : Variant
   ,PropertyDelete:function(Data$7, Id) {
      var Result = undefined;
      if (Data$7) {
         if (Data$7[Id]) {
            delete Data$7[Id];
         }
      }
      Result = Data$7;
      return Result
   }
   /// function TVariant.PropertyExists(const Data: Variant; const Id: String) : Boolean
   ,PropertyExists:function(Data$8, Id$1) {
      var Result = false;
      Result = (Data$8).hasOwnProperty(Id$1);
      return Result
   }
   /// function TVariant.PropertyRead(const Data: Variant; const Id: String) : Variant
   ,PropertyRead:function(Data$9, Id$2) {
      var Result = undefined;
      Result = Data$9[Id$2];
      return Result
   }
   /// function TVariant.PropertyWrite(const Data: Variant; const Id: String; const Value: Variant) : Variant
   ,PropertyWrite:function(Data$10, Id$3, Value$9) {
      var Result = undefined;
      Result = Data$10;
    Result[Id$3] = Value$9;
      return Result
   }
   /// function TVariant.Search(const Field: String; const Value: Variant; const AssociateArray: Variant) : Variant
   ,Search:function(Self, Field, Value$10, AssociateArray) {
      var Result = undefined;
      var index = 0,
         LLongs = 0,
         LSingles = 0;
      Result = undefined;
      index = 0;
      LLongs = AssociateArray.length>>>3;
      LSingles = AssociateArray.length%8;
      while (LLongs>0) {
         if (AssociateArray[index][Field]==Value$10) {
            Result = AssociateArray[index];
            return Result;
         }
         ++index;
         if (AssociateArray[index][Field]==Value$10) {
            Result = AssociateArray[index];
            return Result;
         }
         ++index;
         if (AssociateArray[index][Field]==Value$10) {
            Result = AssociateArray[index];
            return Result;
         }
         ++index;
         if (AssociateArray[index][Field]==Value$10) {
            Result = AssociateArray[index];
            return Result;
         }
         ++index;
         if (AssociateArray[index][Field]==Value$10) {
            Result = AssociateArray[index];
            return Result;
         }
         ++index;
         if (AssociateArray[index][Field]==Value$10) {
            Result = AssociateArray[index];
            return Result;
         }
         ++index;
         if (AssociateArray[index][Field]==Value$10) {
            Result = AssociateArray[index];
            return Result;
         }
         ++index;
         if (AssociateArray[index][Field]==Value$10) {
            Result = AssociateArray[index];
            return Result;
         }
         ++index;
         --LLongs;
      }
      while (LSingles>0) {
         if (AssociateArray[index][Field]==Value$10) {
            Result = AssociateArray[index];
            return Result;
         }
         ++index;
         --LSingles;
      }
      return Result
   }
   /// function TVariant.Sync(target: JObject; const prop: Variant) : JObject
   ,Sync$1:function(target, prop) {
      var Result = null;
      var n;
      for (n in prop) if ((prop).hasOwnProperty(n)) target[n]=prop[n];
    Result = target;
      return Result
   }
   /// function TVariant.Sync(const target: Variant; const prop: Variant) : Variant
   ,Sync:function(target$1, prop$1) {
      var Result = undefined;
      var n$1;
      for (n$1 in prop$1) if ((prop$1).hasOwnProperty(n$1)) target$1[n$1]=prop$1[n];
    Result = target$1;
      return Result
   }
   /// function TVariant.ToObject(const Value: Variant) : TObject
   ,ToObject:function(Value$11) {
      var Result = null;
      Result = Value$11;
      return Result
   }
   /// function TVariant.ValidRef(const aValue: Variant) : Boolean
   ,ValidRef:function(aValue$15) {
      return aValue$15!=undefined&&aValue$15!=null;
   }
   ,Destroy:TObject.Destroy
};
/// TTextFormation enumeration
var TTextFormation = { 256:"tfHex", 257:"tfOrdinal", 258:"tfFloat", 259:"tfQuote" };
/// function TStringHelper.ContainsHex(const Self: String) : Boolean
function TStringHelper$ContainsHex(Self$15) {
   var Result = false;
   var x$3 = 0;
   var LStart = 0;
   var LItem = "";
   var LLen = 0;
   Result = false;
   LLen = Self$15.length;
   if (LLen>=1) {
      LStart = 1;
      if (Self$15.charAt(0)=="$") {
         ++LStart;
         --LLen;
      } else {
         LItem = (Self$15.substr(0,1)).toLocaleUpperCase();
         Result = ($R[70].indexOf(LItem)+1)>0;
         if (!Result) {
            return Result;
         }
      }
      if (LLen>=1) {
         var $temp6;
         for(x$3=LStart,$temp6=Self$15.length;x$3<=$temp6;x$3++) {
            LItem = (Self$15.charAt(x$3-1)).toLocaleUpperCase();
            Result = ($R[70].indexOf(LItem)+1)>0;
            if (!Result) {
               break;
            }
         }
      }
   }
   return Result
}
/// function TStringHelper.ContainsOrdinal(const Self: String) : Boolean
function TStringHelper$ContainsOrdinal(Self$16) {
   var Result = false;
   var LLen$1 = 0,
      x$4 = 0;
   var LItem$1 = "";
   Result = false;
   LLen$1 = Self$16.length;
   if (LLen$1>=1) {
      var $temp7;
      for(x$4=1,$temp7=LLen$1;x$4<=$temp7;x$4++) {
         LItem$1 = Self$16.charAt(x$4-1);
         Result = ($R[69].indexOf(LItem$1)+1)>0;
         if (!Result) {
            break;
         }
      }
   }
   return Result
}
/// function TStringHelper.ContainsFloat(const Self: String) : Boolean
function TStringHelper$ContainsFloat(Self$17) {
   var Result = false;
   var x$5 = 0;
   var LItem$2 = "";
   var LLen$2 = 0;
   var LLine = false;
   Result = false;
   LLen$2 = Self$17.length;
   if (LLen$2>=1) {
      LLine = false;
      var $temp8;
      for(x$5=1,$temp8=LLen$2;x$5<=$temp8;x$5++) {
         LItem$2 = Self$17.charAt(x$5-1);
         if (LItem$2==".") {
            if (x$5==1&&LLen$2==1) {
               break;
            }
            if (x$5==1&&LLen$2>1) {
               LLine = true;
               continue;
            }
            if (x$5>1&&x$5<LLen$2) {
               if (LLine) {
                  break;
               } else {
                  LLine = true;
                  continue;
               }
            } else {
               break;
            }
         }
         Result = ("0123456789".indexOf(LItem$2)+1)>0;
         if (!Result) {
            break;
         }
      }
   }
   return Result
}
/// function TStringHelper.ContainsQuote(const Self: String) : Boolean
function TStringHelper$ContainsQuote(Self$18) {
   var Result = false;
   var LLen$3 = 0;
   var LStart$1 = 0;
   var LFound = false;
   var LQuote = ["",""];
   Result = false;
   LLen$3 = Self$18.length;
   if (LLen$3>=2) {
      LStart$1 = 1;
      while (LStart$1<=LLen$3) {
         if (Self$18.charAt(LStart$1-1)==" ") {
            ++LStart$1;
            continue;
         } else {
            break;
         }
      }
      LQuote[false?1:0] = "'";
      LQuote[true?1:0] = "\"";
      if (Self$18.charAt(LStart$1-1)!=LQuote[true?1:0]||Self$18.charAt(LStart$1-1)!=LQuote[false?1:0]) {
         return Result;
      }
      if (LStart$1>=LLen$3) {
         return Result;
      }
      ++LStart$1;
      LFound = false;
      while (LStart$1<=LLen$3) {
         if (Self$18.charAt(LStart$1-1)!=LQuote[true?1:0]||Self$18.charAt(LStart$1-1)!=LQuote[false?1:0]) {
            LFound = true;
         }
         ++LStart$1;
      }
      if (!LFound) {
         return Result;
      }
      if (LStart$1==LLen$3) {
         Result = true;
         return Result;
      }
      while (LStart$1<=LLen$3) {
         if (Self$18.charAt(LStart$1-1)!=" ") {
            LFound = false;
            break;
         } else {
            ++LStart$1;
         }
      }
      Result = LFound;
   }
   return Result
}
/// function TStringHelper.ContainsFormation(const Self: String; const Formation: TTextFormation) : Boolean
function TStringHelper$ContainsFormation(Self$19, Formation) {
   var Result = false;
   switch (Formation) {
      case 256 :
         Result = TStringHelper$ContainsHex(Self$19);
         break;
      case 257 :
         Result = TStringHelper$ContainsOrdinal(Self$19);
         break;
      case 258 :
         Result = TStringHelper$ContainsFloat(Self$19);
         break;
      case 259 :
         Result = TStringHelper$ContainsQuote(Self$19);
         break;
   }
   return Result
}
/// TStringBuilder = class (TObject)
var TStringBuilder = {
   $ClassName:"TStringBuilder",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.Data = "";
   }
   /// function TStringBuilder.Append(const Text: array of String) : TStringBuilder
   ,Append$1:function(Self, Text$4) {
      var Result = null;
      var x$6 = 0;
      if (Text$4.length>0) {
         var $temp9;
         for(x$6=0,$temp9=Text$4.length;x$6<$temp9;x$6++) {
            Self.Data = Self.Data+Text$4[x$6];
         }
      }
      Result = Self;
      return Result
   }
   /// function TStringBuilder.Append(const Text: String) : TStringBuilder
   ,Append:function(Self, Text$5) {
      var Result = null;
      Self.Data = Self.Data+Text$5;
      Result = Self;
      return Result
   }
   /// function TStringBuilder.AppendFormat(const Text: String; const Args: array of const) : TStringBuilder
   ,AppendFormat:function(Self, Text$6, Args) {
      var Result = null;
      Self.Data = Self.Data+Format(Text$6,Args.slice(0));
      Result = Self;
      return Result
   }
   /// procedure TStringBuilder.Clear()
   ,Clear:function(Self) {
      Self.Data = "";
   }
   /// function TStringBuilder.CopyTo(const Index: Integer; const Count: Integer) : String
   ,CopyTo:function(Self, Index, Count$15) {
      return Self.Data.substr(Index-1,Count$15);
   }
   /// constructor TStringBuilder.Create(const InitialText: String)
   ,Create$17:function(Self, InitialText) {
      Self.Data = InitialText;
      return Self
   }
   /// function TStringBuilder.Format(const Values: array of const) : TStringBuilder
   ,Format$1:function(Self, Values$10) {
      var Result = null;
      Self.Data = Format(Self.Data,Values$10.slice(0));
      Result = Self;
      return Result
   }
   /// procedure TStringBuilder.Pack()
   ,Pack:function(Self) {
      Self.Data = StrReplace(Self.Data,"  "," ");
   }
   /// function TStringBuilder.Replace(Pattern: String; ReplaceWith: String) : TStringBuilder
   ,Replace:function(Self, Pattern, ReplaceWith) {
      var Result = null;
      Self.Data = StrReplace(Self.Data,Pattern,ReplaceWith);
      Result = Self;
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// function TStringArrayHelper.Pack(Self: TStrArray; const TrimItems: Boolean) : TStrArray
function TStringArrayHelper$Pack$1(Self$20, TrimItems) {
   var Result = [];
   var x$7 = 0,
      temp$2 = "";
   x$7 = 0;
   while (x$7<Self$20.length) {
      temp$2 = Self$20[x$7];
      if (TrimItems) {
         temp$2 = Trim$_String_(temp$2);
      }
      if (temp$2.length>0) {
         Result.push(temp$2);
      }
      ++x$7;
   }
   return Result
}
/// procedure TStringArrayHelper.Pack(Self: TStrArray)
function TStringArrayHelper$Pack$2(Self$21) {
   var x$8 = 0,
      temp$3 = "";
   x$8 = 0;
   while (x$8<Self$21.length) {
      temp$3 = Self$21[x$8];
      temp$3 = Trim$_String_(temp$3);
      if (temp$3.length<1) {
         Self$21.splice(x$8,1)
         ;
         if (x$8<(Self$21.length-1)) {
            continue;
         }
      }
      ++x$8;
   }
}
/// TString = class (TObject)
var TString = {
   $ClassName:"TString",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TString.BinaryStrToInt(BinStr: String) : Integer
   ,BinaryStrToInt:function(BinStr) {
      var Result = {v:0};
      try {
         if (!TString.ExamineBinary(BinStr,Result)) {
            throw EW3Exception.CreateFmt$($New(EConvertBinaryStringInvalid),"Failed to convert binary string (%s)",[BinStr]);
         }
      } finally {return Result.v}
   }
   /// procedure TString.BuildCRCTable()
   ,BuildCRCTable:function(Self) {
      var x$9 = 0;
      var y = 0;
      var r = 0;
      for(x$9=0;x$9<=255;x$9++) {
         r = x$9<<1;
         for(y=8;y>=0;y--) {
            if (r&1) {
               r = (r>>>1)^3988292384;
            } else {
               r = r>>>1;
            }
            CRC_Table[x$9] = r;
         }
      }
   }
   /// function TString.CalcCRC(Text: String) : Integer
   ,CalcCRC:function(Self, Text$7) {
      var Result = 0;
      var x$10 = 0;
      var LResult = 0;
      var LCharCode = 0;
      var LLen$4 = 0;
      var LBytes = [];
      LLen$4 = Text$7.length;
      if (LLen$4>0) {
         if (!CRC_Table_Ready) {
            TString.BuildCRCTable(Self);
            CRC_Table_Ready = true;
         }
         LBytes = TString.EncodeUTF8(Self,Text$7);
         LResult = 4294967295;
         var $temp10;
         for(x$10=0,$temp10=LBytes.length;x$10<$temp10;x$10++) {
            LCharCode = LBytes[x$10];
            LResult = (LResult>>>8)^CRC_Table[LCharCode^(LResult&255)];
         }
         Result = ( (LResult ^ -1) >>> 0);
      }
      return Result
   }
   /// function TString.CharCodeAt(const Text: String; const Index: Integer) : Byte
   ,CharCodeAt:function(Self, Text$8, Index$1) {
      var Result = 0;
      var LIndex = 0;
      LIndex = (Index$1>0)?Index$1-1:Index$1;
      Result = (Text$8).charCodeAt(LIndex);
      return Result
   }
   /// function TString.CharCodeFor(const Character: Char) : Integer
   ,CharCodeFor:function(Self, Character) {
      var Result = 0;
      Result = (Character).charCodeAt(0);
      return Result
   }
   /// function TString.CreateGUID() : String
   ,CreateGUID:function(Self) {
      var Result = "";
      var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = "-";

    Result = s.join("");
      Result = (Result).toUpperCase();
      return Result
   }
   /// function TString.DecodeBase64(TextToDecode: String) : String
   ,DecodeBase64:function(Self, TextToDecode) {
      return TBase64EncDec.Base64ToString(TBase64EncDec,TextToDecode);
   }
   /// function TString.DecodeURI(const Text: String) : String
   ,DecodeURI:function(Self, Text$9) {
      var Result = "";
      Result = (Text$9).replace(new RegExp('\\+','g'),' ');
    Result = unescape(Result);
      return Result
   }
   /// function TString.DecodeURIComponent(const Text: String) : String
   ,DecodeURIComponent:function(Self, Text$10) {
      var Result = "";
      Result = decodeURIComponent(Text$10);
      return Result
   }
   /// function TString.DecodeUTF8(const BytesToDecode: TByteArray) : String
   ,DecodeUTF8:function(Self, BytesToDecode) {
      var Result = "";
      var LCodec = null;
      LCodec = TCustomCodec.Create$28$($New(TUTF8Codec));
      try {
         Result = TUTF8Codec.Decode$(LCodec,BytesToDecode);
      } finally {
         TObject.Free(LCodec);
      }
      return Result
   }
   /// function TString.EncodeBase64(TextToEncode: String) : String
   ,EncodeBase64:function(Self, TextToEncode) {
      return TBase64EncDec.StringToBase64(TBase64EncDec,TextToEncode);
   }
   /// function TString.EncodeTags(const Text: String) : String
   ,EncodeTags:function(Self, Text$11) {
      var Result = "";
      var LastAdded = 0;
      var Part = "";
      var i = 0,
         j = 0;
      function AddUntil(EndPos) {
         var ii = 0;
         var ch = "";
         var $temp11;
         for(ii=LastAdded+1,$temp11=EndPos;ii<=$temp11;ii++) {
            ch = Text$11.charAt(ii-1);
            {var $temp12 = ch;
               if ($temp12=="\"") {
                  Result+="&quot;";
               }
                else if ($temp12=="'") {
                  Result+="&#39;";
               }
                else if ($temp12=="<") {
                  Result+="&lt;";
               }
                else if ($temp12==">") {
                  Result+="&gt;";
               }
                else if ($temp12=="\/") {
                  Result+="&#47;";
               }
                else {
                  Result+=ch;
               }
            }
         }
         LastAdded = EndPos;
      };
      function CheckAllowed(Value$12) {
         var Result = false;
         var lc = "",
            ii$1 = 0;
         lc = (Value$12).toLowerCase();
         for(ii$1=0;ii$1<=5;ii$1++) {
            if (Allowed[ii$1]==lc) {
               Result = true;
               return Result;
            }
         }
         return Result
      };
      var Allowed = ["&amp;","&lt;","&gt;","&quot;","&#39;","&#47;"];
      i = (Text$11.indexOf("&")+1);
      while (i>0) {
         AddUntil((i-1));
         Part = Text$11.substr(i-1,6);
         j = (Part.indexOf(";")+1);
         if (j>0) {
            if (j<6) {
               Part = Part.substr(0,j);
            }
            if (CheckAllowed(Part)) {
               Result+=Part;
               (LastAdded+= Part.length);
               (i+= (Part.length-1));
            } else {
               Result+="&amp;";
               ++LastAdded;
            }
         } else {
            Result+="&amp;";
            ++LastAdded;
         }
         i = (Text$11.indexOf("&",(i+1)-1)+1);
      }
      AddUntil(Text$11.length);
      return Result
   }
   /// function TString.EncodeURI(const Text: String) : String
   ,EncodeURI:function(Self, Text$12) {
      var Result = "";
      Result = escape(Text$12);
    Result = (Result).replace(new RegExp('\\+','g'),'%2B');
    Result = (Result).replace(new RegExp('%20','g'),'+');
      return Result
   }
   /// function TString.EncodeURIComponent(const Text: String) : String
   ,EncodeURIComponent:function(Self, Text$13) {
      var Result = "";
      Result = encodeURIComponent(Text$13);
      return Result
   }
   /// function TString.EncodeUTF8(TextToEncode: String) : TByteArray
   ,EncodeUTF8:function(Self, TextToEncode$1) {
      var Result = [];
      var LCodec$1 = null;
      LCodec$1 = TCustomCodec.Create$28$($New(TUTF8Codec));
      try {
         Result = TUTF8Codec.Encode$(LCodec$1,TextToEncode$1);
      } finally {
         TObject.Free(LCodec$1);
      }
      return Result
   }
   /// function TString.Equals(const First: String; const Second: String) : Boolean
   ,Equals:function(Self, First$2, Second) {
      return (Trim$_String_(First$2)).toLowerCase()==(Trim$_String_(Second)).toLowerCase();
   }
   /// function TString.ExamineBinary(Text: String; var value: Longword) : Boolean
   ,ExamineBinary:function(Text$14, value) {
      var Result = false;
      var BitIndex = 0,
         x$11 = 0;
      value.v = TDataTypeConverter.InitUint32(0);
      if ((Text$14.charAt(0)=="%")) {
         Text$14 = (Text$14).substring(1);
      } else if ((Text$14.substr(0,2)=="0b")) {
         Text$14 = (Text$14).substring(2);
      }
      if (!TString.ValidBinChars(Text$14)) {
         Result = false;
         return Result;
      }
      BitIndex = 0;
      for(x$11=Text$14.length;x$11>=1;x$11--) {
         if (Text$14.charAt(x$11-1)=="1") {
            TInteger.SetBit(BitIndex,true,value);
         }
         ++BitIndex;
         if (BitIndex>31) {
            break;
         }
      }
      Result = true;
      return Result
   }
   /// function TString.ExamineBoolean(Text: String; var Value: Boolean) : Boolean
   ,ExamineBoolean:function(Text$15, Value$13) {
      var Result = false;
      Text$15 = Trim$_String_(Text$15);
      {var $temp13 = (Text$15).toLocaleLowerCase();
         if ($temp13=="true") {
            Result = true;
            Value$13.v = true;
         }
          else if ($temp13=="yes") {
            Result = true;
            Value$13.v = true;
         }
          else if ($temp13=="false") {
            Result = true;
            Value$13.v = false;
         }
          else if ($temp13=="no") {
            Result = true;
            Value$13.v = false;
         }
      }
      return Result
   }
   /// function TString.ExamineFloat(Text: String; var Value: Float) : Boolean
   ,ExamineFloat:function(Text$16, Value$14) {
      var Result = false;
      var TextLen = 0,
         scan = false,
         offset = 0,
         character = "";
      Text$16 = Trim$_String_(Text$16);
      TextLen = Text$16.length;
      if (TextLen>=1) {
         scan = false;
         offset = 0;
         for (var $temp14=0;$temp14<Text$16.length;$temp14++) {
            character=$uniCharAt(Text$16,$temp14);
            if (!character) continue;
            ++offset;
            if (character==".") {
               if (offset==1&&TextLen==1) {
                  break;
               }
               if (offset==1&&TextLen>1) {
                  scan = true;
                  continue;
               }
               if (offset>1&&offset<TextLen) {
                  if (scan) {
                     break;
                  } else {
                     scan = true;
                     continue;
                  }
               } else {
                  break;
               }
            }
            Result = ((character>="0")&&(character<="9"));
            if (!Result) {
               break;
            }
         }
         if (Result) {
            Value$14.v = parseFloat(Text$16);
         }
      }
      return Result
   }
   /// function TString.ExamineInteger(Text: String; var Value: Integer) : Boolean
   ,ExamineInteger:function(Text$17, Value$15) {
      var Result = false;
      var TextLen$1 = 0,
         Prefix$1 = 0;
      Text$17 = Trim$_String_(Text$17);
      TextLen$1 = Text$17.length;
      if (TextLen$1>0) {
         Prefix$1 = TString.ExaminePrefixType(Text$17);
         if (Prefix$1) {
            switch (Prefix$1) {
               case 1 :
                  --TextLen$1;
                  Text$17 = RightStr(Text$17,TextLen$1);
                  Result = TString.ValidHexChars(Text$17);
                  if (Result) {
                     Value$15.v = parseInt("0x"+Text$17,16);
                  }
                  break;
               case 2 :
                  (TextLen$1-= 2);
                  Text$17 = RightStr(Text$17,TextLen$1);
                  Result = TString.ValidHexChars(Text$17);
                  if (Result) {
                     Value$15.v = parseInt("0x"+Text$17,16);
                  }
                  break;
               case 3 :
                  --TextLen$1;
                  Text$17 = RightStr(Text$17,TextLen$1);
                  Result = TString.ValidBinChars(Text$17);
                  if (Result) {
                     Value$15.v = TString.BinaryStrToInt(Text$17);
                  }
                  break;
               case 4 :
                  (TextLen$1-= 2);
                  Text$17 = RightStr(Text$17,TextLen$1);
                  Result = TString.ValidBinChars(Text$17);
                  if (Result) {
                     Value$15.v = TString.BinaryStrToInt(Text$17);
                  }
                  break;
               case 5 :
                  return Result;
                  break;
               default :
                  Result = TString.ValidDecChars(Text$17);
                  if (Result) {
                     Value$15.v = parseInt(Text$17,10);
                  }
            }
         } else {
            Result = TString.ValidDecChars(Text$17);
            if (Result) {
               Value$15.v = parseInt(Text$17,10);
            }
         }
      }
      return Result
   }
   /// function TString.ExaminePrefixType(const Text: String) : TValuePrefixType
   ,ExaminePrefixType:function(Text$18) {
      var Result = 0;
      Result = 0;
      if (Text$18.length>0) {
         if ((Text$18.charAt(0)=="$")) {
            Result = 1;
         } else if ((Text$18.substr(0,2)=="0x")) {
            Result = 2;
         } else if ((Text$18.charAt(0)=="%")) {
            Result = 3;
         } else if ((Text$18.substr(0,2)=="0b")) {
            Result = 4;
         } else if ((Text$18.charAt(0)=="\"")) {
            Result = 5;
         }
      }
      return Result
   }
   /// function TString.ExamineQuotedString(Text: String; var Value: String) : Boolean
   ,ExamineQuotedString:function(Text$19, Value$16) {
      var Result = false;
      var TextLen$2 = 0;
      Text$19 = Trim$_String_(Text$19);
      TextLen$2 = Text$19.length;
      if (TextLen$2>0) {
         if ((Text$19.charAt(0)=="\"")) {
            if (StrEndsWith(Text$19,"\"")) {
               (TextLen$2-= 2);
               Value$16.v = Text$19.substr(1,TextLen$2);
               Result = true;
            }
         }
      }
      return Result
   }
   /// function TString.Explode(const Value: String; const Delimiter: String) : TStrArray
   ,Explode:function(Self, Value$17, Delimiter) {
      var Result = [];
      Result = (Value$17).split(Delimiter);
      return Result
   }
   /// function TString.FillChar(Count: Integer; const Charcode: Integer) : String
   ,FillChar:function(Self, Count$16, Charcode) {
      var Result = "";
      var LCache = "";
      if (Count$16>0) {
         LCache = String.fromCharCode(Charcode);
         while (Count$16>0) {
            Result+=LCache;
            --Count$16;
         }
      }
      return Result
   }
   /// function TString.ForEach(const Text: String; const Callback: TW3StringEvaluationProc) : String
   ,ForEach:function(Self, Text$20, Callback$1) {
      var Result = "";
      var x$12 = 0;
      var LSample = "";
      if (Callback$1) {
         var $temp15;
         for(x$12=1,$temp15=Text$20.length;x$12<=$temp15;x$12++) {
            LSample = Text$20.charAt(x$12-1);
            if (Callback$1(LSample)) {
               Result+=LSample;
            }
         }
      } else {
         Result = Text$20;
      }
      return Result
   }
   /// function TString.FromCharCode(const CharCode: Byte) : Char
   ,FromCharCode:function(Self, CharCode) {
      var Result = "";
      Result = String.fromCharCode(CharCode);
      return Result
   }
   /// function TString.GetStringIsQuoted(const Text: String) : Boolean
   ,GetStringIsQuoted:function(Text$21) {
      var Result = false;
      var TextLen$3 = 0;
      TextLen$3 = Text$21.length;
      if (TextLen$3>0) {
         Result = (Text$21.charAt(0)=="\"")&&StrEndsWith(Text$21,"\"");
      }
      return Result
   }
   /// function TString.HexStrToInt(HexStr: String) : Integer
   ,HexStrToInt:function(HexStr) {
      var Result = {v:0};
      try {
         if (!TString.ExamineInteger(HexStr,Result)) {
            throw EW3Exception.CreateFmt$($New(EConvertHexStringInvalid),"Failed to convert hex string (%s)",[HexStr]);
         }
      } finally {return Result.v}
   }
   /// function TString.LeftPad(Pad: Char; Value: String; FitLength: Integer) : String
   ,LeftPad:function(Self, Pad, Value$18, FitLength) {
      var Result = "";
      Result = Value$18;
      Pad = Pad.charAt(0);
      if (FitLength>Result.length) {
         while (Result.length<FitLength) {
            Result = Pad.charAt(0)+Result;
         }
      }
      return Result
   }
   /// function TString.QuoteString(const Text: String) : String
   ,QuoteString:function(Text$22) {
      var Result = "";
      if (TString.GetStringIsQuoted(Text$22)) {
         Result = Text$22;
      } else {
         Result = "\""+Text$22+"\"";
      }
      return Result
   }
   /// function TString.ResolveDataType(Text: String; var Determined: TRTLDatatype) : Boolean
   ,ResolveDataType:function(Text$23, Determined) {
      var Result = false;
      var DummyBoolean = {};
      DummyBoolean.v = false;
      var DummyFloat = {};
      DummyFloat.v = 0;
      var LPrefix = 0;
      Result = false;
      Determined.v = 0;
      if (TString.ExamineBoolean(Text$23,DummyBoolean)) {
         Determined.v = 1;
         Result = true;
         return Result;
      }
      if (TString.ExamineFloat(Text$23,DummyFloat)) {
         Determined.v = 9;
         Result = true;
         return Result;
      }
      if (TString.ValidDecChars(Text$23)) {
         Determined.v = 7;
         Result = true;
         return Result;
      }
      LPrefix = TString.ExaminePrefixType(Text$23);
      if (LPrefix) {
         switch (LPrefix) {
            case 5 :
               Determined.v = 10;
               break;
            case 1 :
               Text$23 = RightStr(Text$23,Text$23.length-1);
               switch (Text$23.length) {
                  case 1 :
                     Determined.v = 2;
                     break;
                  case 2 :
                     Determined.v = 2;
                     break;
                  case 4 :
                     Determined.v = 4;
                     break;
                  case 8 :
                     Determined.v = 5;
                     break;
               }
               break;
            case 2 :
               Text$23 = RightStr(Text$23,Text$23.length-2);
               switch (Text$23.length) {
                  case 1 :
                     Determined.v = 2;
                     break;
                  case 2 :
                     Determined.v = 2;
                     break;
                  case 4 :
                     Determined.v = 4;
                     break;
                  case 8 :
                     Determined.v = 5;
                     break;
               }
               break;
            case 3 :
               Text$23 = RightStr(Text$23,Text$23.length-1);
               if (Text$23.length<=8) {
                  Determined.v = 2;
               } else if (Text$23.length>8&&Text$23.length<=16) {
                  Determined.v = 4;
               } else if (Text$23.length>16&&Text$23.length<=32) {
                  Determined.v = 5;
               }
               break;
            case 4 :
               Text$23 = RightStr(Text$23,Text$23.length-2);
               if (Text$23.length<=8) {
                  Determined.v = 2;
               } else if (Text$23.length>8&&Text$23.length<=16) {
                  Determined.v = 4;
               } else if (Text$23.length>16&&Text$23.length<=32) {
                  Determined.v = 5;
               }
               break;
         }
         Result = (Determined.v!=0);
      }
      return Result
   }
   /// function TString.UnQuoteString(const Text: String) : String
   ,UnQuoteString:function(Text$24) {
      var Result = "";
      var TextLen$4 = 0;
      TextLen$4 = Text$24.length;
      if (TextLen$4>0) {
         if ((Text$24.charAt(0)=="\"")&&StrEndsWith(Text$24,"\"")) {
            (TextLen$4-= 2);
            Result = Text$24.substr(1,TextLen$4);
         } else {
            Result = Text$24;
         }
      }
      return Result
   }
   /// function TString.ValidBinChars(const Text: String) : Boolean
   ,ValidBinChars:function(Text$25) {
      var Result = false;
      var character$1 = "";
      for (var $temp16=0;$temp16<Text$25.length;$temp16++) {
         character$1=$uniCharAt(Text$25,$temp16);
         if (!character$1) continue;
         Result = ((character$1=="0")||(character$1=="1"));
         if (!Result) {
            break;
         }
      }
      return Result
   }
   /// function TString.ValidDecChars(const Text: String) : Boolean
   ,ValidDecChars:function(Text$26) {
      var Result = false;
      var character$2 = "";
      for (var $temp17=0;$temp17<Text$26.length;$temp17++) {
         character$2=$uniCharAt(Text$26,$temp17);
         if (!character$2) continue;
         Result = ((character$2>="0")&&(character$2<="9"));
         if (!Result) {
            break;
         }
      }
      return Result
   }
   /// function TString.ValidHexChars(const Text: String) : Boolean
   ,ValidHexChars:function(Text$27) {
      var Result = false;
      var character$3 = "";
      for (var $temp18=0;$temp18<Text$27.length;$temp18++) {
         character$3=$uniCharAt(Text$27,$temp18);
         if (!character$3) continue;
         Result = (((character$3>="0")&&(character$3<="9"))||((character$3>="a")&&(character$3<="f"))||((character$3>="A")&&(character$3<="F")));
         if (!Result) {
            break;
         }
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TRangeF = record
function Copy$TRangeF(s,d) {
   d.Maximum=s.Maximum;
   d.Minimum=s.Minimum;
   return d;
}
function Clone$TRangeF($) {
   return {
      Maximum:$.Maximum,
      Minimum:$.Minimum
   }
}
/// function TRangeF.ClipTo(var Self: TRangeF; const Value: Float) : Float
function TRangeF$ClipTo(Self$22, Value$19) {
   return (Value$19>Self$22.Maximum)?Self$22.Maximum:(Value$19<Self$22.Minimum)?Self$22.Minimum:Value$19;
}
/// function TRangeF.Create(const JObj: Variant) : TRangeF
function Create$19(JObj) {
   var Result = {Maximum:0,Minimum:0};
   if (JObj) {
      Result.Minimum = TVariant.AsInteger(JObj["Minimum"]);
      Result.Maximum = TVariant.AsInteger(JObj["Maximum"]);
   } else {
      throw Exception.Create($New(EW3Exception),"Failed to create range, Javascript object was unassigned or null error");
   }
   return Result
}
/// function TRangeF.Create(const aMinimum: Float; const aMaximum: Float) : TRangeF
function Create$18(aMinimum, aMaximum) {
   var Result = {Maximum:0,Minimum:0};
   Result.Minimum = aMinimum;
   Result.Maximum = aMaximum;
   return Result
}
/// function TRangeF.Inside(var Self: TRangeF; const Value: Float) : Boolean
function TRangeF$Inside(Self$23, Value$20) {
   return Value$20>Self$23.Minimum&&Value$20<Self$23.Maximum;
}
/// function TRangeF.OffBy(var Self: TRangeF; const Value: Float) : Float
function TRangeF$OffBy(Self$24, Value$21) {
   return (Value$21<Self$24.Minimum)?Self$24.Minimum-Value$21:(Value$21>Self$24.Maximum)?Value$21-Self$24.Maximum:0;
}
/// procedure TRangeF.Reset(var Self: TRangeF)
function TRangeF$Reset$1(Self$25) {
   Self$25.Minimum = 0;
   Self$25.Maximum = 0;
}
/// function TRangeF.Valid(var Self: TRangeF) : Boolean
function TRangeF$Valid(Self$26) {
   return Self$26.Minimum<Self$26.Maximum;
}
/// function TRangeF.Within(var Self: TRangeF; const Value: Float) : Boolean
function TRangeF$Within(Self$27, Value$22) {
   return Value$22>=Self$27.Minimum&&Value$22<=Self$27.Maximum;
}
/// TRange = record
function Copy$TRange(s,d) {
   d.Maximum$1=s.Maximum$1;
   d.Minimum$1=s.Minimum$1;
   return d;
}
function Clone$TRange($) {
   return {
      Maximum$1:$.Maximum$1,
      Minimum$1:$.Minimum$1
   }
}
/// function TRange.ClipTo(var Self: TRange; const Value: Integer) : Integer
function TRange$ClipTo$1(Self$28, Value$23) {
   return (Value$23>Self$28.Maximum$1)?Self$28.Maximum$1:(Value$23<Self$28.Minimum$1)?Self$28.Minimum$1:Value$23;
}
/// function TRange.Create(const JObj: Variant) : TRange
function Create$21(JObj$1) {
   var Result = {Maximum$1:0,Minimum$1:0};
   if (JObj$1) {
      Result.Minimum$1 = TVariant.AsInteger(JObj$1["Minimum"]);
      Result.Maximum$1 = TVariant.AsInteger(JObj$1["Maximum"]);
   } else {
      throw Exception.Create($New(EW3Exception),"Failed to create range, Javascript object was unassigned or null error");
   }
   return Result
}
/// function TRange.Create(const aMinimum: Integer; const aMaximum: Integer) : TRange
function Create$20(aMinimum$1, aMaximum$1) {
   var Result = {Maximum$1:0,Minimum$1:0};
   Result.Minimum$1 = aMinimum$1;
   Result.Maximum$1 = aMaximum$1;
   return Result
}
/// function TRange.Inside(var Self: TRange; const Value: Integer) : Boolean
function TRange$Inside$1(Self$29, Value$24) {
   return Value$24>Self$29.Minimum$1&&Value$24<Self$29.Maximum$1;
}
/// function TRange.OffBy(var Self: TRange; const Value: Integer) : Integer
function TRange$OffBy$1(Self$30, Value$25) {
   return (Value$25<Self$30.Minimum$1)?Self$30.Minimum$1-Value$25:(Value$25>Self$30.Maximum$1)?Value$25-Self$30.Maximum$1:0;
}
/// procedure TRange.Reset(var Self: TRange)
function TRange$Reset$2(Self$31) {
   Self$31.Minimum$1 = 0;
   Self$31.Maximum$1 = 0;
}
/// function TRange.Valid(var Self: TRange) : Boolean
function TRange$Valid$1(Self$32) {
   return Self$32.Minimum$1<Self$32.Maximum$1;
}
/// function TRange.Within(var Self: TRange; const Value: Integer) : Boolean
function TRange$Within$1(Self$33, Value$26) {
   return Value$26>=Self$33.Minimum$1&&Value$26<=Self$33.Maximum$1;
}
/// TInterfacedObject = class (TObject)
var TInterfacedObject = {
   $ClassName:"TInterfacedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// TInteger = class (TObject)
var TInteger = {
   $ClassName:"TInteger",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TInteger.Average(const anArray: TIntArray) : Float
   ,Average:function(anArray) {
      var Result = 0;
      var mCount = 0;
      mCount = anArray.length;
      if (!mCount) {
         Result = 0;
      } else {
         Result = TInteger.Sum(anArray)/mCount;
      }
      return Result
   }
   /// function TInteger.Diff(const Primary: Integer; const Secondary: Integer) : Integer
   ,Diff:function(Primary, Secondary) {
      var Result = 0;
      if (Primary!=Secondary) {
         if (Primary>Secondary) {
            Result = Primary-Secondary;
         } else {
            Result = Secondary-Primary;
         }
         if (Result<0) {
            Result = (Result-1)^(-1);
         }
      } else {
         Result = 0;
      }
      return Result
   }
   /// function TInteger.EnsureRange(const aValue: Integer; const aMin: Integer; const aMax: Integer) : Integer
   ,EnsureRange:function(aValue$16, aMin, aMax) {
      return ClampInt(aValue$16,aMin,aMax);
   }
   /// function TInteger.FromPxStr(const aValue: String) : Integer
   ,FromPxStr:function(aValue$17) {
      var Result = 0;
      var LText$2 = "";
      if (StrEndsWith((aValue$17).toLocaleLowerCase(),"px")) {
         LText$2 = aValue$17.substr(0,(aValue$17.length-2));
         if (TVariant.IsNumber(LText$2)) {
            Result = parseInt(LText$2,10);
         }
      }
      return Result
   }
   /// function TInteger.GetBit(index: Integer; const buffer: Integer) : Boolean
   ,GetBit:function(index$1, buffer$1) {
      var Result = false;
      if (index$1>=0&&index$1<=31) {
         Result = ((buffer$1&(1<<index$1))!=0);
      } else {
         throw Exception.Create($New(Exception),"Invalid bit index, expected 0..31");
      }
      return Result
   }
   /// function TInteger.Largest(const anArray: TIntArray) : Integer
   ,Largest$1:function(Self, anArray$1) {
      var Result = 0;
      var a$452 = 0;
      var x$13 = 0;
      var $temp19;
      for(a$452=0,$temp19=anArray$1.length;a$452<$temp19;a$452++) {
         x$13 = anArray$1[a$452];
         if (x$13>Result) {
            Result = x$13;
         }
      }
      return Result
   }
   /// function TInteger.Largest(const Primary: Integer; const Secondary: Integer) : Integer
   ,Largest:function(Self, Primary$1, Secondary$1) {
      var Result = 0;
      if (Primary$1>Secondary$1) {
         Result = Primary$1;
      } else {
         Result = Secondary$1;
      }
      return Result
   }
   /// function TInteger.Middle(const Primary: Integer; const Secondary: Integer) : Integer
   ,Middle:function(Primary$2, Secondary$2) {
      return $Div(Primary$2+Secondary$2,2);
   }
   /// function TInteger.PercentOfValue(const Value: Integer; const Total: Integer) : Integer
   ,PercentOfValue:function(Value$27, Total) {
      var Result = 0;
      if (Value$27<=Total) {
         Result = Trunc(Value$27/Total*100);
      } else {
         Result = 0;
      }
      return Result
   }
   /// function TInteger.Power(const Value: Integer; const Factor: Integer) : Integer
   ,Power$1:function(Value$28, Factor) {
      var Result = 0;
      Result = math.pow(Value$28,Factor);
      return Result
   }
   /// procedure TInteger.SetBit(index: Integer; aValue: Boolean; var buffer: Integer)
   ,SetBit:function(index$2, aValue$18, buffer$2) {
      if (index$2>=0&&index$2<=31) {
         if (aValue$18) {
            buffer$2.v = buffer$2.v|(1<<index$2);
         } else {
            buffer$2.v = buffer$2.v&(~(1<<index$2));
         }
      } else {
         throw Exception.Create($New(Exception),"Invalid bit index, expected 0..31");
      }
   }
   /// function TInteger.Sign(const Value: Integer) : Integer
   ,Sign$3:function(Self, Value$29) {
      return $Sign(Value$29);
   }
   /// function TInteger.Smallest(const anArray: TIntArray) : Integer
   ,Smallest$1:function(Self, anArray$2) {
      var Result = 0;
      var a$453 = 0;
      var x$14 = 0;
      Result = (anArray$2.length-1);
      if (Result>=0) {
         Result = anArray$2[0];
         var $temp20;
         for(a$453=0,$temp20=anArray$2.length;a$453<$temp20;a$453++) {
            x$14 = anArray$2[a$453];
            if (x$14<Result) {
               Result = x$14;
            }
         }
      }
      return Result
   }
   /// function TInteger.Smallest(const Primary: Integer; const Secondary: Integer) : Integer
   ,Smallest:function(Self, Primary$3, Secondary$3) {
      var Result = 0;
      if (Primary$3<Secondary$3) {
         Result = Primary$3;
      } else {
         Result = Secondary$3;
      }
      return Result
   }
   /// procedure TInteger.Sort(var Domain: TIntArray)
   ,Sort:function(Self, Domain) {
      Domain.v.sort($CmpNum);
   }
   /// function TInteger.SubtractLargest(const First: Integer; const Second: Integer) : Integer
   ,SubtractLargest:function(First$3, Second$1) {
      var Result = 0;
      if (First$3>Second$1) {
         Result = Second$1-First$3;
      } else {
         Result = First$3-Second$1;
      }
      return Result
   }
   /// function TInteger.SubtractSmallest(const First: Integer; const Second: Integer) : Integer
   ,SubtractSmallest:function(First$4, Second$2) {
      var Result = 0;
      if (First$4<Second$2) {
         Result = Second$2-First$4;
      } else {
         Result = First$4-Second$2;
      }
      return Result
   }
   /// function TInteger.Sum(const anArray: TIntArray) : Integer
   ,Sum:function(anArray$3) {
      var Result = 0;
      var a$454 = 0;
      var x$15 = 0;
      Result = 0;
      var $temp21;
      for(a$454=0,$temp21=anArray$3.length;a$454<$temp21;a$454++) {
         x$15 = anArray$3[a$454];
         Result+=x$15;
      }
      return Result
   }
   /// procedure TInteger.Swap(var Primary: Integer; var Secondary: Integer)
   ,Swap:function(Primary$4, Secondary$4) {
      var temp$4 = 0;
      temp$4 = Primary$4.v;
      Primary$4.v = Secondary$4.v;
      Secondary$4.v = temp$4;
   }
   /// function TInteger.ToHex(const aValue: Integer) : String
   ,ToHex:function(aValue$19) {
      var Result = "";
      Result = IntToHex(aValue$19,16);
      if (Result.length<2) {
         do {
            Result = "0"+Result;
         } while (!(Result.length>=2));
      }
      Result = Result.substr((Result.length-1)-1,2);
      return Result
   }
   /// function TInteger.ToNearest(const Value: Integer; const Factor: Integer) : Integer
   ,ToNearest:function(Value$30, Factor$1) {
      var Result = 0;
      var FTemp = 0;
      Result = Value$30;
      FTemp = Value$30%Factor$1;
      if (FTemp>0) {
         (Result+= (Factor$1-FTemp));
      }
      return Result
   }
   /// function TInteger.ToNearestSigned(const Value: Integer; const Factor: Integer) : Integer
   ,ToNearestSigned:function(Value$31, Factor$2) {
      return (1+($Div(Value$31-1,Factor$2)))*Factor$2;
   }
   /// function TInteger.ToPsStr(const Percent: Integer) : String
   ,ToPsStr:function(Percent) {
      return Percent.toString()+"%";
   }
   /// function TInteger.ToPxStr(const aValue: Integer) : String
   ,ToPxStr:function(aValue$20) {
      return aValue$20.toString()+"px";
   }
   /// function TInteger.WithinRange(const Value: Integer; const Lowest: Integer; const Highest: Integer) : Boolean
   ,WithinRange:function(Value$32, Lowest, Highest) {
      return Value$32>=Lowest&&Value$32<=Highest;
   }
   /// function TInteger.WrapRange(const aValue: Integer; const aLowRange: Integer; const aHighRange: Integer) : Integer
   ,WrapRange:function(aValue$21, aLowRange, aHighRange) {
      var Result = 0;
      if (aValue$21>aHighRange) {
         Result = aLowRange+TInteger.Diff(aHighRange,(aValue$21-1));
         if (Result>aHighRange) {
            Result = TInteger.WrapRange(Result,aLowRange,aHighRange);
         }
      } else if (aValue$21<aLowRange) {
         Result = aHighRange-TInteger.Diff(aLowRange,(aValue$21+1));
         if (Result<aLowRange) {
            Result = TInteger.WrapRange(Result,aLowRange,aHighRange);
         }
      } else {
         Result = aValue$21;
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// function THandleHelper.Valid(const Self: THandle) : Boolean
function THandleHelper$Valid$3(Self$34) {
   var Result = false;
   Result = !( (Self$34 == undefined) || (Self$34 == null) );
   return Result
}
/// function THandleHelper.Defined(const Self: THandle) : Boolean
function THandleHelper$Defined$1(Self$35) {
   var Result = false;
   Result = !(Self$35 == undefined);
   return Result
}
/// function THandleHelper.UnDefined(const Self: THandle) : Boolean
function THandleHelper$UnDefined$1(Self$36) {
   var Result = false;
   Result = (Self$36 == undefined);
   return Result
}
/// function THandleHelper.Equals(const Self: THandle; const Source: THandle) : Boolean
function THandleHelper$Equals$6(Self$37, Source$1) {
   var Result = false;
   Result = (Self$37 == Source$1);
   return Result
}
/// TFloat = class (TObject)
var TFloat = {
   $ClassName:"TFloat",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TFloat.PercentOfValue(const Value: double; Total: double) : double
   ,PercentOfValue$1:function(Value$33, Total$1) {
      var Result = undefined;
      if (Value$33<=Total$1) {
         Result = Value$33/Total$1*100;
      }
      return Result
   }
   /// function TFloat.Power(const Value: double; const Factor: double) : double
   ,Power$2:function(Value$34, Factor$3) {
      var Result = undefined;
      Result = Math.pow(Value$34,Factor$3);
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TFileAccessMode enumeration
var TFileAccessMode = [ "fmOpenRead", "fmOpenWrite", "fmOpenReadWrite" ];
/// TEnumState enumeration
var TEnumState = { 1:"esContinue", 0:"esAbort" };
/// TEnumResult enumeration
var TEnumResult = { 160:"erContinue", 16:"erBreak" };
/// TDataTypeMap = record
function Copy$TDataTypeMap(s,d) {
   d.Boolean=s.Boolean;
   d.Number$1=s.Number$1;
   d.String$1=s.String$1;
   d.Object$2=s.Object$2;
   d.Undefined=s.Undefined;
   d.Function$1=s.Function$1;
   return d;
}
function Clone$TDataTypeMap($) {
   return {
      Boolean:$.Boolean,
      Number$1:$.Number$1,
      String$1:$.String$1,
      Object$2:$.Object$2,
      Undefined:$.Undefined,
      Function$1:$.Function$1
   }
}
/// TCriticalSectionData = record
function Copy$TCriticalSectionData(s,d) {
   d.__csdID=s.__csdID;
   d.__csdCB=s.__csdCB;
   return d;
}
function Clone$TCriticalSectionData($) {
   return {
      __csdID:$.__csdID,
      __csdCB:$.__csdCB
   }
}
/// procedure TCriticalSectionData.Reset(var Self: TCriticalSectionData)
function TCriticalSectionData$Reset$3(Self$38) {
   Self$38.__csdID = -1;
   Self$38.__csdCB = null;
}
/// function TCriticalSectionData.Create(const Id: Integer; const CB: TStdCallback) : TCriticalSectionData
function Create$22(Id$4, CB$5) {
   var Result = {__csdID:0,__csdCB:null};
   Result.__csdID = Id$4;
   Result.__csdCB = CB$5;
   return Result
}
/// TCriticalSection = class (TObject)
var TCriticalSection = {
   $ClassName:"TCriticalSection",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FCurrent = {__csdID:0,__csdCB:null};
      $.FKilled = false;
      $.FLock = 0;
      $.FStack = [];
   }
   /// anonymous TSourceMethodSymbol
   ,a$19:function(Self) {
      return Self.FLock>0;
   }
   /// procedure TCriticalSection.Acquire(const CB: TStdCallback)
   ,Acquire:function(Self, CB$6) {
      if (!Self.FKilled) {
         if (CB$6) {
            if (!Self.FLock) {
               ++Self.FLock;
               ++__CSUniqueId;
               Self.FCurrent = Create$22(__CSUniqueId,CB$6);
               CB$6(true);
            } else {
               TCriticalSection.PushLockRequestToStack(Self,CB$6);
            }
         } else {
            throw EW3Exception.CreateFmt$($New(EW3CriticalSection),"Method %s.%s failed, valid callback is required error",[TObject.ClassName(Self.ClassType), "TCriticalSection.Acquire"]);
         }
      }
   }
   /// procedure TCriticalSection.CancelStackWith(const SignalValue: Boolean)
   ,CancelStackWith:function(Self, SignalValue) {
      var a$455 = 0;
      var temp$5 = {__csdID:0,__csdCB:null};
      if (Self.FStack.length>0) {
         try {
            var a$456 = [];
            a$456 = Self.FStack;
            var $temp22;
            for(a$455=0,$temp22=a$456.length;a$455<$temp22;a$455++) {
               Copy$TCriticalSectionData(a$456[a$455],temp$5);
               try {
                  if (temp$5.__csdCB) {
                     temp$5.__csdCB(SignalValue);
                  }
               } catch ($e) {
                  /* null */
               }
            }
         } finally {
            Self.FStack.length=0;
         }
      }
   }
   /// procedure TCriticalSection.CBNoBehavior(Success: Boolean)
   ,CBNoBehavior:function(Self, Success$11) {
      /* null */
   }
   /// destructor TCriticalSection.Destroy()
   ,Destroy:function(Self) {
      Self.FKilled = true;
      Self.FLock = -1;
      TCriticalSection.CancelStackWith(Self,false);
      TObject.Destroy(Self);
   }
   /// procedure TCriticalSection.Enter(const CB: TStdCallback)
   ,Enter:function(Self, CB$7) {
      if (!Self.FKilled) {
         TCriticalSection.Acquire(Self,CB$7);
      }
   }
   /// procedure TCriticalSection.EnterLockRequestFromStack()
   ,EnterLockRequestFromStack:function(Self) {
      if (Self.FStack.length>0) {
         ++Self.FLock;
         Copy$TCriticalSectionData(Self.FStack[0],Self.FCurrent);
         Self.FStack.shift();
         Self.FCurrent.__csdCB(true);
      }
   }
   /// procedure TCriticalSection.Leave(const CB: TStdCallback)
   ,Leave:function(Self, CB$8) {
      if (!Self.FKilled) {
         TCriticalSection.Release(Self,CB$8);
      }
   }
   /// procedure TCriticalSection.PushLockRequestToStack(const CB: TStdCallback)
   ,PushLockRequestToStack:function(Self, CB$9) {
      var Item$1 = {__csdID:0,__csdCB:null};
      if (!Self.FKilled) {
         ++__CSUniqueId;
         Item$1 = Create$22(__CSUniqueId,CB$9);
         Self.FStack.push(Clone$TCriticalSectionData(Item$1));
      }
   }
   /// procedure TCriticalSection.Release(const CB: TStdCallback)
   ,Release:function(Self, CB$10) {
      if (!Self.FKilled) {
         if (CB$10) {
            if (Self.FLock>0) {
               --Self.FLock;
               CB$10(true);
               if (Self.FStack.length>0) {
                  TCriticalSection.EnterLockRequestFromStack(Self);
               } else {
                  TCriticalSectionData$Reset$3(Self.FCurrent);
               }
            } else {
               CB$10(false);
            }
         } else {
            throw EW3Exception.CreateFmt$($New(EW3CriticalSection),"Method %s.%s failed, valid callback is required error",[TObject.ClassName(Self.ClassType), "TCriticalSection.Release"]);
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// TBoolean = class (TObject)
var TBoolean = {
   $ClassName:"TBoolean",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TBoolean.TrueFalse(const Value: Boolean) : String
   ,TrueFalse:function(Value$35) {
      return (BoolToStr(Value$35)).toLocaleLowerCase();
   }
   /// function TBoolean.YesNo(const Value: Boolean) : String
   ,YesNo:function(Value$36) {
      return pre_YesNo[Value$36?1:0];
   }
   /// function TBoolean.OnOff(const Value: Boolean) : String
   ,OnOff:function(Value$37) {
      return pre_OnOff[Value$37?1:0];
   }
   /// function TBoolean.StartStop(const Value: Boolean) : String
   ,StartStop:function(Value$38) {
      return pre_StartStop[Value$38?1:0];
   }
   /// function TBoolean.RunningPaused(const Value: Boolean) : String
   ,RunningPaused:function(Value$39) {
      return pre_RunPause[Value$39?1:0];
   }
   /// function TBoolean.Binary(const Value: Boolean) : Integer
   ,Binary:function(Value$40) {
      return pre_binary[Value$40?1:0];
   }
   ,Destroy:TObject.Destroy
};
function StringReplace(SourceString, OldPattern, NewPattern, Flags) {
   var Result = "";
   var temp$6 = "",
      xpos = 0,
      ASeg = "",
      BSeg = "",
      Parts = [];
   if ($SetIn(Flags,0,0,2)) {
      if ($SetIn(Flags,1,0,2)) {
         Result = StrReplace(SourceString,OldPattern,NewPattern);
      } else {
         Parts = (SourceString).split(OldPattern);
         Result = (Parts).join(NewPattern);
      }
   } else {
      temp$6 = SourceString;
      if ($SetIn(Flags,1,0,2)) {
         OldPattern = (temp$6).toLocaleLowerCase();
         temp$6 = (temp$6).toLocaleLowerCase();
      }
      xpos = (temp$6.indexOf(OldPattern)+1);
      if (xpos>0) {
         ASeg = (temp$6).substr(0,(xpos-1));
         (xpos+= OldPattern.length);
         BSeg = temp$6.substr(xpos-1);
         Result = ASeg+NewPattern+BSeg;
      }
   }
   return Result
};
function GetIsRunningInBrowser() {
   var Result = false;
   Result = (!(typeof window === 'undefined'));
   return Result
};
/// EW3Exception = class (Exception)
var EW3Exception = {
   $ClassName:"EW3Exception",$Parent:Exception
   ,$Init:function ($) {
      Exception.$Init($);
   }
   /// constructor EW3Exception.CreateFmt(aText: String; const aValues: array of const)
   ,CreateFmt:function(Self, aText, aValues) {
      Exception.Create(Self,Format(aText,aValues.slice(0)));
      return Self
   }
   /// constructor EW3Exception.Create(const MethodName: String; const Instance: TObject; const ErrorText: String)
   ,Create$27:function(Self, MethodName, Instance$5, ErrorText) {
      var LCallerName = "";
      LCallerName = (Instance$5)?TObject.ClassName(Instance$5.ClassType):"Anonymous";
      EW3Exception.CreateFmt$(Self,$R[0],[MethodName, LCallerName, ErrorText]);
      return Self
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt$:function($){return $.ClassType.CreateFmt.apply($.ClassType, arguments)}
   ,Create$27$:function($){return $.ClassType.Create$27.apply($.ClassType, arguments)}
};
/// EW3OwnedObject = class (EW3Exception)
var EW3OwnedObject = {
   $ClassName:"EW3OwnedObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EW3LockError = class (EW3Exception)
var EW3LockError = {
   $ClassName:"EW3LockError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EW3CriticalSection = class (EW3Exception)
var EW3CriticalSection = {
   $ClassName:"EW3CriticalSection",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
function SetupTypeLUT() {
   __TYPE_MAP.Boolean = typeof(true);
   __TYPE_MAP.Number$1 = typeof(0);
   __TYPE_MAP.String$1 = typeof("");
   __TYPE_MAP.Object$2 = typeof(TVariant.CreateObject());
   __TYPE_MAP.Undefined = typeof(undefined);
   __TYPE_MAP.Function$1 = typeof(function () {
      /* null */
   });
};
/// TValuePrefixType enumeration
var TValuePrefixType = [ "vpNone", "vpHexPascal", "vpHexC", "vpBinPascal", "vpBinC", "vpString" ];
/// TSystemEndianType enumeration
var TSystemEndianType = [ "stDefault", "stLittleEndian", "stBigEndian" ];
function TryStrToInt(Data$11, Value$41) {
   return TString.ExamineInteger(Data$11,Value$41);
};
function TryStrToFloat(Data$12, Value$42) {
   return TString.ExamineFloat(Data$12,Value$42);
};
function TryStrToBool(Data$13, Value$43) {
   return TString.ExamineBoolean(Data$13,Value$43);
};
/// TRTLDatatype enumeration
var TRTLDatatype = [ "itUnknown", "itBoolean", "itByte", "itChar", "itWord", "itLong", "itInt16", "itInt32", "itFloat32", "itFloat64", "itString" ];
/// TDataTypeConverter = class (TObject)
var TDataTypeConverter = {
   $ClassName:"TDataTypeConverter",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.OnEndianChanged = null;
      $.FBuffer = $.FView = null;
      $.FEndian = 0;
      $.FTyped = null;
   }
   /// function TDataTypeConverter.Base64ToBytes(const Base64: String) : TByteArray
   ,Base64ToBytes:function(Self, Base64) {
      return TBase64EncDec.Base64ToBytes$2(TBase64EncDec,Base64);
   }
   /// function TDataTypeConverter.Base64ToStr(Value: String) : String
   ,Base64ToStr:function(Self, Value$44) {
      var Result = "";
      Result = atob(Value$44);
      return Result
   }
   /// function TDataTypeConverter.BooleanToBytes(const Value: Boolean) : TByteArray
   ,BooleanToBytes:function(Self, Value$45) {
      var Result = [];
      Result.push((Value$45)?1:0);
      return Result
   }
   /// function TDataTypeConverter.BooleanToTypedArray(const Value: Boolean) : TMemoryHandle
   ,BooleanToTypedArray:function(Self, Value$46) {
      var Result = undefined;
      Result = new Uint8Array(1);
      Result[0] = (Value$46)?1:0;
      return Result
   }
   /// function TDataTypeConverter.BytesToBase64(const Bytes: TByteArray) : String
   ,BytesToBase64:function(Self, Bytes$1) {
      return TBase64EncDec.BytesToBase64$2(TBase64EncDec,Bytes$1);
   }
   /// function TDataTypeConverter.BytesToBoolean(const Data: TByteArray) : Boolean
   ,BytesToBoolean:function(Self, Data$14) {
      return Data$14[0]>0;
   }
   /// function TDataTypeConverter.BytesToChar(const Value: TByteArray) : Char
   ,BytesToChar:function(Self, Value$47) {
      return TString.DecodeUTF8(TString,Value$47);
   }
   /// function TDataTypeConverter.BytesToFloat32(const Data: TByteArray) : Float
   ,BytesToFloat32:function(Self, Data$15) {
      var Result = 0;
      Self.FView.setUint8(0,Data$15[0]);
      Self.FView.setUint8(1,Data$15[1]);
      Self.FView.setUint8(2,Data$15[2]);
      Self.FView.setUint8(3,Data$15[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getFloat32(0);
            break;
         case 1 :
            Result = Self.FView.getFloat32(0,true);
            break;
         case 2 :
            Result = Self.FView.getFloat32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToFloat64(const Data: TByteArray) : Float
   ,BytesToFloat64:function(Self, Data$16) {
      var Result = 0;
      Self.FView.setUint8(0,Data$16[0]);
      Self.FView.setUint8(1,Data$16[1]);
      Self.FView.setUint8(2,Data$16[2]);
      Self.FView.setUint8(3,Data$16[3]);
      Self.FView.setUint8(4,Data$16[4]);
      Self.FView.setUint8(5,Data$16[5]);
      Self.FView.setUint8(6,Data$16[6]);
      Self.FView.setUint8(7,Data$16[7]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getFloat64(0);
            break;
         case 1 :
            Result = Self.FView.getFloat64(0,true);
            break;
         case 2 :
            Result = Self.FView.getFloat64(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToInt16(const Data: TByteArray) : SmallInt
   ,BytesToInt16:function(Self, Data$17) {
      var Result = 0;
      Self.FView.setUint8(0,Data$17[0]);
      Self.FView.setUint8(1,Data$17[1]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getInt16(0);
            break;
         case 1 :
            Result = Self.FView.getInt16(0,true);
            break;
         case 2 :
            Result = Self.FView.getInt16(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToInt32(const Data: TByteArray) : Integer
   ,BytesToInt32:function(Self, Data$18) {
      var Result = 0;
      Self.FView.setUint8(0,Data$18[0]);
      Self.FView.setUint8(1,Data$18[1]);
      Self.FView.setUint8(2,Data$18[2]);
      Self.FView.setUint8(3,Data$18[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getInt32(0);
            break;
         case 1 :
            Result = Self.FView.getInt32(0,true);
            break;
         case 2 :
            Result = Self.FView.getInt32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToString(const Data: TByteArray) : String
   ,BytesToString:function(Self, Data$19) {
      var Result = "";
      Result = String.fromCharCode.apply(String, Data$19);
      return Result
   }
   /// function TDataTypeConverter.BytesToTypedArray(const Values: TByteArray) : TMemoryHandle
   ,BytesToTypedArray:function(Self, Values$11) {
      var Result = undefined;
      var LLen$5 = 0;
      LLen$5 = Values$11.length;
      Result = new Uint8Array(LLen$5);
      (Result).set(Values$11, 0);
      return Result
   }
   /// function TDataTypeConverter.BytesToUInt16(const Data: TByteArray) : Word
   ,BytesToUInt16:function(Self, Data$20) {
      var Result = 0;
      Self.FView.setUint8(0,Data$20[0]);
      Self.FView.setUint8(1,Data$20[1]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getUint16(0);
            break;
         case 1 :
            Result = Self.FView.getUint16(0,true);
            break;
         case 2 :
            Result = Self.FView.getUint16(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToUInt32(const Data: TByteArray) : Longword
   ,BytesToUInt32:function(Self, Data$21) {
      var Result = 0;
      Self.FView.setUint8(0,Data$21[0]);
      Self.FView.setUint8(1,Data$21[1]);
      Self.FView.setUint8(2,Data$21[2]);
      Self.FView.setUint8(3,Data$21[3]);
      switch (Self.FEndian) {
         case 0 :
            Result = Self.FView.getUint32(0);
            break;
         case 1 :
            Result = Self.FView.getUint32(0,true);
            break;
         case 2 :
            Result = Self.FView.getUint32(0,false);
            break;
      }
      return Result
   }
   /// function TDataTypeConverter.BytesToVariant(Data: TByteArray) : Variant
   ,BytesToVariant:function(Self, Data$22) {
      var Result = undefined;
      var LType$1 = 0;
      LType$1 = Data$22[0];
      Data$22.shift();
      switch (LType$1) {
         case 161 :
            Result = TDataTypeConverter.BytesToBoolean(Self,Data$22);
            break;
         case 162 :
            Result = Data$22[0];
            break;
         case 168 :
            Result = TDataTypeConverter.BytesToUInt16(Self,Data$22);
            break;
         case 169 :
            Result = TDataTypeConverter.BytesToUInt32(Self,Data$22);
            break;
         case 163 :
            Result = TDataTypeConverter.BytesToInt16(Self,Data$22);
            break;
         case 164 :
            Result = TDataTypeConverter.BytesToInt32(Self,Data$22);
            break;
         case 165 :
            Result = TDataTypeConverter.BytesToFloat32(Self,Data$22);
            break;
         case 166 :
            Result = TDataTypeConverter.BytesToFloat64(Self,Data$22);
            break;
         case 167 :
            Result = TString.DecodeUTF8(TString,Data$22);
            break;
         default :
            throw EW3Exception.CreateFmt$($New(EDatatype),"Failed to convert bytes[] to intrinsic type, unknown identifier [%s] error",[IntToHex2(LType$1)]);
      }
      return Result
   }
   /// function TDataTypeConverter.ByteToChar(const Value: Byte) : Char
   ,ByteToChar:function(Self, Value$48) {
      var Result = "";
      Result = String.fromCharCode(Value$48);
      return Result
   }
   /// function TDataTypeConverter.ByteToTypedArray(const Value: Byte) : TMemoryHandle
   ,ByteToTypedArray:function(Self, Value$49) {
      var Result = undefined;
      Result = new Uint8Array(1);
      Result[0] = (Value$49<0)?0:(Value$49>255)?255:Value$49;
      return Result
   }
   /// function TDataTypeConverter.CharToByte(const Value: Char) : Word
   ,CharToByte:function(Self, Value$50) {
      var Result = 0;
      Result = (Value$50).charCodeAt(0);
      return Result
   }
   /// function TDataTypeConverter.CharToBytes(const Value: Char) : TByteArray
   ,CharToBytes:function(Self, Value$51) {
      return TString.EncodeUTF8(TString,Value$51.charAt(-1));
   }
   /// constructor TDataTypeConverter.Create()
   ,Create$15:function(Self) {
      TObject.Create(Self);
      Self.FBuffer = new ArrayBuffer(16);
    Self.FView   = new DataView(Self.FBuffer);
      Self.FTyped = new Uint8Array(Self.FBuffer,0,15);
      return Self
   }
   /// destructor TDataTypeConverter.Destroy()
   ,Destroy:function(Self) {
      Self.FTyped = null;
      Self.FView = null;
      Self.FBuffer = null;
      TObject.Destroy(Self);
   }
   /// function TDataTypeConverter.Float32ToBytes(const Value: float32) : TByteArray
   ,Float32ToBytes:function(Self, Value$52) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setFloat32(0,Value$52);
            break;
         case 1 :
            Self.FView.setFloat32(0,Value$52,true);
            break;
         case 2 :
            Self.FView.setFloat32(0,Value$52,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   /// function TDataTypeConverter.Float32ToTypedArray(const Value: float32) : TMemoryHandle
   ,Float32ToTypedArray:function(Self, Value$53) {
      var Result = undefined;
      var LBytes$1 = 0;
      LBytes$1 = TDataTypeConverter.SizeOfType(TDataTypeConverter,8);
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setFloat32(0,Value$53);
            break;
         case 1 :
            Self.FView.setFloat32(0,Value$53,true);
            break;
         case 2 :
            Self.FView.setFloat32(0,Value$53,false);
            break;
      }
      Result = new Uint8Array(LBytes$1);
      TMarshal.Move$1(TMarshal,Self.FTyped,0,Result,0,LBytes$1);
      return Result
   }
   /// function TDataTypeConverter.Float64ToBytes(const Value: float64) : TByteArray
   ,Float64ToBytes:function(Self, Value$54) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setFloat64(0,Number(Value$54));
            break;
         case 1 :
            Self.FView.setFloat64(0,Number(Value$54),true);
            break;
         case 2 :
            Self.FView.setFloat64(0,Number(Value$54),false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 8 );
      return Result
   }
   /// function TDataTypeConverter.Float64ToTypedArray(const Value: float64) : TMemoryHandle
   ,Float64ToTypedArray:function(Self, Value$55) {
      var Result = undefined;
      var LBytes$2 = 0;
      LBytes$2 = TDataTypeConverter.SizeOfType(Self.ClassType,9);
      Result = new Uint8Array(LBytes$2);
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setFloat64(0,Number(Value$55));
            break;
         case 1 :
            Self.FView.setFloat64(0,Number(Value$55),true);
            break;
         case 2 :
            Self.FView.setFloat64(0,Number(Value$55),false);
            break;
      }
      TMarshal.Move$1(TMarshal,Self.FTyped,0,Result,0,LBytes$2);
      return Result
   }
   /// function TDataTypeConverter.InitFloat32(const Value: float32) : float32
   ,InitFloat32:function(Value$56) {
      var Result = 0;
      var temp$7 = null;
      temp$7 = new Float32Array(1);
      temp$7[0]=Value$56;
      Result = temp$7[0];
      temp$7 = null;
      return Result
   }
   /// function TDataTypeConverter.InitFloat64(const Value: float64) : float64
   ,InitFloat64:function(Value$57) {
      var Result = undefined;
      var temp$8 = null;
      temp$8 = new Float64Array(1);
      temp$8[0]=(Number(Value$57));
      Result = temp$8[0];
      temp$8 = null;
      return Result
   }
   /// function TDataTypeConverter.InitInt08(const Value: Byte) : Byte
   ,InitInt08:function(Value$58) {
      var Result = 0;
      var temp$9 = null;
      temp$9 = new Int8Array(1);
      temp$9[0]=((Value$58<-128)?-128:(Value$58>127)?127:Value$58);
      Result = temp$9[0];
      temp$9 = null;
      return Result
   }
   /// function TDataTypeConverter.InitInt16(const Value: SmallInt) : SmallInt
   ,InitInt16:function(Value$59) {
      var Result = 0;
      var temp$10 = null;
      temp$10 = new Int16Array(1);
      temp$10[0]=((Value$59<-32768)?-32768:(Value$59>32767)?32767:Value$59);
      Result = temp$10[0];
      temp$10 = null;
      return Result
   }
   /// function TDataTypeConverter.InitInt32(const Value: Integer) : Integer
   ,InitInt32:function(Value$60) {
      var Result = 0;
      var temp$11 = null;
      temp$11 = new Int32Array(1);
      temp$11[0]=((Value$60<-2147483648)?-2147483648:(Value$60>2147483647)?2147483647:Value$60);
      Result = temp$11[0];
      temp$11 = null;
      return Result
   }
   /// function TDataTypeConverter.InitUint08(const Value: Byte) : Byte
   ,InitUint08:function(Value$61) {
      var Result = 0;
      var LTemp$1 = null;
      LTemp$1 = new Uint8Array(1);
      LTemp$1[0]=((Value$61<0)?0:(Value$61>255)?255:Value$61);
      Result = LTemp$1[0];
      LTemp$1 = null;
      return Result
   }
   /// function TDataTypeConverter.InitUint16(const Value: Word) : Word
   ,InitUint16:function(Value$62) {
      var Result = 0;
      var temp$12 = null;
      temp$12 = new Uint16Array(1);
      temp$12[0]=((Value$62<0)?0:(Value$62>65536)?65536:Value$62);
      Result = temp$12[0];
      temp$12 = null;
      return Result
   }
   /// function TDataTypeConverter.InitUint32(const Value: Longword) : Longword
   ,InitUint32:function(Value$63) {
      var Result = 0;
      var temp$13 = null;
      temp$13 = new Uint32Array(1);
      temp$13[0]=((Value$63<0)?0:(Value$63>4294967295)?4294967295:Value$63);
      Result = temp$13[0];
      temp$13 = null;
      return Result
   }
   /// function TDataTypeConverter.Int16ToBytes(const Value: SmallInt) : TByteArray
   ,Int16ToBytes:function(Self, Value$64) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setInt16(0,Value$64);
            break;
         case 1 :
            Self.FView.setInt16(0,Value$64,true);
            break;
         case 2 :
            Self.FView.setInt16(0,Value$64,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 2 );
      return Result
   }
   /// function TDataTypeConverter.Int16ToTypedArray(const Value: Word) : TMemoryHandle
   ,Int16ToTypedArray:function(Self, Value$65) {
      var Result = undefined;
      var LBytes$3 = 0;
      LBytes$3 = TDataTypeConverter.SizeOfType(Self.ClassType,6);
      Result = new Uint8Array(LBytes$3);
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setInt16(0,Value$65);
            break;
         case 1 :
            Self.FView.setInt16(0,Value$65,true);
            break;
         case 2 :
            Self.FView.setInt16(0,Value$65,false);
            break;
      }
      TMarshal.Move$1(TMarshal,Self.FTyped,0,Result,0,LBytes$3);
      return Result
   }
   /// function TDataTypeConverter.Int32ToBytes(const Value: Integer) : TByteArray
   ,Int32ToBytes:function(Self, Value$66) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setInt32(0,Value$66);
            break;
         case 1 :
            Self.FView.setInt32(0,Value$66,true);
            break;
         case 2 :
            Self.FView.setInt32(0,Value$66,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   /// function TDataTypeConverter.Int32ToTypedArray(const Value: Longword) : TMemoryHandle
   ,Int32ToTypedArray:function(Self, Value$67) {
      var Result = undefined;
      var LBytes$4 = 0;
      LBytes$4 = TDataTypeConverter.SizeOfType(Self.ClassType,7);
      Result = new Uint8Array(LBytes$4);
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setInt32(0,Value$67);
            break;
         case 1 :
            Self.FView.setInt32(0,Value$67,true);
            break;
         case 2 :
            Self.FView.setInt32(0,Value$67,false);
            break;
      }
      TMarshal.Move$1(TMarshal,Self.FTyped,0,Result,0,LBytes$4);
      return Result
   }
   /// function TDataTypeConverter.NameOfType(const Kind: TRTLDatatype) : String
   ,NameOfType:function(Self, Kind) {
      return _NAMES[Kind];
   }
   /// procedure TDataTypeConverter.SetEndian(const NewEndian: TSystemEndianType)
   ,SetEndian:function(Self, NewEndian) {
      if (NewEndian!=Self.FEndian) {
         Self.FEndian = NewEndian;
         if (Self.OnEndianChanged) {
            Self.OnEndianChanged(Self);
         }
      }
   }
   /// function TDataTypeConverter.SizeOfType(const Kind: TRTLDatatype) : Integer
   ,SizeOfType:function(Self, Kind$1) {
      return __SIZES[Kind$1];
   }
   /// function TDataTypeConverter.StringToBytes(const Value: String) : TByteArray
   ,StringToBytes:function(Self, Value$68) {
      var Result = [];
      Result = (Value$68).split("").map( function( val ) {
        return val.charCodeAt( 0 );
    } );
      return Result
   }
   /// function TDataTypeConverter.StringToTypedArray(value: String) : TMemoryHandle
   ,StringToTypedArray:function(Self, value$1) {
      var Result = undefined;
      var x$16 = 0;
      Result = new Uint8Array(value$1.length);
      var $temp23;
      for(x$16=0,$temp23=value$1.length;x$16<$temp23;x$16++) {
         Result[x$16] = (value$1).charCodeAt(x$16);
      }
      return Result
   }
   /// function TDataTypeConverter.StrToBase64(Value: String) : String
   ,StrToBase64:function(Self, Value$69) {
      var Result = "";
      Result = btoa(Value$69);
      return Result
   }
   /// function TDataTypeConverter.SystemEndian() : TSystemEndianType
   ,SystemEndian:function(Self) {
      var Result = 0;
      var LLittle = 0,
         LBig = 0;
      LLittle = 1;
      LBig = 2;
      try {
         var LBuffer = new ArrayBuffer(2);
      var L8Array = new Uint8Array(LBuffer);
      var L16array = new Uint16Array(LBuffer);
      L8Array[0] = 0xAA;
      L8Array[1] = 0xBB;
      if(L16array[0] === 0xBBAA) {
        Result = LLittle;
      } else {
        if (L16array[0] === 0xAABB) Result = LBig;
      }
      } catch ($e) {
         /* null */
      }
      return Result
   }
   /// function TDataTypeConverter.TypeByName(TypeName: String) : TRTLDatatype
   ,TypeByName:function(Self, TypeName) {
      var Result = 0;
      var x$17 = 0,
         a$457 = 0;
      var Name$10 = "";
      TypeName = Trim$_String_((TypeName).toLocaleLowerCase());
      if (TypeName.length>0) {
         x$17 = 0;
         for(a$457=0;a$457<=10;a$457++) {
            Name$10 = _NAMES[a$457];
            if ((Name$10).toLocaleLowerCase()==TypeName) {
               Result = x$17;
               break;
            }
            ++x$17;
         }
      } else {
         Result = 0;
      }
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToBytes(const Value: TMemoryHandle) : TByteArray
   ,TypedArrayToBytes:function(Self, Value$70) {
      var Result = [];
      if (!TVariant.ValidRef(Value$70)) {
         throw EW3Exception.Create$27$($New(EConvertError),"TDataTypeConverter.TypedArrayToBytes",Self,$R[68]);
      }
      Result = Array.prototype.slice.call(Value$70);
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToFloat32(const Value: TMemoryHandle) : float32
   ,TypedArrayToFloat32:function(Self, Value$71) {
      var Result = 0;
      var LBuffer = null,
         LBytes$5 = 0,
         LTypeSize = 0,
         LView = null;
      if (!TVariant.ValidRef(Value$71)) {
         throw EW3Exception.Create$27$($New(EConvertError),"TDataTypeConverter.TypedArrayToFloat32",Self,$R[68]);
      }
      LBuffer = Value$71.buffer;
      LBytes$5 = LBuffer.byteLength;
      LTypeSize = TDataTypeConverter.SizeOfType(TDataTypeConverter,8);
      if (LBytes$5<LTypeSize) {
         throw EW3Exception.Create$27$($New(EConvertError),"TDataTypeConverter.TypedArrayToFloat32",Self,Format($R[67],["Float32", LTypeSize, LBytes$5]));
      }
      if (LBytes$5>LTypeSize) {
         LBytes$5 = LTypeSize;
      }
      LView = new DataView(LBuffer);
      switch (Self.FEndian) {
         case 0 :
            Result = LView.getFloat32(0);
            break;
         case 1 :
            Result = LView.getFloat32(0,true);
            break;
         case 2 :
            Result = LView.getFloat32(0,false);
            break;
      }
      LView = null;
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToStr(const Value: TMemoryHandle) : String
   ,TypedArrayToStr:function(Self, Value$72) {
      var Result = "";
      var x$18 = 0;
      if (TVariant.ValidRef(Value$72)) {
         if (Value$72.length>0) {
            var $temp24;
            for(x$18=0,$temp24=Value$72.length-1;x$18<=$temp24;x$18++) {
               Result += String.fromCharCode((Value$72)[x$18]);
            }
         }
      }
      return Result
   }
   /// function TDataTypeConverter.TypedArrayToUInt32(const Value: TMemoryHandle) : Longword
   ,TypedArrayToUInt32:function(Self, Value$73) {
      var Result = 0;
      var LBuffer$1 = null,
         LBytes$6 = 0,
         LTypeSize$1 = 0,
         LView$1 = null;
      if (!TVariant.ValidRef(Value$73)) {
         throw EW3Exception.Create$27$($New(EConvertError),"TDataTypeConverter.TypedArrayToUInt32",Self,$R[68]);
      }
      LBuffer$1 = Value$73.buffer;
      LBytes$6 = LBuffer$1.byteLength;
      LTypeSize$1 = TDataTypeConverter.SizeOfType(Self.ClassType,7);
      if (LBytes$6<LTypeSize$1) {
         throw EW3Exception.Create$27$($New(EConvertError),"TDataTypeConverter.TypedArrayToUInt32",Self,Format($R[67],["Int32", LTypeSize$1, LBytes$6]));
      }
      if (LBytes$6>LTypeSize$1) {
         LBytes$6 = LTypeSize$1;
      }
      LView$1 = new DataView(LBuffer$1);
      switch (Self.FEndian) {
         case 0 :
            Result = LView$1.getUint32(0);
            break;
         case 1 :
            Result = LView$1.getUint32(0,true);
            break;
         case 2 :
            Result = LView$1.getUint32(0,false);
            break;
      }
      LView$1 = null;
      return Result
   }
   /// function TDataTypeConverter.UInt16ToBytes(const Value: Word) : TByteArray
   ,UInt16ToBytes:function(Self, Value$74) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setUint16(0,Value$74);
            break;
         case 1 :
            Self.FView.setUint16(0,Value$74,true);
            break;
         case 2 :
            Self.FView.setUint16(0,Value$74,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 2 );
      return Result
   }
   /// function TDataTypeConverter.UInt32ToBytes(const Value: Longword) : TByteArray
   ,UInt32ToBytes:function(Self, Value$75) {
      var Result = [];
      switch (Self.FEndian) {
         case 0 :
            Self.FView.setUint32(0,Value$75);
            break;
         case 1 :
            Self.FView.setUint32(0,Value$75,true);
            break;
         case 2 :
            Self.FView.setUint32(0,Value$75,false);
            break;
      }
      Result = Array.prototype.slice.call( (Self).FTyped, 0, 4 );
      return Result
   }
   /// function TDataTypeConverter.VariantToBytes(Value: Variant) : TByteArray
   ,VariantToBytes:function(Self, Value$76) {
      var Result = [];
      var LType$2 = 0;
      function GetUnSignedIntType() {
         var Result = 0;
         if (Value$76<=255) {
            return 162;
         }
         if (Value$76<=65536) {
            return 168;
         }
         if (Value$76<=2147483647) {
            Result = 169;
         }
         return Result
      };
      function GetSignedIntType() {
         var Result = 0;
         if (Value$76>-32768) {
            Result = 163;
            return Result;
         }
         if (Value$76>-2147483648) {
            Result = 164;
         }
         return Result
      };
      function IsFloat32(x$19) {
         var Result = false;
         Result = isFinite(x$19) && x$19 == Math.fround(x$19);
         return Result
      };
      switch (TW3VariantHelper$DataType$1(Value$76)) {
         case 2 :
            Result = [161].slice();
            Result.pusha(TDataTypeConverter.BooleanToBytes(Self.ClassType,(Value$76?true:false)));
            break;
         case 3 :
            if (Value$76<0) {
               LType$2 = GetSignedIntType();
            } else {
               LType$2 = GetUnSignedIntType();
            }
            if (LType$2) {
               Result = [LType$2].slice();
               switch (LType$2) {
                  case 162 :
                     Result.push(TDataTypeConverter.InitInt08(parseInt(Value$76,10)));
                     break;
                  case 168 :
                     Result.pusha(TDataTypeConverter.UInt16ToBytes(Self,TDataTypeConverter.InitUint16(parseInt(Value$76,10))));
                     break;
                  case 169 :
                     Result.pusha(TDataTypeConverter.UInt32ToBytes(Self,TDataTypeConverter.InitUint32(parseInt(Value$76,10))));
                     break;
                  case 163 :
                     Result.pusha(TDataTypeConverter.Int16ToBytes(Self,TDataTypeConverter.InitInt16(parseInt(Value$76,10))));
                     break;
                  case 164 :
                     Result.pusha(TDataTypeConverter.Int32ToBytes(Self,TDataTypeConverter.InitInt32(parseInt(Value$76,10))));
                     break;
               }
            } else {
               throw Exception.Create($New(EDatatype),"Invalid datatype, failed to identify number [integer] type error");
            }
            break;
         case 4 :
            if (IsFloat32(Value$76)) {
               Result = [165].slice();
               Result.pusha(TDataTypeConverter.Float32ToBytes(Self,(Number(Value$76))));
            } else {
               Result = [166].slice();
               Result.pusha(TDataTypeConverter.Float64ToBytes(Self,(Number(Value$76))));
            }
            break;
         case 5 :
            Result = [167].slice();
            Result.pusha(TString.EncodeUTF8(TString,String(Value$76)));
            break;
         default :
            throw Exception.Create($New(EDatatype),"Invalid datatype, byte conversion failed error");
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$15$:function($){return $.ClassType.Create$15($)}
};
/// TDatatype = class (TObject)
var TDatatype = {
   $ClassName:"TDatatype",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TDatatype.Base64ToBytes(const Base64: String) : TByteArray
   ,Base64ToBytes$1:function(Self, Base64$1) {
      return TDataTypeConverter.Base64ToBytes(__Def_Converter.ClassType,Base64$1);
   }
   /// function TDatatype.BooleanToBytes(const Value: Boolean) : TByteArray
   ,BooleanToBytes$1:function(Self, Value$77) {
      return TDataTypeConverter.BooleanToBytes(__Def_Converter.ClassType,Value$77);
   }
   /// function TDatatype.BooleanToTypedArray(const Value: Boolean) : TMemoryHandle
   ,BooleanToTypedArray$1:function(Self, Value$78) {
      return TDataTypeConverter.BooleanToTypedArray(__Def_Converter,Value$78);
   }
   /// function TDatatype.BytesToBase64(const Bytes: TByteArray) : String
   ,BytesToBase64$1:function(Self, Bytes$2) {
      return TDataTypeConverter.BytesToBase64(__Def_Converter.ClassType,Bytes$2);
   }
   /// function TDatatype.BytesToBoolean(const Data: TByteArray) : Boolean
   ,BytesToBoolean$1:function(Self, Data$23) {
      return TDataTypeConverter.BytesToBoolean(__Def_Converter,Data$23);
   }
   /// function TDatatype.BytesToChar(const Value: TByteArray) : Char
   ,BytesToChar$1:function(Self, Value$79) {
      return TDataTypeConverter.BytesToChar(__Def_Converter.ClassType,Value$79);
   }
   /// function TDatatype.BytesToFloat32(const Data: TByteArray) : float32
   ,BytesToFloat32$1:function(Self, Data$24) {
      return TDataTypeConverter.BytesToFloat32(__Def_Converter,Data$24);
   }
   /// function TDatatype.BytesToFloat64(const Data: TByteArray) : float64
   ,BytesToFloat64$1:function(Self, Data$25) {
      return TDataTypeConverter.BytesToFloat64(__Def_Converter,Data$25);
   }
   /// function TDatatype.BytesToInt16(const Data: TByteArray) : SmallInt
   ,BytesToInt16$1:function(Self, Data$26) {
      return TDataTypeConverter.BytesToInt16(__Def_Converter,Data$26);
   }
   /// function TDatatype.BytesToInt32(const Data: TByteArray) : Integer
   ,BytesToInt32$1:function(Self, Data$27) {
      return TDataTypeConverter.BytesToInt32(__Def_Converter,Data$27);
   }
   /// function TDatatype.BytesToString(const Data: TByteArray) : String
   ,BytesToString$1:function(Self, Data$28) {
      return TDataTypeConverter.BytesToString(__Def_Converter,Data$28);
   }
   /// function TDatatype.BytesToTypedArray(const Values: TByteArray) : TMemoryHandle
   ,BytesToTypedArray$1:function(Self, Values$12) {
      return TDataTypeConverter.BytesToTypedArray(__Def_Converter,Values$12);
   }
   /// function TDatatype.BytesToUInt16(const Data: TByteArray) : Word
   ,BytesToUInt16$1:function(Self, Data$29) {
      return TDataTypeConverter.BytesToUInt16(__Def_Converter,Data$29);
   }
   /// function TDatatype.BytesToUInt32(const Data: TByteArray) : Longword
   ,BytesToUInt32$1:function(Self, Data$30) {
      return TDataTypeConverter.BytesToUInt32(__Def_Converter,Data$30);
   }
   /// function TDatatype.BytesToVariant(const Data: TByteArray) : Variant
   ,BytesToVariant$1:function(Self, Data$31) {
      return TDataTypeConverter.BytesToVariant(__Def_Converter,Data$31);
   }
   /// function TDatatype.ByteToChar(const Value: Byte) : Char
   ,ByteToChar$1:function(Self, Value$80) {
      return TDataTypeConverter.ByteToChar(__Def_Converter.ClassType,Value$80);
   }
   /// function TDatatype.ByteToTypedArray(const Value: Byte) : TMemoryHandle
   ,ByteToTypedArray$1:function(Self, Value$81) {
      return TDataTypeConverter.ByteToTypedArray(__Def_Converter,Value$81);
   }
   /// function TDatatype.CharToByte(const Value: Char) : Byte
   ,CharToByte$1:function(Self, Value$82) {
      return TDataTypeConverter.CharToByte(__Def_Converter.ClassType,Value$82);
   }
   /// function TDatatype.CharToBytes(const Value: Char) : TByteArray
   ,CharToBytes$1:function(Self, Value$83) {
      return TDataTypeConverter.CharToBytes(__Def_Converter.ClassType,Value$83);
   }
   /// function TDatatype.Float32ToBytes(const Value: float32) : TByteArray
   ,Float32ToBytes$1:function(Self, Value$84) {
      return TDataTypeConverter.Float32ToBytes(__Def_Converter,Value$84);
   }
   /// function TDatatype.Float32ToTypedArray(const Value: float32) : TMemoryHandle
   ,Float32ToTypedArray$1:function(Self, Value$85) {
      return TDataTypeConverter.Float32ToTypedArray(__Def_Converter,Value$85);
   }
   /// function TDatatype.Float64ToBytes(const Value: float64) : TByteArray
   ,Float64ToBytes$1:function(Self, Value$86) {
      return TDataTypeConverter.Float64ToBytes(__Def_Converter,Value$86);
   }
   /// function TDatatype.Float64ToTypedArray(const Value: float64) : TMemoryHandle
   ,Float64ToTypedArray$1:function(Self, Value$87) {
      return TDataTypeConverter.Float64ToTypedArray(__Def_Converter,Value$87);
   }
   /// function TDatatype.GetEndian() : TSystemEndianType
   ,GetEndian:function(Self) {
      return __Def_Converter.FEndian;
   }
   /// function TDatatype.InitFloat32(const Value: float32) : float32
   ,InitFloat32$1:function(Value$88) {
      return TDataTypeConverter.InitFloat32(Value$88);
   }
   /// function TDatatype.InitFloat64(const Value: float64) : float64
   ,InitFloat64$1:function(Value$89) {
      return TDataTypeConverter.InitFloat64(Value$89);
   }
   /// function TDatatype.InitInt08(const Value: Byte) : Byte
   ,InitInt08$1:function(Value$90) {
      return TDataTypeConverter.InitInt08(Value$90);
   }
   /// function TDatatype.InitInt16(const Value: SmallInt) : SmallInt
   ,InitInt16$1:function(Value$91) {
      return TDataTypeConverter.InitInt16(Value$91);
   }
   /// function TDatatype.InitInt32(const Value: Integer) : Integer
   ,InitInt32$1:function(Value$92) {
      return TDataTypeConverter.InitInt32(Value$92);
   }
   /// function TDatatype.InitUint08(const Value: Byte) : Byte
   ,InitUint08$1:function(Value$93) {
      return TDataTypeConverter.InitUint08(Value$93);
   }
   /// function TDatatype.InitUint16(const Value: Word) : Word
   ,InitUint16$1:function(Value$94) {
      return TDataTypeConverter.InitUint16(Value$94);
   }
   /// function TDatatype.InitUint32(const Value: Longword) : Longword
   ,InitUint32$1:function(Value$95) {
      return TDataTypeConverter.InitUint32(Value$95);
   }
   /// function TDatatype.Int16ToBytes(const Value: SmallInt) : TByteArray
   ,Int16ToBytes$1:function(Self, Value$96) {
      return TDataTypeConverter.Int16ToBytes(__Def_Converter,Value$96);
   }
   /// function TDatatype.Int16ToTypedArray(const Value: Word) : TMemoryHandle
   ,Int16ToTypedArray$1:function(Self, Value$97) {
      return TDataTypeConverter.Int16ToTypedArray(__Def_Converter,Value$97);
   }
   /// function TDatatype.Int32ToBytes(const Value: Longword) : TByteArray
   ,Int32ToBytes$1:function(Self, Value$98) {
      return TDataTypeConverter.Int32ToBytes(__Def_Converter,Value$98);
   }
   /// function TDatatype.Int32ToTypedArray(const Value: Longword) : TMemoryHandle
   ,Int32ToTypedArray$1:function(Self, Value$99) {
      return TDataTypeConverter.Int32ToTypedArray(__Def_Converter,Value$99);
   }
   /// function TDatatype.NameOfType(const Kind: TRTLDatatype) : String
   ,NameOfType$1:function(Self, Kind$2) {
      return TDataTypeConverter.NameOfType(__Def_Converter.ClassType,Kind$2);
   }
   /// procedure TDatatype.SetEndian(const NewEndian: TSystemEndianType)
   ,SetEndian$1:function(Self, NewEndian$1) {
      TDataTypeConverter.SetEndian(__Def_Converter,NewEndian$1);
   }
   /// function TDatatype.SizeOfType(const Kind: TRTLDatatype) : Integer
   ,SizeOfType$1:function(Self, Kind$3) {
      return TDataTypeConverter.SizeOfType(__Def_Converter.ClassType,Kind$3);
   }
   /// function TDatatype.StringToBytes(const Value: String) : TByteArray
   ,StringToBytes$1:function(Self, Value$100) {
      return TDataTypeConverter.StringToBytes(__Def_Converter,Value$100);
   }
   /// function TDatatype.StringToTypedArray(const Value: String) : TMemoryHandle
   ,StringToTypedArray$1:function(Self, Value$101) {
      return TDataTypeConverter.StringToTypedArray(__Def_Converter,Value$101);
   }
   /// function TDatatype.SystemEndian() : TSystemEndianType
   ,SystemEndian$1:function(Self) {
      return TDataTypeConverter.SystemEndian(__Def_Converter.ClassType);
   }
   /// function TDatatype.TypeByName(TypeName: String) : TRTLDatatype
   ,TypeByName$1:function(Self, TypeName$1) {
      return TDataTypeConverter.TypeByName(__Def_Converter.ClassType,TypeName$1);
   }
   /// function TDatatype.TypedArrayToBytes(const Value: TDefaultBufferType) : TByteArray
   ,TypedArrayToBytes$1:function(Self, Value$102) {
      return TDataTypeConverter.TypedArrayToBytes(__Def_Converter,Value$102);
   }
   /// function TDatatype.TypedArrayToStr(const Value: TDefaultBufferType) : String
   ,TypedArrayToStr$1:function(Self, Value$103) {
      return TDataTypeConverter.TypedArrayToStr(__Def_Converter,Value$103);
   }
   /// function TDatatype.TypedArrayToUInt32(const Value: TDefaultBufferType) : Longword
   ,TypedArrayToUInt32$1:function(Self, Value$104) {
      return TDataTypeConverter.TypedArrayToUInt32(__Def_Converter,Value$104);
   }
   /// function TDatatype.UInt16ToBytes(const Value: Word) : TByteArray
   ,UInt16ToBytes$1:function(Self, Value$105) {
      return TDataTypeConverter.UInt16ToBytes(__Def_Converter,Value$105);
   }
   /// function TDatatype.UInt32ToBytes(const Value: Longword) : TByteArray
   ,UInt32ToBytes$1:function(Self, Value$106) {
      return TDataTypeConverter.UInt32ToBytes(__Def_Converter,Value$106);
   }
   /// function TDatatype.VariantToBytes(const Value: Variant) : TByteArray
   ,VariantToBytes$1:function(Self, Value$107) {
      return TDataTypeConverter.VariantToBytes(__Def_Converter,Value$107);
   }
   ,Destroy:TObject.Destroy
};
/// TByteOrderMarkType enumeration
var TByteOrderMarkType = [ "bomNone", "bomUTF8", "bomUTF16", "bomUTF32" ];
/// TByteOrderMark = class (TObject)
var TByteOrderMark = {
   $ClassName:"TByteOrderMark",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TByteOrderMark.CheckBOM(const Bytes: TByteArray) : TByteOrderMarkType
   ,CheckBOM:function(Self, Bytes$3) {
      var Result = 0;
      Result = 0;
      if (TByteOrderMark.CheckUTF8(Self,Bytes$3)) {
         Result = 1;
      } else if (TByteOrderMark.CheckUTF16(Self,Bytes$3)) {
         Result = 2;
      } else if (TByteOrderMark.CheckUTF32(Self,Bytes$3)) {
         Result = 3;
      }
      return Result
   }
   /// function TByteOrderMark.CheckUTF16(const Bytes: TByteArray) : Boolean
   ,CheckUTF16:function(Self, Bytes$4) {
      var Result = false;
      if (Bytes$4.length>=2) {
         Result = Bytes$4[0]==254&&Bytes$4[1]==255||Bytes$4[0]==255&&Bytes$4[1]==254;
      }
      return Result
   }
   /// function TByteOrderMark.CheckUTF32(const Bytes: TByteArray) : Boolean
   ,CheckUTF32:function(Self, Bytes$5) {
      var Result = false;
      if (Bytes$5.length>=4) {
         Result = (Bytes$5[0]==0)&&(Bytes$5[1]==0)&&Bytes$5[2]==254&&Bytes$5[3]==255||Bytes$5[0]==255&&Bytes$5[1]==254&&(Bytes$5[2]==0)&&(Bytes$5[3]==0);
      }
      return Result
   }
   /// function TByteOrderMark.CheckUTF8(const Bytes: TByteArray) : Boolean
   ,CheckUTF8:function(Self, Bytes$6) {
      return Bytes$6.length>=3&&Bytes$6[0]==239&&Bytes$6[1]==187&&Bytes$6[2]==191;
   }
   /// function TByteOrderMark.GetUTF16BOM() : TByteArray
   ,GetUTF16BOM:function(Self) {
      return [254, 255].slice();
   }
   /// function TByteOrderMark.GetUTF32BOM() : TByteArray
   ,GetUTF32BOM:function(Self) {
      return [0, 0, 254, 255].slice();
   }
   /// function TByteOrderMark.GetUTF8BOM() : TByteArray
   ,GetUTF8BOM:function(Self) {
      return [239, 187, 191].slice();
   }
   /// function TByteOrderMark.StripBOM(const Bytes: TByteArray) : TByteArray
   ,StripBOM:function(Self, Bytes$7) {
      var Result = [];
      Result = Bytes$7;
      if (TByteOrderMark.CheckUTF8(Self,Bytes$7)) {
         Result.splice(0,3)
         ;
      } else if (TByteOrderMark.CheckUTF16(Self,Bytes$7)) {
         Result.splice(0,2)
         ;
      } else if (TByteOrderMark.CheckUTF32(Self,Bytes$7)) {
         Result.splice(0,4)
         ;
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TBase64EncDec = class (TObject)
var TBase64EncDec = {
   $ClassName:"TBase64EncDec",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TBase64EncDec.Base64ToBytes(const b64: String) : TByteArray
   ,Base64ToBytes$2:function(Self, b64) {
      var Result = [];
      var ASeg$1 = 0;
      var BSeg$1 = 0;
      var CSeg = 0;
      var DSeg = 0;
      var LTextLen = 0,
         LPlaceholderCount = 0,
         BufferSize = 0,
         xpos$1 = 0,
         idx = 0,
         temp$14 = 0,
         temp$15 = 0,
         temp$16 = 0;
      LTextLen = b64.length;
      if (LTextLen>0) {
         LPlaceholderCount = 0;
         if (LTextLen%4<1) {
            LPlaceholderCount = (b64.charAt((LTextLen-1)-1)=="=")?2:(b64.charAt(LTextLen-1)=="=")?1:0;
         }
         BufferSize = ($Div(LTextLen*3,4))-LPlaceholderCount;
         $ArraySetLenC(Result,BufferSize,function (){return 0});
         if (LPlaceholderCount>0) {
            (LTextLen-= 4);
         }
         xpos$1 = 1;
         idx = 0;
         while (xpos$1<LTextLen) {
            ASeg$1 = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1-1))]<<18;
            BSeg$1 = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1))]<<12;
            CSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1+1))]<<6;
            DSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1+2))];
            temp$14 = ((ASeg$1|BSeg$1)|CSeg)|DSeg;
            Result[idx]=(temp$14>>>16)&255;
            ++idx;
            Result[idx]=(temp$14>>>8)&255;
            ++idx;
            Result[idx]=temp$14&255;
            ++idx;
            (xpos$1+= 4);
         }
         switch (LPlaceholderCount) {
            case 1 :
               ASeg$1 = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1-1))]<<2;
               BSeg$1 = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1))]>>>4;
               temp$15 = ASeg$1|BSeg$1;
               Result[idx]=temp$15&255;
               break;
            case 2 :
               ASeg$1 = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1-1))]<<10;
               BSeg$1 = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1))]<<4;
               CSeg = __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,b64.charAt(xpos$1+1))]>>>2;
               temp$16 = (ASeg$1|BSeg$1)|CSeg;
               Result[idx]=(temp$16>>>8)&255;
               ++idx;
               Result[idx]=temp$16&255;
               break;
         }
      }
      return Result
   }
   /// function TBase64EncDec.Base64ToString(const b64: String) : String
   ,Base64ToString:function(Self, b64$1) {
      var Result = "";
      Result = atob(b64$1);
      return Result
   }
   /// function TBase64EncDec.BytesToBase64(const Data: TByteArray) : String
   ,BytesToBase64$2:function(Self, Data$32) {
      var Result = "";
      var LLen$6 = 0,
         LExtra = 0,
         LStrideLen = 0,
         LMaxChunkLength = 0,
         i$1 = 0,
         Ahead = 0,
         SegSize = 0,
         output = "",
         LTemp$2 = 0,
         LTemp$3 = 0;
      LLen$6 = Data$32.length;
      if (LLen$6>0) {
         LExtra = Data$32.length%3;
         LStrideLen = LLen$6-LExtra;
         LMaxChunkLength = 16383;
         i$1 = 0;
         while (i$1<LStrideLen) {
            Ahead = i$1+LMaxChunkLength;
            SegSize = (Ahead>LStrideLen)?LStrideLen:Ahead;
            Result+=TBase64EncDec.EncodeChunk(Self,Data$32,i$1,SegSize);
            (i$1+= LMaxChunkLength);
         }
         if (LExtra>0) {
            --LLen$6;
         }
         output = "";
         switch (LExtra) {
            case 1 :
               LTemp$2 = Data$32[LLen$6];
               output+=__B64_Lookup[LTemp$2>>>2];
               output+=__B64_Lookup[(LTemp$2<<4)&63];
               output+="==";
               break;
            case 2 :
               LTemp$3 = (Data$32[LLen$6-1]<<8)+Data$32[LLen$6];
               output+=__B64_Lookup[LTemp$3>>>10];
               output+=__B64_Lookup[(LTemp$3>>>4)&63];
               output+=__B64_Lookup[(LTemp$3<<2)&63];
               output+="=";
               break;
         }
         Result+=output;
      }
      return Result
   }
   /// function TBase64EncDec.CalcByteLength(const b64: String) : Integer
   ,CalcByteLength:function(Self, b64$2) {
      var Result = 0;
      var LLen$7 = 0,
         LPlaceholderCount$1 = 0;
      LLen$7 = b64$2.length;
      if (LLen$7>0) {
         LPlaceholderCount$1 = 0;
         if (LLen$7%4<1) {
            LPlaceholderCount$1 = (b64$2.charAt((LLen$7-1)-1)=="=")?2:(b64$2.charAt(LLen$7-1)=="=")?1:0;
         }
         Result = ($Div(LLen$7*3,4))-LPlaceholderCount$1;
      }
      return Result
   }
   /// function TBase64EncDec.EncodeChunk(const Data: TByteArray; startpos: Integer; endpos: Integer) : String
   ,EncodeChunk:function(Self, Data$33, startpos, endpos) {
      var Result = "";
      var temp$17 = 0;
      while (startpos<endpos) {
         temp$17 = (Data$33[startpos]<<16)+(Data$33[startpos+1]<<8)+Data$33[startpos+2];
         Result+=__B64_Lookup[(temp$17>>>18)&63]+__B64_Lookup[(temp$17>>>12)&63]+__B64_Lookup[(temp$17>>>6)&63]+__B64_Lookup[temp$17&63];
         (startpos+= 3);
      }
      return Result
   }
   /// function TBase64EncDec.ExtractPlaceholderCount(const b64: String) : Integer
   ,ExtractPlaceholderCount:function(Self, b64$3) {
      var Result = 0;
      var LLen$8 = 0;
      LLen$8 = b64$3.length;
      if (LLen$8>0) {
         if (LLen$8%4<1) {
            Result = (b64$3.charAt((LLen$8-1)-1)=="=")?2:(b64$3.charAt(LLen$8-1)=="=")?1:0;
         }
      }
      return Result
   }
   /// function TBase64EncDec.StringToBase64(const Text: String) : String
   ,StringToBase64:function(Self, Text$28) {
      var Result = "";
      Result = btoa(Text$28);
      return Result
   }
   /// function TBase64EncDec.TripletToBase64(const num: Integer) : String
   ,TripletToBase64:function(Self, num) {
      return __B64_Lookup[(num>>>18)&63]+__B64_Lookup[(num>>>12)&63]+__B64_Lookup[(num>>>6)&63]+__B64_Lookup[num&63];
   }
   ,Destroy:TObject.Destroy
};
/// EDatatype = class (EW3Exception)
var EDatatype = {
   $ClassName:"EDatatype",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EConvertError = class (EW3Exception)
var EConvertError = {
   $ClassName:"EConvertError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EConvertHexStringInvalid = class (EConvertError)
var EConvertHexStringInvalid = {
   $ClassName:"EConvertHexStringInvalid",$Parent:EConvertError
   ,$Init:function ($) {
      EConvertError.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EConvertHexInvalidContent = class (EConvertError)
var EConvertHexInvalidContent = {
   $ClassName:"EConvertHexInvalidContent",$Parent:EConvertError
   ,$Init:function ($) {
      EConvertError.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EConvertBinaryStringInvalid = class (EConvertError)
var EConvertBinaryStringInvalid = {
   $ClassName:"EConvertBinaryStringInvalid",$Parent:EConvertError
   ,$Init:function ($) {
      EConvertError.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EConvertBinaryInvalidChar = class (EConvertError)
var EConvertBinaryInvalidChar = {
   $ClassName:"EConvertBinaryInvalidChar",$Parent:EConvertError
   ,$Init:function ($) {
      EConvertError.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
function SetupConversionLUT() {
   try {
      __CONV_BUFFER = new ArrayBuffer(16);
      __CONV_VIEW   = new DataView(__CONV_BUFFER);
      __CONV_ARRAY = new Uint8Array(__CONV_BUFFER,0,15);
   } catch ($e) {
      var e$9 = $W($e);
      /* null */
   }
};
function SetupBase64() {
   var i$2 = 0;
   var $temp25;
   for(i$2=1,$temp25=CNT_B64_CHARSET.length;i$2<=$temp25;i$2++) {
      __B64_Lookup[i$2-1] = CNT_B64_CHARSET.charAt(i$2-1);
      __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,CNT_B64_CHARSET.charAt(i$2-1))] = i$2-1;
   }
   __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,"-")] = 62;
   __B64_RevLookup[TDataTypeConverter.CharToByte(TDataTypeConverter,"_")] = 63;
};
/// TUnManaged = class (TObject)
var TUnManaged = {
   $ClassName:"TUnManaged",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TUnManaged.AllocMemA(const Size: Integer) : TMemoryHandle
   ,AllocMemA:function(Self, Size$5) {
      var Result = undefined;
      if (Size$5>0) {
         Result = new Uint8Array(Size$5);
      } else {
         Result = null;
      }
      return Result
   }
   /// procedure TUnManaged.FreeMemA(const Memory: TMemoryHandle)
   ,FreeMemA:function(Self, Memory$1) {
      if (Memory$1) {
         Memory$1.buffer = null;
      }
   }
   /// function TUnManaged.ReAllocMemA(const Memory: TMemoryHandle; Size: Integer) : TMemoryHandle
   ,ReAllocMemA:function(Self, Memory$2, Size$6) {
      var Result = undefined;
      if (Memory$2) {
         if (Size$6>0) {
            Result = new Uint8Array(Size$6);
            TMarshal.Move$1(TMarshal,Memory$2,0,Result,0,Size$6);
         }
      } else {
         Result = TUnManaged.AllocMemA(Self,Size$6);
      }
      return Result
   }
   /// function TUnManaged.ReadMemoryA(const Memory: TMemoryHandle; const Offset: Integer; Size: Integer) : TMemoryHandle
   ,ReadMemoryA:function(Self, Memory$3, Offset$1, Size$7) {
      var Result = undefined;
      var LTotal = 0;
      if (Memory$3&&Offset$1>=0) {
         LTotal = Offset$1+Size$7;
         if (LTotal>Memory$3.length) {
            Size$7 = parseInt((Memory$3.length-LTotal),10);
         }
         if (Size$7>0) {
            Result = new Uint8Array(Memory$3.buffer.slice(Offset$1,Size$7));
         }
      }
      return Result
   }
   /// function TUnManaged.WriteMemoryA(const Memory: TMemoryHandle; const Offset: Integer; const Data: TMemoryHandle) : Integer
   ,WriteMemoryA:function(Self, Memory$4, Offset$2, Data$34) {
      var Result = 0;
      var mTotal,
         mChunk = null,
         mTemp = null;
      if (Memory$4) {
         if (Data$34) {
            mTotal = Offset$2+Data$34.length;
            if (mTotal>Memory$4.length) {
               Result = parseInt((Memory$4.length-mTotal),10);
            } else {
               Result = parseInt(Data$34.length,10);
            }
            if (Result>0) {
               if (Offset$2+Data$34.length<=Memory$4.length) {
                  Memory$4.set(Data$34,Offset$2);
               } else {
                  mChunk = Data$34.buffer.slice(0,Result-1);
                  mTemp = new Uint8Array(mChunk);
                  Memory$4.set(mTemp,Offset$2);
               }
            }
         }
      }
      return Result
   }
   /// procedure TUnManaged.FillMemoryA(const Memory: TMemoryHandle; const Offset: Integer; Size: Integer; const Data: TMemoryHandle)
   ,FillMemoryA:function(Self, Memory$5, Offset$3, Size$8, Data$35) {
      var x$20 = 0,
         mEnd = 0,
         mToWrite;
      if (Memory$5) {
         if (Offset$3>=0) {
            if (Offset$3<Memory$5.length) {
               if (Data$35) {
                  x$20 = Offset$3;
                  mEnd = Offset$3+Size$8-1;
                  while (x$20<mEnd) {
                     mToWrite = Data$35.length;
                     if (x$20+mToWrite-1>mEnd) {
                        mToWrite = x$20+mToWrite-1-mEnd;
                     }
                     if (mToWrite<1) {
                        break;
                     }
                     TMarshal.Move$1(TMarshal,Data$35,0,Memory$5,x$20,parseInt(mToWrite,10));
                     (x$20+= parseInt(mToWrite,10));
                  }
               }
            }
         }
      }
   }
   ,Destroy:TObject.Destroy
};
/// function TMemoryHandleHelper.Valid(const Self: TMemoryHandle) : Boolean
function TMemoryHandleHelper$Valid$4(Self$39) {
   var Result = false;
   Result = !( (Self$39 == undefined) || (Self$39 == null) );
   return Result
}
/// function TMemoryHandleHelper.Equals(const Self: TMemoryHandle; const Source: TMemoryHandle) : Boolean
function TMemoryHandleHelper$Equals$7(Self$40, Source$2) {
   var Result = false;
   Result = (Self$40 == Source$2);
   return Result
}
/// function TMemoryHandleHelper.Defined(const Self: TMemoryHandle) : Boolean
function TMemoryHandleHelper$Defined$2(Self$41) {
   var Result = false;
   Result = !(Self$41 == undefined);
   return Result
}
/// function TMemoryHandleHelper.UnDefined(const Self: TMemoryHandle) : Boolean
function TMemoryHandleHelper$UnDefined$2(Self$42) {
   var Result = false;
   Result = (Self$42 == undefined);
   return Result
}
/// TMarshal = class (TObject)
var TMarshal = {
   $ClassName:"TMarshal",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TMarshal.AllocMem(const Size: Integer) : TAddress
   ,AllocMem:function(Self, Size$9) {
      var Result = null;
      var LBuffer$2 = null,
         LArray = null;
      if (Size$9>0) {
         LBuffer$2 = new ArrayBuffer(Size$9);
         LArray = new Uint8Array(LBuffer$2,0,Size$9);
         Result = TAddress.Create$33$($New(TAddress),LArray,0);
      }
      return Result
   }
   /// procedure TMarshal.Fill(const Target: TMemoryHandle; Offset: Integer; ByteLen: Integer; Value: Byte)
   ,Fill:function(Self, Target, Offset$4, ByteLen, Value$108) {
      var LBuffer$3 = null,
         LTotalSize = 0,
         LTemp$4 = undefined,
         LLongword = 0,
         LTarget$1 = null,
         x$21 = 0,
         LLongs$1 = 0;
      if (Target) {
         if (Target.buffer) {
            LBuffer$3 = Target.buffer;
            LTotalSize = LBuffer$3.byteLength;
            if (Offset$4>=0&&Offset$4<LTotalSize) {
               if (Offset$4+ByteLen>LTotalSize) {
                  ByteLen = LTotalSize-Offset$4;
               }
               Value$108 = TDataTypeConverter.InitUint08(Value$108);
               if (Target.fill) {
                  Target.fill(Value$108,Offset$4,ByteLen);
               } else {
                  LTemp$4 = TUnManaged.AllocMemA(TUnManaged,4);
                  try {
                     LTemp$4[0] = Value$108;
                     LTemp$4[1] = Value$108;
                     LTemp$4[2] = Value$108;
                     LTemp$4[3] = Value$108;
                     LLongword = TDatatype.TypedArrayToUInt32$1(TDatatype,LTemp$4);
                     LTarget$1 = new DataView(LBuffer$3);
                     x$21 = Offset$4;
                     LLongs$1 = ByteLen>>>5;
                     while (LLongs$1>0) {
                        LTarget$1.setUint32(x$21,LLongword);
                        (x$21+= 4);
                        LTarget$1.setUint32(x$21,LLongword);
                        (x$21+= 4);
                        LTarget$1.setUint32(x$21,LLongword);
                        (x$21+= 4);
                        LTarget$1.setUint32(x$21,LLongword);
                        (x$21+= 4);
                        LTarget$1.setUint32(x$21,LLongword);
                        (x$21+= 4);
                        LTarget$1.setUint32(x$21,LLongword);
                        (x$21+= 4);
                        LTarget$1.setUint32(x$21,LLongword);
                        (x$21+= 4);
                        LTarget$1.setUint32(x$21,LLongword);
                        (x$21+= 4);
                        (x$21+= 32);
                        --LLongs$1;
                     }
                     LLongs$1 = ByteLen%32;
                     while (LLongs$1>0) {
                        LTarget$1.setUint8(x$21,Value$108);
                        ++x$21;
                        --LLongs$1;
                     }
                     LTarget$1.buffer = null;
                     LTarget$1 = null;
                  } finally {
                     TUnManaged.FreeMemA(TUnManaged,LTemp$4);
                     LTemp$4 = null;
                  }
               }
            }
         } else {
            throw Exception.Create($New(EUnManaged),"Memory fill failed, invalid or incompatible target buffer error");
         }
      }
   }
   /// procedure TMarshal.FillChar(const Target: TAddress; const Size: Integer; const Value: Byte)
   ,FillChar$2:function(Self, Target$1, Size$10, Value$109) {
      if (Target$1!==null) {
         if (Target$1.FBuffer$2) {
            TMarshal.Fill(TMarshal,Target$1.FBuffer$2,Target$1.FOffset$1,Size$10,Value$109);
         }
      }
   }
   /// procedure TMarshal.FillChar(const Target: TAddress; const Size: Integer; const Value: Char)
   ,FillChar$1:function(Self, Target$2, Size$11, Value$110) {
      var LByte = 0;
      if (Target$2!==null) {
         if (Value$110.length>0) {
            LByte = TString.CharCodeFor(TString,Value$110);
            if (Target$2.FBuffer$2) {
               TMarshal.Fill(Self,Target$2.FBuffer$2,Target$2.FOffset$1,Size$11,LByte);
            }
         }
      }
   }
   /// procedure TMarshal.FreeMem(const Segment: TAddress)
   ,FreeMem:function(Self, Segment$1) {
      if (Segment$1!==null) {
         TObject.Free(Segment$1);
      }
   }
   /// procedure TMarshal.Move(const Source: TMemoryHandle; const SourceStart: Integer; const Target: TMemoryHandle; const TargetStart: Integer; const Size: Integer)
   ,Move$1:function(Self, Source$3, SourceStart, Target$3, TargetStart, Size$12) {
      var LRef = null;
      if (TMemoryHandleHelper$Defined$2(Source$3)&&TMemoryHandleHelper$Valid$4(Source$3)&&SourceStart>=0) {
         if (TMemoryHandleHelper$Defined$2(Target$3)&&TMemoryHandleHelper$Valid$4(Target$3)&&TargetStart>=0) {
            LRef = Source$3.subarray(SourceStart,SourceStart+Size$12);
            Target$3.set(LRef,TargetStart);
         }
      }
   }
   /// procedure TMarshal.Move(const Source: TAddress; const Target: TAddress; const Size: Integer)
   ,Move:function(Self, Source$4, Target$4, Size$13) {
      if (Source$4!==null) {
         if (Target$4!==null) {
            if (Size$13>0) {
               TMarshal.Move$1(Self,Source$4.FBuffer$2,Source$4.FOffset$1,Target$4.FBuffer$2,Target$4.FOffset$1,Size$13);
            }
         }
      }
   }
   /// function TMarshal.ReadMemory(const Segment: TAddress; const Size: Integer) : TByteArray
   ,ReadMemory:function(Self, Segment$2, Size$14) {
      var Result = [];
      var mHandle = null,
         mOffset = 0,
         mLongs = 0,
         x$22 = 0;
      if (Segment$2!==null&&Size$14>0) {
         mHandle = Segment$2.FBuffer$2;
         mOffset = Segment$2.FOffset$1;
         mLongs = Size$14>>>3;
         x$22 = 0;
         $ArraySetLenC(Result,Size$14,function (){return 0});
         while (mLongs>0) {
            Result[x$22]=mHandle[mOffset];
            Result[x$22+1]=mHandle[(mOffset+1)];
            Result[x$22+2]=mHandle[(mOffset+2)];
            Result[x$22+3]=mHandle[(mOffset+3)];
            Result[x$22+4]=mHandle[(mOffset+4)];
            Result[x$22+5]=mHandle[(mOffset+5)];
            Result[x$22+6]=mHandle[(mOffset+6)];
            Result[x$22+7]=mHandle[(mOffset+7)];
            (x$22+= 8);
            (mOffset+= 8);
            --mLongs;
         }
         switch ((Size$14%8)) {
            case 1 :
               Result[x$22]=mHandle[mOffset];
               break;
            case 2 :
               Result[x$22]=mHandle[mOffset];
               Result[x$22+1]=mHandle[(mOffset+1)];
               break;
            case 3 :
               Result[x$22]=mHandle[mOffset];
               Result[x$22+1]=mHandle[(mOffset+1)];
               Result[x$22+2]=mHandle[(mOffset+2)];
               break;
            case 4 :
               Result[x$22]=mHandle[mOffset];
               Result[x$22+1]=mHandle[(mOffset+1)];
               Result[x$22+2]=mHandle[(mOffset+2)];
               Result[x$22+3]=mHandle[(mOffset+3)];
               break;
            case 5 :
               Result[x$22]=mHandle[mOffset];
               Result[x$22+1]=mHandle[(mOffset+1)];
               Result[x$22+2]=mHandle[(mOffset+2)];
               Result[x$22+3]=mHandle[(mOffset+3)];
               Result[x$22+4]=mHandle[(mOffset+4)];
               break;
            case 6 :
               Result[x$22]=mHandle[mOffset];
               Result[x$22+1]=mHandle[(mOffset+1)];
               Result[x$22+2]=mHandle[(mOffset+2)];
               Result[x$22+3]=mHandle[(mOffset+3)];
               Result[x$22+4]=mHandle[(mOffset+4)];
               Result[x$22+5]=mHandle[(mOffset+5)];
               break;
            case 7 :
               Result[x$22]=mHandle[mOffset];
               Result[x$22+1]=mHandle[(mOffset+1)];
               Result[x$22+2]=mHandle[(mOffset+2)];
               Result[x$22+3]=mHandle[(mOffset+3)];
               Result[x$22+4]=mHandle[(mOffset+4)];
               Result[x$22+5]=mHandle[(mOffset+5)];
               Result[x$22+6]=mHandle[(mOffset+6)];
               break;
         }
      }
      return Result
   }
   /// procedure TMarshal.ReAllocMem(var Segment: TAddress; const Size: Integer)
   ,ReAllocMem:function(Self, Segment$3, Size$15) {
      var LSize = 0,
         LTemp$5 = null;
      if (Segment$3.v!==null) {
         LSize = Segment$3.v.FBuffer$2.length;
         LTemp$5 = TMarshal.AllocMem(Self,Size$15);
         if (Size$15>LSize) {
            TMarshal.Move(Self,Segment$3.v,LTemp$5,LSize);
         } else {
            TMarshal.Move(Self,Segment$3.v,LTemp$5,Size$15);
         }
         TObject.Free(Segment$3.v);
         Segment$3.v = LTemp$5;
      } else {
         Segment$3.v = TMarshal.AllocMem(Self,Size$15);
      }
   }
   /// procedure TMarshal.WriteMemory(const Segment: TAddress; const Data: TByteArray)
   ,WriteMemory:function(Self, Segment$4, Data$36) {
      if (Segment$4!==null) {
         if (Data$36.length>0) {
            Segment$4.FBuffer$2.set(Data$36,Segment$4.FOffset$1);
         }
      }
   }
   ,Destroy:TObject.Destroy
};
/// function TBufferHandleHelper.Valid(const Self: TBufferHandle) : Boolean
function TBufferHandleHelper$Valid$5(Self$43) {
   var Result = false;
   Result = !( (Self$43 == undefined) || (Self$43 == null) );
   return Result
}
/// function TBufferHandleHelper.Equals(const Self: TBufferHandle; const Source: TBufferHandle) : Boolean
function TBufferHandleHelper$Equals$8(Self$44, Source$5) {
   var Result = false;
   Result = (Self$44 == Source$5);
   return Result
}
/// function TBufferHandleHelper.Defined(const Self: TBufferHandle) : Boolean
function TBufferHandleHelper$Defined$3(Self$45) {
   var Result = false;
   Result = !(self == undefined);
   return Result
}
/// function TBufferHandleHelper.UnDefined(const Self: TBufferHandle) : Boolean
function TBufferHandleHelper$UnDefined$3(Self$46) {
   var Result = false;
   Result = (self == undefined);
   return Result
}
/// TAddress = class (TObject)
var TAddress = {
   $ClassName:"TAddress",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FBuffer$2 = undefined;
      $.FOffset$1 = 0;
   }
   /// function TAddress.Addr(const Index: Integer) : TAddress
   ,Addr:function(Self, Index$2) {
      var Result = null;
      var LTarget$2 = 0;
      if (Index$2>=0) {
         LTarget$2 = Self.FOffset$1+Index$2;
         if (LTarget$2>=0&&LTarget$2<Self.FBuffer$2.byteLength) {
            Result = TAddress.Create$33$($New(TAddress),Self.FBuffer$2,LTarget$2);
         } else {
            throw Exception.Create($New(EAddress),"Failed to derive address, entrypoint exceeds segment bounds error");
         }
      } else {
         throw Exception.Create($New(EAddress),"Failed to derive address, invalid entrypoint error");
      }
      return Result
   }
   /// function TAddress.Adopt() : TBinaryData
   ,Adopt:function(Self) {
      var Result = null;
      if (Self.FBuffer$2) {
         Result = TBinaryData.Create$42$($New(TBinaryData),Self.FBuffer$2.buffer);
      } else {
         throw Exception.Create($New(EAddress),"Failed to adopt memory, null reference error");
      }
      return Result
   }
   /// constructor TAddress.Create(const Memory: TBinaryData)
   ,Create$35:function(Self, Memory$6) {
      if (Memory$6!==null) {
         if (TAllocation.GetSize$3$(Memory$6)>0) {
            TAddress.Create$33$(Self,TAllocation.GetHandle$(Memory$6),0);
         } else {
            throw Exception.Create($New(Exception),"Invalid memory object error");
         }
      } else {
         throw Exception.Create($New(Exception),"Invalid memory object error");
      }
      return Self
   }
   /// constructor TAddress.Create(const Stream: TStream)
   ,Create$34:function(Self, Stream) {
      var LRef$1 = undefined;
      if ($Is(Stream,TMemoryStream)) {
         LRef$1 = TAllocation.GetHandle$($As(Stream,TMemoryStream).FBuffer$1);
         if (LRef$1) {
            TAddress.Create$33$(Self,LRef$1,0);
         } else {
            throw Exception.Create($New(EAddress),$R[3]);
         }
      } else {
         throw Exception.Create($New(EAddress),$R[4]);
      }
      return Self
   }
   /// constructor TAddress.Create(const Segment: TBufferHandle; const Offset: Integer)
   ,Create$33:function(Self, Segment$5, Offset$5) {
      TObject.Create(Self);
      if (Segment$5&&TBufferHandleHelper$Valid$5(Segment$5)) {
         Self.FBuffer$2 = Segment$5;
      } else {
         throw Exception.Create($New(EAddress),"Failed to derive address, invalid segment error");
      }
      if (Offset$5>=0) {
         Self.FOffset$1 = Offset$5;
      } else {
         throw Exception.Create($New(EAddress),"Failed to derive address, invalid offset error");
      }
      return Self
   }
   /// destructor TAddress.Destroy()
   ,Destroy:function(Self) {
      Self.FBuffer$2 = null;
      Self.FOffset$1 = 0;
      TObject.Destroy(Self);
   }
   /// procedure TAddress.ForEach(const CB: TAddressEnumProc)
   ,ForEach$1:function(Self, CB$11) {
      var LCancel = {v:false},
         LLength = 0,
         x$23 = 0;
      if (CB$11) {
         LCancel.v = false;
         LLength = Self.FBuffer$2.byteLength;
         var $temp26;
         for(x$23=Self.FOffset$1,$temp26=LLength;x$23<$temp26;x$23++) {
            CB$11(x$23,LCancel);
            if (LCancel.v) {
               break;
            }
         }
      }
   }
   /// function TAddress.GetSize() : Integer
   ,GetSize$2:function(Self) {
      return Self.FBuffer$2.byteLength;
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$35$:function($){return $.ClassType.Create$35.apply($.ClassType, arguments)}
   ,Create$34$:function($){return $.ClassType.Create$34.apply($.ClassType, arguments)}
   ,Create$33$:function($){return $.ClassType.Create$33.apply($.ClassType, arguments)}
};
/// EUnManaged = class (EW3Exception)
var EUnManaged = {
   $ClassName:"EUnManaged",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EAddress = class (EW3Exception)
var EAddress = {
   $ClassName:"EAddress",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TW3CustomDictionary = class (TObject)
var TW3CustomDictionary = {
   $ClassName:"TW3CustomDictionary",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FLUT = undefined;
   }
   /// procedure TW3CustomDictionary.Clear()
   ,Clear$7:function(Self) {
      Self.FLUT = TVariant.CreateObject();
   }
   /// function TW3CustomDictionary.Contains(const ItemKeys: TStrArray) : Boolean
   ,Contains$1:function(Self, ItemKeys) {
      var Result = false;
      var a$458 = 0;
      var ItemName = "";
      if (ItemKeys.length>0) {
         var $temp27;
         for(a$458=0,$temp27=ItemKeys.length;a$458<$temp27;a$458++) {
            ItemName = ItemKeys[a$458];
            Result = TW3CustomDictionary.Contains(Self,ItemName);
            if (!Result) {
               break;
            }
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EW3Exception),$R[0],["TW3CustomDictionary.Contains", TObject.ClassName(Self.ClassType), $R[66]]);
      }
      return Result
   }
   /// function TW3CustomDictionary.Contains(const ItemKey: String) : Boolean
   ,Contains:function(Self, ItemKey) {
      var Result = false;
      if (ItemKey.length>0) {
         Result = (Self.FLUT.hasOwnProperty(ItemKey)?true:false);
      } else {
         throw EW3Exception.CreateFmt$($New(EW3Exception),$R[0],["TW3CustomDictionary.Contains", TObject.ClassName(Self.ClassType), $R[65]]);
      }
      return Result
   }
   /// constructor TW3CustomDictionary.Create()
   ,Create$74:function(Self) {
      TObject.Create(Self);
      Self.FLUT = TVariant.CreateObject();
      return Self
   }
   /// procedure TW3CustomDictionary.Delete(const ItemKey: String)
   ,Delete$2:function(Self, ItemKey$1) {
      if (ItemKey$1.length>0) {
         try {
            delete (Self.FLUT[ItemKey$1]);
         } catch ($e) {
            var e$10 = $W($e);
            throw EW3Exception.CreateFmt$($New(EW3Exception),$R[0],["TW3CustomDictionary.Delete", TObject.ClassName(Self.ClassType), e$10.FMessage]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EW3Exception),$R[0],["TW3CustomDictionary.Delete", TObject.ClassName(Self.ClassType), $R[65]]);
      }
   }
   /// destructor TW3CustomDictionary.Destroy()
   ,Destroy:function(Self) {
      TW3CustomDictionary.Clear$7(Self);
      Self.FLUT = undefined;
      TObject.Destroy(Self);
   }
   /// procedure TW3CustomDictionary.ForEach(const Callback: TW3DictionaryCallback)
   ,ForEach$4:function(Self, Callback$2) {
      var LObj$1 = {};
      var LName$2 = "";
      if (Callback$2) {
         for (LName$2 in Self.FLUT) {
            LObj$1.v = Self.FLUT[LName$2];
            if (Callback$2(LName$2,LObj$1)==160) {
               (Self.FLUT)[LName$2] = LObj$1;
            } else {
               break;
            }
         }
      }
   }
   /// function TW3CustomDictionary.GetData() : JObject
   ,GetData:function(Self) {
      return Self.FLUT;
   }
   /// function TW3CustomDictionary.GetEmpty() : Boolean
   ,GetEmpty:function(Self) {
      var Result = false;
      var LKeys = [];
      LKeys = TW3CustomDictionary.GetKeys$1(Self);
      Result = (LKeys.length==0);
      return Result
   }
   /// function TW3CustomDictionary.GetItem(const ItemKey: String) : Variant
   ,GetItem$1:function(Self, ItemKey$2) {
      var Result = undefined;
      if (ItemKey$2.length>0) {
         try {
            Result = Self.FLUT[ItemKey$2];
         } catch ($e) {
            var e$11 = $W($e);
            throw EW3Exception.CreateFmt$($New(EW3Exception),$R[0],["TW3CustomDictionary.GetItem", TObject.ClassName(Self.ClassType), e$11.FMessage]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EW3Exception),$R[0],["TW3CustomDictionary.GetItem", TObject.ClassName(Self.ClassType), $R[65]]);
      }
      return Result
   }
   /// function TW3CustomDictionary.GetKeyCount() : Integer
   ,GetKeyCount:function(Self) {
      var Result = 0;
      var LProperty = "";
      for (LProperty in Self.FLUT) {
         ++Result      }
      return Result
   }
   /// function TW3CustomDictionary.GetKeys() : TStrArray
   ,GetKeys$1:function(Self) {
      return TVariant.Properties(Self.FLUT);
   }
   /// function TW3CustomDictionary.KeyExists(const ItemKey: String) : Boolean
   ,KeyExists:function(Self, ItemKey$3) {
      var Result = false;
      if (ItemKey$3.length>0) {
         Result = (Self.FLUT.hasOwnProperty(ItemKey$3)?true:false);
      } else {
         throw EW3Exception.CreateFmt$($New(EW3Exception),$R[0],["TW3CustomDictionary.KeyExists", TObject.ClassName(Self.ClassType), $R[65]]);
      }
      return Result
   }
   /// procedure TW3CustomDictionary.SetItem(const ItemKey: String; const KeyValue: Variant)
   ,SetItem:function(Self, ItemKey$4, KeyValue) {
      if (ItemKey$4.length>0) {
         try {
            Self.FLUT[ItemKey$4] = KeyValue;
         } catch ($e) {
            var e$12 = $W($e);
            throw EW3Exception.CreateFmt$($New(EW3Exception),$R[0],["TW3CustomDictionary.SetItem", TObject.ClassName(Self.ClassType), e$12.FMessage]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EW3Exception),$R[0],["TW3CustomDictionary.SetItem", TObject.ClassName(Self.ClassType), $R[65]]);
      }
   }
   /// function TW3CustomDictionary.ToJSON() : String
   ,ToJSON$2:function(Self) {
      var Result = "";
      Result = JSON.stringify(Self.FLUT);
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$74$:function($){return $.ClassType.Create$74($)}
   ,Delete$2$:function($){return $.ClassType.Delete$2.apply($.ClassType, arguments)}
   ,GetEmpty$:function($){return $.ClassType.GetEmpty($)}
   ,GetItem$1$:function($){return $.ClassType.GetItem$1.apply($.ClassType, arguments)}
   ,SetItem$:function($){return $.ClassType.SetItem.apply($.ClassType, arguments)}
};
/// TW3VarDictionary = class (TW3CustomDictionary)
var TW3VarDictionary = {
   $ClassName:"TW3VarDictionary",$Parent:TW3CustomDictionary
   ,$Init:function ($) {
      TW3CustomDictionary.$Init($);
   }
   ,Destroy:TW3CustomDictionary.Destroy
   ,Create$74:TW3CustomDictionary.Create$74
   ,Delete$2:TW3CustomDictionary.Delete$2
   ,GetEmpty:TW3CustomDictionary.GetEmpty
   ,GetItem$1:TW3CustomDictionary.GetItem$1
   ,SetItem:TW3CustomDictionary.SetItem
};
/// TW3StrDictionary = class (TW3CustomDictionary)
var TW3StrDictionary = {
   $ClassName:"TW3StrDictionary",$Parent:TW3CustomDictionary
   ,$Init:function ($) {
      TW3CustomDictionary.$Init($);
   }
   /// anonymous TSourceMethodSymbol
   ,a$106:function(Self, ItemKey$5) {
      return TVariant.AsString(TW3CustomDictionary.GetItem$1$(Self,ItemKey$5));
   }
   /// anonymous TSourceMethodSymbol
   ,a$105:function(Self, ItemKey$6, Value$111) {
      TW3CustomDictionary.SetItem$(Self,ItemKey$6,Value$111);
   }
   ,Destroy:TW3CustomDictionary.Destroy
   ,Create$74:TW3CustomDictionary.Create$74
   ,Delete$2:TW3CustomDictionary.Delete$2
   ,GetEmpty:TW3CustomDictionary.GetEmpty
   ,GetItem$1:TW3CustomDictionary.GetItem$1
   ,SetItem:TW3CustomDictionary.SetItem
};
/// TW3ObjDictionary = class (TW3CustomDictionary)
var TW3ObjDictionary = {
   $ClassName:"TW3ObjDictionary",$Parent:TW3CustomDictionary
   ,$Init:function ($) {
      TW3CustomDictionary.$Init($);
   }
   /// anonymous TSourceMethodSymbol
   ,a$108:function(Self, ItemKey$7) {
      return TVariant.AsObject(TW3CustomDictionary.GetItem$1$(Self,ItemKey$7));
   }
   /// anonymous TSourceMethodSymbol
   ,a$107:function(Self, ItemKey$8, Value$112) {
      TW3CustomDictionary.SetItem$(Self,ItemKey$8,Value$112);
   }
   ,Destroy:TW3CustomDictionary.Destroy
   ,Create$74:TW3CustomDictionary.Create$74
   ,Delete$2:TW3CustomDictionary.Delete$2
   ,GetEmpty:TW3CustomDictionary.GetEmpty
   ,GetItem$1:TW3CustomDictionary.GetItem$1
   ,SetItem:TW3CustomDictionary.SetItem
};
/// TW3IntDictionary = class (TW3CustomDictionary)
var TW3IntDictionary = {
   $ClassName:"TW3IntDictionary",$Parent:TW3CustomDictionary
   ,$Init:function ($) {
      TW3CustomDictionary.$Init($);
   }
   /// anonymous TSourceMethodSymbol
   ,a$110:function(Self, ItemKey$9) {
      return TVariant.AsInteger(TW3CustomDictionary.GetItem$1$(Self,ItemKey$9));
   }
   /// anonymous TSourceMethodSymbol
   ,a$109:function(Self, ItemKey$10, Value$113) {
      TW3CustomDictionary.SetItem$(Self,ItemKey$10,Value$113);
   }
   ,Destroy:TW3CustomDictionary.Destroy
   ,Create$74:TW3CustomDictionary.Create$74
   ,Delete$2:TW3CustomDictionary.Delete$2
   ,GetEmpty:TW3CustomDictionary.GetEmpty
   ,GetItem$1:TW3CustomDictionary.GetItem$1
   ,SetItem:TW3CustomDictionary.SetItem
};
/// TW3FloatDictionary = class (TW3CustomDictionary)
var TW3FloatDictionary = {
   $ClassName:"TW3FloatDictionary",$Parent:TW3CustomDictionary
   ,$Init:function ($) {
      TW3CustomDictionary.$Init($);
   }
   /// anonymous TSourceMethodSymbol
   ,a$112:function(Self, ItemKey$11) {
      return TVariant.AsFloat(TW3CustomDictionary.GetItem$1$(Self,ItemKey$11));
   }
   /// anonymous TSourceMethodSymbol
   ,a$111:function(Self, ItemKey$12, Value$114) {
      TW3CustomDictionary.SetItem$(Self,ItemKey$12,Value$114);
   }
   ,Destroy:TW3CustomDictionary.Destroy
   ,Create$74:TW3CustomDictionary.Create$74
   ,Delete$2:TW3CustomDictionary.Delete$2
   ,GetEmpty:TW3CustomDictionary.GetEmpty
   ,GetItem$1:TW3CustomDictionary.GetItem$1
   ,SetItem:TW3CustomDictionary.SetItem
};
/// TW3OwnedErrorObject = class (TW3OwnedObject)
var TW3OwnedErrorObject = {
   $ClassName:"TW3OwnedErrorObject",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.FLastError$1 = "";
      $.FOptions$4 = null;
   }
   /// procedure TW3OwnedErrorObject.ClearLastError()
   ,ClearLastError$1:function(Self) {
      Self.FLastError$1 = "";
   }
   /// constructor TW3OwnedErrorObject.Create(const AOwner: TObject)
   ,Create$16:function(Self, AOwner$1) {
      TW3OwnedObject.Create$16(Self,AOwner$1);
      Self.FOptions$4 = TW3ErrorObjectOptions.Create$48$($New(TW3ErrorObjectOptions));
      return Self
   }
   /// destructor TW3OwnedErrorObject.Destroy()
   ,Destroy:function(Self) {
      TObject.Free(Self.FOptions$4);
      TObject.Destroy(Self);
   }
   /// function TW3OwnedErrorObject.GetFailed() : Boolean
   ,GetFailed$1:function(Self) {
      return Self.FLastError$1.length>0;
   }
   /// function TW3OwnedErrorObject.GetLastError() : String
   ,GetLastError$1:function(Self) {
      return Self.FLastError$1;
   }
   /// procedure TW3OwnedErrorObject.SetLastError(const ErrorText: String)
   ,SetLastError$1:function(Self, ErrorText$1) {
      Self.FLastError$1 = Trim$_String_(ErrorText$1);
      if (Self.FLastError$1.length>0) {
         if (Self.FOptions$4.AutoWriteToConsole) {
            if (console) {
          console.log( (Self.FLastError$1) );
        }
         }
         if (Self.FOptions$4.ThrowExceptions) {
            throw Exception.Create($New(EW3ErrorObject),Self.FLastError$1);
         }
      }
   }
   /// procedure TW3OwnedErrorObject.SetLastErrorF(const ErrorText: String; const Values: array of const)
   ,SetLastErrorF$1:function(Self, ErrorText$2, Values$13) {
      Self.FLastError$1 = Format(ErrorText$2,Values$13.slice(0));
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16$:function($){return $.ClassType.Create$16.apply($.ClassType, arguments)}
   ,SetLastError$1$:function($){return $.ClassType.SetLastError$1.apply($.ClassType, arguments)}
};
TW3OwnedErrorObject.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3OwnedLockedErrorObject = class (TW3OwnedErrorObject)
var TW3OwnedLockedErrorObject = {
   $ClassName:"TW3OwnedLockedErrorObject",$Parent:TW3OwnedErrorObject
   ,$Init:function ($) {
      TW3OwnedErrorObject.$Init($);
      $.OnObjectUnLocked = null;
      $.OnObjectLocked = null;
      $.FLocked$2 = 0;
   }
   /// procedure TW3OwnedLockedErrorObject.DisableAlteration()
   ,DisableAlteration$2:function(Self) {
      ++Self.FLocked$2;
      if (Self.FLocked$2==1) {
         TW3OwnedLockedErrorObject.ObjectLocked$2$(Self);
      }
   }
   /// procedure TW3OwnedLockedErrorObject.EnableAlteration()
   ,EnableAlteration$2:function(Self) {
      if (Self.FLocked$2>0) {
         --Self.FLocked$2;
         if (!Self.FLocked$2) {
            TW3OwnedLockedErrorObject.ObjectUnLocked$2$(Self);
         }
      }
   }
   /// function TW3OwnedLockedErrorObject.GetLockState() : Boolean
   ,GetLockState$2:function(Self) {
      return Self.FLocked$2>0;
   }
   /// procedure TW3OwnedLockedErrorObject.ObjectLocked()
   ,ObjectLocked$2:function(Self) {
      if (Self.OnObjectLocked) {
         Self.OnObjectLocked(Self);
      }
   }
   /// procedure TW3OwnedLockedErrorObject.ObjectUnLocked()
   ,ObjectUnLocked$2:function(Self) {
      if (Self.OnObjectUnLocked) {
         Self.OnObjectUnLocked(Self);
      }
   }
   ,Destroy:TW3OwnedErrorObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,SetLastError$1:TW3OwnedErrorObject.SetLastError$1
   ,ObjectLocked$2$:function($){return $.ClassType.ObjectLocked$2($)}
   ,ObjectUnLocked$2$:function($){return $.ClassType.ObjectUnLocked$2($)}
};
TW3OwnedLockedErrorObject.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3HandleBasedObject = class (TObject)
var TW3HandleBasedObject = {
   $ClassName:"TW3HandleBasedObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FHandle$2 = undefined;
   }
   /// procedure TW3HandleBasedObject.ObjectHandleChanged(const PreviousHandle: THandle; const NewHandle: THandle)
   ,ObjectHandleChanged:function(Self, PreviousHandle, NewHandle) {
      /* null */
   }
   /// function TW3HandleBasedObject.AcceptObjectHandle(const CandidateHandle: THandle) : Boolean
   ,AcceptObjectHandle:function(Self, CandidateHandle) {
      return true;
   }
   /// procedure TW3HandleBasedObject.SetObjectHandle(const NewHandle: THandle)
   ,SetObjectHandle:function(Self, NewHandle$1) {
      var LTemp$6 = undefined;
      if (TW3HandleBasedObject.AcceptObjectHandle(Self,NewHandle$1)) {
         LTemp$6 = Self.FHandle$2;
         Self.FHandle$2 = NewHandle$1;
         TW3HandleBasedObject.ObjectHandleChanged$(Self,LTemp$6,Self.FHandle$2);
      } else {
         throw EW3Exception.CreateFmt$($New(EW3HandleBasedObject),$R[64],["TW3HandleBasedObject.SetObjectHandle"]);
      }
   }
   /// function TW3HandleBasedObject.GetObjectHandle() : THandle
   ,GetObjectHandle:function(Self) {
      return Self.FHandle$2;
   }
   ,Destroy:TObject.Destroy
   ,ObjectHandleChanged$:function($){return $.ClassType.ObjectHandleChanged.apply($.ClassType, arguments)}
   ,SetObjectHandle$:function($){return $.ClassType.SetObjectHandle.apply($.ClassType, arguments)}
   ,GetObjectHandle$:function($){return $.ClassType.GetObjectHandle($)}
};
/// TW3ErrorObjectOptions = class (TObject)
var TW3ErrorObjectOptions = {
   $ClassName:"TW3ErrorObjectOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.AutoWriteToConsole = $.ThrowExceptions = $.AutoResetError = false;
   }
   /// constructor TW3ErrorObjectOptions.Create()
   ,Create$48:function(Self) {
      Self.AutoResetError = true;
      Self.AutoWriteToConsole = false;
      Self.ThrowExceptions = false;
      return Self
   }
   ,Destroy:TObject.Destroy
   ,Create$48$:function($){return $.ClassType.Create$48($)}
};
/// TW3ErrorObject = class (TObject)
var TW3ErrorObject = {
   $ClassName:"TW3ErrorObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FLastError = "";
      $.FOptions = null;
   }
   /// procedure TW3ErrorObject.ClearLastError()
   ,ClearLastError:function(Self) {
      Self.FLastError = "";
   }
   /// constructor TW3ErrorObject.Create()
   ,Create$3:function(Self) {
      TObject.Create(Self);
      Self.FOptions = TW3ErrorObjectOptions.Create$48$($New(TW3ErrorObjectOptions));
      return Self
   }
   /// destructor TW3ErrorObject.Destroy()
   ,Destroy:function(Self) {
      TObject.Free(Self.FOptions);
      TObject.Destroy(Self);
   }
   /// function TW3ErrorObject.GetExceptionClass() : TW3ExceptionClass
   ,GetExceptionClass:function(Self) {
      return EW3ErrorObject;
   }
   /// function TW3ErrorObject.GetFailed() : Boolean
   ,GetFailed:function(Self) {
      return Self.FLastError.length>0;
   }
   /// function TW3ErrorObject.GetLastError() : String
   ,GetLastError:function(Self) {
      return Self.FLastError;
   }
   /// procedure TW3ErrorObject.SetLastError(const ErrorText: String)
   ,SetLastError:function(Self, ErrorText$3) {
      var ErrClass = null;
      Self.FLastError = Trim$_String_(ErrorText$3);
      if (Self.FLastError.length>0) {
         if (Self.FOptions.AutoWriteToConsole) {
            if (console) {
          console.log( (Self.FLastError) );
       }
         }
         if (Self.FOptions.ThrowExceptions) {
            ErrClass = TW3ErrorObject.GetExceptionClass$(Self);
            if (!ErrClass) {
               ErrClass = EW3ErrorObject;
            }
            throw Exception.Create($NewDyn(ErrClass," in TW3ErrorObject.SetLastError [line: 340, column: 22, file: System.Objects]"),Self.FLastError);
         }
      }
   }
   /// procedure TW3ErrorObject.SetLastErrorF(const ErrorText: String; const Values: array of const)
   ,SetLastErrorF:function(Self, ErrorText$4, Values$14) {
      TW3ErrorObject.SetLastError$(Self,Format(ErrorText$4,Values$14.slice(0)));
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3$:function($){return $.ClassType.Create$3($)}
   ,GetExceptionClass$:function($){return $.ClassType.GetExceptionClass($)}
   ,SetLastError$:function($){return $.ClassType.SetLastError.apply($.ClassType, arguments)}
};
TW3ErrorObject.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// EW3Persistence = class (EW3Exception)
var EW3Persistence = {
   $ClassName:"EW3Persistence",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EW3HandleBasedObject = class (EW3Exception)
var EW3HandleBasedObject = {
   $ClassName:"EW3HandleBasedObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EW3ErrorObject = class (EW3Exception)
var EW3ErrorObject = {
   $ClassName:"EW3ErrorObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TJSONObject = class (TObject)
var TJSONObject = {
   $ClassName:"TJSONObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FClone = false;
      $.FInstance = undefined;
      $.FOptions$6 = [0];
   }
   /// anonymous TSourceMethodSymbol
   ,a$98:function(Self) {
      return Self.FInstance;
   }
   /// function TJSONObject.AddOrSet(const PropertyName: String; const Data: Variant) : TJSONObject
   ,AddOrSet:function(Self, PropertyName, Data$37) {
      var Result = null;
      Result = Self;
      if (TJSONObject.Exists$3(Self,PropertyName)) {
         if ($SetIn(Self.FOptions$6,3,0,4)) {
            Self.FInstance[PropertyName] = Data$37;
         } else {
            throw EW3Exception.CreateFmt$($New(EJSONObject),"Failed to set value[%s], instance does not allow alteration",[PropertyName]);
         }
      } else if ($SetIn(Self.FOptions$6,1,0,4)) {
         Self.FInstance[PropertyName] = Data$37;
      } else {
         throw EW3Exception.CreateFmt$($New(EJSONObject),"Failed to add value [%s], instance does not allow new properties",[PropertyName]);
      }
      return Result
   }
   /// procedure TJSONObject.Clear()
   ,Clear$3:function(Self) {
      Self.FInstance = TVariant.CreateObject();
   }
   /// constructor TJSONObject.Create(const Instance: TJsInstance; const Options: TJSONObjectOptions; Clone: Boolean)
   ,Create$69:function(Self, Instance$6, Options$4, Clone$1) {
      TObject.Create(Self);
      Self.FOptions$6 = Options$4.slice(0);
      if (TW3VariantHelper$Valid$2(Instance$6)) {
         if (TW3VariantHelper$IsObject(Instance$6)) {
            if (Clone$1) {
               Self.FInstance = TVariant.CreateObject();
               TVariant.ForEachProperty(Instance$6,function (Name$11, Data$38) {
                  var Result = 1;
                  TJSONObject.AddOrSet(Self,Name$11,Data$38);
                  Result = 1;
                  return Result
               });
            } else {
               Self.FInstance = Instance$6;
            }
         } else {
            throw Exception.Create($New(EJSONObject),"Failed to clone instance, reference is not an object");
         }
      } else {
         if ($SetIn(Self.FOptions$6,0,0,4)) {
            Self.FInstance = TVariant.CreateObject();
         } else {
            throw Exception.Create($New(EJSONObject),"Instance was nil, provided options does not allow initialization error");
         }
      }
      return Self
   }
   /// constructor TJSONObject.Create(const Instance: TJsInstance; const Options: TJSONObjectOptions)
   ,Create$68:function(Self, Instance$7, Options$5) {
      TObject.Create(Self);
      Self.FOptions$6 = Options$5.slice(0);
      if (TW3VariantHelper$Valid$2(Instance$7)&&TW3VariantHelper$IsObject(Instance$7)) {
         Self.FInstance = Instance$7;
      } else {
         if ($SetIn(Self.FOptions$6,0,0,4)) {
            Self.FInstance = TVariant.CreateObject();
         } else {
            throw Exception.Create($New(EJSONObject),"Instance was nil, provided options does not allow initialization error");
         }
      }
      return Self
   }
   /// constructor TJSONObject.Create(const Instance: TJsInstance)
   ,Create$67:function(Self, Instance$8) {
      TObject.Create(Self);
      Self.FOptions$6 = [15];
      if (Instance$8) {
         if (TW3VariantHelper$IsObject(Instance$8)) {
            Self.FInstance = Instance$8;
         } else {
            throw Exception.Create($New(EJSONObject),"Failed to attach to JSON instance, reference is not an object");
         }
      } else {
         Self.FInstance = TVariant.CreateObject();
      }
      return Self
   }
   /// constructor TJSONObject.Create()
   ,Create$66:function(Self) {
      TObject.Create(Self);
      Self.FOptions$6 = [15];
      Self.FInstance = TVariant.CreateObject();
      return Self
   }
   /// destructor TJSONObject.Destroy()
   ,Destroy$19:function(Self) {
      Self.FInstance = null;
      TObject.Destroy(Self);
   }
   /// function TJSONObject.Exists(const PropertyName: String) : Boolean
   ,Exists$3:function(Self, PropertyName$1) {
      return (Object.hasOwnProperty.call(Self.FInstance,PropertyName$1)?true:false);
   }
   /// function TJSONObject.ForEach(const Callback: TTJSONObjectEnumProc) : TJSONObject
   ,ForEach$3:function(Self, Callback$3) {
      var Result = null;
      var LData = {};
      var NameList = [],
         a$459 = 0;
      var xName = "";
      Result = Self;
      if (Callback$3) {
         NameList = TJSONObject.Keys(Self);
         var $temp28;
         for(a$459=0,$temp28=NameList.length;a$459<$temp28;a$459++) {
            xName = NameList[a$459];
            TJSONObject.Read$5(Self,xName,LData);
            if (Callback$3(xName,LData.v)==1) {
               TJSONObject.Write$5(Self,xName,LData.v);
            } else {
               break;
            }
         }
      }
      return Result
   }
   /// procedure TJSONObject.FromJSON(const Text: String)
   ,FromJSON:function(Self, Text$29) {
      Self.FInstance = JSON.parse(Text$29);
   }
   /// function TJSONObject.GetItem(const Index: Integer) : Variant
   ,GetItem:function(Self, Index$3) {
      var Result = undefined;
      var LRef$2 = undefined;
      LRef$2 = Self.FInstance;
      Result = Object.keys(LRef$2)[Index$3];
      return Result
   }
   /// function TJSONObject.GetItemByName(const Name: String) : Variant
   ,GetItemByName:function(Self, Name$12) {
      return Self.FInstance[Name$12];
   }
   /// function TJSONObject.GetPropertyCount() : Integer
   ,GetPropertyCount:function(Self) {
      var Result = 0;
      var LRef$3 = undefined;
      LRef$3 = Self.FInstance;
      Result = Object.keys(LRef$3).length;
      return Result
   }
   /// function TJSONObject.HighlightJSON(const JsonStr: String) : String
   ,HighlightJSON:function(Self, JsonStr) {
      var Result = "";
      Result = "";
      function syntaxHighlight(json) {
    if (typeof json != 'string') {
         json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
    }
    return syntaxHighlight(JsonStr);
      return Result
   }
   /// function TJSONObject.Keys() : TStrArray
   ,Keys:function(Self) {
      return TVariant.Properties(Self.FInstance);
   }
   /// procedure TJSONObject.LoadFromBuffer(const Buffer: TBinaryData)
   ,LoadFromBuffer$2:function(Self, Buffer$2) {
      var LBytes$7 = [],
         LText$3 = "";
      LBytes$7 = TBinaryData.ToBytes(Buffer$2);
      LText$3 = TString.DecodeUTF8(TString,LBytes$7);
      Self.FInstance = JSON.parse(LText$3);
   }
   /// procedure TJSONObject.LoadFromStream(const Stream: TStream)
   ,LoadFromStream$3:function(Self, Stream$1) {
      var LBytes$8 = [],
         LText$4 = "";
      LBytes$8 = TStream.Read$1$(Stream$1,TStream.GetSize$(Stream$1));
      LText$4 = TString.DecodeUTF8(TString,LBytes$8);
      Self.FInstance = JSON.parse(LText$4);
   }
   /// function TJSONObject.Read(const PropertyName: String; var Data: Variant) : TJSONObject
   ,Read$5:function(Self, PropertyName$2, Data$39) {
      var Result = null;
      Result = Self;
      if (TJSONObject.Exists$3(Self,PropertyName$2)) {
         Data$39.v = Self.FInstance[PropertyName$2];
      } else {
         throw EW3Exception.CreateFmt$($New(EJSONObject),"Failed to read value, property [%s] not found error",[PropertyName$2]);
      }
      return Result
   }
   /// function TJSONObject.ReadBoolean(const PropertyName: String) : Boolean
   ,ReadBoolean$1:function(Self, PropertyName$3) {
      var Result = false;
      if (TJSONObject.Exists$3(Self,PropertyName$3)) {
         Result = TVariant.AsBool(Self.FInstance[PropertyName$3]);
      } else {
         throw EW3Exception.CreateFmt$($New(EJSONObject),"Failed to read value, property [%s] not found error",[PropertyName$3]);
      }
      return Result
   }
   /// function TJSONObject.ReadDateTime(const PropertyName: String) : TDateTime
   ,ReadDateTime$2:function(Self, PropertyName$4) {
      var Result = undefined;
      if (TJSONObject.Exists$3(Self,PropertyName$4)) {
         Result = TVariant.AsFloat(Self.FInstance[PropertyName$4]);
      } else {
         throw EW3Exception.CreateFmt$($New(EJSONObject),"Failed to read value, property [%s] not found error",[PropertyName$4]);
      }
      return Result
   }
   /// function TJSONObject.ReadFloat(const PropertyName: String) : Float
   ,ReadFloat$1:function(Self, PropertyName$5) {
      var Result = 0;
      if (TJSONObject.Exists$3(Self,PropertyName$5)) {
         Result = TVariant.AsFloat(Self.FInstance[PropertyName$5]);
      } else {
         throw EW3Exception.CreateFmt$($New(EJSONObject),"Failed to read value, property [%s] not found error",[PropertyName$5]);
      }
      return Result
   }
   /// function TJSONObject.ReadInteger(const PropertyName: String) : Integer
   ,ReadInteger$1:function(Self, PropertyName$6) {
      var Result = 0;
      if (TJSONObject.Exists$3(Self,PropertyName$6)) {
         Result = TVariant.AsInteger(Self.FInstance[PropertyName$6]);
      } else {
         throw EW3Exception.CreateFmt$($New(EJSONObject),"Failed to read value, property [%s] not found error",[PropertyName$6]);
      }
      return Result
   }
   /// function TJSONObject.ReadString(const PropertyName: String) : String
   ,ReadString$3:function(Self, PropertyName$7) {
      var Result = "";
      if (TJSONObject.Exists$3(Self,PropertyName$7)) {
         Result = TVariant.AsString(Self.FInstance[PropertyName$7]);
      } else {
         throw EW3Exception.CreateFmt$($New(EJSONObject),"Failed to read value, property [%s] not found error",[PropertyName$7]);
      }
      return Result
   }
   /// procedure TJSONObject.SaveToBuffer(const Buffer: TBinaryData)
   ,SaveToBuffer$2:function(Self, Buffer$3) {
      var LText$5 = "",
         LBytes$9 = [];
      LText$5 = JSON.stringify(Self.FInstance)+"   ";
      LBytes$9 = TString.EncodeUTF8(TString,LText$5);
      TBinaryData.AppendBytes$(Buffer$3,LBytes$9);
   }
   /// procedure TJSONObject.SaveToStream(const Stream: TStream)
   ,SaveToStream$3:function(Self, Stream$2) {
      var LText$6 = "",
         LBytes$10 = [];
      LText$6 = JSON.stringify(Self.FInstance)+"   ";
      LBytes$10 = TString.EncodeUTF8(TString,LText$6);
      TStream.Write$2$(Stream$2,LBytes$10);
   }
   /// function TJSONObject.ToJSON() : String
   ,ToJSON:function(Self) {
      return JSON.stringify(Self.FInstance,null,2);
   }
   /// function TJSONObject.ToString() : String
   ,ToString$4:function(Self) {
      return JSON.stringify(Self.FInstance);
   }
   /// function TJSONObject.Write(const PropertyName: String; const Data: Variant) : TJSONObject
   ,Write$5:function(Self, PropertyName$8, Data$40) {
      var Result = null;
      Result = Self;
      if (TJSONObject.Exists$3(Self,PropertyName$8)) {
         if ($SetIn(Self.FOptions$6,3,0,4)) {
            Self.FInstance[PropertyName$8] = Data$40;
         } else {
            throw EW3Exception.CreateFmt$($New(EJSONObject),"Failed to set value[%s], instance does not allow alteration",[PropertyName$8]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EJSONObject),"Failed to write value, property [%s] not found error",[PropertyName$8]);
      }
      return Result
   }
   ,Destroy:TObject.Destroy
   ,Create$66$:function($){return $.ClassType.Create$66($)}
};
/// EJSONObject = class (EW3Exception)
var EJSONObject = {
   $ClassName:"EJSONObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TAllocation = class (TDataTypeConverter)
var TAllocation = {
   $ClassName:"TAllocation",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FHandle = undefined;
      $.FOptions$2 = null;
      $.FSize = 0;
   }
   /// anonymous TSourceMethodSymbol
   ,a$29:function(Self) {
      return (Self.FHandle?true:false);
   }
   /// anonymous TSourceMethodSymbol
   ,a$28:function(Self) {
      return ((!Self.FHandle)?true:false);
   }
   /// procedure TAllocation.Allocate(const NumberOfBytes: Integer)
   ,Allocate:function(Self, NumberOfBytes) {
      var NewSize = 0;
      if (Self.FHandle) {
         TAllocation.Release$1(Self);
      }
      if (NumberOfBytes>0) {
         NewSize = 0;
         if (Self.FOptions$2.FUseCache) {
            NewSize = TInteger.ToNearest(NumberOfBytes,Self.FOptions$2.FCacheSize);
         } else {
            NewSize = NumberOfBytes;
         }
         Self.FHandle = TUnManaged.AllocMemA(TUnManaged,NewSize);
         Self.FSize = NumberOfBytes;
         TAllocation.HandleAllocated$(Self);
      }
   }
   /// constructor TAllocation.Create(const ByteSize: Integer)
   ,Create$38:function(Self, ByteSize) {
      TDataTypeConverter.Create$15$(Self);
      if (ByteSize>0) {
         TAllocation.Allocate(Self,ByteSize);
      }
      return Self
   }
   /// constructor TAllocation.Create()
   ,Create$15:function(Self) {
      TDataTypeConverter.Create$15(Self);
      Self.FOptions$2 = TAllocationOptions.Create$36($New(TAllocationOptions),Self);
      return Self
   }
   /// function TAllocation.DataGetSize() : Integer
   ,DataGetSize$1:function(Self) {
      return TAllocation.GetSize$3$(Self);
   }
   /// function TAllocation.DataOffset() : Integer
   ,DataOffset$1:function(Self) {
      return 0;
   }
   /// function TAllocation.DataRead(const Offset: Integer; const ByteCount: Integer) : TByteArray
   ,DataRead$1:function(Self, Offset$6, ByteCount) {
      return TDataTypeConverter.TypedArrayToBytes(Self,TUnManaged.ReadMemoryA(TUnManaged,TAllocation.GetHandle$(Self),Offset$6,ByteCount));
   }
   /// procedure TAllocation.DataWrite(const Offset: Integer; const Bytes: TByteArray)
   ,DataWrite$1:function(Self, Offset$7, Bytes$8) {
      TUnManaged.WriteMemoryA(TUnManaged,TAllocation.GetHandle$(Self),Offset$7,TDataTypeConverter.BytesToTypedArray(Self,Bytes$8));
   }
   /// destructor TAllocation.Destroy()
   ,Destroy:function(Self) {
      if (Self.FHandle) {
         TAllocation.Release$1(Self);
      }
      TObject.Free(Self.FOptions$2);
      TDataTypeConverter.Destroy(Self);
   }
   /// function TAllocation.GetBufferHandle() : TBufferHandle
   ,GetBufferHandle:function(Self) {
      var Result = undefined;
      if (Self.FHandle) {
         Result = Self.FHandle.buffer;
      }
      return Result
   }
   /// function TAllocation.GetHandle() : TMemoryHandle
   ,GetHandle:function(Self) {
      return Self.FHandle;
   }
   /// function TAllocation.GetSize() : Integer
   ,GetSize$3:function(Self) {
      return Self.FSize;
   }
   /// function TAllocation.GetTotalSize() : Integer
   ,GetTotalSize$1:function(Self) {
      var Result = 0;
      if (Self.FHandle) {
         Result = parseInt(Self.FHandle.length,10);
      }
      return Result
   }
   /// function TAllocation.GetTransport() : IBinaryTransport
   ,GetTransport:function(Self) {
      return $AsIntf(Self,"IBinaryTransport");
   }
   /// procedure TAllocation.Grow(const NumberOfBytes: Integer)
   ,Grow:function(Self, NumberOfBytes$1) {
      var ExactNewSize = 0,
         TotalNewSize = 0;
      if (Self.FHandle) {
         ExactNewSize = Self.FSize+NumberOfBytes$1;
         if (Self.FOptions$2.FUseCache) {
            if (NumberOfBytes$1<TAllocationOptions.GetCacheFree$(Self.FOptions$2)) {
               (Self.FSize+= NumberOfBytes$1);
            } else {
               TotalNewSize = TInteger.ToNearest(ExactNewSize,Self.FOptions$2.FCacheSize);
               TAllocation.ReAllocate(Self,TotalNewSize);
               Self.FSize = ExactNewSize;
            }
         } else {
            TAllocation.ReAllocate(Self,ExactNewSize);
         }
      } else {
         TAllocation.Allocate(Self,NumberOfBytes$1);
      }
   }
   /// procedure TAllocation.HandleAllocated()
   ,HandleAllocated:function(Self) {
      /* null */
   }
   /// procedure TAllocation.HandleReleased()
   ,HandleReleased:function(Self) {
      /* null */
   }
   /// procedure TAllocation.ReAllocate(const NewSize: Integer)
   ,ReAllocate:function(Self, NewSize$1) {
      var LSizeToSet = 0;
      if (NewSize$1>0) {
         if (Self.FHandle) {
            if (NewSize$1!=Self.FSize) {
               TAllocation.HandleReleased$(Self);
               LSizeToSet = 0;
               if (Self.FOptions$2.FUseCache) {
                  LSizeToSet = TInteger.ToNearest(NewSize$1,Self.FOptions$2.FCacheSize);
               } else {
                  LSizeToSet = TInteger.ToNearest(NewSize$1,16);
               }
               Self.FHandle = TUnManaged.ReAllocMemA(TUnManaged,Self.FHandle,LSizeToSet);
               Self.FSize = NewSize$1;
            }
         } else {
            TAllocation.Allocate(Self,NewSize$1);
         }
         TAllocation.HandleAllocated$(Self);
      } else {
         TAllocation.Release$1(Self);
      }
   }
   /// procedure TAllocation.Release()
   ,Release$1:function(Self) {
      if (Self.FHandle) {
         try {
            Self.FHandle = null;
            Self.FSize = 0;
         } finally {
            TAllocation.HandleReleased$(Self);
         }
      }
   }
   /// procedure TAllocation.Shrink(const NumberOfBytes: Integer)
   ,Shrink:function(Self, NumberOfBytes$2) {
      var ExactNewSize$1 = 0,
         Spare = 0,
         AlignedSize = 0;
      if (Self.FHandle) {
         ExactNewSize$1 = TInteger.EnsureRange((Self.FSize-NumberOfBytes$2),0,2147483647);
         if (Self.FOptions$2.FUseCache) {
            if (ExactNewSize$1>0) {
               Spare = ExactNewSize$1%Self.FOptions$2.FCacheSize;
               if (Spare>0) {
                  AlignedSize = ExactNewSize$1;
                  (AlignedSize+= (Self.FOptions$2.FCacheSize-Spare));
                  TAllocation.ReAllocate(Self,AlignedSize);
                  Self.FSize = ExactNewSize$1;
               } else {
                  Self.FSize = ExactNewSize$1;
               }
            } else {
               TAllocation.Release$1(Self);
            }
         } else if (ExactNewSize$1>0) {
            TAllocation.ReAllocate(Self,ExactNewSize$1);
         } else {
            TAllocation.Release$1(Self);
         }
      }
   }
   /// procedure TAllocation.Transport(const Target: IBinaryTransport)
   ,Transport:function(Self, Target$5) {
      var Data$41 = [];
      if (Target$5===null) {
         throw Exception.Create($New(EAllocation),"Invalid transport interface, reference was NIL error");
      } else {
         if (!TAllocation.a$28(Self)) {
            try {
               Data$41 = TDataTypeConverter.TypedArrayToBytes(Self,TAllocation.GetHandle$(Self));
               Target$5[3](Target$5[0](),Data$41);
            } catch ($e) {
               var e$13 = $W($e);
               throw EW3Exception.CreateFmt$($New(EAllocation),"Data transport failed, mechanism threw exception %s with error [%s]",[TObject.ClassName(e$13.ClassType), e$13.FMessage]);
            }
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$15$:function($){return $.ClassType.Create$15($)}
   ,Create$38$:function($){return $.ClassType.Create$38.apply($.ClassType, arguments)}
   ,GetBufferHandle$:function($){return $.ClassType.GetBufferHandle($)}
   ,GetHandle$:function($){return $.ClassType.GetHandle($)}
   ,GetSize$3$:function($){return $.ClassType.GetSize$3($)}
   ,GetTotalSize$1$:function($){return $.ClassType.GetTotalSize$1($)}
   ,HandleAllocated$:function($){return $.ClassType.HandleAllocated($)}
   ,HandleReleased$:function($){return $.ClassType.HandleReleased($)}
};
TAllocation.$Intf={
   IBinaryTransport:[TAllocation.DataOffset$1,TAllocation.DataGetSize$1,TAllocation.DataRead$1,TAllocation.DataWrite$1]
   ,IAllocation:[TAllocation.GetHandle,TAllocation.GetTotalSize$1,TAllocation.GetSize$3,TAllocation.GetTransport,TAllocation.Allocate,TAllocation.Grow,TAllocation.Shrink,TAllocation.ReAllocate,TAllocation.Transport,TAllocation.Release$1]
}
/// TBinaryData = class (TAllocation)
var TBinaryData = {
   $ClassName:"TBinaryData",$Parent:TAllocation
   ,$Init:function ($) {
      TAllocation.$Init($);
      $.FDataView = null;
   }
   /// function TBinaryData.Allocation() : IAllocation
   ,Allocation:function(Self) {
      return $AsIntf(Self,"IAllocation");
   }
   /// procedure TBinaryData.AppendBuffer(const Raw: TMemoryHandle)
   ,AppendBuffer:function(Self, Raw) {
      var LOffset = 0;
      if (Raw) {
         if (Raw.length>0) {
            LOffset = TAllocation.GetSize$3$(Self);
            TAllocation.Grow(Self,Raw.length);
            TBinaryData.WriteBuffer$2(Self,LOffset,Raw);
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Append failed, invalid source handle error");
      }
   }
   /// procedure TBinaryData.AppendBytes(const Bytes: TByteArray)
   ,AppendBytes:function(Self, Bytes$9) {
      var LLen$9 = 0,
         LOffset$1 = 0;
      LLen$9 = Bytes$9.length;
      if (LLen$9>0) {
         LOffset$1 = TAllocation.GetSize$3$(Self);
         TAllocation.Grow(Self,LLen$9);
         TAllocation.GetHandle$(Self).set(Bytes$9,LOffset$1);
      }
   }
   /// procedure TBinaryData.AppendFloat32(const Value: float32)
   ,AppendFloat32:function(Self, Value$115) {
      var LOffset$2 = 0;
      LOffset$2 = TAllocation.GetSize$3$(Self);
      TAllocation.Grow(Self,TDataTypeConverter.SizeOfType(TDataTypeConverter,8));
      TBinaryData.WriteFloat32(Self,LOffset$2,Value$115);
   }
   /// procedure TBinaryData.AppendFloat64(const Value: float64)
   ,AppendFloat64:function(Self, Value$116) {
      var LOffset$3 = 0;
      LOffset$3 = TAllocation.GetSize$3$(Self);
      TAllocation.Grow(Self,TDataTypeConverter.SizeOfType(TDataTypeConverter,9));
      TBinaryData.WriteFloat64(Self,LOffset$3,Value$116);
   }
   /// procedure TBinaryData.AppendMemory(const Buffer: TBinaryData; const ReleaseBufferOnExit: Boolean)
   ,AppendMemory:function(Self, Buffer$4, ReleaseBufferOnExit) {
      var LOffset$4 = 0;
      if (Buffer$4!==null) {
         try {
            if (TAllocation.GetSize$3$(Buffer$4)>0) {
               LOffset$4 = TAllocation.GetSize$3$(Self);
               TAllocation.Grow(Self,TAllocation.GetSize$3$(Buffer$4));
               TBinaryData.WriteBinaryData(Self,LOffset$4,Buffer$4);
            }
         } finally {
            if (ReleaseBufferOnExit) {
               TObject.Free(Buffer$4);
            }
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Append failed, Invalid source buffer error");
      }
   }
   /// procedure TBinaryData.AppendStr(const Text: String)
   ,AppendStr:function(Self, Text$30) {
      var LLen$10 = 0,
         LOffset$5 = 0,
         LTemp$7 = [],
         x$24 = 0;
      LLen$10 = Text$30.length;
      if (LLen$10>0) {
         LOffset$5 = TAllocation.GetSize$3$(Self);
         LTemp$7 = TString.EncodeUTF8(TString,Text$30);
         TAllocation.Grow(Self,LTemp$7.length);
         var $temp29;
         for(x$24=0,$temp29=LTemp$7.length;x$24<$temp29;x$24++) {
            Self.FDataView.setInt8(LOffset$5,LTemp$7[x$24]);
            ++LOffset$5;
         }
      }
   }
   /// function TBinaryData.Clone() : TBinaryData
   ,Clone:function(Self) {
      return TBinaryData.Create$42$($New(TBinaryData),TBinaryData.ToTypedArray(Self));
   }
   /// function TBinaryData.Copy(const Offset: Integer; const ByteLen: Integer) : TByteArray
   ,Copy$1:function(Self, Offset$8, ByteLen$1) {
      var Result = [];
      var LSize$1 = 0,
         LTemp$8 = null;
      LSize$1 = TAllocation.GetSize$3$(Self);
      if (LSize$1>0) {
         if (Offset$8>=0&&Offset$8<LSize$1) {
            if (Offset$8+ByteLen$1<=LSize$1) {
               LTemp$8 = new Uint8Array(Self.FDataView.buffer,Offset$8,ByteLen$1);
               Result = Array.prototype.slice.call(LTemp$8);
               LTemp$8.buffer = null;
               LTemp$8 = null;
            }
         }
      }
      return Result
   }
   /// procedure TBinaryData.CopyFrom(const Buffer: TBinaryData; const Offset: Integer; const ByteLen: Integer)
   ,CopyFrom$2:function(Self, Buffer$5, Offset$9, ByteLen$2) {
      if (Buffer$5!==null) {
         TBinaryData.CopyFromMemory(Self,TAllocation.GetHandle$(Buffer$5),Offset$9,ByteLen$2);
      } else {
         throw Exception.Create($New(EBinaryData),"CopyFrom failed, source instance was NIL error");
      }
   }
   /// procedure TBinaryData.CopyFromMemory(const Raw: TMemoryHandle; Offset: Integer; ByteLen: Integer)
   ,CopyFromMemory:function(Self, Raw$1, Offset$10, ByteLen$3) {
      if (TMemoryHandleHelper$Valid$4(Raw$1)) {
         if (TBinaryData.OffsetInRange(Self,Offset$10)) {
            if (ByteLen$3>0) {
               TMarshal.Move$1(TMarshal,Raw$1,0,TAllocation.GetHandle$(Self),Offset$10,ByteLen$3);
            }
         } else {
            throw EW3Exception.CreateFmt$($New(EBinaryData),"Cut memory failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3$(Self)-1, Offset$10]);
         }
      } else {
         throw Exception.Create($New(EBinaryData),"CopyFrom failed, invalid source handle error");
      }
   }
   /// constructor TBinaryData.Create(aHandle: TMemoryHandle)
   ,Create$42:function(Self, aHandle) {
      var LSignature;
      TDataTypeConverter.Create$15$(Self);
      if (TMemoryHandleHelper$Defined$2(aHandle)&&TMemoryHandleHelper$Valid$4(aHandle)) {
         if (aHandle.toString) {
            LSignature = aHandle.toString();
            if (SameText(String(LSignature),"[object Uint8Array]")||SameText(String(LSignature),"[object Uint8ClampedArray]")) {
               TAllocation.Allocate(Self,parseInt(aHandle.length,10));
               TMarshal.Move$1(TMarshal,aHandle,0,TAllocation.GetHandle$(Self),0,parseInt(aHandle.length,10));
            } else {
               throw Exception.Create($New(EBinaryData),"Invalid buffer type, expected handle of type Uint8[clamped]Array");
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Invalid buffer type, expected handle of type Uint8[clamped]Array");
         }
      }
      return Self
   }
   /// function TBinaryData.CutBinaryData(const Offset: Integer; const ByteLen: Integer) : TBinaryData
   ,CutBinaryData:function(Self, Offset$11, ByteLen$4) {
      var Result = null;
      var LSize$2 = 0,
         LNewBuffer = null;
      if (ByteLen$4>0) {
         LSize$2 = TAllocation.GetSize$3$(Self);
         if (LSize$2>0) {
            if (TBinaryData.OffsetInRange(Self,Offset$11)) {
               LNewBuffer = TAllocation.GetHandle$(Self).subarray(Offset$11,Offset$11+ByteLen$4-1);
               Result = TBinaryData.Create$42$($New(TBinaryData),LNewBuffer);
            } else {
               throw EW3Exception.CreateFmt$($New(EBinaryData),"Cut memory failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3$(Self)-1, Offset$11]);
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Cut memory failed, buffer is empty error");
         }
      } else {
         Result = TBinaryData.Create$42$($New(TBinaryData),null);
      }
      return Result
   }
   /// function TBinaryData.CutStream(const Offset: Integer; const ByteLen: Integer) : TStream
   ,CutStream:function(Self, Offset$12, ByteLen$5) {
      return TBinaryData.ToStream(TBinaryData.CutBinaryData(Self,Offset$12,ByteLen$5));
   }
   /// function TBinaryData.CutTypedArray(Offset: Integer; ByteLen: Integer) : TMemoryHandle
   ,CutTypedArray:function(Self, Offset$13, ByteLen$6) {
      var Result = undefined;
      var LTemp$9 = null;
      if (ByteLen$6>0) {
         if (TBinaryData.OffsetInRange(Self,Offset$13)) {
            if (TAllocation.GetSize$3$(Self)-Offset$13>0) {
               LTemp$9 = Self.FDataView.buffer.slice(Offset$13,Offset$13+ByteLen$6);
               Result = new Uint8Array(LTemp$9);
            }
         }
      }
      return Result
   }
   /// procedure TBinaryData.FromBase64(FileData: String)
   ,FromBase64:function(Self, FileData) {
      var LBytes$11 = [];
      TAllocation.Release$1(Self);
      if (FileData.length>0) {
         LBytes$11 = TBase64EncDec.Base64ToBytes$2(TBase64EncDec,FileData);
         if (LBytes$11.length>0) {
            TBinaryData.AppendBytes$(Self,LBytes$11);
         }
      }
   }
   /// procedure TBinaryData.FromNodeBuffer(const NodeBuffer: JNodeBuffer)
   ,FromNodeBuffer:function(Self, NodeBuffer) {
      var LTypedAccess = undefined;
      if (!TAllocation.a$28(Self)) {
         TAllocation.Release$1(Self);
      }
      if (NodeBuffer!==null) {
         LTypedAccess = new Uint8Array(NodeBuffer);
         TBinaryData.AppendBuffer(Self,LTypedAccess);
      }
   }
   /// function TBinaryData.GetBit(const BitIndex: Integer) : Boolean
   ,GetBit$1:function(Self, BitIndex$1) {
      var Result = false;
      var LOffset$6 = 0;
      LOffset$6 = BitIndex$1>>>3;
      if (TBinaryData.OffsetInRange(Self,LOffset$6)) {
         Result = TBitAccess.Get(TBitAccess,(BitIndex$1%8),TBinaryData.GetByte(Self,LOffset$6));
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),"Invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, BitIndex$1]);
      }
      return Result
   }
   /// function TBinaryData.GetBitCount() : Integer
   ,GetBitCount:function(Self) {
      return TAllocation.GetSize$3$(Self)<<3;
   }
   /// function TBinaryData.GetByte(const Index: Integer) : Byte
   ,GetByte:function(Self, Index$4) {
      var Result = 0;
      if (TAllocation.GetHandle$(Self)) {
         if (TBinaryData.OffsetInRange(Self,Index$4)) {
            Result = Self.FDataView.getUint8(Index$4);
         } else {
            throw EW3Exception.CreateFmt$($New(EBinaryData),"invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, Index$4]);
         }
      }
      return Result
   }
   /// procedure TBinaryData.HandleAllocated()
   ,HandleAllocated:function(Self) {
      var LRef$4 = undefined;
      LRef$4 = TAllocation.GetBufferHandle$(Self);
      (Self.FDataView) = new DataView(LRef$4);
   }
   /// procedure TBinaryData.HandleReleased()
   ,HandleReleased:function(Self) {
      Self.FDataView = null;
   }
   /// procedure TBinaryData.LoadFromStream(const Stream: TStream)
   ,LoadFromStream:function(Self, Stream$3) {
      var BytesToRead = 0;
      TAllocation.Release$1(Self);
      if (Stream$3!==null) {
         if (TStream.GetSize$(Stream$3)>0) {
            BytesToRead = TStream.GetSize$(Stream$3)-TStream.GetPosition$(Stream$3);
            if (BytesToRead>0) {
               TBinaryData.AppendBytes$(Self,TStream.ReadBuffer$(Stream$3,TStream.GetPosition$(Stream$3),BytesToRead));
            }
         }
      } else {
         throw Exception.Create($New(EBinaryData),$R[63]);
      }
   }
   /// function TBinaryData.OffsetInRange(const Offset: Integer) : Boolean
   ,OffsetInRange:function(Self, Offset$14) {
      var Result = false;
      var LSize$3 = 0;
      LSize$3 = TAllocation.GetSize$3$(Self);
      if (LSize$3>0) {
         Result = Offset$14>=0&&Offset$14<=LSize$3;
      } else {
         Result = (Offset$14==0);
      }
      return Result
   }
   /// function TBinaryData.ReadBool(Offset: Integer) : Boolean
   ,ReadBool:function(Self, Offset$15) {
      var Result = false;
      if (TBinaryData.OffsetInRange(Self,Offset$15)) {
         Result = Self.FDataView.getUint8(Offset$15)>0;
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),$R[61],[Offset$15, 0, TAllocation.GetSize$3$(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadBytes(Offset: Integer; ByteLen: Integer) : TByteArray
   ,ReadBytes:function(Self, Offset$16, ByteLen$7) {
      var Result = [];
      var LSize$4 = 0,
         x$25 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$16)) {
         LSize$4 = TAllocation.GetSize$3$(Self);
         if (Offset$16+ByteLen$7<=LSize$4) {
            var $temp30;
            for(x$25=0,$temp30=ByteLen$7;x$25<$temp30;x$25++) {
               Result.push(Self.FDataView.getUint8(Offset$16+x$25));
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),$R[61],[Offset$16, 0, TAllocation.GetSize$3$(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadFloat32(Offset: Integer) : float32
   ,ReadFloat32:function(Self, Offset$17) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$17)) {
         if (Offset$17+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)<=TAllocation.GetSize$3$(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getFloat32(Offset$17);
                  break;
               case 1 :
                  Result = Self.FDataView.getFloat32(Offset$17,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getFloat32(Offset$17,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),$R[61],[Offset$17, 0, TAllocation.GetSize$3$(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadFloat64(Offset: Integer) : float64
   ,ReadFloat64:function(Self, Offset$18) {
      var Result = undefined;
      if (TBinaryData.OffsetInRange(Self,Offset$18)) {
         if (Offset$18+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)<=TAllocation.GetSize$3$(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getFloat64(Offset$18);
                  break;
               case 1 :
                  Result = Self.FDataView.getFloat64(Offset$18,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getFloat64(Offset$18,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),$R[61],[Offset$18, 0, TAllocation.GetSize$3$(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadInt(Offset: Integer) : Integer
   ,ReadInt:function(Self, Offset$19) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$19)) {
         if (Offset$19+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)<=TAllocation.GetSize$3$(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getInt32(Offset$19);
                  break;
               case 1 :
                  Result = Self.FDataView.getInt32(Offset$19,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getInt32(Offset$19,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),$R[61],[Offset$19, 0, TAllocation.GetSize$3$(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadInt16(Offset: Integer) : SmallInt
   ,ReadInt16:function(Self, Offset$20) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$20)) {
         if (Offset$20+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)<=TAllocation.GetSize$3$(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getInt16(Offset$20);
                  break;
               case 1 :
                  Result = Self.FDataView.getInt16(Offset$20,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getInt16(Offset$20,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),$R[61],[Offset$20, 0, TAllocation.GetSize$3$(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadLong(Offset: Integer) : Longword
   ,ReadLong$1:function(Self, Offset$21) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$21)) {
         if (Offset$21+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)<=TAllocation.GetSize$3$(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getUint32(Offset$21);
                  break;
               case 1 :
                  Result = Self.FDataView.getUint32(Offset$21,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getUint32(Offset$21,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),$R[61],[Offset$21, 0, TAllocation.GetSize$3$(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadStr(Offset: Integer; ByteLen: Integer) : String
   ,ReadStr$1:function(Self, Offset$22, ByteLen$8) {
      var Result = "";
      var LSize$5 = 0,
         LFetch = [],
         x$26 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$22)) {
         LSize$5 = TAllocation.GetSize$3$(Self);
         if (Offset$22+ByteLen$8<=LSize$5) {
            var $temp31;
            for(x$26=0,$temp31=ByteLen$8;x$26<$temp31;x$26++) {
               LFetch.push(TBinaryData.GetByte(Self,(Offset$22+x$26)));
            }
            Result = TString.DecodeUTF8(TString,LFetch);
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),$R[61],[Offset$22, 0, TAllocation.GetSize$3$(Self)-1]);
      }
      return Result
   }
   /// function TBinaryData.ReadWord(Offset: Integer) : Word
   ,ReadWord$1:function(Self, Offset$23) {
      var Result = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$23)) {
         if (Offset$23+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)<=TAllocation.GetSize$3$(Self)) {
            switch (Self.FEndian) {
               case 0 :
                  Result = Self.FDataView.getUint16(Offset$23);
                  break;
               case 1 :
                  Result = Self.FDataView.getUint16(Offset$23,true);
                  break;
               case 2 :
                  Result = Self.FDataView.getUint16(Offset$23,false);
                  break;
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Read failed, data length exceeds boundaries error");
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),$R[61],[Offset$23, 0, TAllocation.GetSize$3$(Self)-1]);
      }
      return Result
   }
   /// procedure TBinaryData.SaveToStream(const Stream: TStream)
   ,SaveToStream:function(Self, Stream$4) {
      var LSize$6 = 0,
         LCache$1 = [],
         LTemp$10 = null;
      if (Stream$4!==null) {
         LSize$6 = TAllocation.GetSize$3$(Self);
         if (LSize$6>0) {
            LTemp$10 = new Uint8Array(Self.FDataView.buffer,0,LSize$6);
            LCache$1 = Array.prototype.slice.call(LTemp$10);
            LTemp$10 = null;
            TStream.WriteBuffer$(Stream$4,LCache$1,TStream.GetPosition$(Stream$4));
         }
      } else {
         throw Exception.Create($New(EBinaryData),$R[62]);
      }
   }
   /// procedure TBinaryData.SetBit(const BitIndex: Integer; const value: Boolean)
   ,SetBit$1:function(Self, BitIndex$2, value$2) {
      TBinaryData.SetByte(Self,(BitIndex$2>>>3),TBitAccess.Set$4(TBitAccess,(BitIndex$2%8),TBinaryData.GetByte(Self,(BitIndex$2>>>3)),value$2));
   }
   /// procedure TBinaryData.SetByte(const Index: Integer; const Value: Byte)
   ,SetByte:function(Self, Index$5, Value$117) {
      if (TAllocation.GetHandle$(Self)) {
         if (TBinaryData.OffsetInRange(Self,Index$5)) {
            Self.FDataView.setUint8(Index$5,Value$117);
         } else {
            throw EW3Exception.CreateFmt$($New(EBinaryData),"Invalid byte index, expected %d..%d, not %d",[0, Self.FDataView.byteLength-1, Index$5]);
         }
      }
   }
   /// function TBinaryData.ToBase64() : String
   ,ToBase64:function(Self) {
      return TDataTypeConverter.BytesToBase64(Self.ClassType,TBinaryData.ToBytes(Self));
   }
   /// function TBinaryData.ToBytes() : TByteArray
   ,ToBytes:function(Self) {
      var Result = [];
      var LSize$7 = 0,
         LTemp$11 = null;
      LSize$7 = TAllocation.GetSize$3$(Self);
      if (LSize$7>0) {
         LTemp$11 = new Uint8Array(Self.FDataView.buffer,0,LSize$7);
         Result = Array.prototype.slice.call(LTemp$11);
         LTemp$11.buffer = null;
         LTemp$11 = null;
      } else {
         Result = [];
      }
      return Result
   }
   /// function TBinaryData.ToHexDump(BytesPerRow: Integer; Options: TBufferHexDumpOptions) : String
   ,ToHexDump:function(Self, BytesPerRow, Options$6) {
      var Result = "";
      var mDump = [],
         mCount$1 = 0,
         x$27 = 0;
      var LByte$1 = 0,
         mPad = 0,
         z = 0;
      var mPad$1 = 0,
         z$1 = 0;
      function SliceToText(DataSlice) {
         var Result = "";
         var y$1 = 0;
         var LChar = "",
            LOff = 0;
         if (DataSlice.length>0) {
            var $temp32;
            for(y$1=0,$temp32=DataSlice.length;y$1<$temp32;y$1++) {
               LChar = TDataTypeConverter.ByteToChar(TDataTypeConverter,DataSlice[y$1]);
               if ((((LChar>="A")&&(LChar<="Z"))||((LChar>="a")&&(LChar<="z"))||((LChar>="0")&&(LChar<="9"))||(LChar==",")||(LChar==";")||(LChar=="<")||(LChar==">")||(LChar=="{")||(LChar=="}")||(LChar=="[")||(LChar=="]")||(LChar=="-")||(LChar=="_")||(LChar=="#")||(LChar=="$")||(LChar=="%")||(LChar=="&")||(LChar=="\/")||(LChar=="(")||(LChar==")")||(LChar=="!")||(LChar=="§")||(LChar=="^")||(LChar==":")||(LChar==",")||(LChar=="?"))) {
                  Result+=LChar;
               } else {
                  Result+="_";
               }
            }
         }
         LOff = BytesPerRow-DataSlice.length;
         while (LOff>0) {
            Result+="_";
            --LOff;
         }
         Result+="\r\n";
         return Result
      };
      if (TAllocation.GetHandle$(Self)) {
         mCount$1 = 0;
         BytesPerRow = TInteger.EnsureRange(BytesPerRow,2,64);
         var $temp33;
         for(x$27=0,$temp33=TAllocation.GetSize$3$(Self);x$27<$temp33;x$27++) {
            LByte$1 = TBinaryData.GetByte(Self,x$27);
            mDump.push(LByte$1);
            if ($SetIn(Options$6,0,0,2)) {
               Result+="$"+(IntToHex2(LByte$1)).toLocaleUpperCase();
            } else {
               Result+=(IntToHex2(LByte$1)).toLocaleUpperCase();
            }
            ++mCount$1;
            if (mCount$1>=BytesPerRow) {
               if ($SetIn(Options$6,1,0,2)&&mCount$1>0) {
                  Result+=" ";
                  mPad = BytesPerRow-mCount$1;
                  var $temp34;
                  for(z=1,$temp34=mPad;z<=$temp34;z++) {
                     if ($SetIn(Options$6,0,0,2)) {
                        Result+="$";
                     }
                     Result+="00";
                     ++mCount$1;
                     if (mCount$1>=BytesPerRow) {
                        mCount$1 = 0;
                        break;
                     } else {
                        Result+=" ";
                     }
                  }
               }
               if (mDump.length>0) {
                  Result+=SliceToText(mDump);
                  mDump.length=0;
                  mCount$1 = 0;
               }
            } else {
               Result+=" ";
            }
         }
         if (mDump.length>0) {
            if ($SetIn(Options$6,1,0,2)&&mCount$1>0) {
               mPad$1 = BytesPerRow-mCount$1;
               var $temp35;
               for(z$1=1,$temp35=mPad$1;z$1<=$temp35;z$1++) {
                  if ($SetIn(Options$6,0,0,2)) {
                     Result+="$";
                  }
                  Result+="00";
                  ++mCount$1;
                  if (mCount$1>=BytesPerRow) {
                     break;
                  } else {
                     Result+=" ";
                  }
               }
            }
            Result+=" "+SliceToText(mDump);
            mCount$1 = 0;
            mDump.length=0;
         }
      }
      return Result
   }
   /// function TBinaryData.ToNodeBuffer() : JNodeBuffer
   ,ToNodeBuffer:function(Self) {
      var Result = null;
      if (TAllocation.a$28(Self)) {
         Result = new Buffer();
      } else {
         Result = new Buffer(TBinaryData.ToTypedArray(Self));
      }
      return Result
   }
   /// function TBinaryData.ToStream() : TStream
   ,ToStream:function(Self) {
      var Result = null;
      var LSize$8 = 0,
         LCache$2 = [],
         LTemp$12 = null;
      Result = TDataTypeConverter.Create$15$($New(TMemoryStream));
      LSize$8 = TAllocation.GetSize$3$(Self);
      if (LSize$8>0) {
         LTemp$12 = new Uint8Array(Self.FDataView.buffer,0,LSize$8);
         LCache$2 = Array.prototype.slice.call(LTemp$12);
         LTemp$12 = null;
         if (LCache$2.length>0) {
            try {
               TStream.Write$2$(Result,LCache$2);
               TStream.SetPosition$(Result,0);
            } catch ($e) {
               var e$14 = $W($e);
               TObject.Free(Result);
               Result = null;
               throw $e;
            }
         }
      }
      return Result
   }
   /// function TBinaryData.ToString() : String
   ,ToString$2:function(Self) {
      var Result = "";
      var CHUNK_SIZE = 32768;
      var LRef$5 = undefined;
      if (TAllocation.GetHandle$(Self)) {
         LRef$5 = TAllocation.GetHandle$(Self);
         var c = [];
    for (var i=0; i < (LRef$5).length; i += CHUNK_SIZE) {
      c.push(String.fromCharCode.apply(null, (LRef$5).subarray(i, i + CHUNK_SIZE)));
    }
    Result = c.join("");
      }
      return Result
   }
   /// function TBinaryData.ToTypedArray() : TMemoryHandle
   ,ToTypedArray:function(Self) {
      var Result = undefined;
      var LLen$11 = 0,
         LTemp$13 = null;
      LLen$11 = TAllocation.GetSize$3$(Self);
      if (LLen$11>0) {
         LTemp$13 = Self.FDataView.buffer.slice(0,LLen$11);
         Result = new Uint8Array(LTemp$13);
      }
      return Result
   }
   /// procedure TBinaryData.WriteBinaryData(const Offset: Integer; const Data: TBinaryData)
   ,WriteBinaryData:function(Self, Offset$24, Data$42) {
      var LGrowth = 0;
      if (Data$42!==null) {
         if (TAllocation.GetSize$3$(Data$42)>0) {
            if (TBinaryData.OffsetInRange(Self,Offset$24)) {
               LGrowth = 0;
               if (Offset$24+TAllocation.GetSize$3$(Data$42)>TAllocation.GetSize$3$(Self)-1) {
                  LGrowth = Offset$24+TAllocation.GetSize$3$(Data$42)-TAllocation.GetSize$3$(Self);
               }
               if (LGrowth>0) {
                  TAllocation.Grow(Self,LGrowth);
               }
               TMarshal.Move$1(TMarshal,TAllocation.GetHandle$(Data$42),0,TAllocation.GetHandle$(Self),0,TAllocation.GetSize$3$(Data$42));
            } else {
               throw EW3Exception.CreateFmt$($New(EBinaryData),"Write string failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3$(Self)-1, Offset$24]);
            }
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Write failed, invalid source buffer [nil] error");
      }
   }
   /// procedure TBinaryData.WriteBool(const Offset: Integer; const Data: Boolean)
   ,WriteBool:function(Self, Offset$25, Data$43) {
      var LGrowth$1 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$25)) {
         LGrowth$1 = 0;
         if (Offset$25+TDataTypeConverter.SizeOfType(TDataTypeConverter,2)>TAllocation.GetSize$3$(Self)-1) {
            LGrowth$1 = Offset$25+TDataTypeConverter.SizeOfType(TDataTypeConverter,2)-TAllocation.GetSize$3$(Self);
         }
         if (LGrowth$1>0) {
            TAllocation.Grow(Self,LGrowth$1);
         }
         if (Data$43) {
            Self.FDataView.setUint8(Offset$25,1);
         } else {
            Self.FDataView.setUint8(Offset$25,0);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),"Write boolean failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3$(Self)-1, Offset$25]);
      }
   }
   /// procedure TBinaryData.WriteBuffer(const Offset: Integer; const Data: TMemoryHandle)
   ,WriteBuffer$2:function(Self, Offset$26, Data$44) {
      var LBuffer$4 = null,
         LGrowth$2 = 0;
      if (Data$44) {
         if (Data$44.buffer) {
            LBuffer$4 = Data$44.buffer;
            if (LBuffer$4.byteLength>0) {
               if (TBinaryData.OffsetInRange(Self,Offset$26)) {
                  LGrowth$2 = 0;
                  if (Offset$26+Data$44.length>TAllocation.GetSize$3$(Self)-1) {
                     LGrowth$2 = Offset$26+Data$44.length-TAllocation.GetSize$3$(Self);
                  }
                  if (LGrowth$2>0) {
                     TAllocation.Grow(Self,LGrowth$2);
                  }
                  TMarshal.Move$1(TMarshal,Data$44,0,TAllocation.GetHandle$(Self),Offset$26,parseInt(TAllocation.GetHandle$(Self).length,10));
               } else {
                  throw EW3Exception.CreateFmt$($New(EBinaryData),"Write typed-handle failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3$(Self)-1, Offset$26]);
               }
            }
         } else {
            throw Exception.Create($New(EBinaryData),"Write failed, invalid handle or unassigned buffer");
         }
      } else {
         throw Exception.Create($New(EBinaryData),"Write failed, source handle was nil error");
      }
   }
   /// procedure TBinaryData.WriteBytes(const Offset: Integer; const Data: TByteArray)
   ,WriteBytes:function(Self, Offset$27, Data$45) {
      var LGrowth$3 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$27)) {
         if (Data$45.length>0) {
            LGrowth$3 = 0;
            if (Offset$27+Data$45.length>TAllocation.GetSize$3$(Self)-1) {
               LGrowth$3 = Offset$27+Data$45.length-TAllocation.GetSize$3$(Self);
            }
            if (LGrowth$3>0) {
               TAllocation.Grow(Self,LGrowth$3);
            }
            TAllocation.GetHandle$(Self).set(Data$45,Offset$27);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),"Write bytearray failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3$(Self)-1, Offset$27]);
      }
   }
   /// procedure TBinaryData.WriteFloat32(const Offset: Integer; const Data: float32)
   ,WriteFloat32:function(Self, Offset$28, Data$46) {
      var LGrowth$4 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$28)) {
         LGrowth$4 = 0;
         if (Offset$28+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)>TAllocation.GetSize$3$(Self)-1) {
            LGrowth$4 = Offset$28+TDataTypeConverter.SizeOfType(TDataTypeConverter,8)-TAllocation.GetSize$3$(Self);
         }
         if (LGrowth$4>0) {
            TAllocation.Grow(Self,LGrowth$4);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setFloat32(Offset$28,Data$46);
               break;
            case 1 :
               Self.FDataView.setFloat32(Offset$28,Data$46,true);
               break;
            case 2 :
               Self.FDataView.setFloat32(Offset$28,Data$46,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),"Write float failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3$(Self)-1, Offset$28]);
      }
   }
   /// procedure TBinaryData.WriteFloat64(const Offset: Integer; const Data: float64)
   ,WriteFloat64:function(Self, Offset$29, Data$47) {
      var LGrowth$5 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$29)) {
         LGrowth$5 = 0;
         if (Offset$29+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)>TAllocation.GetSize$3$(Self)-1) {
            LGrowth$5 = Offset$29+TDataTypeConverter.SizeOfType(TDataTypeConverter,9)-TAllocation.GetSize$3$(Self);
         }
         if (LGrowth$5>0) {
            TAllocation.Grow(Self,LGrowth$5);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setFloat64(Offset$29,Number(Data$47));
               break;
            case 1 :
               Self.FDataView.setFloat64(Offset$29,Number(Data$47),true);
               break;
            case 2 :
               Self.FDataView.setFloat64(Offset$29,Number(Data$47),false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),"Write float failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3$(Self)-1, Offset$29]);
      }
   }
   /// procedure TBinaryData.WriteInt16(const Offset: Integer; const Data: SmallInt)
   ,WriteInt16:function(Self, Offset$30, Data$48) {
      var LGrowth$6 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$30)) {
         LGrowth$6 = 0;
         if (Offset$30+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)>TAllocation.GetSize$3$(Self)-1) {
            LGrowth$6 = Offset$30+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)-TAllocation.GetSize$3$(Self);
         }
         if (LGrowth$6>0) {
            TAllocation.Grow(Self,LGrowth$6);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setInt16(Offset$30,Data$48);
               break;
            case 1 :
               Self.FDataView.setInt16(Offset$30,Data$48,true);
               break;
            case 2 :
               Self.FDataView.setInt16(Offset$30,Data$48,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),"Write int16 failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3$(Self)-1, Offset$30]);
      }
   }
   /// procedure TBinaryData.WriteInt32(const Offset: Integer; const Data: Integer)
   ,WriteInt32:function(Self, Offset$31, Data$49) {
      var LGrowth$7 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$31)) {
         LGrowth$7 = 0;
         if (Offset$31+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)>TAllocation.GetSize$3$(Self)-1) {
            LGrowth$7 = Offset$31+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)-TAllocation.GetSize$3$(Self);
         }
         if (LGrowth$7>0) {
            TAllocation.Grow(Self,LGrowth$7);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setInt32(Offset$31,Data$49);
               break;
            case 1 :
               Self.FDataView.setInt32(Offset$31,Data$49,true);
               break;
            case 2 :
               Self.FDataView.setInt32(Offset$31,Data$49,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),"Write integer failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3$(Self)-1, Offset$31]);
      }
   }
   /// procedure TBinaryData.WriteLong(const Offset: Integer; const Data: Longword)
   ,WriteLong:function(Self, Offset$32, Data$50) {
      var LGrowth$8 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$32)) {
         LGrowth$8 = 0;
         if (Offset$32+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)>TAllocation.GetSize$3$(Self)-1) {
            LGrowth$8 = Offset$32+TDataTypeConverter.SizeOfType(TDataTypeConverter,7)-TAllocation.GetSize$3$(Self);
         }
         if (LGrowth$8>0) {
            TAllocation.Grow(Self,LGrowth$8);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setUint32(Offset$32,Data$50);
               break;
            case 1 :
               Self.FDataView.setUint32(Offset$32,Data$50,true);
               break;
            case 2 :
               Self.FDataView.setUint32(Offset$32,Data$50,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),"Write longword failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3$(Self)-1, Offset$32]);
      }
   }
   /// procedure TBinaryData.WriteStr(const Offset: Integer; const Data: String)
   ,WriteStr$1:function(Self, Offset$33, Data$51) {
      var LTemp$14 = [],
         LGrowth$9 = 0,
         x$28 = 0;
      if (Data$51.length>0) {
         if (TBinaryData.OffsetInRange(Self,Offset$33)) {
            LTemp$14 = TString.EncodeUTF8(TString,Data$51);
            LGrowth$9 = 0;
            if (Offset$33+LTemp$14.length>TAllocation.GetSize$3$(Self)-1) {
               LGrowth$9 = Offset$33+LTemp$14.length-TAllocation.GetSize$3$(Self);
            }
            if (LGrowth$9>0) {
               TAllocation.Grow(Self,LGrowth$9);
            }
            var $temp36;
            for(x$28=0,$temp36=LTemp$14.length;x$28<$temp36;x$28++) {
               Self.FDataView.setUint8(Offset$33+x$28,LTemp$14[x$28]);
            }
         } else {
            throw EW3Exception.CreateFmt$($New(EBinaryData),"Write string failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3$(Self)-1, Offset$33]);
         }
      }
   }
   /// procedure TBinaryData.WriteWord(const Offset: Integer; const Data: Word)
   ,WriteWord$1:function(Self, Offset$34, Data$52) {
      var LGrowth$10 = 0;
      if (TBinaryData.OffsetInRange(Self,Offset$34)) {
         LGrowth$10 = 0;
         if (Offset$34+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)>TAllocation.GetSize$3$(Self)-1) {
            LGrowth$10 = Offset$34+TDataTypeConverter.SizeOfType(TDataTypeConverter,6)-TAllocation.GetSize$3$(Self);
         }
         if (LGrowth$10>0) {
            TAllocation.Grow(Self,LGrowth$10);
         }
         switch (Self.FEndian) {
            case 0 :
               Self.FDataView.setUint16(Offset$34,Data$52);
               break;
            case 1 :
               Self.FDataView.setUint16(Offset$34,Data$52,true);
               break;
            case 2 :
               Self.FDataView.setUint16(Offset$34,Data$52,false);
               break;
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EBinaryData),"Write word [uint16] failed, invalid offset. Expected %d..%d not %d",[0, TAllocation.GetSize$3$(Self)-1, Offset$34]);
      }
   }
   ,Destroy:TAllocation.Destroy
   ,Create$15:TAllocation.Create$15
   ,Create$38:TAllocation.Create$38
   ,GetBufferHandle:TAllocation.GetBufferHandle
   ,GetHandle:TAllocation.GetHandle
   ,GetSize$3:TAllocation.GetSize$3
   ,GetTotalSize$1:TAllocation.GetTotalSize$1
   ,HandleAllocated$:function($){return $.ClassType.HandleAllocated($)}
   ,HandleReleased$:function($){return $.ClassType.HandleReleased($)}
   ,AppendBytes$:function($){return $.ClassType.AppendBytes.apply($.ClassType, arguments)}
   ,AppendFloat32$:function($){return $.ClassType.AppendFloat32.apply($.ClassType, arguments)}
   ,AppendFloat64$:function($){return $.ClassType.AppendFloat64.apply($.ClassType, arguments)}
   ,AppendMemory$:function($){return $.ClassType.AppendMemory.apply($.ClassType, arguments)}
   ,AppendStr$:function($){return $.ClassType.AppendStr.apply($.ClassType, arguments)}
   ,Create$42$:function($){return $.ClassType.Create$42.apply($.ClassType, arguments)}
   ,GetBitCount$:function($){return $.ClassType.GetBitCount($)}
   ,ToNodeBuffer$:function($){return $.ClassType.ToNodeBuffer($)}
};
TBinaryData.$Intf={
   IBinaryDataReadWriteAccess:[TBinaryData.ReadFloat32,TBinaryData.ReadFloat64,TBinaryData.ReadBool,TBinaryData.ReadInt,TBinaryData.ReadLong$1,TBinaryData.ReadInt16,TBinaryData.ReadWord$1,TBinaryData.ReadStr$1,TBinaryData.ReadBytes,TBinaryData.AppendBytes,TBinaryData.AppendStr,TBinaryData.AppendMemory,TBinaryData.AppendBuffer,TBinaryData.AppendFloat32,TBinaryData.AppendFloat64,TBinaryData.WriteFloat32,TBinaryData.WriteFloat64,TBinaryData.WriteStr$1,TBinaryData.WriteBytes,TBinaryData.WriteBinaryData,TBinaryData.WriteBuffer$2,TBinaryData.WriteInt32,TBinaryData.WriteLong,TBinaryData.WriteInt16,TBinaryData.WriteWord$1,TBinaryData.WriteBool,TBinaryData.Copy$1,TBinaryData.CopyFrom$2,TBinaryData.CopyFromMemory,TBinaryData.CutBinaryData,TBinaryData.CutStream,TBinaryData.CutTypedArray]
   ,IBinaryDataImport:[TBinaryData.FromBase64]
   ,IBinaryDataReadAccess:[TBinaryData.Copy$1,TBinaryData.ReadFloat32,TBinaryData.ReadFloat64,TBinaryData.ReadBool,TBinaryData.ReadInt,TBinaryData.ReadLong$1,TBinaryData.ReadInt16,TBinaryData.ReadWord$1,TBinaryData.ReadStr$1,TBinaryData.ReadBytes]
   ,IBinaryDataBitAccess:[TBinaryData.GetBitCount,TBinaryData.GetBit$1,TBinaryData.SetBit$1]
   ,IBinaryDataWriteAccess:[TBinaryData.AppendBytes,TBinaryData.AppendStr,TBinaryData.AppendMemory,TBinaryData.AppendBuffer,TBinaryData.AppendFloat32,TBinaryData.AppendFloat64,TBinaryData.WriteFloat32,TBinaryData.WriteFloat64,TBinaryData.WriteStr$1,TBinaryData.WriteBytes,TBinaryData.WriteBinaryData,TBinaryData.WriteBuffer$2,TBinaryData.WriteInt32,TBinaryData.WriteLong,TBinaryData.WriteInt16,TBinaryData.WriteWord$1,TBinaryData.WriteBool,TBinaryData.Copy$1,TBinaryData.CopyFrom$2,TBinaryData.CopyFromMemory,TBinaryData.CutBinaryData,TBinaryData.CutStream,TBinaryData.CutTypedArray]
   ,IBinaryDataExport:[TBinaryData.Copy$1,TBinaryData.ToBase64,TBinaryData.ToString$2,TBinaryData.ToTypedArray,TBinaryData.ToBytes,TBinaryData.ToHexDump,TBinaryData.ToStream,TBinaryData.Clone]
   ,IBinaryTransport:[TAllocation.DataOffset$1,TAllocation.DataGetSize$1,TAllocation.DataRead$1,TAllocation.DataWrite$1]
   ,IAllocation:[TAllocation.GetHandle,TAllocation.GetTotalSize$1,TAllocation.GetSize$3,TAllocation.GetTransport,TAllocation.Allocate,TAllocation.Grow,TAllocation.Shrink,TAllocation.ReAllocate,TAllocation.Transport,TAllocation.Release$1]
}
/// EBinaryData = class (EW3Exception)
var EBinaryData = {
   $ClassName:"EBinaryData",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TBitAccess = class (TObject)
var TBitAccess = {
   $ClassName:"TBitAccess",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TBitAccess.Assemble(const Value: TByteArray) : Byte
   ,Assemble:function(Self, Value$118) {
      var Result = 0;
      var x$29 = 0;
      if (Value$118.length==7) {
         for(x$29=7;x$29>=0;x$29--) {
            Result = TBitAccess.Set$4(TBitAccess,x$29,Result,(Value$118[x$29]==1));
         }
      }
      return Result
   }
   /// function TBitAccess.Count(const Value: Byte) : Integer
   ,Count:function(Self, Value$119) {
      return CNT_BitBuffer_ByteTable[Value$119];
   }
   /// function TBitAccess.Dismantle(const Value: Byte) : TByteArray
   ,Dismantle:function(Self, Value$120) {
      var Result = [];
      var x$30 = 0;
      for(x$30=7;x$30>=0;x$30--) {
         if (Value$120&(1<<(x$30%8))) {
            Result.push(1);
         } else {
            Result.push(0);
         }
      }
      return Result
   }
   /// function TBitAccess.FromByteString(const Value: String) : Byte
   ,FromByteString:function(Self, Value$121) {
      var Result = 0;
      var mByte = 0;
      mByte = 0;
      if (Value$121.length>=8) {
         mByte = TBitAccess.Set$4(Self,7,mByte,(Value$121.charAt(0)=="1"));
         mByte = TBitAccess.Set$4(Self,6,mByte,(Value$121.charAt(1)=="1"));
         mByte = TBitAccess.Set$4(Self,5,mByte,(Value$121.charAt(2)=="1"));
         mByte = TBitAccess.Set$4(Self,4,mByte,(Value$121.charAt(3)=="1"));
         mByte = TBitAccess.Set$4(Self,3,mByte,(Value$121.charAt(4)=="1"));
         mByte = TBitAccess.Set$4(Self,2,mByte,(Value$121.charAt(5)=="1"));
         mByte = TBitAccess.Set$4(Self,1,mByte,(Value$121.charAt(6)=="1"));
         mByte = TBitAccess.Set$4(Self,0,mByte,(Value$121.charAt(7)=="1"));
      }
      Result = mByte;
      return Result
   }
   /// function TBitAccess.Get(const index: Integer; const Value: Byte) : Boolean
   ,Get:function(Self, index$3, Value$122) {
      var Result = false;
      var mMask = 0;
      if (index$3>=0&&index$3<8) {
         mMask = 1<<index$3;
         Result = ((Value$122&mMask)!=0);
      } else {
         throw EW3Exception.CreateFmt$($New(EW3Exception),"Invalid bit index, expected 0..7 not %d",[index$3]);
      }
      return Result
   }
   /// function TBitAccess.Set(const Index: Integer; const Value: Byte; const Data: Boolean) : Byte
   ,Set$4:function(Self, Index$6, Value$123, Data$53) {
      var Result = 0;
      var mSet = false;
      var mMask$1 = 0;
      Result = Value$123;
      if (Index$6>=0&&Index$6<8) {
         mMask$1 = 1<<Index$6;
         mSet = ((Value$123&mMask$1)!=0);
         if (mSet!=Data$53) {
            if (Data$53) {
               Result = Result|mMask$1;
            } else {
               Result = (Result&(~mMask$1));
            }
         }
      }
      return Result
   }
   /// function TBitAccess.ToByteString(const Value: Byte) : String
   ,ToByteString:function(Self, Value$124) {
      var Result = "";
      var x$31 = 0;
      for(x$31=7;x$31>=0;x$31--) {
         if (TBitAccess.Get(TBitAccess,x$31,Value$124)) {
            Result+="1";
         } else {
            Result+="0";
         }
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
var CNT_BitBuffer_ByteTable = [0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8];
/// TStream = class (TDataTypeConverter)
var TStream = {
   $ClassName:"TStream",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
   }
   /// function TStream.CopyFrom(const Source: TStream; Count: Integer) : Integer
   ,CopyFrom:function(Self, Source$6, Count$17) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.DataGetSize() : Integer
   ,DataGetSize:function(Self) {
      return TStream.GetSize$(Self);
   }
   /// function TStream.DataOffset() : Integer
   ,DataOffset:function(Self) {
      return TStream.GetPosition$(Self);
   }
   /// function TStream.DataRead(const Offset: Integer; const ByteCount: Integer) : TByteArray
   ,DataRead:function(Self, Offset$35, ByteCount$1) {
      return TStream.ReadBuffer$(Self,Offset$35,ByteCount$1);
   }
   /// procedure TStream.DataWrite(const Offset: Integer; const Bytes: TByteArray)
   ,DataWrite:function(Self, Offset$36, Bytes$10) {
      TStream.WriteBuffer$(Self,Bytes$10,Offset$36);
   }
   /// function TStream.GetBOF() : Boolean
   ,GetBOF:function(Self) {
      return TStream.GetPosition$(Self)<=0;
   }
   /// function TStream.GetEOF() : Boolean
   ,GetEOF:function(Self) {
      return TStream.GetPosition$(Self)>=TStream.GetSize$(Self);
   }
   /// function TStream.GetPosition() : Integer
   ,GetPosition:function(Self) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.GetSize() : Integer
   ,GetSize:function(Self) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.Read(const Count: Integer) : TByteArray
   ,Read$1:function(Self, Count$18) {
      return TStream.ReadBuffer$(Self,TStream.GetPosition$(Self),Count$18);
   }
   /// function TStream.ReadBuffer(var Buffer: TByteArray; Offset: Integer; Count: Integer) : Integer
   ,ReadBuffer$1:function(Self, Buffer$6, Offset$37, Count$19) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.ReadBuffer(Offset: Integer; Count: Integer) : TByteArray
   ,ReadBuffer:function(Self, Offset$38, Count$20) {
      var Result = [];
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.Seek(const Offset: Integer; Origin: TStreamSeekOrigin) : Integer
   ,Seek:function(Self, Offset$39, Origin) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// procedure TStream.SetPosition(NewPosition: Integer)
   ,SetPosition:function(Self, NewPosition) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   /// procedure TStream.SetSize(NewSize: Integer)
   ,SetSize:function(Self, NewSize$2) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   /// function TStream.Skip(Amount: Integer) : Integer
   ,Skip:function(Self, Amount) {
      var Result = 0;
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
      return Result
   }
   /// function TStream.Write(const Buffer: TByteArray) : Integer
   ,Write$2:function(Self, Buffer$7) {
      var Result = 0;
      TStream.WriteBuffer$(Self,Buffer$7,TStream.GetPosition$(Self));
      Result = Buffer$7.length;
      return Result
   }
   /// procedure TStream.WriteBuffer(const Buffer: TByteArray; Offset: Integer)
   ,WriteBuffer:function(Self, Buffer$8, Offset$40) {
      throw Exception.Create($New(EStreamNotImplemented),$R[5]);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
   ,CopyFrom$:function($){return $.ClassType.CopyFrom.apply($.ClassType, arguments)}
   ,GetBOF$:function($){return $.ClassType.GetBOF($)}
   ,GetEOF$:function($){return $.ClassType.GetEOF($)}
   ,GetPosition$:function($){return $.ClassType.GetPosition($)}
   ,GetSize$:function($){return $.ClassType.GetSize($)}
   ,Read$1$:function($){return $.ClassType.Read$1.apply($.ClassType, arguments)}
   ,ReadBuffer$1$:function($){return $.ClassType.ReadBuffer$1.apply($.ClassType, arguments)}
   ,ReadBuffer$:function($){return $.ClassType.ReadBuffer.apply($.ClassType, arguments)}
   ,Seek$:function($){return $.ClassType.Seek.apply($.ClassType, arguments)}
   ,SetPosition$:function($){return $.ClassType.SetPosition.apply($.ClassType, arguments)}
   ,SetSize$:function($){return $.ClassType.SetSize.apply($.ClassType, arguments)}
   ,Skip$:function($){return $.ClassType.Skip.apply($.ClassType, arguments)}
   ,Write$2$:function($){return $.ClassType.Write$2.apply($.ClassType, arguments)}
   ,WriteBuffer$:function($){return $.ClassType.WriteBuffer.apply($.ClassType, arguments)}
};
TStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// TMemoryStream = class (TStream)
var TMemoryStream = {
   $ClassName:"TMemoryStream",$Parent:TStream
   ,$Init:function ($) {
      TStream.$Init($);
      $.FBuffer$1 = null;
      $.FPos = 0;
   }
   /// function TMemoryStream.CopyFrom(const Source: TStream; Count: Integer) : Integer
   ,CopyFrom:function(Self, Source$7, Count$21) {
      var Result = 0;
      var LData$1 = [];
      LData$1 = TStream.ReadBuffer$(Source$7,TStream.GetPosition$(Source$7),Count$21);
      TStream.WriteBuffer$(Self,LData$1,TStream.GetPosition$(Self));
      Result = LData$1.length;
      return Result
   }
   /// constructor TMemoryStream.Create()
   ,Create$15:function(Self) {
      TDataTypeConverter.Create$15(Self);
      Self.FBuffer$1 = TDataTypeConverter.Create$15$($New(TAllocation));
      return Self
   }
   /// destructor TMemoryStream.Destroy()
   ,Destroy:function(Self) {
      TObject.Free(Self.FBuffer$1);
      TDataTypeConverter.Destroy(Self);
   }
   /// function TMemoryStream.GetPosition() : Integer
   ,GetPosition:function(Self) {
      return Self.FPos;
   }
   /// function TMemoryStream.GetSize() : Integer
   ,GetSize:function(Self) {
      return TAllocation.GetSize$3$(Self.FBuffer$1);
   }
   /// function TMemoryStream.ReadBuffer(Offset: Integer; Count: Integer) : TByteArray
   ,ReadBuffer:function(Self, Offset$41, Count$22) {
      var Result = [];
      var LLen$12 = 0,
         LBytesToMove = 0,
         LTemp$15 = null;
      LLen$12 = 0;
      if (TStream.GetPosition$(Self)<TStream.GetSize$(Self)) {
         LLen$12 = TStream.GetSize$(Self)-TStream.GetPosition$(Self);
      }
      if (LLen$12>0) {
         try {
            LBytesToMove = Count$22;
            if (LBytesToMove>LLen$12) {
               LBytesToMove = LLen$12;
            }
            LTemp$15 = new Uint8Array(LBytesToMove);
            TMarshal.Move$1(TMarshal,TAllocation.GetHandle$(Self.FBuffer$1),Offset$41,LTemp$15,0,LBytesToMove);
            Result = TDataTypeConverter.TypedArrayToBytes(Self,LTemp$15);
            TStream.SetPosition$(Self,Offset$41+Result.length);
         } catch ($e) {
            var e$15 = $W($e);
            throw EW3Exception.CreateFmt$($New(EStreamReadError),$R[8],[e$15.FMessage]);
         }
      }
      return Result
   }
   /// function TMemoryStream.Seek(const Offset: Integer; Origin: TStreamSeekOrigin) : Integer
   ,Seek:function(Self, Offset$42, Origin$1) {
      var Result = 0;
      var LSize$9 = 0;
      LSize$9 = TStream.GetSize$(Self);
      if (LSize$9>0) {
         switch (Origin$1) {
            case 0 :
               if (Offset$42>-1) {
                  TStream.SetPosition$(Self,Offset$42);
                  Result = Offset$42;
               }
               break;
            case 1 :
               Result = TInteger.EnsureRange((TStream.GetPosition$(Self)+Offset$42),0,LSize$9);
               TStream.SetPosition$(Self,Result);
               break;
            case 2 :
               Result = TInteger.EnsureRange((TStream.GetSize$(Self)-Math.abs(Offset$42)),0,LSize$9);
               TStream.SetPosition$(Self,Result);
               break;
         }
      }
      return Result
   }
   /// procedure TMemoryStream.SetBuffer(NewBuffer: TAllocation)
   ,SetBuffer:function(Self, NewBuffer) {
      Self.FBuffer$1 = NewBuffer;
   }
   /// procedure TMemoryStream.SetPosition(NewPosition: Integer)
   ,SetPosition:function(Self, NewPosition$1) {
      var LSize$10 = 0;
      LSize$10 = TStream.GetSize$(Self);
      if (LSize$10>0) {
         Self.FPos = TInteger.EnsureRange(NewPosition$1,0,LSize$10);
      }
   }
   /// procedure TMemoryStream.SetSize(NewSize: Integer)
   ,SetSize:function(Self, NewSize$3) {
      var LSize$11 = 0,
         LDiff = 0,
         LDiff$1 = 0;
      LSize$11 = TStream.GetSize$(Self);
      if (NewSize$3>0) {
         if (NewSize$3==LSize$11) {
            return;
         }
         if (NewSize$3>LSize$11) {
            LDiff = NewSize$3-LSize$11;
            if (TAllocation.GetSize$3$(Self.FBuffer$1)+LDiff>0) {
               TAllocation.Grow(Self.FBuffer$1,LDiff);
            } else {
               TAllocation.Release$1(Self.FBuffer$1);
            }
         } else {
            LDiff$1 = LSize$11-NewSize$3;
            if (TAllocation.GetSize$3$(Self.FBuffer$1)-LDiff$1>0) {
               TAllocation.Shrink(Self.FBuffer$1,LDiff$1);
            } else {
               TAllocation.Release$1(Self.FBuffer$1);
            }
         }
      } else {
         TAllocation.Release$1(Self.FBuffer$1);
      }
      Self.FPos = TInteger.EnsureRange(Self.FPos,0,TStream.GetSize$(Self));
   }
   /// function TMemoryStream.Skip(Amount: Integer) : Integer
   ,Skip:function(Self, Amount$1) {
      var Result = 0;
      var LSize$12 = 0,
         LTotal$1 = 0;
      LSize$12 = TStream.GetSize$(Self);
      if (LSize$12>0) {
         LTotal$1 = TStream.GetPosition$(Self)+Amount$1;
         if (LTotal$1>LSize$12) {
            LTotal$1 = LSize$12-LTotal$1;
         }
         (Self.FPos+= LTotal$1);
         Result = LTotal$1;
      }
      return Result
   }
   /// procedure TMemoryStream.WriteBuffer(const Buffer: TByteArray; Offset: Integer)
   ,WriteBuffer:function(Self, Buffer$9, Offset$43) {
      var mData = undefined,
         mData$1 = undefined;
      try {
         if (TAllocation.a$28(Self.FBuffer$1)&&Offset$43<1) {
            TAllocation.Allocate(Self.FBuffer$1,Buffer$9.length);
            mData = TDataTypeConverter.BytesToTypedArray(Self,Buffer$9);
            TMarshal.Move$1(TMarshal,mData,0,TAllocation.GetHandle$(Self.FBuffer$1),0,Buffer$9.length);
         } else {
            if (TStream.GetPosition$(Self)==TStream.GetSize$(Self)) {
               TAllocation.Grow(Self.FBuffer$1,Buffer$9.length);
               mData$1 = TDataTypeConverter.BytesToTypedArray(Self,Buffer$9);
               TMarshal.Move$1(TMarshal,mData$1,0,TAllocation.GetHandle$(Self.FBuffer$1),Offset$43,Buffer$9.length);
            } else {
               TMarshal.Move$1(TMarshal,TDataTypeConverter.BytesToTypedArray(Self,Buffer$9),0,TAllocation.GetHandle$(Self.FBuffer$1),Offset$43,Buffer$9.length);
            }
         }
         TStream.SetPosition$(Self,Offset$43+Buffer$9.length);
      } catch ($e) {
         var e$16 = $W($e);
         throw EW3Exception.CreateFmt$($New(EStreamWriteError),$R[7],[e$16.FMessage]);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$15$:function($){return $.ClassType.Create$15($)}
   ,CopyFrom$:function($){return $.ClassType.CopyFrom.apply($.ClassType, arguments)}
   ,GetBOF:TStream.GetBOF
   ,GetEOF:TStream.GetEOF
   ,GetPosition$:function($){return $.ClassType.GetPosition($)}
   ,GetSize$:function($){return $.ClassType.GetSize($)}
   ,Read$1:TStream.Read$1
   ,ReadBuffer$1:TStream.ReadBuffer$1
   ,ReadBuffer$:function($){return $.ClassType.ReadBuffer.apply($.ClassType, arguments)}
   ,Seek$:function($){return $.ClassType.Seek.apply($.ClassType, arguments)}
   ,SetPosition$:function($){return $.ClassType.SetPosition.apply($.ClassType, arguments)}
   ,SetSize$:function($){return $.ClassType.SetSize.apply($.ClassType, arguments)}
   ,Skip$:function($){return $.ClassType.Skip.apply($.ClassType, arguments)}
   ,Write$2:TStream.Write$2
   ,WriteBuffer$:function($){return $.ClassType.WriteBuffer.apply($.ClassType, arguments)}
   ,SetBuffer$:function($){return $.ClassType.SetBuffer.apply($.ClassType, arguments)}
};
TMemoryStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// TStringStream = class (TMemoryStream)
var TStringStream = {
   $ClassName:"TStringStream",$Parent:TMemoryStream
   ,$Init:function ($) {
      TMemoryStream.$Init($);
   }
   /// function TStringStream.GetDataString() : String
   ,GetDataString:function(Self) {
      var Result = "";
      if (TStream.GetSize$(Self)>0) {
         Result = TDataTypeConverter.TypedArrayToStr(Self,TAllocation.GetHandle$(Self.FBuffer$1));
         if (Result.length>TStream.GetSize$(Self)) {
            Result = Result.substr(0,TStream.GetSize$(Self));
         }
      }
      return Result
   }
   /// procedure TStringStream.SetDataString(const Value: String)
   ,SetDataString:function(Self, Value$125) {
      TStream.SetSize$(Self,0);
      if (Value$125.length>0) {
         TStream.Write$2$(Self,TDataTypeConverter.StringToBytes(Self,Value$125));
      }
   }
   /// function TStringStream.ReadString(const Count: Integer) : String
   ,ReadString:function(Self, Count$23) {
      return TDataTypeConverter.BytesToString(Self,TStream.Read$1$(Self,Count$23));
   }
   /// function TStringStream.WriteString(const Buffer: String) : Integer
   ,WriteString$1:function(Self, Buffer$10) {
      return TStream.Write$2$(Self,TDataTypeConverter.StringToBytes(Self,Buffer$10));
   }
   ,Destroy:TMemoryStream.Destroy
   ,Create$15:TMemoryStream.Create$15
   ,CopyFrom:TMemoryStream.CopyFrom
   ,GetBOF:TStream.GetBOF
   ,GetEOF:TStream.GetEOF
   ,GetPosition:TMemoryStream.GetPosition
   ,GetSize:TMemoryStream.GetSize
   ,Read$1:TStream.Read$1
   ,ReadBuffer$1:TStream.ReadBuffer$1
   ,ReadBuffer:TMemoryStream.ReadBuffer
   ,Seek:TMemoryStream.Seek
   ,SetPosition:TMemoryStream.SetPosition
   ,SetSize:TMemoryStream.SetSize
   ,Skip:TMemoryStream.Skip
   ,Write$2:TStream.Write$2
   ,WriteBuffer:TMemoryStream.WriteBuffer
   ,SetBuffer:TMemoryStream.SetBuffer
};
TStringStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// TStreamSeekOrigin enumeration
var TStreamSeekOrigin = [ "soFromBeginning", "soFromCurrent", "soFromEnd" ];
/// TCustomFileStream = class (TStream)
var TCustomFileStream = {
   $ClassName:"TCustomFileStream",$Parent:TStream
   ,$Init:function ($) {
      TStream.$Init($);
      $.FAccess$1 = 0;
      $.FFilename = "";
   }
   /// procedure TCustomFileStream.SetAccessMode(const Value: TFileAccessMode)
   ,SetAccessMode:function(Self, Value$126) {
      Self.FAccess$1 = Value$126;
   }
   /// procedure TCustomFileStream.SetFilename(const Value: String)
   ,SetFilename:function(Self, Value$127) {
      Self.FFilename = Value$127;
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
   ,CopyFrom:TStream.CopyFrom
   ,GetBOF:TStream.GetBOF
   ,GetEOF:TStream.GetEOF
   ,GetPosition:TStream.GetPosition
   ,GetSize:TStream.GetSize
   ,Read$1:TStream.Read$1
   ,ReadBuffer$1:TStream.ReadBuffer$1
   ,ReadBuffer:TStream.ReadBuffer
   ,Seek:TStream.Seek
   ,SetPosition:TStream.SetPosition
   ,SetSize:TStream.SetSize
   ,Skip:TStream.Skip
   ,Write$2:TStream.Write$2
   ,WriteBuffer:TStream.WriteBuffer
   ,Create$32$:function($){return $.ClassType.Create$32.apply($.ClassType, arguments)}
   ,SetAccessMode$:function($){return $.ClassType.SetAccessMode.apply($.ClassType, arguments)}
   ,SetFilename$:function($){return $.ClassType.SetFilename.apply($.ClassType, arguments)}
};
TCustomFileStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
/// EStream = class (EW3Exception)
var EStream = {
   $ClassName:"EStream",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EStreamWriteError = class (EStream)
var EStreamWriteError = {
   $ClassName:"EStreamWriteError",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EStreamReadError = class (EStream)
var EStreamReadError = {
   $ClassName:"EStreamReadError",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EStreamNotImplemented = class (EStream)
var EStreamNotImplemented = {
   $ClassName:"EStreamNotImplemented",$Parent:EStream
   ,$Init:function ($) {
      EStream.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TAllocationOptions = class (TW3OwnedObject)
var TAllocationOptions = {
   $ClassName:"TAllocationOptions",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.FCacheSize = 0;
      $.FUseCache = false;
   }
   /// anonymous TSourceMethodSymbol
   ,a$27:function(Self) {
      return $As(TW3OwnedObject.GetOwner$(Self),TAllocation);
   }
   /// function TAllocationOptions.AcceptOwner(const CandidateObject: TObject) : Boolean
   ,AcceptOwner:function(Self, CandidateObject$1) {
      return CandidateObject$1!==null&&$Is(CandidateObject$1,TAllocation);
   }
   /// constructor TAllocationOptions.Create(const AOwner: TAllocation)
   ,Create$36:function(Self, AOwner$2) {
      TW3OwnedObject.Create$16(Self,AOwner$2);
      Self.FCacheSize = 4096;
      Self.FUseCache = true;
      return Self
   }
   /// function TAllocationOptions.GetCacheFree() : Integer
   ,GetCacheFree:function(Self) {
      return Self.FCacheSize-TAllocationOptions.GetCacheUsed$(Self);
   }
   /// function TAllocationOptions.GetCacheUsed() : Integer
   ,GetCacheUsed:function(Self) {
      var Result = 0;
      if (Self.FUseCache) {
         Result = parseInt((Self.FCacheSize-(TAllocation.GetHandle$(TAllocationOptions.a$27(Self)).length-TAllocation.GetSize$3$(TAllocationOptions.a$27(Self)))),10);
      }
      return Result
   }
   /// procedure TAllocationOptions.SetCacheSize(const NewCacheSize: Integer)
   ,SetCacheSize:function(Self, NewCacheSize) {
      Self.FCacheSize = (NewCacheSize<1024)?1024:(NewCacheSize>1024000)?1024000:NewCacheSize;
   }
   /// procedure TAllocationOptions.SetUseCache(const NewUseValue: Boolean)
   ,SetUseCache:function(Self, NewUseValue) {
      Self.FUseCache = NewUseValue;
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedObject.Create$16
   ,GetCacheFree$:function($){return $.ClassType.GetCacheFree($)}
   ,GetCacheUsed$:function($){return $.ClassType.GetCacheUsed($)}
   ,SetCacheSize$:function($){return $.ClassType.SetCacheSize.apply($.ClassType, arguments)}
   ,SetUseCache$:function($){return $.ClassType.SetUseCache.apply($.ClassType, arguments)}
};
TAllocationOptions.$Intf={
   IW3OwnedObjectAccess:[TAllocationOptions.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
function a$460(Self) {
   return ((!Self[0]())?true:false);
}/// EAllocation = class (EW3Exception)
var EAllocation = {
   $ClassName:"EAllocation",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TW3CustomReader = class (TDataTypeConverter)
var TW3CustomReader = {
   $ClassName:"TW3CustomReader",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FAccess$2 = null;
      $.FBookmarks = [];
      $.FOffset$2 = $.FTotalSize$1 = 0;
      $.FOptions$3 = [0];
   }
   /// anonymous TSourceMethodSymbol
   ,a$32:function(Self) {
      return TW3CustomReader.GetReadOffset$(Self)>=TW3CustomReader.GetTotalSize$2$(Self);
   }
   /// anonymous TSourceMethodSymbol
   ,a$31:function(Self) {
      return TW3CustomReader.GetTotalSize$2$(Self)>0&&TW3CustomReader.GetReadOffset$(Self)<TW3CustomReader.GetTotalSize$2$(Self);
   }
   /// procedure TW3CustomReader.Bookmark()
   ,Bookmark:function(Self) {
      if ($SetIn(Self.FOptions$3,0,0,1)) {
         Self.FBookmarks.push(TW3CustomReader.GetReadOffset$(Self));
      } else {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.Bookmark",Self,"Bookmarks not supported by data-provider");
      }
   }
   /// constructor TW3CustomReader.Create(const Access: IBinaryTransport)
   ,Create$39:function(Self, Access$2) {
      TDataTypeConverter.Create$15(Self);
      Self.FOptions$3 = [1];
      Self.FAccess$2 = Access$2;
      Self.FOffset$2 = Self.FAccess$2[0]();
      Self.FTotalSize$1 = Self.FAccess$2[1]();
      return Self
   }
   /// function TW3CustomReader.GetReadOffset() : Integer
   ,GetReadOffset:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions$3,0,0,1)) {
         Result = Self.FOffset$2;
      } else {
         Result = Self.FAccess$2[0]();
      }
      return Result
   }
   /// function TW3CustomReader.GetTotalSize() : Integer
   ,GetTotalSize$2:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions$3,0,0,1)) {
         Result = Self.FTotalSize$1;
      } else {
         Result = Self.FAccess$2[1]();
      }
      return Result
   }
   /// function TW3CustomReader.QueryBreachOfBoundary(const NumberOfBytes: Integer) : Boolean
   ,QueryBreachOfBoundary$1:function(Self, NumberOfBytes$3) {
      return TW3CustomReader.GetTotalSize$2$(Self)-TW3CustomReader.GetReadOffset$(Self)<NumberOfBytes$3;
   }
   /// function TW3CustomReader.Read(const BytesToRead: Integer) : TByteArray
   ,Read$2:function(Self, BytesToRead$1) {
      var Result = [];
      if (BytesToRead$1>0) {
         Result = Self.FAccess$2[2](TW3CustomReader.GetReadOffset$(Self),BytesToRead$1);
         if ($SetIn(Self.FOptions$3,0,0,1)) {
            (Self.FOffset$2+= Result.length);
         }
      } else {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.Read",Self,("Invalid read length ("+BytesToRead$1.toString()+")"));
      }
      return Result
   }
   /// function TW3CustomReader.ReadBoolean() : Boolean
   ,ReadBoolean:function(Self) {
      var Result = false;
      var LBytesToRead = 0;
      LBytesToRead = TDataTypeConverter.SizeOfType(TDataTypeConverter,1);
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,LBytesToRead)) {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.ReadBoolean",Self,Format($R[10],[LBytesToRead]));
      } else {
         Result = TDataTypeConverter.BytesToBoolean(Self,TW3CustomReader.Read$2(Self,LBytesToRead));
      }
      return Result
   }
   /// function TW3CustomReader.ReadByte() : Byte
   ,ReadByte:function(Self) {
      var Result = 0;
      var LBytesToRead$1 = 0,
         Data$54 = [];
      LBytesToRead$1 = TDataTypeConverter.SizeOfType(TDataTypeConverter,2);
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,LBytesToRead$1)) {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.ReadByte",Self,Format($R[10],[LBytesToRead$1]));
      } else {
         Data$54 = TW3CustomReader.Read$2(Self,LBytesToRead$1);
         Result = Data$54[0];
      }
      return Result
   }
   /// function TW3CustomReader.ReadChar() : Char
   ,ReadChar:function(Self) {
      var Result = "";
      var LBytesToRead$2 = 0;
      LBytesToRead$2 = TDataTypeConverter.SizeOfType(TDataTypeConverter,3);
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,LBytesToRead$2)) {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.ReadChar",Self,Format($R[10],[LBytesToRead$2]));
      } else {
         Result = TString.DecodeUTF8(TString,TW3CustomReader.Read$2(Self,LBytesToRead$2));
      }
      return Result
   }
   /// function TW3CustomReader.ReadDate() : TDate
   ,ReadDate:function(Self) {
      var Result = 0;
      var LBytesToRead$3 = 0;
      LBytesToRead$3 = TDataTypeConverter.SizeOfType(TDataTypeConverter,8);
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,LBytesToRead$3)) {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.ReadDate",Self,Format($R[10],[LBytesToRead$3]));
      } else {
         Result = TDataTypeConverter.BytesToFloat32(Self,TW3CustomReader.Read$2(Self,LBytesToRead$3));
      }
      return Result
   }
   /// function TW3CustomReader.ReadDateTime() : TDateTime
   ,ReadDateTime:function(Self) {
      var Result = undefined;
      var LBytesToRead$4 = 0;
      LBytesToRead$4 = TDataTypeConverter.SizeOfType(TDataTypeConverter,9);
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,LBytesToRead$4)) {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.ReadDateTime",Self,Format($R[10],[LBytesToRead$4]));
      } else {
         Result = TDataTypeConverter.BytesToFloat64(Self,TW3CustomReader.Read$2(Self,LBytesToRead$4));
      }
      return Result
   }
   /// function TW3CustomReader.ReadDouble() : double
   ,ReadDouble:function(Self) {
      var Result = undefined;
      var LBytesToRead$5 = 0;
      LBytesToRead$5 = TDataTypeConverter.SizeOfType(TDataTypeConverter,9);
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,LBytesToRead$5)) {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.ReadDouble",Self,Format($R[10],[LBytesToRead$5]));
      } else {
         Result = TDataTypeConverter.BytesToFloat64(Self,TW3CustomReader.Read$2(Self,LBytesToRead$5));
      }
      return Result
   }
   /// function TW3CustomReader.ReadInteger() : Integer
   ,ReadInteger:function(Self) {
      var Result = 0;
      var LBytesToRead$6 = 0;
      LBytesToRead$6 = TDataTypeConverter.SizeOfType(TDataTypeConverter,7);
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,LBytesToRead$6)) {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.ReadInteger",Self,Format($R[10],[LBytesToRead$6]));
      } else {
         Result = TDataTypeConverter.BytesToInt32(Self,TW3CustomReader.Read$2(Self,LBytesToRead$6));
      }
      return Result
   }
   /// function TW3CustomReader.ReadLong() : Longword
   ,ReadLong:function(Self) {
      var Result = 0;
      var LBytesToRead$7 = 0;
      LBytesToRead$7 = TDataTypeConverter.SizeOfType(TDataTypeConverter,7);
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,LBytesToRead$7)) {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.ReadLong",Self,Format($R[10],[LBytesToRead$7]));
      } else {
         Result = TDataTypeConverter.BytesToUInt32(Self,TW3CustomReader.Read$2(Self,LBytesToRead$7));
      }
      return Result
   }
   /// function TW3CustomReader.ReadSingle() : Float
   ,ReadSingle:function(Self) {
      var Result = 0;
      var LBytesToRead$8 = 0;
      LBytesToRead$8 = TDataTypeConverter.SizeOfType(TDataTypeConverter,8);
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,LBytesToRead$8)) {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.ReadSingle",Self,Format($R[10],[LBytesToRead$8]));
      } else {
         Result = TDataTypeConverter.BytesToFloat32(Self,TW3CustomReader.Read$2(Self,LBytesToRead$8));
      }
      return Result
   }
   /// function TW3CustomReader.ReadSmallInt() : SmallInt
   ,ReadSmallInt:function(Self) {
      var Result = 0;
      var LBytesToRead$9 = 0;
      LBytesToRead$9 = TDataTypeConverter.SizeOfType(TDataTypeConverter,6);
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,LBytesToRead$9)) {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.ReadSmallInt",Self,Format($R[10],[LBytesToRead$9]));
      } else {
         Result = TDataTypeConverter.BytesToInt16(Self,TW3CustomReader.Read$2(Self,LBytesToRead$9));
      }
      return Result
   }
   /// function TW3CustomReader.ReadStr(const BytesToRead: Integer) : String
   ,ReadStr:function(Self, BytesToRead$2) {
      var Result = "";
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,BytesToRead$2)) {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.ReadStr",Self,Format($R[10],[BytesToRead$2]));
      } else if (BytesToRead$2>0) {
         Result = TDataTypeConverter.BytesToString(Self,TW3CustomReader.Read$2(Self,BytesToRead$2));
      }
      return Result
   }
   /// function TW3CustomReader.ReadString() : String
   ,ReadString$1:function(Self) {
      var Result = "";
      var LSignature$1 = 0,
         LLength$1 = 0;
      LSignature$1 = TW3CustomReader.ReadInteger(Self);
      if (LSignature$1==3131756270) {
         LLength$1 = TW3CustomReader.ReadInteger(Self);
         if (LLength$1>0) {
            Result = TW3CustomReader.ReadStr(Self,LLength$1);
         }
      } else {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.ReadString",Self,Format($R[11],["string"]));
      }
      return Result
   }
   /// function TW3CustomReader.ReadTime() : TTime
   ,ReadTime:function(Self) {
      var Result = 0;
      var LBytesToRead$10 = 0;
      LBytesToRead$10 = TDataTypeConverter.SizeOfType(TDataTypeConverter,8);
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,LBytesToRead$10)) {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.ReadTime",Self,Format($R[10],[LBytesToRead$10]));
      } else {
         Result = TDataTypeConverter.BytesToFloat32(Self,TW3CustomReader.Read$2(Self,LBytesToRead$10));
      }
      return Result
   }
   /// function TW3CustomReader.ReadVariant() : Variant
   ,ReadVariant:function(Self) {
      var Result = undefined;
      var LKind = 1,
         ByteLen$9 = 0;
      LKind = TW3CustomReader.ReadInteger(Self);
      if (LKind==1) {
         Result = null;
      } else {
         ByteLen$9 = TW3CustomReader.ReadInteger(Self);
         if (ByteLen$9>0) {
            Result = TDataTypeConverter.BytesToVariant(Self,TW3CustomReader.Read$2(Self,ByteLen$9));
         }
      }
      return Result
   }
   /// function TW3CustomReader.ReadWord() : Word
   ,ReadWord:function(Self) {
      var Result = 0;
      var LBytesToRead$11 = 0;
      LBytesToRead$11 = TDataTypeConverter.SizeOfType(TDataTypeConverter,6);
      if (TW3CustomReader.QueryBreachOfBoundary$1(Self,LBytesToRead$11)) {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.ReadWord",Self,Format($R[10],[LBytesToRead$11]));
      } else {
         Result = TDataTypeConverter.BytesToUInt16(Self,TW3CustomReader.Read$2(Self,LBytesToRead$11));
      }
      return Result
   }
   /// procedure TW3CustomReader.SetUpdateOption(const NewUpdateMode: TW3ReaderProviderOptions)
   ,SetUpdateOption:function(Self, NewUpdateMode) {
      Self.FOptions$3 = NewUpdateMode.slice(0);
   }
   /// procedure TW3CustomReader.UnBookmark()
   ,UnBookmark:function(Self) {
      if ($SetIn(Self.FOptions$3,0,0,1)) {
         if (Self.FBookmarks.length>0) {
            Self.FOffset$2 = Self.FBookmarks.pop();
         } else {
            throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.UnBookmark",Self,"Bookmark stack is empty error");
         }
      } else {
         throw EW3Exception.Create$27$($New(EW3ReadError),"TW3CustomReader.UnBookmark",Self,"Bookmarks not supported by data-provider");
      }
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
   ,Create$39$:function($){return $.ClassType.Create$39.apply($.ClassType, arguments)}
   ,GetReadOffset$:function($){return $.ClassType.GetReadOffset($)}
   ,GetTotalSize$2$:function($){return $.ClassType.GetTotalSize$2($)}
};
/// TReader = class (TW3CustomReader)
var TReader = {
   $ClassName:"TReader",$Parent:TW3CustomReader
   ,$Init:function ($) {
      TW3CustomReader.$Init($);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
   ,Create$39:TW3CustomReader.Create$39
   ,GetReadOffset:TW3CustomReader.GetReadOffset
   ,GetTotalSize$2:TW3CustomReader.GetTotalSize$2
};
/// TStreamReader = class (TReader)
var TStreamReader = {
   $ClassName:"TStreamReader",$Parent:TReader
   ,$Init:function ($) {
      TReader.$Init($);
   }
   /// constructor TStreamReader.Create(const Stream: TStream)
   ,Create$40:function(Self, Stream$5) {
      TW3CustomReader.Create$39(Self,$AsIntf(Stream$5,"IBinaryTransport"));
      if (!$SetIn(Self.FOptions$3,0,0,1)) {
         TW3CustomReader.SetUpdateOption(Self,[1]);
      }
      return Self
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
   ,Create$39:TW3CustomReader.Create$39
   ,GetReadOffset:TW3CustomReader.GetReadOffset
   ,GetTotalSize$2:TW3CustomReader.GetTotalSize$2
   ,Create$40$:function($){return $.ClassType.Create$40.apply($.ClassType, arguments)}
};
/// EW3ReadError = class (EW3Exception)
var EW3ReadError = {
   $ClassName:"EW3ReadError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TW3CustomWriter = class (TDataTypeConverter)
var TW3CustomWriter = {
   $ClassName:"TW3CustomWriter",$Parent:TDataTypeConverter
   ,$Init:function ($) {
      TDataTypeConverter.$Init($);
      $.FAccess = null;
      $.FOffset = $.FTotalSize = 0;
      $.FOptions$1 = [0];
   }
   /// anonymous TSourceMethodSymbol
   ,a$25:function(Self) {
      return TW3CustomWriter.GetOffset(Self)>=TW3CustomWriter.GetTotalSize(Self);
   }
   /// anonymous TSourceMethodSymbol
   ,a$24:function(Self) {
      return TW3CustomWriter.GetTotalSize(Self)>0&&TW3CustomWriter.GetOffset(Self)<TW3CustomWriter.GetTotalSize(Self);
   }
   /// constructor TW3CustomWriter.Create(const Access: IBinaryTransport)
   ,Create$29:function(Self, Access$3) {
      TDataTypeConverter.Create$15(Self);
      Self.FAccess = Access$3;
      Self.FOffset = Self.FAccess[0]();
      Self.FTotalSize = Self.FAccess[1]();
      Self.FOptions$1 = [3];
      return Self
   }
   /// function TW3CustomWriter.GetOffset() : Integer
   ,GetOffset:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions$1,0,0,2)) {
         Result = Self.FOffset;
      } else {
         Result = Self.FAccess[0]();
      }
      return Result
   }
   /// function TW3CustomWriter.GetTotalFree() : Integer
   ,GetTotalFree:function(Self) {
      return Self.FAccess[1]()-TW3CustomWriter.GetOffset(Self);
   }
   /// function TW3CustomWriter.GetTotalSize() : Integer
   ,GetTotalSize:function(Self) {
      var Result = 0;
      if ($SetIn(Self.FOptions$1,0,0,2)) {
         Result = 2147483647;
      } else {
         Result = Self.FAccess[1]();
      }
      return Result
   }
   /// function TW3CustomWriter.QueryBreachOfBoundary(const BytesToFit: Integer) : Boolean
   ,QueryBreachOfBoundary:function(Self, BytesToFit) {
      var Result = false;
      if (BytesToFit>=1) {
         if ($SetIn(Self.FOptions$1,1,0,2)) {
            Result = false;
         } else {
            Result = TW3CustomWriter.GetTotalFree(Self)<BytesToFit;
         }
      }
      return Result
   }
   /// procedure TW3CustomWriter.SetAccessOptions(const NewOptions: TW3WriterOptions)
   ,SetAccessOptions:function(Self, NewOptions) {
      Self.FOptions$1 = NewOptions.slice(0);
   }
   /// function TW3CustomWriter.Write(const Data: TByteArray) : Integer
   ,Write$1:function(Self, Data$55) {
      var Result = 0;
      var LBytesToWrite = 0,
         LBytesLeft = 0,
         LBytesMissing = 0;
      LBytesToWrite = Data$55.length;
      if (LBytesToWrite>0) {
         if ($SetIn(Self.FOptions$1,1,0,2)) {
            Self.FAccess[3](TW3CustomWriter.GetOffset(Self),Data$55);
            if ($SetIn(Self.FOptions$1,0,0,2)) {
               (Self.FOffset+= Data$55.length);
            }
         } else {
            if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite)) {
               LBytesLeft = TW3CustomWriter.GetTotalSize(Self)-TW3CustomWriter.GetOffset(Self);
               LBytesMissing = Math.abs(LBytesLeft-LBytesToWrite);
               (LBytesToWrite-= LBytesMissing);
               $ArraySetLenC(Data$55,LBytesToWrite,function (){return 0});
            }
            if (LBytesToWrite>1) {
               Self.FAccess[3](TW3CustomWriter.GetOffset(Self),Data$55);
               if ($SetIn(Self.FOptions$1,0,0,2)) {
                  (Self.FOffset+= Data$55.length);
               }
            } else {
               throw EW3Exception.Create$27$($New(EW3WriteError),"TW3CustomWriter.Write",Self,Format($R[12],[Data$55.length]));
            }
         }
         Result = Data$55.length;
      } else {
         throw EW3Exception.Create$27$($New(EW3WriteError),"TW3CustomWriter.Write",Self,Format($R[14],[LBytesToWrite]));
      }
      return Result
   }
   /// procedure TW3CustomWriter.WriteBoolean(const Value: Boolean)
   ,WriteBoolean:function(Self, Value$128) {
      var LBytesToWrite$1 = 0;
      LBytesToWrite$1 = TDataTypeConverter.SizeOfType(TDataTypeConverter,1);
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite$1)) {
         throw EW3Exception.Create$27$($New(EW3WriteError),"TW3CustomWriter.WriteBoolean",Self,Format($R[12],[LBytesToWrite$1]));
      } else {
         TW3CustomWriter.Write$1(Self,TDataTypeConverter.BooleanToBytes(Self.ClassType,Value$128));
      }
   }
   /// procedure TW3CustomWriter.WriteByte(const Value: Byte)
   ,WriteByte:function(Self, Value$129) {
      var LValue = 0,
         LBytesToWrite$2 = 0;
      LValue = TInteger.EnsureRange(Value$129,0,255);
      LBytesToWrite$2 = 1;
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite$2)) {
         throw EW3Exception.Create$27$($New(EW3WriteError),"TW3CustomWriter.WriteByte",Self,Format($R[12],[LBytesToWrite$2]));
      } else {
         TW3CustomWriter.Write$1(Self,([LValue].slice()));
      }
   }
   /// procedure TW3CustomWriter.WriteChar(const Value: Char)
   ,WriteChar:function(Self, Value$130) {
      var LBytesToWrite$3 = 0;
      LBytesToWrite$3 = TDataTypeConverter.SizeOfType(TDataTypeConverter,3);
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite$3)) {
         throw EW3Exception.Create$27$($New(EW3WriteError),"TW3CustomWriter.WriteChar",Self,Format($R[12],[LBytesToWrite$3]));
      } else {
         TW3CustomWriter.Write$1(Self,TString.EncodeUTF8(TString,Value$130.charAt(0)));
      }
   }
   /// procedure TW3CustomWriter.WriteDate(const Value: TDate)
   ,WriteDate:function(Self, Value$131) {
      var LBytesToWrite$4 = 0;
      LBytesToWrite$4 = TDataTypeConverter.SizeOfType(TDataTypeConverter,8);
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite$4)) {
         throw EW3Exception.Create$27$($New(EW3WriteError),"TW3CustomWriter.WriteDate",Self,Format($R[12],[LBytesToWrite$4]));
      } else {
         TW3CustomWriter.Write$1(Self,TDataTypeConverter.Float32ToBytes(Self,Value$131));
      }
   }
   /// procedure TW3CustomWriter.WriteDateTime(const Value: TDateTime)
   ,WriteDateTime:function(Self, Value$132) {
      var LBytesToWrite$5 = 0;
      LBytesToWrite$5 = TDataTypeConverter.SizeOfType(TDataTypeConverter,9);
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite$5)) {
         throw EW3Exception.Create$27$($New(EW3WriteError),"TW3CustomWriter.WriteDateTime",Self,Format($R[12],[LBytesToWrite$5]));
      } else {
         TW3CustomWriter.Write$1(Self,TDataTypeConverter.Float64ToBytes(Self,Value$132));
      }
   }
   /// procedure TW3CustomWriter.WriteDouble(const Value: double)
   ,WriteDouble:function(Self, Value$133) {
      var LBytesToWrite$6 = 0;
      LBytesToWrite$6 = TDataTypeConverter.SizeOfType(TDataTypeConverter,9);
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite$6)) {
         throw EW3Exception.Create$27$($New(EW3WriteError),"TW3CustomWriter.WriteDouble",Self,Format($R[12],[LBytesToWrite$6]));
      } else {
         TW3CustomWriter.Write$1(Self,TDataTypeConverter.Float64ToBytes(Self,Value$133));
      }
   }
   /// procedure TW3CustomWriter.WriteInteger(const Value: Integer)
   ,WriteInteger:function(Self, Value$134) {
      var LBytesToWrite$7 = 0;
      LBytesToWrite$7 = TDataTypeConverter.SizeOfType(TDataTypeConverter,7);
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite$7)) {
         throw EW3Exception.Create$27$($New(EW3WriteError),"TW3CustomWriter.WriteInteger",Self,Format($R[12],[LBytesToWrite$7]));
      } else {
         TW3CustomWriter.Write$1(Self,TDataTypeConverter.Int32ToBytes(Self,Value$134));
      }
   }
   /// procedure TW3CustomWriter.WriteSingle(const Value: Float)
   ,WriteSingle:function(Self, Value$135) {
      var LBytesToWrite$8 = 0;
      LBytesToWrite$8 = TDataTypeConverter.SizeOfType(TDataTypeConverter,8);
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite$8)) {
         throw EW3Exception.Create$27$($New(EW3WriteError),"TW3CustomWriter.WriteSingle",Self,Format($R[12],[LBytesToWrite$8]));
      } else {
         TW3CustomWriter.Write$1(Self,TDataTypeConverter.Float32ToBytes(Self,Value$135));
      }
   }
   /// procedure TW3CustomWriter.WriteStr(const Value: String)
   ,WriteStr:function(Self, Value$136) {
      var LBytes$12 = [],
         LBytesToWrite$9 = 0;
      LBytes$12 = TDataTypeConverter.StringToBytes(Self,Value$136);
      LBytesToWrite$9 = LBytes$12.length;
      if (LBytesToWrite$9>0) {
         if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite$9)) {
            throw EW3Exception.Create$27$($New(EW3WriteError),"TW3CustomWriter.WriteStr",Self,Format($R[12],[LBytesToWrite$9]));
         } else {
            try {
               TW3CustomWriter.Write$1(Self,LBytes$12);
            } catch ($e) {
               var e$17 = $W($e);
               throw EW3Exception.Create$27$($New(EW3WriteError),"TW3CustomWriter.WriteStr",Self,e$17.FMessage);
            }
         }
      }
   }
   /// procedure TW3CustomWriter.WriteString(const Value: String)
   ,WriteString:function(Self, Value$137) {
      var LTotal$2 = 0,
         LBytes$13 = [];
      LTotal$2 = TDataTypeConverter.SizeOfType(TDataTypeConverter,7);
      (LTotal$2+= TDataTypeConverter.SizeOfType(TDataTypeConverter,7));
      LBytes$13 = TDataTypeConverter.StringToBytes(Self,Value$137);
      (LTotal$2+= LBytes$13.length);
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LTotal$2)) {
         throw EW3Exception.Create$27$($New(EW3WriteError),"TW3CustomWriter.WriteString",Self,Format($R[12],[LTotal$2]));
      } else {
         try {
            TW3CustomWriter.WriteInteger(Self,3131756270);
            TW3CustomWriter.WriteInteger(Self,LBytes$13.length);
            if (LBytes$13.length>0) {
               TW3CustomWriter.Write$1(Self,LBytes$13);
            }
         } catch ($e) {
            var e$18 = $W($e);
            throw EW3Exception.Create$27$($New(EW3WriteError),"TW3CustomWriter.WriteString",Self,e$18.FMessage);
         }
      }
   }
   /// procedure TW3CustomWriter.WriteTime(const Value: TTime)
   ,WriteTime:function(Self, Value$138) {
      var LBytesToWrite$10 = 0;
      LBytesToWrite$10 = TDataTypeConverter.SizeOfType(TDataTypeConverter,8);
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite$10)) {
         throw EW3Exception.Create$27$($New(EW3WriteError),"TW3CustomWriter.WriteTime",Self,Format($R[12],[LBytesToWrite$10]));
      } else {
         TW3CustomWriter.Write$1(Self,TDataTypeConverter.Float32ToBytes(Self,Value$138));
      }
   }
   /// procedure TW3CustomWriter.WriteVariant(const Value: Variant)
   ,WriteVariant:function(Self, Value$139) {
      var LDataType = 1,
         LBytes$14 = [];
      LDataType = TW3VariantHelper$DataType$1(Value$139);
      if (LDataType==1) {
         TW3CustomWriter.WriteInteger(Self,1);
      } else {
         LBytes$14 = TDataTypeConverter.VariantToBytes(Self,Value$139);
         TW3CustomWriter.WriteInteger(Self,LDataType);
         TW3CustomWriter.WriteInteger(Self,LBytes$14.length);
         TW3CustomWriter.Write$1(Self,LBytes$14);
      }
   }
   /// procedure TW3CustomWriter.WriteWord(const Value: Word)
   ,WriteWord:function(Self, Value$140) {
      var LValue$1 = 0,
         LBytesToWrite$11 = 0;
      LValue$1 = TInteger.EnsureRange(Value$140,0,65536);
      LBytesToWrite$11 = TDataTypeConverter.SizeOfType(TDataTypeConverter,6);
      if (TW3CustomWriter.QueryBreachOfBoundary(Self,LBytesToWrite$11)) {
         throw EW3Exception.Create$27$($New(EW3WriteError),"TW3CustomWriter.WriteWord",Self,Format($R[12],[LBytesToWrite$11]));
      } else {
         TW3CustomWriter.Write$1(Self,TDataTypeConverter.Int16ToBytes(Self,LValue$1));
      }
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
   ,Create$29$:function($){return $.ClassType.Create$29.apply($.ClassType, arguments)}
};
/// TWriter = class (TW3CustomWriter)
var TWriter = {
   $ClassName:"TWriter",$Parent:TW3CustomWriter
   ,$Init:function ($) {
      TW3CustomWriter.$Init($);
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
   ,Create$29:TW3CustomWriter.Create$29
};
/// TStreamWriter = class (TW3CustomWriter)
var TStreamWriter = {
   $ClassName:"TStreamWriter",$Parent:TW3CustomWriter
   ,$Init:function ($) {
      TW3CustomWriter.$Init($);
   }
   /// anonymous TSourceMethodSymbol
   ,a$26:function(Self, Value$141) {
      TW3CustomWriter.SetAccessOptions(Self,Value$141);
   }
   /// constructor TStreamWriter.Create(const Stream: TStream)
   ,Create$30:function(Self, Stream$6) {
      TW3CustomWriter.Create$29(Self,$AsIntf(Stream$6,"IBinaryTransport"));
      TW3CustomWriter.SetAccessOptions(Self,[3]);
      return Self
   }
   ,Destroy:TDataTypeConverter.Destroy
   ,Create$15:TDataTypeConverter.Create$15
   ,Create$29:TW3CustomWriter.Create$29
   ,Create$30$:function($){return $.ClassType.Create$30.apply($.ClassType, arguments)}
};
/// EW3WriteError = class (EW3Exception)
var EW3WriteError = {
   $ClassName:"EW3WriteError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TW3DirectoryParser = class (TW3ErrorObject)
var TW3DirectoryParser = {
   $ClassName:"TW3DirectoryParser",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
   }
   /// function TW3DirectoryParser.GetErrorObject() : IW3ErrorAccess
   ,GetErrorObject:function(Self) {
      return $AsIntf(Self,"IW3ErrorAccess");
   }
   /// function TW3DirectoryParser.IsPathRooted(FilePath: String) : Boolean
   ,IsPathRooted:function(Self, FilePath) {
      var Result = false;
      var LMoniker = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      FilePath = (Trim$_String_(FilePath)).toLocaleLowerCase();
      if (FilePath.length>0) {
         LMoniker = TW3DirectoryParser.GetRootMoniker$(Self);
         Result = StrBeginsWith(FilePath,LMoniker);
      }
      return Result
   }
   /// function TW3DirectoryParser.IsRelativePath(FilePath: String) : Boolean
   ,IsRelativePath:function(Self, FilePath$1) {
      var Result = false;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TW3DirectoryParser.IsValidPath$(Self,FilePath$1)) {
         Result = !StrBeginsWith(FilePath$1,TW3DirectoryParser.GetRootMoniker$(Self));
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$3:TW3ErrorObject.Create$3
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsPathRooted$:function($){return $.ClassType.IsPathRooted.apply($.ClassType, arguments)}
   ,IsRelativePath$:function($){return $.ClassType.IsRelativePath.apply($.ClassType, arguments)}
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3DirectoryParser.$Intf={
   IW3DirectoryParser:[TW3DirectoryParser.GetPathSeparator,TW3DirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3DirectoryParser.IsValidPath,TW3DirectoryParser.HasValidPathChars,TW3DirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3DirectoryParser.GetFileNameWithoutExtension,TW3DirectoryParser.GetPathName,TW3DirectoryParser.GetDevice,TW3DirectoryParser.GetFileName,TW3DirectoryParser.GetExtension,TW3DirectoryParser.GetDirectoryName,TW3DirectoryParser.IncludeTrailingPathDelimiter,TW3DirectoryParser.IncludeLeadingPathDelimiter,TW3DirectoryParser.ExcludeLeadingPathDelimiter,TW3DirectoryParser.ExcludeTrailingPathDelimiter,TW3DirectoryParser.ChangeFileExt]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TW3UnixDirectoryParser = class (TW3DirectoryParser)
var TW3UnixDirectoryParser = {
   $ClassName:"TW3UnixDirectoryParser",$Parent:TW3DirectoryParser
   ,$Init:function ($) {
      TW3DirectoryParser.$Init($);
   }
   /// function TW3UnixDirectoryParser.ChangeFileExt(const FilePath: String; NewExt: String) : String
   ,ChangeFileExt:function(Self, FilePath$2, NewExt) {
      NewExt={v:NewExt};
      var Result = "";
      var Separator$1 = "",
         x$32 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      Separator$1 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrEndsWith(FilePath$2,Separator$1)) {
         TW3ErrorObject.SetLastError$(Self,"Failed to change extension, path has no filename error");
         Result = FilePath$2;
         return Result;
      }
      NewExt.v = Trim$_String_(NewExt.v);
      while ((NewExt.v.charAt(0)==".")) {
         Delete(NewExt,1,1);
         if (NewExt.v.length<1) {
            break;
         }
      }
      if (NewExt.v.length>0) {
         NewExt.v = "."+NewExt.v;
      }
      for(x$32=FilePath$2.length;x$32>=1;x$32--) {
         {var $temp37 = FilePath$2.charAt(x$32-1);
            if ($temp37==".") {
               Result = FilePath$2.substr(0,(x$32-1))+NewExt.v;
               break;
            }
             else if ($temp37==Separator$1) {
               Result = FilePath$2+NewExt.v;
               break;
            }
         }
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter(const FilePath: String) : String
   ,ExcludeLeadingPathDelimiter:function(Self, FilePath$3) {
      var Result = "";
      if (StrBeginsWith(FilePath$3,TW3DirectoryParser.GetPathSeparator$(Self))) {
         Result = FilePath$3.substr(1);
      } else {
         Result = FilePath$3;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter(const FilePath: String) : String
   ,ExcludeTrailingPathDelimiter:function(Self, FilePath$4) {
      var Result = "";
      if (StrEndsWith(FilePath$4,TW3DirectoryParser.GetPathSeparator$(Self))) {
         Result = (FilePath$4).substr(0,(FilePath$4.length-1));
      } else {
         Result = FilePath$4;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetDevice(const FilePath: String) : String
   ,GetDevice:function(Self, FilePath$5) {
      var Result = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$5.length>0) {
         if (StrBeginsWith(FilePath$5,TW3DirectoryParser.GetRootMoniker$(Self))) {
            Result = TW3DirectoryParser.GetRootMoniker$(Self);
         } else {
            Result = "";
         }
      } else {
         TW3ErrorObject.SetLastError$(Self,"Failed to extract device, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetDirectoryName(const FilePath: String) : String
   ,GetDirectoryName:function(Self, FilePath$6) {
      var Result = "";
      var Separator$2 = "",
         NameParts = [];
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$6.length>0) {
         Separator$2 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(FilePath$6,Separator$2)) {
            Result = FilePath$6;
            return Result;
         }
         NameParts = (FilePath$6).split(Separator$2);
         NameParts.splice((NameParts.length-1),1)
         ;
         Result = (NameParts).join(Separator$2)+Separator$2;
      } else {
         TW3ErrorObject.SetLastError$(Self,"Failed to extract directory, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetExtension(const Filename: String) : String
   ,GetExtension:function(Self, Filename$3) {
      var Result = "";
      var Separator$3 = "",
         x$33 = 0;
      var dx = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (Filename$3.length>0) {
         Separator$3 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Filename$3,Separator$3)) {
            TW3ErrorObject.SetLastError$(Self,"Failed to extract extension, path contains no filename error");
         } else {
            for(x$33=Filename$3.length;x$33>=1;x$33--) {
               {var $temp38 = Filename$3.charAt(x$33-1);
                  if ($temp38==".") {
                     dx = Filename$3.length;
                     (dx-= x$33);
                     ++dx;
                     Result = RightStr(Filename$3,dx);
                     break;
                  }
                   else if ($temp38==Separator$3) {
                     break;
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastError$(Self,"Failed to extract extension, filename was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetFileName(const FilePath: String) : String
   ,GetFileName:function(Self, FilePath$7) {
      var Result = "";
      var Temp$1 = "",
         Separator$4 = "",
         Parts$1 = [];
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      Temp$1 = Trim$_String_(FilePath$7);
      if (Temp$1.length>0) {
         Separator$4 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Temp$1,Separator$4)) {
            TW3ErrorObject.SetLastError$(Self,"No filename part in path error");
         } else {
            Parts$1 = (Temp$1).split(Separator$4);
            Result = Parts$1[(Parts$1.length-1)];
         }
      } else {
         TW3ErrorObject.SetLastError$(Self,"Failed to extract filename, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetFileNameWithoutExtension(const Filename: String) : String
   ,GetFileNameWithoutExtension:function(Self, Filename$4) {
      var Result = "";
      var temp$18 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      temp$18 = TW3DirectoryParser.GetFileName$(Self,Filename$4);
      if (!TW3ErrorObject.GetFailed(Self)) {
         Result = TW3DirectoryParser.ChangeFileExt$(Self,temp$18,"");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetPathName(const FilePath: String) : String
   ,GetPathName:function(Self, FilePath$8) {
      var Result = "";
      var Temp$2 = "",
         Parts$2 = [],
         Separator$5 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      Temp$2 = Trim$_String_(FilePath$8);
      if (Temp$2.length>0) {
         Separator$5 = TW3DirectoryParser.GetPathSeparator$(Self);
         if (StrEndsWith(Temp$2,Separator$5)) {
            if (Temp$2==TW3DirectoryParser.GetRootMoniker$(Self)) {
               TW3ErrorObject.SetLastError$(Self,"Failed to get directory name, path is root");
               return "";
            }
            Temp$2 = (Temp$2).substr(0,(Temp$2.length-1));
            Parts$2 = (Temp$2).split(Separator$5);
            Result = Parts$2[(Parts$2.length-1)];
            return Result;
         }
         Parts$2 = (Temp$2).split(Separator$5);
         if (Parts$2.length>1) {
            Result = Parts$2[(Parts$2.length-1)-1];
         } else {
            Result = Parts$2[(Parts$2.length-1)];
         }
      } else {
         TW3ErrorObject.SetLastError$(Self,"Failed to extract directory name, path was empty error");
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.GetPathSeparator() : Char
   ,GetPathSeparator:function(Self) {
      return "\/";
   }
   /// function TW3UnixDirectoryParser.GetRootMoniker() : String
   ,GetRootMoniker:function(Self) {
      return "~\/";
   }
   /// function TW3UnixDirectoryParser.HasValidFileNameChars(FileName: String) : Boolean
   ,HasValidFileNameChars:function(Self, FileName) {
      var Result = false;
      var el = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FileName.length>0) {
         if ((FileName.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \" \" in filename \"%s\" error",[FileName]);
         } else {
            for (var $temp39=0;$temp39<FileName.length;$temp39++) {
               el=$uniCharAt(FileName,$temp39);
               if (!el) continue;
               Result = (((el>="A")&&(el<="Z"))||((el>="a")&&(el<="z"))||((el>="0")&&(el<="9"))||(el=="-")||(el=="_")||(el==".")||(el==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \"%s\" in filename \"%s\" error",[el, FileName]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.HasValidPathChars(FolderName: String) : Boolean
   ,HasValidPathChars:function(Self, FolderName) {
      var Result = false;
      var el$1 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if ((FolderName.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \" \" in foldername \"%s\" error",[FolderName]);
      } else {
         if (FolderName.length>0) {
            for (var $temp40=0;$temp40<FolderName.length;$temp40++) {
               el$1=$uniCharAt(FolderName,$temp40);
               if (!el$1) continue;
               Result = (((el$1>="A")&&(el$1<="Z"))||((el$1>="a")&&(el$1<="z"))||((el$1>="0")&&(el$1<="9"))||(el$1=="-")||(el$1=="_")||(el$1==".")||(el$1==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \"%s\" in foldername \"%s\" error",[el$1, FolderName]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.IncludeLeadingPathDelimiter(const FilePath: String) : String
   ,IncludeLeadingPathDelimiter:function(Self, FilePath$9) {
      var Result = "";
      var Separator$6 = "";
      Separator$6 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$9,Separator$6)) {
         Result = FilePath$9;
      } else {
         Result = Separator$6+FilePath$9;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.IncludeTrailingPathDelimiter(const FilePath: String) : String
   ,IncludeTrailingPathDelimiter:function(Self, FilePath$10) {
      var Result = "";
      var LSeparator = "";
      LSeparator = TW3DirectoryParser.GetPathSeparator$(Self);
      Result = FilePath$10;
      if (!StrEndsWith(Result,LSeparator)) {
         Result+=LSeparator;
      }
      return Result
   }
   /// function TW3UnixDirectoryParser.IsValidPath(FilePath: String) : Boolean
   ,IsValidPath:function(Self, FilePath$11) {
      var Result = false;
      var Separator$7 = "",
         PathParts = [],
         Index$7 = 0,
         a$461 = 0;
      var part = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if ((FilePath$11.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \" \" in path \"%s\" error",[FilePath$11]);
      } else {
         if (FilePath$11.length>0) {
            Separator$7 = TW3DirectoryParser.GetPathSeparator$(Self);
            PathParts = (FilePath$11).split(Separator$7);
            Index$7 = 0;
            var $temp41;
            for(a$461=0,$temp41=PathParts.length;a$461<$temp41;a$461++) {
               part = PathParts[a$461];
               {var $temp42 = part;
                  if ($temp42=="") {
                     TW3ErrorObject.SetLastErrorF(Self,"Path has multiple separators (%s) error",[FilePath$11]);
                     return false;
                  }
                   else if ($temp42=="~") {
                     if (Index$7>0) {
                        TW3ErrorObject.SetLastErrorF(Self,"Path has misplaced root moniker (%s) error",[FilePath$11]);
                        return false;
                     }
                  }
                   else {
                     if (Index$7==(PathParts.length-1)) {
                        if (!TW3DirectoryParser.HasValidFileNameChars$(Self,part)) {
                           return false;
                        }
                     } else if (!TW3DirectoryParser.HasValidPathChars$(Self,part)) {
                        return false;
                     }
                  }
               }
               Index$7+=1;
            }
            Result = true;
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$3:TW3ErrorObject.Create$3
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsPathRooted:TW3DirectoryParser.IsPathRooted
   ,IsRelativePath:TW3DirectoryParser.IsRelativePath
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3UnixDirectoryParser.$Intf={
   IW3DirectoryParser:[TW3UnixDirectoryParser.GetPathSeparator,TW3UnixDirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3UnixDirectoryParser.IsValidPath,TW3UnixDirectoryParser.HasValidPathChars,TW3UnixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3UnixDirectoryParser.GetFileNameWithoutExtension,TW3UnixDirectoryParser.GetPathName,TW3UnixDirectoryParser.GetDevice,TW3UnixDirectoryParser.GetFileName,TW3UnixDirectoryParser.GetExtension,TW3UnixDirectoryParser.GetDirectoryName,TW3UnixDirectoryParser.IncludeTrailingPathDelimiter,TW3UnixDirectoryParser.IncludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter,TW3UnixDirectoryParser.ChangeFileExt]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TPath = class (TObject)
var TPath = {
   $ClassName:"TPath",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TPath.ChangeFileExt(const FilePath: String; const NewExt: String) : String
   ,ChangeFileExt$3:function(FilePath$12, NewExt$1) {
      var Result = "";
      var Access$4 = null,
         Error$2 = null;
      Access$4 = GetDirectoryParser();
      Error$2 = Access$4[2]();
      Result = Access$4[18](FilePath$12,NewExt$1);
      if (Error$2[0]()) {
         throw Exception.Create($New(EPathError),Error$2[1]());
      }
      return Result
   }
   /// function TPath.ExcludeLeadingPathDelimiter(const FilePath: String) : String
   ,ExcludeLeadingPathDelimiter$3:function(FilePath$13) {
      var Result = "";
      var Access$5 = null,
         Error$3 = null;
      Access$5 = GetDirectoryParser();
      Error$3 = Access$5[2]();
      Result = Access$5[16](FilePath$13);
      if (Error$3[0]()) {
         throw Exception.Create($New(EPathError),Error$3[1]());
      }
      return Result
   }
   /// function TPath.ExcludeTrailingPathDelimiter(const FilePath: String) : String
   ,ExcludeTrailingPathDelimiter$3:function(FilePath$14) {
      var Result = "";
      var Access$6 = null,
         Error$4 = null;
      Access$6 = GetDirectoryParser();
      Error$4 = Access$6[2]();
      Result = Access$6[17](FilePath$14);
      if (Error$4[0]()) {
         throw Exception.Create($New(EPathError),Error$4[1]());
      }
      return Result
   }
   /// function TPath.GetDevice(const FilePath: String) : String
   ,GetDevice$3:function(FilePath$15) {
      var Result = "";
      var Access$7 = null,
         Error$5 = null;
      Access$7 = GetDirectoryParser();
      Error$5 = Access$7[2]();
      Result = Access$7[10](FilePath$15);
      if (Error$5[0]()) {
         throw Exception.Create($New(EPathError),Error$5[1]());
      }
      return Result
   }
   /// function TPath.GetDirectoryName(const FilePath: String) : String
   ,GetDirectoryName$3:function(FilePath$16) {
      var Result = "";
      var Access$8 = null,
         Error$6 = null;
      Access$8 = GetDirectoryParser();
      Error$6 = Access$8[2]();
      Result = Access$8[13](FilePath$16);
      if (Error$6[0]()) {
         throw Exception.Create($New(EPathError),Error$6[1]());
      }
      return Result
   }
   /// function TPath.GetExtension(const Filename: String) : String
   ,GetExtension$3:function(Filename$5) {
      var Result = "";
      var Access$9 = null;
      Access$9 = GetDirectoryParser();
      Result = Access$9[12](Filename$5);
      return Result
   }
   /// function TPath.GetFileName(const FilePath: String) : String
   ,GetFileName$3:function(FilePath$17) {
      var Result = "";
      var Access$10 = null,
         Error$7 = null;
      Access$10 = GetDirectoryParser();
      Error$7 = Access$10[2]();
      Result = Access$10[11](FilePath$17);
      if (Error$7[0]()) {
         throw Exception.Create($New(EPathError),Error$7[1]());
      }
      return Result
   }
   /// function TPath.GetFileNameWithoutExtension(const Filename: String) : String
   ,GetFileNameWithoutExtension$3:function(Filename$6) {
      var Result = "";
      var Access$11 = null,
         Error$8 = null;
      Access$11 = GetDirectoryParser();
      Error$8 = Access$11[2]();
      Result = Access$11[8](Filename$6);
      if (Error$8[0]()) {
         throw Exception.Create($New(EPathError),Error$8[1]());
      }
      return Result
   }
   /// function TPath.GetGUIDFilename(const UseSeparator: Boolean; const Ext: String) : String
   ,GetGUIDFilename$1:function(UseSeparator, Ext) {
      var Result = "";
      var temp$19 = "";
      temp$19 = Trim$_String_(Ext);
      if (temp$19.length>0) {
         while ((temp$19.charAt(0)==".")) {
            temp$19 = RightStr(temp$19,temp$19.length-1);
         }
         temp$19 = "."+temp$19;
      }
      Result = TString.CreateGUID(TString)+temp$19;
      if (!UseSeparator) {
         Result = StrReplace(Result,"-","");
      }
      return Result
   }
   /// function TPath.GetGUIDFilename(const UseSeparator: Boolean) : String
   ,GetGUIDFilename:function(UseSeparator$1) {
      var Result = "";
      Result = TString.CreateGUID(TString);
      if (!UseSeparator$1) {
         Result = StrReplace(Result,"-","");
      }
      Result+=".dat";
      return Result
   }
   /// function TPath.GetPathName(const FilePath: String) : String
   ,GetPathName$3:function(FilePath$18) {
      var Result = "";
      var Access$12 = null,
         Error$9 = null;
      Access$12 = GetDirectoryParser();
      Error$9 = Access$12[2]();
      Result = Access$12[9](FilePath$18);
      return Result
   }
   /// function TPath.GetPathSeparator() : Char
   ,GetPathSeparator$4:function() {
      var Result = "";
      var Access$13 = null,
         Error$10 = null;
      Access$13 = GetDirectoryParser();
      Error$10 = Access$13[2]();
      Result = Access$13[0]();
      if (Error$10[0]()) {
         throw Exception.Create($New(EPathError),Error$10[1]());
      }
      return Result
   }
   /// function TPath.GetRootMoniker() : String
   ,GetRootMoniker$4:function() {
      var Result = "";
      var Access$14 = null,
         Error$11 = null;
      Access$14 = GetDirectoryParser();
      Error$11 = Access$14[2]();
      Result = Access$14[1]();
      if (Error$11[0]()) {
         throw Exception.Create($New(EPathError),Error$11[1]());
      }
      return Result
   }
   /// function TPath.HasValidFileNameChars(const FileName: String) : Boolean
   ,HasValidFileNameChars$3:function(FileName$1) {
      var Result = false;
      var Access$15 = null,
         Error$12 = null;
      Access$15 = GetDirectoryParser();
      Error$12 = Access$15[2]();
      Result = Access$15[5](FileName$1);
      if (Error$12[0]()) {
         throw Exception.Create($New(EPathError),Error$12[1]());
      }
      return Result
   }
   /// function TPath.HasValidPathChars(const FilePath: String) : Boolean
   ,HasValidPathChars$3:function(FilePath$19) {
      var Result = false;
      var Access$16 = null,
         Error$13 = null;
      Access$16 = GetDirectoryParser();
      Error$13 = Access$16[2]();
      Result = Access$16[4](FilePath$19);
      if (Error$13[0]()) {
         throw Exception.Create($New(EPathError),Error$13[1]());
      }
      return Result
   }
   /// function TPath.IncludeLeadingPathDelimiter(const FilePath: String) : String
   ,IncludeLeadingPathDelimiter$3:function(FilePath$20) {
      var Result = "";
      var Access$17 = null,
         Error$14 = null;
      Access$17 = GetDirectoryParser();
      Error$14 = Access$17[2]();
      Result = Access$17[15](FilePath$20);
      if (Error$14[0]()) {
         throw Exception.Create($New(EPathError),Error$14[1]());
      }
      return Result
   }
   /// function TPath.IncludeTrailingPathDelimiter(const FilePath: String) : String
   ,IncludeTrailingPathDelimiter$3:function(FilePath$21) {
      var Result = "";
      var Access$18 = null,
         Error$15 = null;
      Access$18 = GetDirectoryParser();
      Error$15 = Access$18[2]();
      Result = Access$18[14](FilePath$21);
      if (Error$15[0]()) {
         throw Exception.Create($New(EPathError),Error$15[1]());
      }
      return Result
   }
   /// function TPath.IsPathRooted(const FilePath: String) : Boolean
   ,IsPathRooted$1:function(FilePath$22) {
      var Result = false;
      var Access$19 = null,
         Error$16 = null;
      Access$19 = GetDirectoryParser();
      Error$16 = Access$19[2]();
      Result = Access$19[7](FilePath$22);
      if (Error$16[0]()) {
         throw Exception.Create($New(EPathError),Error$16[1]());
      }
      return Result
   }
   /// function TPath.IsRelativePath(const FilePath: String) : Boolean
   ,IsRelativePath$1:function(FilePath$23) {
      var Result = false;
      var Access$20 = null,
         Error$17 = null;
      Access$20 = GetDirectoryParser();
      Error$17 = Access$20[2]();
      Result = Access$20[6](FilePath$23);
      if (Error$17[0]()) {
         throw Exception.Create($New(EPathError),Error$17[1]());
      }
      return Result
   }
   /// function TPath.IsValidPath(const FilePath: String) : Boolean
   ,IsValidPath$3:function(FilePath$24) {
      var Result = false;
      var Access$21 = null,
         Error$18 = null;
      Access$21 = GetDirectoryParser();
      Error$18 = Access$21[2]();
      Result = Access$21[3](FilePath$24);
      if (Error$18[0]()) {
         throw Exception.Create($New(EPathError),Error$18[1]());
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
function InstallDirectoryParser(NewParser) {
   if (__Parser!==null) {
      TObject.Free(__Parser);
      __Parser = null;
   }
   __Parser = NewParser;
};
function GetDirectoryParser() {
   var Result = null;
   if (__Parser===null) {
      if (GetIsRunningInBrowser()) {
         __Parser = TW3ErrorObject.Create$3$($New(TW3UnixDirectoryParser));
      }
   }
   if (__Parser!==null) {
      Result = $AsIntf(__Parser,"IW3DirectoryParser");
   } else {
      Result = null;
   }
   return Result
};
/// EPathError = class (EW3Exception)
var EPathError = {
   $ClassName:"EPathError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TW3Component = class (TW3OwnedLockedErrorObject)
var TW3Component = {
   $ClassName:"TW3Component",$Parent:TW3OwnedLockedErrorObject
   ,$Init:function ($) {
      TW3OwnedLockedErrorObject.$Init($);
      $.TagObject = null;
      $.TagValue = 0;
      $.Name = "";
      $.FInitialized = false;
   }
   /// constructor TW3Component.Create(AOwner: TW3Component)
   ,Create$44:function(Self, AOwner$3) {
      TW3OwnedErrorObject.Create$16(Self,AOwner$3);
      Self.FInitialized = true;
      TW3Component.InitializeObject$(Self);
      return Self
   }
   /// constructor TW3Component.CreateEx(AOwner: TObject)
   ,CreateEx:function(Self, AOwner$4) {
      TW3OwnedErrorObject.Create$16(Self,AOwner$4);
      Self.FInitialized = false;
      return Self
   }
   /// destructor TW3Component.Destroy()
   ,Destroy:function(Self) {
      if (Self.FInitialized) {
         TW3Component.FinalizeObject$(Self);
      }
      TW3OwnedErrorObject.Destroy(Self);
   }
   /// procedure TW3Component.FinalizeObject()
   ,FinalizeObject:function(Self) {
      /* null */
   }
   /// procedure TW3Component.FreeAfter(Delay: Integer)
   ,FreeAfter:function(Self, Delay$2) {
      TW3Dispatch.Execute(TW3Dispatch,$Event0(Self,TObject.Free),Delay$2);
   }
   /// procedure TW3Component.InitializeObject()
   ,InitializeObject:function(Self) {
      /* null */
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,SetLastError$1:TW3OwnedErrorObject.SetLastError$1
   ,ObjectLocked$2:TW3OwnedLockedErrorObject.ObjectLocked$2
   ,ObjectUnLocked$2:TW3OwnedLockedErrorObject.ObjectUnLocked$2
   ,Create$44$:function($){return $.ClassType.Create$44.apply($.ClassType, arguments)}
   ,CreateEx$:function($){return $.ClassType.CreateEx.apply($.ClassType, arguments)}
   ,FinalizeObject$:function($){return $.ClassType.FinalizeObject($)}
   ,FreeAfter$:function($){return $.ClassType.FreeAfter.apply($.ClassType, arguments)}
   ,InitializeObject$:function($){return $.ClassType.InitializeObject($)}
};
TW3Component.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3Timer = class (TW3Component)
var TW3Timer = {
   $ClassName:"TW3Timer",$Parent:TW3Component
   ,$Init:function ($) {
      TW3Component.$Init($);
      $.OnTime = null;
      $.FDelay = 0;
      $.FRepeater = null;
   }
   /// constructor TW3Timer.Create(AOwner: TW3Component)
   ,Create$44:function(Self, AOwner$5) {
      TW3Component.Create$44(Self,AOwner$5);
      Self.FDelay = 1000;
      Self.FRepeater = TW3EventRepeater.Create$47$($New(TW3EventRepeater));
      Self.FRepeater.OnRepeat = $Event1(Self,TW3Timer.HandleEventCallback);
      return Self
   }
   /// destructor TW3Timer.Destroy()
   ,Destroy:function(Self) {
      TObject.Free(Self.FRepeater);
      TW3Component.Destroy(Self);
   }
   /// function TW3Timer.GetEnabled() : Boolean
   ,GetEnabled:function(Self) {
      return Self.FRepeater.FActive;
   }
   /// function TW3Timer.HandleEventCallback(Sender: TW3CustomRepeater) : TW3RepeatResult
   ,HandleEventCallback:function(Self, Sender$18) {
      var Result = 241;
      Result = 241;
      if (Self.OnTime) {
         Self.OnTime(Self);
      }
      return Result
   }
   /// procedure TW3Timer.SetDelay(const NewDelay: Integer)
   ,SetDelay:function(Self, NewDelay) {
      Self.FDelay = Math.max(NewDelay,1);
      if (Self.FDelay!=Self.FRepeater.FDelay$1) {
         TW3CustomRepeater.SetDelay$1$(Self.FRepeater,Self.FDelay);
      }
   }
   /// procedure TW3Timer.SetEnabled(const NewState: Boolean)
   ,SetEnabled:function(Self, NewState) {
      if (NewState!=TW3Timer.GetEnabled$(Self)) {
         TW3CustomRepeater.SetDelay$1$(Self.FRepeater,Self.FDelay);
         TW3CustomRepeater.SetActive$(Self.FRepeater,(!TW3Timer.GetEnabled$(Self)));
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,SetLastError$1:TW3OwnedErrorObject.SetLastError$1
   ,ObjectLocked$2:TW3OwnedLockedErrorObject.ObjectLocked$2
   ,ObjectUnLocked$2:TW3OwnedLockedErrorObject.ObjectUnLocked$2
   ,Create$44$:function($){return $.ClassType.Create$44.apply($.ClassType, arguments)}
   ,CreateEx:TW3Component.CreateEx
   ,FinalizeObject:TW3Component.FinalizeObject
   ,FreeAfter:TW3Component.FreeAfter
   ,InitializeObject:TW3Component.InitializeObject
   ,GetEnabled$:function($){return $.ClassType.GetEnabled($)}
   ,SetDelay$:function($){return $.ClassType.SetDelay.apply($.ClassType, arguments)}
   ,SetEnabled$:function($){return $.ClassType.SetEnabled.apply($.ClassType, arguments)}
};
TW3Timer.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3RepeatResult enumeration
var TW3RepeatResult = { 241:"rrContinue", 242:"rrStop", 243:"rrDispose" };
/// TW3CustomRepeater = class (TObject)
var TW3CustomRepeater = {
   $ClassName:"TW3CustomRepeater",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.OnRepeat = null;
      $.FActive = false;
      $.FDelay$1 = 0;
      $.FHandle$1 = undefined;
   }
   /// procedure TW3CustomRepeater.AllocTimer()
   ,AllocTimer:function(Self) {
      if (Self.FHandle$1) {
         TW3CustomRepeater.FreeTimer$(Self);
      }
      Self.FHandle$1 = TW3Dispatch.SetInterval(TW3Dispatch,$Event0(Self,TW3CustomRepeater.CBExecute$),Self.FDelay$1);
   }
   /// destructor TW3CustomRepeater.Destroy()
   ,Destroy:function(Self) {
      if (Self.FActive) {
         TW3CustomRepeater.SetActive$(Self,false);
      }
      TObject.Destroy(Self);
   }
   /// procedure TW3CustomRepeater.FreeTimer()
   ,FreeTimer:function(Self) {
      if (Self.FHandle$1) {
         TW3Dispatch.ClearInterval(TW3Dispatch,Self.FHandle$1);
         Self.FHandle$1 = undefined;
      }
   }
   /// procedure TW3CustomRepeater.SetActive(const NewActive: Boolean)
   ,SetActive:function(Self, NewActive) {
      if (NewActive!=Self.FActive) {
         try {
            if (Self.FActive) {
               TW3CustomRepeater.FreeTimer$(Self);
            } else {
               TW3CustomRepeater.AllocTimer$(Self);
            }
         } finally {
            Self.FActive = NewActive;
         }
      }
   }
   /// procedure TW3CustomRepeater.SetDelay(const NewDelay: Integer)
   ,SetDelay$1:function(Self, NewDelay$1) {
      var LState = false;
      LState = Self.FActive;
      if (LState) {
         TW3CustomRepeater.SetActive$(Self,false);
      }
      Self.FDelay$1 = Math.max(NewDelay$1,1);
      if (LState) {
         TW3CustomRepeater.SetActive$(Self,true);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,AllocTimer$:function($){return $.ClassType.AllocTimer($)}
   ,CBExecute$:function($){return $.ClassType.CBExecute($)}
   ,FreeTimer$:function($){return $.ClassType.FreeTimer($)}
   ,SetActive$:function($){return $.ClassType.SetActive.apply($.ClassType, arguments)}
   ,SetDelay$1$:function($){return $.ClassType.SetDelay$1.apply($.ClassType, arguments)}
};
/// TW3EventRepeater = class (TW3CustomRepeater)
var TW3EventRepeater = {
   $ClassName:"TW3EventRepeater",$Parent:TW3CustomRepeater
   ,$Init:function ($) {
      TW3CustomRepeater.$Init($);
   }
   /// procedure TW3EventRepeater.CBExecute()
   ,CBExecute:function(Self) {
      if (Self.OnRepeat) {
         switch (Self.OnRepeat(Self)) {
            case 242 :
               TW3CustomRepeater.SetActive$(Self,false);
               break;
            case 243 :
               TW3CustomRepeater.SetActive$(Self,false);
               TObject.Free(Self);
               break;
         }
      }
   }
   /// constructor TW3EventRepeater.Create()
   ,Create$47:function(Self) {
      TObject.Create(Self);
      return Self
   }
   /// constructor TW3EventRepeater.Create(const EventHandler: TW3RepeaterEvent; const WaitForInMs: Integer)
   ,Create$46:function(Self, EventHandler, WaitForInMs) {
      TObject.Create(Self);
      Self.OnRepeat = EventHandler;
      TW3CustomRepeater.SetDelay$1$(Self,WaitForInMs);
      if ((Self.OnRepeat!==null)&&WaitForInMs>0) {
         TW3CustomRepeater.SetActive$(Self,true);
      }
      return Self
   }
   ,Destroy:TW3CustomRepeater.Destroy
   ,AllocTimer:TW3CustomRepeater.AllocTimer
   ,CBExecute$:function($){return $.ClassType.CBExecute($)}
   ,FreeTimer:TW3CustomRepeater.FreeTimer
   ,SetActive:TW3CustomRepeater.SetActive
   ,SetDelay$1:TW3CustomRepeater.SetDelay$1
   ,Create$47$:function($){return $.ClassType.Create$47($)}
   ,Create$46$:function($){return $.ClassType.Create$46.apply($.ClassType, arguments)}
};
/// TW3Dispatch = class (TObject)
var TW3Dispatch = {
   $ClassName:"TW3Dispatch",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// procedure TW3Dispatch.CancelExecute(const Handle: TW3DispatchHandle)
   ,CancelExecute:function(Self, Handle$7) {
      clearTimeout(Handle$7);
   }
   /// procedure TW3Dispatch.ClearInterval(const Handle: TW3DispatchHandle)
   ,ClearInterval:function(Self, Handle$8) {
      clearInterval(Handle$8);
   }
   /// procedure TW3Dispatch.ClearTimeOut(const Handle: TW3DispatchHandle)
   ,ClearTimeOut:function(Self, Handle$9) {
      clearTimeout(Handle$9);
   }
   /// function TW3Dispatch.Execute(const EntryPoint: TProcedureRef; const WaitForInMs: Integer) : TW3DispatchHandle
   ,Execute:function(Self, EntryPoint$1, WaitForInMs$1) {
      var Result = undefined;
      Result = setTimeout(EntryPoint$1,WaitForInMs$1);
      return Result
   }
   /// function TW3Dispatch.JsNow() : JDate
   ,JsNow:function(Self) {
      return new Date();
   }
   /// procedure TW3Dispatch.RepeatExecute(const Entrypoint: TProcedureRef; const RepeatCount: Integer; const IntervalInMs: Integer)
   ,RepeatExecute:function(Self, Entrypoint, RepeatCount, IntervalInMs) {
      if (Entrypoint) {
         if (RepeatCount>0) {
            Entrypoint();
            if (RepeatCount>1) {
               TW3Dispatch.Execute(Self,function () {
                  TW3Dispatch.RepeatExecute(Self,Entrypoint,(RepeatCount-1),IntervalInMs);
               },IntervalInMs);
            }
         } else {
            Entrypoint();
            TW3Dispatch.Execute(Self,function () {
               TW3Dispatch.RepeatExecute(Self,Entrypoint,(-1),IntervalInMs);
            },IntervalInMs);
         }
      }
   }
   /// function TW3Dispatch.SetInterval(const Entrypoint: TProcedureRef; const IntervalDelayInMS: Integer) : TW3DispatchHandle
   ,SetInterval:function(Self, Entrypoint$1, IntervalDelayInMS) {
      var Result = undefined;
      Result = setInterval(Entrypoint$1,IntervalDelayInMS);
      return Result
   }
   /// function TW3Dispatch.SetTimeOut(const Entrypoint: TProcedureRef; const WaitForInMs: Integer) : TW3DispatchHandle
   ,SetTimeOut:function(Self, Entrypoint$2, WaitForInMs$2) {
      var Result = undefined;
      Result = setTimeout(Entrypoint$2,WaitForInMs$2);
      return Result
   }
   /// function TW3Dispatch.Ticks() : Integer
   ,Ticks:function(Self) {
      return new Date().getTime()*1000+621355968000000000;
   }
   /// function TW3Dispatch.TicksBetween(const Past: TDateTime; const Future: TDateTime) : Integer
   ,TicksBetween:function(Self, Past, Future) {
      return TInteger.SubtractSmallest(TW3Dispatch.TicksOf(Self,Past),TW3Dispatch.TicksOf(Self,Future));
   }
   /// function TW3Dispatch.TicksOf(const Present: TDateTime) : Integer
   ,TicksOf:function(Self, Present) {
      var Result = 0;
      var temp$20 = null;
      temp$20 = DateTimeToJDate(Present);
      Result = temp$20.getTime()*1000+621355968000000000;
      return Result
   }
   ,Destroy:TObject.Destroy
};
function GetMilliseconds() {
   var Result = 0;
   Result = Date.now();
   return Result
};
/// EW3EventRepeater = class (EW3Exception)
var EW3EventRepeater = {
   $ClassName:"EW3EventRepeater",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EW3CustomRepeater = class (EW3Exception)
var EW3CustomRepeater = {
   $ClassName:"EW3CustomRepeater",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
function YearOf$1(value$3) {
   var Result = {v:0};
   try {
      var mVoid = {};
      mVoid.v = 0;
      DecodeDate$1((Number(value$3)),Result,mVoid,mVoid);
   } finally {return Result.v}
};
function WeeksOfYear(Year) {
   var Result = 0;
   if (IsLeapYear(Year)) {
      Result = 53;
   } else {
      Result = 52;
   }
   return Result
};
function WeeksOfYear$1(value$4) {
   return WeeksOfYear(YearOf$1(value$4));
};
function WeekOfYear(Value$142) {
   var Result = 0;
   var mDays = 0;
   mDays = DayOfYear$1(Value$142);
   Result = $Div(mDays,7);
   if (mDays%7>0) {
      ++Result;
   }
   return Result
};
/// TW3TimeStamp = record
function Copy$TW3TimeStamp(s,d) {
   d.StampTime=s.StampTime;
   d.StampDate=s.StampDate;
   return d;
}
function Clone$TW3TimeStamp($) {
   return {
      StampTime:$.StampTime,
      StampDate:$.StampDate
   }
}
function TryEncodeTime(Hour, Min$3, Sec, MSec, Time$1) {
   var Result = false;
   var mStamp = {StampTime:0,StampDate:0};
   if (Hour<24&&Min$3<60&&Sec<60&&MSec<1000) {
      mStamp.StampTime = Hour*3600000+Min$3*60*1000+Sec*1000+MSec;
      mStamp.StampDate = 693594;
      Time$1.v = Number(TimeStampToDateTime(mStamp));
      Result = true;
   }
   return Result
};
function TryEncodeDate(Year$1, Month, Day, Date$2) {
   var Result = false;
   var I = 0;
   var LLeap = false;
   Date$2.v = TDataTypeConverter.InitFloat32(0);
   if (Year$1>=1&&Year$1<=9999&&Month>=1&&Month<=12&&Day>=1) {
      LLeap = IsLeapYear(Year$1);
      if (Day<=CNT_DaysInMonthData[LLeap?1:0][(Month)-1]) {
         var $temp43;
         for(I=1,$temp43=Month;I<$temp43;I++) {
            (Day+= CNT_DaysInMonthData[LLeap?1:0][(I)-1]);
         }
         I = Year$1-1;
         Date$2.v+=I*365+($Div(I,4))-($Div(I,100))+($Div(I,400))+Day-693594;
         Result = true;
      }
   }
   return Result
};
function TimeToMilliSeconds(Value$143) {
   var Result = 0;
   var mHour = {};
   mHour.v = 0;
   var mMinute = {};
   mMinute.v = 0;
   var mSecond = {};
   mSecond.v = 0;
   var mMsec = {};
   mMsec.v = 0;
   DecodeTime$1(Value$143,mHour,mMinute,mSecond,mMsec);
   Result = mHour.v*3600000+mMinute.v*60000+mSecond.v*1000+mMsec.v;
   return Result
};
function TimeStampToDateTime(Stamp) {
   var Result = undefined;
   var Temp = 0;
   Temp = Stamp.StampDate;
   (Temp-= 693594);
   Temp*=86400000;
   if (Temp>=0) {
      (Temp+= Stamp.StampTime);
   } else {
      (Temp-= Stamp.StampTime);
   }
   Result = Temp/86400000;
   return Result
};
function TimeOf(value$5) {
   var Result = 0;
   var mHour$1 = {};
   mHour$1.v = 0;
   var mMinute$1 = {};
   mMinute$1.v = 0;
   var mSecond$1 = {};
   mSecond$1.v = 0;
   var mMSec = {};
   mMSec.v = 0;
   DecodeTime$1(value$5,mHour$1,mMinute$1,mSecond$1,mMSec);
   Result = EncodeTime$1(mHour$1.v,mMinute$1.v,mSecond$1.v,mMSec.v);
   return Result
};
function SecondsOf(value$6) {
   var Result = {v:0};
   try {
      var mVoid$1 = {};
      mVoid$1.v = 0;
      DecodeTime$1(TimeOf(value$6),mVoid$1,mVoid$1,Result,mVoid$1);
   } finally {return Result.v}
};
function SecondsBetween(aThen, aNow) {
   return Math.round(MillisecondsBetween(aThen,aNow)/1000);
};
function MonthOf(value$7) {
   var Result = {v:0};
   try {
      var mVoid$2 = {};
      mVoid$2.v = 0;
      DecodeDate$1((Number(value$7)),mVoid$2,Result,mVoid$2);
   } finally {return Result.v}
};
function MinutesBetween(aThen$1, aNow$1) {
   return Math.round(MillisecondsBetween(aThen$1,aNow$1)/60000);
};
function MinuteOf(value$8) {
   var Result = {v:0};
   try {
      var mVoid$3 = {};
      mVoid$3.v = 0;
      DecodeTime$1(TimeOf(value$8),mVoid$3,Result,mVoid$3,mVoid$3);
   } finally {return Result.v}
};
function MilliSecondsOf(Value$144) {
   var Result = {v:0};
   try {
      var mVoid$4 = {};
      mVoid$4.v = 0;
      DecodeTime$1(TimeOf(Value$144),mVoid$4,mVoid$4,mVoid$4,Result);
   } finally {return Result.v}
};
function MillisecondsBetween(aThen$2, aNow$2) {
   var Result = 0;
   var mSrc = 0,
      mDst = 0;
   mSrc = TimeToMilliSeconds(aNow$2);
   mDst = TimeToMilliSeconds(aThen$2);
   Result = Math.max(mSrc,mDst)-Math.min(mSrc,mDst);
   return Result
};
function JDateToLocalDateTime(Obj) {
   return (Obj.getTime()-60000*Obj.getTimezoneOffset())/86400000+25569;
};
function JDateToDateTime(Obj$1) {
   return Obj$1.getTime()/86400000+25569;
};
function HoursBetween(aThen$3, aNow$3) {
   return Math.round(MillisecondsBetween(aThen$3,aNow$3)/3600000);
};
function HourOf(value$9) {
   var Result = {v:0};
   try {
      var mVoid$5 = {};
      mVoid$5.v = 0;
      DecodeTime$1(TimeOf(value$9),Result,mVoid$5,mVoid$5,mVoid$5);
   } finally {return Result.v}
};
function EncodeTime$1(Hour$1, Minute, Second$3, Millisecond) {
   var Result = {v:0};
   try {
      TryEncodeTime(Hour$1,Minute,Second$3,Millisecond,Result);
   } finally {return Result.v}
};
function EncodeDateTime(Year$2, Months, Days, Hours, Minutes, Seconds, MilliSeconds) {
   var Result = undefined;
   var LDate = {v:0},
      LTime = {v:0};
   Result = TDataTypeConverter.InitFloat64(0);
   LDate.v = TDataTypeConverter.InitFloat32(0);
   if (TryEncodeDate(Year$2,Months,Days,LDate)) {
      LTime.v = TDataTypeConverter.InitFloat32(0);
      if (TryEncodeTime(Hours,Minutes,Seconds,MilliSeconds,LTime)) {
         if (LDate.v>=0) {
            Result+=LDate.v+LTime.v;
         } else {
            Result+=LDate.v-LTime.v;
         }
      }
   }
   return Result
};
function EncodeDateTime$1(Date$3, Time$2) {
   var Result = undefined;
   Result = Date$3;
   if (Result>=0) {
      Result+=Time$2;
   } else {
      Result-=Time$2;
   }
   return Result
};
function EncodeDate$1(Year$3, Month$1, Day$1) {
   var Result = {v:0};
   try {
      TryEncodeDate(Year$3,Month$1,Day$1,Result);
   } finally {return Result.v}
};
function DecodeTime$1(DateTime, Hours$1, Minutes$1, Seconds$1, Milliseconds) {
   var MinCount = {};
   MinCount.v = 0;
   var MSecCount = {};
   MSecCount.v = 0;
   DivMod$_Integer_Integer_Integer_Integer_(DateTimeToTimeStamp(DateTime).StampTime,60000,MinCount,MSecCount);
   DivMod$_Integer_Integer_Integer_Integer_(MinCount.v,60,Hours$1,Minutes$1);
   DivMod$_Integer_Integer_Integer_Integer_(MSecCount.v,1000,Seconds$1,Milliseconds);
};
function DecodeDateTime(value$10, year, month, day, hour, minute, second, mseconds) {
   DecodeDate$1((Number(value$10)),year,month,day);
   DecodeTime$1(value$10,hour,minute,second,mseconds);
};
function DecodeDate$1(EntryDate, Year$4, Month$2, Day$2) {
   var QTX = 0;
   var M = 0;
   var Higher = {};
   Higher.v = 0;
   var Lower = {};
   Lower.v = 0;
   var DaysOfWeek = 0;
   var TimeInMS = 0;
   TimeInMS = DateTimeToTimeStamp(EntryDate).StampDate;
   if (TimeInMS>0) {
      DaysOfWeek = TimeInMS%7+1;
      --TimeInMS;
      QTX = 1;
      while (TimeInMS>=146097) {
         (TimeInMS-= 146097);
         (QTX+= 400);
      }
      DivMod$_Integer_Integer_Integer_Integer_(TimeInMS,36524,Higher,Lower);
      if (Higher.v==4) {
         --Higher.v;
         (Lower.v+= 36524);
      }
      (QTX+= (Higher.v*100));
      DivMod$_Integer_Integer_Integer_Integer_(Lower.v,1461,Higher,Lower);
      (QTX+= (Higher.v<<2));
      DivMod$_Integer_Integer_Integer_Integer_(Lower.v,365,Higher,Lower);
      if (Higher.v==4) {
         --Higher.v;
         (Lower.v+= 365);
      }
      (QTX+= Higher.v);
      M = 1;
      do {
         Higher.v = CNT_DaysInMonthData[IsLeapYear(QTX)?1:0][(M)-1];
         if (Lower.v<Higher.v) {
            break;
         }
         (Lower.v-= Higher.v);
         ++M;
      } while (!(Lower.v<Higher.v));
      ++Lower.v;
      Year$4.v = QTX;
      Month$2.v = M;
      Day$2.v = Lower.v;
   }
};
function DaysInMonth(Value$145) {
   var Result = 0;
   var mMonth = 0;
   mMonth = MonthOf(Value$145);
   if (mMonth>0&&mMonth<13) {
      Result = CNT_DaysInMonthData[IsLeapYear(YearOf$1(Value$145))?1:0][(mMonth)-1];
   }
   return Result
};
function DaysInMonth$1(Month$3) {
   var Result = 0;
   if (Month$3>0&&Month$3<13) {
      Result = CNT_DaysInMonthData[IsLeapYear(YearOf$1(Now()))?1:0][(Month$3)-1];
   }
   return Result
};
function DayOfYear$1(value$11) {
   var Result = 0;
   var mMonth$1 = 0;
   var x = 0;
   mMonth$1 = MonthOf(value$11);
   var $temp44;
   for(x=1,$temp44=mMonth$1;x<$temp44;x++) {
      (Result+= DaysInMonth$1(x));
   }
   (Result+= DayOf(value$11));
   return Result
};
function DayOf(value$12) {
   var Result = {v:0};
   try {
      var mVoid$6 = {};
      mVoid$6.v = 0;
      DecodeDate$1((Number(value$12)),mVoid$6,mVoid$6,Result);
   } finally {return Result.v}
};
function DateTimeToTimeStamp(Value$146) {
   var Result = {StampTime:0,StampDate:0};
   var _temp = 0;
   var _tempEx = 0;
   _temp = Math.round(Number(Value$146*86400000));
   _tempEx = $Div(_temp,86400000);
   Result.StampDate = 693594+_tempEx;
   Result.StampTime = Math.abs(_temp)%86400000;
   return Result
};
function DateTimeToLocalJDate(Present$1) {
   var Result = null;
   Result = new Date();
   Result.setTime(Math.round((Present$1-25569)*86400000)+60000*Result.getTimezoneOffset());
   return Result
};
function DateTimeToJDate(Present$2) {
   var Result = null;
   Result = new Date();
   Result.setTime(Math.round((Present$2-25569)*86400000));
   return Result
};
function DateTimeToBytes(Value$147) {
   var Result = [];
   var mRaw = null;
   var mWrite = null;
   mRaw = new Uint8Array(8);
    mWrite = new DataView((mRaw).buffer);
   mWrite.setFloat64(0,Number(Value$147),false);
   Result.push(mWrite.getUint8(0));
   Result.push(mWrite.getUint8(1));
   Result.push(mWrite.getUint8(2));
   Result.push(mWrite.getUint8(3));
   Result.push(mWrite.getUint8(4));
   Result.push(mWrite.getUint8(5));
   Result.push(mWrite.getUint8(6));
   Result.push(mWrite.getUint8(7));
   return Result
};
function DateOf(value$13) {
   var Result = 0;
   var myear = {};
   myear.v = 0;
   var mMonth$2 = {};
   mMonth$2.v = 0;
   var mDay = {};
   mDay.v = 0;
   DecodeDate$1((Number(value$13)),myear,mMonth$2,mDay);
   Result = EncodeDate$1(myear.v,mMonth$2.v,mDay.v);
   return Result
};
function BytesToDateTime(Bytes$11) {
   var Result = undefined;
   var mRaw$1 = null;
   var mWrite$1 = null;
   mRaw$1 = new Uint8Array(8);
    mWrite$1 = new DataView((mRaw$1).buffer);
   mWrite$1.setUint8(0,Bytes$11[0]);
   mWrite$1.setUint8(1,Bytes$11[1]);
   mWrite$1.setUint8(2,Bytes$11[2]);
   mWrite$1.setUint8(3,Bytes$11[3]);
   mWrite$1.setUint8(4,Bytes$11[4]);
   mWrite$1.setUint8(5,Bytes$11[5]);
   mWrite$1.setUint8(6,Bytes$11[6]);
   mWrite$1.setUint8(7,Bytes$11[7]);
   Result = mWrite$1.getFloat64(0,false);
   return Result
};
var CNT_DaysInMonthData = [[31,28,31,30,31,30,31,31,30,31,30,31],[31,29,31,30,31,30,31,31,30,31,30,31]];
/// TW3StorageObjectType enumeration
var TW3StorageObjectType = [ "otUnknown", "otFile", "otFolder", "otBlockDevice", "otCharacterDevice", "otSymbolicLink", "otFIFO", "otSocket" ];
/// TW3StorageDevice = class (TW3Component)
var TW3StorageDevice = {
   $ClassName:"TW3StorageDevice",$Parent:TW3Component
   ,$Init:function ($) {
      TW3Component.$Init($);
      $.OnUnMounted = null;
      $.OnMounted = null;
      $.FActive$1 = false;
      $.FIdentifier = $.FName = "";
      $.FOptions$5 = [0];
   }
   /// procedure TW3StorageDevice.Examine(CB: TW3FileOperationExamineCallBack)
   ,Examine$2:function(Self, CB$12) {
      var LFiles = null;
      if (TW3StorageDevice.GetActive$(Self)) {
         TW3StorageDevice.Examine$1$(Self,"",CB$12);
      } else {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed, device not active error",["TW3StorageDevice.Examine"]);
         if (CB$12) {
            LFiles.dlPath = "";
            CB$12(Self,"",LFiles,false);
         }
      }
   }
   /// procedure TW3StorageDevice.FinalizeObject()
   ,FinalizeObject:function(Self) {
      if (Self.FActive$1) {
         TW3StorageDevice.UnMount$(Self,null);
      }
      TW3Component.FinalizeObject(Self);
   }
   /// function TW3StorageDevice.GetActive() : Boolean
   ,GetActive:function(Self) {
      return Self.FActive$1;
   }
   /// procedure TW3StorageDevice.GetDeviceId(CB: TW3DeviceIdCallback)
   ,GetDeviceId:function(Self, CB$13) {
      if (CB$13) {
         CB$13(Self.FIdentifier,true);
      }
   }
   /// procedure TW3StorageDevice.GetDeviceOptions(CB: TW3DeviceOptionsCallback)
   ,GetDeviceOptions:function(Self, CB$14) {
      if (CB$14) {
         CB$14(Self.FOptions$5.slice(0),true);
      }
   }
   /// procedure TW3StorageDevice.GetName(CB: TW3DeviceNameCallback)
   ,GetName:function(Self, CB$15) {
      if (CB$15) {
         CB$15(Self.FName,true);
      }
   }
   /// procedure TW3StorageDevice.InitializeObject()
   ,InitializeObject:function(Self) {
      TW3Component.InitializeObject(Self);
      Self.FOptions$5 = [2];
      Self.FIdentifier = TString.CreateGUID(TString);
      Self.FName = "dh0";
   }
   /// procedure TW3StorageDevice.SetActive(const NewActiveState: Boolean)
   ,SetActive$1:function(Self, NewActiveState) {
      Self.FActive$1 = NewActiveState;
   }
   /// procedure TW3StorageDevice.SetDeviceId(NewDeviceId: String; CB: TW3DeviceIdCallback)
   ,SetDeviceId:function(Self, NewDeviceId, CB$16) {
      function DeviceIdFailed() {
         if (CB$16) {
            CB$16(NewDeviceId,false);
         }
      };
      NewDeviceId = Trim$_String_(NewDeviceId);
      if (NewDeviceId!=Self.FIdentifier) {
         if (NewDeviceId.length>0) {
            Self.FIdentifier = NewDeviceId;
            if (CB$16) {
               CB$16(NewDeviceId,true);
            }
         } else {
            DeviceIdFailed();
         }
      } else {
         DeviceIdFailed();
      }
   }
   /// procedure TW3StorageDevice.SetDeviceOptions(NewOptions: TW3StorageDeviceOptions; CB: TW3DeviceOptionsCallback)
   ,SetDeviceOptions:function(Self, NewOptions$1, CB$17) {
      Self.FOptions$5 = NewOptions$1.slice(0);
      if (CB$17) {
         CB$17(Self.FOptions$5.slice(0),true);
      }
   }
   /// procedure TW3StorageDevice.SetName(NewValue: String; CB: TW3DeviceNameCallback)
   ,SetName$1:function(Self, NewValue, CB$18) {
      function NameFailed() {
         if (CB$18) {
            CB$18(NewValue,false);
         }
      };
      NewValue = Trim$_String_(NewValue);
      if (NewValue!=Self.FName) {
         if (NewValue.length>0) {
            Self.FName = NewValue;
            if (CB$18) {
               CB$18(NewValue,true);
            }
         } else {
            NameFailed();
         }
      } else {
         NameFailed();
      }
   }
   ,Destroy:TW3Component.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,SetLastError$1:TW3OwnedErrorObject.SetLastError$1
   ,ObjectLocked$2:TW3OwnedLockedErrorObject.ObjectLocked$2
   ,ObjectUnLocked$2:TW3OwnedLockedErrorObject.ObjectUnLocked$2
   ,Create$44:TW3Component.Create$44
   ,CreateEx:TW3Component.CreateEx
   ,FinalizeObject$:function($){return $.ClassType.FinalizeObject($)}
   ,FreeAfter:TW3Component.FreeAfter
   ,InitializeObject$:function($){return $.ClassType.InitializeObject($)}
   ,CdUp$:function($){return $.ClassType.CdUp.apply($.ClassType, arguments)}
   ,ChDir$:function($){return $.ClassType.ChDir.apply($.ClassType, arguments)}
   ,DirExists$:function($){return $.ClassType.DirExists.apply($.ClassType, arguments)}
   ,Examine$2$:function($){return $.ClassType.Examine$2.apply($.ClassType, arguments)}
   ,Examine$1$:function($){return $.ClassType.Examine$1.apply($.ClassType, arguments)}
   ,FileExists$1$:function($){return $.ClassType.FileExists$1.apply($.ClassType, arguments)}
   ,GetActive$:function($){return $.ClassType.GetActive($)}
   ,GetDeviceId$:function($){return $.ClassType.GetDeviceId.apply($.ClassType, arguments)}
   ,GetDeviceOptions$:function($){return $.ClassType.GetDeviceOptions.apply($.ClassType, arguments)}
   ,GetFileSize$:function($){return $.ClassType.GetFileSize.apply($.ClassType, arguments)}
   ,GetName$:function($){return $.ClassType.GetName.apply($.ClassType, arguments)}
   ,GetPath$1$:function($){return $.ClassType.GetPath$1.apply($.ClassType, arguments)}
   ,GetStorageObjectType$:function($){return $.ClassType.GetStorageObjectType.apply($.ClassType, arguments)}
   ,Load$:function($){return $.ClassType.Load.apply($.ClassType, arguments)}
   ,MakeDir$:function($){return $.ClassType.MakeDir.apply($.ClassType, arguments)}
   ,Mount$:function($){return $.ClassType.Mount.apply($.ClassType, arguments)}
   ,RemoveDir$:function($){return $.ClassType.RemoveDir.apply($.ClassType, arguments)}
   ,Save$:function($){return $.ClassType.Save.apply($.ClassType, arguments)}
   ,SetActive$1$:function($){return $.ClassType.SetActive$1.apply($.ClassType, arguments)}
   ,SetDeviceId$:function($){return $.ClassType.SetDeviceId.apply($.ClassType, arguments)}
   ,SetDeviceOptions$:function($){return $.ClassType.SetDeviceOptions.apply($.ClassType, arguments)}
   ,SetName$1$:function($){return $.ClassType.SetName$1.apply($.ClassType, arguments)}
   ,UnMount$:function($){return $.ClassType.UnMount.apply($.ClassType, arguments)}
};
TW3StorageDevice.$Intf={
   IW3StorageDevice:[TW3StorageDevice.GetActive,TW3StorageDevice.GetName,TW3StorageDevice.GetDeviceOptions,TW3StorageDevice.GetDeviceId,TW3StorageDevice.GetPath$1,TW3StorageDevice.GetFileSize,TW3StorageDevice.FileExists$1,TW3StorageDevice.DirExists,TW3StorageDevice.MakeDir,TW3StorageDevice.RemoveDir,TW3StorageDevice.Examine$1,TW3StorageDevice.ChDir,TW3StorageDevice.CdUp,TW3StorageDevice.GetStorageObjectType,TW3StorageDevice.Load,TW3StorageDevice.Save]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3DeviceAuthenticationData = class (TObject)
var TW3DeviceAuthenticationData = {
   $ClassName:"TW3DeviceAuthenticationData",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.adEndpoint = $.adOAuthAccessToken = $.adPassword = $.adApplicationKey = $.adApplicationSecret = $.adUsername = "";
   }
   ,Destroy:TObject.Destroy
};
/// TNJFileItemType enumeration
var TNJFileItemType = [ "wtFile", "wtFolder" ];
/// TNJFileItemList = class (JObject)
function TNJFileItemList() {
   this.dlItems = [];
}
$Extend(Object,TNJFileItemList,
   {
      "dlPath" : ""
   });

/// TNJFileItem = class (JObject)
function TNJFileItem() {
}
$Extend(Object,TNJFileItem,
   {
      "diFileName" : "",
      "diFileType" : 0,
      "diFileSize" : 0,
      "diFileMode" : "",
      "diCreated" : undefined,
      "diModified" : undefined
   });

/// TInifileValue = record
function Copy$TInifileValue(s,d) {
   d.fvType=s.fvType;
   d.fvName=s.fvName;
   d.fvData=s.fvData;
   return d;
}
function Clone$TInifileValue($) {
   return {
      fvType:$.fvType,
      fvName:$.fvName,
      fvData:$.fvData
   }
}
/// TCustomParser = class (TW3ErrorObject)
var TCustomParser = {
   $ClassName:"TCustomParser",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.FContext = null;
   }
   /// procedure TCustomParser.SetContext(const NewContext: TParserContext)
   ,SetContext:function(Self, NewContext) {
      Self.FContext = NewContext;
   }
   /// function TCustomParser.Parse() : Boolean
   ,Parse:function(Self) {
      var Result = false;
      Result = false;
      TW3ErrorObject.SetLastErrorF(Self,"No parser implemented for class %s",[TObject.ClassName(Self.ClassType)]);
      return Result
   }
   /// constructor TCustomParser.Create(const ParseContext: TParserContext)
   ,Create$70:function(Self, ParseContext) {
      TW3ErrorObject.Create$3(Self);
      Self.FContext = ParseContext;
      return Self
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$3:TW3ErrorObject.Create$3
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,Parse$:function($){return $.ClassType.Parse($)}
   ,Create$70$:function($){return $.ClassType.Create$70.apply($.ClassType, arguments)}
};
TCustomParser.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TInifileParser = class (TCustomParser)
var TInifileParser = {
   $ClassName:"TInifileParser",$Parent:TCustomParser
   ,$Init:function ($) {
      TCustomParser.$Init($);
      $.InifileData = [];
   }
   /// function TInifileParser.ParseGroup() : Boolean
   ,ParseGroup:function(Self) {
      var Result = false;
      var LName$3 = {};
      LName$3.v = "";
      var LCategory = {fvName$1:"",fvValues:[]};
      var LBuffer$5 = null;
      TW3ErrorObject.ClearLastError(Self);
      LBuffer$5 = Self.FContext.FBuffer$3;
      TTextBuffer.ConsumeJunk(LBuffer$5);
      TTextBuffer.ConsumeCRLF(LBuffer$5);
      if (TTextBuffer.Current(LBuffer$5)=="[") {
         if (TTextBuffer.Next(LBuffer$5)) {
            if (TTextBuffer.ReadWord$2(LBuffer$5,LName$3)) {
               LCategory.fvName$1 = LName$3.v;
               Self.InifileData.push(Clone$TIniFileCategory(LCategory));
               if (TTextBuffer.Current(LBuffer$5)=="]") {
                  TTextBuffer.ReadToEOL(LBuffer$5);
                  Result = true;
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Failed to parse group, expected \"[\" not \"%s\" error",[TTextBuffer.Current(LBuffer$5)]);
      }
      return Result
   }
   /// function TInifileParser.ParseValue() : Boolean
   ,ParseValue:function(Self) {
      var Result = false;
      var LName$4 = {v:""},
         LValue$2 = {v:""},
         LBuffer$6 = null;
      TW3ErrorObject.ClearLastError(Self);
      LName$4.v = "";
      LValue$2.v = "";
      LBuffer$6 = Self.FContext.FBuffer$3;
      TTextBuffer.Back(LBuffer$6);
      if (TTextBuffer.ReadWord$2(LBuffer$6,LName$4)) {
         TTextBuffer.ConsumeJunk(LBuffer$6);
         if (TTextBuffer.Current(LBuffer$6)=="=") {
            TTextBuffer.Next(LBuffer$6);
            TTextBuffer.ConsumeJunk(LBuffer$6);
            {var $temp45 = TTextBuffer.Current(LBuffer$6);
               if ($temp45=="\"") {
                  LValue$2.v = TTextBuffer.ReadQuotedString(LBuffer$6);
               }
                else if (($temp45>="A")&&($temp45<="Z")) {
                  if (TTextBuffer.ReadToEOL$1(LBuffer$6,LValue$2)) {
                     LValue$2.v = Trim$_String_(LValue$2.v);
                  } else {
                     TW3ErrorObject.SetLastErrorF(Self,"Failed to parse value [%s], expected number",[LName$4.v]);
                  }
               }
                else if (($temp45>="a")&&($temp45<="z")) {
                  if (TTextBuffer.ReadToEOL$1(LBuffer$6,LValue$2)) {
                     LValue$2.v = Trim$_String_(LValue$2.v);
                  } else {
                     TW3ErrorObject.SetLastErrorF(Self,"Failed to parse value [%s], expected number",[LName$4.v]);
                  }
               }
                else if ($temp45==" ") {
                  if (TTextBuffer.ReadToEOL$1(LBuffer$6,LValue$2)) {
                     LValue$2.v = Trim$_String_(LValue$2.v);
                  } else {
                     TW3ErrorObject.SetLastErrorF(Self,"Failed to parse value [%s], expected number",[LName$4.v]);
                  }
               }
                else if (($temp45>="0")&&($temp45<="9")) {
                  if (TTextBuffer.ReadToEOL$1(LBuffer$6,LValue$2)) {
                     LValue$2.v = Trim$_String_(LValue$2.v);
                  } else {
                     TW3ErrorObject.SetLastErrorF(Self,"Failed to parse value [%s], expected number",[LName$4.v]);
                  }
               }
                else {
                  TW3ErrorObject.SetLastError$(Self,"Failed to parse value, expected quoted string or number error");
               }
            }
            if (!TW3ErrorObject.GetFailed(Self)) {
               TInifileParser.PushValue(Self,LName$4.v,LValue$2.v);
               Result = true;
            }
         } else {
            TW3ErrorObject.SetLastErrorF(Self,"Failed to read value, expected [=] not [%s]",[TTextBuffer.Current(LBuffer$6)]);
         }
      }
      return Result
   }
   /// procedure TInifileParser.PushValue(Name: String; Data: String)
   ,PushValue:function(Self, Name$13, Data$56) {
      var LDataType$1 = {};
      LDataType$1.v = 0;
      var LParsedData,
         LParsedData = undefined;
      var idx$1 = 0,
         FTemp$1 = {};
      FTemp$1.v = false;
      var FTemp$2 = {};
      FTemp$2.v = 0;
      var FTemp$3 = {};
      FTemp$3.v = 0;
      var Value$148 = {fvType:0,fvName:"",fvData:undefined};
      TW3ErrorObject.ClearLastError(Self);
      if (Self.InifileData.length>0) {
         Name$13 = (Trim$_String_(Name$13)).toLocaleLowerCase();
         if (Name$13.length>0) {
            idx$1 = Self.InifileData.length-1;
            if (TString.ResolveDataType(Data$56,LDataType$1)) {
               switch (LDataType$1.v) {
                  case 0 :
                     LParsedData = TVariant.AsString(Data$56);
                     break;
                  case 1 :
                     if (TryStrToBool(Data$56,FTemp$1)) {
                        LParsedData = FTemp$1.v;
                     } else {
                        TW3ErrorObject.SetLastErrorF(Self,"Failed to parse boolean [%s] error",[Data$56]);
                     }
                     break;
                  case 2 :
                  case 4 :
                  case 5 :
                  case 6 :
                  case 7 :
                     if (TryStrToInt(Data$56,FTemp$2)) {
                        LParsedData = FTemp$2.v;
                     } else {
                        TW3ErrorObject.SetLastErrorF(Self,"Failed to parse integer [%s] error",[Data$56]);
                     }
                     break;
                  case 8 :
                  case 9 :
                     if (TryStrToFloat(Data$56,FTemp$3)) {
                        LParsedData = FTemp$3.v;
                     } else {
                        TW3ErrorObject.SetLastErrorF(Self,"Failed to parse integer [%s] error",[Data$56]);
                     }
                     break;
                  case 3 :
                  case 10 :
                     if (TString.GetStringIsQuoted(Data$56)) {
                        LParsedData = TString.UnQuoteString(Data$56);
                     } else {
                        LParsedData = Data$56;
                     }
                     break;
               }
            } else {
               LParsedData = TVariant.AsString(Data$56);
            }
            if (!TW3ErrorObject.GetFailed(Self)) {
               Value$148.fvType = LDataType$1.v;
               Value$148.fvName = Name$13;
               Value$148.fvData = LParsedData;
               Self.InifileData[idx$1].fvValues.push(Clone$TInifileValue(Value$148));
            }
         } else {
            TW3ErrorObject.SetLastError$(Self,"Invalid name [empty] error");
         }
      } else {
         TW3ErrorObject.SetLastError$(Self,"Failed to add value, no category defined");
      }
   }
   /// function TInifileParser.Parse() : Boolean
   ,Parse:function(Self) {
      var Result = false;
      var LBuffer$7 = null;
      TW3ErrorObject.ClearLastError(Self);
      if (Self.FContext!==null) {
         if (TTextBuffer.Empty$2(Self.FContext.FBuffer$3)) {
            TW3ErrorObject.SetLastError$(Self,"Failed to parse inifile data, buffer was empty error");
         } else {
            LBuffer$7 = Self.FContext.FBuffer$3;
            if (TTextBuffer.BOF$3(LBuffer$7)) {
               if (!TTextBuffer.First(LBuffer$7)) {
                  return Result;
               }
            }
            if (TTextBuffer.EOF$3(LBuffer$7)) {
               Result = true;
               return Result;
            }
            do {
               TTextBuffer.ConsumeJunk(LBuffer$7);
               TTextBuffer.ConsumeCRLF(LBuffer$7);
               if (TTextBuffer.EOF$3(LBuffer$7)) {
                  break;
               }
               {var $temp46 = TTextBuffer.Current(LBuffer$7);
                  if ($temp46=="[") {
                     if (!TInifileParser.ParseGroup(Self)) {
                        break;
                     }
                  }
                   else if (($temp46>="A")&&($temp46<="Z")) {
                     if (!TInifileParser.ParseValue(Self)) {
                        break;
                     }
                  }
                   else if (($temp46>="a")&&($temp46<="z")) {
                     if (!TInifileParser.ParseValue(Self)) {
                        break;
                     }
                  }
                   else {
                     TTextBuffer.ConsumeJunk(LBuffer$7);
                     TTextBuffer.ConsumeCRLF(LBuffer$7);
                     if (LBuffer$7.FOffset$3==LBuffer$7.FData$1.length) {
                        break;
                     }
                     if (!TTextBuffer.EOF$3(LBuffer$7)) {
                        TW3ErrorObject.SetLastErrorF(Self,"Parsing failed, unexpected character [%s] error",[TTextBuffer.Current(LBuffer$7)]);
                        break;
                     }
                     break;
                  }
               }
               TTextBuffer.Next(LBuffer$7);
            } while (!TTextBuffer.EOF$3(LBuffer$7));
            Result = TW3ErrorObject.GetFailed(Self)==false;
         }
      } else {
         TW3ErrorObject.SetLastError$(Self,"Failed to parse inifile data, context was nil error");
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$3:TW3ErrorObject.Create$3
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,Parse$:function($){return $.ClassType.Parse($)}
   ,Create$70:TCustomParser.Create$70
};
TInifileParser.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TIniFileCategory = record
function Copy$TIniFileCategory(s,d) {
   d.fvName$1=s.fvName$1;
   d.fvValues=s.fvValues;
   return d;
}
function Clone$TIniFileCategory($) {
   return {
      fvName$1:$.fvName$1,
      fvValues:$.fvValues
   }
}
/// TCustomInifile = class (TObject)
var TCustomInifile = {
   $ClassName:"TCustomInifile",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FData = [];
   }
   /// anonymous TSourceMethodSymbol
   ,a$99:function(Self) {
      return (Self.FData.length==0);
   }
   /// function TCustomInifile.CategoryExists(Name: String) : Boolean
   ,CategoryExists:function(Self, Name$14) {
      return TCustomInifile.GetCategoryIndex(Self,Name$14)>=0;
   }
   /// procedure TCustomInifile.Clear()
   ,Clear$4:function(Self) {
      if (Self.FData.length>0) {
         Self.FData.length=0;
      }
   }
   /// function TCustomInifile.FindOrAddCategory(Name: String) : Integer
   ,FindOrAddCategory:function(Self, Name$15) {
      var Result = 0;
      var LCat = {fvName$1:"",fvValues:[]};
      Name$15 = Trim$_String_(Name$15);
      if (Name$15.length>0) {
         Result = TCustomInifile.GetCategoryIndex(Self,Name$15);
         if (Result<0) {
            LCat.fvName$1 = Trim$_String_(Name$15);
            Self.FData.push(Clone$TIniFileCategory(LCat));
            Result = Self.FData.length-1;
         }
      } else {
         Result = -1;
      }
      return Result
   }
   /// function TCustomInifile.FindOrAddValue(CatIndex: Integer; Name: String) : Integer
   ,FindOrAddValue:function(Self, CatIndex, Name$16) {
      var Result = 0;
      var x$34 = 0;
      var vrec = {fvType:0,fvName:"",fvData:undefined};
      Result = -1;
      Name$16 = Trim$_String_(Name$16);
      if (Name$16.length>0) {
         var $temp47;
         for(x$34=0,$temp47=Self.FData[CatIndex].fvValues.length;x$34<$temp47;x$34++) {
            if ((Self.FData[CatIndex].fvValues[x$34].fvName).toLocaleLowerCase()==(Name$16).toLocaleLowerCase()) {
               Result = x$34;
               break;
            }
         }
         if (Result<0) {
            vrec.fvName = Name$16;
            Self.FData[CatIndex].fvValues.push(Clone$TInifileValue(vrec));
            Result = Self.FData[CatIndex].fvValues.length-1;
         }
      }
      return Result
   }
   /// procedure TCustomInifile.FromJSON(const Content: String)
   ,FromJSON$1:function(Self, Content) {
      var obj;
      TCustomInifile.Clear$4(Self);
      if (Content.length>0) {
         try {
            obj = JSON.parse(Content);
            Self.FData = obj;
         } catch ($e) {
            var e$19 = $W($e);
            TCustomInifile.Clear$4(Self);
            throw Exception.Create($New(EInifile),e$19.FMessage);
         }
      }
   }
   /// procedure TCustomInifile.FromString(const Content: String)
   ,FromString:function(Self, Content$1) {
      if (Content$1.length>0) {
         TCustomInifile.ParseContent(Self,Content$1);
      } else {
         TCustomInifile.Clear$4(Self);
      }
   }
   /// function TCustomInifile.GetCategories() : TStrArray
   ,GetCategories:function(Self) {
      var Result = [];
      var x$35 = 0;
      var $temp48;
      for(x$35=0,$temp48=Self.FData.length;x$35<$temp48;x$35++) {
         Result.push(Self.FData[x$35].fvName$1);
      }
      return Result
   }
   /// function TCustomInifile.GetCategoryIndex(Name: String) : Integer
   ,GetCategoryIndex:function(Self, Name$17) {
      var Result = 0;
      var x$36 = 0;
      Result = -1;
      Name$17 = (Trim$_String_(Name$17)).toLocaleLowerCase();
      if (Name$17.length>0) {
         var $temp49;
         for(x$36=0,$temp49=Self.FData.length;x$36<$temp49;x$36++) {
            if ((Self.FData[x$36].fvName$1).toLocaleLowerCase()==Name$17) {
               Result = x$36;
               break;
            }
         }
      }
      return Result
   }
   /// function TCustomInifile.GetValues(Category: String) : TStrArray
   ,GetValues:function(Self, Category) {
      var Result = [];
      var CatIndex$1 = 0,
         x$37 = 0;
      CatIndex$1 = TCustomInifile.GetCategoryIndex(Self,Category);
      if (CatIndex$1>=0) {
         var $temp50;
         for(x$37=0,$temp50=Self.FData[CatIndex$1].fvValues.length;x$37<$temp50;x$37++) {
            Result.push(Self.FData[CatIndex$1].fvValues[x$37].fvName);
         }
      }
      return Result
   }
   /// procedure TCustomInifile.LoadFromFile(Filename: String)
   ,LoadFromFile:function(Self, Filename$7) {
      var LStream = null;
      LStream = TCustomFileStream.Create$32$($New(TFileStream),Filename$7,0);
      try {
         TCustomInifile.LoadFromStream$4(Self,LStream);
      } finally {
         TObject.Free(LStream);
      }
   }
   /// procedure TCustomInifile.LoadFromStream(const Stream: TStream)
   ,LoadFromStream$4:function(Self, Stream$7) {
      var LBytesToRead$12 = 0,
         LData$2 = [],
         LText$7 = "";
      if (Stream$7!==null) {
         LBytesToRead$12 = TStream.GetSize$(Stream$7)-TStream.GetPosition$(Stream$7);
         if (LBytesToRead$12>0) {
            LData$2 = TStream.Read$1$(Stream$7,TStream.GetSize$(Stream$7));
            LText$7 = TDatatype.BytesToString$1(TDatatype,LData$2);
            TCustomInifile.ParseContent(Self,LText$7);
         } else {
            throw Exception.Create($New(EInifile),"Failed to load data, stream was empty [EOF?] error");
         }
      } else {
         throw Exception.Create($New(EInifile),"Failed to load data, stream was NIL error");
      }
   }
   /// procedure TCustomInifile.ParseContent(FileContent: String)
   ,ParseContent:function(Self, FileContent) {
      var LData$3 = "";
      var LContext = null,
         LParser$1 = null,
         obj$1;
      TCustomInifile.Clear$4(Self);
      FileContent = Trim$_String_(FileContent);
      if (FileContent.length>0) {
         LContext = TParserContext.Create$73$($New(TParserContext),FileContent);
         LParser$1 = TCustomParser.Create$70$($New(TInifileParser),LContext);
         try {
            if (TCustomParser.Parse$(LParser$1)) {
               LData$3 = JSON.stringify(LParser$1.InifileData);
            } else {
               throw Exception.Create($New(EInifile),LParser$1.FLastError);
            }
         } finally {
            TObject.Free(LParser$1);
         }
         if (LData$3.length>0) {
            obj$1 = JSON.parse(LData$3);
            Self.FData = obj$1;
         }
      }
   }
   /// function TCustomInifile.Read(Category: String; ValueName: String) : Variant
   ,Read$6:function(Self, Category$1, ValueName) {
      var Result = undefined;
      var CatIndex$2 = 0,
         ValIndex = 0;
      CatIndex$2 = TCustomInifile.FindOrAddCategory(Self,Category$1);
      if (CatIndex$2>=0) {
         ValIndex = TCustomInifile.FindOrAddValue(Self,CatIndex$2,ValueName);
         if (ValIndex>=0) {
            Result = Self.FData[CatIndex$2].fvValues[ValIndex].fvData;
         } else {
            Result = undefined;
         }
      } else {
         Result = undefined;
      }
      return Result
   }
   /// function TCustomInifile.ReadBoolean(Category: String; ValueName: String; Default: Boolean) : Boolean
   ,ReadBoolean$2:function(Self, Category$2, ValueName$1, Default) {
      var Result = false;
      var Data$57;
      Data$57 = TCustomInifile.Read$6(Self,Category$2,ValueName$1);
      if (Data$57==undefined) {
         Result = Default;
      } else {
         Result = TVariant.AsBool(Data$57);
      }
      return Result
   }
   /// function TCustomInifile.ReadFloat(Category: String; ValueName: String; Default: Float) : Float
   ,ReadFloat$2:function(Self, Category$3, ValueName$2, Default$1) {
      var Result = 0;
      var Data$58;
      Data$58 = TCustomInifile.Read$6(Self,Category$3,ValueName$2);
      if (Data$58==undefined) {
         Result = Default$1;
      } else {
         Result = TVariant.AsFloat(Data$58);
      }
      return Result
   }
   /// function TCustomInifile.ReadInteger(Category: String; ValueName: String; Default: Integer) : Integer
   ,ReadInteger$2:function(Self, Category$4, ValueName$3, Default$2) {
      var Result = 0;
      var Data$59;
      Data$59 = TCustomInifile.Read$6(Self,Category$4,ValueName$3);
      if (Data$59==undefined) {
         Result = Default$2;
      } else {
         Result = TVariant.AsInteger(Data$59);
      }
      return Result
   }
   /// function TCustomInifile.ReadString(Category: String; ValueName: String; Default: String) : String
   ,ReadString$4:function(Self, Category$5, ValueName$4, Default$3) {
      var Result = "";
      var Data$60;
      Data$60 = TCustomInifile.Read$6(Self,Category$5,ValueName$4);
      if (Data$60==undefined) {
         Result = Default$3;
      } else {
         Result = TVariant.AsString(Data$60);
      }
      return Result
   }
   /// procedure TCustomInifile.SaveToFile(Filename: String)
   ,SaveToFile:function(Self, Filename$8) {
      var LStream$1 = null;
      LStream$1 = TCustomFileStream.Create$32$($New(TFileStream),Filename$8,1);
      try {
         TCustomInifile.SaveToStream$4(Self,LStream$1);
      } finally {
         TObject.Free(LStream$1);
      }
   }
   /// procedure TCustomInifile.SaveToStream(const Stream: TStream)
   ,SaveToStream$4:function(Self, Stream$8) {
      var LText$8 = "",
         x$38 = 0;
      var y$2 = 0;
      if (Stream$8!==null) {
         LText$8 = "";
         if (Self.FData.length>0) {
            var $temp51;
            for(x$38=0,$temp51=Self.FData.length;x$38<$temp51;x$38++) {
               LText$8+=("["+Self.FData[x$38].fvName$1.toString()+"]")+"\r\n";
               if (Self.FData[x$38].fvValues.length>0) {
                  var $temp52;
                  for(y$2=0,$temp52=Self.FData[x$38].fvValues.length;y$2<$temp52;y$2++) {
                     LText$8+=Self.FData[x$38].fvValues[y$2].fvName;
                     LText$8+=" = ";
                     switch (Self.FData[x$38].fvValues[y$2].fvType) {
                        case 1 :
                           LText$8+=BoolToStr((Self.FData[x$38].fvValues[y$2].fvData?true:false));
                           break;
                        case 2 :
                        case 4 :
                        case 5 :
                        case 6 :
                        case 7 :
                           LText$8+=(parseInt(Self.FData[x$38].fvValues[y$2].fvData,10)).toString();
                           break;
                        case 8 :
                        case 9 :
                           LText$8+=FloatToStr$_Float_(Number(Self.FData[x$38].fvValues[y$2].fvData));
                           break;
                        case 3 :
                        case 10 :
                           LText$8+=TString.QuoteString(FloatToStr$_Float_(Number(Self.FData[x$38].fvValues[y$2].fvData)));
                           break;
                     }
                     LText$8+="\r\n";
                  }
               }
            }
            TStream.Write$2$(Stream$8,TDatatype.StringToBytes$1(TDatatype,LText$8));
         }
      } else {
         throw Exception.Create($New(EInifile),"Failed to save data, stream was NIL error");
      }
   }
   /// function TCustomInifile.ToJSON() : String
   ,ToJSON$1:function(Self) {
      return JSON.stringify(Self.FData);
   }
   /// function TCustomInifile.ToString() : String
   ,ToString$5:function(Self) {
      return JSON.stringify(Self.FData);
   }
   /// function TCustomInifile.ValueExists(Category: String; Name: String) : Boolean
   ,ValueExists:function(Self, Category$6, Name$18) {
      var Result = false;
      var CIDX = 0,
         y$3 = 0;
      CIDX = TCustomInifile.GetCategoryIndex(Self,Category$6);
      if (CIDX>=0) {
         Name$18 = (Trim$_String_(Name$18)).toLocaleLowerCase();
         if (Name$18.length>0) {
            var $temp53;
            for(y$3=0,$temp53=Self.FData[CIDX].fvValues.length;y$3<$temp53;y$3++) {
               Result = (Self.FData[CIDX].fvValues[y$3].fvName).toLocaleLowerCase()==Name$18;
               if (Result) {
                  break;
               }
            }
         }
      }
      return Result
   }
   /// procedure TCustomInifile.Write(Category: String; ValueName: String; Data: Variant)
   ,Write$6:function(Self, Category$7, ValueName$5, Data$61) {
      var CatIndex$3 = 0,
         ValIndex$1 = 0;
      CatIndex$3 = TCustomInifile.FindOrAddCategory(Self,Category$7);
      if (CatIndex$3>=0) {
         ValIndex$1 = TCustomInifile.FindOrAddValue(Self,CatIndex$3,ValueName$5);
         if (ValIndex$1>=0) {
            Self.FData[CatIndex$3].fvValues[ValIndex$1].fvData = Data$61;
         }
      }
   }
   /// procedure TCustomInifile.WriteBoolean(Category: String; ValueName: String; Data: Boolean)
   ,WriteBoolean$1:function(Self, Category$8, ValueName$6, Data$62) {
      TCustomInifile.Write$6(Self,Category$8,ValueName$6,Data$62);
   }
   /// procedure TCustomInifile.WriteFloat(Category: String; ValueName: String; Data: Float)
   ,WriteFloat$1:function(Self, Category$9, ValueName$7, Data$63) {
      TCustomInifile.Write$6(Self,Category$9,ValueName$7,Data$63);
   }
   /// procedure TCustomInifile.WriteInteger(Category: String; ValueName: String; Data: Integer)
   ,WriteInteger$1:function(Self, Category$10, ValueName$8, Data$64) {
      TCustomInifile.Write$6(Self,Category$10,ValueName$8,Data$64);
   }
   /// procedure TCustomInifile.WriteString(Category: String; ValueName: String; Data: String)
   ,WriteString$3:function(Self, Category$11, ValueName$9, Data$65) {
      TCustomInifile.Write$6(Self,Category$11,ValueName$9,Data$65);
   }
   ,Destroy:TObject.Destroy
};
/// TInifile = class (TCustomInifile)
var TInifile = {
   $ClassName:"TInifile",$Parent:TCustomInifile
   ,$Init:function ($) {
      TCustomInifile.$Init($);
   }
   ,Destroy:TObject.Destroy
};
/// EInifile = class (EW3Exception)
var EInifile = {
   $ClassName:"EInifile",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TTextBufferBookmark = class (TObject)
var TTextBufferBookmark = {
   $ClassName:"TTextBufferBookmark",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.bbCol = $.bbRow = $.bbOffset = 0;
   }
   /// function TTextBufferBookmark.Equals(const ThisMark: TTextBufferBookmark) : Boolean
   ,Equals$3:function(Self, ThisMark) {
      return ThisMark!==null&&ThisMark!==Self&&Self.bbOffset==ThisMark.bbOffset&&Self.bbCol==ThisMark.bbCol&&Self.bbRow==ThisMark.bbRow;
   }
   ,Destroy:TObject.Destroy
};
/// TTextBuffer = class (TW3ErrorObject)
var TTextBuffer = {
   $ClassName:"TTextBuffer",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.FCol = $.FLength = $.FOffset$3 = $.FRow = 0;
      $.FData$1 = "";
   }
   /// function TTextBuffer.Back() : Boolean
   ,Back:function(Self) {
      var Result = false;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      Result = !TTextBuffer.Empty$2(Self);
      if (Result) {
         Result = Self.FOffset$3>1;
         if (Result) {
            --Self.FOffset$3;
         } else {
            TW3ErrorObject.SetLastError$(Self,$R[56]);
         }
      } else {
         TW3ErrorObject.SetLastError$(Self,$R[55]);
      }
      return Result
   }
   /// function TTextBuffer.BOF() : Boolean
   ,BOF$3:function(Self) {
      var Result = false;
      if (!TTextBuffer.Empty$2(Self)) {
         Result = Self.FOffset$3<1;
      }
      return Result
   }
   /// function TTextBuffer.Bookmark() : TTextBufferBookmark
   ,Bookmark$1:function(Self) {
      var Result = null;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TTextBuffer.Empty$2(Self)) {
         throw Exception.Create($New(ETextBuffer),"Failed to bookmark location, buffer is empty error");
      } else {
         Result = TObject.Create($New(TTextBufferBookmark));
         Result.bbOffset = Self.FOffset$3;
         Result.bbCol = Self.FCol;
         Result.bbRow = Self.FRow;
      }
      return Result
   }
   /// function TTextBuffer.BracketEnter() : Boolean
   ,BracketEnter:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&Self.FData$1.charAt(Self.FOffset$3-1)=="[";
   }
   /// function TTextBuffer.BracketLeave() : Boolean
   ,BracketLeave:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&Self.FData$1.charAt(Self.FOffset$3-1)=="]";
   }
   /// procedure TTextBuffer.Clear()
   ,Clear$5:function(Self) {
      Self.FData$1 = "";
      Self.FOffset$3 = -1;
      Self.FLength = 0;
      Self.FCol = -1;
      Self.FRow = -1;
   }
   /// function TTextBuffer.Colon() : Boolean
   ,Colon:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&Self.FData$1.charAt(Self.FOffset$3-1)==":";
   }
   /// function TTextBuffer.Compare(const CompareText: String; const CaseSensitive: Boolean) : Boolean
   ,Compare:function(Self, CompareText$1, CaseSensitive) {
      var Result = false;
      var LenToRead = 0,
         ReadData = {v:""};
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TTextBuffer.Empty$2(Self)) {
         TW3ErrorObject.SetLastError$(Self,$R[55]);
      } else {
         if (TTextBuffer.BOF$3(Self)) {
            if (!TTextBuffer.First(Self)) {
               return Result;
            }
         }
         if (TTextBuffer.EOF$3(Self)) {
            TW3ErrorObject.SetLastError$(Self,$R[57]);
            return Result;
         }
         LenToRead = CompareText$1.length;
         if (LenToRead>0) {
            ReadData.v = "";
            if (TTextBuffer.Peek$1(Self,LenToRead,ReadData)) {
               if (CaseSensitive) {
                  Result = ReadData.v==CompareText$1;
               } else {
                  Result = (ReadData.v).toLocaleLowerCase()==(CompareText$1).toLocaleLowerCase();
               }
            }
         } else {
            TW3ErrorObject.SetLastError$(Self,$R[60]);
         }
      }
      return Result
   }
   /// function TTextBuffer.ConditionEnter() : Boolean
   ,ConditionEnter:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&Self.FData$1.charAt(Self.FOffset$3-1)=="(";
   }
   /// function TTextBuffer.ConditionLeave() : Boolean
   ,ConditionLeave:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&Self.FData$1.charAt(Self.FOffset$3-1)==")";
   }
   /// procedure TTextBuffer.ConsumeCRLF()
   ,ConsumeCRLF:function(Self) {
      if (!TTextBuffer.Empty$2(Self)) {
         if (TTextBuffer.BOF$3(Self)) {
            if (!TTextBuffer.First(Self)) {
               return;
            }
         }
         if (TTextBuffer.EOF$3(Self)) {
            TW3ErrorObject.SetLastError$(Self,$R[57]);
            return;
         }
         if (Self.FData$1.charAt(Self.FOffset$3-1)=="\r") {
            if (Self.FData$1.charAt(Self.FOffset$3)=="\n") {
               (Self.FOffset$3+= 2);
            } else {
               ++Self.FOffset$3;
            }
            ++Self.FRow;
            Self.FCol = 0;
         }
      }
   }
   /// procedure TTextBuffer.ConsumeJunk()
   ,ConsumeJunk:function(Self) {
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TTextBuffer.Empty$2(Self)) {
         TW3ErrorObject.SetLastError$(Self,$R[55]);
      } else {
         if (TTextBuffer.BOF$3(Self)) {
            if (!TTextBuffer.First(Self)) {
               return;
            }
         }
         if (TTextBuffer.EOF$3(Self)) {
            TW3ErrorObject.SetLastError$(Self,$R[57]);
            return;
         }
         do {
            {var $temp54 = TTextBuffer.Current(Self);
               if ($temp54==" ") {
                  /* null */
               }
                else if ($temp54=="\"") {
                  break;
               }
                else if ($temp54=="\b") {
                  /* null */
               }
                else if ($temp54=="\t") {
                  /* null */
               }
                else if ($temp54=="\/") {
                  if (TTextBuffer.Compare(Self,"\/*",false)) {
                     if (TTextBuffer.ReadTo$2(Self,"*\/")) {
                        (Self.FOffset$3+= 2);
                        continue;
                     } else {
                        TW3ErrorObject.SetLastError$(Self,$R[58]);
                     }
                  } else {
                     if (TTextBuffer.Compare(Self,"\/\/",false)) {
                        if (TTextBuffer.ReadToEOL(Self)) {
                           continue;
                        } else {
                           TW3ErrorObject.SetLastError$(Self,$R[59]);
                        }
                     }
                  }
               }
                else if ($temp54=="(") {
                  if (TTextBuffer.Compare(Self,"(*",false)&&(!TTextBuffer.Compare(Self,"(*)",false))) {
                     if (TTextBuffer.ReadTo$2(Self,"*)")) {
                        (Self.FOffset$3+= 2);
                        continue;
                     } else {
                        TW3ErrorObject.SetLastError$(Self,$R[58]);
                     }
                  } else {
                     break;
                  }
               }
                else if ($temp54=="\r") {
                  if (Self.FData$1.charAt(Self.FOffset$3)=="\n") {
                     (Self.FOffset$3+= 2);
                  } else {
                     ++Self.FOffset$3;
                  }
                  continue;
               }
                else if ($temp54=="\n") {
                  ++Self.FOffset$3;
                  continue;
               }
                else {
                  break;
               }
            }
            if (!TTextBuffer.Next(Self)) {
               break;
            }
         } while (!TTextBuffer.EOF$3(Self));
      }
   }
   /// constructor TTextBuffer.Create(const BufferText: String)
   ,Create$71:function(Self, BufferText) {
      TW3ErrorObject.Create$3(Self);
      if (BufferText.length>0) {
         TTextBuffer.LoadBufferText(Self,BufferText);
      } else {
         TTextBuffer.Clear$5(Self);
      }
      return Self
   }
   /// function TTextBuffer.CrLf() : Boolean
   ,CrLf:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&Self.FData$1.charAt(Self.FOffset$3-1)=="\r"&&Self.FData$1.charAt(Self.FOffset$3)=="\n";
   }
   /// function TTextBuffer.Current() : Char
   ,Current:function(Self) {
      var Result = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TTextBuffer.Empty$2(Self)) {
         TW3ErrorObject.SetLastError$(Self,$R[55]);
         Result = "";
      } else {
         if (Self.FOffset$3>=1) {
            if (Self.FOffset$3<=Self.FData$1.length) {
               Result = Self.FData$1.charAt(Self.FOffset$3-1);
            } else {
               TW3ErrorObject.SetLastError$(Self,$R[57]);
               Result = "";
            }
         } else {
            TW3ErrorObject.SetLastError$(Self,$R[56]);
            Result = "";
         }
      }
      return Result
   }
   /// destructor TTextBuffer.Destroy()
   ,Destroy:function(Self) {
      TTextBuffer.Clear$5(Self);
      TW3ErrorObject.Destroy(Self);
   }
   /// function TTextBuffer.Empty() : Boolean
   ,Empty$2:function(Self) {
      return Self.FLength<1;
   }
   /// function TTextBuffer.EOF() : Boolean
   ,EOF$3:function(Self) {
      var Result = false;
      if (!TTextBuffer.Empty$2(Self)) {
         Result = Self.FOffset$3>Self.FData$1.length;
      }
      return Result
   }
   /// function TTextBuffer.Equal() : Boolean
   ,Equal:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&Self.FData$1.charAt(Self.FOffset$3-1)=="=";
   }
   /// function TTextBuffer.First() : Boolean
   ,First:function(Self) {
      var Result = false;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TTextBuffer.Empty$2(Self)) {
         TW3ErrorObject.SetLastError$(Self,$R[55]);
      } else {
         Self.FOffset$3 = 1;
         Result = true;
      }
      return Result
   }
   /// function TTextBuffer.GetCurrentLocation() : TTextBufferBookmark
   ,GetCurrentLocation:function(Self) {
      var Result = null;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TTextBuffer.Empty$2(Self)) {
         throw Exception.Create($New(ETextBuffer),"Failed to return position, buffer is empty error");
      } else {
         Result = TObject.Create($New(TTextBufferBookmark));
         Result.bbOffset = Self.FOffset$3;
         Result.bbCol = Self.FCol;
         Result.bbRow = Self.FRow;
      }
      return Result
   }
   /// procedure TTextBuffer.Inject(const TextToInject: String)
   ,Inject:function(Self, TextToInject) {
      var LSeg1 = "",
         LSeg2 = "";
      LSeg1 = Self.FData$1.substr(0,Self.FOffset$3);
      LSeg2 = Self.FData$1.substr((Self.FOffset$3+1)-1);
      Self.FData$1 = LSeg1+TextToInject+LSeg2;
   }
   /// function TTextBuffer.Last() : Boolean
   ,Last:function(Self) {
      var Result = false;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TTextBuffer.Empty$2(Self)) {
         TW3ErrorObject.SetLastError$(Self,$R[55]);
      } else {
         Self.FOffset$3 = Self.FData$1.length;
         Result = true;
      }
      return Result
   }
   /// function TTextBuffer.Less() : Boolean
   ,Less:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&Self.FData$1.charAt(Self.FOffset$3-1)=="<";
   }
   /// procedure TTextBuffer.LoadBufferText(const NewBuffer: String)
   ,LoadBufferText:function(Self, NewBuffer$1) {
      var TempLen = 0;
      TTextBuffer.Clear$5(Self);
      TempLen = NewBuffer$1.length;
      if (TempLen>0) {
         Self.FData$1 = NewBuffer$1;
         Self.FOffset$3 = 0;
         Self.FCol = 0;
         Self.FRow = 0;
         Self.FLength = TempLen;
      }
   }
   /// function TTextBuffer.More() : Boolean
   ,More:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&Self.FData$1.charAt(Self.FOffset$3-1)==">";
   }
   /// function TTextBuffer.Next() : Boolean
   ,Next:function(Self) {
      var Result = false;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TTextBuffer.Empty$2(Self)) {
         TW3ErrorObject.SetLastError$(Self,$R[55]);
      } else {
         if (TTextBuffer.BOF$3(Self)) {
            if (!TTextBuffer.First(Self)) {
               return Result;
            }
         }
         if (TTextBuffer.EOF$3(Self)) {
            TW3ErrorObject.SetLastError$(Self,$R[57]);
            return Result;
         }
         ++Self.FOffset$3;
         ++Self.FCol;
         if (Self.FOffset$3<Self.FData$1.length) {
            if (Self.FData$1.charAt(Self.FOffset$3-1)=="\r") {
               ++Self.FRow;
               Self.FCol = 0;
               if (Self.FData$1.charAt(Self.FOffset$3)=="\n") {
                  (Self.FOffset$3+= 2);
               } else {
                  ++Self.FOffset$3;
               }
            }
         }
         Result = true;
      }
      return Result
   }
   /// function TTextBuffer.NextLine() : Boolean
   ,NextLine:function(Self) {
      var Result = false;
      var ThisRow = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (!TTextBuffer.Empty$2(Self)) {
         TTextBuffer.ConsumeJunk(Self);
         if (!TTextBuffer.EOF$3(Self)) {
            ThisRow = Self.FRow;
            while (Self.FRow==ThisRow) {
               TTextBuffer.Next(Self);
               if (TTextBuffer.EOF$3(Self)) {
                  break;
               }
            }
            Result = Self.FRow!=ThisRow&&(!TTextBuffer.EOF$3(Self));
         }
      }
      return Result
   }
   /// function TTextBuffer.NextNoCrLf() : Boolean
   ,NextNoCrLf:function(Self) {
      var Result = false;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TTextBuffer.Empty$2(Self)) {
         TW3ErrorObject.SetLastError$(Self,$R[55]);
      } else {
         Result = Self.FOffset$3<=Self.FData$1.length;
         if (Result) {
            ++Self.FOffset$3;
            if (!function(v$){return ((v$=="\r")||(v$=="\n"))}(Self.FData$1.charAt(Self.FOffset$3-1))) {
               ++Self.FCol;
            }
         } else {
            TW3ErrorObject.SetLastError$(Self,$R[57]);
         }
      }
      return Result
   }
   /// function TTextBuffer.NextNonControlChar(const CompareWith: Char) : Boolean
   ,NextNonControlChar:function(Self, CompareWith) {
      var Result = {v:false};
      try {
         var Mark = null;
         if (TW3ErrorObject.GetFailed(Self)) {
            TW3ErrorObject.ClearLastError(Self);
         }
         if (TTextBuffer.Empty$2(Self)) {
            TW3ErrorObject.SetLastError$(Self,$R[55]);
         } else {
            if (TTextBuffer.BOF$3(Self)) {
               if (!TTextBuffer.First(Self)) {
                  return Result.v;
               }
            }
            if (TTextBuffer.EOF$3(Self)) {
               TW3ErrorObject.SetLastError$(Self,$R[57]);
               return Result.v;
            }
            Mark = TTextBuffer.Bookmark$1(Self);
            try {
               do {
                  if (!function(v$){return ((v$==" ")||(v$=="\r")||(v$=="\n")||(v$=="\t"))}(TTextBuffer.Current(Self))) {
                     break;
                  }
                  TTextBuffer.Next(Self);
               } while (!TTextBuffer.EOF$3(Self));
               Result.v = (TTextBuffer.Current(Self)).toLowerCase()==(CompareWith).toLowerCase();
            } finally {
               TTextBuffer.Restore(Self,Mark);
            }
         }
      } finally {return Result.v}
   }
   /// function TTextBuffer.NextNonControlText(const CompareWith: String) : Boolean
   ,NextNonControlText:function(Self, CompareWith$1) {
      var Result = false;
      var Mark$1 = null;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TTextBuffer.Empty$2(Self)) {
         TW3ErrorObject.SetLastError$(Self,$R[55]);
      } else {
         if (TTextBuffer.BOF$3(Self)) {
            if (!TTextBuffer.First(Self)) {
               return Result;
            }
         }
         if (TTextBuffer.EOF$3(Self)) {
            TW3ErrorObject.SetLastError$(Self,$R[57]);
            return Result;
         }
         Mark$1 = TTextBuffer.Bookmark$1(Self);
         try {
            do {
               if (!function(v$){return ((v$==" ")||(v$=="\r")||(v$=="\n")||(v$=="\t"))}(TTextBuffer.Current(Self))) {
                  break;
               }
               TTextBuffer.Next(Self);
            } while (!TTextBuffer.EOF$3(Self));
            if (!TTextBuffer.EOF$3(Self)) {
               Result = TTextBuffer.Compare(Self,CompareWith$1,false);
            }
         } finally {
            TTextBuffer.Restore(Self,Mark$1);
         }
      }
      return Result
   }
   /// function TTextBuffer.Numeric() : Boolean
   ,Numeric:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&((Self.FData$1.charAt(Self.FOffset$3-1)>="0")&&(Self.FData$1.charAt(Self.FOffset$3-1)<="9"));
   }
   /// function TTextBuffer.Peek(CharCount: Integer; var TextRead: String) : Boolean
   ,Peek$1:function(Self, CharCount, TextRead) {
      var Result = false;
      var Mark$2 = null;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      Result = !TTextBuffer.Empty$2(Self);
      if (Result) {
         Result = !TTextBuffer.EOF$3(Self);
         if (Result) {
            Result = false;
            SetLength(TextRead,0);
            Mark$2 = TTextBuffer.Bookmark$1(Self);
            try {
               while (CharCount>0) {
                  TextRead.v = TextRead.v+TTextBuffer.Current(Self);
                  if (!TTextBuffer.Next(Self)) {
                     break;
                  }
                  --CharCount;
               }
            } finally {
               TTextBuffer.Restore(Self,Mark$2);
            }
            Result = TextRead.v.length>0;
         } else {
            TW3ErrorObject.SetLastError$(Self,$R[57]);
         }
      } else {
         TW3ErrorObject.SetLastError$(Self,$R[55]);
      }
      return Result
   }
   /// function TTextBuffer.Peek() : Char
   ,Peek:function(Self) {
      var Result = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TTextBuffer.Empty$2(Self)) {
         Result = "";
         TW3ErrorObject.SetLastError$(Self,$R[55]);
      } else {
         if (Self.FOffset$3<Self.FData$1.length) {
            Result = Self.FData$1.charAt(Self.FOffset$3);
         } else {
            Result = "";
            TW3ErrorObject.SetLastError$(Self,$R[57]);
         }
      }
      return Result
   }
   /// function TTextBuffer.Pipe() : Boolean
   ,Pipe:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&Self.FData$1.charAt(Self.FOffset$3-1)=="|";
   }
   /// function TTextBuffer.Ptr() : Boolean
   ,Ptr:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&Self.FData$1.charAt(Self.FOffset$3-1)=="^";
   }
   /// function TTextBuffer.Punctum() : Boolean
   ,Punctum:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&Self.FData$1.charAt(Self.FOffset$3-1)==".";
   }
   /// function TTextBuffer.Question() : Boolean
   ,Question:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&Self.FData$1.charAt(Self.FOffset$3-1)=="?";
   }
   /// function TTextBuffer.Read() : Char
   ,Read$8:function(Self) {
      var Result = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TTextBuffer.Empty$2(Self)) {
         Result = "";
      } else {
         Result = TTextBuffer.Current(Self);
         TTextBuffer.Next(Self);
      }
      return Result
   }
   /// function TTextBuffer.Read(var Fragment: Char) : Boolean
   ,Read$7:function(Self, Fragment) {
      var Result = false;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TTextBuffer.Empty$2(Self)) {
         Result = false;
         Fragment.v = "";
         TW3ErrorObject.SetLastError$(Self,"Buffer is empty error");
      } else {
         Result = Self.FOffset$3<=Self.FData$1.length;
         if (Result) {
            Fragment.v = Self.FData$1.charAt(Self.FOffset$3-1);
            ++Self.FOffset$3;
         } else {
            Fragment.v = "";
            TW3ErrorObject.SetLastError$(Self,"Offset at BOF error");
         }
      }
      return Result
   }
   /// function TTextBuffer.ReadCommaList(var List: TStrArray) : Boolean
   ,ReadCommaList:function(Self, List) {
      var Result = false;
      var LTemp$16 = "";
      var LValue$3 = "";
      List.v.length=0;
      if (!TTextBuffer.Empty$2(Self)) {
         TTextBuffer.ConsumeJunk(Self);
         while (!TTextBuffer.EOF$3(Self)) {
            {var $temp55 = TTextBuffer.Current(Self);
               if ($temp55=="\t") {
                  /* null */
               }
                else if ($temp55=="\r") {
                  TTextBuffer.ConsumeCRLF(Self)               }
                else if ($temp55=="\n") {
                  TTextBuffer.ConsumeCRLF(Self)               }
                else if ($temp55=="") {
                  break;
               }
                else if ($temp55==";") {
                  Result = true;
                  break;
               }
                else if ($temp55=="\"") {
                  LValue$3 = TTextBuffer.ReadQuotedString(Self);
                  if (LValue$3.length>0) {
                     List.v.push(LValue$3);
                     LValue$3 = "";
                  }
               }
                else if ($temp55==",") {
                  LTemp$16 = Trim$_String_(LTemp$16);
                  if (LTemp$16.length>0) {
                     List.v.push(LTemp$16);
                     LTemp$16 = "";
                  }
               }
                else {
                  LTemp$16+=TTextBuffer.Current(Self);
               }
            }
            if (!TTextBuffer.Next(Self)) {
               break;
            }
         }
         if (LTemp$16.length>0) {
            List.v.push(LTemp$16);
         }
         Result = List.v.length>0;
      }
      return Result
   }
   /// function TTextBuffer.ReadQuotedString() : String
   ,ReadQuotedString:function(Self) {
      var Result = "";
      var TempChar = "";
      if (!TTextBuffer.Empty$2(Self)) {
         if (!TTextBuffer.EOF$3(Self)) {
            if (TTextBuffer.Current(Self)!="\"") {
               TW3ErrorObject.SetLastError$(Self,"Failed to read quoted string, expected index on \" character error");
               return Result;
            }
            if (!TTextBuffer.NextNoCrLf(Self)) {
               TW3ErrorObject.SetLastError$(Self,"Failed to skip initial \" character error");
               return Result;
            }
            while (!TTextBuffer.EOF$3(Self)) {
               TempChar = TTextBuffer.Current(Self);
               if (TempChar=="\"") {
                  if (!TTextBuffer.NextNoCrLf(Self)) {
                     TW3ErrorObject.SetLastError$(Self,"failed to skip final \" character in string error");
                  }
                  break;
               }
               Result+=TempChar;
               if (!TTextBuffer.NextNoCrLf(Self)) {
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TTextBuffer.ReadTag(var Info: TTbTagData) : Boolean
   ,ReadTag:function(Self, Info$3) {
      var Result = false;
      var TempChar$1 = "";
      var TagText = "";
      var tempText = {};
      tempText.v = "";
      var tempName = {};
      tempName.v = "";
      var tempValue = {};
      tempValue.v = "";
      var Buffer$11 = null;
      function AddTagValue(ThisName, ThisValue) {
         var Result = 0;
         var NameValue = {tvName:"",tvValue:""};
         NameValue.tvName = ThisName;
         NameValue.tvValue = ThisValue;
         Info$3.nvValues.push(Clone$TTbTagValue(NameValue));
         Result = Info$3.nvValues.length-1;
         return Result
      };
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      Result = false;
      Info$3.nvClosed = false;
      Info$3.nvValues.length=0;
      if (!TTextBuffer.Empty$2(Self)) {
         if (!TTextBuffer.EOF$3(Self)) {
            TTextBuffer.ConsumeJunk(Self);
            if (TTextBuffer.EOF$3(Self)) {
               return Result;
            }
            if (TTextBuffer.Current(Self)!="<") {
               return Result;
            } else {
               if (!TTextBuffer.Next(Self)) {
                  return Result;
               }
            }
            if (TTextBuffer.Current(Self)=="\/") {
               Info$3.nvClosed = true;
               if (!TTextBuffer.Next(Self)) {
                  return Result;
               }
            }
            while (!TTextBuffer.EOF$3(Self)) {
               TempChar$1 = TTextBuffer.Read$8(Self);
               if (TempChar$1=="") {
                  break;
               }
               if (TempChar$1=="\"") {
                  Result = true;
                  break;
               }
               TagText+=TempChar$1;
            }
            if (!Result) {
               return Result;
            }
            Result = false;
            TagText = Trim$_String_(TagText);
            Buffer$11 = TTextBuffer.Create$71$($New(TTextBuffer),TagText);
            try {
               if (TTextBuffer.ReadWord$2(Buffer$11,tempText)) {
                  Info$3.nvTagId = tempText.v;
               } else {
                  TW3ErrorObject.SetLastError$(Self,"Invalid Tag, failed to read id error");
                  return Result;
               }
               while (!TTextBuffer.EOF$3(Buffer$11)) {
                  TTextBuffer.ConsumeJunk(Buffer$11);
                  tempName.v = "";
                  if (TTextBuffer.ReadWord$2(Buffer$11,tempName)) {
                     TTextBuffer.ConsumeJunk(Buffer$11);
                  } else {
                     break;
                  }
                  if (TTextBuffer.Current(Buffer$11)=="=") {
                     TTextBuffer.Next(Buffer$11);
                     if (TTextBuffer.Current(Buffer$11)=="\"") {
                        tempValue.v = TTextBuffer.ReadQuotedString(Self);
                     } else {
                        TTextBuffer.ReadWord$2(Self,tempValue);
                     }
                     AddTagValue(tempName.v,tempValue.v);
                  } else {
                     AddTagValue(tempName.v,"");
                  }
               }
            } finally {
               TObject.Free(Buffer$11);
            }
         }
      }
      return Result
   }
   /// function TTextBuffer.ReadTo(MatchText: String; var TextRead: String) : Boolean
   ,ReadTo$3:function(Self, MatchText, TextRead$1) {
      var Result = false;
      var TempCache = {v:""};
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      Result = false;
      SetLength(TextRead$1,0);
      if (TTextBuffer.Empty$2(Self)) {
         Result = false;
         TW3ErrorObject.SetLastError$(Self,$R[55]);
      } else {
         if (TTextBuffer.BOF$3(Self)) {
            if (!TTextBuffer.First(Self)) {
               return Result;
            }
         }
         if (TTextBuffer.EOF$3(Self)) {
            TW3ErrorObject.SetLastError$(Self,$R[57]);
            return Result;
         }
         if (MatchText.length>0) {
            MatchText = (MatchText).toLocaleLowerCase();
            do {
               TempCache.v = "";
               if (TTextBuffer.Peek$1(Self,MatchText.length,TempCache)) {
                  TempCache.v = (TempCache.v).toLocaleLowerCase();
                  Result = SameText(TempCache.v,MatchText);
                  if (Result) {
                     break;
                  } else {
                     TextRead$1.v = TextRead$1.v+TTextBuffer.Current(Self);
                  }
               } else {
                  TextRead$1.v = TextRead$1.v+TTextBuffer.Current(Self);
               }
               if (!TTextBuffer.Next(Self)) {
                  break;
               }
            } while (!TTextBuffer.EOF$3(Self));
         }
      }
      return Result
   }
   /// function TTextBuffer.ReadTo(MatchText: String) : Boolean
   ,ReadTo$2:function(Self, MatchText$1) {
      var Result = false;
      var MatchLen = 0,
         TempCache$1 = {v:""};
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TTextBuffer.Empty$2(Self)) {
         Result = false;
         TW3ErrorObject.SetLastError$(Self,$R[55]);
      } else {
         if (TTextBuffer.BOF$3(Self)) {
            if (!TTextBuffer.First(Self)) {
               return Result;
            }
         }
         if (TTextBuffer.EOF$3(Self)) {
            TW3ErrorObject.SetLastError$(Self,$R[57]);
            return Result;
         }
         MatchLen = MatchText$1.length;
         if (MatchLen>0) {
            MatchText$1 = (MatchText$1).toLocaleLowerCase();
            do {
               TempCache$1.v = "";
               if (TTextBuffer.Peek$1(Self,MatchLen,TempCache$1)) {
                  TempCache$1.v = (TempCache$1.v).toLocaleLowerCase();
                  Result = SameText(TempCache$1.v,MatchText$1);
                  if (Result) {
                     break;
                  }
               }
               if (!TTextBuffer.Next(Self)) {
                  break;
               }
            } while (!TTextBuffer.EOF$3(Self));
         }
      }
      return Result
   }
   /// function TTextBuffer.ReadTo(const Resignators: TSysCharSet; var TextRead: String) : Boolean
   ,ReadTo$1:function(Self, Resignators, TextRead$2) {
      var Result = false;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      SetLength(TextRead$2,0);
      if (TTextBuffer.Empty$2(Self)) {
         Result = false;
         TW3ErrorObject.SetLastError$(Self,$R[55]);
      } else {
         if (TTextBuffer.BOF$3(Self)) {
            if (!TTextBuffer.First(Self)) {
               return Result;
            }
         }
         if (TTextBuffer.EOF$3(Self)) {
            TW3ErrorObject.SetLastError$(Self,$R[57]);
            return Result;
         }
         do {
            if (Resignators.indexOf(TTextBuffer.Current(Self))>=0) {
               break;
            } else {
               TextRead$2.v+=TTextBuffer.Current(Self);
            }
            if (!TTextBuffer.Next(Self)) {
               break;
            }
         } while (!TTextBuffer.EOF$3(Self));
         Result = TextRead$2.v.length>0;
      }
      return Result
   }
   /// function TTextBuffer.ReadTo(const CB: TTextValidCB; var TextRead: String) : Boolean
   ,ReadTo:function(Self, CB$19, TextRead$3) {
      var Result = false;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      SetLength(TextRead$3,0);
      if (TTextBuffer.Empty$2(Self)) {
         Result = false;
         TW3ErrorObject.SetLastError$(Self,$R[55]);
      } else {
         if (TTextBuffer.BOF$3(Self)) {
            if (!TTextBuffer.First(Self)) {
               return Result;
            }
         }
         if (TTextBuffer.EOF$3(Self)) {
            TW3ErrorObject.SetLastError$(Self,$R[57]);
            return Result;
         }
         if (!CB$19) {
            TW3ErrorObject.SetLastError$(Self,"Invalid callback handler");
            return Result;
         }
         do {
            if (CB$19(TTextBuffer.Current(Self))) {
               TextRead$3.v = TextRead$3.v+TTextBuffer.Current(Self);
            } else {
               break;
            }
            if (!TTextBuffer.Next(Self)) {
               break;
            }
         } while (!TTextBuffer.EOF$3(Self));
         Result = TextRead$3.v.length>0;
      }
      return Result
   }
   /// function TTextBuffer.ReadToEOL(var TextRead: String) : Boolean
   ,ReadToEOL$1:function(Self, TextRead$4) {
      var Result = false;
      var LStart$2 = 0,
         LLen$13 = 0,
         LLen$14 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      TextRead$4.v = "";
      if (!TTextBuffer.Empty$2(Self)) {
         if (TTextBuffer.BOF$3(Self)) {
            if (!TTextBuffer.First(Self)) {
               return Result;
            }
         }
         if (TTextBuffer.EOF$3(Self)) {
            TW3ErrorObject.SetLastError$(Self,$R[57]);
            return Result;
         }
         LStart$2 = Self.FOffset$3;
         do {
            if (Self.FData$1.charAt(Self.FOffset$3-1)=="\r"&&Self.FData$1.charAt(Self.FOffset$3)=="\n") {
               if (Self.FOffset$3>LStart$2) {
                  LLen$13 = Self.FOffset$3-LStart$2;
                  TextRead$4.v = Self.FData$1.substr(LStart$2-1,LLen$13);
               }
               Result = true;
               break;
            }
            ++Self.FOffset$3;
            ++Self.FCol;
         } while (!TTextBuffer.EOF$3(Self));
         if (!Result) {
            if (Self.FOffset$3>=Self.FData$1.length) {
               if (LStart$2<Self.FOffset$3) {
                  LLen$14 = Self.FOffset$3-LStart$2;
                  if (LLen$14>0) {
                     TextRead$4.v = Self.FData$1.substr(LStart$2-1,LLen$14);
                     Result = true;
                  }
                  return Result;
               }
            }
         }
      }
      return Result
   }
   /// function TTextBuffer.ReadToEOL() : Boolean
   ,ReadToEOL:function(Self) {
      var Result = false;
      var LStart$3 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TTextBuffer.Empty$2(Self)) {
         TW3ErrorObject.SetLastError$(Self,$R[55]);
      } else {
         if (TTextBuffer.BOF$3(Self)) {
            if (!TTextBuffer.First(Self)) {
               return Result;
            }
         }
         if (TTextBuffer.EOF$3(Self)) {
            TW3ErrorObject.SetLastError$(Self,$R[57]);
            return Result;
         }
         LStart$3 = Self.FOffset$3;
         do {
            if (Self.FData$1.charAt(Self.FOffset$3-1)=="\r"&&Self.FData$1.charAt(Self.FOffset$3)=="\n") {
               Result = true;
               break;
            } else {
               ++Self.FOffset$3;
               ++Self.FCol;
            }
         } while (!TTextBuffer.EOF$3(Self));
         if (!Result) {
            if (TTextBuffer.EOF$3(Self)) {
               if (LStart$3<Self.FOffset$3) {
                  Result = true;
               }
            }
         }
      }
      return Result
   }
   /// function TTextBuffer.ReadWord(var TextRead: String) : Boolean
   ,ReadWord$2:function(Self, TextRead$5) {
      var Result = false;
      var el$2 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      TextRead$5.v = "";
      if (TTextBuffer.Empty$2(Self)) {
         TW3ErrorObject.SetLastError$(Self,"Failed to read word, buffer is empty error");
      } else {
         TTextBuffer.ConsumeJunk(Self);
         if (TTextBuffer.EOF$3(Self)) {
            TW3ErrorObject.SetLastError$(Self,"Failed to read word, unexpected EOF");
         } else {
            do {
               el$2 = TTextBuffer.Current(Self);
               if ((((el$2>="A")&&(el$2<="Z"))||((el$2>="a")&&(el$2<="z"))||((el$2>="0")&&(el$2<="9"))||(el$2=="_")||(el$2=="-"))) {
                  TextRead$5.v+=el$2;
               } else {
                  break;
               }
               if (!TTextBuffer.NextNoCrLf(Self)) {
                  break;
               }
            } while (!TTextBuffer.EOF$3(Self));
            Result = TextRead$5.v.length>0;
         }
      }
      return Result
   }
   /// procedure TTextBuffer.Restore(const Mark: TTextBufferBookmark)
   ,Restore:function(Self, Mark$3) {
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TTextBuffer.Empty$2(Self)) {
         throw Exception.Create($New(ETextBuffer),"Failed to restore bookmark, buffer is empty error");
      } else {
         if (Mark$3!==null) {
            Self.FOffset$3 = Mark$3.bbOffset;
            Self.FCol = Mark$3.bbCol;
            Self.FRow = Mark$3.bbRow;
            TObject.Free(Mark$3);
         } else {
            throw Exception.Create($New(ETextBuffer),"Failed to restore bookmark, object was nil error");
         }
      }
   }
   /// function TTextBuffer.SemiColon() : Boolean
   ,SemiColon:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&Self.FData$1.charAt(Self.FOffset$3-1)==";";
   }
   /// procedure TTextBuffer.SetCacheData(NewText: String)
   ,SetCacheData:function(Self, NewText) {
      TTextBuffer.LoadBufferText(Self,NewText);
   }
   /// function TTextBuffer.Space() : Boolean
   ,Space:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&TTextBuffer.Current(Self)==" ";
   }
   /// function TTextBuffer.Tab() : Boolean
   ,Tab:function(Self) {
      return (!TTextBuffer.Empty$2(Self))&&Self.FOffset$3>=1&&Self.FOffset$3<=Self.FData$1.length&&Self.FData$1.charAt(Self.FOffset$3-1)=="\t";
   }
   /// function TTextBuffer.Trail() : String
   ,Trail:function(Self) {
      var Result = "";
      if (!TTextBuffer.Empty$2(Self)) {
         if (!TTextBuffer.EOF$3(Self)) {
            Result = Self.FData$1.substr(Self.FOffset$3-1);
         }
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3:TW3ErrorObject.Create$3
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,Create$71$:function($){return $.ClassType.Create$71.apply($.ClassType, arguments)}
};
TTextBuffer.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TTbTagValue = record
function Copy$TTbTagValue(s,d) {
   d.tvName=s.tvName;
   d.tvValue=s.tvValue;
   return d;
}
function Clone$TTbTagValue($) {
   return {
      tvName:$.tvName,
      tvValue:$.tvValue
   }
}
/// TTbTagData = record
function Copy$TTbTagData(s,d) {
   d.nvClosed=s.nvClosed;
   d.nvTagId=s.nvTagId;
   d.nvValues=s.nvValues;
   return d;
}
function Clone$TTbTagData($) {
   return {
      nvClosed:$.nvClosed,
      nvTagId:$.nvTagId,
      nvValues:$.nvValues
   }
}
/// TParserModelObject = class (TObject)
var TParserModelObject = {
   $ClassName:"TParserModelObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.Context = $.FParent = null;
      $.FChildren = [];
   }
   /// function TParserModelObject.ChildAdd(const Instance: TParserModelObject) : TParserModelObject
   ,ChildAdd:function(Self, Instance$9) {
      var Result = null;
      if (Self.FChildren.indexOf(Instance$9)<0) {
         Self.FChildren.push(Instance$9);
      }
      Result = Instance$9;
      return Result
   }
   /// function TParserModelObject.ChildGetCount() : Integer
   ,ChildGetCount:function(Self) {
      return Self.FChildren.length;
   }
   /// function TParserModelObject.ChildGetItem(const Index: Integer) : TParserModelObject
   ,ChildGetItem:function(Self, Index$8) {
      return Self.FChildren[Index$8];
   }
   /// procedure TParserModelObject.Clear()
   ,Clear$6:function(Self) {
      var LItem$3 = null;
      var a$462 = 0;
      try {
         var a$463 = [];
         a$463 = Self.FChildren;
         var $temp56;
         for(a$462=0,$temp56=a$463.length;a$462<$temp56;a$462++) {
            LItem$3 = a$463[a$462];
            if (LItem$3!==null) {
               TObject.Free(LItem$3);
            }
         }
      } finally {
         Self.FChildren.length=0;
      }
   }
   /// constructor TParserModelObject.Create(const AParent: TParserModelObject)
   ,Create$72:function(Self, AParent) {
      TObject.Create(Self);
      Self.FParent = AParent;
      return Self
   }
   /// destructor TParserModelObject.Destroy()
   ,Destroy:function(Self) {
      TParserModelObject.Clear$6$(Self);
      Self.FChildren.length=0;
      TObject.Destroy(Self);
   }
   /// function TParserModelObject.GetParent() : TParserModelObject
   ,GetParent:function(Self) {
      return Self.FParent;
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,ChildAdd$:function($){return $.ClassType.ChildAdd.apply($.ClassType, arguments)}
   ,ChildGetCount$:function($){return $.ClassType.ChildGetCount($)}
   ,ChildGetItem$:function($){return $.ClassType.ChildGetItem.apply($.ClassType, arguments)}
   ,Clear$6$:function($){return $.ClassType.Clear$6($)}
   ,Create$72$:function($){return $.ClassType.Create$72.apply($.ClassType, arguments)}
   ,GetParent$:function($){return $.ClassType.GetParent($)}
};
/// TParserContext = class (TW3ErrorObject)
var TParserContext = {
   $ClassName:"TParserContext",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.Model = $.FBuffer$3 = null;
      $.FStack$1 = [];
   }
   /// procedure TParserContext.ClearStack()
   ,ClearStack:function(Self) {
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      try {
         Self.FStack$1.length=0;
      } catch ($e) {
         var e$20 = $W($e);
         TW3ErrorObject.SetLastError$(Self,("Internal error:"+e$20.FMessage))      }
   }
   /// constructor TParserContext.Create(const SourceCode: String)
   ,Create$73:function(Self, SourceCode) {
      TW3ErrorObject.Create$3(Self);
      Self.FBuffer$3 = TTextBuffer.Create$71$($New(TTextBuffer),SourceCode);
      return Self
   }
   /// destructor TParserContext.Destroy()
   ,Destroy:function(Self) {
      TParserContext.ClearStack(Self);
      TObject.Free(Self.FBuffer$3);
      TW3ErrorObject.Destroy(Self);
   }
   /// function TParserContext.Peek() : TParserModelObject
   ,Peek$2:function(Self) {
      var Result = null;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      try {
         Result = $Peek(Self.FStack$1," in TParserContext.Peek [line: 1858, column: 22, file: System.Text.Parser]");
      } catch ($e) {
         var e$21 = $W($e);
         TW3ErrorObject.SetLastError$(Self,("Internal error:"+e$21.FMessage))      }
      return Result
   }
   /// function TParserContext.Pop() : TParserModelObject
   ,Pop:function(Self) {
      var Result = null;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      try {
         Result = Self.FStack$1.pop();
      } catch ($e) {
         var e$22 = $W($e);
         TW3ErrorObject.SetLastError$(Self,("Internal error:"+e$22.FMessage))      }
      return Result
   }
   /// procedure TParserContext.Push(const ModelObj: TParserModelObject)
   ,Push:function(Self, ModelObj) {
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      try {
         Self.FStack$1.push(ModelObj);
      } catch ($e) {
         var e$23 = $W($e);
         TW3ErrorObject.SetLastError$(Self,("Internal error:"+e$23.FMessage))      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3:TW3ErrorObject.Create$3
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,Create$73$:function($){return $.ClassType.Create$73.apply($.ClassType, arguments)}
};
TParserContext.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// ETextBuffer = class (Exception)
var ETextBuffer = {
   $ClassName:"ETextBuffer",$Parent:Exception
   ,$Init:function ($) {
      Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
};
/// EModelObject = class (EW3Exception)
var EModelObject = {
   $ClassName:"EModelObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TW3Structure = class (TObject)
var TW3Structure = {
   $ClassName:"TW3Structure",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TW3Structure.ReadBool(const Name: String) : Boolean
   ,ReadBool$1:function(Self, Name$19) {
      return (TW3Structure.Read$3$(Self,Name$19)?true:false);
   }
   /// function TW3Structure.ReadBytes(const Name: String) : TByteArray
   ,ReadBytes$1:function(Self, Name$20) {
      return TW3Structure.Read$3$(Self,Name$20);
   }
   /// function TW3Structure.ReadDateTime(const Name: String) : TDateTime
   ,ReadDateTime$1:function(Self, Name$21) {
      return TW3Structure.Read$3$(Self,Name$21);
   }
   /// function TW3Structure.ReadFloat(const Name: String) : Float
   ,ReadFloat:function(Self, Name$22) {
      return Number(TW3Structure.Read$3$(Self,Name$22));
   }
   /// function TW3Structure.ReadInt(const Name: String) : Integer
   ,ReadInt$1:function(Self, Name$23) {
      return parseInt(TW3Structure.Read$3$(Self,Name$23),10);
   }
   /// function TW3Structure.ReadString(const Name: String; const Decode: Boolean) : String
   ,ReadString$2:function(Self, Name$24, Decode$3) {
      var Result = "";
      if (Decode$3) {
         Result = TString.DecodeBase64(TString,String(TW3Structure.Read$3$(Self,Name$24)));
      } else {
         Result = String(TW3Structure.Read$3$(Self,Name$24));
      }
      return Result
   }
   /// procedure TW3Structure.WriteBool(const Name: String; value: Boolean)
   ,WriteBool$1:function(Self, Name$25, value$14) {
      TW3Structure.Write$3$(Self,Name$25,value$14);
   }
   /// procedure TW3Structure.WriteBytes(const Name: String; const Value: TByteArray)
   ,WriteBytes$1:function(Self, Name$26, Value$149) {
      try {
         TW3Structure.Write$3$(Self,Name$26,TDatatype.BytesToBase64$1(TDatatype,Value$149));
      } catch ($e) {
         var e$24 = $W($e);
         throw EW3Exception.CreateFmt$($New(EW3Structure),"Failed to write bytes to structure, system threw exception [%s] with message [%s]",[TObject.ClassName(e$24.ClassType), e$24.FMessage]);
      }
   }
   /// procedure TW3Structure.WriteDateTime(const Name: String; value: TDateTime)
   ,WriteDateTime$1:function(Self, Name$27, value$15) {
      TW3Structure.Write$3$(Self,Name$27,value$15);
   }
   /// procedure TW3Structure.WriteFloat(const Name: String; value: Float)
   ,WriteFloat:function(Self, Name$28, value$16) {
      TW3Structure.Write$3$(Self,Name$28,value$16);
   }
   /// procedure TW3Structure.WriteInt(const Name: String; value: Integer)
   ,WriteInt:function(Self, Name$29, value$17) {
      TW3Structure.Write$3$(Self,Name$29,value$17);
   }
   /// procedure TW3Structure.WriteStream(const Name: String; const Value: TStream)
   ,WriteStream:function(Self, Name$30, Value$150) {
      var LBytes$15 = [];
      if (Value$150!==null) {
         if (TStream.GetSize$(Value$150)>0) {
            TStream.SetPosition$(Value$150,0);
            LBytes$15 = TStream.Read$1$(Value$150,TStream.GetSize$(Value$150));
         }
         TW3Structure.WriteBytes$1(Self,Name$30,LBytes$15);
      } else {
         throw Exception.Create($New(EW3Structure),"Failed to write stream to structure, stream was nil error");
      }
   }
   /// procedure TW3Structure.WriteString(Name: String; Value: String; const Encode: Boolean)
   ,WriteString$2:function(Self, Name$31, Value$151, Encode$3) {
      if (Encode$3) {
         Value$151 = TString.EncodeBase64(TString,Value$151);
      }
      TW3Structure.Write$3$(Self,Name$31,Value$151);
   }
   ,Destroy:TObject.Destroy
   ,Clear$1$:function($){return $.ClassType.Clear$1($)}
   ,Exists$1$:function($){return $.ClassType.Exists$1.apply($.ClassType, arguments)}
   ,LoadFromBuffer$:function($){return $.ClassType.LoadFromBuffer.apply($.ClassType, arguments)}
   ,LoadFromStream$1$:function($){return $.ClassType.LoadFromStream$1.apply($.ClassType, arguments)}
   ,Read$3$:function($){return $.ClassType.Read$3.apply($.ClassType, arguments)}
   ,SaveToBuffer$:function($){return $.ClassType.SaveToBuffer.apply($.ClassType, arguments)}
   ,SaveToStream$1$:function($){return $.ClassType.SaveToStream$1.apply($.ClassType, arguments)}
   ,Write$3$:function($){return $.ClassType.Write$3.apply($.ClassType, arguments)}
};
TW3Structure.$Intf={
   IW3Structure:[TW3Structure.WriteString$2,TW3Structure.WriteInt,TW3Structure.WriteBool$1,TW3Structure.WriteFloat,TW3Structure.WriteDateTime$1,TW3Structure.ReadString$2,TW3Structure.ReadInt$1,TW3Structure.ReadBool$1,TW3Structure.ReadFloat,TW3Structure.ReadDateTime$1,TW3Structure.Read$3,TW3Structure.Write$3]
   ,IW3StructureReadAccess:[TW3Structure.Exists$1,TW3Structure.ReadString$2,TW3Structure.ReadInt$1,TW3Structure.ReadBool$1,TW3Structure.ReadFloat,TW3Structure.ReadDateTime$1,TW3Structure.Read$3,TW3Structure.ReadBytes$1]
   ,IW3StructureWriteAccess:[TW3Structure.WriteString$2,TW3Structure.WriteInt,TW3Structure.WriteBool$1,TW3Structure.WriteFloat,TW3Structure.WriteDateTime$1,TW3Structure.Write$3,TW3Structure.WriteBytes$1,TW3Structure.WriteStream]
}
/// EW3Structure = class (EW3Exception)
var EW3Structure = {
   $ClassName:"EW3Structure",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TJSONStructure = class (TW3Structure)
var TJSONStructure = {
   $ClassName:"TJSONStructure",$Parent:TW3Structure
   ,$Init:function ($) {
      TW3Structure.$Init($);
      $.FObj = null;
   }
   /// anonymous TSourceMethodSymbol
   ,a$97:function(Self) {
      return Self.FObj.FInstance;
   }
   /// procedure TJSONStructure.Clear()
   ,Clear$1:function(Self) {
      TJSONObject.Clear$3(Self.FObj);
   }
   /// constructor TJSONStructure.Create()
   ,Create$65:function(Self) {
      TObject.Create(Self);
      Self.FObj = TJSONObject.Create$66$($New(TJSONObject));
      return Self
   }
   /// destructor TJSONStructure.Destroy()
   ,Destroy:function(Self) {
      TObject.Free(Self.FObj);
      TObject.Destroy(Self);
   }
   /// function TJSONStructure.Exists(const Name: String) : Boolean
   ,Exists$1:function(Self, Name$32) {
      return TJSONObject.Exists$3(Self.FObj,Name$32);
   }
   /// procedure TJSONStructure.ForEach(const Process: TJSONStructureProcess)
   ,ForEach$2:function(Self, Process$2) {
      if (Process$2) {
         TVariant.ForEachProperty(Self.FObj.FInstance,function (Name$33, Data$66) {
            var Result = 1;
            if (Process$2(Name$33,Data$66)==160) {
               Result = 1;
            } else {
               Result = 0;
            }
            return Result
         });
      }
   }
   /// procedure TJSONStructure.FromJSon(const Text: String)
   ,FromJSon:function(Self, Text$31) {
      var LOptions = [0];
      LOptions = [15];
      Self.FObj = TJSONObject.Create$68($New(TJSONObject),null,LOptions);
      TJSONObject.FromJSON(Self.FObj,Text$31);
   }
   /// procedure TJSONStructure.LoadFromBuffer(const Buffer: TBinaryData)
   ,LoadFromBuffer:function(Self, Buffer$12) {
      TJSONObject.LoadFromBuffer$2(Self.FObj,Buffer$12);
   }
   /// procedure TJSONStructure.LoadFromStream(const Stream: TStream)
   ,LoadFromStream$1:function(Self, Stream$9) {
      TJSONObject.LoadFromStream$3(Self.FObj,Stream$9);
   }
   /// function TJSONStructure.Read(const Name: String) : Variant
   ,Read$3:function(Self, Name$34) {
      var Result = {v:undefined};
      try {
         if (TJSONObject.Exists$3(Self.FObj,Name$34)) {
            TJSONObject.Read$5(Self.FObj,Name$34,Result);
         } else {
            Result.v = undefined;
         }
      } finally {return Result.v}
   }
   /// procedure TJSONStructure.SaveToBuffer(const Buffer: TBinaryData)
   ,SaveToBuffer:function(Self, Buffer$13) {
      TJSONObject.SaveToBuffer$2(Self.FObj,Buffer$13);
   }
   /// procedure TJSONStructure.SaveToStream(const Stream: TStream)
   ,SaveToStream$1:function(Self, Stream$10) {
      TJSONObject.SaveToStream$3(Self.FObj,Stream$10);
   }
   /// function TJSONStructure.ToJSon() : String
   ,ToJSon:function(Self) {
      return TJSONObject.ToJSON(Self.FObj);
   }
   /// function TJSONStructure.ToJSonObject() : TJSONObject
   ,ToJSonObject:function(Self) {
      var Result = null;
      var LOptions$1 = [0];
      LOptions$1 = [15];
      Result = TJSONObject.Create$68($New(TJSONObject),null,LOptions$1);
      TJSONObject.FromJSON(Result,TJSONObject.ToJSON(Self.FObj));
      return Result
   }
   /// function TJSONStructure.ToStream() : TStream
   ,ToStream$1:function(Self) {
      var Result = null;
      Result = TDataTypeConverter.Create$15$($New(TMemoryStream));
      TW3Structure.SaveToStream$1$(Self,Result);
      TStream.SetPosition$(Result,0);
      return Result
   }
   /// function TJSONStructure.ToString() : String
   ,ToString$3:function(Self) {
      return TJSONStructure.ToJSon(Self);
   }
   /// procedure TJSONStructure.Write(const Name: String; const Value: Variant)
   ,Write$3:function(Self, Name$35, Value$152) {
      TJSONObject.AddOrSet(Self.FObj,Name$35,Value$152);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Clear$1$:function($){return $.ClassType.Clear$1($)}
   ,Exists$1$:function($){return $.ClassType.Exists$1.apply($.ClassType, arguments)}
   ,LoadFromBuffer$:function($){return $.ClassType.LoadFromBuffer.apply($.ClassType, arguments)}
   ,LoadFromStream$1$:function($){return $.ClassType.LoadFromStream$1.apply($.ClassType, arguments)}
   ,Read$3$:function($){return $.ClassType.Read$3.apply($.ClassType, arguments)}
   ,SaveToBuffer$:function($){return $.ClassType.SaveToBuffer.apply($.ClassType, arguments)}
   ,SaveToStream$1$:function($){return $.ClassType.SaveToStream$1.apply($.ClassType, arguments)}
   ,Write$3$:function($){return $.ClassType.Write$3.apply($.ClassType, arguments)}
   ,Create$65$:function($){return $.ClassType.Create$65($)}
   ,ToStream$1$:function($){return $.ClassType.ToStream$1($)}
   ,ToString$3$:function($){return $.ClassType.ToString$3($)}
};
TJSONStructure.$Intf={
   IW3Structure:[TW3Structure.WriteString$2,TW3Structure.WriteInt,TW3Structure.WriteBool$1,TW3Structure.WriteFloat,TW3Structure.WriteDateTime$1,TW3Structure.ReadString$2,TW3Structure.ReadInt$1,TW3Structure.ReadBool$1,TW3Structure.ReadFloat,TW3Structure.ReadDateTime$1,TJSONStructure.Read$3,TJSONStructure.Write$3]
   ,IW3StructureReadAccess:[TJSONStructure.Exists$1,TW3Structure.ReadString$2,TW3Structure.ReadInt$1,TW3Structure.ReadBool$1,TW3Structure.ReadFloat,TW3Structure.ReadDateTime$1,TJSONStructure.Read$3,TW3Structure.ReadBytes$1]
   ,IW3StructureWriteAccess:[TW3Structure.WriteString$2,TW3Structure.WriteInt,TW3Structure.WriteBool$1,TW3Structure.WriteFloat,TW3Structure.WriteDateTime$1,TJSONStructure.Write$3,TW3Structure.WriteBytes$1,TW3Structure.WriteStream]
}
/// TFNV1aHash = class (TObject)
var TFNV1aHash = {
   $ClassName:"TFNV1aHash",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TFNV1aHash.Generate(const Text: String) : String
   ,Generate$1:function(Self, Text$32) {
      var Result = "";
      var LVal = 0,
         x$39 = 0;
      LVal = 2166136261;
      var $temp57;
      for(x$39=1,$temp57=Text$32.length;x$39<=$temp57;x$39++) {
         LVal = LVal^TDatatype.CharToByte$1(TDatatype,Text$32.charAt(x$39-1));
         LVal+=(LVal>>>1)+(LVal>>>4)+(LVal>>>7)+(LVal>>>8)+(LVal>>>24);
      }
      Result = "$"+(IntToHex(LVal,8)).toLocaleUpperCase();
      return Result
   }
   /// function TFNV1aHash.Generate(const Text: String; const Salt: Longword) : String
   ,Generate:function(Self, Text$33, Salt) {
      var Result = "";
      var LVal$1 = 0,
         x$40 = 0;
      LVal$1 = TDatatype.InitUint32$1(Salt);
      if (LVal$1<1) {
         LVal$1 = 2166136261;
      }
      var $temp58;
      for(x$40=1,$temp58=Text$33.length;x$40<=$temp58;x$40++) {
         LVal$1 = LVal$1^TDatatype.CharToByte$1(TDatatype,Text$33.charAt(x$40-1));
         LVal$1+=(LVal$1>>>1)+(LVal$1>>>4)+(LVal$1>>>7)+(LVal$1>>>8)+(LVal$1>>>24);
      }
      Result = "$"+(IntToHex(LVal$1,8)).toLocaleUpperCase();
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TW3Transaction = class (TW3Component)
var TW3Transaction = {
   $ClassName:"TW3Transaction",$Parent:TW3Component
   ,$Init:function ($) {
      TW3Component.$Init($);
      $.FDatabase$1 = $.FParams = null;
   }
   /// function TW3Transaction.CanExecute() : Boolean
   ,CanExecute:function(Self) {
      var Result = false;
      var obj$2 = null;
      obj$2 = TW3Transaction.GetDatabase$(Self);
      Result = (obj$2!==null)&&TW3Database.GetState$(obj$2)==2;
      return Result
   }
   /// procedure TW3Transaction.FinalizeObject()
   ,FinalizeObject:function(Self) {
      TObject.Free(Self.FParams);
      TW3Component.FinalizeObject(Self);
   }
   /// function TW3Transaction.GetDatabase() : TW3Database
   ,GetDatabase:function(Self) {
      return Self.FDatabase$1;
   }
   /// procedure TW3Transaction.InitializeObject()
   ,InitializeObject:function(Self) {
      TW3Component.InitializeObject(Self);
      Self.FParams = TW3OwnedObject.Create$16$($New(TW3TransactionParameters),Self);
   }
   /// procedure TW3Transaction.SetDatabase(NewDatabase: TW3Database)
   ,SetDatabase:function(Self, NewDatabase) {
      if (NewDatabase!==Self.FDatabase$1) {
         Self.FDatabase$1 = NewDatabase;
      }
   }
   ,Destroy:TW3Component.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,SetLastError$1:TW3OwnedErrorObject.SetLastError$1
   ,ObjectLocked$2:TW3OwnedLockedErrorObject.ObjectLocked$2
   ,ObjectUnLocked$2:TW3OwnedLockedErrorObject.ObjectUnLocked$2
   ,Create$44:TW3Component.Create$44
   ,CreateEx:TW3Component.CreateEx
   ,FinalizeObject$:function($){return $.ClassType.FinalizeObject($)}
   ,FreeAfter:TW3Component.FreeAfter
   ,InitializeObject$:function($){return $.ClassType.InitializeObject($)}
   ,CanExecute$:function($){return $.ClassType.CanExecute($)}
   ,GetDatabase$:function($){return $.ClassType.GetDatabase($)}
   ,SetDatabase$:function($){return $.ClassType.SetDatabase.apply($.ClassType, arguments)}
};
TW3Transaction.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3SQLTransaction = class (TW3Transaction)
var TW3SQLTransaction = {
   $ClassName:"TW3SQLTransaction",$Parent:TW3Transaction
   ,$Init:function ($) {
      TW3Transaction.$Init($);
      $.FHandle$4 = undefined;
      $.FSQL = "";
   }
   /// function TW3SQLTransaction.CanExecute() : Boolean
   ,CanExecute:function(Self) {
      return TW3Transaction.CanExecute(Self)&&Trim$_String_(Self.FSQL).length>0;
   }
   /// function TW3SQLTransaction.GetSQL() : String
   ,GetSQL:function(Self) {
      return Self.FSQL;
   }
   /// procedure TW3SQLTransaction.SetHandle(NewHandle: THandle)
   ,SetHandle:function(Self, NewHandle$2) {
      Self.FHandle$4 = NewHandle$2;
   }
   /// procedure TW3SQLTransaction.SetSQL(NewSQL: String)
   ,SetSQL:function(Self, NewSQL) {
      if (NewSQL!=Self.FSQL) {
         Self.FSQL = NewSQL;
      }
   }
   ,Destroy:TW3Component.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,SetLastError$1:TW3OwnedErrorObject.SetLastError$1
   ,ObjectLocked$2:TW3OwnedLockedErrorObject.ObjectLocked$2
   ,ObjectUnLocked$2:TW3OwnedLockedErrorObject.ObjectUnLocked$2
   ,Create$44:TW3Component.Create$44
   ,CreateEx:TW3Component.CreateEx
   ,FinalizeObject:TW3Transaction.FinalizeObject
   ,FreeAfter:TW3Component.FreeAfter
   ,InitializeObject:TW3Transaction.InitializeObject
   ,CanExecute$:function($){return $.ClassType.CanExecute($)}
   ,GetDatabase:TW3Transaction.GetDatabase
   ,SetDatabase:TW3Transaction.SetDatabase
   ,Execute$1$:function($){return $.ClassType.Execute$1.apply($.ClassType, arguments)}
   ,GetSQL$:function($){return $.ClassType.GetSQL($)}
   ,SetHandle$:function($){return $.ClassType.SetHandle.apply($.ClassType, arguments)}
   ,SetSQL$:function($){return $.ClassType.SetSQL.apply($.ClassType, arguments)}
};
TW3SQLTransaction.$Intf={
   ISQLTransaction:[TW3SQLTransaction.SetHandle,TW3SQLTransaction.GetSQL,TW3SQLTransaction.SetSQL,TW3SQLTransaction.CanExecute]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3WriteTransaction = class (TW3SQLTransaction)
var TW3WriteTransaction = {
   $ClassName:"TW3WriteTransaction",$Parent:TW3SQLTransaction
   ,$Init:function ($) {
      TW3SQLTransaction.$Init($);
   }
   ,Destroy:TW3Component.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,SetLastError$1:TW3OwnedErrorObject.SetLastError$1
   ,ObjectLocked$2:TW3OwnedLockedErrorObject.ObjectLocked$2
   ,ObjectUnLocked$2:TW3OwnedLockedErrorObject.ObjectUnLocked$2
   ,Create$44:TW3Component.Create$44
   ,CreateEx:TW3Component.CreateEx
   ,FinalizeObject:TW3Transaction.FinalizeObject
   ,FreeAfter:TW3Component.FreeAfter
   ,InitializeObject:TW3Transaction.InitializeObject
   ,CanExecute:TW3SQLTransaction.CanExecute
   ,GetDatabase:TW3Transaction.GetDatabase
   ,SetDatabase:TW3Transaction.SetDatabase
   ,Execute$1:TW3SQLTransaction.Execute$1
   ,GetSQL:TW3SQLTransaction.GetSQL
   ,SetHandle:TW3SQLTransaction.SetHandle
   ,SetSQL:TW3SQLTransaction.SetSQL
};
TW3WriteTransaction.$Intf={
   ISQLTransaction:[TW3SQLTransaction.SetHandle,TW3SQLTransaction.GetSQL,TW3SQLTransaction.SetSQL,TW3SQLTransaction.CanExecute]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3TransactionParameters = class (TW3OwnedLockedObject)
var TW3TransactionParameters = {
   $ClassName:"TW3TransactionParameters",$Parent:TW3OwnedLockedObject
   ,$Init:function ($) {
      TW3OwnedLockedObject.$Init($);
      $.FObjects = [];
   }
   /// function TW3TransactionParameters.Add(Name: String; const Value: Variant) : TW3TransactionNamedParameter
   ,Add:function(Self, Name$36, Value$153) {
      var Result = null;
      Name$36 = Trim$_String_(Name$36);
      if (Name$36.length>0) {
         if (TW3TransactionParameters.IndexOf(Self,Name$36)==-1) {
            Result = TW3OwnedObject.Create$16$($New(TW3TransactionNamedParameter),Self);
            Self.FObjects.push(Result);
            Result.Name = Name$36;
            Result.Value = Value$153;
         } else {
            throw EW3Exception.CreateFmt$($New(EW3TransactionParameters),"Method %s failed, could not add parameter (%s), that name already exists",["TW3TransactionParameters.Add", Name$36]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EW3TransactionParameters),"Method %s failed, could not create parameter, name was empty error",["TW3TransactionParameters.Add"]);
      }
      return Result
   }
   /// function TW3TransactionParameters.AddNameOnly(Name: String) : TW3TransactionNamedParameter
   ,AddNameOnly:function(Self, Name$37) {
      var Result = null;
      Name$37 = (Trim$_String_(Name$37)).toLocaleLowerCase();
      if (Name$37.length>0) {
         if (TW3TransactionParameters.IndexOf(Self,Name$37)==-1) {
            Result = TW3OwnedObject.Create$16$($New(TW3TransactionNamedParameter),Self);
            Self.FObjects.push(Result);
            Result.Name = Trim$_String_(Name$37);
         } else {
            throw EW3Exception.CreateFmt$($New(EW3TransactionParameters),"Method %s failed, could not add parameter (%s), that name already exists",["TW3TransactionParameters.AddNameOnly", Name$37]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EW3TransactionParameters),"Method %s failed, could not create parameter, name was empty error",["TW3TransactionParameters.AddNameOnly"]);
      }
      return Result
   }
   /// function TW3TransactionParameters.AddValueOnly(const Value: Variant) : TW3TransactionNamedParameter
   ,AddValueOnly:function(Self, Value$154) {
      var Result = null;
      Result = TW3OwnedObject.Create$16$($New(TW3TransactionNamedParameter),Self);
      Self.FObjects.push(Result);
      Result.Value = Value$154;
      return Result
   }
   /// procedure TW3TransactionParameters.Clear()
   ,Clear$8:function(Self) {
      var a$464 = 0;
      var item = null;
      try {
         var a$465 = [];
         a$465 = Self.FObjects;
         var $temp59;
         for(a$464=0,$temp59=a$465.length;a$464<$temp59;a$464++) {
            item = a$465[a$464];
            TObject.Free(item);
         }
      } finally {
         Self.FObjects.length=0;
      }
   }
   /// destructor TW3TransactionParameters.Destroy()
   ,Destroy:function(Self) {
      if (Self.FObjects.length>0) {
         TW3TransactionParameters.Clear$8(Self);
      }
      TObject.Destroy(Self);
   }
   /// function TW3TransactionParameters.GetNames() : TStrArray
   ,GetNames:function(Self) {
      var Result = [];
      var a$466 = 0;
      var item$1 = null;
      var a$467 = [];
      a$467 = Self.FObjects;
      var $temp60;
      for(a$466=0,$temp60=a$467.length;a$466<$temp60;a$466++) {
         item$1 = a$467[a$466];
         Result.push(item$1.Name);
      }
      return Result
   }
   /// function TW3TransactionParameters.GetParameter(index: Integer) : TW3TransactionNamedParameter
   ,GetParameter:function(Self, index$4) {
      return Self.FObjects[index$4];
   }
   /// function TW3TransactionParameters.GetParameterCount() : Integer
   ,GetParameterCount:function(Self) {
      return Self.FObjects.length;
   }
   /// function TW3TransactionParameters.GetValues() : TVarArray
   ,GetValues$1:function(Self) {
      var Result = [];
      var a$468 = 0;
      var item$2 = null;
      var a$469 = [];
      a$469 = Self.FObjects;
      var $temp61;
      for(a$468=0,$temp61=a$469.length;a$468<$temp61;a$468++) {
         item$2 = a$469[a$468];
         Result.push(item$2.Value);
      }
      return Result
   }
   /// function TW3TransactionParameters.IndexOf(Name: String) : Integer
   ,IndexOf:function(Self, Name$38) {
      var Result = 0;
      var x$41 = 0;
      Result = -1;
      Name$38 = (Trim$_String_(Name$38)).toLocaleLowerCase();
      if (Name$38.length>0) {
         var $temp62;
         for(x$41=0,$temp62=Self.FObjects.length;x$41<$temp62;x$41++) {
            if ((Self.FObjects[x$41].Name).toLocaleLowerCase()==Name$38) {
               Result = x$41;
               break;
            }
         }
      }
      return Result
   }
   /// function TW3TransactionParameters.ObjectOf(Name: String) : TW3TransactionNamedParameter
   ,ObjectOf:function(Self, Name$39) {
      var Result = null;
      var a$470 = 0;
      var item$3 = null;
      Name$39 = (Trim$_String_(Name$39)).toLocaleLowerCase();
      if (Name$39.length>0) {
         var a$471 = [];
         a$471 = Self.FObjects;
         var $temp63;
         for(a$470=0,$temp63=a$471.length;a$470<$temp63;a$470++) {
            item$3 = a$471[a$470];
            if ((item$3.Name).toLocaleLowerCase()==Name$39) {
               Result = item$3;
               break;
            }
         }
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3OwnedLockedObject.ObjectLocked
   ,ObjectUnLocked:TW3OwnedLockedObject.ObjectUnLocked
};
TW3TransactionParameters.$Intf={
   IW3TransactionParameters:[TW3TransactionParameters.GetParameter,TW3TransactionParameters.GetParameterCount,TW3TransactionParameters.Add,TW3TransactionParameters.AddNameOnly,TW3TransactionParameters.AddValueOnly,TW3TransactionParameters.GetValues$1,TW3TransactionParameters.GetNames,TW3TransactionParameters.ObjectOf,TW3TransactionParameters.IndexOf,TW3TransactionParameters.Clear$8]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3TransactionNamedParameter = class (TW3OwnedObject)
var TW3TransactionNamedParameter = {
   $ClassName:"TW3TransactionNamedParameter",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.Name = "";
      $.Value = undefined;
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
};
TW3TransactionNamedParameter.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3ReadTransaction = class (TW3SQLTransaction)
var TW3ReadTransaction = {
   $ClassName:"TW3ReadTransaction",$Parent:TW3SQLTransaction
   ,$Init:function ($) {
      TW3SQLTransaction.$Init($);
   }
   ,Destroy:TW3Component.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,SetLastError$1:TW3OwnedErrorObject.SetLastError$1
   ,ObjectLocked$2:TW3OwnedLockedErrorObject.ObjectLocked$2
   ,ObjectUnLocked$2:TW3OwnedLockedErrorObject.ObjectUnLocked$2
   ,Create$44:TW3Component.Create$44
   ,CreateEx:TW3Component.CreateEx
   ,FinalizeObject:TW3Transaction.FinalizeObject
   ,FreeAfter:TW3Component.FreeAfter
   ,InitializeObject:TW3Transaction.InitializeObject
   ,CanExecute:TW3SQLTransaction.CanExecute
   ,GetDatabase:TW3Transaction.GetDatabase
   ,SetDatabase:TW3Transaction.SetDatabase
   ,Execute$1:TW3SQLTransaction.Execute$1
   ,GetSQL:TW3SQLTransaction.GetSQL
   ,SetHandle:TW3SQLTransaction.SetHandle
   ,SetSQL:TW3SQLTransaction.SetSQL
};
TW3ReadTransaction.$Intf={
   ISQLTransaction:[TW3SQLTransaction.SetHandle,TW3SQLTransaction.GetSQL,TW3SQLTransaction.SetSQL,TW3SQLTransaction.CanExecute]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3DatabaseState enumeration
var TW3DatabaseState = [ "dbInactive", "dbConnecting", "dbActive", "dbDisconnecting" ];
/// TW3Database = class (TW3Component)
var TW3Database = {
   $ClassName:"TW3Database",$Parent:TW3Component
   ,$Init:function ($) {
      TW3Component.$Init($);
      $.OnStateChanged = null;
      $.OnAfterClose = null;
      $.OnClosed = null;
      $.OnBeforeClose = null;
      $.OnAfterOpen = null;
      $.OnOpen = null;
      $.OnBeforeOpen = null;
      $.FLocation = "";
      $.FObjects$1 = [];
      $.FState = 0;
   }
   /// procedure TW3Database.apiAfterClose(Success: Boolean)
   ,apiAfterClose:function(Self, Success$12) {
      if (Success$12) {
         TW3Database.SetState$(Self,0);
         TW3Database.SetLocation$(Self,"");
      }
      if (Self.OnAfterClose) {
         Self.OnAfterClose(Self);
      }
   }
   /// procedure TW3Database.apiAfterOpen(Success: Boolean)
   ,apiAfterOpen:function(Self, Success$13) {
      if (Success$13) {
         TW3Database.SetState$(Self,2);
      }
      if (Self.OnAfterOpen) {
         Self.OnAfterOpen(Self);
      }
   }
   /// procedure TW3Database.apiBeforeClose()
   ,apiBeforeClose:function(Self) {
      TW3Database.SetState$(Self,3);
      if (Self.OnBeforeClose) {
         Self.OnBeforeClose(Self);
      }
   }
   /// procedure TW3Database.apiBeforeOpen()
   ,apiBeforeOpen:function(Self) {
      TW3Database.SetState$(Self,1);
      if (Self.OnBeforeOpen) {
         Self.OnBeforeOpen(Self);
      }
   }
   /// procedure TW3Database.Close(CB: TW3DatabaseCBStandard)
   ,Close:function(Self, CB$20) {
      TW3OwnedErrorObject.ClearLastError$1(Self);
      if (TW3Database.GetActive$1$(Self)) {
         TW3Database.apiBeforeClose$(Self);
         WriteLn("Doing API-Close now");
         TW3Database.apiClose$(Self,function (Sender$19, Success$14) {
            if (Success$14) {
               WriteLn("API-Close done successfully");
            } else {
               WriteLn("API-Close failed to execute: "+TW3OwnedErrorObject.GetLastError$1(Self));
            }
            if (CB$20) {
               CB$20(Self,Success$14);
            }
            if (Self.OnClosed) {
               Self.OnClosed(Self);
            }
            TW3Database.apiAfterClose$(Self,Success$14);
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"Method %s failed, database not active error",["TW3Database.Close"]);
         if (CB$20) {
            CB$20(Self,false);
         }
      }
   }
   /// function TW3Database.GetActive() : Boolean
   ,GetActive$1:function(Self) {
      return (TW3Database.GetState$(Self)!=0);
   }
   /// function TW3Database.GetLocation() : String
   ,GetLocation:function(Self) {
      return Self.FLocation;
   }
   /// function TW3Database.GetState() : TW3DatabaseState
   ,GetState:function(Self) {
      return Self.FState;
   }
   /// procedure TW3Database.Open(DbLocation: String; CB: TW3DatabaseCBStandard)
   ,Open$1:function(Self, DbLocation, CB$21) {
      TW3OwnedErrorObject.ClearLastError$1(Self);
      if (TW3Database.GetActive$1$(Self)) {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"Method %s failed, database already active error",["TW3Database.Open"]);
         if (CB$21) {
            CB$21(Self,false);
         }
      } else {
         TW3Database.apiBeforeOpen$(Self);
         try {
            TW3Database.SetLocation$(Self,DbLocation);
            TW3Database.apiOpen$(Self,function (Sender$20, Success$15) {
               try {
                  if (Success$15) {
                     TW3Database.SetState$(Self,2);
                     if (Self.OnOpen) {
                        Self.OnOpen(Self);
                     }
                     TW3Database.apiAfterOpen$(Self,Success$15);
                  } else {
                     TW3Database.SetLocation$(Self,"");
                  }
               } finally {
                  if (CB$21) {
                     CB$21(Self,Success$15);
                  }
               }
            });
         } catch ($e) {
            var e$25 = $W($e);
            TW3OwnedErrorObject.SetLastErrorF$1(Self,"Method %s failed, system threw exception %s with message: %s",["TW3Database.Open", TObject.ClassName(e$25.ClassType), e$25.FMessage]);
            TW3Database.apiAfterOpen$(Self,false);
            if (CB$21) {
               CB$21(Self,false);
            }
         }
      }
   }
   /// procedure TW3Database.Open(DbLocation: String)
   ,Open:function(Self, DbLocation$1) {
      TW3OwnedErrorObject.ClearLastError$1(Self);
      if (TW3Database.GetActive$1$(Self)) {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"Method %s failed, database already active error",["TW3Database.Open"]);
      } else {
         TW3Database.apiBeforeOpen$(Self);
         try {
            TW3Database.SetLocation$(Self,DbLocation$1);
            TW3Database.apiOpen$(Self,function (Sender$21, Success$16) {
               if (Success$16) {
                  TW3Database.SetState$(Self,2);
                  TW3Database.SetLocation$(Self,TW3Database.GetLocation(Self));
                  if (Self.OnOpen) {
                     Self.OnOpen(Self);
                  }
               }
               TW3Database.apiAfterOpen$(Self,Success$16);
            });
         } catch ($e) {
            var e$26 = $W($e);
            TW3OwnedErrorObject.SetLastErrorF$1(Self,"Method %s failed, system threw exception %s with message: %s",["TW3Database.Open", TObject.ClassName(e$26.ClassType), e$26.FMessage])         }
      }
   }
   /// function TW3Database.RegisterTransaction(Transaction: TW3Transaction) : Boolean
   ,RegisterTransaction:function(Self, Transaction) {
      var Result = false;
      TW3OwnedErrorObject.ClearLastError$1(Self);
      if (Transaction!==null) {
         if (Self.FObjects$1.indexOf(Transaction)<0) {
            Self.FObjects$1.push(Transaction);
            Result = true;
         } else {
            TW3OwnedErrorObject.SetLastErrorF$1(Self,"Method %s failed, transaction uknown error",["TW3Database.RegisterTransaction"]);
         }
      } else {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"Method %s failed, transaction was nil error",["TW3Database.RegisterTransaction"]);
      }
      return Result
   }
   /// procedure TW3Database.SetLocation(const NewLocation: String)
   ,SetLocation:function(Self, NewLocation) {
      Self.FLocation = NewLocation;
   }
   /// procedure TW3Database.SetState(const NewState: TW3DatabaseState)
   ,SetState:function(Self, NewState$1) {
      TW3OwnedErrorObject.ClearLastError$1(Self);
      if (NewState$1!=Self.FState) {
         Self.FState = NewState$1;
         if (Self.OnStateChanged) {
            Self.OnStateChanged(Self,Self.FState);
         }
      }
   }
   /// function TW3Database.UnRegisterTransaction(Transaction: TW3Transaction) : Boolean
   ,UnRegisterTransaction:function(Self, Transaction$1) {
      var Result = false;
      var idx$2 = 0;
      TW3OwnedErrorObject.ClearLastError$1(Self);
      if (Transaction$1!==null) {
         idx$2 = Self.FObjects$1.indexOf(Transaction$1);
         if (idx$2>=0) {
            Self.FObjects$1.splice(idx$2,1)
            ;
            Result = true;
         } else {
            TW3OwnedErrorObject.SetLastErrorF$1(Self,"Method %s failed, transaction uknown error",["TW3Database.UnRegisterTransaction"]);
         }
      } else {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"Method %s failed, transaction was nil error",["TW3Database.UnRegisterTransaction"]);
      }
      return Result
   }
   ,Destroy:TW3Component.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,SetLastError$1:TW3OwnedErrorObject.SetLastError$1
   ,ObjectLocked$2:TW3OwnedLockedErrorObject.ObjectLocked$2
   ,ObjectUnLocked$2:TW3OwnedLockedErrorObject.ObjectUnLocked$2
   ,Create$44:TW3Component.Create$44
   ,CreateEx:TW3Component.CreateEx
   ,FinalizeObject:TW3Component.FinalizeObject
   ,FreeAfter:TW3Component.FreeAfter
   ,InitializeObject:TW3Component.InitializeObject
   ,apiAfterClose$:function($){return $.ClassType.apiAfterClose.apply($.ClassType, arguments)}
   ,apiAfterOpen$:function($){return $.ClassType.apiAfterOpen.apply($.ClassType, arguments)}
   ,apiBeforeClose$:function($){return $.ClassType.apiBeforeClose($)}
   ,apiBeforeOpen$:function($){return $.ClassType.apiBeforeOpen($)}
   ,apiClose$:function($){return $.ClassType.apiClose.apply($.ClassType, arguments)}
   ,apiOpen$:function($){return $.ClassType.apiOpen.apply($.ClassType, arguments)}
   ,CreateReadTransaction$:function($){return $.ClassType.CreateReadTransaction.apply($.ClassType, arguments)}
   ,CreateWriteTransaction$:function($){return $.ClassType.CreateWriteTransaction.apply($.ClassType, arguments)}
   ,GetActive$1$:function($){return $.ClassType.GetActive$1($)}
   ,GetState$:function($){return $.ClassType.GetState($)}
   ,SetLocation$:function($){return $.ClassType.SetLocation.apply($.ClassType, arguments)}
   ,SetState$:function($){return $.ClassType.SetState.apply($.ClassType, arguments)}
};
TW3Database.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// EW3TransactionParameters = class (EW3Exception)
var EW3TransactionParameters = {
   $ClassName:"EW3TransactionParameters",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EW3Database = class (EW3Exception)
var EW3Database = {
   $ClassName:"EW3Database",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EW3Transaction = class (EW3Database)
var EW3Transaction = {
   $ClassName:"EW3Transaction",$Parent:EW3Database
   ,$Init:function ($) {
      EW3Database.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TW3DatasetObject = class (TW3OwnedLockedObject)
var TW3DatasetObject = {
   $ClassName:"TW3DatasetObject",$Parent:TW3OwnedLockedObject
   ,$Init:function ($) {
      TW3OwnedLockedObject.$Init($);
      $.FReadOnly = false;
   }
   /// procedure TW3DatasetObject.ObjectLocked()
   ,ObjectLocked:function(Self) {
      TW3DatasetObject.SetReadOnly$(Self,true);
      TW3OwnedLockedObject.ObjectLocked(Self);
   }
   /// procedure TW3DatasetObject.ObjectUnLocked()
   ,ObjectUnLocked:function(Self) {
      TW3DatasetObject.SetReadOnly$(Self,false);
      TW3OwnedLockedObject.ObjectUnLocked(Self);
   }
   /// procedure TW3DatasetObject.SetReadOnly(const NewState: Boolean)
   ,SetReadOnly:function(Self, NewState$2) {
      Self.FReadOnly = NewState$2;
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked$:function($){return $.ClassType.ObjectLocked($)}
   ,ObjectUnLocked$:function($){return $.ClassType.ObjectUnLocked($)}
   ,SetReadOnly$:function($){return $.ClassType.SetReadOnly.apply($.ClassType, arguments)}
};
TW3DatasetObject.$Intf={
   IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TW3DatasetField = class (TW3DatasetObject)
var TW3DatasetField = {
   $ClassName:"TW3DatasetField",$Parent:TW3DatasetObject
   ,$Init:function ($) {
      TW3DatasetObject.$Init($);
      $.FKind = 0;
      $.FName$1 = "";
      $.FValue = undefined;
   }
   /// anonymous TSourceMethodSymbol
   ,a$129:function(Self, Value$155) {
      Self.FValue = Value$155;
   }
   /// anonymous TSourceMethodSymbol
   ,a$128:function(Self) {
      return TVariant.AsFloat(Self.FValue);
   }
   /// anonymous TSourceMethodSymbol
   ,a$127:function(Self, Value$156) {
      Self.FValue = Value$156;
   }
   /// anonymous TSourceMethodSymbol
   ,a$126:function(Self) {
      return TVariant.AsBool(Self.FValue);
   }
   /// anonymous TSourceMethodSymbol
   ,a$125:function(Self, Value$157) {
      Self.FValue = Value$157;
   }
   /// anonymous TSourceMethodSymbol
   ,a$124:function(Self) {
      return TVariant.AsInteger(Self.FValue);
   }
   /// anonymous TSourceMethodSymbol
   ,a$123:function(Self, Value$158) {
      Self.FValue = Value$158;
   }
   /// anonymous TSourceMethodSymbol
   ,a$122:function(Self) {
      return TVariant.AsString(Self.FValue);
   }
   /// function TW3DatasetField.AcceptOwner(const CandidateObject: TObject) : Boolean
   ,AcceptOwner:function(Self, CandidateObject$2) {
      return CandidateObject$2!==null&&$Is(CandidateObject$2,TW3DatasetFields);
   }
   /// constructor TW3DatasetField.Create(const Owner: TW3DatasetFields)
   ,Create$75:function(Self, Owner$11) {
      TW3OwnedObject.Create$16(Self,Owner$11);
      Self.FKind = 0;
      Self.FName$1 = "Field"+TW3Identifiers.GenerateUniqueNumber(TW3Identifiers).toString();
      return Self
   }
   /// procedure TW3DatasetField.Generate()
   ,Generate$2:function(Self) {
      /* null */
   }
   /// function TW3DatasetField.GetAsDateTime() : TDateTime
   ,GetAsDateTime:function(Self) {
      return Self.FValue;
   }
   /// function TW3DatasetField.GetGenerated() : Boolean
   ,GetGenerated:function(Self) {
      return false;
   }
   /// function TW3DatasetField.GetOwner() : TW3DatasetFields
   ,GetOwner$1:function(Self) {
      return $As(TW3OwnedObject.GetOwner(Self),TW3DatasetFields);
   }
   /// function TW3DatasetField.GetValue() : Variant
   ,GetValue:function(Self) {
      return Self.FValue;
   }
   /// procedure TW3DatasetField.LoadFromStream(const Stream: TStream)
   ,LoadFromStream$5:function(Self, Stream$11) {
      var Reader = null;
      Reader = TW3CustomReader.Create$39$($New(TReader),$AsIntf(Stream$11,"IBinaryTransport"));
      try {
         if (TW3CustomReader.ReadInteger(Reader)==3133065982) {
            Self.FKind = TW3CustomReader.ReadInteger(Reader);
            Self.FName$1 = TW3CustomReader.ReadString$1(Reader);
            TW3DatasetField.ReadBinaryValue$(Self,Reader);
         } else {
            throw Exception.Create($New(EW3DatasetField),$R[37]);
         }
      } finally {
         TObject.Free(Reader);
      }
   }
   /// procedure TW3DatasetField.ReadBinaryValue(const Reader: TReader)
   ,ReadBinaryValue:function(Self, Reader$1) {
      switch (Self.FKind) {
         case 1 :
            TW3DatasetField.SetValue$(Self,TW3CustomReader.ReadBoolean(Reader$1));
            break;
         case 2 :
            TW3DatasetField.SetValue$(Self,TW3CustomReader.ReadInteger(Reader$1));
            break;
         case 3 :
            TW3DatasetField.SetValue$(Self,TW3CustomReader.ReadDouble(Reader$1));
            break;
         case 4 :
            TW3DatasetField.SetValue$(Self,TW3CustomReader.ReadString$1(Reader$1));
            break;
         case 5 :
            TW3DatasetField.SetValue$(Self,TW3CustomReader.ReadDouble(Reader$1));
            break;
         case 6 :
            TW3DatasetField.SetValue$(Self,TW3CustomReader.ReadInteger(Reader$1));
            break;
         case 7 :
            TW3DatasetField.SetValue$(Self,TW3CustomReader.ReadString$1(Reader$1));
            break;
         default :
            throw Exception.Create($New(EW3DatasetField),$R[54]);
      }
   }
   /// procedure TW3DatasetField.SaveToStream(const Stream: TStream)
   ,SaveToStream$5:function(Self, Stream$12) {
      var Writer = null;
      Writer = TW3CustomWriter.Create$29$($New(TWriter),$AsIntf(Stream$12,"IBinaryTransport"));
      try {
         TW3CustomWriter.WriteInteger(Writer,3133065982);
         TW3CustomWriter.WriteInteger(Writer,Self.FKind);
         TW3CustomWriter.WriteString(Writer,Self.FName$1);
         TW3DatasetField.WriteBinaryValue$(Self,Writer);
      } finally {
         TObject.Free(Writer);
      }
   }
   /// procedure TW3DatasetField.SetAsDateTime(const NewValue: TDateTime)
   ,SetAsDateTime:function(Self, NewValue$1) {
      Self.FValue = NewValue$1;
   }
   /// procedure TW3DatasetField.SetKind(const NewFieldType: TW3DatasetFieldType)
   ,SetKind:function(Self, NewFieldType) {
      Self.FKind = NewFieldType;
   }
   /// procedure TW3DatasetField.SetName(NewName: String)
   ,SetName$2:function(Self, NewName) {
      if (TW3OwnedLockedObject.GetLockState(Self)) {
         throw Exception.Create($New(EW3DatasetField),$R[32]);
      } else {
         NewName = (Trim$_String_(NewName)).toLocaleLowerCase();
         if (NewName.length>0) {
            Self.FName$1 = NewName;
         } else {
            throw Exception.Create($New(EW3DatasetField),"Invalid fieldname, string was empty error");
         }
      }
   }
   /// procedure TW3DatasetField.SetValue(const NewValue: Variant)
   ,SetValue:function(Self, NewValue$2) {
      Self.FValue = NewValue$2;
   }
   /// procedure TW3DatasetField.WriteBinaryValue(const Writer: TWriter)
   ,WriteBinaryValue:function(Self, Writer$1) {
      switch (Self.FKind) {
         case 1 :
            TW3CustomWriter.WriteBoolean(Writer$1,TW3DatasetField.a$126(Self));
            break;
         case 2 :
            TW3CustomWriter.WriteInteger(Writer$1,TW3DatasetField.a$124(Self));
            break;
         case 3 :
            TW3CustomWriter.WriteDouble(Writer$1,TW3DatasetField.a$128(Self));
            break;
         case 4 :
            TW3CustomWriter.WriteString(Writer$1,TW3DatasetField.a$122(Self));
            break;
         case 5 :
            TW3CustomWriter.WriteDouble(Writer$1,TW3DatasetField.a$128(Self));
            break;
         case 6 :
            TW3CustomWriter.WriteInteger(Writer$1,TW3DatasetField.a$124(Self));
            break;
         case 7 :
            TW3CustomWriter.WriteString(Writer$1,TW3DatasetField.a$122(Self));
            break;
         default :
            throw Exception.Create($New(EW3DatasetField),$R[54]);
      }
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3DatasetObject.ObjectLocked
   ,ObjectUnLocked:TW3DatasetObject.ObjectUnLocked
   ,SetReadOnly:TW3DatasetObject.SetReadOnly
   ,Create$75$:function($){return $.ClassType.Create$75.apply($.ClassType, arguments)}
   ,Generate$2$:function($){return $.ClassType.Generate$2($)}
   ,GetGenerated$:function($){return $.ClassType.GetGenerated($)}
   ,GetOwner$1$:function($){return $.ClassType.GetOwner$1($)}
   ,GetValue$:function($){return $.ClassType.GetValue($)}
   ,LoadFromStream$5$:function($){return $.ClassType.LoadFromStream$5.apply($.ClassType, arguments)}
   ,ReadBinaryValue$:function($){return $.ClassType.ReadBinaryValue.apply($.ClassType, arguments)}
   ,SaveToStream$5$:function($){return $.ClassType.SaveToStream$5.apply($.ClassType, arguments)}
   ,SetKind$:function($){return $.ClassType.SetKind.apply($.ClassType, arguments)}
   ,SetName$2$:function($){return $.ClassType.SetName$2.apply($.ClassType, arguments)}
   ,SetValue$:function($){return $.ClassType.SetValue.apply($.ClassType, arguments)}
   ,WriteBinaryValue$:function($){return $.ClassType.WriteBinaryValue.apply($.ClassType, arguments)}
};
TW3DatasetField.$Intf={
   IW3OwnedObjectAccess:[TW3DatasetField.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// TW3StringField = class (TW3DatasetField)
var TW3StringField = {
   $ClassName:"TW3StringField",$Parent:TW3DatasetField
   ,$Init:function ($) {
      TW3DatasetField.$Init($);
   }
   /// anonymous TSourceMethodSymbol
   ,a$131:function(Self) {
      return TVariant.AsString(TW3DatasetField.GetValue(Self));
   }
   /// anonymous TSourceMethodSymbol
   ,a$130:function(Self, Value$159) {
      TW3DatasetField.SetValue(Self,Value$159);
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3DatasetField.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3DatasetObject.ObjectLocked
   ,ObjectUnLocked:TW3DatasetObject.ObjectUnLocked
   ,SetReadOnly:TW3DatasetObject.SetReadOnly
   ,Create$75:TW3DatasetField.Create$75
   ,Generate$2:TW3DatasetField.Generate$2
   ,GetGenerated:TW3DatasetField.GetGenerated
   ,GetOwner$1:TW3DatasetField.GetOwner$1
   ,GetValue:TW3DatasetField.GetValue
   ,LoadFromStream$5:TW3DatasetField.LoadFromStream$5
   ,ReadBinaryValue:TW3DatasetField.ReadBinaryValue
   ,SaveToStream$5:TW3DatasetField.SaveToStream$5
   ,SetKind:TW3DatasetField.SetKind
   ,SetName$2:TW3DatasetField.SetName$2
   ,SetValue:TW3DatasetField.SetValue
   ,WriteBinaryValue:TW3DatasetField.WriteBinaryValue
};
TW3StringField.$Intf={
   IW3OwnedObjectAccess:[TW3DatasetField.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// TW3IntegerField = class (TW3DatasetField)
var TW3IntegerField = {
   $ClassName:"TW3IntegerField",$Parent:TW3DatasetField
   ,$Init:function ($) {
      TW3DatasetField.$Init($);
   }
   /// anonymous TSourceMethodSymbol
   ,a$133:function(Self) {
      return TVariant.AsInteger(TW3DatasetField.GetValue(Self));
   }
   /// anonymous TSourceMethodSymbol
   ,a$132:function(Self, Value$160) {
      TW3DatasetField.SetValue(Self,Value$160);
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3DatasetField.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3DatasetObject.ObjectLocked
   ,ObjectUnLocked:TW3DatasetObject.ObjectUnLocked
   ,SetReadOnly:TW3DatasetObject.SetReadOnly
   ,Create$75:TW3DatasetField.Create$75
   ,Generate$2:TW3DatasetField.Generate$2
   ,GetGenerated:TW3DatasetField.GetGenerated
   ,GetOwner$1:TW3DatasetField.GetOwner$1
   ,GetValue:TW3DatasetField.GetValue
   ,LoadFromStream$5:TW3DatasetField.LoadFromStream$5
   ,ReadBinaryValue:TW3DatasetField.ReadBinaryValue
   ,SaveToStream$5:TW3DatasetField.SaveToStream$5
   ,SetKind:TW3DatasetField.SetKind
   ,SetName$2:TW3DatasetField.SetName$2
   ,SetValue:TW3DatasetField.SetValue
   ,WriteBinaryValue:TW3DatasetField.WriteBinaryValue
};
TW3IntegerField.$Intf={
   IW3OwnedObjectAccess:[TW3DatasetField.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// TW3GUIDField = class (TW3DatasetField)
var TW3GUIDField = {
   $ClassName:"TW3GUIDField",$Parent:TW3DatasetField
   ,$Init:function ($) {
      TW3DatasetField.$Init($);
   }
   /// procedure TW3GUIDField.Generate()
   ,Generate$2:function(Self) {
      TW3DatasetField.SetValue(Self,(TString.CreateGUID(TString)).toLocaleUpperCase());
   }
   /// function TW3GUIDField.GetGenerated() : Boolean
   ,GetGenerated:function(Self) {
      return true;
   }
   /// procedure TW3GUIDField.WriteBinaryValue(const Writer: TWriter)
   ,WriteBinaryValue:function(Self, Writer$2) {
      TW3CustomWriter.WriteString(Writer$2,TW3DatasetField.a$122(Self));
   }
   /// procedure TW3GUIDField.ReadBinaryValue(const Reader: TReader)
   ,ReadBinaryValue:function(Self, Reader$2) {
      TW3DatasetField.SetValue$(Self,TW3CustomReader.ReadString$1(Reader$2));
   }
   /// anonymous TSourceMethodSymbol
   ,a$134:function(Self) {
      return TVariant.AsString(TW3DatasetField.GetValue(Self));
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3DatasetField.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3DatasetObject.ObjectLocked
   ,ObjectUnLocked:TW3DatasetObject.ObjectUnLocked
   ,SetReadOnly:TW3DatasetObject.SetReadOnly
   ,Create$75:TW3DatasetField.Create$75
   ,Generate$2$:function($){return $.ClassType.Generate$2($)}
   ,GetGenerated$:function($){return $.ClassType.GetGenerated($)}
   ,GetOwner$1:TW3DatasetField.GetOwner$1
   ,GetValue:TW3DatasetField.GetValue
   ,LoadFromStream$5:TW3DatasetField.LoadFromStream$5
   ,ReadBinaryValue$:function($){return $.ClassType.ReadBinaryValue.apply($.ClassType, arguments)}
   ,SaveToStream$5:TW3DatasetField.SaveToStream$5
   ,SetKind:TW3DatasetField.SetKind
   ,SetName$2:TW3DatasetField.SetName$2
   ,SetValue:TW3DatasetField.SetValue
   ,WriteBinaryValue$:function($){return $.ClassType.WriteBinaryValue.apply($.ClassType, arguments)}
};
TW3GUIDField.$Intf={
   IW3OwnedObjectAccess:[TW3DatasetField.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// TW3FloatField = class (TW3DatasetField)
var TW3FloatField = {
   $ClassName:"TW3FloatField",$Parent:TW3DatasetField
   ,$Init:function ($) {
      TW3DatasetField.$Init($);
   }
   /// anonymous TSourceMethodSymbol
   ,a$136:function(Self) {
      return TVariant.AsFloat(TW3DatasetField.GetValue(Self));
   }
   /// anonymous TSourceMethodSymbol
   ,a$135:function(Self, Value$161) {
      TW3DatasetField.SetValue(Self,Value$161);
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3DatasetField.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3DatasetObject.ObjectLocked
   ,ObjectUnLocked:TW3DatasetObject.ObjectUnLocked
   ,SetReadOnly:TW3DatasetObject.SetReadOnly
   ,Create$75:TW3DatasetField.Create$75
   ,Generate$2:TW3DatasetField.Generate$2
   ,GetGenerated:TW3DatasetField.GetGenerated
   ,GetOwner$1:TW3DatasetField.GetOwner$1
   ,GetValue:TW3DatasetField.GetValue
   ,LoadFromStream$5:TW3DatasetField.LoadFromStream$5
   ,ReadBinaryValue:TW3DatasetField.ReadBinaryValue
   ,SaveToStream$5:TW3DatasetField.SaveToStream$5
   ,SetKind:TW3DatasetField.SetKind
   ,SetName$2:TW3DatasetField.SetName$2
   ,SetValue:TW3DatasetField.SetValue
   ,WriteBinaryValue:TW3DatasetField.WriteBinaryValue
};
TW3FloatField.$Intf={
   IW3OwnedObjectAccess:[TW3DatasetField.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// TW3FilterValue = class (TObject)
var TW3FilterValue = {
   $ClassName:"TW3FilterValue",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.RawValue = "";
      $.IsField = false;
      $.DataField = $.FFilter = null;
   }
   /// function TW3FilterValue.AsBoolean() : Boolean
   ,AsBoolean:function(Self) {
      var Result = {v:false};
      try {
         if (TW3FilterValue.GetDataType(Self)==1) {
            if (Self.IsField) {
               Result.v = TW3DatasetField.a$126(Self.DataField);
            } else if (!TryStrToBool(Self.RawValue,Result)) {
               throw EW3Exception.CreateFmt$($New(EW3FilterValue),"Failed to convert [%s] to boolean error",[Self.RawValue]);
            }
         } else {
            throw EW3Exception.CreateFmt$($New(EW3FilterValue),"Value [%s] does not resolve to boolean error",[Self.RawValue]);
         }
      } finally {return Result.v}
   }
   /// function TW3FilterValue.AsFloat() : Float
   ,AsFloat$2:function(Self) {
      var Result = {v:0};
      try {
         if (TW3FilterValue.GetDataType(Self)==9) {
            if (Self.IsField) {
               Result.v = TW3DatasetField.a$128(Self.DataField);
            } else if (!TryStrToFloat(Self.RawValue,Result)) {
               throw EW3Exception.CreateFmt$($New(EW3FilterValue),"Failed to convert [%s] to float error",[Self.RawValue]);
            }
         } else {
            throw EW3Exception.CreateFmt$($New(EW3FilterValue),"Value [%s] does not resolve to float error",[Self.RawValue]);
         }
      } finally {return Result.v}
   }
   /// function TW3FilterValue.AsInteger() : Integer
   ,AsInteger$1:function(Self) {
      var Result = {v:0};
      try {
         if (TW3FilterValue.GetDataType(Self)==7) {
            if (Self.IsField) {
               Result.v = TW3DatasetField.a$124(Self.DataField);
            } else if (!TryStrToInt(Self.RawValue,Result)) {
               throw EW3Exception.CreateFmt$($New(EW3FilterValue),"Failed to convert [%s] to integer error",[Self.RawValue]);
            }
         } else {
            throw EW3Exception.CreateFmt$($New(EW3FilterValue),"Value [%s] does not resolve to integer error",[Self.RawValue]);
         }
      } finally {return Result.v}
   }
   /// function TW3FilterValue.AsString() : String
   ,AsString$2:function(Self) {
      var Result = "";
      var LKind$1 = 0;
      LKind$1 = TW3FilterValue.GetDataType(Self);
      if (LKind$1==10) {
         if (Self.IsField) {
            Result = TW3DatasetField.a$122(Self.DataField);
         } else if (TString.GetStringIsQuoted(Self.RawValue)) {
            Result = TString.UnQuoteString(Self.RawValue);
         } else {
            throw EW3Exception.CreateFmt$($New(EW3FilterValue),"Expected quoted string [%s] error",[Self.RawValue]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EW3FilterValue),"Value [%s] did not resolve to string but [%d] error",[Self.RawValue, LKind$1]);
      }
      return Result
   }
   /// function TW3FilterValue.AsVariant() : Variant
   ,AsVariant:function(Self) {
      var Result = undefined;
      if (Self.IsField) {
         Result = TW3DatasetField.GetValue$(Self.DataField);
      } else {
         Result = ((Self.RawValue).toLocaleLowerCase()=="null")?undefined:Self.RawValue;
      }
      return Result
   }
   /// constructor TW3FilterValue.Create(const Filter: TW3DatasetFilter)
   ,Create$76:function(Self, Filter$1) {
      TObject.Create(Self);
      Self.FFilter = Filter$1;
      return Self
   }
   /// function TW3FilterValue.Equals(const WithThis: TW3FilterValue) : Boolean
   ,Equals$4:function(Self, WithThis) {
      var Result = false;
      var ThisType = 0,
         ThatType = 0;
      if (Self.FFilter!==null) {
         ThisType = TW3FilterValue.GetDataType(Self);
         ThatType = TW3FilterValue.GetDataType(WithThis);
         if (ThisType==ThatType) {
            switch (ThisType) {
               case 0 :
                  Result = TW3FilterValue.AsVariant(Self)==TW3FilterValue.AsVariant(WithThis);
                  break;
               case 1 :
                  Result = TW3FilterValue.AsBoolean(Self)==TW3FilterValue.AsBoolean(WithThis);
                  break;
               case 7 :
                  Result = TW3FilterValue.AsInteger$1(Self)==TW3FilterValue.AsInteger$1(WithThis);
                  break;
               case 9 :
                  Result = TW3FilterValue.AsFloat$2(Self)==TW3FilterValue.AsFloat$2(WithThis);
                  break;
               case 10 :
                  Result = TW3FilterValue.AsString$2(Self)==TW3FilterValue.AsString$2(WithThis);
                  break;
            }
         } else {
            if (!ThatType) {
               switch (ThisType) {
                  case 0 :
                     Result = TW3FilterValue.AsVariant(Self)==TW3FilterValue.AsVariant(WithThis);
                     break;
                  case 1 :
                     Result = TW3FilterValue.AsBoolean(Self)==TW3FilterValue.AsVariant(WithThis);
                     break;
                  case 7 :
                     Result = TW3FilterValue.AsInteger$1(Self)==TW3FilterValue.AsVariant(WithThis);
                     break;
                  case 9 :
                     Result = TW3FilterValue.AsFloat$2(Self)==TW3FilterValue.AsVariant(WithThis);
                     break;
                  case 10 :
                     Result = TW3FilterValue.AsString$2(Self)==TW3FilterValue.AsVariant(WithThis);
                     break;
               }
            } else if (!ThisType) {
               switch (ThatType) {
                  case 0 :
                     Result = TW3FilterValue.AsVariant(Self)==TW3FilterValue.AsVariant(WithThis);
                     break;
                  case 1 :
                     Result = TW3FilterValue.AsVariant(Self)==TW3FilterValue.AsBoolean(WithThis);
                     break;
                  case 7 :
                     Result = TW3FilterValue.AsVariant(Self)==TW3FilterValue.AsInteger$1(WithThis);
                     break;
                  case 9 :
                     Result = TW3FilterValue.AsVariant(Self)==TW3FilterValue.AsFloat$2(WithThis);
                     break;
                  case 10 :
                     Result = TW3FilterValue.AsVariant(Self)==TW3FilterValue.AsString$2(WithThis);
                     break;
               }
            }
         }
      } else {
         throw Exception.Create($New(EW3FilterValue),"Evaluation failed, no filter reference set error");
      }
      return Result
   }
   /// function TW3FilterValue.GetDataType() : TRTLDatatype
   ,GetDataType:function(Self) {
      var Result = 0;
      var TempBool = {v:false};
      Result = 0;
      if (Self.IsField) {
         switch (Self.DataField.FKind) {
            case 1 :
               Result = 1;
               break;
            case 6 :
            case 2 :
               Result = 7;
               break;
            case 3 :
            case 5 :
               Result = 9;
               break;
            case 7 :
            case 4 :
               Result = 10;
               break;
            default :
               Result = 0;
         }
      } else {
         if (TStringHelper$ContainsHex(Self.RawValue)) {
            Result = 7;
         } else if (TStringHelper$ContainsOrdinal(Self.RawValue)) {
            Result = 7;
         } else if (TStringHelper$ContainsFloat(Self.RawValue)) {
            Result = 9;
         } else if (TString.GetStringIsQuoted(Self.RawValue)) {
            Result = 10;
         } else {
            if ((Self.RawValue).toLocaleLowerCase()=="null") {
               Result = 0;
               return Result;
            }
            TempBool.v = false;
            if (TryStrToBool(Self.RawValue,TempBool)) {
               Result = 1;
            } else {
               Result = 0;
            }
         }
      }
      return Result
   }
   /// function TW3FilterValue.Less(const WithThis: TW3FilterValue) : Boolean
   ,Less$1:function(Self, WithThis$1) {
      var Result = false;
      var ThisType$1 = 0,
         ThatType$1 = 0;
      if (Self.FFilter!==null) {
         ThisType$1 = TW3FilterValue.GetDataType(Self);
         ThatType$1 = TW3FilterValue.GetDataType(WithThis$1);
         if (ThisType$1==ThatType$1) {
            switch (ThisType$1) {
               case 1 :
                  Result = TW3FilterValue.AsBoolean(Self)==false&&TW3FilterValue.AsBoolean(WithThis$1)==true;
                  break;
               case 7 :
                  Result = TW3FilterValue.AsInteger$1(Self)<TW3FilterValue.AsInteger$1(WithThis$1);
                  break;
               case 9 :
                  Result = TW3FilterValue.AsFloat$2(Self)<TW3FilterValue.AsFloat$2(WithThis$1);
                  break;
               case 10 :
                  Result = TW3FilterValue.AsString$2(Self)<TW3FilterValue.AsString$2(WithThis$1);
                  break;
            }
         } else {
            if (!ThatType$1) {
               try {
                  switch (ThisType$1) {
                     case 0 :
                        Result = TW3FilterValue.AsVariant(Self)<TW3FilterValue.AsVariant(WithThis$1);
                        break;
                     case 1 :
                        Result = TW3FilterValue.AsBoolean(Self)==false&&TW3FilterValue.AsVariant(WithThis$1)==true;
                        break;
                     case 7 :
                        Result = TW3FilterValue.AsInteger$1(Self)<TW3FilterValue.AsVariant(WithThis$1);
                        break;
                     case 9 :
                        Result = TW3FilterValue.AsFloat$2(Self)<TW3FilterValue.AsVariant(WithThis$1);
                        break;
                     case 10 :
                        Result = TW3FilterValue.AsString$2(Self)<TW3FilterValue.AsVariant(WithThis$1);
                        break;
                  }
               } catch ($e) {
                  var e$27 = $W($e);
                  /* null */
               }
            } else if (!ThisType$1) {
               try {
                  switch (ThatType$1) {
                     case 0 :
                        Result = TW3FilterValue.AsVariant(Self)<TW3FilterValue.AsVariant(WithThis$1);
                        break;
                     case 1 :
                        Result = TW3FilterValue.AsVariant(Self)==false&&TW3FilterValue.AsBoolean(WithThis$1)==true;
                        break;
                     case 7 :
                        Result = TW3FilterValue.AsVariant(Self)<TW3FilterValue.AsInteger$1(WithThis$1);
                        break;
                     case 9 :
                        Result = TW3FilterValue.AsVariant(Self)<TW3FilterValue.AsFloat$2(WithThis$1);
                        break;
                     case 10 :
                        Result = TW3FilterValue.AsVariant(Self)<TW3FilterValue.AsString$2(WithThis$1);
                        break;
                  }
               } catch ($e) {
                  var e$28 = $W($e);
                  /* null */
               }
            }
         }
      } else {
         throw Exception.Create($New(EW3FilterValue),"Evaluation failed, no filter reference set error");
      }
      return Result
   }
   /// function TW3FilterValue.Like(const WithThis: TW3FilterValue) : Boolean
   ,Like:function(Self, WithThis$2) {
      var Result = false;
      var ThisType$2 = 0,
         ThatType$2 = 0,
         left = "",
         right = {v:""},
         index$5 = 0,
         index$6 = 0,
         index$7 = 0;
      if (Self.FFilter!==null) {
         ThisType$2 = TW3FilterValue.GetDataType(Self);
         ThatType$2 = TW3FilterValue.GetDataType(WithThis$2);
         if (ThisType$2==ThatType$2) {
            if (ThisType$2==10) {
               left = TW3FilterValue.AsString$2(Self);
               right.v = TW3FilterValue.AsString$2(WithThis$2);
               if ((right.v.charAt(0)=="%")&&StrEndsWith(right.v,"%")) {
                  Delete(right,1,1);
                  Delete(right,right.v.length,1);
                  index$5 = (left.indexOf(right.v)+1);
                  Result = index$5>0;
               } else if ((right.v.charAt(0)=="%")) {
                  Delete(right,1,1);
                  index$6 = (left.indexOf(right.v)+1);
                  Result = index$6>1;
               } else if (StrEndsWith(right.v,"%")) {
                  Delete(right,right.v.length,1);
                  index$7 = (left.indexOf(right.v)+1);
                  Result = index$7==1;
               }
            } else {
               throw Exception.Create($New(EW3FilterValue),"Evaluation failed, LIKE expects string-to-string expression error");
            }
         } else {
            throw Exception.Create($New(EW3FilterValue),"Evaluation failed, LIKE expects string-to-string expression error");
         }
      } else {
         throw Exception.Create($New(EW3FilterValue),"Evaluation failed, no filter reference set error");
      }
      return Result
   }
   /// function TW3FilterValue.More(const WithThis: TW3FilterValue) : Boolean
   ,More$1:function(Self, WithThis$3) {
      var Result = false;
      var ThisType$3 = 0,
         ThatType$3 = 0;
      if (Self.FFilter!==null) {
         ThisType$3 = TW3FilterValue.GetDataType(Self);
         ThatType$3 = TW3FilterValue.GetDataType(WithThis$3);
         if (ThisType$3==TW3FilterValue.GetDataType(WithThis$3)) {
            switch (ThisType$3) {
               case 1 :
                  Result = TW3FilterValue.AsBoolean(Self)==true&&TW3FilterValue.AsBoolean(WithThis$3)==false;
                  break;
               case 7 :
                  Result = TW3FilterValue.AsInteger$1(Self)>TW3FilterValue.AsInteger$1(WithThis$3);
                  break;
               case 9 :
                  Result = TW3FilterValue.AsFloat$2(Self)>TW3FilterValue.AsFloat$2(WithThis$3);
                  break;
               case 10 :
                  Result = TW3FilterValue.AsString$2(Self)>TW3FilterValue.AsString$2(WithThis$3);
                  break;
            }
         } else {
            if (!ThatType$3) {
               try {
                  switch (ThisType$3) {
                     case 0 :
                        Result = TW3FilterValue.AsVariant(Self)>TW3FilterValue.AsVariant(WithThis$3);
                        break;
                     case 1 :
                        Result = TW3FilterValue.AsBoolean(Self)==true&&TW3FilterValue.AsVariant(WithThis$3)==false;
                        break;
                     case 7 :
                        Result = TW3FilterValue.AsInteger$1(Self)>TW3FilterValue.AsVariant(WithThis$3);
                        break;
                     case 9 :
                        Result = TW3FilterValue.AsFloat$2(Self)>TW3FilterValue.AsVariant(WithThis$3);
                        break;
                     case 10 :
                        Result = TW3FilterValue.AsString$2(Self)>TW3FilterValue.AsVariant(WithThis$3);
                        break;
                  }
               } catch ($e) {
                  var e$29 = $W($e);
                  /* null */
               }
            } else if (!ThisType$3) {
               try {
                  switch (ThatType$3) {
                     case 0 :
                        Result = TW3FilterValue.AsVariant(Self)>TW3FilterValue.AsVariant(WithThis$3);
                        break;
                     case 1 :
                        Result = TW3FilterValue.AsVariant(Self)==true&&TW3FilterValue.AsBoolean(WithThis$3)==false;
                        break;
                     case 7 :
                        Result = TW3FilterValue.AsVariant(Self)>TW3FilterValue.AsInteger$1(WithThis$3);
                        break;
                     case 9 :
                        Result = TW3FilterValue.AsVariant(Self)>TW3FilterValue.AsFloat$2(WithThis$3);
                        break;
                     case 10 :
                        Result = TW3FilterValue.AsVariant(Self)>TW3FilterValue.AsString$2(WithThis$3);
                        break;
                  }
               } catch ($e) {
                  var e$30 = $W($e);
                  /* null */
               }
            }
         }
      } else {
         throw Exception.Create($New(EW3FilterValue),"Evaluation failed, no filter reference set error");
      }
      return Result
   }
   /// function TW3FilterValue.MoreLess(const WithThis: TW3FilterValue) : Boolean
   ,MoreLess:function(Self, WithThis$4) {
      return !TW3FilterValue.Equals$4(Self,WithThis$4);
   }
   /// function TW3FilterValue.NotEqual(const WithThis: TW3FilterValue) : Boolean
   ,NotEqual:function(Self, WithThis$5) {
      return !TW3FilterValue.Equals$4(Self,WithThis$5);
   }
   /// function TW3FilterValue.ToString() : String
   ,ToString$6:function(Self) {
      return (Self.IsField)?"["+Self.RawValue+"]":Self.RawValue;
   }
   ,Destroy:TObject.Destroy
   ,Create$76$:function($){return $.ClassType.Create$76.apply($.ClassType, arguments)}
};
/// TW3FilterSymbol enumeration
var TW3FilterSymbol = [ "fsymInvalid", "fsymEquals", "fsymGreater", "fsymLess", "fsymLessGreater", "fsymLike" ];
/// TW3FilterOperator enumeration
var TW3FilterOperator = [ "fopInvalid", "fopAnd", "fopOr", "fopNot" ];
/// TW3FilterExpression = class (TObject)
var TW3FilterExpression = {
   $ClassName:"TW3FilterExpression",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.Success = false;
      $.Operator = 0;
      $.Symbol = 0;
      $.FLeft = $.FRight = null;
   }
   /// constructor TW3FilterExpression.Create(const Filter: TW3DatasetFilter)
   ,Create$77:function(Self, Filter$2) {
      TObject.Create(Self);
      Self.FLeft = TW3FilterValue.Create$76$($New(TW3FilterValue),Filter$2);
      Self.FRight = TW3FilterValue.Create$76$($New(TW3FilterValue),Filter$2);
      Self.Operator = 1;
      Self.Symbol = 0;
      return Self
   }
   /// destructor TW3FilterExpression.Destroy()
   ,Destroy:function(Self) {
      TObject.Free(Self.FLeft);
      TObject.Free(Self.FRight);
      TObject.Destroy(Self);
   }
   /// function TW3FilterExpression.ToString() : String
   ,ToString$7:function(Self) {
      var Result = "";
      Result = "==================================\r";
      Result+="Filter Expression:\r";
      Result+=("Operator = "+TFilterOperatorHelper$ToString$12(Self.Operator).toString()+"\r");
      Result+=("Symbol   = "+TFilterSymbolHelper$ToString$13(Self.Symbol).toString()+"\r");
      Result+=("Value A  = "+TW3FilterValue.ToString$6(Self.FLeft).toString()+"\r");
      Result+=("Value B  = "+TW3FilterValue.ToString$6(Self.FRight).toString()+"\r");
      Result+="\r";
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$77$:function($){return $.ClassType.Create$77.apply($.ClassType, arguments)}
};
/// TW3FieldDefs = class (TW3DatasetObject)
var TW3FieldDefs = {
   $ClassName:"TW3FieldDefs",$Parent:TW3DatasetObject
   ,$Init:function ($) {
      TW3DatasetObject.$Init($);
      $.FFields = [];
      $.FId = 0;
   }
   /// anonymous TSourceMethodSymbol
   ,a$144:function(Self) {
      return Self.FFields.length;
   }
   /// anonymous TSourceMethodSymbol
   ,a$143:function(Self, index$8) {
      return Self.FFields[index$8];
   }
   /// function TW3FieldDefs.AcceptOwner(const CandidateObject: TObject) : Boolean
   ,AcceptOwner:function(Self, CandidateObject$3) {
      return CandidateObject$3!==null&&$Is(CandidateObject$3,TW3Dataset);
   }
   /// function TW3FieldDefs.Add(FieldName: String; const FieldType: TW3DatasetFieldType) : TW3FieldDef
   ,Add$1:function(Self, FieldName, FieldType) {
      var Result = null;
      if (TW3OwnedLockedObject.GetLockState(Self)) {
         throw Exception.Create($New(EW3FieldDefs),$R[36]);
      } else {
         FieldName = (Trim$_String_(FieldName)).toLocaleLowerCase();
         if (FieldName.length>0) {
            if (TW3FieldDefs.IndexOf$1(Self,FieldName)<0) {
               Result = TW3FieldDef.Create$79$($New(TW3FieldDef),Self);
               TW3FieldDef.SetName$3(Result,FieldName);
               TW3FieldDef.SetType(Result,FieldType);
               Self.FFields.push(Result);
            } else {
               throw Exception.Create($New(EW3FieldDefs),$R[38]);
            }
         } else {
            throw Exception.Create($New(EW3FieldDefs),$R[39]);
         }
      }
      return Result
   }
   /// procedure TW3FieldDefs.Clear()
   ,Clear$9:function(Self) {
      var a$472 = 0;
      var Field$1 = null;
      if (Self.FFields.length>0) {
         try {
            var a$473 = [];
            a$473 = Self.FFields;
            var $temp64;
            for(a$472=0,$temp64=a$473.length;a$472<$temp64;a$472++) {
               Field$1 = a$473[a$472];
               TObject.Free(Field$1);
            }
         } finally {
            Self.FFields.length=0;
         }
      }
   }
   /// constructor TW3FieldDefs.Create(const Owner: TW3Dataset)
   ,Create$78:function(Self, Owner$12) {
      TW3OwnedObject.Create$16(Self,Owner$12);
      return Self
   }
   /// procedure TW3FieldDefs.Delete(const Field: TW3FieldDef)
   ,Delete$4:function(Self, Field$2) {
      var FieldIndex = 0;
      if (TW3OwnedLockedObject.GetLockState(Self)) {
         throw Exception.Create($New(EW3FieldDefs),$R[36]);
      } else {
         if (Field$2!==null) {
            FieldIndex = Self.FFields.indexOf(Field$2);
            if (FieldIndex>=0) {
               TW3FieldDefs.Delete$3(Self,FieldIndex);
            }
         } else {
            throw Exception.Create($New(EW3FieldDefs),$R[40]);
         }
      }
   }
   /// procedure TW3FieldDefs.Delete(const FieldIndex: Integer)
   ,Delete$3:function(Self, FieldIndex$1) {
      if (TW3OwnedLockedObject.GetLockState(Self)) {
         throw Exception.Create($New(EW3FieldDefs),$R[36]);
      } else {
         if (FieldIndex$1>=0&&FieldIndex$1<Self.FFields.length) {
            TObject.Free(Self.FFields[FieldIndex$1]);
            Self.FFields.splice(FieldIndex$1,1)
            ;
         }
      }
   }
   /// destructor TW3FieldDefs.Destroy()
   ,Destroy:function(Self) {
      if (Self.FFields.length>0) {
         TW3FieldDefs.Clear$9(Self);
      }
      TObject.Destroy(Self);
   }
   /// function TW3FieldDefs.FieldByName(FieldName: String) : TW3FieldDef
   ,FieldByName:function(Self, FieldName$1) {
      var Result = null;
      var x$42 = 0;
      var Field$3 = null;
      if (Self.FFields.length>0) {
         FieldName$1 = (Trim$_String_(FieldName$1)).toLocaleLowerCase();
         if (FieldName$1.length>0) {
            var $temp65;
            for(x$42=0,$temp65=Self.FFields.length;x$42<$temp65;x$42++) {
               Field$3 = Self.FFields[x$42];
               if (Field$3.FName$2==FieldName$1) {
                  Result = Field$3;
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3FieldDefs.GetFieldIdentifier() : String
   ,GetFieldIdentifier:function(Self) {
      var Result = "";
      do {
         ++Self.FId;
         Result = "Field"+Self.FId.toString();
      } while (!(TW3FieldDefs.IndexOf$1(Self,Result)<0));
      return Result
   }
   /// function TW3FieldDefs.GetOwner() : TW3Dataset
   ,GetOwner$2:function(Self) {
      return $As(TW3OwnedObject.GetOwner(Self),TW3Dataset);
   }
   /// function TW3FieldDefs.IndexOf(FieldName: String) : Integer
   ,IndexOf$1:function(Self, FieldName$2) {
      var Result = 0;
      var x$43 = 0;
      var Field$4 = null;
      Result = -1;
      if (Self.FFields.length>0) {
         FieldName$2 = (Trim$_String_(FieldName$2)).toLocaleLowerCase();
         if (FieldName$2.length>0) {
            var $temp66;
            for(x$43=0,$temp66=Self.FFields.length;x$43<$temp66;x$43++) {
               Field$4 = Self.FFields[x$43];
               if (Field$4.FName$2==FieldName$2) {
                  Result = x$43;
                  break;
               }
            }
         }
      }
      return Result
   }
   /// procedure TW3FieldDefs.LoadFromJSON(const TextData: String)
   ,LoadFromJSON:function(Self, TextData) {
      var Header = {ddMagic:0,ddDefs:[]};
      var x$44 = 0;
      TW3FieldDefs.Clear$9(Self);
      try {
         Header = JSON.parse(TextData);
      } catch ($e) {
         var e$31 = $W($e);
         throw EW3Exception.CreateFmt$($New(EW3FieldDefs),"Failed to load field-definitions, system threw exception: %s",[e$31.FMessage]);
      }
      if (Header.ddMagic==3401235116) {
         if (Header.ddDefs.length>0) {
            var $temp67;
            for(x$44=0,$temp67=Header.ddDefs.length;x$44<$temp67;x$44++) {
               TW3FieldDefs.Add$1(Self,Header.ddDefs[x$44].fdName,Header.ddDefs[x$44].fdDatatype);
            }
         }
      } else {
         throw Exception.Create($New(EW3FieldDefs),$R[41]);
      }
   }
   /// procedure TW3FieldDefs.LoadFromStream(const Stream: TStream)
   ,LoadFromStream$6:function(Self, Stream$13) {
      var Reader$3 = null,
         ItemCount = 0,
         Item$2 = null;
      TW3FieldDefs.Clear$9(Self);
      Reader$3 = TW3CustomReader.Create$39$($New(TReader),$AsIntf(Stream$13,"IBinaryTransport"));
      try {
         if (TW3CustomReader.ReadInteger(Reader$3)==3401235116) {
            ItemCount = TW3CustomReader.ReadInteger(Reader$3);
            while (ItemCount>0) {
               Item$2 = TW3FieldDef.Create$79$($New(TW3FieldDef),Self);
               TW3FieldDef.ReadFrom$(Item$2,Reader$3);
               Self.FFields.push(Item$2);
               --ItemCount;
            }
         } else {
            throw Exception.Create($New(EW3FieldDefs),$R[41]);
         }
      } finally {
         TObject.Free(Reader$3);
      }
   }
   /// function TW3FieldDefs.SaveToJSON() : String
   ,SaveToJSON:function(Self) {
      var Result = "";
      var Header$1 = {ddMagic:0,ddDefs:[]};
      var x$45 = 0;
      Header$1.ddMagic = 3401235116;
      if (Self.FFields.length>0) {
         $ArraySetLenC(Header$1.ddDefs,Self.FFields.length,function (){return {fdName:"",fdDatatype:0}});
         var $temp68;
         for(x$45=0,$temp68=Self.FFields.length;x$45<$temp68;x$45++) {
            Header$1.ddDefs[x$45].fdDatatype = Self.FFields[x$45].FDatatype;
            Header$1.ddDefs[x$45].fdName = Self.FFields[x$45].FName$2;
         }
      }
      Result = JSON.stringify(Header$1);
      return Result
   }
   /// procedure TW3FieldDefs.SaveToStream(const Stream: TStream)
   ,SaveToStream$6:function(Self, Stream$14) {
      var Writer$3 = null,
         x$46 = 0;
      Writer$3 = TW3CustomWriter.Create$29$($New(TWriter),$AsIntf(Stream$14,"IBinaryTransport"));
      try {
         TW3CustomWriter.WriteInteger(Writer$3,3401235116);
         TW3CustomWriter.WriteInteger(Writer$3,Self.FFields.length);
         if (Self.FFields.length>0) {
            var $temp69;
            for(x$46=0,$temp69=Self.FFields.length;x$46<$temp69;x$46++) {
               TW3FieldDef.WriteTo$(Self.FFields[x$46],Writer$3);
            }
         }
      } finally {
         TObject.Free(Writer$3);
      }
   }
   /// procedure TW3FieldDefs.SetReadOnly(const NewState: Boolean)
   ,SetReadOnly:function(Self, NewState$3) {
      var a$474 = 0;
      var Item$3 = null;
      if (NewState$3!=TW3OwnedLockedObject.GetLockState(Self)) {
         TW3DatasetObject.SetReadOnly(Self,NewState$3);
         if (Self.FFields.length>0) {
            var a$475 = [];
            a$475 = Self.FFields;
            var $temp70;
            for(a$474=0,$temp70=a$475.length;a$474<$temp70;a$474++) {
               Item$3 = a$475[a$474];
               TW3DatasetObject.SetReadOnly$(Item$3,NewState$3);
            }
         }
      }
   }
   /// function TW3FieldDefs.ToString() : String
   ,ToString$8:function(Self) {
      var Result = "";
      var a$476 = 0;
      var field = null,
         Template = "",
         TypeName$2 = "";
      if (Self.FFields.length>0) {
         var a$477 = [];
         a$477 = Self.FFields;
         var $temp71;
         for(a$476=0,$temp71=a$477.length;a$476<$temp71;a$476++) {
            field = a$477[a$476];
            Template = "name=\"%s\" datatype=\"%s\"";
            TypeName$2 = "unknown";
            switch (field.FDatatype) {
               case 1 :
                  TypeName$2 = "boolean";
                  break;
               case 2 :
                  TypeName$2 = "integer";
                  break;
               case 3 :
                  TypeName$2 = "float";
                  break;
               case 4 :
                  TypeName$2 = "string";
                  break;
               case 5 :
                  TypeName$2 = "datetime";
                  break;
               case 6 :
                  TypeName$2 = "autoinc";
                  break;
               case 7 :
                  TypeName$2 = "guid";
                  break;
            }
            Result+=Format(Template,[field.FName$2, TypeName$2]);
            if (field!==Self.FFields[(Self.FFields.length-1)]) {
               Result+="\r";
            }
         }
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3DatasetObject.ObjectLocked
   ,ObjectUnLocked:TW3DatasetObject.ObjectUnLocked
   ,SetReadOnly$:function($){return $.ClassType.SetReadOnly.apply($.ClassType, arguments)}
   ,Create$78$:function($){return $.ClassType.Create$78.apply($.ClassType, arguments)}
   ,GetFieldIdentifier$:function($){return $.ClassType.GetFieldIdentifier($)}
   ,GetOwner$2$:function($){return $.ClassType.GetOwner$2($)}
   ,ToString$8$:function($){return $.ClassType.ToString$8($)}
};
TW3FieldDefs.$Intf={
   IW3OwnedObjectAccess:[TW3FieldDefs.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// TW3FieldDef = class (TW3DatasetObject)
var TW3FieldDef = {
   $ClassName:"TW3FieldDef",$Parent:TW3DatasetObject
   ,$Init:function ($) {
      TW3DatasetObject.$Init($);
      $.FDatatype = 0;
      $.FName$2 = "";
   }
   /// function TW3FieldDef.AcceptOwner(const CandidateObject: TObject) : Boolean
   ,AcceptOwner:function(Self, CandidateObject$4) {
      return CandidateObject$4!==null&&$Is(CandidateObject$4,TW3FieldDefs);
   }
   /// constructor TW3FieldDef.Create(const Owner: TW3FieldDefs)
   ,Create$79:function(Self, Owner$13) {
      TW3OwnedObject.Create$16(Self,Owner$13);
      if (Owner$13!==null) {
         Self.FName$2 = TW3FieldDefs.GetFieldIdentifier$(Owner$13);
      } else {
         throw EW3Exception.CreateFmt$($New(EW3FieldDef),"Failed to create field-definition [%s], parent was nil error",[TObject.ClassName(Self.ClassType)]);
      }
      return Self
   }
   /// function TW3FieldDef.GetOwner() : TW3DatasetFields
   ,GetOwner$3:function(Self) {
      return $As(TW3OwnedObject.GetOwner(Self),TW3DatasetFields);
   }
   /// procedure TW3FieldDef.ReadFrom(const Reader: TReader)
   ,ReadFrom:function(Self, Reader$4) {
      if (TW3CustomReader.ReadInteger(Reader$4)==3401239294) {
         Self.FDatatype = TW3CustomReader.ReadInteger(Reader$4);
         Self.FName$2 = TW3CustomReader.ReadString$1(Reader$4);
      }
   }
   /// procedure TW3FieldDef.SetName(DefName: String)
   ,SetName$3:function(Self, DefName) {
      if (!TW3OwnedLockedObject.GetLockState(Self)) {
         DefName = (Trim$_String_(DefName)).toLocaleLowerCase();
         if (DefName.length>0) {
            Self.FName$2 = DefName;
         } else {
            throw Exception.Create($New(EW3FieldDef),"Failed to set field-def name, string was empty error");
         }
      }
   }
   /// procedure TW3FieldDef.SetType(const DefDataType: TW3DatasetFieldType)
   ,SetType:function(Self, DefDataType) {
      if (!TW3OwnedLockedObject.GetLockState(Self)) {
         Self.FDatatype = DefDataType;
      }
   }
   /// procedure TW3FieldDef.WriteTo(const Writer: TWriter)
   ,WriteTo:function(Self, Writer$4) {
      TW3CustomWriter.WriteInteger(Writer$4,3401239294);
      TW3CustomWriter.WriteInteger(Writer$4,Self.FDatatype);
      TW3CustomWriter.WriteString(Writer$4,Self.FName$2);
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3DatasetObject.ObjectLocked
   ,ObjectUnLocked:TW3DatasetObject.ObjectUnLocked
   ,SetReadOnly:TW3DatasetObject.SetReadOnly
   ,Create$79$:function($){return $.ClassType.Create$79.apply($.ClassType, arguments)}
   ,GetOwner$3$:function($){return $.ClassType.GetOwner$3($)}
   ,ReadFrom$:function($){return $.ClassType.ReadFrom.apply($.ClassType, arguments)}
   ,WriteTo$:function($){return $.ClassType.WriteTo.apply($.ClassType, arguments)}
};
TW3FieldDef.$Intf={
   IW3OwnedObjectAccess:[TW3FieldDef.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// TW3DateTimeField = class (TW3DatasetField)
var TW3DateTimeField = {
   $ClassName:"TW3DateTimeField",$Parent:TW3DatasetField
   ,$Init:function ($) {
      TW3DatasetField.$Init($);
   }
   /// function TW3DateTimeField.GetValue() : TDateTime
   ,GetValue$1:function(Self) {
      return TW3DatasetField.GetValue(Self);
   }
   /// procedure TW3DateTimeField.SetValue(const NewValue: TDateTime)
   ,SetValue$1:function(Self, NewValue$3) {
      TW3DatasetField.SetValue(Self,NewValue$3);
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3DatasetField.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3DatasetObject.ObjectLocked
   ,ObjectUnLocked:TW3DatasetObject.ObjectUnLocked
   ,SetReadOnly:TW3DatasetObject.SetReadOnly
   ,Create$75:TW3DatasetField.Create$75
   ,Generate$2:TW3DatasetField.Generate$2
   ,GetGenerated:TW3DatasetField.GetGenerated
   ,GetOwner$1:TW3DatasetField.GetOwner$1
   ,GetValue:TW3DatasetField.GetValue
   ,LoadFromStream$5:TW3DatasetField.LoadFromStream$5
   ,ReadBinaryValue:TW3DatasetField.ReadBinaryValue
   ,SaveToStream$5:TW3DatasetField.SaveToStream$5
   ,SetKind:TW3DatasetField.SetKind
   ,SetName$2:TW3DatasetField.SetName$2
   ,SetValue:TW3DatasetField.SetValue
   ,WriteBinaryValue:TW3DatasetField.WriteBinaryValue
};
TW3DateTimeField.$Intf={
   IW3OwnedObjectAccess:[TW3DatasetField.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// TW3DatasetState enumeration
var TW3DatasetState = [ "dsIdle", "dsInsert", "dsEdit" ];
/// TW3DatasetFilter = class (TW3ErrorObject)
var TW3DatasetFilter = {
   $ClassName:"TW3DatasetFilter",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.FCache = [];
      $.FCurrent$1 = $.FDataset = null;
      $.FFilterText = "";
      $.FOperatorStack = [];
      $.FReady = false;
   }
   /// procedure TW3DatasetFilter.Clear()
   ,Clear$10:function(Self) {
      var x$47 = 0;
      Self.FReady = false;
      Self.FFilterText = "";
      Self.FOperatorStack.length=0;
      try {
         var $temp72;
         for(x$47=0,$temp72=Self.FCache.length;x$47<$temp72;x$47++) {
            if (Self.FCache[x$47]!==null) {
               TObject.Free(Self.FCache[x$47]);
            }
         }
      } finally {
         Self.FCache.length=0;
      }
   }
   /// constructor TW3DatasetFilter.Create(const Dataset: TW3Dataset)
   ,Create$80:function(Self, Dataset$1) {
      TW3ErrorObject.Create$3(Self);
      Self.FDataset = Dataset$1;
      return Self
   }
   /// destructor TW3DatasetFilter.Destroy()
   ,Destroy:function(Self) {
      TW3DatasetFilter.Clear$10(Self);
      TW3ErrorObject.Destroy(Self);
   }
   /// function TW3DatasetFilter.Execute() : Boolean
   ,Execute$2:function(Self) {
      var Result = false;
      var a$478 = 0;
      var expr = null,
         a$479 = 0;
      var item$4 = null,
         a$480 = 0;
      var item$5 = null;
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (Self.FCache.length>0) {
         var a$481 = [],
             a$482 = [],
             a$483 = [];
         a$483 = Self.FCache;
         var $temp73;
         for(a$478=0,$temp73=a$483.length;a$478<$temp73;a$478++) {
            expr = a$483[a$478];
            expr.Success = false;
         }
         a$482 = Self.FCache;
         var $temp74;
         for(a$479=0,$temp74=a$482.length;a$479<$temp74;a$479++) {
            item$4 = a$482[a$479];
            switch (item$4.Symbol) {
               case 1 :
                  item$4.Success = TW3FilterValue.Equals$4(item$4.FLeft,item$4.FRight);
                  break;
               case 2 :
                  item$4.Success = TW3FilterValue.More$1(item$4.FLeft,item$4.FRight);
                  break;
               case 3 :
                  item$4.Success = TW3FilterValue.Less$1(item$4.FLeft,item$4.FRight);
                  break;
               case 4 :
                  item$4.Success = TW3FilterValue.NotEqual(item$4.FLeft,item$4.FRight);
                  break;
               case 5 :
                  item$4.Success = TW3FilterValue.Like(item$4.FLeft,item$4.FRight);
                  break;
            }
         }
         Result = true;
         a$481 = Self.FCache;
         var $temp75;
         for(a$480=0,$temp75=a$481.length;a$480<$temp75;a$480++) {
            item$5 = a$481[a$480];
            {var $temp76 = item$5.Operator;
               if ($temp76==1) {
                  if (!item$5.Success) {
                     Result = false;
                     break;
                  }
               }
                else if ($temp76==2) {
                  /* null */
               }
                else if ($temp76==3) {
                  if (item$5.Success) {
                     Result = false;
                     break;
                  }
               }
            }
         }
      }
      return Result
   }
   /// function TW3DatasetFilter.Parse(FilterText: String) : Boolean
   ,Parse$2:function(Self, FilterText$1) {
      var Result = {v:false};
      try {
         var Buffer$14 = null;
         var Operator$1 = {};
         Operator$1.v = 0;
         var last = false,
            first = false;
         if (Self.FOptions.AutoResetError) {
            TW3ErrorObject.ClearLastError(Self);
         }
         TW3DatasetFilter.Clear$10(Self);
         last = false;
         first = true;
         Buffer$14 = TTextBuffer.Create$71$($New(TTextBuffer),"");
         try {
            TTextBuffer.LoadBufferText(Buffer$14,FilterText$1);
            try {
               do {
                  TTextBuffer.ConsumeJunk(Buffer$14);
                  Self.FCurrent$1 = TW3FilterExpression.Create$77$($New(TW3FilterExpression),Self);
                  if (TW3DatasetFilter.ParseLeft(Self,Buffer$14,Self.FCurrent$1)) {
                     if (TW3DatasetFilter.ParseSymbol(Self,Buffer$14,Self.FCurrent$1)) {
                        if (TW3DatasetFilter.ParseRight(Self,Buffer$14,Self.FCurrent$1)) {
                           if (Self.FOperatorStack.length>0) {
                              Self.FCurrent$1.Operator = Self.FOperatorStack.pop();
                           }
                           Self.FCache.push(Self.FCurrent$1);
                           if (TW3DatasetFilter.ParseOperator(Self,Buffer$14,Operator$1)) {
                              Self.FOperatorStack.push(Operator$1.v);
                           } else {
                              TTextBuffer.ConsumeJunk(Buffer$14);
                              if (TTextBuffer.EOF$3(Buffer$14)) {
                                 TW3ErrorObject.ClearLastError(Self);
                                 last = true;
                              }
                           }
                           if (last) {
                              break;
                           }
                        } else {
                           TObject.Free(Self.FCurrent$1);
                           Self.FCurrent$1 = null;
                           break;
                        }
                     } else {
                        TObject.Free(Self.FCurrent$1);
                        Self.FCurrent$1 = null;
                        break;
                     }
                  } else {
                     TObject.Free(Self.FCurrent$1);
                     Self.FCurrent$1 = null;
                     break;
                  }
               } while (!TTextBuffer.EOF$3(Buffer$14));
               if (!TW3ErrorObject.GetFailed(Self)) {
                  if (Self.FOperatorStack.length>0) {
                     TW3ErrorObject.SetLastErrorF(Self,"Syntax error, operator '%s' found but without context error",[TFilterOperatorHelper$ToString$12(Self.FOperatorStack.pop())]);
                  } else {
                     Result.v = true;
                  }
               }
            } catch ($e) {
               var e$32 = $W($e);
               TW3ErrorObject.SetLastError$(Self,e$32.FMessage);
               TW3DatasetFilter.Clear$10(Self);
               return Result.v;
            }
            if (Result.v) {
               Self.FReady = true;
               Self.FFilterText = FilterText$1;
            }
         } finally {
            TObject.Free(Buffer$14);
         }
      } finally {return Result.v}
   }
   /// function TW3DatasetFilter.ParseLeft(const Buffer: TTextBuffer; const Expression: TW3FilterExpression) : Boolean
   ,ParseLeft:function(Self, Buffer$15, Expression) {
      var Result = {v:false};
      try {
         var Text$34 = {};
         Text$34.v = "";
         var LOperator = {};
         LOperator.v = 0;
         var Bookmark$2 = null,
            FieldName$3 = "",
            idx$3 = 0;
         if (Self.FOptions.AutoResetError) {
            TW3ErrorObject.ClearLastError(Self);
         }
         TTextBuffer.ConsumeJunk(Buffer$15);
         if (TTextBuffer.EOF$3(Buffer$15)) {
            TW3ErrorObject.SetLastError$(Self,"Unexpected end of text, expected fieldname error");
         } else {
            Bookmark$2 = TTextBuffer.Bookmark$1(Buffer$15);
            try {
               if (TW3DatasetFilter.ParseOperator(Self,Buffer$15,LOperator)) {
                  switch ($Peek(Self.FOperatorStack," in TW3DatasetFilter.ParseLeft [line: 1237, column: 29, file: System.dataset]")) {
                     case 1 :
                        if (LOperator.v==3) {
                           Self.FOperatorStack.pop();
                           Self.FOperatorStack.push(3);
                           TObject.Free(Bookmark$2);
                           Bookmark$2 = null;
                        }
                        break;
                     case 2 :
                        TW3ErrorObject.SetLastError$(Self,"Syntax error, or based composite-operators not supported error");
                        return Result.v;
                        break;
                  }
               } else {
                  TW3ErrorObject.ClearLastError(Self);
               }
            } finally {
               if (Bookmark$2!==null) {
                  TTextBuffer.Restore(Buffer$15,Bookmark$2);
               }
            }
            TTextBuffer.ConsumeJunk(Buffer$15);
            if (TTextBuffer.ReadWord$2(Buffer$15,Text$34)) {
               Text$34.v = Trim$_String_(Text$34.v);
               if (Text$34.v.length>0) {
                  FieldName$3 = Text$34.v;
                  Expression.FLeft.RawValue = FieldName$3;
                  if (_Reserved.indexOf((FieldName$3).toLocaleLowerCase())>=0) {
                     TW3ErrorObject.SetLastErrorF(Self,"syntax error, expected fieldname but found reserved word [%s]",[FieldName$3]);
                     return Result.v;
                  }
                  idx$3 = TW3FieldDefs.IndexOf$1(Self.FDataset.FDefs,FieldName$3);
                  if (idx$3>=0) {
                     Expression.FLeft.IsField = true;
                     Expression.FLeft.DataField = TW3DatasetFields.FieldByName$1(Self.FDataset.FFields$2,FieldName$3);
                  } else {
                     TW3ErrorObject.SetLastErrorF(Self,"Invalid filter, expected fieldname not %s",[FieldName$3]);
                     return Result.v;
                  }
                  Result.v = true;
               }
            } else {
               TW3ErrorObject.SetLastErrorF(Self,"Unexpected end of text, expected fieldname error (%s)",[Buffer$15.FLastError]);
            }
         }
      } finally {return Result.v}
   }
   /// function TW3DatasetFilter.ParseOperator(const Buffer: TTextBuffer; var Operator: TW3FilterOperator) : Boolean
   ,ParseOperator:function(Self, Buffer$16, Operator$2) {
      var Result = false;
      var Bookmark$3 = null,
         temp$21 = {v:""};
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      Operator$2.v = 0;
      TTextBuffer.ConsumeJunk(Buffer$16);
      if (TTextBuffer.EOF$3(Buffer$16)) {
         TW3ErrorObject.SetLastError$(Self,"Invalid or maelformed filter, unexpected end of text (expected statement) error");
      } else {
         Bookmark$3 = TTextBuffer.Bookmark$1(Buffer$16);
         temp$21.v = "";
         if (TTextBuffer.ReadWord$2(Buffer$16,temp$21)) {
            if (_StatementLUT.hasOwnProperty(temp$21.v)) {
               Operator$2.v = parseInt(_StatementLUT[temp$21.v],10);
            } else {
               TTextBuffer.Restore(Buffer$16,Bookmark$3);
               return Result;
            }
            Result = (Operator$2.v!=0);
         }
      }
      return Result
   }
   /// function TW3DatasetFilter.ParseRight(const Buffer: TTextBuffer; const Expression: TW3FilterExpression) : Boolean
   ,ParseRight:function(Self, Buffer$17, Expression$1) {
      var Result = false;
      var Temp$3 = {};
      Temp$3.v = "";
      var Value$162 = "",
         idx$4 = 0;
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      TTextBuffer.ConsumeJunk(Buffer$17);
      if (TTextBuffer.EOF$3(Buffer$17)) {
         TW3ErrorObject.SetLastError$(Self,"Syntax error, unexpected end of text");
      } else {
         Value$162 = "";
         if (TTextBuffer.Current(Buffer$17)=="\"") {
            Value$162 = "\"";
            TTextBuffer.Next(Buffer$17);
            if (TTextBuffer.ReadTo$1(Buffer$17,(["\""].slice()),Temp$3)) {
               Value$162+=Temp$3.v;
               Value$162+="\"";
               TTextBuffer.Next(Buffer$17);
               Expression$1.FRight.RawValue = Value$162;
               Result = true;
            } else {
               TW3ErrorObject.SetLastError$(Self,"Invalid filter, unclosed string error");
            }
         } else {
            if (TTextBuffer.ReadWord$2(Buffer$17,Temp$3)) {
               Value$162 = Trim$_String_(Temp$3.v);
               Self.FCurrent$1.FRight.RawValue = Value$162;
               if (_Reserved.indexOf((Value$162).toLocaleLowerCase())>=0) {
                  TW3ErrorObject.SetLastErrorF(Self,"Syntax error, expected string or value not reserved word [%s]",[Value$162]);
                  return Result;
               }
               idx$4 = TW3FieldDefs.IndexOf$1(Self.FDataset.FDefs,Value$162);
               if (idx$4>=0) {
                  Expression$1.FRight.IsField = true;
                  Expression$1.FRight.DataField = TW3DatasetFields.FieldByName$1(Self.FDataset.FFields$2,Value$162);
                  Result = true;
                  return Result;
               }
               if ((Value$162).toLocaleLowerCase()!="null") {
                  if (!TStringHelper$ContainsHex(Value$162)) {
                     if (!TStringHelper$ContainsOrdinal(Value$162)) {
                        if (!TStringHelper$ContainsFloat(Value$162)) {
                           if (!TStringHelper$ContainsQuote(Value$162)) {
                              TW3ErrorObject.SetLastErrorF(Self,"Invalid filter, expected string, hex, float or numeric not [%s]",[Value$162]);
                              return Result;
                           }
                        }
                     }
                  }
               }
               Result = Value$162.length>0;
            } else {
               TW3ErrorObject.SetLastError$(Self,"Syntax error, expected string, number or fieldname");
            }
         }
      }
      return Result
   }
   /// function TW3DatasetFilter.ParseSymbol(const Buffer: TTextBuffer; const Expression: TW3FilterExpression) : Boolean
   ,ParseSymbol:function(Self, Buffer$18, Expression$2) {
      var Result = false;
      var text = {};
      text.v = "";
      var Operand = 0,
         Bookmark$4 = null;
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      Operand = 0;
      TTextBuffer.ConsumeJunk(Buffer$18);
      if (TTextBuffer.EOF$3(Buffer$18)) {
         TW3ErrorObject.SetLastError$(Self,"Invalid or maelformed filter, unexpected end of text (expected operand) error");
      } else {
         if (function(v$){return ((v$=="<")||(v$=="=")||(v$==">"))}(TTextBuffer.Current(Buffer$18))) {
            do {
               if (function(v$){return ((v$=="<")||(v$=="=")||(v$==">"))}(TTextBuffer.Current(Buffer$18))) {
                  text.v+=TTextBuffer.Current(Buffer$18);
               } else {
                  break;
               }
               if (!TTextBuffer.Next(Buffer$18)) {
                  break;
               }
            } while (!TTextBuffer.EOF$3(Buffer$18));
         } else {
            Bookmark$4 = TTextBuffer.Bookmark$1(Buffer$18);
            if (TTextBuffer.ReadWord$2(Buffer$18,text)) {
               if (_Reserved.indexOf((text.v).toLocaleLowerCase())>=0) {
                  TTextBuffer.Restore(Buffer$18,Bookmark$4);
                  TW3ErrorObject.SetLastErrorF(Self,"Invalid filter, unexpected use of reserved operator [%s]",[text.v]);
               } else {
                  /* null */
               }
            } else {
               TW3ErrorObject.SetLastError$(Self,"Invalid or maelformed filter, expected operand error");
               return Result;
            }
         }
         text.v = (text.v).toLocaleLowerCase();
         if (_OperandLUT.hasOwnProperty(text.v)) {
            Operand = parseInt(_OperandLUT[text.v],10);
         }
         Expression$2.Symbol = Operand;
         Result = (Operand!=0);
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3:TW3ErrorObject.Create$3
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,Create$80$:function($){return $.ClassType.Create$80.apply($.ClassType, arguments)}
};
TW3DatasetFilter.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TW3DatasetFieldType enumeration
var TW3DatasetFieldType = [ "ftUnknown", "ftboolean", "ftinteger", "ftFloat", "ftString", "ftDateTime", "ftAutoInc", "ftGUID" ];
/// TW3DatasetFields = class (TW3DatasetObject)
var TW3DatasetFields = {
   $ClassName:"TW3DatasetFields",$Parent:TW3DatasetObject
   ,$Init:function ($) {
      TW3DatasetObject.$Init($);
      $.FFields$1 = [];
   }
   /// anonymous TSourceMethodSymbol
   ,a$146:function(Self, index$9) {
      return Self.FFields$1[index$9];
   }
   /// anonymous TSourceMethodSymbol
   ,a$145:function(Self) {
      return Self.FFields$1.length;
   }
   /// function TW3DatasetFields.AcceptOwner(const CandidateObject: TObject) : Boolean
   ,AcceptOwner:function(Self, CandidateObject$5) {
      return CandidateObject$5!==null&&$Is(CandidateObject$5,TW3Dataset);
   }
   /// function TW3DatasetFields.Add(FieldName: String; const FieldType: TW3DatasetFieldType) : TW3DatasetField
   ,Add$2:function(Self, FieldName$4, FieldType$1) {
      var Result = null;
      FieldName$4 = (Trim$_String_(FieldName$4)).toLocaleLowerCase();
      if (FieldName$4.length>0) {
         if (TW3DatasetFields.IndexOf$2(Self,FieldName$4)<0) {
            switch (FieldType$1) {
               case 1 :
                  Result = TW3DatasetField.Create$75$($New(TW3BooleanField),Self);
                  break;
               case 2 :
                  Result = TW3DatasetField.Create$75$($New(TW3IntegerField),Self);
                  break;
               case 3 :
                  Result = TW3DatasetField.Create$75$($New(TW3FloatField),Self);
                  break;
               case 4 :
                  Result = TW3DatasetField.Create$75$($New(TW3StringField),Self);
                  break;
               case 5 :
                  Result = TW3DatasetField.Create$75$($New(TW3DateTimeField),Self);
                  break;
               case 6 :
                  Result = TW3DatasetField.Create$75$($New(TW3AutoIncField),Self);
                  break;
               case 7 :
                  Result = TW3DatasetField.Create$75$($New(TW3GUIDField),Self);
                  break;
               default :
                  throw Exception.Create($New(EW3DatasetFields),$R[35]);
            }
            TW3DatasetField.SetName$2$(Result,FieldName$4);
            TW3DatasetField.SetKind$(Result,FieldType$1);
            Self.FFields$1.push(Result);
         } else {
            throw EW3Exception.CreateFmt$($New(EW3DatasetFields),"Failed to add field [%s], a field with that name already exists error",[FieldName$4]);
         }
      } else {
         throw Exception.Create($New(EW3DatasetFields),$R[39]);
      }
      return Result
   }
   /// procedure TW3DatasetFields.Clear()
   ,Clear$11:function(Self) {
      var a$484 = 0;
      var field$1 = null;
      if (Self.FFields$1.length>0) {
         try {
            var a$485 = [];
            a$485 = Self.FFields$1;
            var $temp77;
            for(a$484=0,$temp77=a$485.length;a$484<$temp77;a$484++) {
               field$1 = a$485[a$484];
               TObject.Free(field$1);
            }
         } finally {
            Self.FFields$1.length=0;
         }
      }
   }
   /// constructor TW3DatasetFields.Create(const Owner: TW3Dataset)
   ,Create$81:function(Self, Owner$14) {
      TW3OwnedObject.Create$16(Self,Owner$14);
      return Self
   }
   /// function TW3DatasetFields.DataExport() : Variant
   ,DataExport:function(Self) {
      var Result = undefined;
      var a$486 = 0;
      var Field$5 = null;
      Result = TVariant.CreateObject();
      if (Self.FFields$1.length>0) {
         var a$487 = [];
         a$487 = Self.FFields$1;
         var $temp78;
         for(a$486=0,$temp78=a$487.length;a$486<$temp78;a$486++) {
            Field$5 = a$487[a$486];
            Result[Field$5.FName$1] = TW3DatasetField.GetValue$(Field$5);
         }
      }
      return Result
   }
   /// procedure TW3DatasetFields.DataImport(const FieldData: TW3DatasetPacket)
   ,DataImport:function(Self, FieldData) {
      var a$488 = 0;
      var Field$6 = null;
      if (Self.FFields$1.length>0) {
         var a$489 = [];
         a$489 = Self.FFields$1;
         var $temp79;
         for(a$488=0,$temp79=a$489.length;a$488<$temp79;a$488++) {
            Field$6 = a$489[a$488];
            TW3DatasetField.SetValue$(Field$6,FieldData[Field$6.FName$1]);
         }
      }
   }
   /// procedure TW3DatasetFields.Delete(const FieldIndex: Integer)
   ,Delete$5:function(Self, FieldIndex$2) {
      if (FieldIndex$2>=0&&FieldIndex$2<Self.FFields$1.length) {
         TObject.Free(Self.FFields$1[FieldIndex$2]);
         Self.FFields$1.splice(FieldIndex$2,1)
         ;
      } else {
         throw EW3Exception.CreateFmt$($New(EW3DatasetFields),"Failed to delete field, invalid fieldindex [%d] error",[FieldIndex$2]);
      }
   }
   /// procedure TW3DatasetFields.DeleteByName(FieldName: String)
   ,DeleteByName:function(Self, FieldName$5) {
      var LIndex$1 = 0;
      LIndex$1 = TW3DatasetFields.IndexOf$2(Self,FieldName$5);
      if (LIndex$1>=0) {
         TObject.Free(Self.FFields$1[LIndex$1]);
         Self.FFields$1.splice(LIndex$1,1)
         ;
      } else {
         throw EW3Exception.CreateFmt$($New(EW3DatasetFields),"Failed to delete field [%s], field not found error",[FieldName$5]);
      }
   }
   /// destructor TW3DatasetFields.Destroy()
   ,Destroy:function(Self) {
      if (TW3DatasetFields.a$145(Self)>0) {
         TW3DatasetFields.Clear$11(Self);
      }
      TObject.Destroy(Self);
   }
   /// function TW3DatasetFields.FieldByName(FieldName: String) : TW3DatasetField
   ,FieldByName$1:function(Self, FieldName$6) {
      var Result = null;
      var x$48 = 0;
      var Field$7 = null;
      FieldName$6 = (Trim$_String_(FieldName$6)).toLocaleLowerCase();
      if (FieldName$6.length>0) {
         var $temp80;
         for(x$48=0,$temp80=Self.FFields$1.length;x$48<$temp80;x$48++) {
            Field$7 = Self.FFields$1[x$48];
            if (Field$7.FName$1==FieldName$6) {
               Result = Field$7;
               break;
            }
         }
      }
      return Result
   }
   /// function TW3DatasetFields.GetOwner() : TW3Dataset
   ,GetOwner$4:function(Self) {
      return $As(TW3OwnedObject.GetOwner(Self),TW3Dataset);
   }
   /// function TW3DatasetFields.IndexOf(FieldName: String) : Integer
   ,IndexOf$2:function(Self, FieldName$7) {
      var Result = 0;
      var x$49 = 0;
      var Field$8 = null;
      Result = -1;
      FieldName$7 = (Trim$_String_(FieldName$7)).toLocaleLowerCase();
      if (FieldName$7.length>0) {
         var $temp81;
         for(x$49=0,$temp81=Self.FFields$1.length;x$49<$temp81;x$49++) {
            Field$8 = Self.FFields$1[x$49];
            if (Field$8.FName$1==FieldName$7) {
               Result = x$49;
               break;
            }
         }
      }
      return Result
   }
   /// procedure TW3DatasetFields.LoadFromStream(const Stream: TStream)
   ,LoadFromStream$7:function(Self, Stream$15) {
      var Reader$5 = null;
      var FieldCount = 0,
         Field$9 = null;
      function PeekFieldType() {
         var Result = null;
         var StreamPos = 0;
         StreamPos = TStream.GetPosition$(Stream$15);
         try {
            if (TW3CustomReader.ReadInteger(Reader$5)==3133065982) {
               switch (TW3CustomReader.ReadInteger(Reader$5)) {
                  case 5 :
                     Result = TW3DateTimeField;
                     break;
                  case 6 :
                     Result = TW3AutoIncField;
                     break;
                  case 1 :
                     Result = TW3BooleanField;
                     break;
                  case 2 :
                     Result = TW3IntegerField;
                     break;
                  case 4 :
                     Result = TW3StringField;
                     break;
                  case 3 :
                     Result = TW3FloatField;
                     break;
                  case 7 :
                     Result = TW3GUIDField;
                     break;
                  default :
                     throw Exception.Create($New(EW3DatasetFields),$R[51]);
               }
            } else {
               throw Exception.Create($New(EW3DatasetFields),$R[52]);
            }
         } finally {
            TStream.SetPosition$(Stream$15,StreamPos);
         }
         return Result
      };
      TW3DatasetFields.Clear$11(Self);
      Reader$5 = TW3CustomReader.Create$39$($New(TReader),$AsIntf(Stream$15,"IBinaryTransport"));
      try {
         if (TW3CustomReader.ReadInteger(Reader$5)==3405691582) {
            FieldCount = TW3CustomReader.ReadInteger(Reader$5);
            while (FieldCount>0) {
               Field$9 = TW3DatasetField.Create$75$($NewDyn(PeekFieldType()," in TW3DatasetFields.LoadFromStream [line: 2800, column: 36, file: System.dataset]"),Self);
               TW3DatasetField.LoadFromStream$5$(Field$9,Stream$15);
               Self.FFields$1.push(Field$9);
               --FieldCount;
            }
         } else {
            throw Exception.Create($New(EW3DatasetFields),$R[53]);
         }
      } finally {
         TObject.Free(Reader$5);
      }
   }
   /// procedure TW3DatasetFields.ResetFieldValues()
   ,ResetFieldValues:function(Self) {
      var a$490 = 0;
      var Field$10 = null;
      if (Self.FFields$1.length>0) {
         var a$491 = [];
         a$491 = Self.FFields$1;
         var $temp82;
         for(a$490=0,$temp82=a$491.length;a$490<$temp82;a$490++) {
            Field$10 = a$491[a$490];
            TW3DatasetField.SetValue$(Field$10,null);
         }
      }
   }
   /// procedure TW3DatasetFields.SaveToStream(const Stream: TStream)
   ,SaveToStream$7:function(Self, Stream$16) {
      var Writer$5 = null,
         x$50 = 0;
      if (Stream$16!==null) {
         Writer$5 = TW3CustomWriter.Create$29$($New(TWriter),$AsIntf(Stream$16,"IBinaryTransport"));
         try {
            TW3CustomWriter.WriteInteger(Writer$5,3405691582);
            TW3CustomWriter.WriteInteger(Writer$5,TW3DatasetFields.a$145(Self));
         } finally {
            TObject.Free(Writer$5);
         }
         var $temp83;
         for(x$50=0,$temp83=TW3DatasetFields.a$145(Self);x$50<$temp83;x$50++) {
            TW3DatasetField.SaveToStream$5$(TW3DatasetFields.a$146(Self,x$50),Stream$16);
         }
      }
   }
   /// procedure TW3DatasetFields.SetReadOnly(const NewState: Boolean)
   ,SetReadOnly:function(Self, NewState$4) {
      var a$492 = 0;
      var Field$11 = null;
      if (NewState$4!=TW3OwnedLockedObject.GetLockState(Self)) {
         TW3DatasetObject.SetReadOnly(Self,NewState$4);
         if (Self.FFields$1.length>0) {
            var a$493 = [];
            a$493 = Self.FFields$1;
            var $temp84;
            for(a$492=0,$temp84=a$493.length;a$492<$temp84;a$492++) {
               Field$11 = a$493[a$492];
               TW3DatasetObject.SetReadOnly$(Field$11,NewState$4);
            }
         }
      }
   }
   /// function TW3DatasetFields.ToString() : String
   ,ToString$9:function(Self) {
      var Result = "";
      var Text$35 = "",
         x$51 = 0;
      Text$35 = "";
      var $temp85;
      for(x$51=0,$temp85=TW3DatasetFields.a$145(Self);x$51<$temp85;x$51++) {
         Text$35+=TW3DatasetFields.a$146(Self,x$51).FName$1+"="+TW3DatasetField.a$122(TW3DatasetFields.a$146(Self,x$51));
         if (x$51<TW3DatasetFields.a$145(Self)-1) {
            Text$35+=", ";
         }
      }
      Result = Text$35;
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3DatasetObject.ObjectLocked
   ,ObjectUnLocked:TW3DatasetObject.ObjectUnLocked
   ,SetReadOnly$:function($){return $.ClassType.SetReadOnly.apply($.ClassType, arguments)}
   ,Create$81$:function($){return $.ClassType.Create$81.apply($.ClassType, arguments)}
   ,DataExport$:function($){return $.ClassType.DataExport($)}
   ,DataImport$:function($){return $.ClassType.DataImport.apply($.ClassType, arguments)}
   ,GetOwner$4$:function($){return $.ClassType.GetOwner$4($)}
};
TW3DatasetFields.$Intf={
   IW3OwnedObjectAccess:[TW3DatasetFields.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// TW3Dataset = class (TObject)
var TW3Dataset = {
   $ClassName:"TW3Dataset",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FActive$2 = $.FDestroying = $.FFiltered = false;
      $.FCache$1 = [];
      $.FDefs = $.FFields$2 = $.FFilter$1 = $.FOnAdd = $.FOnClosed = $.FOnCreated = $.FOnDelete = $.FOnPos = $.FOnState = null;
      $.FDsIndex = 0;
      $.FFilterText$1 = "";
      $.FState$1 = 0;
   }
   /// procedure TW3Dataset.Append()
   ,Append$2:function(Self) {
      if (Self.FActive$2) {
         if (!Self.FState$1) {
            TW3Dataset.SetPosition$3(Self,Self.FCache$1.length);
            TW3Dataset.SetState$1(Self,1);
            TW3DatasetFields.ResetFieldValues(Self.FFields$2);
            TW3Dataset.UpdateGeneratedFields(Self);
         } else {
            throw Exception.Create($New(EW3Dataset),$R[34]);
         }
      } else {
         throw Exception.Create($New(EW3Dataset),$R[33]);
      }
   }
   /// procedure TW3Dataset.Back()
   ,Back$1:function(Self) {
      if (Self.FActive$2) {
         if (!Self.FState$1) {
            TW3Dataset.SetPosition$3(Self,TInteger.EnsureRange((Self.FDsIndex-1),(-1),TW3Dataset.GetRecCount$(Self)));
         } else {
            throw Exception.Create($New(EW3Dataset),$R[34]);
         }
      } else {
         throw Exception.Create($New(EW3Dataset),$R[33]);
      }
   }
   /// procedure TW3Dataset.Cancel()
   ,Cancel:function(Self) {
      if (Self.FActive$2) {
         if ((1<<Self.FState$1&6)!=0) {
            TW3Dataset.SetState$1(Self,0);
            TW3DatasetFields.ResetFieldValues(Self.FFields$2);
         } else {
            throw Exception.Create($New(EW3Dataset),$R[34]);
         }
      } else {
         throw Exception.Create($New(EW3Dataset),$R[33]);
      }
   }
   /// procedure TW3Dataset.Close()
   ,Close$1:function(Self) {
      if (Self.FActive$2) {
         TW3Dataset.DoBeforeDatasetClosed$(Self);
         try {
            try {
               TW3DatasetFields.Clear$11(Self.FFields$2);
               Self.FCache$1.length=0;
               if (Self.FFilter$1!==null) {
                  Self.FFiltered = false;
                  TObject.Free(Self.FFilter$1);
                  Self.FFilter$1 = null;
               }
            } finally {
               Self.FActive$2 = false;
               Self.FState$1 = 0;
               Self.FDsIndex = -1;
               TW3DatasetFields.ResetFieldValues(Self.FFields$2);
               TW3DatasetObject.SetReadOnly$(Self.FFields$2,false);
               TW3DatasetObject.SetReadOnly$(Self.FDefs,false);
            }
         } finally {
            TW3Dataset.DoAfterDatasetClosed$(Self);
         }
      } else {
         throw Exception.Create($New(EW3Dataset),$R[33]);
      }
   }
   /// constructor TW3Dataset.Create()
   ,Create$82:function(Self) {
      TObject.Create(Self);
      Self.FFields$2 = TW3DatasetFields.Create$81$($New(TW3DatasetFields),Self);
      Self.FDefs = TW3FieldDefs.Create$78$($New(TW3FieldDefs),Self);
      Self.FState$1 = 0;
      Self.FDestroying = false;
      Self.FDsIndex = -1;
      return Self
   }
   /// procedure TW3Dataset.CreateDataset()
   ,CreateDataset:function(Self) {
      var x$52 = 0;
      if (!Self.FActive$2) {
         if (TW3FieldDefs.a$144(Self.FDefs)>0) {
            TW3DatasetFields.Clear$11(Self.FFields$2);
            TW3Dataset.DoBeforeDatasetCreated$(Self);
            try {
               Self.FActive$2 = true;
               TW3Dataset.SetState$1(Self,0);
               TW3Dataset.SetPosition$3(Self,(-1));
               var $temp86;
               for(x$52=0,$temp86=TW3FieldDefs.a$144(Self.FDefs);x$52<$temp86;x$52++) {
                  TW3DatasetFields.Add$2(Self.FFields$2,TW3FieldDefs.a$143(Self.FDefs,x$52).FName$2,TW3FieldDefs.a$143(Self.FDefs,x$52).FDatatype);
               }
               TW3DatasetObject.SetReadOnly$(Self.FFields$2,true);
               TW3DatasetObject.SetReadOnly$(Self.FDefs,true);
            } finally {
               TW3Dataset.DoAfterDatasetCreated$(Self);
            }
         } else {
            throw Exception.Create($New(EW3Dataset),$R[48]);
         }
      }
   }
   /// procedure TW3Dataset.Delete()
   ,Delete$6:function(Self) {
      if (Self.FActive$2) {
         if (!Self.FState$1) {
            if (TW3Dataset.GetRecCount$(Self)>0) {
               if (Self.FDsIndex>=0&&Self.FDsIndex<Self.FCache$1.length) {
                  if (Self.FOnDelete) {
                     Self.FOnDelete(Self,Self.FDsIndex);
                  }
                  Self.FCache$1.splice(Self.FDsIndex,1)
                  ;
                  if (Self.FDsIndex>=Self.FCache$1.length) {
                     Self.FDsIndex = Self.FCache$1.length-1;
                  }
               } else {
                  throw EW3Exception.CreateFmt$($New(EW3Dataset),$R[50],[Self.FDsIndex]);
               }
            } else {
               throw Exception.Create($New(EW3Dataset),$R[49]);
            }
         } else {
            throw Exception.Create($New(EW3Dataset),$R[34]);
         }
      } else {
         throw Exception.Create($New(EW3Dataset),$R[33]);
      }
   }
   /// destructor TW3Dataset.Destroy()
   ,Destroy:function(Self) {
      Self.FDestroying = true;
      if (Self.FActive$2) {
         TW3Dataset.Close$1(Self);
      }
      TObject.Free(Self.FFields$2);
      TObject.Free(Self.FDefs);
      TObject.Destroy(Self);
   }
   /// procedure TW3Dataset.DoAfterDatasetClosed()
   ,DoAfterDatasetClosed:function(Self) {
      if (Self.FOnClosed) {
         if (!Self.FDestroying) {
            Self.FOnClosed(Self);
         }
      }
   }
   /// procedure TW3Dataset.DoAfterDatasetCreated()
   ,DoAfterDatasetCreated:function(Self) {
      if (Self.FOnCreated) {
         if (!Self.FDestroying) {
            Self.FOnCreated(Self);
         }
      }
   }
   /// procedure TW3Dataset.DoBeforeDatasetClosed()
   ,DoBeforeDatasetClosed:function(Self) {
      /* null */
   }
   /// procedure TW3Dataset.DoBeforeDatasetCreated()
   ,DoBeforeDatasetCreated:function(Self) {
      /* null */
   }
   /// procedure TW3Dataset.Edit()
   ,Edit:function(Self) {
      if (Self.FActive$2) {
         if (!Self.FState$1) {
            if (Self.FCache$1.length>0) {
               if (Self.FDsIndex==-1) {
                  TW3Dataset.SetPosition$3(Self,0);
               } else if (Self.FDsIndex==Self.FCache$1.length) {
                  TW3Dataset.SetPosition$3(Self,(Self.FCache$1.length-1));
               }
               TW3Dataset.SetState$1(Self,2);
               TW3Dataset.UpdateGeneratedFields(Self);
            }
         } else {
            throw Exception.Create($New(EW3Dataset),$R[34]);
         }
      } else {
         throw Exception.Create($New(EW3Dataset),$R[33]);
      }
   }
   /// function TW3Dataset.FindFirst() : Boolean
   ,FindFirst:function(Self) {
      var Result = false;
      if (Self.FFiltered) {
         if (TW3Dataset.GetRecCount$(Self)>0) {
            TW3Dataset.First$1(Self);
            if (!TW3DatasetFilter.Execute$2(Self.FFilter$1)) {
               while (1) {
                  TW3Dataset.Next$1(Self);
                  if (TW3Dataset.GetEOF$1$(Self)) {
                     break;
                  } else if (TW3DatasetFilter.Execute$2(Self.FFilter$1)) {
                     break;
                  }
               }
               if (!TW3Dataset.GetEOF$1$(Self)) {
                  Result = TW3DatasetFilter.Execute$2(Self.FFilter$1);
               }
            }
         }
      } else {
         throw Exception.Create($New(EW3Dataset),"Dataset has no active filter error");
      }
      return Result
   }
   /// function TW3Dataset.FindLast() : Boolean
   ,FindLast:function(Self) {
      var Result = false;
      if (Self.FFiltered) {
         if (TW3Dataset.GetRecCount$(Self)>0) {
            TW3Dataset.Last$1(Self);
            Result = TW3DatasetFilter.Execute$2(Self.FFilter$1);
            if (!Result) {
               while (1) {
                  TW3Dataset.Back$1(Self);
                  if (TW3Dataset.GetBOF$1$(Self)) {
                     break;
                  } else if (TW3DatasetFilter.Execute$2(Self.FFilter$1)) {
                     break;
                  }
               }
               if (!TW3Dataset.GetBOF$1$(Self)) {
                  Result = TW3DatasetFilter.Execute$2(Self.FFilter$1);
               }
            }
         }
      } else {
         throw Exception.Create($New(EW3Dataset),"Dataset has no active filter error");
      }
      return Result
   }
   /// function TW3Dataset.FindNext() : Boolean
   ,FindNext:function(Self) {
      var Result = false;
      if (Self.FFiltered) {
         if (TW3Dataset.GetRecCount$(Self)>0) {
            if (!TW3Dataset.GetEOF$1$(Self)) {
               while (1) {
                  TW3Dataset.Next$1(Self);
                  if (TW3Dataset.GetEOF$1$(Self)) {
                     break;
                  } else if (TW3DatasetFilter.Execute$2(Self.FFilter$1)) {
                     break;
                  }
               }
               if (!TW3Dataset.GetEOF$1$(Self)) {
                  Result = TW3DatasetFilter.Execute$2(Self.FFilter$1);
               }
            }
         }
      } else {
         throw Exception.Create($New(EW3Dataset),"Dataset has no active filter error");
      }
      return Result
   }
   /// function TW3Dataset.FindPrior() : Boolean
   ,FindPrior:function(Self) {
      var Result = false;
      if (Self.FFiltered) {
         if (TW3Dataset.GetRecCount$(Self)>0) {
            if (!TW3Dataset.GetBOF$1$(Self)) {
               while (1) {
                  TW3Dataset.Back$1(Self);
                  if (TW3Dataset.GetBOF$1$(Self)) {
                     break;
                  } else if (TW3DatasetFilter.Execute$2(Self.FFilter$1)) {
                     break;
                  }
               }
               if (!TW3Dataset.GetBOF$1$(Self)) {
                  Result = TW3DatasetFilter.Execute$2(Self.FFilter$1);
               }
            }
         }
      } else {
         throw Exception.Create($New(EW3Dataset),"Dataset has no active filter error");
      }
      return Result
   }
   /// procedure TW3Dataset.First()
   ,First$1:function(Self) {
      if (Self.FActive$2) {
         if (!Self.FState$1) {
            TW3Dataset.SetPosition$3(Self,((TW3Dataset.GetRecCount$(Self)>0)?0:-1));
         } else {
            throw Exception.Create($New(EW3Dataset),$R[34]);
         }
      } else {
         throw Exception.Create($New(EW3Dataset),$R[33]);
      }
   }
   /// function TW3Dataset.GetBOF() : Boolean
   ,GetBOF$1:function(Self) {
      return (Self.FActive$2)?Self.FDsIndex<=0:true;
   }
   /// function TW3Dataset.GetEOF() : Boolean
   ,GetEOF$1:function(Self) {
      return (Self.FActive$2)?Self.FDsIndex>=TW3Dataset.GetRecCount$(Self):true;
   }
   /// procedure TW3Dataset.GetPacketToFields()
   ,GetPacketToFields:function(Self) {
      if (Self.FDsIndex>=0&&Self.FDsIndex<Self.FCache$1.length) {
         TW3DatasetFields.DataImport$(Self.FFields$2,Self.FCache$1[Self.FDsIndex]);
      }
   }
   /// function TW3Dataset.GetRecCount() : Integer
   ,GetRecCount:function(Self) {
      return Self.FCache$1.length;
   }
   /// function TW3Dataset.GetRecNo() : Integer
   ,GetRecNo:function(Self) {
      return (Self.FActive$2)?Self.FDsIndex:-1;
   }
   /// procedure TW3Dataset.Insert()
   ,Insert$1:function(Self) {
      if (Self.FActive$2) {
         if (!Self.FState$1) {
            if (TW3Dataset.GetRecNo$(Self)<0) {
               TW3Dataset.SetPosition$3(Self,Self.FCache$1.length);
            }
            TW3Dataset.SetState$1(Self,1);
            TW3DatasetFields.ResetFieldValues(Self.FFields$2);
            TW3Dataset.UpdateGeneratedFields(Self);
         } else {
            throw Exception.Create($New(EW3Dataset),$R[34]);
         }
      } else {
         throw Exception.Create($New(EW3Dataset),$R[33]);
      }
   }
   /// procedure TW3Dataset.Last()
   ,Last$1:function(Self) {
      if (Self.FActive$2) {
         if (!Self.FState$1) {
            TW3Dataset.SetPosition$3(Self,((TW3Dataset.GetRecCount$(Self)>0)?TW3Dataset.GetRecCount$(Self)-1:-1));
         } else {
            throw Exception.Create($New(EW3Dataset),$R[34]);
         }
      } else {
         throw Exception.Create($New(EW3Dataset),$R[33]);
      }
   }
   /// procedure TW3Dataset.LoadFromJSON(const TextData: String)
   ,LoadFromJSON$1:function(Self, TextData$1) {
      var Header$2 = {dhMagic:0,dhCount:0,dhFieldDefs:"",dhData:""};
      if (Self.FActive$2) {
         TW3Dataset.Close$1(Self);
      }
      if (TextData$1.length>0) {
         try {
            Header$2 = JSON.parse(TextData$1);
         } catch ($e) {
            var e$33 = $W($e);
            throw EW3Exception.CreateFmt$($New(EW3Dataset),$R[45],[e$33.FMessage]);
         }
         if (Header$2.dhMagic==51966) {
            if (Header$2.dhFieldDefs.length>0) {
               TW3FieldDefs.LoadFromJSON(Self.FDefs,TString.DecodeURI(TString,Header$2.dhFieldDefs));
            }
            if (Header$2.dhCount>0) {
               try {
                  (Self).FCache = JSON.parse((Header$2).dhData);
               } catch ($e) {
                  var e$34 = $W($e);
                  throw EW3Exception.CreateFmt$($New(EW3Dataset),$R[45],[e$34.FMessage]);
               }
            }
         } else {
            throw Exception.Create($New(EW3Dataset),$R[46]);
         }
      } else {
         throw Exception.Create($New(EW3Dataset),$R[47]);
      }
   }
   /// procedure TW3Dataset.LoadFromStream(const Stream: TStream)
   ,LoadFromStream$8:function(Self, Stream$17) {
      var Reader$6 = null,
         Signature = "",
         RowCount = 0;
      Reader$6 = TW3CustomReader.Create$39$($New(TReader),$AsIntf(Stream$17,"IBinaryTransport"));
      try {
         if (TW3CustomReader.ReadInteger(Reader$6)==2881141438) {
            Signature = TW3CustomReader.ReadString$1(Reader$6);
            if (Signature=="$dataset") {
               TW3FieldDefs.LoadFromStream$6(Self.FDefs,Stream$17);
               RowCount = TW3CustomReader.ReadInteger(Reader$6);
               Self.FDsIndex = 0;
               while (RowCount>0) {
                  Self.FCache$1.push(null);
                  Self.FDsIndex = Self.FCache$1.length-1;
                  TW3DatasetFields.LoadFromStream$7(Self.FFields$2,Stream$17);
                  TW3Dataset.SetPacketFromFields$(Self);
                  --RowCount;
               }
               Self.FActive$2 = true;
               Self.FDsIndex = -1;
            } else {
               throw Exception.Create($New(EW3Dataset),$R[42]);
            }
         } else {
            throw Exception.Create($New(EW3Dataset),$R[43]);
         }
      } finally {
         TObject.Free(Reader$6);
      }
   }
   /// procedure TW3Dataset.MoveTo(const NewRecNo: Integer)
   ,MoveTo:function(Self, NewRecNo) {
      if (Self.FActive$2) {
         if (!Self.FState$1) {
            TW3Dataset.SetPosition$3(Self,TInteger.EnsureRange(NewRecNo,0,(TW3Dataset.GetRecCount$(Self)-1)));
         } else {
            throw Exception.Create($New(EW3Dataset),$R[34]);
         }
      } else {
         throw Exception.Create($New(EW3Dataset),$R[33]);
      }
   }
   /// procedure TW3Dataset.Next()
   ,Next$1:function(Self) {
      if (Self.FActive$2) {
         if (!Self.FState$1) {
            TW3Dataset.SetPosition$3(Self,TInteger.EnsureRange((Self.FDsIndex+1),(-1),TW3Dataset.GetRecCount$(Self)));
         } else {
            throw Exception.Create($New(EW3Dataset),$R[34]);
         }
      } else {
         throw Exception.Create($New(EW3Dataset),$R[33]);
      }
   }
   /// procedure TW3Dataset.Post()
   ,Post:function(Self) {
      var VoidData = undefined;
      if (Self.FActive$2) {
         switch (Self.FState$1) {
            case 1 :
               if (Self.FDsIndex==TW3Dataset.GetRecCount$(Self)) {
                  Self.FCache$1.push(VoidData);
                  Self.FDsIndex = Self.FCache$1.length-1;
               } else {
                  Self.FCache$1.splice(Self.FDsIndex,0,VoidData);
               }
               TW3Dataset.SetPacketFromFields$(Self);
               if (Self.FOnAdd) {
                  Self.FOnAdd(Self);
               }
               TW3Dataset.SetState$1(Self,0);
               break;
            case 2 :
               TW3Dataset.SetPacketFromFields$(Self);
               TW3Dataset.SetState$1(Self,0);
               break;
            default :
               throw Exception.Create($New(EW3Dataset),$R[34]);
         }
      } else {
         throw Exception.Create($New(EW3Dataset),$R[33]);
      }
   }
   /// function TW3Dataset.SaveToJSON() : String
   ,SaveToJSON$1:function(Self) {
      var Result = "";
      var Header$3 = {dhMagic:0,dhCount:0,dhFieldDefs:"",dhData:""};
      if (Self.FActive$2) {
         try {
            Header$3.dhMagic = 51966;
            Header$3.dhCount = TW3Dataset.GetRecCount$(Self);
            Header$3.dhFieldDefs = TString.EncodeURI(TString,TW3FieldDefs.SaveToJSON(Self.FDefs));
            (Header$3).dhData = JSON.stringify((Self).FCache);
            Result = JSON.stringify(Header$3);
         } catch ($e) {
            var e$35 = $W($e);
            throw EW3Exception.CreateFmt$($New(EW3Dataset),$R[44],[e$35.FMessage]);
         }
      }
      return Result
   }
   /// procedure TW3Dataset.SaveToStream(const Stream: TStream)
   ,SaveToStream$8:function(Self, Stream$18) {
      var Writer$6 = null,
         OldRecNo = 0,
         x$53 = 0;
      if (Self.FActive$2) {
         Writer$6 = TW3CustomWriter.Create$29$($New(TWriter),$AsIntf(Stream$18,"IBinaryTransport"));
         try {
            TW3CustomWriter.WriteInteger(Writer$6,2881141438);
            TW3CustomWriter.WriteString(Writer$6,"$dataset");
            TW3FieldDefs.SaveToStream$6(Self.FDefs,Stream$18);
            TW3CustomWriter.WriteInteger(Writer$6,TW3Dataset.GetRecCount$(Self));
            if (TW3Dataset.GetRecCount$(Self)>0) {
               OldRecNo = Self.FDsIndex;
               var $temp87;
               for(x$53=0,$temp87=Self.FCache$1.length;x$53<$temp87;x$53++) {
                  TW3DatasetFields.DataImport$(Self.FFields$2,Self.FCache$1[x$53]);
                  TW3DatasetFields.SaveToStream$7(Self.FFields$2,Stream$18);
               }
               if (Self.FDsIndex!=OldRecNo) {
                  Self.FDsIndex = OldRecNo;
                  TW3Dataset.GetPacketToFields$(Self);
               }
            }
         } finally {
            TObject.Free(Writer$6);
         }
      }
   }
   /// procedure TW3Dataset.SetActive(const NewActiveState: Boolean)
   ,SetActive$2:function(Self, NewActiveState$1) {
      if (NewActiveState$1!=Self.FActive$2) {
         if (NewActiveState$1) {
            TW3Dataset.CreateDataset(Self);
         } else {
            TW3Dataset.Close$1(Self);
         }
      }
   }
   /// procedure TW3Dataset.SetFiltered(const NewFilterActive: Boolean)
   ,SetFiltered:function(Self, NewFilterActive) {
      var ErrorMessage = "";
      function ResetState() {
         try {
            if (Self.FFilter$1!==null) {
               TObject.Free(Self.FFilter$1);
               Self.FFilter$1 = null;
            }
         } finally {
            Self.FFiltered = false;
         }
      };
      if (NewFilterActive!=Self.FFiltered) {
         if (NewFilterActive==false) {
            ResetState();
         } else {
            Self.FFilterText$1 = Trim$_String_(Self.FFilterText$1);
            if (Self.FFilterText$1.length>0) {
               Self.FFilter$1 = TW3DatasetFilter.Create$80$($New(TW3DatasetFilter),Self);
               Self.FFiltered = TW3DatasetFilter.Parse$2(Self.FFilter$1,Self.FFilterText$1);
               if (!Self.FFiltered) {
                  ErrorMessage = Self.FFilter$1.FLastError;
                  ResetState();
                  throw Exception.Create($New(EW3Exception),ErrorMessage);
               }
            } else {
               ResetState();
            }
         }
      }
   }
   /// procedure TW3Dataset.SetFilterText(NewFilterText: String)
   ,SetFilterText:function(Self, NewFilterText) {
      if (Self.FFiltered) {
         TW3Dataset.SetFiltered$(Self,false);
      }
      if (Self.FFilter$1!==null) {
         Self.FFiltered = false;
         TObject.Free(Self.FFilter$1);
         Self.FFilter$1 = null;
      }
      Self.FFilterText$1 = Trim$_String_(NewFilterText);
   }
   /// procedure TW3Dataset.SetPacketFromFields()
   ,SetPacketFromFields:function(Self) {
      if (Self.FDsIndex>=0&&Self.FDsIndex<Self.FCache$1.length) {
         Self.FCache$1[Self.FDsIndex]=TW3DatasetFields.DataExport$(Self.FFields$2);
      }
   }
   /// procedure TW3Dataset.SetPosition(const NewPosition: Integer)
   ,SetPosition$3:function(Self, NewPosition$2) {
      var OldPosition = 0;
      if (NewPosition$2!=Self.FDsIndex) {
         OldPosition = Self.FDsIndex;
         Self.FDsIndex = NewPosition$2;
         if (!(Self.FState$1==1)) {
            if (Self.FDsIndex>=0&&Self.FDsIndex<TW3Dataset.GetRecCount$(Self)) {
               TW3Dataset.GetPacketToFields$(Self);
            } else {
               TW3DatasetFields.ResetFieldValues(Self.FFields$2);
            }
         }
         if (Self.FOnPos) {
            Self.FOnPos(Self,OldPosition,NewPosition$2);
         }
      }
   }
   /// procedure TW3Dataset.SetState(const NewDatasetState: TW3DatasetState)
   ,SetState$1:function(Self, NewDatasetState) {
      Self.FState$1 = NewDatasetState;
      if (Self.FOnState) {
         Self.FOnState(Self,NewDatasetState);
      }
   }
   /// procedure TW3Dataset.UpdateGeneratedFields()
   ,UpdateGeneratedFields:function(Self) {
      var x$54 = 0;
      var Field$12 = null,
         FieldDef = null;
      var $temp88;
      for(x$54=0,$temp88=TW3DatasetFields.a$145(Self.FFields$2);x$54<$temp88;x$54++) {
         Field$12 = TW3DatasetFields.a$146(Self.FFields$2,x$54);
         if (TW3DatasetField.GetGenerated$(Field$12)) {
            TW3DatasetField.Generate$2$(Field$12);
         }
         FieldDef = TW3FieldDefs.FieldByName(Self.FDefs,Field$12.FName$1);
         TW3DatasetField.SetKind$(Field$12,FieldDef.FDatatype);
      }
   }
   /// function TW3Dataset.Version() : String
   ,Version$1:function(Self) {
      return "0.3";
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$82$:function($){return $.ClassType.Create$82($)}
   ,DoAfterDatasetClosed$:function($){return $.ClassType.DoAfterDatasetClosed($)}
   ,DoAfterDatasetCreated$:function($){return $.ClassType.DoAfterDatasetCreated($)}
   ,DoBeforeDatasetClosed$:function($){return $.ClassType.DoBeforeDatasetClosed($)}
   ,DoBeforeDatasetCreated$:function($){return $.ClassType.DoBeforeDatasetCreated($)}
   ,GetBOF$1$:function($){return $.ClassType.GetBOF$1($)}
   ,GetEOF$1$:function($){return $.ClassType.GetEOF$1($)}
   ,GetPacketToFields$:function($){return $.ClassType.GetPacketToFields($)}
   ,GetRecCount$:function($){return $.ClassType.GetRecCount($)}
   ,GetRecNo$:function($){return $.ClassType.GetRecNo($)}
   ,LoadFromStream$8$:function($){return $.ClassType.LoadFromStream$8.apply($.ClassType, arguments)}
   ,SaveToStream$8$:function($){return $.ClassType.SaveToStream$8.apply($.ClassType, arguments)}
   ,SetFiltered$:function($){return $.ClassType.SetFiltered.apply($.ClassType, arguments)}
   ,SetFilterText$:function($){return $.ClassType.SetFilterText.apply($.ClassType, arguments)}
   ,SetPacketFromFields$:function($){return $.ClassType.SetPacketFromFields($)}
};
/// TW3BooleanField = class (TW3DatasetField)
var TW3BooleanField = {
   $ClassName:"TW3BooleanField",$Parent:TW3DatasetField
   ,$Init:function ($) {
      TW3DatasetField.$Init($);
   }
   /// anonymous TSourceMethodSymbol
   ,a$148:function(Self) {
      return TVariant.AsBool(TW3DatasetField.GetValue(Self));
   }
   /// anonymous TSourceMethodSymbol
   ,a$147:function(Self, Value$163) {
      TW3DatasetField.SetValue(Self,Value$163);
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3DatasetField.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3DatasetObject.ObjectLocked
   ,ObjectUnLocked:TW3DatasetObject.ObjectUnLocked
   ,SetReadOnly:TW3DatasetObject.SetReadOnly
   ,Create$75:TW3DatasetField.Create$75
   ,Generate$2:TW3DatasetField.Generate$2
   ,GetGenerated:TW3DatasetField.GetGenerated
   ,GetOwner$1:TW3DatasetField.GetOwner$1
   ,GetValue:TW3DatasetField.GetValue
   ,LoadFromStream$5:TW3DatasetField.LoadFromStream$5
   ,ReadBinaryValue:TW3DatasetField.ReadBinaryValue
   ,SaveToStream$5:TW3DatasetField.SaveToStream$5
   ,SetKind:TW3DatasetField.SetKind
   ,SetName$2:TW3DatasetField.SetName$2
   ,SetValue:TW3DatasetField.SetValue
   ,WriteBinaryValue:TW3DatasetField.WriteBinaryValue
};
TW3BooleanField.$Intf={
   IW3OwnedObjectAccess:[TW3DatasetField.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// TW3AutoIncField = class (TW3DatasetField)
var TW3AutoIncField = {
   $ClassName:"TW3AutoIncField",$Parent:TW3DatasetField
   ,$Init:function ($) {
      TW3DatasetField.$Init($);
      $.FCalc = 0;
   }
   /// anonymous TSourceMethodSymbol
   ,a$149:function(Self) {
      return TVariant.AsInteger(TW3DatasetField.GetValue(Self));
   }
   /// procedure TW3AutoIncField.Generate()
   ,Generate$2:function(Self) {
      ++Self.FCalc;
      TW3DatasetField.SetValue(Self,Self.FCalc);
   }
   /// function TW3AutoIncField.GetGenerated() : Boolean
   ,GetGenerated:function(Self) {
      return true;
   }
   /// procedure TW3AutoIncField.ReadBinaryValue(const Reader: TReader)
   ,ReadBinaryValue:function(Self, Reader$7) {
      Self.FCalc = TW3CustomReader.ReadInteger(Reader$7);
      TW3DatasetField.SetValue$(Self,TW3CustomReader.ReadInteger(Reader$7));
   }
   /// procedure TW3AutoIncField.WriteBinaryValue(const Writer: TWriter)
   ,WriteBinaryValue:function(Self, Writer$7) {
      TW3CustomWriter.WriteInteger(Writer$7,Self.FCalc);
      TW3CustomWriter.WriteInteger(Writer$7,TW3DatasetField.a$124(Self));
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3DatasetField.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3DatasetObject.ObjectLocked
   ,ObjectUnLocked:TW3DatasetObject.ObjectUnLocked
   ,SetReadOnly:TW3DatasetObject.SetReadOnly
   ,Create$75:TW3DatasetField.Create$75
   ,Generate$2$:function($){return $.ClassType.Generate$2($)}
   ,GetGenerated$:function($){return $.ClassType.GetGenerated($)}
   ,GetOwner$1:TW3DatasetField.GetOwner$1
   ,GetValue:TW3DatasetField.GetValue
   ,LoadFromStream$5:TW3DatasetField.LoadFromStream$5
   ,ReadBinaryValue$:function($){return $.ClassType.ReadBinaryValue.apply($.ClassType, arguments)}
   ,SaveToStream$5:TW3DatasetField.SaveToStream$5
   ,SetKind:TW3DatasetField.SetKind
   ,SetName$2:TW3DatasetField.SetName$2
   ,SetValue:TW3DatasetField.SetValue
   ,WriteBinaryValue$:function($){return $.ClassType.WriteBinaryValue.apply($.ClassType, arguments)}
};
TW3AutoIncField.$Intf={
   IW3OwnedObjectAccess:[TW3DatasetField.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// function TFilterSymbolHelper.ToString(const Self: TW3FilterSymbol) : String
function TFilterSymbolHelper$ToString$13(Self$47) {
   var Result = "";
   switch (Self$47) {
      case 0 :
         Result = "invalid";
         break;
      case 1 :
         Result = "equals";
         break;
      case 2 :
         Result = "greater";
         break;
      case 3 :
         Result = "less";
         break;
      case 4 :
         Result = "less-greater";
         break;
      case 5 :
         Result = "like";
         break;
   }
   return Result
}
/// function TFilterOperatorHelper.ToString(const Self: TW3FilterOperator) : String
function TFilterOperatorHelper$ToString$12(Self$48) {
   var Result = "";
   switch (Self$48) {
      case 1 :
         Result = "and";
         break;
      case 2 :
         Result = "or";
         break;
      case 3 :
         Result = "not";
         break;
      default :
         Result = "invalid";
   }
   return Result
}
/// EW3FilterValue = class (EW3Exception)
var EW3FilterValue = {
   $ClassName:"EW3FilterValue",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EW3FilterExpression = class (EW3Exception)
var EW3FilterExpression = {
   $ClassName:"EW3FilterExpression",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EW3FieldDefs = class (EW3Exception)
var EW3FieldDefs = {
   $ClassName:"EW3FieldDefs",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EW3FieldDef = class (EW3Exception)
var EW3FieldDef = {
   $ClassName:"EW3FieldDef",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EW3DatasetFilter = class (EW3Exception)
var EW3DatasetFilter = {
   $ClassName:"EW3DatasetFilter",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EW3DatasetFields = class (EW3Exception)
var EW3DatasetFields = {
   $ClassName:"EW3DatasetFields",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EW3DatasetField = class (EW3Exception)
var EW3DatasetField = {
   $ClassName:"EW3DatasetField",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EW3Dataset = class (EW3Exception)
var EW3Dataset = {
   $ClassName:"EW3Dataset",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TW3FieldDefHeader = record
function Copy$TW3FieldDefHeader(s,d) {
   d.ddMagic=s.ddMagic;
   d.ddDefs=s.ddDefs;
   return d;
}
function Clone$TW3FieldDefHeader($) {
   return {
      ddMagic:$.ddMagic,
      ddDefs:$.ddDefs
   }
}
/// TW3FieldDefData = record
function Copy$TW3FieldDefData(s,d) {
   d.fdName=s.fdName;
   d.fdDatatype=s.fdDatatype;
   return d;
}
function Clone$TW3FieldDefData($) {
   return {
      fdName:$.fdName,
      fdDatatype:$.fdDatatype
   }
}
/// TW3DatasetHeader = record
function Copy$TW3DatasetHeader(s,d) {
   d.dhMagic=s.dhMagic;
   d.dhCount=s.dhCount;
   d.dhFieldDefs=s.dhFieldDefs;
   d.dhData=s.dhData;
   return d;
}
function Clone$TW3DatasetHeader($) {
   return {
      dhMagic:$.dhMagic,
      dhCount:$.dhCount,
      dhFieldDefs:$.dhFieldDefs,
      dhData:$.dhData
   }
}
function SetupFilterLUT() {
   _OperandLUT = TVariant.CreateObject();
   _OperandLUT["<"] = 3;
   _OperandLUT["="] = 1;
   _OperandLUT[">"] = 2;
   _OperandLUT["<>"] = 4;
   _OperandLUT["like"] = 5;
   _StatementLUT = TVariant.CreateObject();
   _StatementLUT["and"] = 1;
   _StatementLUT["or"] = 2;
   _StatementLUT["not"] = 3;
   _Reserved.push("and");
   _Reserved.push("or");
   _Reserved.push("not");
};
/// TSQLite3WriteTransaction = class (TW3WriteTransaction)
var TSQLite3WriteTransaction = {
   $ClassName:"TSQLite3WriteTransaction",$Parent:TW3WriteTransaction
   ,$Init:function ($) {
      TW3WriteTransaction.$Init($);
   }
   /// procedure TSQLite3WriteTransaction.Execute(CB: TW3DatabaseCBStandard)
   ,Execute$1:function(Self, CB$22) {
      var LStatement = null;
      TW3OwnedErrorObject.ClearLastError$1(Self);
      if (!Self.FHandle$4) {
         if (TW3Transaction.GetDatabase$(Self)!==null) {
            try {
               TSQLite3Database.Prepare($As(TW3Transaction.GetDatabase$(Self),TSQLite3Database),Self);
            } catch ($e) {
               var e$36 = $W($e);
               TW3OwnedErrorObject.SetLastError$1$(Self,e$36.FMessage);
               if (CB$22) {
                  CB$22(Self,false);
               }
               return;
            }
            LStatement = Self.FHandle$4;
            LStatement.run(TW3TransactionParameters.GetValues$1(Self.FParams),function (Error$19) {
               if (Error$19!==null) {
                  TW3OwnedErrorObject.SetLastError$1$(Self,Error$19.message);
                  if (CB$22) {
                     CB$22(Self,false);
                  }
                  return;
               }
               if (CB$22) {
                  CB$22(Self,true);
               }
            });
         }
      }
   }
   ,Destroy:TW3Component.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,SetLastError$1:TW3OwnedErrorObject.SetLastError$1
   ,ObjectLocked$2:TW3OwnedLockedErrorObject.ObjectLocked$2
   ,ObjectUnLocked$2:TW3OwnedLockedErrorObject.ObjectUnLocked$2
   ,Create$44:TW3Component.Create$44
   ,CreateEx:TW3Component.CreateEx
   ,FinalizeObject:TW3Transaction.FinalizeObject
   ,FreeAfter:TW3Component.FreeAfter
   ,InitializeObject:TW3Transaction.InitializeObject
   ,CanExecute:TW3SQLTransaction.CanExecute
   ,GetDatabase:TW3Transaction.GetDatabase
   ,SetDatabase:TW3Transaction.SetDatabase
   ,Execute$1$:function($){return $.ClassType.Execute$1.apply($.ClassType, arguments)}
   ,GetSQL:TW3SQLTransaction.GetSQL
   ,SetHandle:TW3SQLTransaction.SetHandle
   ,SetSQL:TW3SQLTransaction.SetSQL
};
TSQLite3WriteTransaction.$Intf={
   ISQLTransaction:[TW3SQLTransaction.SetHandle,TW3SQLTransaction.GetSQL,TW3SQLTransaction.SetSQL,TW3SQLTransaction.CanExecute]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TSQLite3ReadTransaction = class (TW3ReadTransaction)
var TSQLite3ReadTransaction = {
   $ClassName:"TSQLite3ReadTransaction",$Parent:TW3ReadTransaction
   ,$Init:function ($) {
      TW3ReadTransaction.$Init($);
   }
   /// procedure TSQLite3ReadTransaction.Execute(CB: TW3DatabaseCBStandard)
   ,Execute$1:function(Self, CB$23) {
      var LStatement$1 = null;
      TW3OwnedErrorObject.ClearLastError$1(Self);
      if (!Self.FHandle$4) {
         if (TW3Transaction.GetDatabase$(Self)!==null) {
            try {
               TSQLite3Database.Prepare($As(TW3Transaction.GetDatabase$(Self),TSQLite3Database),Self);
            } catch ($e) {
               var e$37 = $W($e);
               TW3OwnedErrorObject.SetLastError$1$(Self,e$37.FMessage);
               if (CB$23) {
                  CB$23(Self,false);
               }
               return;
            }
            LStatement$1 = Self.FHandle$4;
            LStatement$1.all(TW3TransactionParameters.GetValues$1(Self.FParams),function (Error$20, Rows) {
               if (Error$20!==null) {
                  TW3OwnedErrorObject.SetLastError$1$(Self,Error$20.message);
                  if (CB$23) {
                     CB$23(Self,false);
                  }
                  return;
               }
            });
         }
      }
   }
   ,Destroy:TW3Component.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,SetLastError$1:TW3OwnedErrorObject.SetLastError$1
   ,ObjectLocked$2:TW3OwnedLockedErrorObject.ObjectLocked$2
   ,ObjectUnLocked$2:TW3OwnedLockedErrorObject.ObjectUnLocked$2
   ,Create$44:TW3Component.Create$44
   ,CreateEx:TW3Component.CreateEx
   ,FinalizeObject:TW3Transaction.FinalizeObject
   ,FreeAfter:TW3Component.FreeAfter
   ,InitializeObject:TW3Transaction.InitializeObject
   ,CanExecute:TW3SQLTransaction.CanExecute
   ,GetDatabase:TW3Transaction.GetDatabase
   ,SetDatabase:TW3Transaction.SetDatabase
   ,Execute$1$:function($){return $.ClassType.Execute$1.apply($.ClassType, arguments)}
   ,GetSQL:TW3SQLTransaction.GetSQL
   ,SetHandle:TW3SQLTransaction.SetHandle
   ,SetSQL:TW3SQLTransaction.SetSQL
};
TSQLite3ReadTransaction.$Intf={
   ISQLTransaction:[TW3SQLTransaction.SetHandle,TW3SQLTransaction.GetSQL,TW3SQLTransaction.SetSQL,TW3SQLTransaction.CanExecute]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TSQLite3Database = class (TW3Database)
var TSQLite3Database = {
   $ClassName:"TSQLite3Database",$Parent:TW3Database
   ,$Init:function ($) {
      TW3Database.$Init($);
      $.FAccMode = 0;
      $.FHandle$5 = undefined;
   }
   /// function TSQLite3Database.AccessModeToAPIValue() : Integer
   ,AccessModeToAPIValue:function(Self) {
      var Result = 0;
      var LAccess = null;
      LAccess = SQLite3();
      switch (Self.FAccMode) {
         case 0 :
            Result = LAccess.OPEN_CREATE;
            break;
         case 1 :
            Result = LAccess.OPEN_READONLY;
            break;
         case 2 :
            Result = LAccess.OPEN_READWRITE;
            break;
         case 3 :
            Result = LAccess.OPEN_READWRITE|LAccess.OPEN_CREATE;
            break;
         case 4 :
            Result = LAccess.OPEN_READWRITE|LAccess.OPEN_CREATE;
            break;
      }
      return Result
   }
   /// procedure TSQLite3Database.apiClose(CB: TW3DatabaseCBStandard)
   ,apiClose:function(Self, CB$24) {
      var Access$22 = null;
      TW3OwnedErrorObject.ClearLastError$1(Self);
      if (TW3Database.GetActive$1$(Self)) {
         Access$22 = TSQLite3Database.GetDBAPI(Self);
         Access$22.close(function (err$1) {
            if (err$1) {
               WriteLn("Failed to close: "+err$1.message);
               TW3OwnedErrorObject.SetLastError$1$(Self,err$1.message);
            }
            try {
               TSQLite3Database.SetHandle$1$(Self,null);
            } finally {
               if (CB$24) {
                  CB$24(Self,err$1===null);
               }
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastError$1$(Self,"Database not active error");
         if (CB$24) {
            CB$24(Self,false);
         }
      }
   }
   /// procedure TSQLite3Database.apiOpen(CB: TW3DatabaseCBStandard)
   ,apiOpen:function(Self, CB$25) {
      var LDBRef = undefined;
      var LLibRef = null,
         LMode = 0,
         LFilename$1 = "";
      LLibRef = SQLite3();
      LMode = TSQLite3Database.AccessModeToAPIValue$(Self);
      LFilename$1 = TW3Database.GetLocation(Self);
      try {
         LDBRef = new (LLibRef).Database(LFilename$1, LMode);
         TSQLite3Database.SetHandle$1$(Self,LDBRef);
         if (CB$25) {
            CB$25(Self,true);
         }
      } catch ($e) {
         var e$38 = $W($e);
         TW3OwnedErrorObject.SetLastError$1$(Self,e$38.FMessage);
         if (CB$25) {
            CB$25(Self,false);
         } else {
            throw $e;
         }
      }
   }
   /// function TSQLite3Database.CreateReadTransaction(var Transaction: TW3ReadTransaction) : Boolean
   ,CreateReadTransaction:function(Self, Transaction$2) {
      var Result = false;
      Transaction$2.v = TW3Component.Create$44$($New(TSQLite3ReadTransaction),Self);
      TW3Transaction.SetDatabase$(Transaction$2.v,Self);
      TW3Database.RegisterTransaction(Self,Transaction$2.v);
      Result = true;
      return Result
   }
   /// function TSQLite3Database.CreateWriteTransaction(var Transaction: TW3WriteTransaction) : Boolean
   ,CreateWriteTransaction:function(Self, Transaction$3) {
      var Result = false;
      Transaction$3.v = TW3Component.Create$44$($New(TSQLite3WriteTransaction),Self);
      TW3Transaction.SetDatabase$(Transaction$3.v,Self);
      TW3Database.RegisterTransaction(Self,Transaction$3.v);
      Result = true;
      return Result
   }
   /// procedure TSQLite3Database.Execute(SQL: String; CB: TSQLite3Callback)
   ,Execute$6:function(Self, SQL$1, CB$26) {
      var Access$23 = null;
      TW3OwnedErrorObject.ClearLastError$1(Self);
      if (TW3Database.GetActive$1$(Self)) {
         Access$23 = TSQLite3Database.GetDBAPI(Self);
         Access$23.run(SQL$1,function (err$2) {
            if (err$2) {
               TW3OwnedErrorObject.SetLastError$1$(Self,err$2.message);
               if (CB$26) {
                  CB$26(Self,false);
               }
               return;
            }
            if (CB$26) {
               CB$26(Self,true);
            }
         });
      } else {
         throw Exception.Create($New(Exception),$R[29]);
      }
   }
   /// procedure TSQLite3Database.Execute(SQL: String; const Params: array of const; const CB: TSQLite3Callback)
   ,Execute$5:function(Self, SQL$2, Params$1, CB$27) {
      var Access$24 = null;
      TW3OwnedErrorObject.ClearLastError$1(Self);
      if (TW3Database.GetActive$1$(Self)) {
         Access$24 = TSQLite3Database.GetDBAPI(Self);
         Access$24.run(SQL$2,Params$1.slice(0),function (err$3) {
            if (err$3) {
               TW3OwnedErrorObject.SetLastError$1$(Self,err$3.message);
               if (CB$27) {
                  CB$27(Self,false);
               }
            } else if (CB$27) {
               CB$27(Self,true);
            }
         });
      } else {
         throw Exception.Create($New(Exception),$R[29]);
      }
   }
   /// procedure TSQLite3Database.ForEach(Sql: String; Params: TJSONStructure; CB: TSQLite3RecordEnumProc)
   ,ForEach$5:function(Self, Sql, Params$2, CB$28) {
      /* null */
   }
   /// function TSQLite3Database.GetDBAPI() : JSQLite3Database
   ,GetDBAPI:function(Self) {
      var Result = null;
      if (Self.FHandle$5) {
         Result = Self.FHandle$5;
      } else {
         Result = null;
      }
      return Result
   }
   /// procedure TSQLite3Database.InitializeObject()
   ,InitializeObject:function(Self) {
      TW3Component.InitializeObject(Self);
      Self.FAccMode = 3;
      Self.FHandle$5 = undefined;
      Self.FOptions$4.AutoWriteToConsole = true;
   }
   /// procedure TSQLite3Database.Prepare(const Transaction: TW3SQLTransaction)
   ,Prepare:function(Self, Transaction$4) {
      var API = null,
         res = null;
      if (Transaction$4!==null) {
         if (TW3Database.GetActive$1$(Self)) {
            API = TSQLite3Database.GetDBAPI(Self);
            res = API.prepare(Transaction$4.FSQL);
            $AsIntf(Transaction$4,"ISQLTransaction")[0](res);
         } else {
            throw Exception.Create($New(ESQLite3Database),"Prepare failed, database not active error");
         }
      } else {
         throw Exception.Create($New(ESQLite3Database),"Prepare failed, transaction was nil error");
      }
   }
   /// procedure TSQLite3Database.RecordCount(const TableName: String; const CB: TSQLite3CountCallback)
   ,RecordCount:function(Self, TableName, CB$29) {
      var Access$25 = null,
         LSQL = "";
      TW3OwnedErrorObject.ClearLastError$1(Self);
      if (TW3Database.GetActive$1$(Self)) {
         if (CB$29) {
            Access$25 = TSQLite3Database.GetDBAPI(Self);
            LSQL = ("SELECT COUNT(*) as QCount from "+TableName.toString());
            Access$25.all(LSQL,[],function (Error$21, Rows$1) {
               if (Error$21) {
                  if (CB$29) {
                     CB$29(-1);
                  }
                  return;
               }
               CB$29(TVariant.AsInteger(Rows$1[0].QCount));
            });
         }
      } else {
         throw Exception.Create($New(Exception),$R[29]);
      }
   }
   /// procedure TSQLite3Database.SetAccessMode(const NewMode: TSQLite3AccessMode)
   ,SetAccessMode$1:function(Self, NewMode) {
      if (NewMode!=Self.FAccMode) {
         if (TW3Database.GetActive$1$(Self)) {
            throw Exception.Create($New(EW3Exception),"Accessmode cannot be altered on a live database");
         } else {
            Self.FAccMode = NewMode;
         }
      }
   }
   /// procedure TSQLite3Database.SetHandle(NewHandle: THandle)
   ,SetHandle$1:function(Self, NewHandle$3) {
      Self.FHandle$5 = NewHandle$3;
   }
   ,Destroy:TW3Component.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,SetLastError$1:TW3OwnedErrorObject.SetLastError$1
   ,ObjectLocked$2:TW3OwnedLockedErrorObject.ObjectLocked$2
   ,ObjectUnLocked$2:TW3OwnedLockedErrorObject.ObjectUnLocked$2
   ,Create$44:TW3Component.Create$44
   ,CreateEx:TW3Component.CreateEx
   ,FinalizeObject:TW3Component.FinalizeObject
   ,FreeAfter:TW3Component.FreeAfter
   ,InitializeObject$:function($){return $.ClassType.InitializeObject($)}
   ,apiAfterClose:TW3Database.apiAfterClose
   ,apiAfterOpen:TW3Database.apiAfterOpen
   ,apiBeforeClose:TW3Database.apiBeforeClose
   ,apiBeforeOpen:TW3Database.apiBeforeOpen
   ,apiClose$:function($){return $.ClassType.apiClose.apply($.ClassType, arguments)}
   ,apiOpen$:function($){return $.ClassType.apiOpen.apply($.ClassType, arguments)}
   ,CreateReadTransaction$:function($){return $.ClassType.CreateReadTransaction.apply($.ClassType, arguments)}
   ,CreateWriteTransaction$:function($){return $.ClassType.CreateWriteTransaction.apply($.ClassType, arguments)}
   ,GetActive$1:TW3Database.GetActive$1
   ,GetState:TW3Database.GetState
   ,SetLocation:TW3Database.SetLocation
   ,SetState:TW3Database.SetState
   ,AccessModeToAPIValue$:function($){return $.ClassType.AccessModeToAPIValue($)}
   ,Execute$6$:function($){return $.ClassType.Execute$6.apply($.ClassType, arguments)}
   ,Execute$5$:function($){return $.ClassType.Execute$5.apply($.ClassType, arguments)}
   ,SetAccessMode$1$:function($){return $.ClassType.SetAccessMode$1.apply($.ClassType, arguments)}
   ,SetHandle$1$:function($){return $.ClassType.SetHandle$1.apply($.ClassType, arguments)}
};
TSQLite3Database.$Intf={
   IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TSQLite3AccessMode enumeration
var TSQLite3AccessMode = [ "sqaCreate", "sqaReadOnly", "sqaReadWrite", "sqaReadWriteCreate", "sqaDefault" ];
/// ESQLite3Transaction = class (EW3Transaction)
var ESQLite3Transaction = {
   $ClassName:"ESQLite3Transaction",$Parent:EW3Transaction
   ,$Init:function ($) {
      EW3Transaction.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// ESQLite3Database = class (EW3Database)
var ESQLite3Database = {
   $ClassName:"ESQLite3Database",$Parent:EW3Database
   ,$Init:function ($) {
      EW3Database.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
function EventsModule() {
   return __EventsRef;
};
function events() {
   return __EventsRef;
};
function CreateEventEmitter() {
   var Result = null;
   Result = new (__EventsRef).EventEmitter;
   return Result
};
function stream() {
   return require("stream");
};
function SQLite3() {
   return require("sqlite3").verbose();
};
function WriteLnF(Text$36, Data$67) {
   util().log(Format(String(Text$36),Data$67.slice(0)));
};
function WriteLn(Text$37) {
   util().log(String(Text$37));
};
/// TW3Win32DirectoryParser = class (TW3UnixDirectoryParser)
var TW3Win32DirectoryParser = {
   $ClassName:"TW3Win32DirectoryParser",$Parent:TW3UnixDirectoryParser
   ,$Init:function ($) {
      TW3UnixDirectoryParser.$Init($);
   }
   /// function TW3Win32DirectoryParser.GetPathSeparator() : Char
   ,GetPathSeparator:function(Self) {
      return "\\";
   }
   /// function TW3Win32DirectoryParser.GetRootMoniker() : String
   ,GetRootMoniker:function(Self) {
      var Result = "";
      function GetDriveFrom(ThisPath) {
         var Result = "";
         var xpos$2 = 0;
         xpos$2 = (ThisPath.indexOf(":\\")+1);
         if (xpos$2>=2) {
            ++xpos$2;
            Result = ThisPath.substr(0,xpos$2);
         }
         return Result
      };
      Result = (GetDriveFrom(ParamStr$1(1))).toLocaleLowerCase();
      if (Result.length<2) {
         Result = GetDriveFrom(ParamStr$1(0));
         if (Result.length<2) {
            throw Exception.Create($New(Exception),"Failed to extract root moniker from script path error");
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$3:TW3ErrorObject.Create$3
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,ChangeFileExt:TW3UnixDirectoryParser.ChangeFileExt
   ,ExcludeLeadingPathDelimiter:TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter
   ,ExcludeTrailingPathDelimiter:TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter
   ,GetDevice:TW3UnixDirectoryParser.GetDevice
   ,GetDirectoryName:TW3UnixDirectoryParser.GetDirectoryName
   ,GetExtension:TW3UnixDirectoryParser.GetExtension
   ,GetFileName:TW3UnixDirectoryParser.GetFileName
   ,GetFileNameWithoutExtension:TW3UnixDirectoryParser.GetFileNameWithoutExtension
   ,GetPathName:TW3UnixDirectoryParser.GetPathName
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars:TW3UnixDirectoryParser.HasValidFileNameChars
   ,HasValidPathChars:TW3UnixDirectoryParser.HasValidPathChars
   ,IncludeLeadingPathDelimiter:TW3UnixDirectoryParser.IncludeLeadingPathDelimiter
   ,IncludeTrailingPathDelimiter:TW3UnixDirectoryParser.IncludeTrailingPathDelimiter
   ,IsPathRooted:TW3DirectoryParser.IsPathRooted
   ,IsRelativePath:TW3DirectoryParser.IsRelativePath
   ,IsValidPath:TW3UnixDirectoryParser.IsValidPath
};
TW3Win32DirectoryParser.$Intf={
   IW3DirectoryParser:[TW3Win32DirectoryParser.GetPathSeparator,TW3Win32DirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3UnixDirectoryParser.IsValidPath,TW3UnixDirectoryParser.HasValidPathChars,TW3UnixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3UnixDirectoryParser.GetFileNameWithoutExtension,TW3UnixDirectoryParser.GetPathName,TW3UnixDirectoryParser.GetDevice,TW3UnixDirectoryParser.GetFileName,TW3UnixDirectoryParser.GetExtension,TW3UnixDirectoryParser.GetDirectoryName,TW3UnixDirectoryParser.IncludeTrailingPathDelimiter,TW3UnixDirectoryParser.IncludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeLeadingPathDelimiter,TW3UnixDirectoryParser.ExcludeTrailingPathDelimiter,TW3UnixDirectoryParser.ChangeFileExt]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TW3PosixDirectoryParser = class (TW3DirectoryParser)
var TW3PosixDirectoryParser = {
   $ClassName:"TW3PosixDirectoryParser",$Parent:TW3DirectoryParser
   ,$Init:function ($) {
      TW3DirectoryParser.$Init($);
   }
   /// function TW3PosixDirectoryParser.ChangeFileExt(const FilePath: String; NewExt: String) : String
   ,ChangeFileExt:function(Self, FilePath$25, NewExt$2) {
      var Result = "";
      var LName$5 = "";
      var Separator$8 = "",
         x$55 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$25.length>0) {
         if ((FilePath$25.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FilePath$25]);
         } else {
            if (StrEndsWith(FilePath$25," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[FilePath$25]);
            } else {
               Separator$8 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(LName$5,Separator$8)) {
                  TW3ErrorObject.SetLastErrorF(Self,"Path (%s) has no filename error",[FilePath$25]);
               } else {
                  if ((FilePath$25.indexOf(Separator$8)+1)>0) {
                     LName$5 = TW3DirectoryParser.GetFileName$(Self,FilePath$25);
                     if (TW3ErrorObject.GetFailed(Self)) {
                        return Result;
                     }
                  } else {
                     LName$5 = FilePath$25;
                  }
                  if (LName$5.length>0) {
                     if ((LName$5.indexOf(".")+1)>0) {
                        for(x$55=LName$5.length;x$55>=1;x$55--) {
                           if (LName$5.charAt(x$55-1)==".") {
                              Result = LName$5.substr(0,(x$55-1))+NewExt$2;
                              break;
                           }
                        }
                     } else {
                        Result = LName$5+NewExt$2;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty filename (%s) error",[FilePath$25]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.ExcludeLeadingPathDelimiter(const FilePath: String) : String
   ,ExcludeLeadingPathDelimiter:function(Self, FilePath$26) {
      var Result = "";
      var Separator$9 = "";
      Separator$9 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$26,Separator$9)) {
         Result = FilePath$26.substr((1+Separator$9.length)-1);
      } else {
         Result = FilePath$26;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.ExcludeTrailingPathDelimiter(const FilePath: String) : String
   ,ExcludeTrailingPathDelimiter:function(Self, FilePath$27) {
      var Result = "";
      var Separator$10 = "";
      Separator$10 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrEndsWith(FilePath$27,Separator$10)) {
         Result = FilePath$27.substr(0,(FilePath$27.length-Separator$10.length));
      } else {
         Result = FilePath$27;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetDevice(const FilePath: String) : String
   ,GetDevice:function(Self, FilePath$28) {
      var Result = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$28.length>0) {
         if (StrBeginsWith(FilePath$28,TW3DirectoryParser.GetRootMoniker$(Self))) {
            Result = TW3DirectoryParser.GetRootMoniker$(Self);
         } else {
            Result = "";
         }
      } else {
         TW3ErrorObject.SetLastError$(Self,"Failed to extract device, path was empty error");
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetDirectoryName(const FilePath: String) : String
   ,GetDirectoryName:function(Self, FilePath$29) {
      var Result = "";
      var Separator$11 = "",
         NameParts$1 = [];
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$29.length>0) {
         if ((FilePath$29.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FilePath$29]);
         } else {
            if (StrEndsWith(FilePath$29," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[FilePath$29]);
            } else {
               Separator$11 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(FilePath$29,Separator$11)) {
                  Result = FilePath$29;
                  return Result;
               }
               NameParts$1 = (FilePath$29).split(Separator$11);
               NameParts$1.splice((NameParts$1.length-1),1)
               ;
               Result = (NameParts$1).join(Separator$11)+Separator$11;
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty filename (%s) error",[FilePath$29]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetExtension(const Filename: String) : String
   ,GetExtension:function(Self, Filename$9) {
      var Result = "";
      var LName$6 = "";
      var Separator$12 = "",
         x$56 = 0;
      var dx$1 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (Filename$9.length>0) {
         if ((Filename$9.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[Filename$9]);
         } else {
            if (StrEndsWith(Filename$9," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[Filename$9]);
            } else {
               Separator$12 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(LName$6,Separator$12)) {
                  TW3ErrorObject.SetLastErrorF(Self,"Path (%s) has no filename error",[Filename$9]);
               } else {
                  if ((Filename$9.indexOf(Separator$12)+1)>0) {
                     LName$6 = TW3DirectoryParser.GetFileName$(Self,Filename$9);
                  } else {
                     LName$6 = Filename$9;
                  }
                  if (!TW3ErrorObject.GetFailed(Self)) {
                     for(x$56=Filename$9.length;x$56>=1;x$56--) {
                        {var $temp89 = Filename$9.charAt(x$56-1);
                           if ($temp89==".") {
                              dx$1 = Filename$9.length;
                              (dx$1-= x$56);
                              ++dx$1;
                              Result = RightStr(Filename$9,dx$1);
                              break;
                           }
                            else if ($temp89==Separator$12) {
                              break;
                           }
                        }
                     }
                     if (Result.length<1) {
                        TW3ErrorObject.SetLastErrorF(Self,"Failed to extract extension, filename (%s) contained no postfix",[TW3DirectoryParser.GetFileName$(Self,Filename$9)]);
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty filename (%s) error",[Filename$9]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetFileName(const FilePath: String) : String
   ,GetFileName:function(Self, FilePath$30) {
      var Result = "";
      var Separator$13 = "",
         x$57 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$30.length>0) {
         if ((FilePath$30.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$30]);
         } else {
            if (StrEndsWith(FilePath$30," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in path (\"%s\") error",[FilePath$30]);
            } else {
               Separator$13 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (!StrEndsWith(FilePath$30,Separator$13)) {
                  for(x$57=FilePath$30.length;x$57>=1;x$57--) {
                     if (FilePath$30.charAt(x$57-1)!=Separator$13) {
                        Result = FilePath$30.charAt(x$57-1)+Result;
                     } else {
                        break;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty path (%s) error",[FilePath$30]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetFileNameWithoutExtension(const Filename: String) : String
   ,GetFileNameWithoutExtension:function(Self, Filename$10) {
      var Result = "";
      var Separator$14 = "",
         LName$7 = "";
      var x$58 = 0;
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (Filename$10.length>0) {
         if ((Filename$10.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[Filename$10]);
         } else {
            if (StrEndsWith(Filename$10," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in filename (\"%s\") error",[Filename$10]);
            } else {
               Separator$14 = TW3DirectoryParser.GetPathSeparator$(Self);
               if ((Filename$10.indexOf(Separator$14)+1)>0) {
                  LName$7 = TW3DirectoryParser.GetFileName$(Self,Filename$10);
               } else {
                  LName$7 = Filename$10;
               }
               if (!TW3ErrorObject.GetFailed(Self)) {
                  if (LName$7.length>0) {
                     if ((LName$7.indexOf(".")+1)>0) {
                        for(x$58=LName$7.length;x$58>=1;x$58--) {
                           if (LName$7.charAt(x$58-1)==".") {
                              Result = LName$7.substr(0,(x$58-1));
                              break;
                           }
                        }
                     } else {
                        Result = LName$7;
                     }
                  }
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty filename (%s) error",[Filename$10]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetPathName(const FilePath: String) : String
   ,GetPathName:function(Self, FilePath$31) {
      var Result = "";
      var LParts = [],
         LTemp$17 = "";
      var Separator$15 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FilePath$31.length>0) {
         if ((FilePath$31.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$31]);
         } else {
            if (StrEndsWith(FilePath$31," ")) {
               TW3ErrorObject.SetLastErrorF(Self,"Invalid trailing character (\" \") in path (\"%s\") error",[FilePath$31]);
            } else {
               Separator$15 = TW3DirectoryParser.GetPathSeparator$(Self);
               if (StrEndsWith(FilePath$31,Separator$15)) {
                  if (FilePath$31==TW3DirectoryParser.GetRootMoniker$(Self)) {
                     TW3ErrorObject.SetLastError$(Self,"Failed to get directory name, path is root");
                     return Result;
                  }
                  LTemp$17 = (FilePath$31).substr(0,(FilePath$31.length-Separator$15.length));
                  LParts = (LTemp$17).split(Separator$15);
                  Result = LParts[(LParts.length-1)];
                  return Result;
               }
               LParts = (FilePath$31).split(Separator$15);
               if (LParts.length>1) {
                  Result = LParts[(LParts.length-1)-1];
               } else {
                  Result = LParts[(LParts.length-1)];
               }
            }
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid or empty path (%s) error",[FilePath$31]);
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.GetPathSeparator() : Char
   ,GetPathSeparator:function(Self) {
      return "\/";
   }
   /// function TW3PosixDirectoryParser.GetRootMoniker() : String
   ,GetRootMoniker:function(Self) {
      return "\/";
   }
   /// function TW3PosixDirectoryParser.HasValidFileNameChars(FileName: String) : Boolean
   ,HasValidFileNameChars:function(Self, FileName$2) {
      var Result = false;
      var el$3 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (FileName$2.length>0) {
         if ((FileName$2.charAt(0)==" ")) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in filename (\"%s\") error",[FileName$2]);
         } else {
            for (var $temp90=0;$temp90<FileName$2.length;$temp90++) {
               el$3=$uniCharAt(FileName$2,$temp90);
               if (!el$3) continue;
               Result = (((el$3>="A")&&(el$3<="Z"))||((el$3>="a")&&(el$3<="z"))||((el$3>="0")&&(el$3<="9"))||(el$3=="-")||(el$3=="_")||(el$3==".")||(el$3==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \"%s\" in filename \"%s\" error",[el$3, FileName$2]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.HasValidPathChars(FolderName: String) : Boolean
   ,HasValidPathChars:function(Self, FolderName$1) {
      var Result = false;
      var el$4 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if ((FolderName$1.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in directory-name (\"%s\") error",[FolderName$1]);
      } else {
         if (FolderName$1.length>0) {
            for (var $temp91=0;$temp91<FolderName$1.length;$temp91++) {
               el$4=$uniCharAt(FolderName$1,$temp91);
               if (!el$4) continue;
               Result = (((el$4>="A")&&(el$4<="Z"))||((el$4>="a")&&(el$4<="z"))||((el$4>="0")&&(el$4<="9"))||(el$4=="-")||(el$4=="_")||(el$4==".")||(el$4==" "));
               if (!Result) {
                  TW3ErrorObject.SetLastErrorF(Self,"Unexpected character \"%s\" in foldername \"%s\" error",[el$4, FolderName$1]);
                  break;
               }
            }
         }
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.IncludeLeadingPathDelimiter(const FilePath: String) : String
   ,IncludeLeadingPathDelimiter:function(Self, FilePath$32) {
      var Result = "";
      var Separator$16 = "";
      Separator$16 = TW3DirectoryParser.GetPathSeparator$(Self);
      if (StrBeginsWith(FilePath$32,Separator$16)) {
         Result = FilePath$32;
      } else {
         Result = Separator$16+FilePath$32;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.IncludeTrailingPathDelimiter(const FilePath: String) : String
   ,IncludeTrailingPathDelimiter:function(Self, FilePath$33) {
      var Result = "";
      var Separator$17 = "";
      Separator$17 = TW3DirectoryParser.GetPathSeparator$(Self);
      Result = FilePath$33;
      if (!StrEndsWith(Result,Separator$17)) {
         Result+=Separator$17;
      }
      return Result
   }
   /// function TW3PosixDirectoryParser.IsValidPath(FilePath: String) : Boolean
   ,IsValidPath:function(Self, FilePath$34) {
      var Result = false;
      var Separator$18 = "",
         PathParts$1 = [],
         Index$9 = 0,
         a$494 = 0;
      var part$1 = "";
      if (TW3ErrorObject.GetFailed(Self)) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if ((FilePath$34.charAt(0)==" ")) {
         TW3ErrorObject.SetLastErrorF(Self,"Invalid leading character (\" \") in path (\"%s\") error",[FilePath$34]);
      } else {
         if (FilePath$34.length>0) {
            Separator$18 = TW3DirectoryParser.GetPathSeparator$(Self);
            PathParts$1 = (FilePath$34).split(Separator$18);
            Index$9 = 0;
            var $temp92;
            for(a$494=0,$temp92=PathParts$1.length;a$494<$temp92;a$494++) {
               part$1 = PathParts$1[a$494];
               {var $temp93 = part$1;
                  if ($temp93=="") {
                     TW3ErrorObject.SetLastErrorF(Self,"Path has multiple separators (%s) error",[FilePath$34]);
                     return false;
                  }
                   else if ($temp93=="~") {
                     if (Index$9>0) {
                        TW3ErrorObject.SetLastErrorF(Self,"Path has misplaced root moniker (%s) error",[FilePath$34]);
                        return Result;
                     }
                  }
                   else {
                     if (Index$9==(PathParts$1.length-1)) {
                        if (!TW3DirectoryParser.HasValidFileNameChars$(Self,part$1)) {
                           return Result;
                        }
                     } else if (!TW3DirectoryParser.HasValidPathChars$(Self,part$1)) {
                        return Result;
                     }
                  }
               }
               Index$9+=1;
            }
            Result = true;
         }
      }
      return Result
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$3:TW3ErrorObject.Create$3
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,ChangeFileExt$:function($){return $.ClassType.ChangeFileExt.apply($.ClassType, arguments)}
   ,ExcludeLeadingPathDelimiter$:function($){return $.ClassType.ExcludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,ExcludeTrailingPathDelimiter$:function($){return $.ClassType.ExcludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,GetDevice$:function($){return $.ClassType.GetDevice.apply($.ClassType, arguments)}
   ,GetDirectoryName$:function($){return $.ClassType.GetDirectoryName.apply($.ClassType, arguments)}
   ,GetExtension$:function($){return $.ClassType.GetExtension.apply($.ClassType, arguments)}
   ,GetFileName$:function($){return $.ClassType.GetFileName.apply($.ClassType, arguments)}
   ,GetFileNameWithoutExtension$:function($){return $.ClassType.GetFileNameWithoutExtension.apply($.ClassType, arguments)}
   ,GetPathName$:function($){return $.ClassType.GetPathName.apply($.ClassType, arguments)}
   ,GetPathSeparator$:function($){return $.ClassType.GetPathSeparator($)}
   ,GetRootMoniker$:function($){return $.ClassType.GetRootMoniker($)}
   ,HasValidFileNameChars$:function($){return $.ClassType.HasValidFileNameChars.apply($.ClassType, arguments)}
   ,HasValidPathChars$:function($){return $.ClassType.HasValidPathChars.apply($.ClassType, arguments)}
   ,IncludeLeadingPathDelimiter$:function($){return $.ClassType.IncludeLeadingPathDelimiter.apply($.ClassType, arguments)}
   ,IncludeTrailingPathDelimiter$:function($){return $.ClassType.IncludeTrailingPathDelimiter.apply($.ClassType, arguments)}
   ,IsPathRooted:TW3DirectoryParser.IsPathRooted
   ,IsRelativePath:TW3DirectoryParser.IsRelativePath
   ,IsValidPath$:function($){return $.ClassType.IsValidPath.apply($.ClassType, arguments)}
};
TW3PosixDirectoryParser.$Intf={
   IW3DirectoryParser:[TW3PosixDirectoryParser.GetPathSeparator,TW3PosixDirectoryParser.GetRootMoniker,TW3DirectoryParser.GetErrorObject,TW3PosixDirectoryParser.IsValidPath,TW3PosixDirectoryParser.HasValidPathChars,TW3PosixDirectoryParser.HasValidFileNameChars,TW3DirectoryParser.IsRelativePath,TW3DirectoryParser.IsPathRooted,TW3PosixDirectoryParser.GetFileNameWithoutExtension,TW3PosixDirectoryParser.GetPathName,TW3PosixDirectoryParser.GetDevice,TW3PosixDirectoryParser.GetFileName,TW3PosixDirectoryParser.GetExtension,TW3PosixDirectoryParser.GetDirectoryName,TW3PosixDirectoryParser.IncludeTrailingPathDelimiter,TW3PosixDirectoryParser.IncludeLeadingPathDelimiter,TW3PosixDirectoryParser.ExcludeLeadingPathDelimiter,TW3PosixDirectoryParser.ExcludeTrailingPathDelimiter,TW3PosixDirectoryParser.ChangeFileExt]
   ,IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TW3NodeSyncFileAPI = class (TObject)
var TW3NodeSyncFileAPI = {
   $ClassName:"TW3NodeSyncFileAPI",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TW3NodeSyncFileAPI.CreateDirectory(const FullPath: String; Permissions: TW3FilePermissionMask) : Boolean
   ,CreateDirectory:function(FullPath, Permissions) {
      var Result = false;
      NodeFsAPI().mkdirSync(FullPath,Permissions.toString());
      Result = true;
      return Result
   }
   /// function TW3NodeSyncFileAPI.DeleteDirectory(const FullPath: String) : Boolean
   ,DeleteDirectory:function(FullPath$1) {
      var Result = false;
      NodeFsAPI().rmdirSync(FullPath$1);
      Result = true;
      return Result
   }
   /// function TW3NodeSyncFileAPI.DeleteFile(const FullPath: String) : Boolean
   ,DeleteFile:function(FullPath$2) {
      var Result = false;
      NodeFsAPI().unlinkSync(FullPath$2);
      Result = true;
      return Result
   }
   /// function TW3NodeSyncFileAPI.DirectoryExists(const FullPath: String) : Boolean
   ,DirectoryExists:function(FullPath$3) {
      var Result = false;
      var stats = null;
      if (NodeFsAPI().existsSync(FullPath$3)) {
         stats = NodeFsAPI().lstatSync(FullPath$3);
         Result = stats.isDirectory();
      }
      return Result
   }
   /// function TW3NodeSyncFileAPI.Examine(const FullPath: String; var Files: TStrArray) : Boolean
   ,Examine:function(FullPath$4, Files) {
      var Result = false;
      Files.v = NodeFsAPI().readdirSync(FullPath$4);
      Result = true;
      return Result
   }
   /// function TW3NodeSyncFileAPI.FileExists(const Filename: String) : Boolean
   ,FileExists:function(Filename$11) {
      var Result = false;
      var stats$1 = null;
      if (NodeFsAPI().existsSync(Filename$11)) {
         stats$1 = NodeFsAPI().lstatSync(Filename$11);
         Result = stats$1.isFile();
      }
      return Result
   }
   /// function TW3NodeSyncFileAPI.GetFilesize(const Filename: String; var Size: Int64) : Boolean
   ,GetFilesize:function(Filename$12, Size$16) {
      var Result = false;
      var LInfo$1 = null;
      LInfo$1 = NodeFsAPI().statSync(Filename$12);
      Result = LInfo$1.isFile();
      if (Result) {
         Size$16.v = LInfo$1.size;
      }
      return Result
   }
   /// function TW3NodeSyncFileAPI.GetPath() : String
   ,GetPath:function(Self) {
      var Result = "";
      var Access$26 = null;
      Access$26 = process;
      Result = Access$26.cwd();
      return Result
   }
   /// function TW3NodeSyncFileAPI.QueryFilePermissions(const FullPath: String; var Permissions: TW3FilePermissionMask) : Boolean
   ,QueryFilePermissions:function(FullPath$5, Permissions$1) {
      var Result = false;
      var stats$2 = null;
      Permissions$1.v = 0;
      stats$2 = NodeFsAPI().lstatSync(FullPath$5);
      Permissions$1.v = stats$2.mode;
      Result = true;
      return Result
   }
   /// function TW3NodeSyncFileAPI.ReadFile(const FullPath: String) : TByteArray
   ,ReadFile:function(FullPath$6) {
      var Result = [];
      var NodeBuffer$1 = null,
         LTemp$18 = null;
      NodeBuffer$1 = NodeFsAPI().readFileSync(FullPath$6);
      if (NodeBuffer$1!==null) {
         LTemp$18 = TDataTypeConverter.Create$15$($New(TBinaryData));
         try {
            TBinaryData.FromNodeBuffer(LTemp$18,NodeBuffer$1);
            Result = TBinaryData.ToBytes(LTemp$18);
         } finally {
            TObject.Free(LTemp$18);
            NodeBuffer$1 = null;
         }
      }
      return Result
   }
   /// function TW3NodeSyncFileAPI.ReadTextFile(const FullPath: String) : String
   ,ReadTextFile:function(FullPath$7) {
      var Result = "";
      var Options$7 = {encoding:"",flag:""};
      Options$7.encoding = "utf8";
      Options$7.flag = "r";
      Result = String(NodeFsAPI().readFileSync(FullPath$7,Clone$TReadFileSyncOptions(Options$7)));
      return Result
   }
   /// function TW3NodeSyncFileAPI.Rename(const OldPath: String; const NewPath: String) : Boolean
   ,Rename:function(OldPath, NewPath) {
      var Result = false;
      NodeFsAPI().renameSync(OldPath,NewPath);
      Result = true;
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// TW3EnvVariables = class (TObject)
var TW3EnvVariables = {
   $ClassName:"TW3EnvVariables",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TW3EnvVariables.Delete(Name: String) : Boolean
   ,Delete$1:function(Name$40) {
      var Result = false;
      var LRef$6;
      Name$40 = Trim$_String_(Name$40);
      if (Name$40.length>0) {
         if (TW3EnvVariables.Exists(Name$40)) {
            try {
               LRef$6 = process.env;
               delete (LRef$6)[Name$40];
            } catch ($e) {
               var e$39 = $W($e);
               throw EW3Exception.CreateFmt$($New(EW3EnvVariables),"%s failed,system threw exception: %s",["TW3EnvVariables.Delete", e$39.FMessage]);
            }
            Result = ((!process.env.hasOwnProperty(Name$40))?true:false);
         } else {
            throw EW3Exception.CreateFmt$($New(EW3EnvVariables),"%s failed, variable %s does not exist",["TW3EnvVariables.Delete", Name$40]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EW3EnvVariables),"%s failed, variable name was empty error",["TW3EnvVariables.Delete"]);
      }
      return Result
   }
   /// function TW3EnvVariables.Exists(Name: String) : Boolean
   ,Exists:function(Name$41) {
      var Result = false;
      Name$41 = Trim$_String_(Name$41);
      if (Name$41.length>0) {
         Result = (process.env.hasOwnProperty(Name$41)?true:false);
      } else {
         throw EW3Exception.CreateFmt$($New(EW3EnvVariables),"%s failed, variable name was empty error",["TW3EnvVariables.Exists"]);
      }
      return Result
   }
   /// function TW3EnvVariables.Read(Name: String) : String
   ,Read:function(Name$42) {
      var Result = "";
      Name$42 = Trim$_String_(Name$42);
      if (Name$42.length>0) {
         if (TW3EnvVariables.Exists(Name$42)) {
            Result = String(process.env[Name$42]);
         } else {
            throw EW3Exception.CreateFmt$($New(EW3EnvVariables),"%s failed, variable %s does not exist error",["TW3EnvVariables.Read", Name$42]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EW3EnvVariables),"%s failed, variable name was empty error",["TW3EnvVariables.Read"]);
      }
      return Result
   }
   /// procedure TW3EnvVariables.Write(Name: String; const Value: String)
   ,Write:function(Name$43, Value$164) {
      Name$43 = Trim$_String_(Name$43);
      if (Name$43.length>0) {
         try {
            process.env[Name$43] = Value$164;
         } catch ($e) {
            var e$40 = $W($e);
            throw EW3Exception.CreateFmt$($New(EW3EnvVariables),"%s failed,system threw exception: %s",["TW3EnvVariables.Write", e$40.FMessage]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EW3EnvVariables),"%s failed, variable name was empty error",["TW3EnvVariables.Write"]);
      }
   }
   ,Destroy:TObject.Destroy
};
/// TRunPlatform enumeration
var TRunPlatform = [ "rpUnknown", "rpWindows", "rpLinux", "rpMac", "rpEspruino" ];
function SetCurrentDir(Dir) {
   var Result = false;
   process.chdir(Dir);
   Result = true;
   return Result
};
function RmDir(DirectoryName) {
   try {
      NodeFsAPI().rmdirSync(DirectoryName);
   } catch ($e) {
      var e$41 = $W($e);
      throw EW3Exception.CreateFmt$($New(EW3FileOperationError),"%s failed, system threw exception %s with message: %s",["RmDir", TObject.ClassName(e$41.ClassType), e$41.FMessage]);
   }
};
function RemoveDir$1(Dir$1) {
   var Result = false;
   try {
      NodeFsAPI().rmdirSync(Dir$1);
   } catch ($e) {
      var e$42 = $W($e);
      throw EW3Exception.CreateFmt$($New(EW3FileOperationError),"%s failed, system threw exception %s with message: %s",["RemoveDir", TObject.ClassName(e$42.ClassType), e$42.FMessage]);
   }
   Result = true;
   return Result
};
function ReadLn() {
   var Result = "";
   Result = "";
   try {
      process.stdin.setRawMode(true);
		process.stdin.resume();
		process.stdin.on('data', process.exit.bind(process, 0));
   } catch ($e) {
      var e$43 = $W($e);
      /* null */
   }
   return Result
};
function PathSeparator() {
   return NodePathAPI().sep;
};
function PathIsAbsolute(Path$2) {
   var Result = false;
   try {
      Result = NodePathAPI().isAbsolute(Path$2);
   } catch ($e) {
      var e$44 = $W($e);
      throw EW3Exception.CreateFmt$($New(EW3FileOperationError),"%s failed, system threw exception %s with message: %s",["PathIsAbsolute", TObject.ClassName(e$44.ClassType), e$44.FMessage]);
   }
   return Result
};
function ParamStr$1(index$10) {
   var Result = "";
   Result = process.argv[index$10];
   return Result
};
function ParamCount$1() {
   var Result = 0;
   Result = process.argv.length;
   return Result
};
function NormalizePath(Path$3) {
   return NodePathAPI().normalize(Path$3);
};
function MkDir(DirectoryName$1) {
   var LAccessMode = "";
   LAccessMode = "666";
   try {
      NodeFsAPI().mkdirSync(DirectoryName$1,LAccessMode);
   } catch ($e) {
      var e$45 = $W($e);
      throw EW3Exception.CreateFmt$($New(EW3FileOperationError),"%s failed, system threw exception %s with message: %s",["MkDir", TObject.ClassName(e$45.ClassType), e$45.FMessage]);
   }
};
function IncludeTrailingPathDelimiter$4(PathName) {
   var Result = "";
   Result = Trim$_String_(PathName);
   if (!StrEndsWith(Result,PathSeparator())) {
      Result+=PathSeparator();
   }
   return Result
};
function GetPlatform() {
   var Result = 0;
   var token = "";
   token = process.platform;
   token = (Trim$_String_(token)).toLocaleLowerCase();
   {var $temp94 = token;
      if ($temp94=="darwin") {
         Result = 3;
      }
       else if ($temp94=="win32") {
         Result = 1;
      }
       else if ($temp94=="linux") {
         Result = 2;
      }
       else if ($temp94=="espruino") {
         Result = 4;
      }
       else {
         Result = 0;
      }
   }
   return Result
};
function GetEnvironmentVariable(Name$44) {
   var Result = "";
   if (process.env.hasOwnProperty(Name$44)) {
      Result = String(process.env[Name$44]);
   }
   return Result
};
function GetDir() {
   return GetCurrentDir();
};
function GetCurrentDir() {
   return process.cwd();
};
function ForceDirectories(Path$4) {
   var Result = false;
   var LDirs = [],
      Seperator = "",
      LDisk = "",
      temp$22 = "",
      a$495 = 0;
   var xp = "";
   Path$4 = Trim$_String_(Path$4);
   if (Path$4.length>0) {
      Seperator = PathSeparator();
      if (!TPath.IsPathRooted$1(Path$4)) {
         Path$4 = IncludeTrailingPathDelimiter$4(GetCurrentDir())+ExcludeLeadingPathDelimiter$4(Path$4);
      }
      Path$4 = NormalizePath(Path$4);
      if (DirectoryExists$1(Path$4)) {
         Result = true;
      } else {
         LDisk = TPath.GetDevice$3(Path$4);
         temp$22 = RightStr(Path$4,Path$4.length-LDisk.length);
         if (StrEndsWith(temp$22,Seperator)) {
            temp$22 = (temp$22).substr(0,(temp$22.length-1));
         }
         LDirs = (temp$22).split(Seperator);
         if (DirectoryExists$1(LDisk)) {
            var $temp95;
            for(a$495=0,$temp95=LDirs.length;a$495<$temp95;a$495++) {
               xp = LDirs[a$495];
               LDisk+=xp;
               if (!DirectoryExists$1(LDisk)) {
                  try {
                     MkDir(LDisk);
                  } catch ($e) {
                     throw EW3Exception.CreateFmt$($New(EW3FileOperationError),"%s failed, unable to create folder [%s] error",["ForceDirectories", LDisk]);
                  }
               }
               LDisk+=Seperator;
            }
            Result = true;
         } else {
            throw EW3Exception.CreateFmt$($New(EW3FileOperationError),"%s failed, initial root path [%s] does not exist error",["ForceDirectories", LDisk]);
         }
      }
   }
   return Result
};
function FileExists$2(Filename$13) {
   var Result = false;
   var stats$3 = null;
   if (NodeFsAPI().existsSync(Filename$13)) {
      try {
         stats$3 = NodeFsAPI().lstatSync(Filename$13);
         Result = stats$3.isFile();
      } catch ($e) {
         var e$46 = $W($e);
         throw EW3Exception.CreateFmt$($New(EW3FileOperationError),"%s failed, system threw exception %s with message: %s",["FileExists", TObject.ClassName(e$46.ClassType), e$46.FMessage]);
      }
   }
   return Result
};
function ExtractFilePath(Filename$14) {
   return NodePathAPI().dirname(Filename$14);
};
function ExtractFileName(Filename$15) {
   return NodePathAPI().basename(Filename$15);
};
function ExtractFileExt(Filename$16) {
   return NodePathAPI().extname(Filename$16);
};
function ExcludeLeadingPathDelimiter$4(PathName$1) {
   var Result = "";
   if (StrBeginsWith(PathName$1,PathSeparator())) {
      Result = RightStr(PathName$1,PathName$1.length-1);
   } else {
      Result = PathName$1;
   }
   return Result
};
/// EW3FileOperationError = class (EW3Exception)
var EW3FileOperationError = {
   $ClassName:"EW3FileOperationError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// EW3EnvVariables = class (EW3Exception)
var EW3EnvVariables = {
   $ClassName:"EW3EnvVariables",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
function DirectoryExists$1(Dir$2) {
   var Result = false;
   var stats$4 = null;
   if (NodeFsAPI().existsSync(Dir$2)) {
      try {
         stats$4 = NodeFsAPI().lstatSync(Dir$2);
         Result = stats$4.isDirectory();
      } catch ($e) {
         var e$47 = $W($e);
         throw EW3Exception.CreateFmt$($New(EW3FileOperationError),"%s failed, system threw exception %s with message: %s",["DirectoryExists", TObject.ClassName(e$47.ClassType), e$47.FMessage]);
      }
   }
   return Result
};
function CreateDir(Dir$3) {
   var Result = {v:false};
   try {
      var LAccessMode$1 = "";
      LAccessMode$1 = "666";
      try {
         NodeFsAPI().mkdirSync(Dir$3,LAccessMode$1);
      } catch ($e) {
         var e$48 = $W($e);
         return Result.v;
      }
      Result.v = true;
   } finally {return Result.v}
};
function ChDir$1(Directory) {
   SetCurrentDir(Directory);
};
/// TFileStream = class (TCustomFileStream)
var TFileStream = {
   $ClassName:"TFileStream",$Parent:TCustomFileStream
   ,$Init:function ($) {
      TCustomFileStream.$Init($);
      $.FHandle$3 = undefined;
      $.FPosition = 0;
   }
   /// function TFileStream.CopyFrom(const Source: TStream; Count: Integer) : Integer
   ,CopyFrom:function(Self, Source$8, Count$24) {
      var Result = 0;
      var DataRead$2 = [];
      if (Self.FHandle$3) {
         if (Source$8!==null) {
            if (Count$24>0) {
               DataRead$2 = TStream.ReadBuffer$(Source$8,TStream.GetPosition$(Source$8),Count$24);
               TStream.WriteBuffer$(Self,DataRead$2,Self.FPosition);
               Result = DataRead$2.length;
            }
         } else {
            throw Exception.Create($New(EStream),"Invalid source stream error");
         }
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
      return Result
   }
   /// constructor TFileStream.Create(const FullFilePath: String; const AccessMode: TFileAccessMode)
   ,Create$32:function(Self, FullFilePath, AccessMode$2) {
      var Filesystem,
         OpenFlags = "";
      switch (AccessMode$2) {
         case 0 :
            OpenFlags = "rs";
            break;
         case 1 :
            OpenFlags = "w";
            break;
         case 2 :
            OpenFlags = "w+";
            break;
      }
      Filesystem = NodeFsAPI();
      try {
         Self.FHandle$3 = Filesystem.openSync(FullFilePath,OpenFlags);
      } catch ($e) {
         var e$49 = $W($e);
         throw $e;
      }
      TCustomFileStream.SetAccessMode$(Self,AccessMode$2);
      TCustomFileStream.SetFilename$(Self,FullFilePath);
      return Self
   }
   /// destructor TFileStream.Destroy()
   ,Destroy:function(Self) {
      var FileSystem$3 = null;
      if (Self.FHandle$3) {
         FileSystem$3 = NodeFsAPI();
         try {
            FileSystem$3.closeSync(Self.FHandle$3);
         } finally {
            Self.FHandle$3 = undefined;
         }
      }
      TDataTypeConverter.Destroy(Self);
   }
   /// function TFileStream.GetPosition() : Integer
   ,GetPosition:function(Self) {
      var Result = 0;
      if (Self.FHandle$3) {
         Result = Self.FPosition;
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
      return Result
   }
   /// function TFileStream.GetSize() : Integer
   ,GetSize:function(Self) {
      var Result = 0;
      if (Self.FHandle$3) {
         Result = NodeFsAPI().statSync(Self.FFilename).size;
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
      return Result
   }
   /// function TFileStream.ReadBuffer(Offset: Integer; Count: Integer) : TByteArray
   ,ReadBuffer:function(Self, Offset$44, Count$25) {
      var Result = [];
      var LTypedChunk = null;
      var FileSystem$4,
         LCache$3,
         LChunk;
      if (Self.FHandle$3) {
         if (Count$25>0) {
            FileSystem$4 = NodeFsAPI();
            LCache$3 = new Buffer(Count$25);
            try {
               LChunk = FileSystem$4.readSync(Self.FHandle$3,LCache$3,0,Count$25,Self.FPosition);
            } catch ($e) {
               var e$50 = $W($e);
               throw EW3Exception.CreateFmt$($New(EStreamReadError),$R[8],[e$50.FMessage]);
            }
            LTypedChunk = new Uint8Array(LCache$3);
            Result = TDatatype.TypedArrayToBytes$1(TDatatype,LTypedChunk);
            TStream.SetPosition$(Self,Offset$44+Result.length);
         }
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
      return Result
   }
   /// function TFileStream.Seek(const Offset: Integer; Origin: TStreamSeekOrigin) : Integer
   ,Seek:function(Self, Offset$45, Origin$2) {
      var Result = 0;
      var LSize$13 = 0;
      if (Self.FHandle$3) {
         LSize$13 = TStream.GetSize$(Self);
         if (LSize$13>0) {
            switch (Origin$2) {
               case 0 :
                  if (Offset$45>-1) {
                     TStream.SetPosition$(Self,Offset$45);
                     Result = Offset$45;
                  }
                  break;
               case 1 :
                  Result = TInteger.EnsureRange((TStream.GetPosition$(Self)+Offset$45),0,LSize$13);
                  TStream.SetPosition$(Self,Result);
                  break;
               case 2 :
                  Result = TInteger.EnsureRange((TStream.GetSize$(Self)-Math.abs(Offset$45)),0,LSize$13);
                  TStream.SetPosition$(Self,Result);
                  break;
            }
         }
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
      return Result
   }
   /// procedure TFileStream.SetPosition(NewPosition: Integer)
   ,SetPosition:function(Self, NewPosition$3) {
      var LSize$14 = 0;
      if (Self.FHandle$3) {
         LSize$14 = TStream.GetSize$(Self);
         if (LSize$14>0) {
            Self.FPosition = TInteger.EnsureRange(NewPosition$3,0,LSize$14);
         }
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
   }
   /// procedure TFileStream.SetSize(NewSize: Integer)
   ,SetSize:function(Self, NewSize$4) {
      var LFileSystem$1,
         LData$4 = [],
         LSize$15 = 0,
         LDiff$2 = 0;
      if (Self.FHandle$3) {
         LSize$15 = TStream.GetSize$(Self);
         LFileSystem$1 = NodeFsAPI();
         if (NewSize$4<0) {
            NewSize$4 = 0;
         }
         if (NewSize$4>LSize$15) {
            LDiff$2 = NewSize$4-LSize$15;
            $ArraySetLenC(LData$4,NewSize$4,function (){return 0});
            TStream.WriteBuffer$(Self,LData$4,LSize$15);
         } else {
            try {
               LFileSystem$1.ftruncateSync(Self.FHandle$3,NewSize$4);
            } catch ($e) {
               var e$51 = $W($e);
               throw EW3Exception.CreateFmt$($New(EStreamReadError),$R[6],[e$51.FMessage]);
            }
         }
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
   }
   /// function TFileStream.Skip(Amount: Integer) : Integer
   ,Skip:function(Self, Amount$2) {
      var Result = 0;
      var LSize$16 = 0,
         LTotal$3 = 0;
      if (Self.FHandle$3) {
         LSize$16 = TStream.GetSize$(Self);
         if (LSize$16>0) {
            LTotal$3 = TStream.GetPosition$(Self)+Amount$2;
            if (LTotal$3>LSize$16) {
               LTotal$3 = LSize$16-LTotal$3;
            }
            (Self.FPosition+= LTotal$3);
            Result = LTotal$3;
         }
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
      return Result
   }
   /// procedure TFileStream.WriteBuffer(const Buffer: TByteArray; Offset: Integer)
   ,WriteBuffer:function(Self, Buffer$19, Offset$46) {
      var FileSystem$5,
         LChunk$1;
      if (Self.FHandle$3) {
         if (Buffer$19.length>0) {
            FileSystem$5 = NodeFsAPI();
            LChunk$1 = new Buffer( new Uint8Array(Buffer$19).buffer );
            try {
               FileSystem$5.writeSync(Self.FHandle$3,LChunk$1,0,Buffer$19.length,Self.FPosition,"binary");
            } catch ($e) {
               var e$52 = $W($e);
               throw EW3Exception.CreateFmt$($New(EStreamWriteError),$R[7],[e$52.FMessage]);
            }
            TStream.SetPosition$(Self,Offset$46+Buffer$19.length);
         }
      } else {
         throw Exception.Create($New(EStream),$R[9]);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$15:TDataTypeConverter.Create$15
   ,CopyFrom$:function($){return $.ClassType.CopyFrom.apply($.ClassType, arguments)}
   ,GetBOF:TStream.GetBOF
   ,GetEOF:TStream.GetEOF
   ,GetPosition$:function($){return $.ClassType.GetPosition($)}
   ,GetSize$:function($){return $.ClassType.GetSize($)}
   ,Read$1:TStream.Read$1
   ,ReadBuffer$1:TStream.ReadBuffer$1
   ,ReadBuffer$:function($){return $.ClassType.ReadBuffer.apply($.ClassType, arguments)}
   ,Seek$:function($){return $.ClassType.Seek.apply($.ClassType, arguments)}
   ,SetPosition$:function($){return $.ClassType.SetPosition.apply($.ClassType, arguments)}
   ,SetSize$:function($){return $.ClassType.SetSize.apply($.ClassType, arguments)}
   ,Skip$:function($){return $.ClassType.Skip.apply($.ClassType, arguments)}
   ,Write$2:TStream.Write$2
   ,WriteBuffer$:function($){return $.ClassType.WriteBuffer.apply($.ClassType, arguments)}
   ,Create$32$:function($){return $.ClassType.Create$32.apply($.ClassType, arguments)}
   ,SetAccessMode:TCustomFileStream.SetAccessMode
   ,SetFilename:TCustomFileStream.SetFilename
};
TFileStream.$Intf={
   IBinaryTransport:[TStream.DataOffset,TStream.DataGetSize,TStream.DataRead,TStream.DataWrite]
}
function WebSocketCreateServer(Options$8) {
   var Result = undefined;
   var Access = null;
   Access = WebSocketAPI();
   Result = new (Access).Server(Options$8);
   return Result
};
function WebSocketCreateServer$1(Options$9) {
   var Result = undefined;
   var Access$1 = null;
   Access$1 = WebSocketAPI();
   Result = new (Access$1).Server(Options$9);
   return Result
};
function WebSocketAPI() {
   return require("ws");
};
/// JWsVerifyClientInfo = class (TObject)
var JWsVerifyClientInfo = {
   $ClassName:"JWsVerifyClientInfo",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.origin = "";
      $.req = null;
      $.secure = false;
   }
   ,Destroy:TObject.Destroy
};
/// JWsServerOptions = class (TObject)
var JWsServerOptions = {
   $ClassName:"JWsServerOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.backlog = $.maxPayload = $.port$4 = 0;
      $.clientTracking = $.noServer = $.perMessageDeflate = false;
      $.host = $.path = "";
      $.server = undefined;
      $.verifyClient = null;
   }
   ,Destroy:TObject.Destroy
};
/// TCreateServerOptions = record
function Copy$TCreateServerOptions(s,d) {
   d.allowHalfOpen=s.allowHalfOpen;
   return d;
}
function Clone$TCreateServerOptions($) {
   return {
      allowHalfOpen:$.allowHalfOpen
   }
}
/// TCreateConnectionOptions = record
function Copy$TCreateConnectionOptions(s,d) {
   d.allowHalfOpen=s.allowHalfOpen;
   return d;
}
function Clone$TCreateConnectionOptions($) {
   return {
      allowHalfOpen:$.allowHalfOpen
   }
}
/// TConnectOptions = record
function Copy$TConnectOptions(s,d) {
   d.allowHalfOpen=s.allowHalfOpen;
   return d;
}
function Clone$TConnectOptions($) {
   return {
      allowHalfOpen:$.allowHalfOpen
   }
}
function net() {
   return require("net");
};
/// JSocketOptions = record
function Copy$JSocketOptions(s,d) {
   d.fd=s.fd;
   d.type=s.type;
   d.allowHalfOpen=s.allowHalfOpen;
   return d;
}
function Clone$JSocketOptions($) {
   return {
      fd:$.fd,
      type:$.type,
      allowHalfOpen:$.allowHalfOpen
   }
}
/// JServerCredentials = class (TObject)
var JServerCredentials = {
   $ClassName:"JServerCredentials",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.ciphers = $.ca = $.key = $.passphrase = $.cert = $.pfx = "";
      $.crl = undefined;
   }
   ,Destroy:TObject.Destroy
};
/// JServerAddressResult = class (TObject)
var JServerAddressResult = {
   $ClassName:"JServerAddressResult",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.port = 0;
      $.family = $.address = "";
   }
   ,Destroy:TObject.Destroy
};
function http() {
   return require("http");
};
/// TWriteFileOptions = record
function Copy$TWriteFileOptions(s,d) {
   d.encoding=s.encoding;
   d.mode=s.mode;
   d.flag=s.flag;
   return d;
}
function Clone$TWriteFileOptions($) {
   return {
      encoding:$.encoding,
      mode:$.mode,
      flag:$.flag
   }
}
/// TWatchOptions = record
function Copy$TWatchOptions(s,d) {
   d.persistent=s.persistent;
   d.recursive=s.recursive;
   d.encoding$3=s.encoding$3;
   return d;
}
function Clone$TWatchOptions($) {
   return {
      persistent:$.persistent,
      recursive:$.recursive,
      encoding$3:$.encoding$3
   }
}
/// TWatchFileOptions = record
function Copy$TWatchFileOptions(s,d) {
   d.persistent=s.persistent;
   d.interval=s.interval;
   return d;
}
function Clone$TWatchFileOptions($) {
   return {
      persistent:$.persistent,
      interval:$.interval
   }
}
/// TWatchFileListenerObject = record
function Copy$TWatchFileListenerObject(s,d) {
   d.curr=s.curr;
   d.prev=s.prev;
   return d;
}
function Clone$TWatchFileListenerObject($) {
   return {
      curr:$.curr,
      prev:$.prev
   }
}
/// TReadFileSyncOptions = record
function Copy$TReadFileSyncOptions(s,d) {
   d.encoding=s.encoding;
   d.flag=s.flag;
   return d;
}
function Clone$TReadFileSyncOptions($) {
   return {
      encoding:$.encoding,
      flag:$.flag
   }
}
/// TReadFileOptions = record
function Copy$TReadFileOptions(s,d) {
   d.encoding=s.encoding;
   d.flag=s.flag;
   return d;
}
function Clone$TReadFileOptions($) {
   return {
      encoding:$.encoding,
      flag:$.flag
   }
}
/// TCreateWriteStreamOptions = class (TObject)
var TCreateWriteStreamOptions = {
   $ClassName:"TCreateWriteStreamOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.flags = "w";
      $.encoding$6 = "utf8";
      $.fd = null;
      $.mode$1 = 666;
      $.autoClose = true;
   }
   ,Destroy:TObject.Destroy
};
/// TCreateWriteStreamOptionsEx = class (TCreateWriteStreamOptions)
var TCreateWriteStreamOptionsEx = {
   $ClassName:"TCreateWriteStreamOptionsEx",$Parent:TCreateWriteStreamOptions
   ,$Init:function ($) {
      TCreateWriteStreamOptions.$Init($);
      $.start = 0;
   }
   ,Destroy:TObject.Destroy
};
/// TCreateReadStreamOptions = class (TObject)
var TCreateReadStreamOptions = {
   $ClassName:"TCreateReadStreamOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.flags$1 = "r";
      $.encoding$7 = "utf8";
      $.fd$1 = null;
      $.mode$2 = 666;
      $.autoClose$1 = true;
   }
   ,Destroy:TObject.Destroy
};
/// TCreateReadStreamOptionsEx = class (TCreateReadStreamOptions)
var TCreateReadStreamOptionsEx = {
   $ClassName:"TCreateReadStreamOptionsEx",$Parent:TCreateReadStreamOptions
   ,$Init:function ($) {
      TCreateReadStreamOptions.$Init($);
      $.start$1 = $.end$7 = 0;
      $.highWaterMark = 65536;
   }
   ,Destroy:TObject.Destroy
};
/// TAppendFileOptions = record
function Copy$TAppendFileOptions(s,d) {
   d.encoding=s.encoding;
   d.mode=s.mode;
   d.flag=s.flag;
   return d;
}
function Clone$TAppendFileOptions($) {
   return {
      encoding:$.encoding,
      mode:$.mode,
      flag:$.flag
   }
}
function NodeFsAPI() {
   return require("fs");
};
/// JFileAccessConstants = class (TObject)
var JFileAccessConstants = {
   $ClassName:"JFileAccessConstants",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.F_OK = $.O_APPEND = $.O_CREAT = $.O_DIRECT = $.O_DIRECTORY = $.O_DSYNC = $.O_EXCL = $.O_NOATIME = $.O_NOCTTY = $.O_NOFOLLOW = $.O_NONBLOCK = $.O_RDONLY = $.O_RDWR = $.O_SYMLINK = $.O_SYNC = $.O_TRUNC = $.O_WRONLY = $.R_OK = $.S_IFBLK = $.S_IFCHR = $.S_IFDIR = $.S_IFIFO = $.S_IFLNK = $.S_IFMT = $.S_IFREG = $.S_IFSOCK = $.S_IRGRP = $.S_IROTH = $.S_IRUSR = $.S_IRWXG = $.S_IRWXO = $.S_IRWXU = $.S_IWGRP = $.S_IWOTH = $.S_IWUSR = $.S_IXGRP = $.S_IXOTH = $.S_IXUSR = $.W_OK = $.X_OK = 0;
   }
   ,Destroy:TObject.Destroy
};
function NodePathAPI() {
   return require("path");
};
/// JPathParseData = record
function Copy$JPathParseData(s,d) {
   d.root=s.root;
   d.dir$2=s.dir$2;
   d.base=s.base;
   d.ext=s.ext;
   d.name$2=s.name$2;
   return d;
}
function Clone$JPathParseData($) {
   return {
      root:$.root,
      dir$2:$.dir$2,
      base:$.base,
      ext:$.ext,
      name$2:$.name$2
   }
}
/// TMessageFactory = class (TObject)
var TMessageFactory = {
   $ClassName:"TMessageFactory",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FLUT$1 = null;
   }
   /// procedure TMessageFactory.Register(const MessageClass: TQTXMessageBaseClass)
   ,Register:function(Self, MessageClass$1) {
      if (MessageClass$1) {
         if (!TW3CustomDictionary.Contains(Self.FLUT$1,TObject.ClassName(MessageClass$1))) {
            TW3CustomDictionary.SetItem$(Self.FLUT$1,TObject.ClassName(MessageClass$1),MessageClass$1);
         }
      }
   }
   /// function TMessageFactory.Build(MessageClassName: String; var Instance: TQTXBaseMessage) : Boolean
   ,Build:function(Self, MessageClassName, Instance$10) {
      var Result = false;
      var LClass = null;
      var LTemp$19;
      Instance$10.v = null;
      if (MessageClassName.length>0) {
         Result = TW3CustomDictionary.Contains(Self.FLUT$1,MessageClassName);
         if (Result) {
            LTemp$19 = TW3CustomDictionary.GetItem$1$(Self.FLUT$1,MessageClassName);
            LClass = LTemp$19;
            Instance$10.v = TQTXBaseMessage.Create$99$($NewDyn(LClass," in TMessageFactory.Build [line: 76, column: 26, file: ragnarok.messages.factory]"));
         }
      }
      return Result
   }
   /// constructor TMessageFactory.Create()
   ,Create$97:function(Self) {
      TObject.Create(Self);
      Self.FLUT$1 = TW3CustomDictionary.Create$74$($New(TW3VarDictionary));
      TMessageFactory.RegisterIntrinsic$(Self);
      return Self
   }
   /// destructor TMessageFactory.Destroy()
   ,Destroy:function(Self) {
      TObject.Free(Self.FLUT$1);
      TObject.Destroy(Self);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,RegisterIntrinsic$:function($){return $.ClassType.RegisterIntrinsic($)}
   ,Create$97$:function($){return $.ClassType.Create$97($)}
};
/// TRagnarokMessageFactory = class (TMessageFactory)
var TRagnarokMessageFactory = {
   $ClassName:"TRagnarokMessageFactory",$Parent:TMessageFactory
   ,$Init:function ($) {
      TMessageFactory.$Init($);
   }
   /// procedure TRagnarokMessageFactory.RegisterIntrinsic()
   ,RegisterIntrinsic:function(Self) {
      TMessageFactory.Register(Self,TQTXLoginRequest);
      TMessageFactory.Register(Self,TQTXFileIORequest);
   }
   ,Destroy:TMessageFactory.Destroy
   ,RegisterIntrinsic$:function($){return $.ClassType.RegisterIntrinsic($)}
   ,Create$97:TMessageFactory.Create$97
};
/// ERagnarokServer = class (EW3Exception)
var ERagnarokServer = {
   $ClassName:"ERagnarokServer",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TQTXRoute = class (TObject)
var TQTXRoute = {
   $ClassName:"TQTXRoute",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.TagValue = undefined;
      $.Destination = $.Channel = $.Source = "";
      $.FOwner$1 = null;
   }
   /// procedure TQTXRoute.Clear()
   ,Clear$18:function(Self) {
      Self.Source = "";
      Self.Channel = "{5769FAF8-AD26-4941-9D4E-35E1DEDDBC82}";
      Self.Destination = "";
      Self.TagValue = undefined;
   }
   /// constructor TQTXRoute.Create(const Message: TQTXBaseMessage)
   ,Create$102:function(Self, Message$2) {
      TObject.Create(Self);
      Self.FOwner$1 = Message$2;
      return Self
   }
   /// procedure TQTXRoute.ReadObjData(const Root: TQTXJSONObject)
   ,ReadObjData$8:function(Self, Root) {
      Self.Channel = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$5(Root,"channel"));
      Self.Source = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$5(Root,"source"));
      Self.Destination = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$5(Root,"destination"));
      if (TQTXJSONObject.Locate(Root,"tagvalue",false)!==null) {
         Self.TagValue = TQTXJSONObject.ReadVariant$1(Root,"tagvalue");
      }
   }
   /// procedure TQTXRoute.WriteObjData(const Root: TQTXJSONObject)
   ,WriteObjData$8:function(Self, Root$1) {
      TQTXJSONObject.WriteOrAdd(Root$1,"channel",TString.EncodeBase64(TString,Self.Channel));
      TQTXJSONObject.WriteOrAdd(Root$1,"source",TString.EncodeBase64(TString,Self.Source));
      TQTXJSONObject.WriteOrAdd(Root$1,"destination",TString.EncodeBase64(TString,Self.Destination));
      TQTXJSONObject.WriteVariant$1(Root$1,"tagvalue",Self.TagValue);
   }
   ,Destroy:TObject.Destroy
   ,ReadObjData$8$:function($){return $.ClassType.ReadObjData$8.apply($.ClassType, arguments)}
   ,WriteObjData$8$:function($){return $.ClassType.WriteObjData$8.apply($.ClassType, arguments)}
};
/// TQTXBaseMessage = class (TObject)
var TQTXBaseMessage = {
   $ClassName:"TQTXBaseMessage",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.Ticket = "";
      $.SendTime = undefined;
      $.FBinary = $.FEnvelope = $.FRouting = null;
   }
   /// procedure TQTXBaseMessage.Clear()
   ,Clear$17:function(Self) {
      TAllocation.Release$1(Self.FBinary);
      TQTXJSONObject.Clear$19(Self.FEnvelope);
      TQTXRoute.Clear$18(Self.FRouting);
   }
   /// constructor TQTXBaseMessage.Create(const WithTicket: String)
   ,Create$100:function(Self, WithTicket) {
      TQTXBaseMessage.Create$99$(Self);
      Self.Ticket = WithTicket;
      return Self
   }
   /// constructor TQTXBaseMessage.Create()
   ,Create$99:function(Self) {
      TObject.Create(Self);
      Self.FBinary = TDataTypeConverter.Create$15$($New(TBinaryData));
      Self.FEnvelope = TQTXJSONObject.Create$103$($New(TQTXJSONObject));
      Self.FRouting = TQTXRoute.Create$102($New(TQTXRoute),Self);
      return Self
   }
   /// function TQTXBaseMessage.CreateTicket() : String
   ,CreateTicket:function(Self) {
      return TString.CreateGUID(TString);
   }
   /// destructor TQTXBaseMessage.Destroy()
   ,Destroy:function(Self) {
      TObject.Free(Self.FBinary);
      TObject.Free(Self.FEnvelope);
      TObject.Free(Self.FRouting);
      TObject.Destroy(Self);
   }
   /// procedure TQTXBaseMessage.Parse(const ObjectString: String)
   ,Parse$3:function(Self, ObjectString) {
      TQTXJSONObject.FromJSON$3(Self.FEnvelope,ObjectString);
      TQTXBaseMessage.ReadObjData$(Self,Self.FEnvelope);
   }
   /// function TQTXBaseMessage.QueryIdentifier(ObjectString: String) : String
   ,QueryIdentifier:function(Self, ObjectString$1) {
      var Result = {v:""};
      try {
         var LEnvelope = null;
         LEnvelope = TQTXJSONObject.Create$103$($New(TQTXJSONObject));
         try {
            try {
               TQTXJSONObject.FromJSON$3(LEnvelope,ObjectString$1);
               if (TQTXJSONObject.Exists$4(LEnvelope,"identifier")) {
                  Result.v = TQTXJSONObject.ReadString$5(LEnvelope,"identifier");
               }
            } catch ($e) {
               return Result.v;
            }
         } finally {
            TObject.Free(LEnvelope);
         }
      } finally {return Result.v}
   }
   /// procedure TQTXBaseMessage.ReadObjData(const Root: TQTXJSONObject)
   ,ReadObjData:function(Self, Root$2) {
      var LRoute = null,
         LSource = null;
      if (TQTXJSONObject.ReadString$5(Root$2,"identifier")==TObject.ClassName(Self.ClassType)) {
         Self.Ticket = TQTXJSONObject.ReadString$5(Root$2,"ticket");
         LRoute = TQTXJSONObject.Locate(Root$2,"routing",false);
         if (LRoute!==null) {
            TQTXRoute.ReadObjData$8$(Self.FRouting,LRoute);
         }
         LSource = TQTXJSONObject.Locate(Root$2,TObject.ClassName(Self.ClassType),false);
         if (LSource!==null) {
            Self.SendTime = TQTXJSONObject.ReadFloat$3(LSource,"sendtime");
            TBinaryData.FromBase64(Self.FBinary,String(TQTXJSONObject.Read$9(LSource,"attachment")));
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EW3Exception),"Expected identifier of [%s] error",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// function TQTXBaseMessage.Serialize() : String
   ,Serialize:function(Self) {
      var Result = "";
      TQTXBaseMessage.WriteObjData$(Self,Self.FEnvelope);
      Result = TQTXJSONObject.ToJSON$6(Self.FEnvelope,4);
      return Result
   }
   /// procedure TQTXBaseMessage.WriteObjData(const Root: TQTXJSONObject)
   ,WriteObjData:function(Self, Root$3) {
      var LRoute$1 = null,
         LTarget$3 = null;
      TQTXJSONObject.WriteOrAdd(Root$3,"identifier",TObject.ClassName(Self.ClassType));
      TQTXJSONObject.WriteOrAdd(Root$3,"ticket",Self.Ticket);
      LRoute$1 = TQTXJSONObject.Branch(Root$3,"routing");
      TQTXRoute.WriteObjData$8$(Self.FRouting,LRoute$1);
      Self.SendTime = Now();
      LTarget$3 = TQTXJSONObject.Branch(Root$3,TObject.ClassName(Self.ClassType));
      TQTXJSONObject.WriteOrAdd(LTarget$3,"sendtime",Self.SendTime);
      TQTXJSONObject.WriteOrAdd(LTarget$3,"attachment",TBinaryData.ToBase64(Self.FBinary));
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Clear$17$:function($){return $.ClassType.Clear$17($)}
   ,Create$100$:function($){return $.ClassType.Create$100.apply($.ClassType, arguments)}
   ,Create$99$:function($){return $.ClassType.Create$99($)}
   ,ReadObjData$:function($){return $.ClassType.ReadObjData.apply($.ClassType, arguments)}
   ,WriteObjData$:function($){return $.ClassType.WriteObjData.apply($.ClassType, arguments)}
};
/// TQTXResponseMessage = class (TQTXBaseMessage)
var TQTXResponseMessage = {
   $ClassName:"TQTXResponseMessage",$Parent:TQTXBaseMessage
   ,$Init:function ($) {
      TQTXBaseMessage.$Init($);
      $.Code = 0;
      $.Response = "";
   }
   /// procedure TQTXResponseMessage.ReadObjData(const Root: TQTXJSONObject)
   ,ReadObjData:function(Self, Root$4) {
      var LParent = null,
         LSource$1 = null;
      TQTXBaseMessage.ReadObjData(Self,Root$4);
      LParent = TQTXJSONObject.Locate(Root$4,TObject.ClassName(Self.ClassType),false);
      if (LParent!==null) {
         LSource$1 = TQTXJSONObject.Locate(LParent,"response",false);
         if (LSource$1!==null) {
            Self.Code = TQTXJSONObject.ReadInteger$3(LSource$1,"code");
            Self.Response = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$5(LSource$1,"text"));
         } else {
            throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",["response"]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// procedure TQTXResponseMessage.WriteObjData(const Root: TQTXJSONObject)
   ,WriteObjData:function(Self, Root$5) {
      var LParent$1 = null,
         LTarget$4 = null;
      TQTXBaseMessage.WriteObjData(Self,Root$5);
      LParent$1 = TQTXJSONObject.Locate(Root$5,TObject.ClassName(Self.ClassType),false);
      if (LParent$1!==null) {
         LTarget$4 = TQTXJSONObject.Branch(LParent$1,"response");
         if (LTarget$4!==null) {
            TQTXJSONObject.WriteOrAdd(LTarget$4,"code",Self.Code);
            TQTXJSONObject.WriteOrAdd(LTarget$4,"text",TString.EncodeBase64(TString,Self.Response));
         } else {
            throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",["response"]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Clear$17:TQTXBaseMessage.Clear$17
   ,Create$100:TQTXBaseMessage.Create$100
   ,Create$99:TQTXBaseMessage.Create$99
   ,ReadObjData$:function($){return $.ClassType.ReadObjData.apply($.ClassType, arguments)}
   ,WriteObjData$:function($){return $.ClassType.WriteObjData.apply($.ClassType, arguments)}
};
/// TQTXErrorMessage = class (TQTXResponseMessage)
var TQTXErrorMessage = {
   $ClassName:"TQTXErrorMessage",$Parent:TQTXResponseMessage
   ,$Init:function ($) {
      TQTXResponseMessage.$Init($);
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Clear$17:TQTXBaseMessage.Clear$17
   ,Create$100:TQTXBaseMessage.Create$100
   ,Create$99:TQTXBaseMessage.Create$99
   ,ReadObjData:TQTXResponseMessage.ReadObjData
   ,WriteObjData:TQTXResponseMessage.WriteObjData
};
/// EQTXMessage = class (EW3Exception)
var EQTXMessage = {
   $ClassName:"EQTXMessage",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TQTXPropertyDataType enumeration
var TQTXPropertyDataType = [ "qdtInvalid", "qdtBoolean", "qdtinteger", "qdtfloat", "qdtstring", "qdtSymbol", "qdtFunction", "qdtObject", "qdtArray", "qdtVariant" ];
/// TQTXJSONObject = class (TObject)
var TQTXJSONObject = {
   $ClassName:"TQTXJSONObject",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FInstance$1 = undefined;
   }
   /// anonymous TSourceMethodSymbol
   ,a$437:function(Self) {
      return Self.FInstance$1;
   }
   /// function TQTXJSONObject.Branch(const ObjectName: String) : TQTXJSONObject
   ,Branch:function(Self, ObjectName) {
      var Result = null;
      if (Self.FInstance$1.hasOwnProperty(ObjectName)) {
         Result = TQTXJSONObject.Create$104($New(TQTXJSONObject),Self.FInstance$1[ObjectName]);
         return Result;
      }
      Self.FInstance$1[ObjectName] = TVariant.CreateObject();
      Result = TQTXJSONObject.Create$104($New(TQTXJSONObject),Self.FInstance$1[ObjectName]);
      return Result
   }
   /// procedure TQTXJSONObject.Clear()
   ,Clear$19:function(Self) {
      Self.FInstance$1 = TVariant.CreateObject();
   }
   /// constructor TQTXJSONObject.Create(const Instance: THandle)
   ,Create$104:function(Self, Instance$11) {
      TObject.Create(Self);
      if (Instance$11) {
         if (TW3VariantHelper$IsObject(Instance$11)) {
            Self.FInstance$1 = Instance$11;
         } else {
            throw Exception.Create($New(EQTXJSONObject),"Failed to attach to JSON instance, reference is not an object");
         }
      } else {
         throw Exception.Create($New(EQTXJSONObject),"Failed to attach to JSON instance, reference was NIL error");
      }
      return Self
   }
   /// constructor TQTXJSONObject.Create()
   ,Create$103:function(Self) {
      TObject.Create(Self);
      Self.FInstance$1 = TVariant.CreateObject();
      return Self
   }
   /// procedure TQTXJSONObject.Delete(const PropertyName: String)
   ,Delete$10:function(Self, PropertyName$9) {
      var LRef$7 = undefined;
      if (Self.FInstance$1.hasOwnProperty(PropertyName$9)) {
         LRef$7 = Self.FInstance$1;
         delete LRef$7[PropertyName$9];
      }
   }
   /// destructor TQTXJSONObject.Destroy()
   ,Destroy:function(Self) {
      Self.FInstance$1 = null;
      TObject.Destroy(Self);
   }
   /// function TQTXJSONObject.Examine(const PropertyName: String) : TQTXPropertyDataType
   ,Examine$5:function(Self, PropertyName$10) {
      var Result = 0;
      if (Self.FInstance$1.hasOwnProperty(PropertyName$10)) {
         Result = TQTXJSONDataTypeResolver.QueryDataType(TQTXJSONDataTypeResolver,Self.FInstance$1[PropertyName$10]);
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXJSONObject),"Failed to examine datatype, property [%s] not found error",[PropertyName$10]);
      }
      return Result
   }
   /// function TQTXJSONObject.Exists(const PropertyName: String) : Boolean
   ,Exists$4:function(Self, PropertyName$11) {
      var Result = false;
      if (Self.FInstance$1) {
         Result = (Self.FInstance$1.hasOwnProperty(PropertyName$11)?true:false);
      } else {
         Result = false;
      }
      return Result
   }
   /// function TQTXJSONObject.ForEach(const CB: TQTXJSONEnumerator) : TQTXJSONObject
   ,ForEach$9:function(Self, CB$30) {
      var Result = null;
      var a$496 = 0;
      var el$5 = "",
         LData$5;
      Result = Self;
      if (CB$30) {
         var a$497 = [];
         a$497 = TQTXJSONObject.GetInstanceKeys(Self);
         var $temp96;
         for(a$496=0,$temp96=a$497.length;a$496<$temp96;a$496++) {
            el$5 = a$497[a$496];
            LData$5.v = TQTXJSONObject.Read$9(Self,el$5);
            if (CB$30(el$5,LData$5)==1) {
               TQTXJSONObject.Write$12(Self,el$5,LData$5.v);
            } else {
               break;
            }
         }
      }
      return Result
   }
   /// procedure TQTXJSONObject.FromJSON(const Text: String)
   ,FromJSON$3:function(Self, Text$38) {
      Self.FInstance$1 = JSON.parse(Text$38);
   }
   /// function TQTXJSONObject.GetInstanceKeys() : TStrArray
   ,GetInstanceKeys:function(Self) {
      var Result = [];
      var LRef$8 = undefined;
      LRef$8 = Self.FInstance$1;
      if (!(Object.keys === undefined)) {
      Result = Object.keys(LRef$8);
      return Result;
    }

    if (!(Object.getOwnPropertyNames === undefined)) {
        Result = Object.getOwnPropertyNames(LRef$8);
        return Result;
    }

    for (var qtxenum in LRef$8) {
      if ( (LRef$8).hasOwnProperty(qtxenum) == true )
        (Result).push(qtxenum);
    }
    return Result;
      return Result
   }
   /// function TQTXJSONObject.GetProperty(const Index: Integer) : Variant
   ,GetProperty:function(Self, Index$10) {
      var Result = undefined;
      var LRef$9 = undefined;
      LRef$9 = Self.FInstance$1;
      Result = Object.keys(LRef$9)[Index$10];
      return Result
   }
   /// function TQTXJSONObject.GetPropertyByName(const PropertyName: String) : Variant
   ,GetPropertyByName:function(Self, PropertyName$12) {
      var Result = undefined;
      if (Self.FInstance$1.hasOwnProperty(PropertyName$12)) {
         Result = Self.FInstance$1[PropertyName$12];
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXJSONObject),$R[27],[PropertyName$12]);
      }
      return Result
   }
   /// function TQTXJSONObject.GetPropertyCount() : Integer
   ,GetPropertyCount$1:function(Self) {
      var Result = 0;
      var LRef$10 = undefined;
      LRef$10 = Self.FInstance$1;
      Result = Object.keys(LRef$10).length;
      return Result
   }
   /// procedure TQTXJSONObject.LoadFromBuffer(const Buffer: TBinaryData)
   ,LoadFromBuffer$3:function(Self, Buffer$20) {
      var LBytes$16 = [],
         LText$9 = "";
      LBytes$16 = TBinaryData.ToBytes(Buffer$20);
      LText$9 = TString.DecodeUTF8(TString,LBytes$16);
      Self.FInstance$1 = JSON.parse(LText$9);
   }
   /// procedure TQTXJSONObject.LoadFromStream(const Stream: TStream)
   ,LoadFromStream$9:function(Self, Stream$19) {
      var LBytes$17 = [],
         LDecoder = null,
         LText$10 = "";
      LBytes$17 = TStream.Read$1$(Stream$19,TStream.GetSize$(Stream$19));
      LDecoder = new TextDecoder();
      LText$10 = LDecoder.decode(TDatatype.BytesToTypedArray$1(TDatatype,LBytes$17));
      Self.FInstance$1 = JSON.parse(LText$10);
   }
   /// function TQTXJSONObject.Locate(const ObjectName: String; const AllowCreate: Boolean) : TQTXJSONObject
   ,Locate:function(Self, ObjectName$1, AllowCreate) {
      var Result = null;
      if (Self.FInstance$1.hasOwnProperty(ObjectName$1)) {
         Result = TQTXJSONObject.Create$104($New(TQTXJSONObject),Self.FInstance$1[ObjectName$1]);
      } else {
         if (AllowCreate) {
            Self.FInstance$1[ObjectName$1] = TVariant.CreateObject();
            Result = TQTXJSONObject.Create$104($New(TQTXJSONObject),Self.FInstance$1[ObjectName$1]);
         }
      }
      return Result
   }
   /// function TQTXJSONObject.Read(const PropertyName: String) : Variant
   ,Read$9:function(Self, PropertyName$13) {
      var Result = undefined;
      if (Self.FInstance$1.hasOwnProperty(PropertyName$13)) {
         Result = Self.FInstance$1[PropertyName$13];
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXJSONObject),$R[27],[PropertyName$13]);
      }
      return Result
   }
   /// function TQTXJSONObject.ReadBoolean(const PropertyName: String) : Boolean
   ,ReadBoolean$3:function(Self, PropertyName$14) {
      var Result = false;
      if (Self.FInstance$1.hasOwnProperty(PropertyName$14)) {
         Result = TVariant.AsBool(Self.FInstance$1[PropertyName$14]);
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXJSONObject),$R[27],[PropertyName$14]);
      }
      return Result
   }
   /// function TQTXJSONObject.ReadDateTime(const PropertyName: String) : TDateTime
   ,ReadDateTime$3:function(Self, PropertyName$15) {
      var Result = undefined;
      if (Self.FInstance$1.hasOwnProperty(PropertyName$15)) {
         Result = JDateToDateTime(Self.FInstance$1[PropertyName$15]);
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXJSONObject),$R[27],[PropertyName$15]);
      }
      return Result
   }
   /// function TQTXJSONObject.ReadFloat(const PropertyName: String) : Float
   ,ReadFloat$3:function(Self, PropertyName$16) {
      var Result = 0;
      if (Self.FInstance$1.hasOwnProperty(PropertyName$16)) {
         Result = TVariant.AsFloat(Self.FInstance$1[PropertyName$16]);
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXJSONObject),$R[27],[PropertyName$16]);
      }
      return Result
   }
   /// function TQTXJSONObject.ReadInteger(const PropertyName: String) : Integer
   ,ReadInteger$3:function(Self, PropertyName$17) {
      var Result = 0;
      if (Self.FInstance$1.hasOwnProperty(PropertyName$17)) {
         Result = TVariant.AsInteger(Self.FInstance$1[PropertyName$17]);
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXJSONObject),$R[27],[PropertyName$17]);
      }
      return Result
   }
   /// function TQTXJSONObject.ReadString(const PropertyName: String) : String
   ,ReadString$5:function(Self, PropertyName$18) {
      var Result = "";
      if (Self.FInstance$1.hasOwnProperty(PropertyName$18)) {
         Result = TVariant.AsString(Self.FInstance$1[PropertyName$18]);
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXJSONObject),$R[27],[PropertyName$18]);
      }
      return Result
   }
   /// function TQTXJSONObject.ReadVariant(const PropertyName: String) : Variant
   ,ReadVariant$1:function(Self, PropertyName$19) {
      var Result = undefined;
      var LBytes$18 = [];
      if (TQTXJSONObject.Exists$4(Self,PropertyName$19)) {
         LBytes$18 = TDatatype.Base64ToBytes$1(TDatatype,TQTXJSONObject.ReadString$5(Self,PropertyName$19));
         Result = TDatatype.BytesToVariant$1(TDatatype,LBytes$18);
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXJSONObject),$R[27],[PropertyName$19]);
      }
      return Result
   }
   /// procedure TQTXJSONObject.SaveToBuffer(const Buffer: TBinaryData)
   ,SaveToBuffer$3:function(Self, Buffer$21) {
      var LText$11 = "",
         LBytes$19 = [];
      LText$11 = JSON.stringify(Self.FInstance$1)+"   ";
      LBytes$19 = TString.EncodeUTF8(TString,LText$11);
      TBinaryData.AppendBytes$(Buffer$21,LBytes$19);
   }
   /// procedure TQTXJSONObject.SaveToStream(const Stream: TStream)
   ,SaveToStream$9:function(Self, Stream$20) {
      var LText$12 = "",
         LEncoder = null,
         LTyped = null;
      LText$12 = JSON.stringify(Self.FInstance$1);
      LEncoder = new TextEncoder();
      LTyped = LEncoder.encode(LText$12);
      TStream.Write$2$(Stream$20,TDataTypeConverter.TypedArrayToBytes(Stream$20,LTyped));
   }
   /// function TQTXJSONObject.ToJSON(const Indent: Integer; const Replacer: TQTXKeyValueProcessor) : String
   ,ToJSON$7:function(Self, Indent, Replacer) {
      return JSON.stringify(Self.FInstance$1,Replacer,Indent);
   }
   /// function TQTXJSONObject.ToJSON(const Indent: Integer) : String
   ,ToJSON$6:function(Self, Indent$1) {
      return JSON.stringify(Self.FInstance$1,null,Indent$1);
   }
   /// function TQTXJSONObject.ToJSON() : String
   ,ToJSON$5:function(Self) {
      return JSON.stringify(Self.FInstance$1,null,4);
   }
   /// procedure TQTXJSONObject.Write(const PropertyName: String; const Data: Variant)
   ,Write$12:function(Self, PropertyName$20, Data$68) {
      if (Self.FInstance$1.hasOwnProperty(PropertyName$20)) {
         Self.FInstance$1[PropertyName$20] = Data$68;
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXJSONObject),$R[26],[PropertyName$20]);
      }
   }
   /// procedure TQTXJSONObject.WriteBoolean(const PropertyName: String; const Data: Boolean)
   ,WriteBoolean$2:function(Self, PropertyName$21, Data$69) {
      if (Self.FInstance$1.hasOwnProperty(PropertyName$21)) {
         Self.FInstance$1[PropertyName$21] = Data$69;
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXJSONObject),$R[26],[PropertyName$21]);
      }
   }
   /// procedure TQTXJSONObject.WriteDateTime(const PropertyName: String; const Data: TDateTime)
   ,WriteDateTime$2:function(Self, PropertyName$22, Data$70) {
      if (Self.FInstance$1.hasOwnProperty(PropertyName$22)) {
         Self.FInstance$1[PropertyName$22] = DateTimeToJDate(Data$70);
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXJSONObject),$R[26],[PropertyName$22]);
      }
   }
   /// procedure TQTXJSONObject.WriteFloat(const PropertyName: String; const Data: Float)
   ,WriteFloat$2:function(Self, PropertyName$23, Data$71) {
      if (Self.FInstance$1.hasOwnProperty(PropertyName$23)) {
         Self.FInstance$1[PropertyName$23] = FloatToStr$_Float_(Data$71);
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXJSONObject),$R[26],[PropertyName$23]);
      }
   }
   /// procedure TQTXJSONObject.WriteInteger(const PropertyName: String; const Data: Integer)
   ,WriteInteger$2:function(Self, PropertyName$24, Data$72) {
      if (Self.FInstance$1.hasOwnProperty(PropertyName$24)) {
         Self.FInstance$1[PropertyName$24] = Data$72.toString();
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXJSONObject),$R[26],[PropertyName$24]);
      }
   }
   /// procedure TQTXJSONObject.WriteOrAdd(const PropertyName: String; const Data: Variant)
   ,WriteOrAdd:function(Self, PropertyName$25, Data$73) {
      Self.FInstance$1[PropertyName$25] = Data$73;
   }
   /// procedure TQTXJSONObject.WriteString(const PropertyName: String; const Data: String)
   ,WriteString$4:function(Self, PropertyName$26, Data$74) {
      if (Self.FInstance$1.hasOwnProperty(PropertyName$26)) {
         Self.FInstance$1[PropertyName$26] = Data$74;
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXJSONObject),$R[26],[PropertyName$26]);
      }
   }
   /// procedure TQTXJSONObject.WriteVariant(const PropertyName: String; const Value: Variant)
   ,WriteVariant$1:function(Self, PropertyName$27, Value$165) {
      var LBytes$20 = [];
      if (Value$165) {
         LBytes$20 = TDatatype.VariantToBytes$1(TDatatype,Value$165);
         Self.FInstance$1[PropertyName$27] = TDatatype.BytesToBase64$1(TDatatype,LBytes$20);
      } else {
         Self.FInstance$1[PropertyName$27] = Value$165;
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$103$:function($){return $.ClassType.Create$103($)}
};
TQTXJSONObject.$Intf={
   IQTXJSONStorage:[TQTXJSONObject.GetPropertyCount$1,TQTXJSONObject.GetProperty,TQTXJSONObject.GetPropertyByName,TQTXJSONObject.GetInstanceKeys,TQTXJSONObject.ForEach$9,TQTXJSONObject.Exists$4,TQTXJSONObject.Examine$5,TQTXJSONObject.Read$9,TQTXJSONObject.ReadString$5,TQTXJSONObject.ReadInteger$3,TQTXJSONObject.ReadBoolean$3,TQTXJSONObject.ReadDateTime$3,TQTXJSONObject.ReadFloat$3,TQTXJSONObject.ReadVariant$1,TQTXJSONObject.WriteVariant$1,TQTXJSONObject.Write$12,TQTXJSONObject.WriteOrAdd,TQTXJSONObject.WriteString$4,TQTXJSONObject.WriteInteger$2,TQTXJSONObject.WriteBoolean$2,TQTXJSONObject.WriteDateTime$2,TQTXJSONObject.WriteFloat$2,TQTXJSONObject.Branch,TQTXJSONObject.Locate,TQTXJSONObject.Delete$10]
}
/// TQTXJSONDataTypeResolver = class (TObject)
var TQTXJSONDataTypeResolver = {
   $ClassName:"TQTXJSONDataTypeResolver",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// function TQTXJSONDataTypeResolver.QueryArray(const Reference: THandle) : Boolean
   ,QueryArray:function(Self, Reference$1) {
      var Result = false;
      Result = (Reference$1 !== undefined)
      && (Reference$1 !== null)
      && (Array.isArray(Reference$1) === true);
      return Result
   }
   /// function TQTXJSONDataTypeResolver.QueryBoolean(const Reference: THandle) : Boolean
   ,QueryBoolean:function(Self, Reference$2) {
      var Result = false;
      Result = (Reference$2 !== undefined)
      && (Reference$2 !== null)
      && (typeof Reference$2 === "boolean");
      return Result
   }
   /// function TQTXJSONDataTypeResolver.QueryDataType(const Reference: THandle) : TQTXPropertyDataType
   ,QueryDataType:function(Self, Reference$3) {
      var Result = 0;
      var LType$3 = "";
      if (Reference$3) {
         LType$3 = typeof(Reference$3);
         {var $temp97 = (LType$3).toLocaleLowerCase();
            if ($temp97=="object") {
               if (!Reference$3.length) {
                  Result = 7;
               } else {
                  Result = 8;
               }
            }
             else if ($temp97=="function") {
               Result = 6;
            }
             else if ($temp97=="symbol") {
               Result = 5;
            }
             else if ($temp97=="boolean") {
               Result = 1;
            }
             else if ($temp97=="string") {
               Result = 4;
            }
             else if ($temp97=="number") {
               if (Math.round(Number(Reference$3))!=Reference$3) {
                  Result = 3;
               } else {
                  Result = 2;
               }
            }
             else if ($temp97=="array") {
               Result = 8;
            }
             else {
               Result = 9;
            }
         }
      } else {
         Result = 0;
      }
      return Result
   }
   /// function TQTXJSONDataTypeResolver.Queryfloat(const Reference: THandle) : Boolean
   ,Queryfloat:function(Self, Reference$4) {
      var Result = false;
      Result = (Reference$4 !== undefined)
      && (Reference$4 !== null)
      && (typeof Reference$4  === "number")
      && (Number(Reference$4) === Reference$4 && Reference$4 % 1 !== 0);
      return Result
   }
   /// function TQTXJSONDataTypeResolver.QueryFunction(const Reference: THandle) : Boolean
   ,QueryFunction:function(Self, Reference$5) {
      var Result = false;
      Result = (Reference$5 !== undefined)
      && (Reference$5 !== null)
      && (typeof Reference$5 === "function");
      return Result
   }
   /// function TQTXJSONDataTypeResolver.Queryinteger(const Reference: THandle) : Boolean
   ,Queryinteger:function(Self, Reference$6) {
      var Result = false;
      Result = (Reference$6 !== undefined)
      && (Reference$6 !== null)
      && (typeof Reference$6  === "number")
      && (Number(Reference$6) === Reference$6 && Reference$6 % 1 === 0);
      return Result
   }
   /// function TQTXJSONDataTypeResolver.QueryObject(const Reference: THandle) : Boolean
   ,QueryObject:function(Self, Reference$7) {
      var Result = false;
      Result = (Reference$7 !== undefined)
      && (Reference$7 !== null)
      && (typeof Reference$7  === "object")
      && ((Reference$7).length === undefined);
      return Result
   }
   /// function TQTXJSONDataTypeResolver.Querystring(const Reference: THandle) : Boolean
   ,Querystring:function(Self, Reference$8) {
      var Result = false;
      Result = (Reference$8 !== undefined)
      && (Reference$8 !== null)
      && (typeof Reference$8  === "string");
      return Result
   }
   /// function TQTXJSONDataTypeResolver.QuerySymbol(const Reference: THandle) : Boolean
   ,QuerySymbol:function(Self, Reference$9) {
      var Result = false;
      Result = (Reference$9 !== undefined)
      && (Reference$9 !== null)
      && (typeof Reference$9 === "symbol");
      return Result
   }
   /// function TQTXJSONDataTypeResolver.QueryUInt8Array(const Reference: THandle) : Boolean
   ,QueryUInt8Array:function(Self, Reference$10) {
      var Result = false;
      var LTypeName$1 = "";
      if (Reference$10) {
         LTypeName$1 = "";
         LTypeName$1 = Object.prototype.toString.call(Reference$10);
         Result = LTypeName$1=="[object Uint8Array]";
      }
      return Result
   }
   ,Destroy:TObject.Destroy
};
/// EQTXJSONObject = class (EW3Exception)
var EQTXJSONObject = {
   $ClassName:"EQTXJSONObject",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TQTXClientMessage = class (TQTXBaseMessage)
var TQTXClientMessage = {
   $ClassName:"TQTXClientMessage",$Parent:TQTXBaseMessage
   ,$Init:function ($) {
      TQTXBaseMessage.$Init($);
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Clear$17:TQTXBaseMessage.Clear$17
   ,Create$100:TQTXBaseMessage.Create$100
   ,Create$99:TQTXBaseMessage.Create$99
   ,ReadObjData:TQTXBaseMessage.ReadObjData
   ,WriteObjData:TQTXBaseMessage.WriteObjData
};
/// TQTXRelayRequest = class (TQTXClientMessage)
var TQTXRelayRequest = {
   $ClassName:"TQTXRelayRequest",$Parent:TQTXClientMessage
   ,$Init:function ($) {
      TQTXClientMessage.$Init($);
      $.FRelay = null;
   }
   /// procedure TQTXRelayRequest.ReadObjData(const Root: TQTXJSONObject)
   ,ReadObjData:function(Self, Root$6) {
      var LParent$2 = null,
         LSource$2 = null,
         LEnvelope$1 = null;
      TQTXBaseMessage.ReadObjData(Self,Root$6);
      LParent$2 = TQTXJSONObject.Locate(Root$6,TObject.ClassName(Self.ClassType),false);
      if (LParent$2!==null) {
         LSource$2 = TQTXJSONObject.Locate(LParent$2,"relay",false);
         if (LSource$2!==null) {
            LEnvelope$1 = TQTXJSONObject.Locate(LSource$2,"envelope",false);
            if (LEnvelope$1!==null) {
               Self.FRelay = TQTXJSONObject.Locate(LEnvelope$1,"message",false);
            } else {
               throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",["envelope"]);
            }
         } else {
            throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",["relay"]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// procedure TQTXRelayRequest.WriteObjData(const Root: TQTXJSONObject)
   ,WriteObjData:function(Self, Root$7) {
      var LParent$3 = null,
         LTarget$5 = null,
         LEnvelope$2 = null;
      TQTXBaseMessage.WriteObjData(Self,Root$7);
      LParent$3 = TQTXJSONObject.Locate(Root$7,TObject.ClassName(Self.ClassType),false);
      if (LParent$3!==null) {
         LTarget$5 = TQTXJSONObject.Branch(LParent$3,"relay");
         if (LTarget$5!==null) {
            LEnvelope$2 = TQTXJSONObject.Branch(LTarget$5,"envelope");
            if (Self.FRelay!==null) {
               TQTXJSONObject.WriteOrAdd(LEnvelope$2,"message",Self.FRelay);
            }
         } else {
            throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",["relay"]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// function TQTXRelayRequest.GetMessage() : TQTXJSONObject
   ,GetMessage:function(Self) {
      return Self.FRelay;
   }
   /// function TQTXRelayRequest.GetMessageRoute() : TQTXRoute
   ,GetMessageRoute:function(Self) {
      var Result = null;
      var LTemp$20 = null;
      if (Self.FRelay!==null) {
         LTemp$20 = TQTXBaseMessage.Create$99$($New(TQTXBaseMessage));
         TQTXBaseMessage.Parse$3(LTemp$20,TQTXJSONObject.ToJSON$5(Self.FRelay));
         Result = LTemp$20.FRouting;
      }
      return Result
   }
   /// procedure TQTXRelayRequest.SetMessage(const ThisMessage: TQTXJSONObject)
   ,SetMessage:function(Self, ThisMessage) {
      Self.FRelay = ThisMessage;
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Clear$17:TQTXBaseMessage.Clear$17
   ,Create$100:TQTXBaseMessage.Create$100
   ,Create$99:TQTXBaseMessage.Create$99
   ,ReadObjData$:function($){return $.ClassType.ReadObjData.apply($.ClassType, arguments)}
   ,WriteObjData$:function($){return $.ClassType.WriteObjData.apply($.ClassType, arguments)}
};
/// TQTXLoginResponse = class (TQTXResponseMessage)
var TQTXLoginResponse = {
   $ClassName:"TQTXLoginResponse",$Parent:TQTXResponseMessage
   ,$Init:function ($) {
      TQTXResponseMessage.$Init($);
      $.Username = $.SessionID = "";
   }
   /// procedure TQTXLoginResponse.ReadObjData(const Root: TQTXJSONObject)
   ,ReadObjData:function(Self, Root$8) {
      var LParent$4 = null,
         LSource$3 = null;
      TQTXResponseMessage.ReadObjData(Self,Root$8);
      LParent$4 = TQTXJSONObject.Locate(Root$8,TObject.ClassName(Self.ClassType),false);
      if (LParent$4!==null) {
         LSource$3 = TQTXJSONObject.Locate(LParent$4,"session",false);
         if (LSource$3!==null) {
            Self.Username = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$5(LSource$3,"username"));
            Self.SessionID = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$5(LSource$3,"sessionid"));
         } else {
            throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",["session"]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// procedure TQTXLoginResponse.WriteObjData(const Root: TQTXJSONObject)
   ,WriteObjData:function(Self, Root$9) {
      var LParent$5 = null,
         LTarget$6 = null;
      TQTXResponseMessage.WriteObjData(Self,Root$9);
      LParent$5 = TQTXJSONObject.Locate(Root$9,TObject.ClassName(Self.ClassType),false);
      if (LParent$5!==null) {
         LTarget$6 = TQTXJSONObject.Branch(LParent$5,"session");
         if (LTarget$6!==null) {
            TQTXJSONObject.WriteOrAdd(LTarget$6,"username",TString.EncodeBase64(TString,Self.Username));
            TQTXJSONObject.WriteOrAdd(LTarget$6,"sessionid",TString.EncodeBase64(TString,Self.SessionID));
         } else {
            throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",["session"]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Clear$17:TQTXBaseMessage.Clear$17
   ,Create$100:TQTXBaseMessage.Create$100
   ,Create$99:TQTXBaseMessage.Create$99
   ,ReadObjData$:function($){return $.ClassType.ReadObjData.apply($.ClassType, arguments)}
   ,WriteObjData$:function($){return $.ClassType.WriteObjData.apply($.ClassType, arguments)}
};
/// TQTXLoginRequest = class (TQTXClientMessage)
var TQTXLoginRequest = {
   $ClassName:"TQTXLoginRequest",$Parent:TQTXClientMessage
   ,$Init:function ($) {
      TQTXClientMessage.$Init($);
      $.Username = $.Password = "";
   }
   /// procedure TQTXLoginRequest.ReadObjData(const Root: TQTXJSONObject)
   ,ReadObjData:function(Self, Root$10) {
      var LParent$6 = null,
         LSource$4 = null;
      TQTXBaseMessage.ReadObjData(Self,Root$10);
      LParent$6 = TQTXJSONObject.Locate(Root$10,TObject.ClassName(Self.ClassType),false);
      if (LParent$6!==null) {
         LSource$4 = TQTXJSONObject.Locate(LParent$6,"credentials",false);
         if (LSource$4!==null) {
            Self.Username = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$5(LSource$4,"username"));
            Self.Password = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$5(LSource$4,"password"));
         } else {
            throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",["credentials"]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// procedure TQTXLoginRequest.WriteObjData(const Root: TQTXJSONObject)
   ,WriteObjData:function(Self, Root$11) {
      var LParent$7 = null,
         LTarget$7 = null;
      TQTXBaseMessage.WriteObjData(Self,Root$11);
      LParent$7 = TQTXJSONObject.Locate(Root$11,TObject.ClassName(Self.ClassType),false);
      if (LParent$7!==null) {
         LTarget$7 = TQTXJSONObject.Branch(LParent$7,"credentials");
         if (LTarget$7!==null) {
            TQTXJSONObject.WriteOrAdd(LTarget$7,"username",TString.EncodeBase64(TString,Self.Username));
            TQTXJSONObject.WriteOrAdd(LTarget$7,"password",TString.EncodeBase64(TString,Self.Password));
         } else {
            throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",["credentials"]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Clear$17:TQTXBaseMessage.Clear$17
   ,Create$100:TQTXBaseMessage.Create$100
   ,Create$99:TQTXBaseMessage.Create$99
   ,ReadObjData$:function($){return $.ClassType.ReadObjData.apply($.ClassType, arguments)}
   ,WriteObjData$:function($){return $.ClassType.WriteObjData.apply($.ClassType, arguments)}
};
/// TQTXFileIOResponse = class (TQTXResponseMessage)
var TQTXFileIOResponse = {
   $ClassName:"TQTXFileIOResponse",$Parent:TQTXResponseMessage
   ,$Init:function ($) {
      TQTXResponseMessage.$Init($);
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Clear$17:TQTXBaseMessage.Clear$17
   ,Create$100:TQTXBaseMessage.Create$100
   ,Create$99:TQTXBaseMessage.Create$99
   ,ReadObjData:TQTXResponseMessage.ReadObjData
   ,WriteObjData:TQTXResponseMessage.WriteObjData
};
/// TQTXFileIORequest = class (TQTXClientMessage)
var TQTXFileIORequest = {
   $ClassName:"TQTXFileIORequest",$Parent:TQTXClientMessage
   ,$Init:function ($) {
      TQTXClientMessage.$Init($);
      $.Command = $.SessionId = "";
   }
   /// procedure TQTXFileIORequest.ReadObjData(const Root: TQTXJSONObject)
   ,ReadObjData:function(Self, Root$12) {
      var LParent$8 = null,
         LSource$5 = null;
      TQTXBaseMessage.ReadObjData(Self,Root$12);
      LParent$8 = TQTXJSONObject.Locate(Root$12,TObject.ClassName(Self.ClassType),false);
      if (LParent$8!==null) {
         LSource$5 = TQTXJSONObject.Locate(LParent$8,"filesystem",false);
         if (LSource$5!==null) {
            Self.Command = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$5(LSource$5,"command"));
            Self.SessionId = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$5(LSource$5,"sessionid"));
         } else {
            throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",["filesystem"]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// procedure TQTXFileIORequest.WriteObjData(const Root: TQTXJSONObject)
   ,WriteObjData:function(Self, Root$13) {
      var LParent$9 = null,
         LTarget$8 = null;
      TQTXBaseMessage.WriteObjData(Self,Root$13);
      LParent$9 = TQTXJSONObject.Locate(Root$13,TObject.ClassName(Self.ClassType),false);
      if (LParent$9!==null) {
         LTarget$8 = TQTXJSONObject.Branch(LParent$9,"filesystem");
         if (LTarget$8!==null) {
            TQTXJSONObject.WriteOrAdd(LTarget$8,"command",TString.EncodeBase64(TString,Self.Command));
            TQTXJSONObject.WriteOrAdd(LTarget$8,"sessionid",TString.EncodeBase64(TString,Self.SessionId));
         } else {
            throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",["filesystem"]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Clear$17:TQTXBaseMessage.Clear$17
   ,Create$100:TQTXBaseMessage.Create$100
   ,Create$99:TQTXBaseMessage.Create$99
   ,ReadObjData$:function($){return $.ClassType.ReadObjData.apply($.ClassType, arguments)}
   ,WriteObjData$:function($){return $.ClassType.WriteObjData.apply($.ClassType, arguments)}
};
/// TQTXFileIOLoadResponse = class (TQTXFileIOResponse)
var TQTXFileIOLoadResponse = {
   $ClassName:"TQTXFileIOLoadResponse",$Parent:TQTXFileIOResponse
   ,$Init:function ($) {
      TQTXFileIOResponse.$Init($);
      $.Filename = "";
   }
   /// procedure TQTXFileIOLoadResponse.ReadObjData(const Root: TQTXJSONObject)
   ,ReadObjData:function(Self, Root$14) {
      var LParent$10 = null;
      TQTXResponseMessage.ReadObjData(Self,Root$14);
      LParent$10 = TQTXJSONObject.Locate(Root$14,TObject.ClassName(Self.ClassType),false);
      if (LParent$10!==null) {
         if (TQTXJSONObject.Exists$4(LParent$10,"filename")) {
            Self.Filename = TString.DecodeBase64(TString,TQTXJSONObject.ReadString$5(LParent$10,"filename"));
         } else {
            throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",["directorylist"]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// procedure TQTXFileIOLoadResponse.WriteObjData(const Root: TQTXJSONObject)
   ,WriteObjData:function(Self, Root$15) {
      var LParent$11 = null;
      TQTXResponseMessage.WriteObjData(Self,Root$15);
      LParent$11 = TQTXJSONObject.Locate(Root$15,TObject.ClassName(Self.ClassType),false);
      if (LParent$11!==null) {
         TQTXJSONObject.WriteOrAdd(LParent$11,"filename",TString.EncodeBase64(TString,Self.Filename));
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Clear$17:TQTXBaseMessage.Clear$17
   ,Create$100:TQTXBaseMessage.Create$100
   ,Create$99:TQTXBaseMessage.Create$99
   ,ReadObjData$:function($){return $.ClassType.ReadObjData.apply($.ClassType, arguments)}
   ,WriteObjData$:function($){return $.ClassType.WriteObjData.apply($.ClassType, arguments)}
};
/// TQTXFileIODirResponse = class (TQTXFileIOResponse)
var TQTXFileIODirResponse = {
   $ClassName:"TQTXFileIODirResponse",$Parent:TQTXFileIOResponse
   ,$Init:function ($) {
      TQTXFileIOResponse.$Init($);
      $.FDirList = null;
   }
   /// procedure TQTXFileIODirResponse.ReadObjData(const Root: TQTXJSONObject)
   ,ReadObjData:function(Self, Root$16) {
      var LParent$12 = null;
      TQTXResponseMessage.ReadObjData(Self,Root$16);
      LParent$12 = TQTXJSONObject.Locate(Root$16,TObject.ClassName(Self.ClassType),false);
      if (LParent$12!==null) {
         if (TQTXJSONObject.Exists$4(LParent$12,"filesystem")) {
            Self.FDirList = TVariant.AsObject(TQTXJSONObject.Read$9(LParent$12,"filesystem"));
         } else {
            throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",["filesystem"]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to consume message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// procedure TQTXFileIODirResponse.WriteObjData(const Root: TQTXJSONObject)
   ,WriteObjData:function(Self, Root$17) {
      var LParent$13 = null;
      TQTXResponseMessage.WriteObjData(Self,Root$17);
      LParent$13 = TQTXJSONObject.Locate(Root$17,TObject.ClassName(Self.ClassType),false);
      if (LParent$13!==null) {
         TQTXJSONObject.WriteOrAdd(LParent$13,"filesystem",Self.FDirList);
      } else {
         throw EW3Exception.CreateFmt$($New(EQTXMessage),"Failed to compose message, entrypoint \"%s\" not found",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// procedure TQTXFileIODirResponse.LoadDirListFromString(const Data: String)
   ,LoadDirListFromString:function(Self, Data$75) {
      var LTemp$21;
      LTemp$21 = JSON.parse(Data$75);
      (Self.FDirList) = LTemp$21;
   }
   /// constructor TQTXFileIODirResponse.Create()
   ,Create$99:function(Self) {
      TQTXBaseMessage.Create$99(Self);
      Self.FDirList = new TNJFileItemList();
      return Self
   }
   ,Destroy:TQTXBaseMessage.Destroy
   ,Clear$17:TQTXBaseMessage.Clear$17
   ,Create$100:TQTXBaseMessage.Create$100
   ,Create$99$:function($){return $.ClassType.Create$99($)}
   ,ReadObjData$:function($){return $.ClassType.ReadObjData.apply($.ClassType, arguments)}
   ,WriteObjData$:function($){return $.ClassType.WriteObjData.apply($.ClassType, arguments)}
};
/// TQTXChannelOperation enumeration
var TQTXChannelOperation = [ "coOpen", "coClose", "coQuery" ];
/// TRagnarokMessageInfo = class (TObject)
var TRagnarokMessageInfo = {
   $ClassName:"TRagnarokMessageInfo",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.MessageClass = null;
      $.MessageHandler = null;
   }
   ,Destroy:TObject.Destroy
};
/// TRagnarokMessageDispatcher = class (TObject)
var TRagnarokMessageDispatcher = {
   $ClassName:"TRagnarokMessageDispatcher",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FItems$2 = [];
      $.FNameLUT = null;
   }
   /// anonymous TSourceMethodSymbol
   ,a$439:function(Self, index$11) {
      return Self.FItems$2[index$11];
   }
   /// anonymous TSourceMethodSymbol
   ,a$438:function(Self) {
      return Self.FItems$2.length;
   }
   /// procedure TRagnarokMessageDispatcher.Clear()
   ,Clear$20:function(Self) {
      var a$498 = 0;
      var info$2 = null;
      try {
         var a$499 = [];
         a$499 = Self.FItems$2;
         var $temp98;
         for(a$498=0,$temp98=a$499.length;a$498<$temp98;a$498++) {
            info$2 = a$499[a$498];
            TObject.Free(info$2);
         }
      } finally {
         Self.FItems$2.length=0;
         TW3CustomDictionary.Clear$7(Self.FNameLUT);
      }
   }
   /// constructor TRagnarokMessageDispatcher.Create()
   ,Create$105:function(Self) {
      TObject.Create(Self);
      Self.FNameLUT = TW3CustomDictionary.Create$74$($New(TW3ObjDictionary));
      return Self
   }
   /// destructor TRagnarokMessageDispatcher.Destroy()
   ,Destroy:function(Self) {
      if (Self.FItems$2.length>0) {
         TRagnarokMessageDispatcher.Clear$20$(Self);
      }
      TObject.Free(Self.FNameLUT);
      TObject.Destroy(Self);
   }
   /// function TRagnarokMessageDispatcher.GetMessageInfoForClass(const MessageClass: TQTXMessageBaseClass) : TRagnarokMessageInfo
   ,GetMessageInfoForClass:function(Self, MessageClass$2) {
      var Result = null;
      var a$500 = 0;
      var Info$4 = null;
      if (MessageClass$2) {
         var a$501 = [];
         a$501 = Self.FItems$2;
         var $temp99;
         for(a$500=0,$temp99=a$501.length;a$500<$temp99;a$500++) {
            Info$4 = a$501[a$500];
            if (Info$4.MessageClass==MessageClass$2) {
               Result = Info$4;
               break;
            }
         }
      }
      return Result
   }
   /// function TRagnarokMessageDispatcher.GetMessageInfoForName(MessageClassName: String) : TRagnarokMessageInfo
   ,GetMessageInfoForName:function(Self, MessageClassName$1) {
      var Result = null;
      MessageClassName$1 = (Trim$_String_(MessageClassName$1)).toLocaleLowerCase();
      if (MessageClassName$1.length>0) {
         if (TW3CustomDictionary.Contains(Self.FNameLUT,MessageClassName$1)) {
            Result = $As(TW3ObjDictionary.a$108(Self.FNameLUT,MessageClassName$1),TRagnarokMessageInfo);
         }
      }
      return Result
   }
   /// procedure TRagnarokMessageDispatcher.RegisterMessage(const MessageClass: TQTXMessageBaseClass; const Handler: TRRMessageHandler)
   ,RegisterMessage:function(Self, MessageClass$3, Handler) {
      var LName$8 = "",
         Info$5 = null;
      if (MessageClass$3) {
         if (Handler) {
            LName$8 = (Trim$_String_(TObject.ClassName(MessageClass$3))).toLocaleLowerCase();
            if (TW3CustomDictionary.Contains(Self.FNameLUT,LName$8)) {
               throw Exception.Create($New(ERagnarokMessageDispatcher),$R[25]);
            } else {
               Info$5 = TObject.Create($New(TRagnarokMessageInfo));
               Info$5.MessageClass = MessageClass$3;
               Info$5.MessageHandler = Handler;
               Self.FItems$2.push(Info$5);
               TW3ObjDictionary.a$107(Self.FNameLUT,LName$8,Info$5);
            }
         } else {
            throw Exception.Create($New(ERagnarokMessageDispatcher),$R[24]);
         }
      } else {
         throw Exception.Create($New(ERagnarokMessageDispatcher),$R[23]);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Clear$20$:function($){return $.ClassType.Clear$20($)}
   ,Create$105$:function($){return $.ClassType.Create$105($)}
};
/// ERagnarokMessageDispatcher = class (EW3Exception)
var ERagnarokMessageDispatcher = {
   $ClassName:"ERagnarokMessageDispatcher",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TWbAccountState enumeration
var TWbAccountState = [ "asActive", "asLocked", "asUnConfirmed" ];
/// TWbAccountPrivilege enumeration
var TWbAccountPrivilege = [ "apExecute", "apRead", "apWrite", "apCreate", "apInstall", "apAppstore" ];
/// TUserSessionManager = class (TW3ErrorObject)
var TUserSessionManager = {
   $ClassName:"TUserSessionManager",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.OnPrivilegeGranted = null;
      $.OnPrivilegeRevoked = null;
      $.OnSessionClosed = null;
      $.OnSessionOpened = null;
      $.FIPLookup = $.FLookup = null;
      $.FSessions = [];
   }
   /// anonymous TSourceMethodSymbol
   ,a$409:function(Self) {
      return Self.FSessions.length;
   }
   /// anonymous TSourceMethodSymbol
   ,a$408:function(Self, index$12) {
      return Self.FSessions[index$12];
   }
   /// function TUserSessionManager.Add(const ThisSession: TUserSession) : TUserSession
   ,Add$6:function(Self, ThisSession) {
      var Result = null;
      var LRemoteIP = "",
         LSessionID = "";
      TW3ErrorObject.ClearLastError(Self);
      Result = ThisSession;
      if (ThisSession!==null) {
         LRemoteIP = Trim$_String_(ThisSession.FRemoteIP);
         if (LRemoteIP.length>0) {
            if (TW3CustomDictionary.Contains(Self.FIPLookup,ThisSession.FRemoteIP)) {
               return Result;
            }
         }
         LSessionID = (TUserSession.GetIdentifier(ThisSession)).toLocaleLowerCase();
         if (!TW3CustomDictionary.Contains(Self.FLookup,LSessionID)) {
            Self.FSessions.push(ThisSession);
            TW3ObjDictionary.a$107(Self.FLookup,LSessionID,ThisSession);
            if (LRemoteIP.length>0) {
               TW3ObjDictionary.a$107(Self.FIPLookup,LRemoteIP,Result);
            }
            TUserSessionManager.SessionAdded(Self,ThisSession);
         }
      } else {
         TW3ErrorObject.SetLastError$(Self,"Failed to add session, instance was NIL error");
      }
      return Result
   }
   /// function TUserSessionManager.Add(FromThisIP: String) : TUserSession
   ,Add$5:function(Self, FromThisIP) {
      var Result = null;
      var LSessionID$1 = "";
      TW3ErrorObject.ClearLastError(Self);
      FromThisIP = (Trim$_String_(FromThisIP)).toLocaleLowerCase();
      if (FromThisIP.length<1) {
         TW3ErrorObject.SetLastError$(Self,"Failed to add session, remote IP was empty error");
      }
      if (TW3CustomDictionary.Contains(Self.FIPLookup,FromThisIP)) {
         Result = $As(TW3ObjDictionary.a$108(Self.FIPLookup,FromThisIP),TUserSession);
         return Result;
      }
      Result = TUserSession.Create$96$($New(TUserSession),Self,FromThisIP);
      LSessionID$1 = (TUserSession.GetIdentifier(Result)).toLocaleLowerCase();
      Self.FSessions.push(Result);
      TW3ObjDictionary.a$107(Self.FLookup,LSessionID$1,Result);
      TUserSessionManager.SessionAdded(Self,Result);
      TW3ObjDictionary.a$107(Self.FIPLookup,FromThisIP,Result);
      return Result
   }
   /// procedure TUserSessionManager.Clear()
   ,Clear$16:function(Self) {
      var a$502 = 0;
      var item$6 = null;
      try {
         var a$503 = [];
         a$503 = Self.FSessions;
         var $temp100;
         for(a$502=0,$temp100=a$503.length;a$502<$temp100;a$502++) {
            item$6 = a$503[a$502];
            TObject.Free(item$6);
         }
      } finally {
         Self.FSessions.length=0;
         TW3CustomDictionary.Clear$7(Self.FLookup);
      }
   }
   /// constructor TUserSessionManager.Create()
   ,Create$3:function(Self) {
      TW3ErrorObject.Create$3(Self);
      Self.FLookup = TW3CustomDictionary.Create$74$($New(TW3ObjDictionary));
      Self.FIPLookup = TW3CustomDictionary.Create$74$($New(TW3ObjDictionary));
      return Self
   }
   /// destructor TUserSessionManager.Destroy()
   ,Destroy:function(Self) {
      if (Self.FSessions.length>0) {
         TUserSessionManager.Clear$16$(Self);
      }
      TObject.Free(Self.FLookup);
      TObject.Free(Self.FIPLookup);
      TW3ErrorObject.Destroy(Self);
   }
   /// procedure TUserSessionManager.ForEach(const Before: TProcedureRef; const Process: TUserSessionEnumCallback; const After: TProcedureRef)
   ,ForEach$8:function(Self, Before, Process$3, After) {
      var a$504 = 0;
      var LItem$4 = null;
      try {
         if (Before) {
            Before();
         }
      } finally {
         try {
            if (Process$3) {
               var a$505 = [];
               a$505 = Self.FSessions;
               var $temp101;
               for(a$504=0,$temp101=a$505.length;a$504<$temp101;a$504++) {
                  LItem$4 = a$505[a$504];
                  if (Process$3(LItem$4)==16) {
                     break;
                  }
               }
            }
         } finally {
            if (After) {
               After();
            }
         }
      }
   }
   /// procedure TUserSessionManager.ForEach(const Process: TUserSessionEnumCallback)
   ,ForEach$7:function(Self, Process$4) {
      var a$506 = 0;
      var LItem$5 = null;
      var a$507 = [];
      a$507 = Self.FSessions;
      var $temp102;
      for(a$506=0,$temp102=a$507.length;a$506<$temp102;a$506++) {
         LItem$5 = a$507[a$506];
         if (Process$4(LItem$5)==16) {
            break;
         }
      }
   }
   /// function TUserSessionManager.GetSessionbyId(Identifier: String) : TUserSession
   ,GetSessionbyId:function(Self, Identifier$5) {
      var Result = null;
      TW3ErrorObject.ClearLastError(Self);
      Identifier$5 = Trim$_String_((Identifier$5).toLocaleLowerCase());
      if (Identifier$5.length>0) {
         if (TW3CustomDictionary.Contains(Self.FLookup,Identifier$5)) {
            Result = $As(TW3ObjDictionary.a$108(Self.FLookup,Identifier$5),TUserSession);
         } else {
            TW3ErrorObject.SetLastErrorF(Self,"Uknown session [%s] error",[Identifier$5]);
         }
      } else {
         TW3ErrorObject.SetLastError$(Self,"Invalid session-id error");
      }
      return Result
   }
   /// function TUserSessionManager.GetSessionByIP(RemoteIP: String) : TUserSession
   ,GetSessionByIP:function(Self, RemoteIP$1) {
      var Result = null;
      RemoteIP$1 = (Trim$_String_(RemoteIP$1)).toLocaleLowerCase();
      if (RemoteIP$1.length>0) {
         if (TW3CustomDictionary.Contains(Self.FIPLookup,RemoteIP$1)) {
            Result = $As(TW3ObjDictionary.a$108(Self.FIPLookup,RemoteIP$1),TUserSession);
         } else {
            Result = null;
         }
      }
      return Result
   }
   /// procedure TUserSessionManager.PrivilegeGranted(Session: TUserSession; Privilege: TWbAccountPrivilege)
   ,PrivilegeGranted:function(Self, Session$7, Privilege) {
      if (!TUserSession.GetState$1(Session$7)) {
         if (Self.OnPrivilegeGranted) {
            Self.OnPrivilegeGranted(Self,Session$7,Privilege);
         }
      }
   }
   /// procedure TUserSessionManager.PrivilegeRevoked(Session: TUserSession; Privilege: TWbAccountPrivilege)
   ,PrivilegeRevoked:function(Self, Session$8, Privilege$1) {
      if (!TUserSession.GetState$1(Session$8)) {
         if (Self.OnPrivilegeRevoked) {
            Self.OnPrivilegeRevoked(Self,Session$8,Privilege$1);
         }
      }
   }
   /// procedure TUserSessionManager.SessionAdded(const Session: TUserSession)
   ,SessionAdded:function(Self, Session$9) {
      if (Self.OnSessionOpened) {
         Self.OnSessionOpened(Self,Session$9);
      }
   }
   /// procedure TUserSessionManager.SessionDeleted(const Session: TUserSession)
   ,SessionDeleted:function(Self, Session$10) {
      var x$59 = 0;
      var id$3 = "",
         LRemoteIP$1 = "";
      if (Session$10!==null) {
         var $temp103;
         for(x$59=0,$temp103=Self.FSessions.length;x$59<$temp103;x$59++) {
            if (Self.FSessions[x$59]===Session$10) {
               Self.FSessions.splice(x$59,1)
               ;
               break;
            }
         }
         id$3 = (TUserSession.GetIdentifier(Session$10)).toLocaleLowerCase();
         if (TW3CustomDictionary.Contains(Self.FLookup,id$3)) {
            TW3CustomDictionary.Delete$2$(Self.FLookup,id$3);
         }
         LRemoteIP$1 = (Trim$_String_(Session$10.FRemoteIP)).toLocaleLowerCase();
         if (TW3CustomDictionary.Contains(Self.FIPLookup,LRemoteIP$1)) {
            TW3CustomDictionary.Delete$2$(Self.FIPLookup,LRemoteIP$1);
         }
         if (Self.OnSessionClosed) {
            Self.OnSessionClosed(Self,Session$10);
         }
      }
   }
   /// procedure TUserSessionManager.SessionRenamed(const Session: TUserSession; OldName: String; NewName: String)
   ,SessionRenamed:function(Self, Session$11, OldName, NewName$1) {
      TW3ErrorObject.ClearLastError(Self);
      if (Session$11!==null) {
         OldName = Trim$_String_((OldName).toLocaleLowerCase());
         if (TW3CustomDictionary.Contains(Self.FLookup,OldName)) {
            TW3CustomDictionary.Delete$2$(Self.FLookup,OldName);
         }
         NewName$1 = Trim$_String_((NewName$1).toLocaleLowerCase());
         if (NewName$1.length>0) {
            TW3ObjDictionary.a$107(Self.FLookup,NewName$1,Session$11);
         } else {
            TW3ErrorObject.SetLastErrorF(Self,"Failed to rename session [%s], new identifier was empty error",[OldName]);
         }
      } else {
         TW3ErrorObject.SetLastErrorF(Self,"Failed to rename session [%s], object was NIL error",[OldName]);
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3$:function($){return $.ClassType.Create$3($)}
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,Clear$16$:function($){return $.ClassType.Clear$16($)}
};
TUserSessionManager.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TUserSessionData = record
function Copy$TUserSessionData(s,d) {
   d.Identifier=s.Identifier;
   d.LoginTime=s.LoginTime;
   d.Password$1=s.Password$1;
   d.Privileges=s.Privileges.slice(0);
   d.RootFolder=s.RootFolder;
   d.State$2=s.State$2;
   d.Username=s.Username;
   d.Values$6=s.Values$6;
   return d;
}
function Clone$TUserSessionData($) {
   return {
      Identifier:$.Identifier,
      LoginTime:$.LoginTime,
      Password$1:$.Password$1,
      Privileges:$.Privileges.slice(0),
      RootFolder:$.RootFolder,
      State$2:$.State$2,
      Username:$.Username,
      Values$6:$.Values$6
   }
}
/// TUserSession = class (TW3OwnedErrorObject)
var TUserSession = {
   $ClassName:"TUserSession",$Parent:TW3OwnedErrorObject
   ,$Init:function ($) {
      TW3OwnedErrorObject.$Init($);
      $.FData$2 = {Identifier:"",LoginTime:undefined,Password$1:"",Privileges:[0],RootFolder:"",State$2:0,Username:"",Values$6:undefined};
      $.FRemoteIP = "";
      $.FValues = null;
   }
   /// anonymous TSourceMethodSymbol
   ,a$421:function(Self, Value$166) {
      Self.FData$2.RootFolder = Value$166;
   }
   /// anonymous TSourceMethodSymbol
   ,a$420:function(Self) {
      return Self.FData$2.RootFolder;
   }
   /// anonymous TSourceMethodSymbol
   ,a$419:function(Self, Value$167) {
      Self.FData$2.LoginTime = Value$167;
   }
   /// anonymous TSourceMethodSymbol
   ,a$418:function(Self) {
      return Self.FData$2.LoginTime;
   }
   /// anonymous TSourceMethodSymbol
   ,a$417:function(Self, Value$168) {
      Self.FData$2.Password$1 = Value$168;
   }
   /// anonymous TSourceMethodSymbol
   ,a$416:function(Self) {
      return Self.FData$2.Password$1;
   }
   /// anonymous TSourceMethodSymbol
   ,a$415:function(Self, Value$169) {
      Self.FData$2.Username = Value$169;
   }
   /// anonymous TSourceMethodSymbol
   ,a$414:function(Self) {
      return Self.FData$2.Username;
   }
   /// function TUserSession.AcceptOwner(const CandidateObject: TObject) : Boolean
   ,AcceptOwner:function(Self, CandidateObject$6) {
      return CandidateObject$6!==null&&$Is(CandidateObject$6,TUserSessionManager);
   }
   /// constructor TUserSession.Create(Manager: TUserSessionManager; UserIP: String)
   ,Create$96:function(Self, Manager, UserIP) {
      TW3OwnedErrorObject.Create$16(Self,Manager);
      Self.FValues = TJSONStructure.Create$65$($New(TJSONStructure));
      TUserSession.Reset$5$(Self);
      Self.FRemoteIP = Trim$_String_(UserIP);
      return Self
   }
   /// destructor TUserSession.Destroy()
   ,Destroy:function(Self) {
      if (TUserSession.GetOwner$8(Self)!==null) {
         TUserSessionManager.SessionDeleted(TUserSession.GetOwner$8(Self),Self);
      }
      TObject.Free(Self.FValues);
      TW3OwnedErrorObject.Destroy(Self);
   }
   /// procedure TUserSession.FromJSON(const Data: String)
   ,FromJSON$2:function(Self, Data$76) {
      var LValues = {Identifier:"",LoginTime:undefined,Password$1:"",Privileges:[0],RootFolder:"",State$2:0,Username:"",Values$6:undefined};
      LValues = JSON.parse(Data$76);
      Copy$TUserSessionData(LValues,Self.FData$2);
      TJSONStructure.FromJSon(Self.FValues,JSON.stringify(Self.FData$2.Values$6));
      Self.FData$2.Values$6 = undefined;
   }
   /// function TUserSession.GetIdentifier() : String
   ,GetIdentifier:function(Self) {
      return Self.FData$2.Identifier;
   }
   /// function TUserSession.GetOwner() : TUserSessionManager
   ,GetOwner$8:function(Self) {
      return $As(TW3OwnedObject.GetOwner(Self),TUserSessionManager);
   }
   /// function TUserSession.GetPrivileges() : TWbAccountPrivileges
   ,GetPrivileges:function(Self) {
      return Self.FData$2.Privileges.slice(0);
   }
   /// function TUserSession.GetRootFolder() : String
   ,GetRootFolder:function(Self) {
      return Self.FData$2.RootFolder;
   }
   /// function TUserSession.GetState() : TWbAccountState
   ,GetState$1:function(Self) {
      return Self.FData$2.State$2;
   }
   /// function TUserSession.GetValues() : IW3Structure
   ,GetValues$2:function(Self) {
      return $AsIntf(Self.FValues,"IW3Structure");
   }
   /// procedure TUserSession.Reset()
   ,Reset$5:function(Self) {
      Self.FData$2.Privileges = [0];
      Self.FData$2.State$2 = 2;
      Self.FData$2.Identifier = "Session"+TW3Identifiers.GenerateUniqueNumber(TW3Identifiers).toString();
      Self.FData$2.LoginTime = Now();
      Self.FData$2.Username = "";
      Self.FData$2.Password$1 = "";
   }
   /// procedure TUserSession.SetIdentifier(NewIdentifier: String)
   ,SetIdentifier:function(Self, NewIdentifier) {
      var old = "";
      NewIdentifier = Trim$_String_(NewIdentifier);
      if (NewIdentifier!=Self.FData$2.Identifier) {
         old = Self.FData$2.Identifier;
         Self.FData$2.Identifier = NewIdentifier;
      }
   }
   /// procedure TUserSession.SetPrivileges(const NewPrivileges: TWbAccountPrivileges)
   ,SetPrivileges:function(Self, NewPrivileges) {
      if ($SetIn(NewPrivileges,0,0,6)&&(!$SetIn(TUserSession.GetPrivileges(Self),0,0,6))) {
         $SetInc(Self.FData$2.Privileges,0,0,6);
         TUserSessionManager.PrivilegeGranted(TUserSession.GetOwner$8(Self),Self,0);
      }
      if ($SetIn(TUserSession.GetPrivileges(Self),0,0,6)&&(!$SetIn(NewPrivileges,0,0,6))) {
         $SetExc(Self.FData$2.Privileges,0,0,6);
         TUserSessionManager.PrivilegeRevoked(TUserSession.GetOwner$8(Self),Self,0);
      }
      if ($SetIn(NewPrivileges,1,0,6)&&(!$SetIn(TUserSession.GetPrivileges(Self),1,0,6))) {
         $SetInc(Self.FData$2.Privileges,1,0,6);
         TUserSessionManager.PrivilegeGranted(TUserSession.GetOwner$8(Self),Self,1);
      }
      if ($SetIn(TUserSession.GetPrivileges(Self),1,0,6)&&(!$SetIn(NewPrivileges,1,0,6))) {
         $SetExc(Self.FData$2.Privileges,1,0,6);
         TUserSessionManager.PrivilegeRevoked(TUserSession.GetOwner$8(Self),Self,1);
      }
      if ($SetIn(NewPrivileges,2,0,6)&&(!$SetIn(TUserSession.GetPrivileges(Self),2,0,6))) {
         $SetInc(Self.FData$2.Privileges,2,0,6);
         TUserSessionManager.PrivilegeGranted(TUserSession.GetOwner$8(Self),Self,2);
      }
      if ($SetIn(TUserSession.GetPrivileges(Self),2,0,6)&&(!$SetIn(NewPrivileges,2,0,6))) {
         $SetExc(Self.FData$2.Privileges,2,0,6);
         TUserSessionManager.PrivilegeRevoked(TUserSession.GetOwner$8(Self),Self,2);
      }
      if ($SetIn(NewPrivileges,3,0,6)&&(!$SetIn(TUserSession.GetPrivileges(Self),3,0,6))) {
         $SetInc(Self.FData$2.Privileges,3,0,6);
         TUserSessionManager.PrivilegeGranted(TUserSession.GetOwner$8(Self),Self,3);
      }
      if ($SetIn(TUserSession.GetPrivileges(Self),3,0,6)&&(!$SetIn(NewPrivileges,3,0,6))) {
         $SetExc(Self.FData$2.Privileges,3,0,6);
         TUserSessionManager.PrivilegeRevoked(TUserSession.GetOwner$8(Self),Self,3);
      }
      if ($SetIn(NewPrivileges,4,0,6)&&(!$SetIn(TUserSession.GetPrivileges(Self),4,0,6))) {
         $SetInc(Self.FData$2.Privileges,4,0,6);
         TUserSessionManager.PrivilegeGranted(TUserSession.GetOwner$8(Self),Self,4);
      }
      if ($SetIn(TUserSession.GetPrivileges(Self),4,0,6)&&(!$SetIn(NewPrivileges,4,0,6))) {
         $SetExc(Self.FData$2.Privileges,4,0,6);
         TUserSessionManager.PrivilegeRevoked(TUserSession.GetOwner$8(Self),Self,4);
      }
      if ($SetIn(NewPrivileges,5,0,6)&&(!$SetIn(TUserSession.GetPrivileges(Self),5,0,6))) {
         $SetInc(Self.FData$2.Privileges,5,0,6);
         TUserSessionManager.PrivilegeGranted(TUserSession.GetOwner$8(Self),Self,5);
      }
      if ($SetIn(TUserSession.GetPrivileges(Self),5,0,6)&&(!$SetIn(NewPrivileges,5,0,6))) {
         $SetExc(Self.FData$2.Privileges,5,0,6);
         TUserSessionManager.PrivilegeRevoked(TUserSession.GetOwner$8(Self),Self,5);
      }
   }
   /// procedure TUserSession.SetRootFolder(const NewRootFolder: String)
   ,SetRootFolder:function(Self, NewRootFolder) {
      Self.FData$2.RootFolder = NewRootFolder;
   }
   /// procedure TUserSession.SetState(const NewState: TWbAccountState)
   ,SetState$2:function(Self, NewState$5) {
      if (NewState$5!=Self.FData$2.State$2) {
         Self.FData$2.State$2 = NewState$5;
      }
   }
   /// function TUserSession.ToJSON() : String
   ,ToJSON$4:function(Self) {
      var Result = "";
      Self.FData$2.Values$6 = TJSONStructure.ToJSonObject(Self.FValues);
      Result = JSON.stringify(Self.FData$2);
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,SetLastError$1:TW3OwnedErrorObject.SetLastError$1
   ,Create$96$:function($){return $.ClassType.Create$96.apply($.ClassType, arguments)}
   ,FromJSON$2$:function($){return $.ClassType.FromJSON$2.apply($.ClassType, arguments)}
   ,Reset$5$:function($){return $.ClassType.Reset$5($)}
   ,ToJSON$4$:function($){return $.ClassType.ToJSON$4($)}
};
TUserSession.$Intf={
   IW3OwnedObjectAccess:[TUserSession.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IUserSession:[TUserSession.GetOwner$8,TUserSession.GetValues$2,TUserSession.GetIdentifier,TUserSession.SetIdentifier,TUserSession.GetPrivileges,TUserSession.SetPrivileges,TUserSession.GetState$1,TUserSession.SetState$2,TUserSession.GetRootFolder,TUserSession.SetRootFolder,TUserSession.Reset$5,TUserSession.ToJSON$4,TUserSession.FromJSON$2]
}
function SessionManager() {
   var Result = null;
   if (__SessionManager===null) {
      __SessionManager = TW3ErrorObject.Create$3$($New(TUserSessionManager));
   }
   Result = __SessionManager;
   return Result
};
/// TNJWebSocketSocket = class (TW3ErrorObject)
var TNJWebSocketSocket = {
   $ClassName:"TNJWebSocketSocket",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.TagData = undefined;
      $.RemoteAddress = "";
      $.FReq = $.FServer$1 = $.FSocket = null;
   }
   /// anonymous TSourceMethodSymbol
   ,a$180:function(Self) {
      return Self.FSocket;
   }
   /// procedure TNJWebSocketSocket.Close(Code: Integer; Reason: String)
   ,Close$4:function(Self, Code$1, Reason) {
      if (TNJWebSocketSocket.GetReadyState$(Self)==1) {
         Self.FSocket.close(Code$1,Reason);
      }
   }
   /// procedure TNJWebSocketSocket.Close(Code: Integer)
   ,Close$3:function(Self, Code$2) {
      if (TNJWebSocketSocket.GetReadyState$(Self)==1) {
         Self.FSocket.close(Code$2);
      }
   }
   /// procedure TNJWebSocketSocket.Close()
   ,Close$2:function(Self) {
      if (TNJWebSocketSocket.GetReadyState$(Self)!=3) {
         Self.FSocket.terminate();
      }
   }
   /// constructor TNJWebSocketSocket.Create(const Server: TNJRawWebSocketServer; const WsSocket: JWsSocket)
   ,Create$83:function(Self, Server$5, WsSocket) {
      TW3ErrorObject.Create$3(Self);
      if (Server$5!==null) {
         Self.FServer$1 = Server$5;
         if (WsSocket!==null) {
            Self.FSocket = WsSocket;
            Self.FOptions.AutoWriteToConsole = true;
            Self.FOptions.ThrowExceptions = true;
            Self.FSocket["name"] = TW3Identifiers.GenerateUniqueObjectId(TW3Identifiers);
         } else {
            throw Exception.Create($New(Exception),"Failed to create socket, socket was nil error");
         }
      } else {
         throw Exception.Create($New(Exception),"Failed to create socket, server was nil error");
      }
      return Self
   }
   /// destructor TNJWebSocketSocket.Destroy()
   ,Destroy:function(Self) {
      if (Self.FReq!==null) {
         TObject.Free(Self.FReq);
      }
      Self.FServer$1 = null;
      Self.FSocket = null;
      TW3ErrorObject.Destroy(Self);
   }
   /// function TNJWebSocketSocket.GetBinaryType() : String
   ,GetBinaryType:function(Self) {
      return TVariant.AsString(Self.FSocket.binaryType);
   }
   /// function TNJWebSocketSocket.GetBufferedAmount() : Integer
   ,GetBufferedAmount:function(Self) {
      return TVariant.AsInteger(Self.FSocket.bufferedAmount);
   }
   /// function TNJWebSocketSocket.GetExceptionClass() : TW3ExceptionClass
   ,GetExceptionClass:function(Self) {
      return ENJWebSocketSocket;
   }
   /// function TNJWebSocketSocket.GetReadyState() : JWsReadyState
   ,GetReadyState:function(Self) {
      return (Self.FSocket)?Self.FSocket.readyState:3;
   }
   /// function TNJWebSocketSocket.GetSocketName() : String
   ,GetSocketName:function(Self) {
      return String(Self.FSocket["name"]);
   }
   /// function TNJWebSocketSocket.GetSocketRequest() : TNJHttpRequest
   ,GetSocketRequest:function(Self) {
      var Result = null;
      if (Self.FReq===null) {
         if (Self.FSocket.req!==null) {
            Self.FReq = TNJHttpRequest.Create$88$($New(TNJHttpRequest),Self.FServer$1,Self.FSocket.req);
         }
      }
      Result = Self.FReq;
      return Result
   }
   /// procedure TNJWebSocketSocket.Send(const Data: TStream)
   ,Send$2:function(Self, Data$77) {
      var Bytes$12 = [],
         TypedArray = undefined;
      if (Data$77!==null) {
         if (TStream.GetPosition$(Data$77)<TStream.GetSize$(Data$77)) {
            Bytes$12 = TStream.Read$1$(Data$77,(TStream.GetSize$(Data$77)-TStream.GetPosition$(Data$77)));
            TypedArray = TDatatype.BytesToTypedArray$1(TDatatype,Bytes$12);
            if (Self.FSocket!==null) {
               Self.FSocket.send(TypedArray);
            }
         }
      }
   }
   /// procedure TNJWebSocketSocket.Send(const Text: String)
   ,Send$1:function(Self, Text$39) {
      if (Self.FSocket!==null) {
         Self.FSocket.send(Text$39);
      }
   }
   /// procedure TNJWebSocketSocket.Send(const Data: Variant)
   ,Send:function(Self, Data$78) {
      if (Self.FSocket!==null) {
         Self.FSocket.send(Data$78);
      }
   }
   /// procedure TNJWebSocketSocket.SetSocketName(NewName: String)
   ,SetSocketName:function(Self, NewName$2) {
      var OldName$1 = "";
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      NewName$2 = Trim$_String_(NewName$2);
      OldName$1 = TNJWebSocketSocket.GetSocketName$(Self);
      if (NewName$2!=OldName$1) {
         if (NewName$2.length>0) {
            Self.FSocket["name"] = Trim$_String_(NewName$2);
            TNJRawWebSocketServer.DoClientNameChange$(Self.FServer$1,Self,OldName$1);
         } else {
            TW3ErrorObject.SetLastError$(Self,"Invalid socket rename, string was empty error");
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3:TW3ErrorObject.Create$3
   ,GetExceptionClass$:function($){return $.ClassType.GetExceptionClass($)}
   ,SetLastError:TW3ErrorObject.SetLastError
   ,Create$83$:function($){return $.ClassType.Create$83.apply($.ClassType, arguments)}
   ,GetBinaryType$:function($){return $.ClassType.GetBinaryType($)}
   ,GetBufferedAmount$:function($){return $.ClassType.GetBufferedAmount($)}
   ,GetReadyState$:function($){return $.ClassType.GetReadyState($)}
   ,GetSocketName$:function($){return $.ClassType.GetSocketName($)}
   ,GetSocketRequest$:function($){return $.ClassType.GetSocketRequest($)}
   ,SetSocketName$:function($){return $.ClassType.SetSocketName.apply($.ClassType, arguments)}
};
TNJWebSocketSocket.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TNJCustomServer = class (TW3ErrorObject)
var TNJCustomServer = {
   $ClassName:"TNJCustomServer",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.OnBeforeServerStopped = null;
      $.OnBeforeServerStarted = null;
      $.OnAfterServerStopped = null;
      $.OnAfterServerStarted = null;
      $.FActive$3 = false;
      $.FHandle$6 = undefined;
      $.FPort = 0;
   }
   /// procedure TNJCustomServer.AfterStart()
   ,AfterStart:function(Self) {
      if (Self.OnAfterServerStarted) {
         Self.OnAfterServerStarted(Self);
      }
   }
   /// procedure TNJCustomServer.AfterStop()
   ,AfterStop:function(Self) {
      if (Self.OnAfterServerStopped) {
         Self.OnAfterServerStopped(Self);
      }
   }
   /// procedure TNJCustomServer.BeforeStart()
   ,BeforeStart:function(Self) {
      if (Self.OnBeforeServerStarted) {
         Self.OnBeforeServerStarted(Self);
      }
   }
   /// procedure TNJCustomServer.BeforeStop()
   ,BeforeStop:function(Self) {
      if (Self.OnBeforeServerStopped) {
         Self.OnBeforeServerStopped(Self);
      }
   }
   /// function TNJCustomServer.GetActive() : Boolean
   ,GetActive$2:function(Self) {
      return Self.FActive$3;
   }
   /// function TNJCustomServer.GetHandle() : THandle
   ,GetHandle$1:function(Self) {
      return Self.FHandle$6;
   }
   /// function TNJCustomServer.GetPort() : Integer
   ,GetPort:function(Self) {
      return Self.FPort;
   }
   /// procedure TNJCustomServer.SetActive(const Value: Boolean)
   ,SetActive$3:function(Self, Value$170) {
      Self.FActive$3 = Value$170;
   }
   /// procedure TNJCustomServer.SetHandle(const Value: THandle)
   ,SetHandle$2:function(Self, Value$171) {
      Self.FHandle$6 = Value$171;
   }
   /// procedure TNJCustomServer.SetPort(const Value: Integer)
   ,SetPort:function(Self, Value$172) {
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TNJCustomServer.GetActive$2$(Self)) {
         TW3ErrorObject.SetLastError$(Self,"Port cannot be altered while server is active error");
      } else {
         Self.FPort = Value$172;
      }
   }
   /// procedure TNJCustomServer.Start()
   ,Start:function(Self) {
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TNJCustomServer.GetActive$2$(Self)) {
         TW3ErrorObject.SetLastError$(Self,"Server already active error");
      } else {
         TNJCustomServer.BeforeStart$(Self);
         TNJCustomServer.StartServer$(Self);
      }
   }
   /// procedure TNJCustomServer.StartServer()
   ,StartServer:function(Self) {
      TNJCustomServer.SetActive$3$(Self,true);
   }
   /// procedure TNJCustomServer.Stop()
   ,Stop:function(Self) {
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TNJCustomServer.GetActive$2$(Self)) {
         TNJCustomServer.BeforeStop$(Self);
         TNJCustomServer.StopServer$(Self);
      } else {
         TW3ErrorObject.SetLastError$(Self,"Server not started error");
      }
   }
   /// procedure TNJCustomServer.StopServer()
   ,StopServer:function(Self) {
      TNJCustomServer.SetActive$3$(Self,false);
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$3:TW3ErrorObject.Create$3
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,AfterStart$:function($){return $.ClassType.AfterStart($)}
   ,AfterStop$:function($){return $.ClassType.AfterStop($)}
   ,BeforeStart$:function($){return $.ClassType.BeforeStart($)}
   ,BeforeStop$:function($){return $.ClassType.BeforeStop($)}
   ,GetActive$2$:function($){return $.ClassType.GetActive$2($)}
   ,GetHandle$1$:function($){return $.ClassType.GetHandle$1($)}
   ,GetPort$:function($){return $.ClassType.GetPort($)}
   ,SetActive$3$:function($){return $.ClassType.SetActive$3.apply($.ClassType, arguments)}
   ,SetHandle$2$:function($){return $.ClassType.SetHandle$2.apply($.ClassType, arguments)}
   ,SetPort$:function($){return $.ClassType.SetPort.apply($.ClassType, arguments)}
   ,Start$:function($){return $.ClassType.Start($)}
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,Stop$:function($){return $.ClassType.Stop($)}
   ,StopServer$:function($){return $.ClassType.StopServer($)}
};
TNJCustomServer.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TNJRawWebSocketServer = class (TNJCustomServer)
var TNJRawWebSocketServer = {
   $ClassName:"TNJRawWebSocketServer",$Parent:TNJCustomServer
   ,$Init:function ($) {
      TNJCustomServer.$Init($);
      $.OnBinMessage = null;
      $.OnTextMessage = null;
      $.OnClientDisconnected = null;
      $.OnClientConnected = null;
      $.OnError = null;
      $.FPath = "";
      $.FPayload = 0;
      $.FSocketLUT = null;
      $.FSockets = [];
      $.FTrack = false;
   }
   /// procedure TNJRawWebSocketServer.ClearLookupTable()
   ,ClearLookupTable:function(Self) {
      TW3CustomDictionary.Clear$7(Self.FSocketLUT);
   }
   /// constructor TNJRawWebSocketServer.Create()
   ,Create$3:function(Self) {
      TW3ErrorObject.Create$3(Self);
      Self.FSocketLUT = TW3CustomDictionary.Create$74$($New(TW3ObjDictionary));
      Self.FOptions.AutoWriteToConsole = true;
      Self.FOptions.ThrowExceptions = true;
      Self.FPayload = 41943040;
      return Self
   }
   /// destructor TNJRawWebSocketServer.Destroy()
   ,Destroy:function(Self) {
      TObject.Free(Self.FSocketLUT);
      TW3ErrorObject.Destroy(Self);
   }
   /// procedure TNJRawWebSocketServer.Dispatch(Socket: TNJWebSocketSocket; Data: TNJWebsocketMessage)
   ,Dispatch$1:function(Self, Socket$14, Data$79) {
      switch (Data$79.wiType) {
         case 0 :
            if (Self.OnTextMessage) {
               Self.OnTextMessage(Self,Socket$14,Clone$TNJWebsocketMessage(Data$79));
            }
            break;
         case 1 :
            if (Self.OnBinMessage) {
               Self.OnBinMessage(Self,Socket$14,Clone$TNJWebsocketMessage(Data$79));
            }
            break;
      }
   }
   /// procedure TNJRawWebSocketServer.DoClientConnected(Socket: THandle; Request: THandle)
   ,DoClientConnected:function(Self, Socket$15, Request$7) {
      var NJSocket = null;
      NJSocket = TNJWebSocketSocket.Create$83$($New(TNJWebSocketSocket),Self,Socket$15);
      TW3ObjDictionary.a$107(Self.FSocketLUT,TNJWebSocketSocket.GetSocketName$(NJSocket),NJSocket);
      Self.FSockets.push(NJSocket);
      if (Request$7) {
         if (Request$7.connection) {
            NJSocket.RemoteAddress = String(Request$7.connection.remoteAddress);
         }
      }
      if (Self.OnClientConnected) {
         Self.OnClientConnected(Self,NJSocket);
      }
      Socket$15.on("error",function (error$7) {
         var NJSocket$1 = null,
            SocketName;
         if (Self.OnError) {
            NJSocket$1 = null;
            SocketName = Socket$15["name"];
            if (TW3CustomDictionary.Contains(Self.FSocketLUT,(String(SocketName)))) {
               NJSocket$1 = $As(TW3ObjDictionary.a$108(Self.FSocketLUT,(String(SocketName))),TNJWebSocketSocket);
            } else {
               NJSocket$1 = TNJWebSocketSocket.Create$83$($New(TNJWebSocketSocket),Self,Socket$15);
            }
            Self.OnError(Self,NJSocket$1,error$7);
         }
      });
      Socket$15.on("message",function (message$1) {
         var LInfo$2 = {wiType:0,wiBuffer:null,wiText:""};
         var NJSocket$2 = null,
            SocketName$1;
         NJSocket$2 = null;
         if (TW3VariantHelper$IsUInt8Array(message$1)) {
            LInfo$2.wiType = 1;
            LInfo$2.wiBuffer = message$1;
         } else {
            LInfo$2.wiType = 0;
            LInfo$2.wiText = String(message$1);
         }
         SocketName$1 = Socket$15["name"];
         if (TW3CustomDictionary.Contains(Self.FSocketLUT,(String(SocketName$1)))) {
            NJSocket$2 = $As(TW3ObjDictionary.a$108(Self.FSocketLUT,(String(SocketName$1))),TNJWebSocketSocket);
         } else {
            NJSocket$2 = TNJWebSocketSocket.Create$83$($New(TNJWebSocketSocket),Self,Socket$15);
         }
         TNJRawWebSocketServer.Dispatch$1$(Self,NJSocket$2,Clone$TNJWebsocketMessage(LInfo$2));
      });
      Socket$15.on("close",function (code$1, reason) {
         TNJRawWebSocketServer.DoClientDisconnected$(Self,Socket$15,code$1,reason);
      });
   }
   /// procedure TNJRawWebSocketServer.DoClientDisconnected(Socket: THandle; Code: Integer; Reason: String)
   ,DoClientDisconnected:function(Self, Socket$16, Code$3, Reason$1) {
      var RawSocket$1 = null,
         SocketId = "",
         NJSocket$3 = null,
         index$13 = 0;
      RawSocket$1 = Socket$16;
      SocketId = TVariant.AsString(Socket$16["name"]);
      NJSocket$3 = null;
      if (SocketId.length>0) {
         if (TW3CustomDictionary.Contains(Self.FSocketLUT,SocketId)) {
            WriteLn("Found in name lookup!");
            NJSocket$3 = $As(TW3ObjDictionary.a$108(Self.FSocketLUT,SocketId),TNJWebSocketSocket);
         }
      }
      try {
         if (Self.OnClientDisconnected) {
            Self.OnClientDisconnected(Self,NJSocket$3);
         }
      } finally {
         if (NJSocket$3!==null) {
            index$13 = Self.FSockets.indexOf(NJSocket$3);
            Self.FSockets.splice(index$13,1)
            ;
            TW3CustomDictionary.Delete$2$(Self.FSocketLUT,SocketId);
            TObject.Free(NJSocket$3);
         }
      }
   }
   /// procedure TNJRawWebSocketServer.DoClientNameChange(Client: TNJWebSocketSocket; OldName: String)
   ,DoClientNameChange:function(Self, Client, OldName$2) {
      var NewName$3 = "";
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      NewName$3 = Trim$_String_(TNJWebSocketSocket.GetSocketName$(Client));
      if (NewName$3.length>0) {
         if (TW3CustomDictionary.Contains(Self.FSocketLUT,NewName$3)) {
            TW3ErrorObject.SetLastErrorF(Self,"Invalid socket rename, a socket with that name [%s] already exists",[NewName$3]);
            return;
         }
         OldName$2 = Trim$_String_(OldName$2);
         if (OldName$2.length>0) {
            if (TW3CustomDictionary.Contains(Self.FSocketLUT,OldName$2)) {
               TW3CustomDictionary.Delete$2$(Self.FSocketLUT,OldName$2);
            }
         }
         TW3ObjDictionary.a$107(Self.FSocketLUT,TNJWebSocketSocket.GetSocketName$(Client),Client);
      } else {
         TW3ErrorObject.SetLastError$(Self,"Invalid socket rename, string was empty errror");
      }
   }
   /// procedure TNJRawWebSocketServer.Emit(EventName: String; Data: Variant; Attempts: Integer)
   ,Emit:function(Self, EventName, Data$80, Attempts) {
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TNJCustomServer.GetActive$2$(Self)) {
         TNJCustomServer.GetHandle$1$(Self).emit(EventName,Data$80,Attempts);
      } else {
         TW3ErrorObject.SetLastError$(Self,"Failed to issue emit(), server is not active error");
      }
   }
   /// function TNJRawWebSocketServer.GetClient(const Index: Integer) : TNJWebSocketSocket
   ,GetClient:function(Self, Index$11) {
      var Result = null;
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TNJCustomServer.GetActive$2$(Self)) {
         if (Index$11>=0&&Index$11<Self.FSockets.length) {
            Result = Self.FSockets[Index$11];
         }
      } else {
         TW3ErrorObject.SetLastError$(Self,"GetClient failed, server is not active error");
      }
      return Result
   }
   /// function TNJRawWebSocketServer.GetClientByName(Name: String; var Client: TNJWebSocketSocket) : Boolean
   ,GetClientByName:function(Self, Name$45, Client$1) {
      var Result = false;
      var LTemp$22 = null;
      Client$1.v = null;
      if (TNJCustomServer.GetActive$2$(Self)) {
         if (TW3CustomDictionary.Contains(Self.FSocketLUT,Name$45)) {
            LTemp$22 = TW3ObjDictionary.a$108(Self.FSocketLUT,Name$45);
            Result = LTemp$22!==null;
            if (Result) {
               Client$1.v = $As(LTemp$22,TNJWebSocketSocket);
            }
         }
      }
      return Result
   }
   /// function TNJRawWebSocketServer.GetCount() : Integer
   ,GetCount:function(Self) {
      var Result = 0;
      var LHandle = undefined;
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TNJCustomServer.GetActive$2$(Self)) {
         LHandle = TNJCustomServer.GetHandle$1$(Self);
         if (LHandle.clients) {
            Result = parseInt(LHandle.clients.size,10);
         } else {
            TW3ErrorObject.SetLastError$(Self,"Client tracking not enabled, client-count not available error");
         }
      } else {
         Result = -1;
      }
      return Result
   }
   /// function TNJRawWebSocketServer.GetExceptionClass() : TW3ExceptionClass
   ,GetExceptionClass:function(Self) {
      return ENJWebSocketServer;
   }
   /// procedure TNJRawWebSocketServer.InternalSetActive(const Value: Boolean)
   ,InternalSetActive:function(Self, Value$173) {
      TNJCustomServer.SetActive$3(Self,Value$173);
   }
   /// procedure TNJRawWebSocketServer.On(EventName: String; CallBack: TNJWebSocketOnHandler)
   ,On:function(Self, EventName$1, CallBack$1) {
      var LHandle$1 = undefined;
      LHandle$1 = TNJCustomServer.GetHandle$1$(Self);
      if (LHandle$1) {
         LHandle$1.on(EventName$1,CallBack$1);
      }
   }
   /// procedure TNJRawWebSocketServer.SetActive(const Value: Boolean)
   ,SetActive$3:function(Self, Value$174) {
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (Value$174!=TNJCustomServer.GetActive$2$(Self)) {
         TNJCustomServer.SetActive$3(Self,Value$174);
         try {
            if (TNJCustomServer.GetActive$2$(Self)) {
               TNJCustomServer.StartServer$(Self);
            } else {
               TNJCustomServer.StopServer$(Self);
            }
         } catch ($e) {
            var e$53 = $W($e);
            TNJCustomServer.SetActive$3(Self,(!Value$174));
            TW3ErrorObject.SetLastErrorF(Self,"Failed to start server, system threw exception %s with message [%s]",[TObject.ClassName(e$53.ClassType), e$53.FMessage]);
         }
      }
   }
   /// procedure TNJRawWebSocketServer.SetPath(URLPath: String)
   ,SetPath:function(Self, URLPath) {
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TNJCustomServer.GetActive$2$(Self)) {
         TW3ErrorObject.SetLastError$(Self,"Failed to set listen path, server is active error");
      } else {
         Self.FPath = (Trim$_String_(URLPath)).toLocaleLowerCase();
      }
   }
   /// procedure TNJRawWebSocketServer.SetPayload(NewPayload: Integer)
   ,SetPayload:function(Self, NewPayload) {
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TNJCustomServer.GetActive$2$(Self)) {
         TW3ErrorObject.SetLastError$(Self,"Failed to set payload, value must be 1024 or above");
      } else {
         Self.FPayload = (NewPayload<1024)?1024:NewPayload;
      }
   }
   /// procedure TNJRawWebSocketServer.SetTracking(Value: Boolean)
   ,SetTracking:function(Self, Value$175) {
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TNJCustomServer.GetActive$2$(Self)) {
         TW3ErrorObject.SetLastError$(Self,"Failed to set client-tracking, server is active error");
      } else {
         Self.FTrack = Value$175;
      }
   }
   /// procedure TNJRawWebSocketServer.StartServer()
   ,StartServer:function(Self) {
      var LOptions$2,
         LServer;
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      TW3CustomDictionary.Clear$7(Self.FSocketLUT);
      LOptions$2 = TVariant.CreateObject();
      LOptions$2["clientTracking "] = Self.FTrack;
      if (Self.FPath.length>0) {
         LOptions$2["path"] = Self.FPath;
      }
      LOptions$2["port"] = TNJCustomServer.GetPort$(Self);
      LOptions$2["maxPayload"] = Self.FPayload;
      LServer = null;
      try {
         LServer = WebSocketCreateServer(LOptions$2);
      } catch ($e) {
         var e$54 = $W($e);
         TW3ErrorObject.SetLastErrorF(Self,"Failed to create websocket server object, system threw exception %s with message [%s]",[TObject.ClassName(e$54.ClassType), e$54.FMessage]);
         return;
      }
      TNJCustomServer.SetHandle$2$(Self,LServer);
      LServer.on("error",function (error$8) {
         if (Self.OnError) {
            Self.OnError(Self,null,error$8);
         }
      });
      LServer.on("connection",$Event2(Self,TNJRawWebSocketServer.DoClientConnected$));
      TNJCustomServer.AfterStart$(Self);
   }
   /// procedure TNJRawWebSocketServer.StopServer()
   ,StopServer:function(Self) {
      var LHandle$2 = undefined;
      LHandle$2 = TNJCustomServer.GetHandle$1$(Self);
      if (LHandle$2) {
         LHandle$2.close(function () {
            TNJRawWebSocketServer.InternalSetActive(Self,false);
            TNJCustomServer.AfterStop$(Self);
            TW3CustomDictionary.Clear$7(Self.FSocketLUT);
         });
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3$:function($){return $.ClassType.Create$3($)}
   ,GetExceptionClass$:function($){return $.ClassType.GetExceptionClass($)}
   ,SetLastError:TW3ErrorObject.SetLastError
   ,AfterStart:TNJCustomServer.AfterStart
   ,AfterStop:TNJCustomServer.AfterStop
   ,BeforeStart:TNJCustomServer.BeforeStart
   ,BeforeStop:TNJCustomServer.BeforeStop
   ,GetActive$2:TNJCustomServer.GetActive$2
   ,GetHandle$1:TNJCustomServer.GetHandle$1
   ,GetPort:TNJCustomServer.GetPort
   ,SetActive$3$:function($){return $.ClassType.SetActive$3.apply($.ClassType, arguments)}
   ,SetHandle$2:TNJCustomServer.SetHandle$2
   ,SetPort:TNJCustomServer.SetPort
   ,Start:TNJCustomServer.Start
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,Stop:TNJCustomServer.Stop
   ,StopServer$:function($){return $.ClassType.StopServer($)}
   ,Dispatch$1$:function($){return $.ClassType.Dispatch$1.apply($.ClassType, arguments)}
   ,DoClientConnected$:function($){return $.ClassType.DoClientConnected.apply($.ClassType, arguments)}
   ,DoClientDisconnected$:function($){return $.ClassType.DoClientDisconnected.apply($.ClassType, arguments)}
   ,DoClientNameChange$:function($){return $.ClassType.DoClientNameChange.apply($.ClassType, arguments)}
   ,SetPath$:function($){return $.ClassType.SetPath.apply($.ClassType, arguments)}
   ,SetTracking$:function($){return $.ClassType.SetTracking.apply($.ClassType, arguments)}
};
TNJRawWebSocketServer.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TNJWebSocketHybridServer = class (TNJRawWebSocketServer)
var TNJWebSocketHybridServer = {
   $ClassName:"TNJWebSocketHybridServer",$Parent:TNJRawWebSocketServer
   ,$Init:function ($) {
      TNJRawWebSocketServer.$Init($);
   }
   ,Destroy:TNJRawWebSocketServer.Destroy
   ,Create$3:TNJRawWebSocketServer.Create$3
   ,GetExceptionClass:TNJRawWebSocketServer.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,AfterStart:TNJCustomServer.AfterStart
   ,AfterStop:TNJCustomServer.AfterStop
   ,BeforeStart:TNJCustomServer.BeforeStart
   ,BeforeStop:TNJCustomServer.BeforeStop
   ,GetActive$2:TNJCustomServer.GetActive$2
   ,GetHandle$1:TNJCustomServer.GetHandle$1
   ,GetPort:TNJCustomServer.GetPort
   ,SetActive$3:TNJRawWebSocketServer.SetActive$3
   ,SetHandle$2:TNJCustomServer.SetHandle$2
   ,SetPort:TNJCustomServer.SetPort
   ,Start:TNJCustomServer.Start
   ,StartServer:TNJRawWebSocketServer.StartServer
   ,Stop:TNJCustomServer.Stop
   ,StopServer:TNJRawWebSocketServer.StopServer
   ,Dispatch$1:TNJRawWebSocketServer.Dispatch$1
   ,DoClientConnected:TNJRawWebSocketServer.DoClientConnected
   ,DoClientDisconnected:TNJRawWebSocketServer.DoClientDisconnected
   ,DoClientNameChange:TNJRawWebSocketServer.DoClientNameChange
   ,SetPath:TNJRawWebSocketServer.SetPath
   ,SetTracking:TNJRawWebSocketServer.SetTracking
   ,GetHttpServer$:function($){return $.ClassType.GetHttpServer($)}
};
TNJWebSocketHybridServer.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TNJWebSocketServer = class (TNJWebSocketHybridServer)
var TNJWebSocketServer = {
   $ClassName:"TNJWebSocketServer",$Parent:TNJWebSocketHybridServer
   ,$Init:function ($) {
      TNJWebSocketHybridServer.$Init($);
      $.FServer$2 = null;
   }
   /// constructor TNJWebSocketServer.Create()
   ,Create$3:function(Self) {
      TNJRawWebSocketServer.Create$3(Self);
      Self.FServer$2 = TW3ErrorObject.Create$3$($New(TNJHTTPServer));
      Self.FOptions.AutoWriteToConsole = true;
      Self.FOptions.ThrowExceptions = true;
      return Self
   }
   /// destructor TNJWebSocketServer.Destroy()
   ,Destroy:function(Self) {
      TObject.Free(Self.FServer$2);
      TNJRawWebSocketServer.Destroy(Self);
   }
   /// function TNJWebSocketServer.GetHttpServer() : TNJCustomServer
   ,GetHttpServer:function(Self) {
      return Self.FServer$2;
   }
   /// function TNJWebSocketServer.GetPort() : Integer
   ,GetPort:function(Self) {
      var Result = 0;
      if (Self.FServer$2!==null) {
         Result = TNJCustomServer.GetPort$(Self.FServer$2);
      }
      return Result
   }
   /// procedure TNJWebSocketServer.SetPort(const Value: Integer)
   ,SetPort:function(Self, Value$176) {
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TNJCustomServer.GetActive$2$(Self)) {
         TW3ErrorObject.SetLastError$(Self,"Port cannot be altered while server is active error");
      } else if (TNJCustomServer.GetPort$(Self.FServer$2)!=Value$176) {
         TNJCustomServer.SetPort$(Self.FServer$2,Value$176);
      }
   }
   /// procedure TNJWebSocketServer.StartServer()
   ,StartServer:function(Self) {
      var LOptions$3,
         LServer$1;
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      TNJRawWebSocketServer.ClearLookupTable(Self);
      TNJCustomServer.SetActive$3$(Self.FServer$2,true);
      LOptions$3 = TVariant.CreateObject();
      LOptions$3["server"] = TNJCustomServer.GetHandle$1$(Self.FServer$2);
      LOptions$3["clientTracking "] = Self.FTrack;
      LOptions$3["maxPayload"] = 41943040;
      if (Self.FPath.length>0) {
         LOptions$3["path"] = Self.FPath;
      }
      LServer$1 = null;
      try {
         LServer$1 = WebSocketCreateServer(LOptions$3);
      } catch ($e) {
         var e$55 = $W($e);
         TW3ErrorObject.SetLastErrorF(Self,"Failed to create websocket server object, system threw exception %s with message [%s]",[TObject.ClassName(e$55.ClassType), e$55.FMessage]);
         return;
      }
      TNJCustomServer.SetHandle$2$(Self,LServer$1);
      LServer$1.on("error",function (error$9) {
         if (Self.OnError) {
            Self.OnError(Self,null,error$9);
         }
      });
      LServer$1.on("connection",$Event2(Self,TNJRawWebSocketServer.DoClientConnected$));
      TNJCustomServer.AfterStart$(Self);
   }
   /// procedure TNJWebSocketServer.StopServer()
   ,StopServer:function(Self) {
      var LHandle$3 = undefined;
      LHandle$3 = TNJCustomServer.GetHandle$1$(Self);
      if (LHandle$3) {
         LHandle$3.close(function () {
            TNJCustomServer.SetActive$3$(Self.FServer$2,false);
            TNJRawWebSocketServer.InternalSetActive(Self,false);
            TNJCustomServer.AfterStop$(Self);
            TNJRawWebSocketServer.ClearLookupTable(Self);
         });
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3$:function($){return $.ClassType.Create$3($)}
   ,GetExceptionClass:TNJRawWebSocketServer.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,AfterStart:TNJCustomServer.AfterStart
   ,AfterStop:TNJCustomServer.AfterStop
   ,BeforeStart:TNJCustomServer.BeforeStart
   ,BeforeStop:TNJCustomServer.BeforeStop
   ,GetActive$2:TNJCustomServer.GetActive$2
   ,GetHandle$1:TNJCustomServer.GetHandle$1
   ,GetPort$:function($){return $.ClassType.GetPort($)}
   ,SetActive$3:TNJRawWebSocketServer.SetActive$3
   ,SetHandle$2:TNJCustomServer.SetHandle$2
   ,SetPort$:function($){return $.ClassType.SetPort.apply($.ClassType, arguments)}
   ,Start:TNJCustomServer.Start
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,Stop:TNJCustomServer.Stop
   ,StopServer$:function($){return $.ClassType.StopServer($)}
   ,Dispatch$1:TNJRawWebSocketServer.Dispatch$1
   ,DoClientConnected:TNJRawWebSocketServer.DoClientConnected
   ,DoClientDisconnected:TNJRawWebSocketServer.DoClientDisconnected
   ,DoClientNameChange:TNJRawWebSocketServer.DoClientNameChange
   ,SetPath:TNJRawWebSocketServer.SetPath
   ,SetTracking:TNJRawWebSocketServer.SetTracking
   ,GetHttpServer$:function($){return $.ClassType.GetHttpServer($)}
};
TNJWebSocketServer.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TNJWebsocketMessageType enumeration
var TNJWebsocketMessageType = [ "mtText", "mtBinary" ];
/// TNJWebsocketMessage = record
function Copy$TNJWebsocketMessage(s,d) {
   d.wiType=s.wiType;
   d.wiBuffer=s.wiBuffer;
   d.wiText=s.wiText;
   return d;
}
function Clone$TNJWebsocketMessage($) {
   return {
      wiType:$.wiType,
      wiBuffer:$.wiBuffer,
      wiText:$.wiText
   }
}
/// JWsReadyState enumeration
var JWsReadyState = [ "rsConnecting", "rsOpen", "rsClosing", "rsClosed" ];
/// ENJWebSocketSocket = class (EW3Exception)
var ENJWebSocketSocket = {
   $ClassName:"ENJWebSocketSocket",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// ENJWebSocketServer = class (EW3Exception)
var ENJWebSocketServer = {
   $ClassName:"ENJWebSocketServer",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TNJServerChildClass = class (TW3HandleBasedObject)
var TNJServerChildClass = {
   $ClassName:"TNJServerChildClass",$Parent:TW3HandleBasedObject
   ,$Init:function ($) {
      TW3HandleBasedObject.$Init($);
      $.FParent$1 = null;
   }
   /// constructor TNJServerChildClass.Create(Server: TNJCustomServer)
   ,Create$86:function(Self, Server$6) {
      TObject.Create(Self);
      Self.FParent$1 = Server$6;
      return Self
   }
   ,Destroy:TObject.Destroy
   ,ObjectHandleChanged:TW3HandleBasedObject.ObjectHandleChanged
   ,SetObjectHandle:TW3HandleBasedObject.SetObjectHandle
   ,GetObjectHandle:TW3HandleBasedObject.GetObjectHandle
   ,Create$86$:function($){return $.ClassType.Create$86.apply($.ClassType, arguments)}
};
/// TNJCustomServerResponse = class (TNJServerChildClass)
var TNJCustomServerResponse = {
   $ClassName:"TNJCustomServerResponse",$Parent:TNJServerChildClass
   ,$Init:function ($) {
      TNJServerChildClass.$Init($);
   }
   ,Destroy:TObject.Destroy
   ,ObjectHandleChanged:TW3HandleBasedObject.ObjectHandleChanged
   ,SetObjectHandle:TW3HandleBasedObject.SetObjectHandle
   ,GetObjectHandle:TW3HandleBasedObject.GetObjectHandle
   ,Create$86:TNJServerChildClass.Create$86
};
/// TNJCustomServerRequest = class (TNJServerChildClass)
var TNJCustomServerRequest = {
   $ClassName:"TNJCustomServerRequest",$Parent:TNJServerChildClass
   ,$Init:function ($) {
      TNJServerChildClass.$Init($);
   }
   ,Destroy:TObject.Destroy
   ,ObjectHandleChanged:TW3HandleBasedObject.ObjectHandleChanged
   ,SetObjectHandle:TW3HandleBasedObject.SetObjectHandle
   ,GetObjectHandle:TW3HandleBasedObject.GetObjectHandle
   ,Create$86:TNJServerChildClass.Create$86
};
/// ENJServerError = class (EW3Exception)
var ENJServerError = {
   $ClassName:"ENJServerError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TNJNetworkService = class (TW3HandleBasedObject)
var TNJNetworkService = {
   $ClassName:"TNJNetworkService",$Parent:TW3HandleBasedObject
   ,$Init:function ($) {
      TW3HandleBasedObject.$Init($);
      $.OnError = null;
      $.OnAfterStopped = null;
      $.OnBeforeStopped = null;
      $.OnAfterStarted = null;
      $.OnBeforeStarted = null;
      $.FActive$4 = false;
      $.FHost = "";
      $.FPort$2 = 0;
   }
   /// procedure TNJNetworkService.AfterStart()
   ,AfterStart$1:function(Self) {
      if (Self.OnAfterStarted) {
         Self.OnAfterStarted(Self);
      }
   }
   /// procedure TNJNetworkService.AfterStop()
   ,AfterStop$1:function(Self) {
      if (Self.OnAfterStopped) {
         Self.OnAfterStopped(Self);
      }
   }
   /// procedure TNJNetworkService.BeforeStart()
   ,BeforeStart$1:function(Self) {
      if (Self.OnBeforeStarted) {
         Self.OnBeforeStarted(Self);
      }
   }
   /// procedure TNJNetworkService.BeforeStop()
   ,BeforeStop$1:function(Self) {
      if (Self.OnBeforeStopped) {
         Self.OnBeforeStopped(Self);
      }
   }
   /// destructor TNJNetworkService.Destroy()
   ,Destroy:function(Self) {
      if (TNJNetworkService.GetActive$3$(Self)) {
         try {
            try {
               TNJNetworkService.FinalizeService$(Self);
            } catch ($e) {
               var e$56 = $W($e);
               /* null */
            }
         } finally {
            TW3HandleBasedObject.SetObjectHandle$(Self,undefined);
         }
      }
      TObject.Destroy(Self);
   }
   /// function TNJNetworkService.GetActive() : Boolean
   ,GetActive$3:function(Self) {
      return Self.FActive$4;
   }
   /// function TNJNetworkService.GetAddress() : String
   ,GetAddress:function(Self) {
      return Self.FHost;
   }
   /// function TNJNetworkService.GetPort() : Integer
   ,GetPort$2:function(Self) {
      return Self.FPort$2;
   }
   /// procedure TNJNetworkService.SetActive(const NewValue: Boolean)
   ,SetActive$6:function(Self, NewValue$4) {
      if (NewValue$4!=TNJNetworkService.GetActive$3$(Self)) {
         if (TNJNetworkService.GetActive$3$(Self)) {
            TNJNetworkService.BeforeStop$1$(Self);
            TNJNetworkService.FinalizeService$(Self);
         } else {
            TNJNetworkService.BeforeStart$1$(Self);
            TNJNetworkService.InitializeService$(Self);
         }
      }
   }
   /// procedure TNJNetworkService.SetActiveEx(const NewValue: Boolean)
   ,SetActiveEx:function(Self, NewValue$5) {
      Self.FActive$4 = NewValue$5;
   }
   /// procedure TNJNetworkService.SetAddress(const NewHost: String)
   ,SetAddress$1:function(Self, NewHost) {
      if (TNJNetworkService.GetActive$3$(Self)) {
         throw Exception.Create($New(ENJNetworkError),"Address cannot be altered while object is active error");
      } else {
         Self.FHost = Trim$_String_(NewHost);
      }
   }
   /// procedure TNJNetworkService.SetPort(const NewPort: Integer)
   ,SetPort$3:function(Self, NewPort) {
      if (TNJNetworkService.GetActive$3$(Self)) {
         throw Exception.Create($New(ENJNetworkError),"Port cannot be altered while object is active error");
      } else {
         Self.FPort$2 = NewPort;
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,ObjectHandleChanged:TW3HandleBasedObject.ObjectHandleChanged
   ,SetObjectHandle:TW3HandleBasedObject.SetObjectHandle
   ,GetObjectHandle:TW3HandleBasedObject.GetObjectHandle
   ,AfterStart$1$:function($){return $.ClassType.AfterStart$1($)}
   ,AfterStop$1$:function($){return $.ClassType.AfterStop$1($)}
   ,BeforeStart$1$:function($){return $.ClassType.BeforeStart$1($)}
   ,BeforeStop$1$:function($){return $.ClassType.BeforeStop$1($)}
   ,FinalizeService$:function($){return $.ClassType.FinalizeService($)}
   ,GetActive$3$:function($){return $.ClassType.GetActive$3($)}
   ,GetAddress$:function($){return $.ClassType.GetAddress($)}
   ,GetPort$2$:function($){return $.ClassType.GetPort$2($)}
   ,InitializeService$:function($){return $.ClassType.InitializeService($)}
   ,SetActive$6$:function($){return $.ClassType.SetActive$6.apply($.ClassType, arguments)}
   ,SetAddress$1$:function($){return $.ClassType.SetAddress$1.apply($.ClassType, arguments)}
   ,SetPort$3$:function($){return $.ClassType.SetPort$3.apply($.ClassType, arguments)}
};
/// TNJBoundNetworkService = class (TNJNetworkService)
var TNJBoundNetworkService = {
   $ClassName:"TNJBoundNetworkService",$Parent:TNJNetworkService
   ,$Init:function ($) {
      TNJNetworkService.$Init($);
   }
   ,Destroy:TNJNetworkService.Destroy
   ,ObjectHandleChanged:TW3HandleBasedObject.ObjectHandleChanged
   ,SetObjectHandle:TW3HandleBasedObject.SetObjectHandle
   ,GetObjectHandle:TW3HandleBasedObject.GetObjectHandle
   ,AfterStart$1:TNJNetworkService.AfterStart$1
   ,AfterStop$1:TNJNetworkService.AfterStop$1
   ,BeforeStart$1:TNJNetworkService.BeforeStart$1
   ,BeforeStop$1:TNJNetworkService.BeforeStop$1
   ,FinalizeService:TNJNetworkService.FinalizeService
   ,GetActive$3:TNJNetworkService.GetActive$3
   ,GetAddress:TNJNetworkService.GetAddress
   ,GetPort$2:TNJNetworkService.GetPort$2
   ,InitializeService:TNJNetworkService.InitializeService
   ,SetActive$6:TNJNetworkService.SetActive$6
   ,SetAddress$1:TNJNetworkService.SetAddress$1
   ,SetPort$3:TNJNetworkService.SetPort$3
};
/// ENJNetworkError = class (EW3Exception)
var ENJNetworkError = {
   $ClassName:"ENJNetworkError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TNJAddressBindings = class (TW3LockedObject)
var TNJAddressBindings = {
   $ClassName:"TNJAddressBindings",$Parent:TW3LockedObject
   ,$Init:function ($) {
      TW3LockedObject.$Init($);
      $.FItems = [];
   }
   /// function TNJAddressBindings.Add() : TNJAddressBinding
   ,Add$3:function(Self) {
      var Result = null;
      if (TW3LockedObject.GetLockState$1(Self)) {
         throw EW3Exception.CreateFmt$($New(ENJAddressBindings),"%s failed, bindings cannot be altered while active error",["TNJAddressBindings.Add"]);
      } else {
         Result = TNJAddressBinding.Create$89$($New(TNJAddressBinding),Self);
         Self.FItems.push(Result);
      }
      return Result
   }
   /// function TNJAddressBindings.AddEx(Addresse: String; Port: Word) : TNJAddressBinding
   ,AddEx:function(Self, Addresse, Port$3) {
      var Result = null;
      if (TW3LockedObject.GetLockState$1(Self)) {
         throw EW3Exception.CreateFmt$($New(ENJAddressBindings),"%s failed, bindings cannot be altered while active error",["TNJAddressBindings.AddEx"]);
      } else {
         Result = TNJAddressBinding.Create$89$($New(TNJAddressBinding),Self);
         TNJAddressBinding.SetAddress$(Result,Addresse);
         TNJAddressBinding.SetPort$2$(Result,Port$3);
         Self.FItems.push(Result);
      }
      return Result
   }
   /// procedure TNJAddressBindings.Clear()
   ,Clear$12:function(Self) {
      var x$60 = 0;
      if (TW3LockedObject.GetLockState$1(Self)) {
         throw Exception.Create($New(EW3LockError),"Bindings cannot be altered while active error");
      } else {
         try {
            var $temp104;
            for(x$60=0,$temp104=Self.FItems.length;x$60<$temp104;x$60++) {
               TObject.Free(Self.FItems[x$60]);
               Self.FItems[x$60]=null;
            }
         } finally {
            Self.FItems.length=0;
         }
      }
   }
   /// procedure TNJAddressBindings.Delete(const index: Integer)
   ,Delete$7:function(Self, index$14) {
      if (TW3LockedObject.GetLockState$1(Self)) {
         throw Exception.Create($New(EW3LockError),"Bindings cannot be altered while active error");
      } else {
         try {
            Self.FItems.splice(index$14,1)
            ;
         } catch ($e) {
            var e$57 = $W($e);
            throw EW3Exception.CreateFmt$($New(ENJAddressBindings),"Failed to delete binding, system threw exception %s with message %s",[TObject.ClassName(e$57.ClassType), e$57.FMessage]);
         }
      }
   }
   /// procedure TNJAddressBindings.DeleteEx(const Item: TNJAddressBinding)
   ,DeleteEx:function(Self, Item$4) {
      if (Item$4!==null) {
         TNJAddressBindings.Delete$7$(Self,Self.FItems.indexOf(Item$4));
      } else {
         throw EW3Exception.CreateFmt$($New(ENJAddressBindings),"%s failed, item was NIL error",["TNJAddressBindings.DeleteEx"]);
      }
   }
   /// destructor TNJAddressBindings.Destroy()
   ,Destroy:function(Self) {
      if (Self.FItems.length>0) {
         TNJAddressBindings.Clear$12$(Self);
      }
      TObject.Destroy(Self);
   }
   /// function TNJAddressBindings.GetBinding(const index: Integer) : TNJAddressBinding
   ,GetBinding:function(Self, index$15) {
      return Self.FItems[index$15];
   }
   /// function TNJAddressBindings.GetBindingCount() : Integer
   ,GetBindingCount:function(Self) {
      return Self.FItems.length;
   }
   /// function TNJAddressBindings.IndexOf(const Item: TNJAddressBinding) : Integer
   ,IndexOf$3:function(Self, Item$5) {
      var Result = 0;
      if (Item$5!==null) {
         Result = Self.FItems.indexOf(Item$5);
      } else {
         throw EW3Exception.CreateFmt$($New(ENJAddressBindings),"%s failed, item was NIL error",["TNJAddressBindings.IndexOf"]);
      }
      return Result
   }
   /// procedure TNJAddressBindings.ObjectLocked()
   ,ObjectLocked$1:function(Self) {
      var x$61 = 0;
      var LAccess$1 = null;
      var $temp105;
      for(x$61=0,$temp105=Self.FItems.length;x$61<$temp105;x$61++) {
         LAccess$1 = $AsIntf(Self.FItems[x$61],"IW3LockObject");
         LAccess$1[0]();
      }
   }
   /// function TNJAddressBindings.ObjectOfAddr(Address: String) : TNJAddressBinding
   ,ObjectOfAddr:function(Self, Address$2) {
      var Result = null;
      var x$62 = 0;
      var LItem$6 = null;
      Address$2 = Trim$_String_((Address$2).toLocaleLowerCase());
      var $temp106;
      for(x$62=0,$temp106=Self.FItems.length;x$62<$temp106;x$62++) {
         LItem$6 = Self.FItems[x$62];
         if (LItem$6.FAddress==Address$2) {
            Result = LItem$6;
            break;
         }
      }
      return Result
   }
   /// function TNJAddressBindings.ObjectOfPort(Port: Word) : TNJAddressBinding
   ,ObjectOfPort:function(Self, Port$4) {
      var Result = null;
      var x$63 = 0;
      var LItem$7 = null;
      var $temp107;
      for(x$63=0,$temp107=Self.FItems.length;x$63<$temp107;x$63++) {
         LItem$7 = Self.FItems[x$63];
         if (LItem$7.FPort$1==Port$4) {
            Result = LItem$7;
            break;
         }
      }
      return Result
   }
   /// procedure TNJAddressBindings.ObjectUnLocked()
   ,ObjectUnLocked$1:function(Self) {
      var x$64 = 0;
      var LAccess$2 = null;
      var $temp108;
      for(x$64=0,$temp108=Self.FItems.length;x$64<$temp108;x$64++) {
         LAccess$2 = $AsIntf(Self.FItems[x$64],"IW3LockObject");
         LAccess$2[1]();
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,ObjectLocked$1$:function($){return $.ClassType.ObjectLocked$1($)}
   ,ObjectUnLocked$1$:function($){return $.ClassType.ObjectUnLocked$1($)}
   ,Add$3$:function($){return $.ClassType.Add$3($)}
   ,AddEx$:function($){return $.ClassType.AddEx.apply($.ClassType, arguments)}
   ,Clear$12$:function($){return $.ClassType.Clear$12($)}
   ,Delete$7$:function($){return $.ClassType.Delete$7.apply($.ClassType, arguments)}
   ,DeleteEx$:function($){return $.ClassType.DeleteEx.apply($.ClassType, arguments)}
   ,IndexOf$3$:function($){return $.ClassType.IndexOf$3.apply($.ClassType, arguments)}
   ,ObjectOfAddr$:function($){return $.ClassType.ObjectOfAddr.apply($.ClassType, arguments)}
   ,ObjectOfPort$:function($){return $.ClassType.ObjectOfPort.apply($.ClassType, arguments)}
};
TNJAddressBindings.$Intf={
   IW3LockObject:[TW3LockedObject.DisableAlteration$1,TW3LockedObject.EnableAlteration$1,TW3LockedObject.GetLockState$1]
}
/// TNJAddressBinding = class (TW3OwnedLockedObject)
var TNJAddressBinding = {
   $ClassName:"TNJAddressBinding",$Parent:TW3OwnedLockedObject
   ,$Init:function ($) {
      TW3OwnedLockedObject.$Init($);
      $.FAddress = "";
      $.FPort$1 = 0;
   }
   /// function TNJAddressBinding.AcceptOwner(const CandidateObject: TObject) : Boolean
   ,AcceptOwner:function(Self, CandidateObject$7) {
      return (CandidateObject$7!==null)&&$Is(CandidateObject$7,TNJAddressBindings);
   }
   /// constructor TNJAddressBinding.Create(const AOwner: TNJAddressBindings)
   ,Create$89:function(Self, AOwner$6) {
      TW3OwnedObject.Create$16(Self,AOwner$6);
      return Self
   }
   /// function TNJAddressBinding.GetOwner() : TNJAddressBindings
   ,GetOwner$5:function(Self) {
      return $As(TW3OwnedObject.GetOwner$(Self),TNJAddressBindings);
   }
   /// procedure TNJAddressBinding.SetAddress(const NewAddr: String)
   ,SetAddress:function(Self, NewAddr) {
      if (NewAddr!=Self.FAddress) {
         if (TW3OwnedLockedObject.GetLockState(Self)) {
            throw Exception.Create($New(EW3LockError),"Property cannot be altered while active error");
         } else {
            Self.FAddress = NewAddr;
         }
      }
   }
   /// procedure TNJAddressBinding.SetPort(const NewPort: Word)
   ,SetPort$2:function(Self, NewPort$1) {
      if (NewPort$1!=Self.FPort$1) {
         if (TW3OwnedLockedObject.GetLockState(Self)) {
            throw Exception.Create($New(EW3LockError),"Property cannot be altered while active error");
         } else {
            Self.FPort$1 = NewPort$1;
         }
      }
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedObject.Create$16
   ,ObjectLocked:TW3OwnedLockedObject.ObjectLocked
   ,ObjectUnLocked:TW3OwnedLockedObject.ObjectUnLocked
   ,Create$89$:function($){return $.ClassType.Create$89.apply($.ClassType, arguments)}
   ,SetAddress$:function($){return $.ClassType.SetAddress.apply($.ClassType, arguments)}
   ,SetPort$2$:function($){return $.ClassType.SetPort$2.apply($.ClassType, arguments)}
};
TNJAddressBinding.$Intf={
   IW3OwnedObjectAccess:[TNJAddressBinding.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
   ,IW3LockObject:[TW3OwnedLockedObject.DisableAlteration,TW3OwnedLockedObject.EnableAlteration,TW3OwnedLockedObject.GetLockState]
}
/// ENJAddressBindings = class (ENJNetworkError)
var ENJAddressBindings = {
   $ClassName:"ENJAddressBindings",$Parent:ENJNetworkError
   ,$Init:function ($) {
      ENJNetworkError.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
function util() {
   return require("util");
};
/// dgram_bindinfo = class (JObject)
function dgram_bindinfo() {
}
$Extend(Object,dgram_bindinfo,
   {
      "reuseAddr" : false,
      "port" : 0,
      "address" : "",
      "exclusive" : false
   });

function dgram() {
   return require("dgram");
};
/// TNJHTTPServer = class (TNJCustomServer)
var TNJHTTPServer = {
   $ClassName:"TNJHTTPServer",$Parent:TNJCustomServer
   ,$Init:function ($) {
      TNJCustomServer.$Init($);
      $.OnRequest = null;
   }
   /// anonymous TSourceMethodSymbol
   ,a$191:function(Self) {
      return TNJCustomServer.GetHandle$1$(Self);
   }
   /// procedure TNJHTTPServer.Dispatch(request: JServerRequest; response: JServerResponse)
   ,Dispatch$2:function(Self, request$2, response) {
      var LRequest = null,
         LResponse$1 = null;
      if (Self.OnRequest) {
         LRequest = TNJHttpRequest.Create$88$($New(TNJHttpRequest),Self,request$2);
         LResponse$1 = TNJHttpResponse.Create$87$($New(TNJHttpResponse),Self,response);
         try {
            Self.OnRequest(Self,LRequest,LResponse$1);
         } catch ($e) {
            var e$58 = $W($e);
            throw EW3Exception.CreateFmt$($New(ENJHttpServerError),"Dispatch failed, system threw exception %s with message [%s]",[TObject.ClassName(e$58.ClassType), e$58.FMessage]);
         }
         LResponse$1 = null;
         LRequest = null;
      }
   }
   /// procedure TNJHTTPServer.InternalSetActive(const Value: Boolean)
   ,InternalSetActive$1:function(Self, Value$177) {
      TNJCustomServer.SetActive$3(Self,Value$177);
   }
   /// procedure TNJHTTPServer.SetActive(const Value: Boolean)
   ,SetActive$3:function(Self, Value$178) {
      if (Value$178!=TNJCustomServer.GetActive$2$(Self)) {
         TNJCustomServer.SetActive$3(Self,Value$178);
         try {
            if (TNJCustomServer.GetActive$2$(Self)) {
               TNJCustomServer.StartServer$(Self);
            } else {
               TNJCustomServer.StopServer$(Self);
            }
         } catch ($e) {
            var e$59 = $W($e);
            TNJCustomServer.SetActive$3(Self,(!Value$178))         }
      }
   }
   /// procedure TNJHTTPServer.StartServer()
   ,StartServer:function(Self) {
      var LServer$2 = null;
      try {
         LServer$2 = http().createServer($Event2(Self,TNJHTTPServer.Dispatch$2$));
      } catch ($e) {
         var e$60 = $W($e);
         throw EW3Exception.CreateFmt$($New(ENJHttpServerError),"Failed to create NodeJS server object, system threw exception %s with message [%s]",[TObject.ClassName(e$60.ClassType), e$60.FMessage]);
      }
      try {
         LServer$2.listen(TNJCustomServer.GetPort$(Self),"");
      } catch ($e) {
         var e$61 = $W($e);
         LServer$2 = null;
         throw EW3Exception.CreateFmt$($New(ENJHttpServerError),"Failed to start server, system threw exception %s with message %s",[TObject.ClassName(e$61.ClassType), e$61.FMessage]);
      }
      TNJCustomServer.SetHandle$2$(Self,LServer$2);
      TNJCustomServer.AfterStart$(Self);
   }
   /// procedure TNJHTTPServer.StopServer()
   ,StopServer:function(Self) {
      var cb = null;
      cb = function () {
         TNJHTTPServer.InternalSetActive$1(Self,false);
         TNJCustomServer.AfterStop$(Self);
      };
      TNJCustomServer.GetHandle$1$(Self).close(cb);
   }
   ,Destroy:TW3ErrorObject.Destroy
   ,Create$3:TW3ErrorObject.Create$3
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,AfterStart:TNJCustomServer.AfterStart
   ,AfterStop:TNJCustomServer.AfterStop
   ,BeforeStart:TNJCustomServer.BeforeStart
   ,BeforeStop:TNJCustomServer.BeforeStop
   ,GetActive$2:TNJCustomServer.GetActive$2
   ,GetHandle$1:TNJCustomServer.GetHandle$1
   ,GetPort:TNJCustomServer.GetPort
   ,SetActive$3$:function($){return $.ClassType.SetActive$3.apply($.ClassType, arguments)}
   ,SetHandle$2:TNJCustomServer.SetHandle$2
   ,SetPort:TNJCustomServer.SetPort
   ,Start:TNJCustomServer.Start
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,Stop:TNJCustomServer.Stop
   ,StopServer$:function($){return $.ClassType.StopServer($)}
   ,Dispatch$2$:function($){return $.ClassType.Dispatch$2.apply($.ClassType, arguments)}
};
TNJHTTPServer.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TNJHttpResponse = class (TNJCustomServerResponse)
var TNJHttpResponse = {
   $ClassName:"TNJHttpResponse",$Parent:TNJCustomServerResponse
   ,$Init:function ($) {
      TNJCustomServerResponse.$Init($);
   }
   /// anonymous TSourceMethodSymbol
   ,a$195:function(Self) {
      return parseInt(TW3HandleBasedObject.GetObjectHandle$(Self).statusCode,10);
   }
   /// anonymous TSourceMethodSymbol
   ,a$194:function(Self, Value$179) {
      TW3HandleBasedObject.GetObjectHandle$(Self).statusCode;
   }
   /// anonymous TSourceMethodSymbol
   ,a$193:function(Self) {
      return TW3HandleBasedObject.GetObjectHandle$(Self);
   }
   /// constructor TNJHttpResponse.Create(const Server: TNJCustomServer; const ResponseObject: JServerResponse)
   ,Create$87:function(Self, Server$7, ResponseObject) {
      TNJServerChildClass.Create$86(Self,Server$7);
      TW3HandleBasedObject.SetObjectHandle$(Self,ResponseObject);
      return Self
   }
   /// procedure TNJHttpResponse.End(const Stream: TStream; const Encoding: String)
   ,End$4:function(Self, Stream$21, Encoding$1) {
      /* null */
   }
   /// procedure TNJHttpResponse.End(const Stream: TStream)
   ,End$3:function(Self, Stream$22) {
      var LBuffer$8 = null;
      LBuffer$8 = TDataTypeConverter.Create$15$($New(TBinaryData));
      TBinaryData.LoadFromStream(LBuffer$8,Stream$22);
      TW3HandleBasedObject.GetObjectHandle$(Self).end(TBinaryData.ToNodeBuffer$(LBuffer$8));
   }
   /// procedure TNJHttpResponse.End(const Data: Variant; const Encoding: String)
   ,End$2:function(Self, Data$81, Encoding$2) {
      TW3HandleBasedObject.GetObjectHandle$(Self).end(Data$81,Encoding$2);
   }
   /// procedure TNJHttpResponse.End(const Data: Variant)
   ,End$1:function(Self, Data$82) {
      TW3HandleBasedObject.GetObjectHandle$(Self).end(Data$82);
   }
   /// procedure TNJHttpResponse.End(const Buffer: TBinaryData)
   ,End:function(Self, Buffer$22) {
      TW3HandleBasedObject.GetObjectHandle$(Self).end(TBinaryData.ToNodeBuffer$(Buffer$22));
   }
   /// function TNJHttpResponse.GetHeader(const Name: String) : String
   ,GetHeader:function(Self, Name$46) {
      return String(TW3HandleBasedObject.GetObjectHandle$(Self).getHeader(Name$46));
   }
   /// function TNJHttpResponse.GetSendDate() : Boolean
   ,GetSendDate:function(Self) {
      return (TW3HandleBasedObject.GetObjectHandle$(Self).sendDate?true:false);
   }
   /// procedure TNJHttpResponse.RemoveHeader(const Name: String)
   ,RemoveHeader:function(Self, Name$47) {
      TW3HandleBasedObject.GetObjectHandle$(Self).removeHeader(Name$47);
   }
   /// procedure TNJHttpResponse.SetHeader(const Name: String; const Value: String)
   ,SetHeader:function(Self, Name$48, Value$180) {
      TW3HandleBasedObject.GetObjectHandle$(Self).setHeader(Name$48,Value$180);
   }
   /// procedure TNJHttpResponse.SetSendDate(const Value: Boolean)
   ,SetSendDate:function(Self, Value$181) {
      TW3HandleBasedObject.GetObjectHandle$(Self).sendDate = Value$181;
   }
   /// procedure TNJHttpResponse.Write(const Buffer: TBinaryData)
   ,Write$10:function(Self, Buffer$23) {
      TW3HandleBasedObject.GetObjectHandle$(Self).write(TBinaryData.ToNodeBuffer$(Buffer$23));
   }
   /// procedure TNJHttpResponse.Write(const Stream: TStream)
   ,Write$9:function(Self, Stream$23) {
      var LTemp$23 = null;
      LTemp$23 = TDataTypeConverter.Create$15$($New(TBinaryData));
      TBinaryData.LoadFromStream(LTemp$23,Stream$23);
      TW3HandleBasedObject.GetObjectHandle$(Self).write(TBinaryData.ToNodeBuffer$(LTemp$23));
   }
   /// function TNJHttpResponse.Write(const Text: String; const Encoding: String) : Boolean
   ,Write$8:function(Self, Text$40, Encoding$3) {
      return (TW3HandleBasedObject.GetObjectHandle$(Self).write(Text$40,Encoding$3)?true:false);
   }
   /// function TNJHttpResponse.Write(const Text: String) : Boolean
   ,Write$7:function(Self, Text$41) {
      var Result = false;
      try {
         TW3HandleBasedObject.GetObjectHandle$(Self).write(Text$41);
         Result = true;
      } catch ($e) {
         var e$62 = $W($e);
         /* null */
      }
      return Result
   }
   ,Destroy:TObject.Destroy
   ,ObjectHandleChanged:TW3HandleBasedObject.ObjectHandleChanged
   ,SetObjectHandle:TW3HandleBasedObject.SetObjectHandle
   ,GetObjectHandle:TW3HandleBasedObject.GetObjectHandle
   ,Create$86:TNJServerChildClass.Create$86
   ,Create$87$:function($){return $.ClassType.Create$87.apply($.ClassType, arguments)}
   ,GetSendDate$:function($){return $.ClassType.GetSendDate($)}
   ,SetSendDate$:function($){return $.ClassType.SetSendDate.apply($.ClassType, arguments)}
};
/// TNJHttpRequest = class (TNJCustomServerRequest)
var TNJHttpRequest = {
   $ClassName:"TNJHttpRequest",$Parent:TNJCustomServerRequest
   ,$Init:function ($) {
      TNJCustomServerRequest.$Init($);
      $.FEncoding = "";
   }
   /// anonymous TSourceMethodSymbol
   ,a$199:function(Self) {
      return String(TW3HandleBasedObject.GetObjectHandle$(Self).httpVersion);
   }
   /// anonymous TSourceMethodSymbol
   ,a$198:function(Self) {
      return String(TW3HandleBasedObject.GetObjectHandle$(Self).url);
   }
   /// anonymous TSourceMethodSymbol
   ,a$197:function(Self) {
      return String(TW3HandleBasedObject.GetObjectHandle$(Self).method);
   }
   /// anonymous TSourceMethodSymbol
   ,a$196:function(Self) {
      return TW3HandleBasedObject.GetObjectHandle$(Self);
   }
   /// constructor TNJHttpRequest.Create(const Server: TNJCustomServer; const RequestObject: JServerRequest)
   ,Create$88:function(Self, Server$8, RequestObject) {
      TNJServerChildClass.Create$86(Self,Server$8);
      TW3HandleBasedObject.SetObjectHandle$(Self,RequestObject);
      return Self
   }
   /// function TNJHttpRequest.GetConnection() : JNodeSocket
   ,GetConnection:function(Self) {
      var Result = null;
      if (TW3HandleBasedObject.GetObjectHandle$(Self)) {
         if (TW3HandleBasedObject.GetObjectHandle$(Self).connection) {
            Result = TW3HandleBasedObject.GetObjectHandle$(Self).connection;
         }
      }
      return Result
   }
   /// function TNJHttpRequest.GetEncoding() : String
   ,GetEncoding:function(Self) {
      return Self.FEncoding;
   }
   /// function TNJHttpRequest.GetHeaders() : TJSONObject
   ,GetHeaders:function(Self) {
      return TJSONObject.Create$67($New(TJSONObject),TW3HandleBasedObject.GetObjectHandle$(Self).headers);
   }
   /// function TNJHttpRequest.GetTrailers() : TJSONObject
   ,GetTrailers:function(Self) {
      var Result = null;
      if (TW3HandleBasedObject.GetObjectHandle$(Self)) {
         if (TW3HandleBasedObject.GetObjectHandle$(Self).trailers) {
            Result = TJSONObject.Create$67($New(TJSONObject),TW3HandleBasedObject.GetObjectHandle$(Self).trailers);
         }
      }
      return Result
   }
   /// procedure TNJHttpRequest.Pause()
   ,Pause:function(Self) {
      TW3HandleBasedObject.GetObjectHandle$(Self).pause;
   }
   /// procedure TNJHttpRequest.Resume()
   ,Resume:function(Self) {
      TW3HandleBasedObject.GetObjectHandle$(Self).resume;
   }
   /// procedure TNJHttpRequest.SetEncoding(const Value: String)
   ,SetEncoding:function(Self, Value$182) {
      Self.FEncoding = Value$182;
      TW3HandleBasedObject.GetObjectHandle$(Self).setEncoding(Value$182);
   }
   ,Destroy:TObject.Destroy
   ,ObjectHandleChanged:TW3HandleBasedObject.ObjectHandleChanged
   ,SetObjectHandle:TW3HandleBasedObject.SetObjectHandle
   ,GetObjectHandle:TW3HandleBasedObject.GetObjectHandle
   ,Create$86:TNJServerChildClass.Create$86
   ,Create$88$:function($){return $.ClassType.Create$88.apply($.ClassType, arguments)}
   ,GetConnection$:function($){return $.ClassType.GetConnection($)}
   ,GetEncoding$:function($){return $.ClassType.GetEncoding($)}
   ,GetHeaders$:function($){return $.ClassType.GetHeaders($)}
   ,GetTrailers$:function($){return $.ClassType.GetTrailers($)}
   ,Pause$:function($){return $.ClassType.Pause($)}
   ,Resume$:function($){return $.ClassType.Resume($)}
   ,SetEncoding$:function($){return $.ClassType.SetEncoding.apply($.ClassType, arguments)}
};
/// ENJHttpServerError = class (ENJServerError)
var ENJHttpServerError = {
   $ClassName:"ENJHttpServerError",$Parent:ENJServerError
   ,$Init:function ($) {
      ENJServerError.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TWbCMDLineParser = class (TCustomParser)
var TWbCMDLineParser = {
   $ClassName:"TWbCMDLineParser",$Parent:TCustomParser
   ,$Init:function ($) {
      TCustomParser.$Init($);
      $.FItems$3 = [];
   }
   /// anonymous TSourceMethodSymbol
   ,a$441:function(Self, index$16) {
      return Clone$TWbCMDInfo(Self.FItems$3[index$16]);
   }
   /// anonymous TSourceMethodSymbol
   ,a$440:function(Self) {
      return Self.FItems$3.length;
   }
   /// procedure TWbCMDLineParser.Clear()
   ,Clear$21:function(Self) {
      Self.FItems$3.length=0;
   }
   /// constructor TWbCMDLineParser.Create()
   ,Create$106:function(Self) {
      TCustomParser.Create$70(Self,TParserContext.Create$73$($New(TParserContext),""));
      Self.FOptions.AutoResetError = true;
      Self.FOptions.AutoWriteToConsole = true;
      Self.FOptions.ThrowExceptions = false;
      return Self
   }
   /// destructor TWbCMDLineParser.Destroy()
   ,Destroy:function(Self) {
      if (Self.FItems$3.length>0) {
         TWbCMDLineParser.Clear$21$(Self);
      }
      TW3ErrorObject.Destroy(Self);
   }
   /// function TWbCMDLineParser.Parse() : Boolean
   ,Parse:function(Self) {
      var Result = false;
      var LBuffer$9 = null,
         LCMD = {v:""},
         LParams = {v:[]},
         Rec = {ciCommand:"",ciParams:[]};
      var a$508 = 0;
      var item$7 = "";
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      LBuffer$9 = Self.FContext.FBuffer$3;
      if (TTextBuffer.Empty$2(LBuffer$9)) {
         TW3ErrorObject.SetLastError$(Self,"Parsing failed, buffer is empty error");
      } else {
         TTextBuffer.First(LBuffer$9);
         do {
            LCMD.v = "";
            LParams.v = [];
            TTextBuffer.ConsumeJunk(LBuffer$9);
            if (TTextBuffer.EOF$3(LBuffer$9)) {
               break;
            }
            if (!TTextBuffer.ReadWord$2(LBuffer$9,LCMD)) {
               TW3ErrorObject.SetLastError$(Self,"Syntax error, failed to read command error");
               break;
            }
            Rec.ciCommand = LCMD.v;
            TTextBuffer.ConsumeJunk(LBuffer$9);
            if (TTextBuffer.EOF$3(LBuffer$9)||TTextBuffer.CrLf(LBuffer$9)) {
               Self.FItems$3.push(Clone$TWbCMDInfo(Rec));
               break;
            }
            if (!TTextBuffer.ReadCommaList(LBuffer$9,LParams)) {
               break;
            }
            var $temp109;
            for(a$508=0,$temp109=LParams.v.length;a$508<$temp109;a$508++) {
               item$7 = LParams.v[a$508];
               Rec.ciParams.push(item$7);
            }
            Self.FItems$3.push(Clone$TWbCMDInfo(Rec));
            if (!TTextBuffer.Next(LBuffer$9)) {
               break;
            }
         } while (!TTextBuffer.EOF$3(LBuffer$9));
         Result = !TW3ErrorObject.GetFailed(Self);
      }
      return Result
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3:TW3ErrorObject.Create$3
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,Parse$:function($){return $.ClassType.Parse($)}
   ,Create$70:TCustomParser.Create$70
   ,Clear$21$:function($){return $.ClassType.Clear$21($)}
   ,Create$106$:function($){return $.ClassType.Create$106($)}
};
TWbCMDLineParser.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TWbCMDInfo = record
function Copy$TWbCMDInfo(s,d) {
   d.ciCommand=s.ciCommand;
   d.ciParams=s.ciParams;
   return d;
}
function Clone$TWbCMDInfo($) {
   return {
      ciCommand:$.ciCommand,
      ciParams:$.ciParams
   }
}
/// TNameValuePairData = record
function Copy$TNameValuePairData(s,d) {
   d.pdName=s.pdName;
   d.pdValue=s.pdValue;
   return d;
}
function Clone$TNameValuePairData($) {
   return {
      pdName:$.pdName,
      pdValue:$.pdValue
   }
}
/// procedure TNameValuePairData.FromJsObject(var Self: TNameValuePairData; const ObjectReference: Variant)
function TNameValuePairData$FromJsObject(Self$49, ObjectReference) {
   var LNames = [];
   if (TVariant.ValidRef(ObjectReference)) {
      LNames = TVariant.GetKeys(ObjectReference);
      Self$49.pdName = LNames[0];
      Self$49.pdValue = ObjectReference[Self$49.pdName];
   } else {
      throw Exception.Create($New(Exception),"Invalid JSON text, parse resulted in no object error");
   }
}
/// procedure TNameValuePairData.FromJSon(var Self: TNameValuePairData; const JSonText: String)
function TNameValuePairData$FromJSon$1(Self$50, JSonText) {
   var LRef$11;
   if (JSonText.length>0) {
      LRef$11 = JSON.parse(JSonText);
      if (LRef$11) {
         TNameValuePairData$FromJsObject(Self$50,LRef$11);
      }
   } else {
      throw Exception.Create($New(Exception),"Invalid JSON text, nothing to parse error");
   }
}
/// function TNameValuePairData.ToJsObject(var Self: TNameValuePairData) : Variant
function TNameValuePairData$ToJsObject(Self$51) {
   var Result = undefined;
   Result = TVariant.CreateObject();
   Result[Self$51.pdName] = Self$51.pdValue;
   return Result
}
/// function TNameValuePairData.ToJSON(var Self: TNameValuePairData) : String
function TNameValuePairData$ToJSON$8(Self$52) {
   return JSON.stringify(TNameValuePairData$ToJsObject(Self$52));
}
/// function TNameValuePairData.ToString(var Self: TNameValuePairData) : String
function TNameValuePairData$ToString$11(Self$53) {
   return Self$53.pdName+"="+Self$53.pdValue;
}
/// TNameValuePair = class (TObject)
var TNameValuePair = {
   $ClassName:"TNameValuePair",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FData$3 = {pdName:"",pdValue:undefined};
   }
   /// anonymous TSourceMethodSymbol
   ,a$445:function(Self, Value$183) {
      Self.FData$3.pdValue = Value$183;
   }
   /// anonymous TSourceMethodSymbol
   ,a$444:function(Self) {
      return Self.FData$3.pdName;
   }
   /// anonymous TSourceMethodSymbol
   ,a$443:function(Self, Value$184) {
      Self.FData$3.pdName = Value$184;
   }
   /// anonymous TSourceMethodSymbol
   ,a$442:function(Self) {
      return Self.FData$3.pdValue;
   }
   /// procedure TNameValuePair.Clear()
   ,Clear$22:function(Self) {
      Self.FData$3.pdName = "";
      Self.FData$3.pdValue = null;
   }
   /// constructor TNameValuePair.Create()
   ,Create$107:function(Self) {
      TObject.Create(Self);
      TNameValuePair.Clear$22$(Self);
      return Self
   }
   /// procedure TNameValuePair.LoadFromBuffer(const Buffer: TBinaryData)
   ,LoadFromBuffer$4:function(Self, Buffer$24) {
      var LReader = null;
      LReader = TW3CustomReader.Create$39$($New(TReader),$AsIntf(Buffer$24,"IBinaryTransport"));
      try {
         TNameValuePair.ReadFrom$1$(Self,LReader);
      } finally {
         TObject.Free(LReader);
      }
   }
   /// procedure TNameValuePair.LoadFromStream(const Stream: TStream)
   ,LoadFromStream$10:function(Self, Stream$24) {
      var LReader$1 = null;
      LReader$1 = TStreamReader.Create$40$($New(TStreamReader),Stream$24);
      try {
         TNameValuePair.ReadFrom$1$(Self,LReader$1);
      } finally {
         TObject.Free(LReader$1);
      }
   }
   /// procedure TNameValuePair.ReadFrom(const Reader: TReader)
   ,ReadFrom$1:function(Self, Reader$8) {
      var LHeader = 0,
         LLen$15 = 0;
      TNameValuePair.Clear$22$(Self);
      if (Reader$8) {
         LHeader = TW3CustomReader.ReadInteger(Reader$8);
         if (LHeader==3221338814) {
            Self.FData$3.pdName = TW3CustomReader.ReadString$1(Reader$8);
            LLen$15 = TW3CustomReader.ReadInteger(Reader$8);
            if (LLen$15>0) {
               Self.FData$3.pdValue = TDatatype.BytesToVariant$1(TDatatype,TW3CustomReader.Read$2(Reader$8,LLen$15));
            }
         } else {
            throw EW3Exception.CreateFmt$($New(EW3Exception),"Failed to read %s, invalid header [expected %s, not %s]",[TObject.ClassName(Self.ClassType), "$C001BABE", "$"+IntToHex(LHeader,8)]);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EW3Exception),"Failed to write %s, reader was nil error",[TObject.ClassName(Self.ClassType)]);
      }
   }
   /// procedure TNameValuePair.SaveToBuffer(const Buffer: TBinaryData)
   ,SaveToBuffer$4:function(Self, Buffer$25) {
      var LWriter = null;
      LWriter = TW3CustomWriter.Create$29$($New(TWriter),$AsIntf(Buffer$25,"IBinaryTransport"));
      try {
         TNameValuePair.WriteTo$1$(Self,LWriter);
      } finally {
         TObject.Free(LWriter);
      }
   }
   /// procedure TNameValuePair.SaveToStream(const Stream: TStream)
   ,SaveToStream$10:function(Self, Stream$25) {
      var LWriter$1 = null;
      LWriter$1 = TStreamWriter.Create$30$($New(TStreamWriter),Stream$25);
      try {
         TNameValuePair.WriteTo$1$(Self,$As(LWriter$1,TWriter));
      } finally {
         TObject.Free(LWriter$1);
      }
   }
   /// procedure TNameValuePair.WriteTo(const Writer: TWriter)
   ,WriteTo$1:function(Self, Writer$8) {
      var LData$6 = [];
      if (Writer$8) {
         if (Self.FData$3.pdValue) {
            LData$6 = TDatatype.VariantToBytes$1(TDatatype,Self.FData$3.pdValue);
         } else {
            LData$6 = [];
         }
         TW3CustomWriter.WriteInteger(Writer$8,3221338814);
         TW3CustomWriter.WriteString(Writer$8,Self.FData$3.pdName);
         TW3CustomWriter.WriteInteger(Writer$8,LData$6.length);
         if (LData$6.length>0) {
            TW3CustomWriter.Write$1(Writer$8,LData$6);
         }
      } else {
         throw EW3Exception.CreateFmt$($New(EW3Exception),"Failed to write %s, writer was nil error",[TObject.ClassName(Self.ClassType)]);
      }
   }
   ,Destroy:TObject.Destroy
   ,Clear$22$:function($){return $.ClassType.Clear$22($)}
   ,Create$107$:function($){return $.ClassType.Create$107($)}
   ,LoadFromBuffer$4$:function($){return $.ClassType.LoadFromBuffer$4.apply($.ClassType, arguments)}
   ,LoadFromStream$10$:function($){return $.ClassType.LoadFromStream$10.apply($.ClassType, arguments)}
   ,ReadFrom$1$:function($){return $.ClassType.ReadFrom$1.apply($.ClassType, arguments)}
   ,SaveToBuffer$4$:function($){return $.ClassType.SaveToBuffer$4.apply($.ClassType, arguments)}
   ,SaveToStream$10$:function($){return $.ClassType.SaveToStream$10.apply($.ClassType, arguments)}
   ,WriteTo$1$:function($){return $.ClassType.WriteTo$1.apply($.ClassType, arguments)}
};
/// TApplication = class (TObject)
var TApplication = {
   $ClassName:"TApplication",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   /// procedure TApplication.Abort()
   ,Abort:function(Self) {
      var access = null;
      access = process;
      if (access!==null) {
         access.abort();
      }
   }
   /// function TApplication.GetClusterAPI() : JClusterAPI
   ,GetClusterAPI:function(Self) {
      return NodeJSClusterAPI();
   }
   /// function TApplication.GetCPUCoreList() : 
   ,GetCPUCoreList:function(Self) {
      var Result = [];
      var Access$27 = null;
      Access$27 = NodeJSOsAPI();
      if (Access$27!==null) {
         Result = Access$27.cpus();
      }
      return Result
   }
   /// function TApplication.GetExeName() : String
   ,GetExeName:function(Self) {
      return ParamStr$1(1);
   }
   /// function TApplication.GetHostName() : String
   ,GetHostName:function(Self) {
      var Result = "";
      var Access$28 = null;
      Access$28 = NodeJSOsAPI();
      if (Access$28!==null) {
         Result = Access$28.hostname();
      }
      return Result
   }
   /// function TApplication.GetNodeLocation() : String
   ,GetNodeLocation:function(Self) {
      return ParamStr$1(0);
   }
   /// function TApplication.GetNodeProcess() : JNodeProcess
   ,GetNodeProcess:function(Self) {
      return process;
   }
   /// function TApplication.GetOSApi() : Jos_Exports
   ,GetOSApi:function(Self) {
      return NodeJSOsAPI();
   }
   /// function TApplication.GetPlatform() : String
   ,GetPlatform$1:function(Self) {
      var Result = "";
      var Access$29 = null;
      Access$29 = NodeJSOsAPI();
      if (Access$29!==null) {
         Result = Access$29.platform();
      }
      return Result
   }
   /// function TApplication.GetTempDir() : String
   ,GetTempDir:function(Self) {
      var Result = "";
      var Access$30 = null;
      Access$30 = NodeJSOsAPI();
      if (Access$30!==null) {
         Result = Access$30.tmpDir();
      }
      return Result
   }
   /// function TApplication.GetTotalFree() : Integer
   ,GetTotalFree$1:function(Self) {
      var Result = 0;
      var Access$31 = null;
      Access$31 = NodeJSOsAPI();
      if (Access$31!==null) {
         Result = Access$31.freemem();
      }
      return Result
   }
   /// function TApplication.GetTotalMem() : Integer
   ,GetTotalMem:function(Self) {
      var Result = 0;
      var Access$32 = null;
      Access$32 = NodeJSOsAPI();
      if (Access$32!==null) {
         Result = Access$32.totalmem();
      }
      return Result
   }
   /// function TApplication.GetUptime() : Integer
   ,GetUptime:function(Self) {
      var Result = 0;
      Result = Math.Floor(process.uptime());
      return Result
   }
   /// procedure TApplication.RunBeforeNextTick(const CB: TProcedureRef)
   ,RunBeforeNextTick:function(Self, CB$31) {
      process.nextTick(CB$31);
   }
   /// procedure TApplication.Terminate(ExitCode: Integer)
   ,Terminate$1:function(Self, ExitCode) {
      var access$1 = null;
      access$1 = process;
      if (access$1!==null) {
         access$1.exit(ExitCode);
      }
   }
   /// procedure TApplication.Terminate()
   ,Terminate:function(Self) {
      var access$2 = null;
      access$2 = process;
      if (access$2!==null) {
         access$2.exit(0);
      }
   }
   ,Destroy:TObject.Destroy
   ,Abort$:function($){return $.ClassType.Abort($)}
};
function Application() {
   var Result = null;
   if (__App===null) {
      __App = TObject.Create($New(TApplication));
   }
   Result = __App;
   return Result
};
/// TW3NodeStorageDevice = class (TW3StorageDevice)
var TW3NodeStorageDevice = {
   $ClassName:"TW3NodeStorageDevice",$Parent:TW3StorageDevice
   ,$Init:function ($) {
      TW3StorageDevice.$Init($);
      $.FCurrent$2 = $.FRootPath = "";
      $.FFileSys = null;
   }
   /// procedure TW3NodeStorageDevice.CdUp(CB: TW3DeviceCdUpCallback)
   ,CdUp:function(Self, CB$32) {
      if (TW3OwnedErrorObject.GetFailed$1(Self)) {
         TW3OwnedErrorObject.ClearLastError$1(Self);
      }
      if (TW3StorageDevice.GetActive$(Self)) {
         TW3StorageDevice.GetPath$1$(Self,function (Sender$22, Path$5, Success$17) {
            var xCurrentPath = "",
               DirParser = null,
               Moniker = "",
               Separator$19 = "",
               Parts$3 = [];
            if (Success$17) {
               xCurrentPath = Path$5;
               DirParser = GetDirectoryParser();
               Moniker = DirParser[1]();
               Separator$19 = DirParser[0]();
               xCurrentPath = TW3NodeStorageDevice.Normalize(Self,xCurrentPath);
               if ((xCurrentPath).toLocaleLowerCase()==(Moniker).toLocaleLowerCase()) {
                  if (CB$32) {
                     CB$32(Self,false);
                  }
                  return;
               }
               if (StrEndsWith(xCurrentPath,Separator$19)) {
                  xCurrentPath = (xCurrentPath).substr(0,(xCurrentPath.length-1));
               }
               Parts$3 = (xCurrentPath).split(Separator$19);
               Parts$3.splice((Parts$3.length-1),1)
               ;
               xCurrentPath = (Parts$3).join(Separator$19);
               TW3StorageDevice.ChDir$(Self,xCurrentPath,function (Sender$23, Path$6, Success$18) {
                  if (CB$32) {
                     CB$32(Self,Success$18);
                  }
               });
            } else if (CB$32) {
               CB$32(Self,false);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed, device not active error",["TW3NodeStorageDevice.CdUp"]);
         if (CB$32) {
            CB$32(Self,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.ChDir(FolderPath: String; CB: TW3DeviceChDirCallback)
   ,ChDir:function(Self, FolderPath, CB$33) {
      var Access$33 = null,
         workingpath = "";
      if (TW3OwnedErrorObject.GetFailed$1(Self)) {
         TW3OwnedErrorObject.ClearLastError$1(Self);
      }
      if (TW3StorageDevice.GetActive$(Self)) {
         FolderPath = TW3NodeStorageDevice.Normalize(Self,FolderPath);
         Access$33 = process;
         if (Access$33!==null) {
            workingpath = "";
            try {
               Access$33.chdir(FolderPath);
               workingpath = Access$33.cwd();
            } catch ($e) {
               var e$63 = $W($e);
               TW3OwnedErrorObject.SetLastError$1$(Self,e$63.FMessage);
               if (CB$33) {
                  CB$33(Self,FolderPath,false);
               }
               return;
            }
            Self.FCurrent$2 = workingpath;
            if (CB$33) {
               CB$33(Self,workingpath,workingpath.length>0);
            }
         } else {
            TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed, unable to access process error",["TW3NodeStorageDevice.ChDir"]);
            if (CB$33) {
               CB$33(Self,FolderPath,false);
            }
         }
      } else {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed, device not active error",["TW3NodeStorageDevice.ChDir"]);
         if (CB$33) {
            CB$33(Self,FolderPath,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.DirExists(FolderName: String; CB: TW3DeviceGetFileExistsCallback)
   ,DirExists:function(Self, FolderName$2, CB$34) {
      var PROCNAME = "";
      if (TW3OwnedErrorObject.GetFailed$1(Self)) {
         TW3OwnedErrorObject.ClearLastError$1(Self);
      }
      if (TW3StorageDevice.GetActive$(Self)) {
         PROCNAME = "TW3NodeStorageDevice.DirExists";
         FolderName$2 = TW3NodeStorageDevice.Normalize(Self,FolderName$2);
         Self.FFileSys.lstat(FolderName$2,function (err$4, stats$5) {
            if (err$4) {
               TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed with message %s",[PROCNAME, err$4.message]);
               if (CB$34) {
                  CB$34(Self,FolderName$2,false);
               }
               return;
            }
            if (CB$34) {
               CB$34(Self,FolderName$2,stats$5.isDirectory());
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed, device not active error",["TW3NodeStorageDevice.DirExists"]);
         if (CB$34) {
            CB$34(Self,FolderName$2,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.Examine(FolderPath: String; CB: TW3FileOperationExamineCallBack)
   ,Examine$1:function(Self, FolderPath$1, CB$35) {
      var LWalker = null;
      var PROCNAME$1 = "",
         LFiles$1 = null;
      if (TW3OwnedErrorObject.GetFailed$1(Self)) {
         TW3OwnedErrorObject.ClearLastError$1(Self);
      }
      if (TW3StorageDevice.GetActive$(Self)) {
         PROCNAME$1 = "TW3NodeStorageDevice.Examine";
         FolderPath$1 = TW3NodeStorageDevice.Normalize(Self,FolderPath$1);
         LWalker = TW3ErrorObject.Create$3$($New(TNJFileWalker));
         TNJFileWalker.Examine$4(LWalker,FolderPath$1,function (Sender$24, Success$19) {
            var LTemp$24 = null;
            var LTemp$25 = null;
            if (Success$19) {
               if (CB$35) {
                  LTemp$24.dlPath = FolderPath$1;
                  LTemp$24.dlItems = TNJFileWalker.ExtractAll(LWalker);
                  CB$35(Self,FolderPath$1,LTemp$24,true);
               }
            } else {
               if (CB$35) {
                  LTemp$25.dlPath = FolderPath$1;
                  CB$35(Self,FolderPath$1,LTemp$25,false);
               }
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed, device not active error",["TW3NodeStorageDevice.Examine"]);
         if (CB$35) {
            LFiles$1.dlPath = FolderPath$1;
            CB$35(Self,FolderPath$1,LFiles$1,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.FileExists(Filename: String; CB: TW3DeviceGetFileExistsCallback)
   ,FileExists$1:function(Self, Filename$17, CB$36) {
      var PROCNAME$2 = "";
      if (TW3OwnedErrorObject.GetFailed$1(Self)) {
         TW3OwnedErrorObject.ClearLastError$1(Self);
      }
      if (TW3StorageDevice.GetActive$(Self)) {
         PROCNAME$2 = "TW3NodeStorageDevice.FileExists";
         Filename$17 = TW3NodeStorageDevice.Normalize(Self,Filename$17);
         Self.FFileSys.exists(Filename$17,function (exists$1) {
            if (exists$1) {
               Self.FFileSys.lstat(Filename$17,function (err$5, stats$6) {
                  if (err$5) {
                     TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed with message %s",[PROCNAME$2, err$5.message]);
                     if (CB$36) {
                        CB$36(Self,Filename$17,false);
                     }
                     return;
                  }
                  if (CB$36) {
                     CB$36(Self,Filename$17,stats$6.isFile());
                  }
               });
            } else if (CB$36) {
               CB$36(Self,Filename$17,false);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed, device not active error",["TW3NodeStorageDevice.FileExists"]);
         if (CB$36) {
            CB$36(Self,"",false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.GetFileSize(Filename: String; CB: TW3DeviceGetFileSizeCallback)
   ,GetFileSize:function(Self, Filename$18, CB$37) {
      var PROCNAME$3 = "";
      if (TW3OwnedErrorObject.GetFailed$1(Self)) {
         TW3OwnedErrorObject.ClearLastError$1(Self);
      }
      if (TW3StorageDevice.GetActive$(Self)) {
         PROCNAME$3 = "TW3NodeStorageDevice.GetFileSize";
         Filename$18 = TW3NodeStorageDevice.Normalize(Self,Filename$18);
         Self.FFileSys.lstat(Filename$18,function (err$6, stats$7) {
            if (err$6) {
               TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed with message %s",[PROCNAME$3, err$6.message]);
               if (CB$37) {
                  CB$37(Self,Filename$18,0,false);
               }
               return;
            }
            if (CB$37) {
               CB$37(Self,Filename$18,stats$7.size,true);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed, device not active error",["TW3NodeStorageDevice.GetFileSize"]);
         if (CB$37) {
            CB$37(Self,"",0,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.GetPath(CB: TW3DeviceGetPathCallback)
   ,GetPath$1:function(Self, CB$38) {
      var Access$34 = null;
      if (TW3OwnedErrorObject.GetFailed$1(Self)) {
         TW3OwnedErrorObject.ClearLastError$1(Self);
      }
      if (TW3StorageDevice.GetActive$(Self)) {
         try {
            Access$34 = process;
            if (Access$34!==null) {
               if (CB$38) {
                  CB$38(Self,Access$34.cwd(),true);
               }
            }
         } catch ($e) {
            var e$64 = $W($e);
            TW3OwnedErrorObject.SetLastError$1$(Self,e$64.FMessage);
            CB$38(Self,"",false);
            return;
         }
      } else {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed, device not active error",["TW3NodeStorageDevice.GetPath"]);
         if (CB$38) {
            CB$38(Self,"",false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.GetStorageObjectType(ObjName: String; CB: TW3DeviceObjTypeCallback)
   ,GetStorageObjectType:function(Self, ObjName, CB$39) {
      var PROCNAME$4 = "";
      if (TW3OwnedErrorObject.GetFailed$1(Self)) {
         TW3OwnedErrorObject.ClearLastError$1(Self);
      }
      if (TW3StorageDevice.GetActive$(Self)) {
         PROCNAME$4 = "TW3NodeStorageDevice.GetStorageObjectType";
         ObjName = TW3NodeStorageDevice.Normalize(Self,ObjName);
         Self.FFileSys.lstat(ObjName,function (err$7, stats$8) {
            if (err$7) {
               TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed with message %s",[PROCNAME$4, err$7.message]);
               if (CB$39) {
                  CB$39(Self,ObjName,0,false);
               }
               return;
            }
            if (CB$39) {
               if (stats$8.isDirectory()) {
                  CB$39(Self,ObjName,2,true);
               } else if (stats$8.isFile()) {
                  CB$39(Self,ObjName,1,true);
               } else if (stats$8.isBlockDevice()) {
                  CB$39(Self,ObjName,3,true);
               } else if (stats$8.isCharacterDevice()) {
                  CB$39(Self,ObjName,4,true);
               } else if (stats$8.isSymbolicLink()) {
                  CB$39(Self,ObjName,5,true);
               } else if (stats$8.isFIFO()) {
                  CB$39(Self,ObjName,6,true);
               } else if (stats$8.isSocket()) {
                  CB$39(Self,ObjName,7,true);
               } else {
                  CB$39(Self,ObjName,0,false);
               }
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed, device not active error",["TW3NodeStorageDevice.GetStorageObjectType"]);
         if (CB$39) {
            CB$39(Self,ObjName,0,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.Load(Filename: String; TagValue: Variant; CB: TW3DeviceLoadCallback)
   ,Load:function(Self, Filename$19, TagValue$7, CB$40) {
      var PROCNAME$5 = "";
      if (TW3OwnedErrorObject.GetFailed$1(Self)) {
         TW3OwnedErrorObject.ClearLastError$1(Self);
      }
      if (TW3StorageDevice.GetActive$(Self)) {
         PROCNAME$5 = "TW3NodeStorageDevice.Load";
         Filename$19 = TW3NodeStorageDevice.Normalize(Self,Filename$19);
         Self.FFileSys.readFile(Filename$19,function (err$8, data$1) {
            var LTypedAccess$1 = null;
            var LBytes$21 = [],
               DataStream = null;
            if (err$8) {
               TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed: %s",[PROCNAME$5, err$8.message]);
               if (CB$40) {
                  CB$40(Self,TagValue$7,Filename$19,null,false);
               }
               return;
            }
            if (CB$40) {
               LTypedAccess$1 = new Uint8Array(data$1);
               LBytes$21 = TDatatype.TypedArrayToBytes$1(TDatatype,LTypedAccess$1);
               DataStream = TDataTypeConverter.Create$15$($New(TMemoryStream));
               if (LBytes$21.length>0) {
                  TStream.Write$2$(DataStream,LBytes$21);
                  TStream.SetPosition$(DataStream,0);
               }
               CB$40(Self,TagValue$7,Filename$19,DataStream,true);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed, device not active error",["TW3NodeStorageDevice.Load"]);
         if (CB$40) {
            CB$40(Self,TagValue$7,Filename$19,null,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.MakeDir(FolderName: String; Mode: TW3FilePermissionMask; CB: TW3DeviceMakeDirCallback)
   ,MakeDir:function(Self, FolderName$3, Mode$1, CB$41) {
      var PROCNAME$6 = "",
         ModeStr = "";
      if (TW3OwnedErrorObject.GetFailed$1(Self)) {
         TW3OwnedErrorObject.ClearLastError$1(Self);
      }
      if (TW3StorageDevice.GetActive$(Self)) {
         PROCNAME$6 = "TW3NodeStorageDevice.MakeDir";
         FolderName$3 = TW3NodeStorageDevice.Normalize(Self,FolderName$3);
         ModeStr = Mode$1.toString();
         Self.FFileSys.mkdir(FolderName$3,ModeStr,function (err$9) {
            if (err$9) {
               TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed with message: %s",[PROCNAME$6, err$9.message]);
               if (CB$41) {
                  CB$41(Self,FolderName$3,false);
               }
            } else if (CB$41) {
               CB$41(Self,FolderName$3,true);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed, device not active error",["TW3NodeStorageDevice.MakeDir"]);
         if (CB$41) {
            CB$41(Self,FolderName$3,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.Mount(Authentication: TW3DeviceAuthenticationData; CB: TW3StorageDeviceMountEvent)
   ,Mount:function(Self, Authentication, CB$42) {
      if (TW3OwnedErrorObject.GetFailed$1(Self)) {
         TW3OwnedErrorObject.ClearLastError$1(Self);
      }
      if (TW3StorageDevice.GetActive$(Self)) {
         TW3StorageDevice.UnMount$(Self,null);
      }
      Self.FFileSys = NodeFsAPI();
      try {
         Self.FRootPath = process.cwd();
      } catch ($e) {
         var e$65 = $W($e);
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed, device not active error",["TW3NodeStorageDevice.Mount"]);
         if (CB$42) {
            CB$42(Self,false);
         }
         return;
      }
      Self.FCurrent$2 = Self.FRootPath;
      TW3StorageDevice.SetActive$1$(Self,true);
      if (CB$42) {
         CB$42(Self,true);
      }
   }
   /// function TW3NodeStorageDevice.Normalize(NameOrPath: String) : String
   ,Normalize:function(Self, NameOrPath) {
      return NodePathAPI().normalize(NameOrPath);
   }
   /// procedure TW3NodeStorageDevice.RemoveDir(FolderName: String; CB: TW3DeviceRemoveDirCallback)
   ,RemoveDir:function(Self, FolderName$4, CB$43) {
      /* null */
   }
   /// procedure TW3NodeStorageDevice.Save(Filename: String; TagValue: Variant; Data: TStream; CB: TW3DeviceSaveCallback)
   ,Save:function(Self, Filename$20, TagValue$8, Data$83, CB$44) {
      var PROCNAME$7 = "",
         buffer$3 = null;
      if (TW3OwnedErrorObject.GetFailed$1(Self)) {
         TW3OwnedErrorObject.ClearLastError$1(Self);
      }
      if (TW3StorageDevice.GetActive$(Self)) {
         PROCNAME$7 = "TW3NodeStorageDevice.Save";
         Filename$20 = TW3NodeStorageDevice.Normalize(Self,Filename$20);
         TStream.SetPosition$(Data$83,0);
         buffer$3 = Buffer.from(TStream.Read$1$(Data$83,TStream.GetSize$(Data$83)));
         Self.FFileSys.writeFile(Filename$20,buffer$3,function (err$10) {
            if (err$10) {
               TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed: %s",[PROCNAME$7, err$10.message]);
               if (CB$44) {
                  CB$44(Self,TagValue$8,Filename$20,false);
               }
               return;
            }
            if (CB$44) {
               CB$44(Self,TagValue$8,Filename$20,true);
            }
         });
      } else {
         TW3OwnedErrorObject.SetLastErrorF$1(Self,"%s failed, device not active error",["TW3NodeStorageDevice.Save"]);
         if (CB$44) {
            CB$44(Self,TagValue$8,Filename$20,false);
         }
      }
   }
   /// procedure TW3NodeStorageDevice.UnMount(CB: TW3StorageDeviceUnMountEvent)
   ,UnMount:function(Self, CB$45) {
      if (TW3OwnedErrorObject.GetFailed$1(Self)) {
         TW3OwnedErrorObject.ClearLastError$1(Self);
      }
      if (TW3StorageDevice.GetActive$(Self)) {
         try {
            TW3StorageDevice.SetActive$1$(Self,false);
            if (CB$45) {
               CB$45(Self,true);
            }
         } finally {
            Self.FFileSys = null;
            Self.FRootPath = "";
            Self.FCurrent$2 = "";
         }
      }
   }
   ,Destroy:TW3Component.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedErrorObject.Create$16
   ,SetLastError$1:TW3OwnedErrorObject.SetLastError$1
   ,ObjectLocked$2:TW3OwnedLockedErrorObject.ObjectLocked$2
   ,ObjectUnLocked$2:TW3OwnedLockedErrorObject.ObjectUnLocked$2
   ,Create$44:TW3Component.Create$44
   ,CreateEx:TW3Component.CreateEx
   ,FinalizeObject:TW3StorageDevice.FinalizeObject
   ,FreeAfter:TW3Component.FreeAfter
   ,InitializeObject:TW3StorageDevice.InitializeObject
   ,CdUp$:function($){return $.ClassType.CdUp.apply($.ClassType, arguments)}
   ,ChDir$:function($){return $.ClassType.ChDir.apply($.ClassType, arguments)}
   ,DirExists$:function($){return $.ClassType.DirExists.apply($.ClassType, arguments)}
   ,Examine$2:TW3StorageDevice.Examine$2
   ,Examine$1$:function($){return $.ClassType.Examine$1.apply($.ClassType, arguments)}
   ,FileExists$1$:function($){return $.ClassType.FileExists$1.apply($.ClassType, arguments)}
   ,GetActive:TW3StorageDevice.GetActive
   ,GetDeviceId:TW3StorageDevice.GetDeviceId
   ,GetDeviceOptions:TW3StorageDevice.GetDeviceOptions
   ,GetFileSize$:function($){return $.ClassType.GetFileSize.apply($.ClassType, arguments)}
   ,GetName:TW3StorageDevice.GetName
   ,GetPath$1$:function($){return $.ClassType.GetPath$1.apply($.ClassType, arguments)}
   ,GetStorageObjectType$:function($){return $.ClassType.GetStorageObjectType.apply($.ClassType, arguments)}
   ,Load$:function($){return $.ClassType.Load.apply($.ClassType, arguments)}
   ,MakeDir$:function($){return $.ClassType.MakeDir.apply($.ClassType, arguments)}
   ,Mount$:function($){return $.ClassType.Mount.apply($.ClassType, arguments)}
   ,RemoveDir$:function($){return $.ClassType.RemoveDir.apply($.ClassType, arguments)}
   ,Save$:function($){return $.ClassType.Save.apply($.ClassType, arguments)}
   ,SetActive$1:TW3StorageDevice.SetActive$1
   ,SetDeviceId:TW3StorageDevice.SetDeviceId
   ,SetDeviceOptions:TW3StorageDevice.SetDeviceOptions
   ,SetName$1:TW3StorageDevice.SetName$1
   ,UnMount$:function($){return $.ClassType.UnMount.apply($.ClassType, arguments)}
};
TW3NodeStorageDevice.$Intf={
   IW3StorageDevice:[TW3StorageDevice.GetActive,TW3StorageDevice.GetName,TW3StorageDevice.GetDeviceOptions,TW3StorageDevice.GetDeviceId,TW3NodeStorageDevice.GetPath$1,TW3NodeStorageDevice.GetFileSize,TW3NodeStorageDevice.FileExists$1,TW3NodeStorageDevice.DirExists,TW3NodeStorageDevice.MakeDir,TW3NodeStorageDevice.RemoveDir,TW3NodeStorageDevice.Examine$1,TW3NodeStorageDevice.ChDir,TW3NodeStorageDevice.CdUp,TW3NodeStorageDevice.GetStorageObjectType,TW3NodeStorageDevice.Load,TW3NodeStorageDevice.Save]
   ,IW3LockObject:[TW3OwnedLockedErrorObject.DisableAlteration$2,TW3OwnedLockedErrorObject.EnableAlteration$2,TW3OwnedLockedErrorObject.GetLockState$2]
   ,IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TNJFileWalker = class (TW3ErrorObject)
var TNJFileWalker = {
   $ClassName:"TNJFileWalker",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.OnIncludeFile = null;
      $.OnAfterWalk = null;
      $.OnBeforeWalk = null;
      $.FBusy = $.FCancel = false;
      $.FCurrent$3 = $.FPath$1 = "";
      $.FDelay$2 = 1;
      $.FFinished = null;
      $.FItemStack = [];
      $.FListData = null;
   }
   /// procedure TNJFileWalker.Cancel()
   ,Cancel$1:function(Self) {
      if (Self.FBusy) {
         Self.FCancel = true;
         Self.FItemStack.length=0;
      }
   }
   /// procedure TNJFileWalker.Clear()
   ,Clear$15:function(Self) {
      Self.FListData.dlPath = "";
      Self.FListData.dlItems.length=0;
      Self.FListData = new TNJFileItemList();
      Self.FItemStack.length=0;
      Self.FBusy = false;
      Self.FCancel = false;
      Self.FFinished = null;
      Self.FPath$1 = "";
   }
   /// constructor TNJFileWalker.Create()
   ,Create$3:function(Self) {
      TW3ErrorObject.Create$3(Self);
      Self.FListData = new TNJFileItemList();
      return Self
   }
   /// destructor TNJFileWalker.Destroy()
   ,Destroy:function(Self) {
      try {
         try {
            if (Self.FBusy) {
               TNJFileWalker.Cancel$1(Self);
            }
         } finally {
            Self.FItemStack.length=0;
            Self.FListData.dlPath = "";
            Self.FListData.dlItems.length=0;
            Self.FListData = null;
            Self.FFinished = null;
         }
      } catch ($e) {
         var e$66 = $W($e);
         /* null */
      }
      TW3ErrorObject.Destroy(Self);
   }
   /// procedure TNJFileWalker.DoAfterWalk()
   ,DoAfterWalk:function(Self) {
      try {
         if (Self.FFinished) {
            Self.FFinished(Self,true);
         }
      } finally {
         if (Self.OnAfterWalk) {
            Self.OnAfterWalk(Self);
         }
      }
   }
   /// procedure TNJFileWalker.DoBeforeWalk()
   ,DoBeforeWalk:function(Self) {
      if (Self.OnBeforeWalk) {
         Self.OnBeforeWalk(Self);
      }
   }
   /// procedure TNJFileWalker.Examine(Path: String; CB: TNJFileWalkerCallback)
   ,Examine$4:function(Self, Path$7, CB$46) {
      var FFileSys$1 = null;
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (Self.FBusy) {
         TW3ErrorObject.SetLastError$(Self,"Filewalker instance is busy error");
         if (CB$46) {
            CB$46(Self,false);
         }
         return;
      }
      TNJFileWalker.Clear$15(Self);
      Self.FBusy = true;
      Self.FFinished = CB$46;
      Self.FPath$1 = Path$7;
      TNJFileWalker.DoBeforeWalk$(Self);
      FFileSys$1 = NodeFsAPI();
      FFileSys$1.readdir(Path$7,function (err$11, files) {
         var a$509 = 0;
         var fsName = "";
         if (err$11) {
            TW3ErrorObject.SetLastErrorF(Self,"Failed to examine [%s], path does not exist error",[Self.FPath$1]);
            if (CB$46) {
               CB$46(Self,false);
            }
         } else {
            Self.FListData.dlPath = Path$7;
            var $temp110;
            for(a$509=0,$temp110=files.length;a$509<$temp110;a$509++) {
               fsName = files[a$509];
               Self.FItemStack.push(fsName);
            }
            TW3Dispatch.Execute(TW3Dispatch,$Event0(Self,TNJFileWalker.NextOrDone),Self.FDelay$2);
         }
      });
   }
   /// function TNJFileWalker.ExtractAll() : TNJFileItems
   ,ExtractAll:function(Self) {
      var Result = [];
      var a$510 = 0;
      var xItem = null;
      if (Self.FBusy) {
         Result = [];
      } else {
         var a$511 = [];
         a$511 = Self.FListData.dlItems;
         var $temp111;
         for(a$510=0,$temp111=a$511.length;a$510<$temp111;a$510++) {
            xItem = a$511[a$510];
            Result.push(xItem);
         }
      }
      return Result
   }
   /// function TNJFileWalker.ExtractFiles() : TNJFileItems
   ,ExtractFiles:function(Self) {
      var Result = [];
      var a$512 = 0;
      var LItem$8 = null;
      if (!Self.FBusy) {
         var a$513 = [];
         a$513 = Self.FListData.dlItems;
         var $temp112;
         for(a$512=0,$temp112=a$513.length;a$512<$temp112;a$512++) {
            LItem$8 = a$513[a$512];
            if (!LItem$8.diFileType) {
               Result.push(LItem$8);
            }
         }
      }
      return Result
   }
   /// function TNJFileWalker.ExtractFolders() : TNJFileItems
   ,ExtractFolders:function(Self) {
      var Result = [];
      var a$514 = 0;
      var LItem$9 = null;
      if (!Self.FBusy) {
         var a$515 = [];
         a$515 = Self.FListData.dlItems;
         var $temp113;
         for(a$514=0,$temp113=a$515.length;a$514<$temp113;a$514++) {
            LItem$9 = a$515[a$514];
            if (LItem$9.diFileType==1) {
               Result.push(LItem$9);
            }
         }
      }
      return Result
   }
   /// procedure TNJFileWalker.ForEach(const Delegate: TNJFileWalkerForEachDelegate)
   ,ForEach$6:function(Self, Delegate) {
      var x$65 = 0;
      if (Delegate) {
         var $temp114;
         for(x$65=0,$temp114=TNJFileWalker.GetItemCount$(Self);x$65<$temp114;x$65++) {
            if (Delegate(TNJFileWalker.GetItem$2$(Self,x$65))==16) {
               break;
            }
         }
      }
   }
   /// function TNJFileWalker.GetItem(Index: Integer) : TNJFileItem
   ,GetItem$2:function(Self, Index$12) {
      return Self.FListData.dlItems[Index$12];
   }
   /// function TNJFileWalker.GetItemCount() : Integer
   ,GetItemCount:function(Self) {
      return Self.FListData.dlItems.length;
   }
   /// procedure TNJFileWalker.NextOrDone()
   ,NextOrDone:function(Self) {
      function SignalDone() {
         Self.FBusy = false;
         TW3Dispatch.Execute(TW3Dispatch,$Event0(Self,TNJFileWalker.DoAfterWalk$),Self.FDelay$2);
      };
      if (Self.FCancel) {
         SignalDone();
         return;
      }
      if (Self.FItemStack.length>0) {
         TW3Dispatch.Execute(TW3Dispatch,$Event0(Self,TNJFileWalker.ProcessFromStack),Self.FDelay$2);
      } else {
         SignalDone();
      }
   }
   /// procedure TNJFileWalker.ProcessFromStack()
   ,ProcessFromStack:function(Self) {
      var LKeep = {v:false},
         FileObjName = "";
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (Self.FCancel) {
         TNJFileWalker.NextOrDone(Self);
         return;
      }
      if (Self.FItemStack.length>0) {
         LKeep.v = true;
         FileObjName = Self.FItemStack.pop();
         Self.FCurrent$3 = NodePathAPI().normalize(IncludeTrailingPathDelimiter$4(Self.FPath$1)+FileObjName);
         NodeFsAPI().stat(Self.FCurrent$3,function (err$12, stats$9) {
            var LItem$10 = null,
               LTemp$26 = "";
            if (err$12) {
               TW3ErrorObject.SetLastError$(Self,err$12.message);
               TNJFileWalker.NextOrDone(Self);
               return;
            }
            LItem$10 = new TNJFileItem();
            LItem$10.diFileName = FileObjName;
            LItem$10.diFileType = (stats$9.isFile())?0:1;
            LItem$10.diFileSize = (stats$9.isFile())?stats$9.size:0;
            LItem$10.diCreated = (stats$9.ctime!==null)?JDateToDateTime(stats$9.ctime):Now();
            LItem$10.diModified = (stats$9.mtime!==null)?JDateToDateTime(stats$9.mtime):Now();
            if (stats$9.isFile()) {
               LTemp$26 = "";
               LTemp$26 = '0' + ((stats$9).mode & parseInt('777', 8)).toString(8);
               LItem$10.diFileMode = LTemp$26;
            }
            if (Self.OnIncludeFile) {
               Self.OnIncludeFile(Self,LItem$10,LKeep);
            }
            if (LKeep.v) {
               Self.FListData.dlItems.push(LItem$10);
            } else {
               LItem$10 = null;
            }
            Self.FCurrent$3 = "";
            TW3Dispatch.Execute(TW3Dispatch,$Event0(Self,TNJFileWalker.NextOrDone),Self.FDelay$2);
         });
      } else {
         TNJFileWalker.NextOrDone(Self);
      }
   }
   /// procedure TNJFileWalker.SetDelay(const NewDelay: Integer)
   ,SetDelay$2:function(Self, NewDelay$2) {
      Self.FDelay$2 = TInteger.EnsureRange(NewDelay$2,1,2147483647);
   }
   /// function TNJFileWalker.ToJSON() : String
   ,ToJSON$3:function(Self) {
      return JSON.stringify(Self.FListData);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3$:function($){return $.ClassType.Create$3($)}
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,DoAfterWalk$:function($){return $.ClassType.DoAfterWalk($)}
   ,DoBeforeWalk$:function($){return $.ClassType.DoBeforeWalk($)}
   ,GetItem$2$:function($){return $.ClassType.GetItem$2.apply($.ClassType, arguments)}
   ,GetItemCount$:function($){return $.ClassType.GetItemCount($)}
   ,SetDelay$2$:function($){return $.ClassType.SetDelay$2.apply($.ClassType, arguments)}
};
TNJFileWalker.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// ENJFileWalker = class (EW3Exception)
var ENJFileWalker = {
   $ClassName:"ENJFileWalker",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// Tspawn_options_object = class (TObject)
var Tspawn_options_object = {
   $ClassName:"Tspawn_options_object",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.detached = false;
      $.stdio = $.custom = $.env = undefined;
      $.cwd = "";
   }
   ,Destroy:TObject.Destroy
};
/// Tfork_options_object = class (TObject)
var Tfork_options_object = {
   $ClassName:"Tfork_options_object",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.cwd = $.encoding = "";
      $.env = undefined;
   }
   ,Destroy:TObject.Destroy
};
/// Texec_options_object = class (TObject)
var Texec_options_object = {
   $ClassName:"Texec_options_object",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.killSignal = $.encoding = $.cwd = "";
      $.maxBuffer = $.timeout = 0;
      $.stdio = $.customFds = $.env = undefined;
   }
   ,Destroy:TObject.Destroy
};
/// TexecFile_options_object = class (TObject)
var TexecFile_options_object = {
   $ClassName:"TexecFile_options_object",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.killSignal = $.maxBuffer = $.encoding = $.cwd = "";
      $.timeout = 0;
      $.stdio = $.customFds = $.env = undefined;
   }
   ,Destroy:TObject.Destroy
};
function child_process() {
   return require("child_process");
};
function NodeJSOsAPI() {
   return require("os");
};
/// Tlisteners_result_object = class (TObject)
var Tlisteners_result_object = {
   $ClassName:"Tlisteners_result_object",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.Function = undefined;
   }
   ,Destroy:TObject.Destroy
};
function NodeJSClusterAPI() {
   return require("cluster");
};
/// TNJHttpsServerMode enumeration
var TNJHttpsServerMode = [ "ssmCertificate", "ssmPfxKey" ];
/// TNJHTTPSServer = class (TNJCustomServer)
var TNJHTTPSServer = {
   $ClassName:"TNJHTTPSServer",$Parent:TNJCustomServer
   ,$Init:function ($) {
      TNJCustomServer.$Init($);
      $.OnRequest = null;
      $.FCerts = $.FCiphers = null;
      $.FMode = 0;
      $.FOptions$7 = [0];
   }
   /// anonymous TSourceMethodSymbol
   ,a$261:function(Self) {
      return TNJCustomServer.GetHandle$1$(Self);
   }
   /// constructor TNJHTTPSServer.Create()
   ,Create$3:function(Self) {
      TW3ErrorObject.Create$3(Self);
      Self.FCiphers = TNJHttpsCiphers.Create$91$($New(TNJHttpsCiphers),Self);
      Self.FCerts = TNJHTTPSCertificates.Create$92$($New(TNJHTTPSCertificates),Self);
      Self.FOptions$7 = [4];
      Self.FMode = 0;
      return Self
   }
   /// destructor TNJHTTPSServer.Destroy()
   ,Destroy:function(Self) {
      TObject.Free(Self.FCiphers);
      TObject.Free(Self.FCerts);
      TW3ErrorObject.Destroy(Self);
   }
   /// procedure TNJHTTPSServer.Dispatch(request: JServerRequest; response: JServerResponse)
   ,Dispatch$3:function(Self, request$3, response$1) {
      var LRequest$1 = null,
         LResponse$2 = null;
      if (Self.OnRequest) {
         LRequest$1 = TNJHttpRequest.Create$88$($New(TNJHttpRequest),Self,request$3);
         try {
            LResponse$2 = TNJHttpResponse.Create$87$($New(TNJHttpResponse),Self,response$1);
            try {
               try {
                  Self.OnRequest(Self,LRequest$1,LResponse$2);
               } catch ($e) {
                  var e$67 = $W($e);
                  throw EW3Exception.CreateFmt$($New(ENJHttpServerError),"Dispatch failed, system threw exception %s with message [%s]",[TObject.ClassName(e$67.ClassType), e$67.FMessage]);
               }
            } finally {
               TObject.Free(LResponse$2);
            }
         } finally {
            TObject.Free(LRequest$1);
         }
      }
   }
   /// procedure TNJHTTPSServer.InternalSetActive(const Value: Boolean)
   ,InternalSetActive$2:function(Self, Value$185) {
      TNJCustomServer.SetActive$3(Self,Value$185);
   }
   /// procedure TNJHTTPSServer.SetActive(const Value: Boolean)
   ,SetActive$3:function(Self, Value$186) {
      if (Value$186!=TNJCustomServer.GetActive$2$(Self)) {
         TNJCustomServer.SetActive$3(Self,Value$186);
         try {
            if (TNJCustomServer.GetActive$2$(Self)) {
               TNJCustomServer.StartServer$(Self);
            } else {
               TNJCustomServer.StopServer$(Self);
            }
         } catch ($e) {
            var e$68 = $W($e);
            TNJCustomServer.SetActive$3(Self,(!Value$186))         }
      }
   }
   /// procedure TNJHTTPSServer.SetMode(const NewMode: TNJHttpsServerMode)
   ,SetMode:function(Self, NewMode$1) {
      if (TNJCustomServer.GetActive$2$(Self)) {
         throw Exception.Create($New(ENJHttpServerError),"Server mode cannot be altered while active error");
      } else {
         Self.FMode = NewMode$1;
      }
   }
   /// procedure TNJHTTPSServer.SetOptions(const NewPrefs: TNJHttpsServerOptions)
   ,SetOptions:function(Self, NewPrefs) {
      if (TNJCustomServer.GetActive$2$(Self)) {
         throw Exception.Create($New(ENJHttpServerError),"Server options cannot be altered while active error");
      } else {
         Self.FOptions$7 = NewPrefs.slice(0);
      }
   }
   /// procedure TNJHTTPSServer.StartServer()
   ,StartServer:function(Self) {
      var LServer$3 = null;
      var opts;
      opts = TVariant.CreateObject();
      switch (Self.FMode) {
         case 0 :
            opts["cert"] = Self.FCerts.FCert;
            opts["ca"] = Self.FCerts.FCa;
            opts["key"] = Self.FCerts.FKey;
            break;
         case 1 :
            opts["pfx"] = Self.FCerts.FPFX;
            opts["passphrase"] = Self.FCerts.FPFXPass;
            break;
      }
      opts["requestCert"] = $SetIn(Self.FOptions$7,0,0,4);
      opts["rejectUnauthorized"] = $SetIn(Self.FOptions$7,1,0,4);
      if ($SetIn(Self.FOptions$7,3,0,4)) {
         if (!TNJHttpsCiphers.a$265(Self.FCiphers)) {
            opts["ciphers"] = TNJHttpsCiphers.ToString$10$(Self.FCiphers);
            opts["honorCipherOrder"] = $SetIn(Self.FOptions$7,2,0,4);
         }
      }
      try {
         LServer$3 = https().createServer(opts,$Event2(Self,TNJHTTPSServer.Dispatch$3$));
      } catch ($e) {
         var e$69 = $W($e);
         throw EW3Exception.CreateFmt$($New(ENJHttpServerError),"Failed to create NodeJS server object, system threw exception %s with message [%s]",[TObject.ClassName(e$69.ClassType), e$69.FMessage]);
      }
      try {
         LServer$3.listen(TNJCustomServer.GetPort$(Self),"");
      } catch ($e) {
         var e$70 = $W($e);
         LServer$3 = null;
         throw EW3Exception.CreateFmt$($New(ENJHttpServerError),"Failed to start server, system threw exception %s with message %s",[TObject.ClassName(e$70.ClassType), e$70.FMessage]);
      }
      TNJCustomServer.SetHandle$2$(Self,LServer$3);
      TNJCustomServer.AfterStart$(Self);
   }
   /// procedure TNJHTTPSServer.StopServer()
   ,StopServer:function(Self) {
      var cb$1 = null;
      cb$1 = function () {
         TNJHTTPSServer.InternalSetActive$2(Self,false);
         TNJCustomServer.AfterStop$(Self);
      };
      TNJCustomServer.GetHandle$1$(Self).close(cb$1);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3$:function($){return $.ClassType.Create$3($)}
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,AfterStart:TNJCustomServer.AfterStart
   ,AfterStop:TNJCustomServer.AfterStop
   ,BeforeStart:TNJCustomServer.BeforeStart
   ,BeforeStop:TNJCustomServer.BeforeStop
   ,GetActive$2:TNJCustomServer.GetActive$2
   ,GetHandle$1:TNJCustomServer.GetHandle$1
   ,GetPort:TNJCustomServer.GetPort
   ,SetActive$3$:function($){return $.ClassType.SetActive$3.apply($.ClassType, arguments)}
   ,SetHandle$2:TNJCustomServer.SetHandle$2
   ,SetPort:TNJCustomServer.SetPort
   ,Start:TNJCustomServer.Start
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,Stop:TNJCustomServer.Stop
   ,StopServer$:function($){return $.ClassType.StopServer($)}
   ,Dispatch$3$:function($){return $.ClassType.Dispatch$3.apply($.ClassType, arguments)}
};
TNJHTTPSServer.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TNJHttpsCiphers = class (TW3OwnedObject)
var TNJHttpsCiphers = {
   $ClassName:"TNJHttpsCiphers",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.Separator = "";
      $.FItems$1 = [];
   }
   /// anonymous TSourceMethodSymbol
   ,a$265:function(Self) {
      return Self.FItems$1.length<1;
   }
   /// anonymous TSourceMethodSymbol
   ,a$264:function(Self) {
      return Self.FItems$1.length;
   }
   /// anonymous TSourceMethodSymbol
   ,a$263:function(Self, index$17) {
      return Self.FItems$1[index$17];
   }
   /// function TNJHttpsCiphers.Add(CipherName: String) : Integer
   ,Add$4:function(Self, CipherName) {
      var Result = 0;
      var temp$23 = 0;
      Result = -1;
      CipherName = Trim$_String_(CipherName);
      if (CipherName.length>0) {
         temp$23 = Self.FItems$1.indexOf(CipherName);
         if (temp$23<0) {
            Self.FItems$1.push(CipherName);
            Result = (Self.FItems$1.length-1);
         } else {
            Result = temp$23;
         }
      }
      return Result
   }
   /// procedure TNJHttpsCiphers.AddCommonCiphers(const BlackListWeakCiphers: Boolean)
   ,AddCommonCiphers:function(Self, BlackListWeakCiphers) {
      var a$516 = 0;
      var el$6 = "",
         a$517 = 0;
      var el$7 = "";
      for(a$516=0;a$516<=11;a$516++) {
         el$6 = _Common_CIPHERS[a$516];
         TNJHttpsCiphers.Add$4(Self,el$6);
      }
      if (BlackListWeakCiphers) {
         for(a$517=0;a$517<=2;a$517++) {
            el$7 = _Common_BlackList[a$517];
            TNJHttpsCiphers.Add$4(Self,el$7);
         }
      }
   }
   /// procedure TNJHttpsCiphers.Clear()
   ,Clear$13:function(Self) {
      Self.FItems$1.length=0;
   }
   /// constructor TNJHttpsCiphers.Create(const AOwner: TNJHTTPSServer)
   ,Create$91:function(Self, AOwner$7) {
      TW3OwnedObject.Create$16(Self,AOwner$7);
      Self.Separator = ":";
      return Self
   }
   /// procedure TNJHttpsCiphers.Delete(CipherName: String)
   ,Delete$9:function(Self, CipherName$1) {
      var idx$5 = 0;
      idx$5 = TNJHttpsCiphers.IndexOf$4(Self,CipherName$1);
      if (idx$5>=0) {
         Self.FItems$1.splice(idx$5,1)
         ;
      }
   }
   /// procedure TNJHttpsCiphers.Delete(Index: Integer)
   ,Delete$8:function(Self, Index$13) {
      Self.FItems$1.splice(Index$13,1)
      ;
   }
   /// function TNJHttpsCiphers.GetOwner() : TNJHTTPSServer
   ,GetOwner$6:function(Self) {
      return $As(TW3OwnedObject.GetOwner(Self),TNJHTTPSServer);
   }
   /// function TNJHttpsCiphers.IndexOf(CipherName: String) : Integer
   ,IndexOf$4:function(Self, CipherName$2) {
      var Result = 0;
      CipherName$2 = Trim$_String_(CipherName$2);
      if (CipherName$2.length>0) {
         Result = Self.FItems$1.indexOf(CipherName$2);
      } else {
         Result = -1;
      }
      return Result
   }
   /// procedure TNJHttpsCiphers.Sort()
   ,Sort$1:function(Self) {
      if (Self.FItems$1.length>0) {
         Self.FItems$1.sort();
      }
   }
   /// function TNJHttpsCiphers.ToString() : String
   ,ToString$10:function(Self) {
      var Result = "";
      if (Self.FItems$1.length>0) {
         if (!$SetIn(TNJHttpsCiphers.GetOwner$6(Self).FOptions$7,2,0,4)) {
            Self.FItems$1.sort();
         }
         if (Self.Separator.length<1) {
            Self.Separator = ":";
         }
         Result = (Self.FItems$1).join(Self.Separator);
      }
      return Result
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
   ,Create$91$:function($){return $.ClassType.Create$91.apply($.ClassType, arguments)}
   ,ToString$10$:function($){return $.ClassType.ToString$10($)}
};
TNJHttpsCiphers.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TNJHTTPSCertificates = class (TW3OwnedObject)
var TNJHTTPSCertificates = {
   $ClassName:"TNJHTTPSCertificates",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.FCa = $.FCert = $.FKey = $.FPFX = $.FPFXPass = "";
   }
   /// procedure TNJHTTPSCertificates.Clear()
   ,Clear$14:function(Self) {
      Self.FCert = "";
      Self.FKey = "";
      Self.FCa = "";
      Self.FPFX = "";
      Self.FPFXPass = "";
   }
   /// constructor TNJHTTPSCertificates.Create(const AOwner: TNJHTTPSServer)
   ,Create$92:function(Self, AOwner$8) {
      TW3OwnedObject.Create$16(Self,AOwner$8);
      return Self
   }
   /// function TNJHTTPSCertificates.GetOwner() : TNJHTTPSServer
   ,GetOwner$7:function(Self) {
      return $As(TW3OwnedObject.GetOwner(Self),TNJHTTPSServer);
   }
   /// procedure TNJHTTPSCertificates.LoadCa(const Filename: String)
   ,LoadCa:function(Self, Filename$21) {
      Self.FCa = TW3NodeSyncFileAPI.ReadTextFile(Filename$21);
   }
   /// procedure TNJHTTPSCertificates.LoadCert(const Filename: String)
   ,LoadCert:function(Self, Filename$22) {
      Self.FCert = TW3NodeSyncFileAPI.ReadTextFile(Filename$22);
   }
   /// procedure TNJHTTPSCertificates.LoadKey(const Filename: String)
   ,LoadKey:function(Self, Filename$23) {
      Self.FKey = TW3NodeSyncFileAPI.ReadTextFile(Filename$23);
   }
   /// procedure TNJHTTPSCertificates.LoadPFX(const Filename: String)
   ,LoadPFX:function(Self, Filename$24) {
      Self.FPFX = TW3NodeSyncFileAPI.ReadTextFile(Filename$24);
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TW3OwnedObject.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
   ,Create$92$:function($){return $.ClassType.Create$92.apply($.ClassType, arguments)}
};
TNJHTTPSCertificates.$Intf={
   IW3OwnedObjectAccess:[TW3OwnedObject.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
function crypto$1() {
   return require("crypto");
};
function tls() {
   return require("tls");
};
/// TClearTextStream_getCipher_object = class (TObject)
var TClearTextStream_getCipher_object = {
   $ClassName:"TClearTextStream_getCipher_object",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.name = $.version = "";
   }
   ,Destroy:TObject.Destroy
};
/// TClearTextStream_address_object = class (TObject)
var TClearTextStream_address_object = {
   $ClassName:"TClearTextStream_address_object",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.port = 0;
      $.family = $.address = "";
   }
   ,Destroy:TObject.Destroy
};
/// JServerOptions = class (TObject)
var JServerOptions = {
   $ClassName:"JServerOptions",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.SNICallback = null;
      $.NPNProtocols = $.crl = $.ca = $.key = $.cert = $.pfx = undefined;
      $.rejectUnauthorized = $.requestCert = $.honorCipherOrder = false;
      $.ciphers = $.passphrase = "";
   }
   ,Destroy:TObject.Destroy
};
function https() {
   return require("https");
};
/// TNJWebSocketServerSecure = class (TNJWebSocketHybridServer)
var TNJWebSocketServerSecure = {
   $ClassName:"TNJWebSocketServerSecure",$Parent:TNJWebSocketHybridServer
   ,$Init:function ($) {
      TNJWebSocketHybridServer.$Init($);
      $.FHttps = null;
   }
   /// constructor TNJWebSocketServerSecure.Create()
   ,Create$3:function(Self) {
      TNJRawWebSocketServer.Create$3(Self);
      Self.FHttps = TW3ErrorObject.Create$3$($New(TNJHTTPSServer));
      return Self
   }
   /// destructor TNJWebSocketServerSecure.Destroy()
   ,Destroy:function(Self) {
      TObject.Free(Self.FHttps);
      TNJRawWebSocketServer.Destroy(Self);
   }
   /// function TNJWebSocketServerSecure.GetHttpServer() : TNJCustomServer
   ,GetHttpServer:function(Self) {
      return Self.FHttps;
   }
   /// function TNJWebSocketServerSecure.GetPort() : Integer
   ,GetPort:function(Self) {
      return TNJCustomServer.GetPort$(Self.FHttps);
   }
   /// procedure TNJWebSocketServerSecure.SetPort(const Value: Integer)
   ,SetPort:function(Self, Value$187) {
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TNJCustomServer.GetActive$2$(Self)) {
         TW3ErrorObject.SetLastError$(Self,"Port cannot be altered while server is active error");
      } else {
         TNJCustomServer.SetPort$(Self.FHttps,Value$187);
      }
   }
   /// procedure TNJWebSocketServerSecure.StartServer()
   ,StartServer:function(Self) {
      var LOptions$4,
         LServer$4;
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      TNJRawWebSocketServer.ClearLookupTable(Self);
      LOptions$4 = TVariant.CreateObject();
      LOptions$4["server"] = TNJCustomServer.GetHandle$1$(Self.FHttps);
      LOptions$4["clientTracking "] = Self.FTrack;
      LOptions$4["maxPayload"] = Self.FPayload;
      if (Self.FPath.length>0) {
         LOptions$4["path"] = Self.FPath;
      }
      LServer$4 = null;
      try {
         LServer$4 = WebSocketCreateServer(LOptions$4);
      } catch ($e) {
         var e$71 = $W($e);
         TW3ErrorObject.SetLastErrorF(Self,"Failed to create websocket server object, system threw exception %s with message [%s]",[TObject.ClassName(e$71.ClassType), e$71.FMessage]);
         return;
      }
      TNJCustomServer.SetHandle$2$(Self,LServer$4);
      LServer$4.on("error",function (error$10) {
         if (Self.OnError) {
            Self.OnError(Self,null,error$10);
         }
      });
      LServer$4.on("connection",$Event2(Self,TNJRawWebSocketServer.DoClientConnected$));
      TNJCustomServer.AfterStart$(Self);
   }
   /// procedure TNJWebSocketServerSecure.StopServer()
   ,StopServer:function(Self) {
      var LHandle$4 = undefined;
      LHandle$4 = TNJCustomServer.GetHandle$1$(Self);
      if (LHandle$4) {
         LHandle$4.close(function () {
            TNJCustomServer.SetActive$3$(Self.FHttps,false);
            TNJRawWebSocketServer.InternalSetActive(Self,false);
            TNJCustomServer.AfterStop$(Self);
            TNJRawWebSocketServer.ClearLookupTable(Self);
         });
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3$:function($){return $.ClassType.Create$3($)}
   ,GetExceptionClass:TNJRawWebSocketServer.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
   ,AfterStart:TNJCustomServer.AfterStart
   ,AfterStop:TNJCustomServer.AfterStop
   ,BeforeStart:TNJCustomServer.BeforeStart
   ,BeforeStop:TNJCustomServer.BeforeStop
   ,GetActive$2:TNJCustomServer.GetActive$2
   ,GetHandle$1:TNJCustomServer.GetHandle$1
   ,GetPort$:function($){return $.ClassType.GetPort($)}
   ,SetActive$3:TNJRawWebSocketServer.SetActive$3
   ,SetHandle$2:TNJCustomServer.SetHandle$2
   ,SetPort$:function($){return $.ClassType.SetPort.apply($.ClassType, arguments)}
   ,Start:TNJCustomServer.Start
   ,StartServer$:function($){return $.ClassType.StartServer($)}
   ,Stop:TNJCustomServer.Stop
   ,StopServer$:function($){return $.ClassType.StopServer($)}
   ,Dispatch$1:TNJRawWebSocketServer.Dispatch$1
   ,DoClientConnected:TNJRawWebSocketServer.DoClientConnected
   ,DoClientDisconnected:TNJRawWebSocketServer.DoClientDisconnected
   ,DoClientNameChange:TNJRawWebSocketServer.DoClientNameChange
   ,SetPath:TNJRawWebSocketServer.SetPath
   ,SetTracking:TNJRawWebSocketServer.SetTracking
   ,GetHttpServer$:function($){return $.ClassType.GetHttpServer($)}
};
TNJWebSocketServerSecure.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TQTXUserSession = class (TW3OwnedObject)
var TQTXUserSession = {
   $ClassName:"TQTXUserSession",$Parent:TW3OwnedObject
   ,$Init:function ($) {
      TW3OwnedObject.$Init($);
      $.FManager = null;
   }
   /// function TQTXUserSession.AcceptOwner(const CandidateObject: TObject) : Boolean
   ,AcceptOwner:function(Self, CandidateObject$8) {
      return CandidateObject$8!==null&&$Is(CandidateObject$8,TQTXUserSession);
   }
   /// constructor TQTXUserSession.Create(const Manager: TQTXSessionManager)
   ,Create$108:function(Self, Manager$1) {
      TW3OwnedObject.Create$16(Self,Manager$1);
      return Self
   }
   /// function TQTXUserSession.GetOwner() : TQTXSessionManager
   ,GetOwner$9:function(Self) {
      return Self.FManager;
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner$:function($){return $.ClassType.AcceptOwner.apply($.ClassType, arguments)}
   ,Create$16:TW3OwnedObject.Create$16
   ,Create$108$:function($){return $.ClassType.Create$108.apply($.ClassType, arguments)}
   ,GetIdentifier$1$:function($){return $.ClassType.GetIdentifier$1($)}
   ,GetPrivileges$1$:function($){return $.ClassType.GetPrivileges$1($)}
   ,GetRemoteHost$:function($){return $.ClassType.GetRemoteHost($)}
   ,GetRootFolder$1$:function($){return $.ClassType.GetRootFolder$1($)}
   ,GetState$2$:function($){return $.ClassType.GetState$2($)}
   ,SetIdentifier$1$:function($){return $.ClassType.SetIdentifier$1.apply($.ClassType, arguments)}
   ,SetPrivileges$1$:function($){return $.ClassType.SetPrivileges$1.apply($.ClassType, arguments)}
   ,SetRemoteHost$:function($){return $.ClassType.SetRemoteHost.apply($.ClassType, arguments)}
   ,SetRootFolder$1$:function($){return $.ClassType.SetRootFolder$1.apply($.ClassType, arguments)}
   ,SetState$3$:function($){return $.ClassType.SetState$3.apply($.ClassType, arguments)}
};
TQTXUserSession.$Intf={
   IW3OwnedObjectAccess:[TQTXUserSession.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TQTXUserSessionMemory = class (TQTXUserSession)
var TQTXUserSessionMemory = {
   $ClassName:"TQTXUserSessionMemory",$Parent:TQTXUserSession
   ,$Init:function ($) {
      TQTXUserSession.$Init($);
      $.FIdent = $.FRemote = $.FRootFolder = "";
      $.FPrivileges = [0];
      $.FState$2 = 0;
   }
   /// function TQTXUserSessionMemory.GetIdentifier() : String
   ,GetIdentifier$1:function(Self) {
      return Self.FIdent;
   }
   /// function TQTXUserSessionMemory.GetPrivileges() : TQTXAccountPrivileges
   ,GetPrivileges$1:function(Self) {
      return Self.FPrivileges.slice(0);
   }
   /// function TQTXUserSessionMemory.GetRemoteHost() : String
   ,GetRemoteHost:function(Self) {
      return Self.FRemote;
   }
   /// function TQTXUserSessionMemory.GetRootFolder() : String
   ,GetRootFolder$1:function(Self) {
      return Self.FRootFolder;
   }
   /// function TQTXUserSessionMemory.GetState() : TQTXAccountState
   ,GetState$2:function(Self) {
      return Self.FState$2;
   }
   /// procedure TQTXUserSessionMemory.SetIdentifier(NewIdentifier: String)
   ,SetIdentifier$1:function(Self, NewIdentifier$1) {
      Self.FIdent = NewIdentifier$1;
   }
   /// procedure TQTXUserSessionMemory.SetPrivileges(NewPrivileges: TQTXAccountPrivileges)
   ,SetPrivileges$1:function(Self, NewPrivileges$1) {
      Self.FPrivileges = NewPrivileges$1.slice(0);
   }
   /// procedure TQTXUserSessionMemory.SetRemoteHost(NewRemoteHost: String)
   ,SetRemoteHost:function(Self, NewRemoteHost) {
      Self.FRemote = NewRemoteHost;
   }
   /// procedure TQTXUserSessionMemory.SetRootFolder(NewRootFolder: String)
   ,SetRootFolder$1:function(Self, NewRootFolder$1) {
      Self.FRootFolder = NewRootFolder$1;
   }
   /// procedure TQTXUserSessionMemory.SetState(NewState: TQTXAccountState)
   ,SetState$3:function(Self, NewState$6) {
      Self.FState$2 = NewState$6;
   }
   ,Destroy:TObject.Destroy
   ,GetOwner:TW3OwnedObject.GetOwner
   ,SetOwner:TW3OwnedObject.SetOwner
   ,AcceptOwner:TQTXUserSession.AcceptOwner
   ,Create$16:TW3OwnedObject.Create$16
   ,Create$108:TQTXUserSession.Create$108
   ,GetIdentifier$1$:function($){return $.ClassType.GetIdentifier$1($)}
   ,GetPrivileges$1$:function($){return $.ClassType.GetPrivileges$1($)}
   ,GetRemoteHost$:function($){return $.ClassType.GetRemoteHost($)}
   ,GetRootFolder$1$:function($){return $.ClassType.GetRootFolder$1($)}
   ,GetState$2$:function($){return $.ClassType.GetState$2($)}
   ,SetIdentifier$1$:function($){return $.ClassType.SetIdentifier$1.apply($.ClassType, arguments)}
   ,SetPrivileges$1$:function($){return $.ClassType.SetPrivileges$1.apply($.ClassType, arguments)}
   ,SetRemoteHost$:function($){return $.ClassType.SetRemoteHost.apply($.ClassType, arguments)}
   ,SetRootFolder$1$:function($){return $.ClassType.SetRootFolder$1.apply($.ClassType, arguments)}
   ,SetState$3$:function($){return $.ClassType.SetState$3.apply($.ClassType, arguments)}
};
TQTXUserSessionMemory.$Intf={
   IW3OwnedObjectAccess:[TQTXUserSession.AcceptOwner,TW3OwnedObject.SetOwner,TW3OwnedObject.GetOwner]
}
/// TQTXSessionManager = class (TObject)
var TQTXSessionManager = {
   $ClassName:"TQTXSessionManager",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
   }
   ,Destroy:TObject.Destroy
   ,FindSessionByHost$:function($){return $.ClassType.FindSessionByHost.apply($.ClassType, arguments)}
   ,FindSessionbyId$:function($){return $.ClassType.FindSessionbyId.apply($.ClassType, arguments)}
   ,FlushSessions$:function($){return $.ClassType.FlushSessions.apply($.ClassType, arguments)}
   ,GetSessionByIndex$:function($){return $.ClassType.GetSessionByIndex.apply($.ClassType, arguments)}
   ,GetSessionCount$:function($){return $.ClassType.GetSessionCount.apply($.ClassType, arguments)}
   ,Register$2$:function($){return $.ClassType.Register$2.apply($.ClassType, arguments)}
   ,Register$1$:function($){return $.ClassType.Register$1.apply($.ClassType, arguments)}
};
/// TQTXSessionManagerMemory = class (TQTXSessionManager)
var TQTXSessionManagerMemory = {
   $ClassName:"TQTXSessionManagerMemory",$Parent:TQTXSessionManager
   ,$Init:function ($) {
      TQTXSessionManager.$Init($);
      $.FIPLookup$1 = $.FLookup$1 = null;
      $.FSessions$1 = [];
   }
   /// constructor TQTXSessionManagerMemory.Create()
   ,Create$109:function(Self) {
      TObject.Create(Self);
      Self.FLookup$1 = TW3CustomDictionary.Create$74$($New(TW3ObjDictionary));
      Self.FIPLookup$1 = TW3CustomDictionary.Create$74$($New(TW3ObjDictionary));
      return Self
   }
   /// destructor TQTXSessionManagerMemory.Destroy()
   ,Destroy:function(Self) {
      if (Self.FSessions$1.length>0) {
         TQTXSessionManager.FlushSessions$(Self,undefined,null);
      }
      TObject.Free(Self.FLookup$1);
      TObject.Free(Self.FIPLookup$1);
      TObject.Destroy(Self);
   }
   /// procedure TQTXSessionManagerMemory.FindSessionByHost(TagValue: Variant; RemoteHost: String; CB: TQTXUserSessionCB)
   ,FindSessionByHost:function(Self, TagValue$9, RemoteHost$1, CB$47) {
      var LObj$2 = null;
      var LError = null;
      RemoteHost$1 = (Trim$_String_(RemoteHost$1)).toLocaleLowerCase();
      if (TW3CustomDictionary.Contains(Self.FIPLookup$1,RemoteHost$1)) {
         LObj$2 = $As(TW3ObjDictionary.a$108(Self.FIPLookup$1,RemoteHost$1),TQTXUserSession);
         if (CB$47) {
            CB$47(TagValue$9,LObj$2,null);
         }
      } else {
         LError = EW3Exception.CreateFmt$($New(EW3Exception),"Unknown ip [%s], could not locate session error",[RemoteHost$1]);
         if (CB$47) {
            CB$47(TagValue$9,null,LError);
         } else {
            throw LError;
         }
      }
   }
   /// procedure TQTXSessionManagerMemory.FindSessionById(TagValue: Variant; Identifier: String; CB: TQTXUserSessionCB)
   ,FindSessionbyId:function(Self, TagValue$10, Identifier$6, CB$48) {
      var LObj$3 = null,
         LError$1 = null;
      Identifier$6 = (Trim$_String_(Identifier$6)).toLocaleLowerCase();
      if (TW3CustomDictionary.Contains(Self.FLookup$1,Identifier$6)) {
         LObj$3 = $As(TW3ObjDictionary.a$108(Self.FLookup$1,Identifier$6),TQTXUserSession);
         if (CB$48) {
            CB$48(TagValue$10,LObj$3,null);
         }
      } else {
         LError$1 = EW3Exception.CreateFmt$($New(EW3Exception),"Unknown session id [%s], could  not locate session error",[Identifier$6]);
         if (CB$48) {
            CB$48(TagValue$10,null,LError$1);
         } else {
            throw LError$1;
         }
      }
   }
   /// procedure TQTXSessionManagerMemory.FlushSessions(TagValue: Variant; CB: TQTXSessionCB)
   ,FlushSessions:function(Self, TagValue$11, CB$49) {
      var a$518 = 0;
      var item$8 = null;
      try {
         var a$519 = [];
         a$519 = Self.FSessions$1;
         var $temp115;
         for(a$518=0,$temp115=a$519.length;a$518<$temp115;a$518++) {
            item$8 = a$519[a$518];
            TObject.Free(item$8);
         }
      } finally {
         Self.FSessions$1.length=0;
         TW3CustomDictionary.Clear$7(Self.FLookup$1);
         if (CB$49) {
            CB$49(TagValue$11);
         }
      }
   }
   /// procedure TQTXSessionManagerMemory.GetSessionByIndex(TagValue: Variant; Index: Integer; CB: TQTXUserSessionCB)
   ,GetSessionByIndex:function(Self, TagValue$12, Index$14, CB$50) {
      var LObj$4 = null;
      try {
         LObj$4 = $As(Self.FSessions$1[Index$14],TQTXUserSessionMemory);
         if (CB$50) {
            CB$50(TagValue$12,LObj$4,null);
         }
      } catch ($e) {
         var e$72 = $W($e);
         if (CB$50) {
            CB$50(TagValue$12,null,e$72);
         } else {
            throw $e;
         }
      }
   }
   /// procedure TQTXSessionManagerMemory.GetSessionCount(TagValue: Variant; CB: TQTXUserSessionCountCB)
   ,GetSessionCount:function(Self, TagValue$13, CB$51) {
      if (CB$51) {
         CB$51(TagValue$13,Self.FSessions$1.length,null);
      }
   }
   /// procedure TQTXSessionManagerMemory.Register(TagValue: Variant; ThisSession: TQTXUserSession; CB: TQTXUserSessionCB)
   ,Register$2:function(Self, TagValue$14, ThisSession$1, CB$52) {
      var LRemoteIP$2 = "",
         LSessionID$2 = "",
         LError$2 = null;
      if (ThisSession$1!==null) {
         LRemoteIP$2 = TQTXUserSession.GetRemoteHost$(ThisSession$1);
         if (LRemoteIP$2.length>0) {
            if (TW3CustomDictionary.Contains(Self.FIPLookup$1,LRemoteIP$2)) {
               if (CB$52) {
                  CB$52(TagValue$14,ThisSession$1,null);
               }
               return;
            }
         }
         LSessionID$2 = (Trim$_String_(TQTXUserSession.GetIdentifier$1$(ThisSession$1))).toLocaleLowerCase();
         if (!TW3CustomDictionary.Contains(Self.FLookup$1,LSessionID$2)) {
            try {
               Self.FSessions$1.push(ThisSession$1);
               TW3ObjDictionary.a$107(Self.FLookup$1,LSessionID$2,ThisSession$1);
               if (LRemoteIP$2.length>0) {
                  TW3ObjDictionary.a$107(Self.FIPLookup$1,LRemoteIP$2,ThisSession$1);
               }
            } finally {
               if (CB$52) {
                  CB$52(TagValue$14,ThisSession$1,null);
               }
            }
         }
      } else {
         LError$2 = Exception.Create($New(EW3Exception),"A session with that Identifier already exists error");
         if (CB$52) {
            CB$52(TagValue$14,null,LError$2);
         } else {
            throw LError$2;
         }
      }
   }
   /// procedure TQTXSessionManagerMemory.Register(TagValue: Variant; RemoteHost: String; CB: TQTXUserSessionCB)
   ,Register$1:function(Self, TagValue$15, RemoteHost$2, CB$53) {
      var LSession$4 = null,
         LSessionID$3 = "";
      RemoteHost$2 = (Trim$_String_(RemoteHost$2)).toLocaleLowerCase();
      if (RemoteHost$2.length<1) {
         if (CB$53) {
            CB$53(TagValue$15,null,Exception.Create($New(EW3Exception),"Failed to add session, ip length was null"));
         }
      }
      if (TW3CustomDictionary.Contains(Self.FIPLookup$1,RemoteHost$2)) {
         if (CB$53) {
            CB$53(TagValue$15,$As(TW3ObjDictionary.a$108(Self.FIPLookup$1,RemoteHost$2),TQTXUserSession),null);
         }
         return;
      }
      LSession$4 = TQTXUserSession.Create$108$($New(TQTXUserSessionMemory),Self);
      TQTXUserSession.SetRemoteHost$(LSession$4,(Trim$_String_(RemoteHost$2)).toLocaleLowerCase());
      LSessionID$3 = (TQTXUserSession.GetIdentifier$1$(LSession$4)).toLocaleLowerCase();
      Self.FSessions$1.push(LSession$4);
      TW3ObjDictionary.a$107(Self.FLookup$1,LSessionID$3,LSession$4);
      TW3ObjDictionary.a$107(Self.FIPLookup$1,RemoteHost$2,LSession$4);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,FindSessionByHost$:function($){return $.ClassType.FindSessionByHost.apply($.ClassType, arguments)}
   ,FindSessionbyId$:function($){return $.ClassType.FindSessionbyId.apply($.ClassType, arguments)}
   ,FlushSessions$:function($){return $.ClassType.FlushSessions.apply($.ClassType, arguments)}
   ,GetSessionByIndex$:function($){return $.ClassType.GetSessionByIndex.apply($.ClassType, arguments)}
   ,GetSessionCount$:function($){return $.ClassType.GetSessionCount.apply($.ClassType, arguments)}
   ,Register$2$:function($){return $.ClassType.Register$2.apply($.ClassType, arguments)}
   ,Register$1$:function($){return $.ClassType.Register$1.apply($.ClassType, arguments)}
   ,Create$109$:function($){return $.ClassType.Create$109($)}
};
/// TQTXAccountState enumeration
var TQTXAccountState = [ "asActive", "asLocked", "asUnConfirmed" ];
/// TQTXAccountPrivilege enumeration
var TQTXAccountPrivilege = [ "apExecute", "apRead", "apWrite", "apCreate", "apInstall", "apAppstore" ];
/// TSubscriptionActivity enumeration
var TSubscriptionActivity = [ "saDispatch", "saClose", "saRemoved" ];
/// TMessageSubscription = class (TObject)
var TMessageSubscription = {
   $ClassName:"TMessageSubscription",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.Identifier = "";
      $.TagValue = undefined;
      $.Callback = null;
   }
   ,Destroy:TObject.Destroy
};
/// TMessageSubscribers = class (TObject)
var TMessageSubscribers = {
   $ClassName:"TMessageSubscribers",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FChannel = $.FLUT$2 = null;
      $.FItems$4 = [];
   }
   /// anonymous TSourceMethodSymbol
   ,a$449:function(Self) {
      return Self.FItems$4.length>0;
   }
   /// function TMessageSubscribers.AddSubscription(TagValue: Variant; const CB: TSubScriberCallback) : String
   ,AddSubscription:function(Self, TagValue$16, CB$54) {
      var Result = "";
      var LObj$5 = null;
      Result = TString.CreateGUID(TString);
      LObj$5 = TObject.Create($New(TMessageSubscription));
      LObj$5.Identifier = Result;
      LObj$5.TagValue = TagValue$16;
      TW3ObjDictionary.a$107(Self.FLUT$2,Result,LObj$5);
      Self.FItems$4.push(LObj$5);
      return Result
   }
   /// procedure TMessageSubscribers.Clear()
   ,Clear$24:function(Self) {
      if (Self.FItems$4.length>0) {
         TMessageSubscribers.Clear$23(Self,false);
      }
   }
   /// procedure TMessageSubscribers.Clear(const Silent: Boolean)
   ,Clear$23:function(Self, Silent) {
      var a$520 = 0;
      var LItem$11 = null;
      if (Self.FItems$4.length>0) {
         try {
            var a$521 = [];
            a$521 = Self.FItems$4;
            var $temp116;
            for(a$520=0,$temp116=a$521.length;a$520<$temp116;a$520++) {
               LItem$11 = a$521[a$520];
               TMessageSubscribers.RemoveSubscription(Self,LItem$11.Identifier,Silent);
            }
         } finally {
            Self.FItems$4.length=0;
            TW3CustomDictionary.Clear$7(Self.FLUT$2);
         }
      }
   }
   /// procedure TMessageSubscribers.Close()
   ,Close$6:function(Self) {
      var a$522 = 0;
      var LObj$6 = null;
      if (Self.FItems$4.length>0) {
         try {
            var a$523 = [];
            a$523 = Self.FItems$4;
            var $temp117;
            for(a$522=0,$temp117=a$523.length;a$522<$temp117;a$522++) {
               LObj$6 = a$523[a$522];
               if (LObj$6.Callback) {
                  LObj$6.Callback(LObj$6.TagValue,1,Self.FChannel,null);
               }
            }
         } finally {
            TMessageSubscribers.Clear$24(Self);
         }
      }
   }
   /// constructor TMessageSubscribers.Create(const Channel: TChannel)
   ,Create$110:function(Self, Channel$1) {
      TObject.Create(Self);
      Self.FChannel = Channel$1;
      Self.FLUT$2 = TW3CustomDictionary.Create$74$($New(TW3ObjDictionary));
      return Self
   }
   /// destructor TMessageSubscribers.Destroy()
   ,Destroy:function(Self) {
      if (Self.FItems$4.length>0) {
         TMessageSubscribers.Clear$23(Self,false);
      }
      TObject.Free(Self.FLUT$2);
      TObject.Destroy(Self);
   }
   /// procedure TMessageSubscribers.Dispatch(const Message: TObject)
   ,Dispatch$4:function(Self, Message$3) {
      var a$524 = 0;
      var LObj$7 = null;
      if (Message$3!==null) {
         if (Self.FItems$4.length>0) {
            var a$525 = [];
            a$525 = Self.FItems$4;
            var $temp118;
            for(a$524=0,$temp118=a$525.length;a$524<$temp118;a$524++) {
               LObj$7 = a$525[a$524];
               try {
                  if (LObj$7.Callback) {
                     LObj$7.Callback(LObj$7.TagValue,0,Self.FChannel,Message$3);
                  }
               } catch ($e) {
                  /* null */
               }
            }
         }
      } else {
         throw Exception.Create($New(EChannelError),"Failed to dispatch message, instance was nil error");
      }
   }
   /// procedure TMessageSubscribers.RemoveSubscription(Identifier: String; Silent: Boolean)
   ,RemoveSubscription:function(Self, Identifier$7, Silent$1) {
      var LObj$8 = null,
         x$66 = 0;
      if (TW3CustomDictionary.Contains(Self.FLUT$2,Identifier$7)) {
         LObj$8 = $As(TW3ObjDictionary.a$108(Self.FLUT$2,Identifier$7),TMessageSubscription);
         TW3CustomDictionary.Delete$2$(Self.FLUT$2,Identifier$7);
         var $temp119;
         for(x$66=0,$temp119=Self.FItems$4.length;x$66<$temp119;x$66++) {
            if (Self.FItems$4[x$66].Identifier==Identifier$7) {
               Self.FItems$4.splice(x$66,1)
               ;
               break;
            }
         }
         if (!Silent$1) {
            if (LObj$8.Callback) {
               LObj$8.Callback(LObj$8.TagValue,2,Self.FChannel,null);
            }
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$110$:function($){return $.ClassType.Create$110.apply($.ClassType, arguments)}
};
/// TChannelManager = class (TObject)
var TChannelManager = {
   $ClassName:"TChannelManager",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FLUT$3 = null;
   }
   /// procedure TChannelManager.CloseAll()
   ,CloseAll:function(Self) {
      TW3CustomDictionary.ForEach$4(Self.FLUT$3,function (ItemKey$13, KeyData) {
         var Result = 160;
         TChannelManager.CloseChannel(Self,ItemKey$13);
         Result = 160;
         return Result
      });
   }
   /// procedure TChannelManager.CloseChannel(Identifier: String)
   ,CloseChannel:function(Self, Identifier$8) {
      var LChannel = null;
      Identifier$8 = Trim$_String_(Identifier$8);
      LChannel = (TW3CustomDictionary.Contains(Self.FLUT$3,Identifier$8))?$As(TW3ObjDictionary.a$108(Self.FLUT$3,Identifier$8),TChannel):null;
      if (LChannel!==null) {
         TMessageSubscribers.Close$6(LChannel.FItems$5);
         TChannel.Close$7(LChannel);
         TW3CustomDictionary.Delete$2$(Self.FLUT$3,Identifier$8);
         TObject.Free(LChannel);
      } else {
         throw EW3Exception.CreateFmt$($New(EChannelError),"Channel found found error [%s]",[Identifier$8]);
      }
   }
   /// constructor TChannelManager.Create()
   ,Create$111:function(Self) {
      TObject.Create(Self);
      Self.FLUT$3 = TW3CustomDictionary.Create$74$($New(TW3ObjDictionary));
      return Self
   }
   /// function TChannelManager.CreateChannel(TagValue: Variant) : TChannel
   ,CreateChannel$1:function(Self, TagValue$17) {
      var Result = null;
      Result = TChannel.Create$112$($New(TChannel),Self,"");
      TW3ObjDictionary.a$107(Self.FLUT$3,Result.FId$1,Result);
      Result.TagValue = TagValue$17;
      return Result
   }
   /// function TChannelManager.CreateChannel(Identifier: String; TagValue: Variant) : TChannel
   ,CreateChannel:function(Self, Identifier$9, TagValue$18) {
      var Result = null;
      Identifier$9 = Trim$_String_(Identifier$9);
      if (TW3CustomDictionary.Contains(Self.FLUT$3,Identifier$9)) {
         throw EW3Exception.CreateFmt$($New(EChannelError),"Failed to create channel [%s], route already exists error",[Identifier$9]);
      }
      Result = TChannel.Create$112$($New(TChannel),Self,Identifier$9);
      TW3ObjDictionary.a$107(Self.FLUT$3,Result.FId$1,Result);
      Result.TagValue = TagValue$18;
      return Result
   }
   /// destructor TChannelManager.Destroy()
   ,Destroy:function(Self) {
      TObject.Free(Self.FLUT$3);
      TObject.Destroy(Self);
   }
   /// function TChannelManager.GetChannelById(Identifier: String) : TChannel
   ,GetChannelById:function(Self, Identifier$10) {
      return (TW3CustomDictionary.Contains(Self.FLUT$3,Identifier$10))?$As(TW3ObjDictionary.a$108(Self.FLUT$3,Identifier$10),TChannel):null;
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$111$:function($){return $.ClassType.Create$111($)}
};
/// TChannel = class (TObject)
var TChannel = {
   $ClassName:"TChannel",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.TagValue = undefined;
      $.FId$1 = "";
      $.FItems$5 = $.FManager$1 = null;
   }
   /// procedure TChannel.Close()
   ,Close$7:function(Self) {
      try {
         if (!TMessageSubscribers.a$449(Self.FItems$5)) {
            try {
               TMessageSubscribers.Close$6(Self.FItems$5);
            } catch ($e) {
               /* null */
            }
         }
      } finally {
         TMessageSubscribers.Clear$24(Self.FItems$5);
      }
   }
   /// constructor TChannel.Create(const Manager: TChannelManager; ChannelName: String)
   ,Create$112:function(Self, Manager$2, ChannelName) {
      TObject.Create(Self);
      Self.FItems$5 = TMessageSubscribers.Create$110$($New(TMessageSubscribers),Self);
      Self.FManager$1 = Manager$2;
      ChannelName = Trim$_String_(ChannelName);
      Self.FId$1 = (ChannelName.length>0)?ChannelName:TChannel.GetChannelId$(Self);
      return Self
   }
   /// destructor TChannel.Destroy()
   ,Destroy:function(Self) {
      try {
         TChannel.Close$7(Self);
      } finally {
         TObject.Free(Self.FItems$5);
      }
      TObject.Destroy(Self);
   }
   /// procedure TChannel.Dispatch(const Message: TObject)
   ,Dispatch$5:function(Self, Message$4) {
      if (Message$4!==null) {
         try {
            TMessageSubscribers.Dispatch$4(Self.FItems$5,Message$4);
         } catch ($e) {
            var e$73 = $W($e);
            throw EW3Exception.CreateFmt$($New(EChannelError),"Failed to dispatch message on channel [%s]: %s",[Self.FId$1, e$73.FMessage]);
         }
      } else {
         throw Exception.Create($New(EChannelError),"Failed to dispatch message, instance was nil error");
      }
   }
   /// function TChannel.GetChannelId() : String
   ,GetChannelId:function(Self) {
      return TString.CreateGUID(TString);
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$112$:function($){return $.ClassType.Create$112.apply($.ClassType, arguments)}
   ,GetChannelId$:function($){return $.ClassType.GetChannelId($)}
};
/// EChannelError = class (EW3Exception)
var EChannelError = {
   $ClassName:"EChannelError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// TNJLogFile = class (TW3ErrorObject)
var TNJLogFile = {
   $ClassName:"TNJLogFile",$Parent:TW3ErrorObject
   ,$Init:function ($) {
      TW3ErrorObject.$Init($);
      $.OnLogClosed = null;
      $.OnLogOpen = null;
      $.OnTextWritten = null;
      $.FLogFile = null;
      $.FLogFilePath = "";
      $.FOptions$8 = [0];
   }
   /// anonymous TSourceMethodSymbol
   ,a$345:function(Self) {
      return Self.FLogFile!==null;
   }
   /// procedure TNJLogFile.Close()
   ,Close$5:function(Self) {
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (Self.FLogFile!==null) {
         try {
            try {
               Self.FLogFile.destroy();
            } catch ($e) {
               /* null */
            }
         } finally {
            Self.FLogFile = null;
            if (Self.OnLogClosed) {
               Self.OnLogClosed(Self);
            }
         }
      }
   }
   /// constructor TNJLogFile.Create()
   ,Create$3:function(Self) {
      TW3ErrorObject.Create$3(Self);
      Self.FOptions$8 = [7];
      Self.FOptions.AutoResetError = true;
      Self.FOptions.AutoWriteToConsole = true;
      Self.FOptions.ThrowExceptions = true;
      return Self
   }
   /// destructor TNJLogFile.Destroy()
   ,Destroy:function(Self) {
      if (Self.FLogFile!==null) {
         TNJLogFile.Close$5(Self);
      }
      TW3ErrorObject.Destroy(Self);
   }
   /// procedure TNJLogFile.Open(LogFileName: String)
   ,Open$2:function(Self, LogFileName$1) {
      var fs = null,
         options$1 = null;
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (Self.FLogFile!==null) {
         TNJLogFile.Close$5(Self);
      }
      fs = NodeFsAPI();
      if ($SetIn(Self.FOptions$8,0,0,3)) {
         if (FileExists$2(LogFileName$1)) {
            try {
               TW3NodeSyncFileAPI.DeleteFile(LogFileName$1);
            } catch ($e) {
               var e$74 = $W($e);
               TW3ErrorObject.SetLastErrorF(Self,"Failed to delete existing logfile (%s): %s",[LogFileName$1, e$74.FMessage]);
               return;
            }
         }
      }
      options$1 = TObject.Create($New(TCreateWriteStreamOptions));
      options$1.flags = "a";
      try {
         Self.FLogFile = fs.createWriteStream(LogFileName$1,options$1);
      } catch ($e) {
         var e$75 = $W($e);
         Self.FLogFile = null;
         TW3ErrorObject.SetLastErrorF(Self,"Failed to create logfile (%s): %s",[LogFileName$1, e$75.FMessage]);
         return;
      }
      Self.FLogFilePath = LogFileName$1;
      if (Self.OnLogOpen) {
         Self.OnLogOpen(Self);
      }
   }
   /// procedure TNJLogFile.SetOptions(NewOptions: TNJLogFileOptions)
   ,SetOptions$1:function(Self, NewOptions$2) {
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TNJLogFile.a$345(Self)) {
         TW3ErrorObject.SetLastError$(Self,"Cannot change options while active error");
      } else {
         Self.FOptions$8 = NewOptions$2.slice(0);
      }
   }
   /// procedure TNJLogFile.Write(Text: String)
   ,Write$11:function(Self, Text$42) {
      var LBaseText = "";
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TNJLogFile.a$345(Self)) {
         LBaseText = Text$42;
         if ($SetIn(Self.FOptions$8,1,0,3)) {
            if ((!StrEndsWith(Text$42,"\r\n"))&&(!StrEndsWith(Text$42,"\r"))) {
               Text$42+="\r\n";
            }
         }
         if ($SetIn(Self.FOptions$8,2,0,3)) {
            Text$42 = TimeToStr(Now(),0)+" "+Text$42;
         }
         try {
            Self.FLogFile.write(Text$42,"utf8");
         } catch ($e) {
            var e$76 = $W($e);
            TW3ErrorObject.SetLastErrorF(Self,"Failed to write to logfile(%s): %s",[Self.FLogFilePath, e$76.FMessage]);
            return;
         }
         if (Self.OnTextWritten) {
            Self.OnTextWritten(Self,LBaseText);
         }
      } else {
         TW3ErrorObject.SetLastError$(Self,"Failed to write data, logfile is closed error");
      }
   }
   /// procedure TNJLogFile.WriteF(Text: String; const Values: array of const)
   ,WriteF:function(Self, Text$43, Values$15) {
      if (Self.FOptions.AutoResetError) {
         TW3ErrorObject.ClearLastError(Self);
      }
      if (TNJLogFile.a$345(Self)) {
         if (Text$43.length>0) {
            TNJLogFile.Write$11(Self,Format(Text$43,Values$15.slice(0)));
         }
      } else {
         TW3ErrorObject.SetLastError$(Self,"Failed to write data, logfile is closed error");
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$3$:function($){return $.ClassType.Create$3($)}
   ,GetExceptionClass:TW3ErrorObject.GetExceptionClass
   ,SetLastError:TW3ErrorObject.SetLastError
};
TNJLogFile.$Intf={
   IW3ErrorAccess:[TW3ErrorObject.GetFailed,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
   ,IW3ErrorObject:[TW3ErrorObject.GetFailed,TW3ErrorObject.SetLastErrorF,TW3ErrorObject.SetLastError,TW3ErrorObject.GetLastError,TW3ErrorObject.ClearLastError]
}
/// TCustomCodec = class (TObject)
var TCustomCodec = {
   $ClassName:"TCustomCodec",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FBindings = [];
      $.FCodecInfo = null;
   }
   /// constructor TCustomCodec.Create()
   ,Create$28:function(Self) {
      TObject.Create(Self);
      Self.FCodecInfo = TCustomCodec.MakeCodecInfo$(Self);
      if (Self.FCodecInfo===null) {
         throw EW3Exception.Create$27$($New(ECodecError),"TCustomCodec.Create",Self,"Internal codec error, failed to obtain registration info error");
      }
      return Self
   }
   /// destructor TCustomCodec.Destroy()
   ,Destroy:function(Self) {
      TObject.Free(Self.FCodecInfo);
      TObject.Destroy(Self);
   }
   /// function TCustomCodec.MakeCodecInfo() : TCodecInfo
   ,MakeCodecInfo:function(Self) {
      return null;
   }
   /// procedure TCustomCodec.RegisterBinding(const Binding: TCodecBinding)
   ,RegisterBinding:function(Self, Binding) {
      if (Self.FBindings.indexOf(Binding)<0) {
         Self.FBindings.push(Binding);
      } else {
         throw EW3Exception.Create$27$($New(ECodecError),"TCustomCodec.RegisterBinding",Self,"Binding already connected to codec error");
      }
   }
   /// procedure TCustomCodec.UnRegisterBinding(const Binding: TCodecBinding)
   ,UnRegisterBinding:function(Self, Binding$1) {
      var LIndex$2 = 0;
      LIndex$2 = Self.FBindings.indexOf(Binding$1);
      if (LIndex$2>=0) {
         Self.FBindings.splice(LIndex$2,1)
         ;
      } else {
         throw EW3Exception.Create$27$($New(ECodecError),"TCustomCodec.UnRegisterBinding",Self,"Binding not connected to this codec error");
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$28$:function($){return $.ClassType.Create$28($)}
   ,DecodeData$:function($){return $.ClassType.DecodeData.apply($.ClassType, arguments)}
   ,EncodeData$:function($){return $.ClassType.EncodeData.apply($.ClassType, arguments)}
   ,MakeCodecInfo$:function($){return $.ClassType.MakeCodecInfo($)}
};
TCustomCodec.$Intf={
   ICodecBinding:[TCustomCodec.RegisterBinding,TCustomCodec.UnRegisterBinding]
   ,ICodecProcess:[TCustomCodec.EncodeData,TCustomCodec.DecodeData]
}
/// TUTF8Codec = class (TCustomCodec)
var TUTF8Codec = {
   $ClassName:"TUTF8Codec",$Parent:TCustomCodec
   ,$Init:function ($) {
      TCustomCodec.$Init($);
   }
   /// function TUTF8Codec.CanUseClampedArray() : Boolean
   ,CanUseClampedArray:function() {
      var Result = {v:false};
      try {
         var LTemp$27 = undefined;
         try {
            LTemp$27 = new Uint8ClampedArray(10);
         } catch ($e) {
            var e$77 = $W($e);
            return Result.v;
         }
         if (LTemp$27) {
            Result.v = true;
         }
      } finally {return Result.v}
   }
   /// procedure TUTF8Codec.EncodeData(const Source: IBinaryTransport; const Target: IBinaryTransport)
   ,EncodeData:function(Self, Source$9, Target$6) {
      var LReader$2 = null,
         LWriter$2 = null,
         BytesToRead$3 = 0,
         LCache$4 = [],
         Text$44 = "";
      LReader$2 = TW3CustomReader.Create$39$($New(TStreamReader),Source$9);
      try {
         LWriter$2 = TW3CustomWriter.Create$29$($New(TStreamWriter),Target$6);
         try {
            BytesToRead$3 = TW3CustomReader.GetTotalSize$2$(LReader$2)-TW3CustomReader.GetReadOffset$(LReader$2);
            if (BytesToRead$3>0) {
               LCache$4 = TW3CustomReader.Read$2(LReader$2,BytesToRead$3);
               Text$44 = TDatatype.BytesToString$1(TDatatype,LCache$4);
               LCache$4.length=0;
               LCache$4 = TUTF8Codec.Encode$(Self,Text$44);
               TW3CustomWriter.Write$1(LWriter$2,LCache$4);
            }
         } finally {
            TObject.Free(LWriter$2);
         }
      } finally {
         TObject.Free(LReader$2);
      }
   }
   /// procedure TUTF8Codec.DecodeData(const Source: IBinaryTransport; const Target: IBinaryTransport)
   ,DecodeData:function(Self, Source$10, Target$7) {
      var LReader$3 = null,
         LWriter$3 = null,
         BytesToRead$4 = 0,
         LCache$5 = [],
         Text$45 = "";
      LReader$3 = TW3CustomReader.Create$39$($New(TReader),Source$10);
      try {
         LWriter$3 = TW3CustomWriter.Create$29$($New(TWriter),Target$7);
         try {
            BytesToRead$4 = TW3CustomReader.GetTotalSize$2$(LReader$3)-TW3CustomReader.GetReadOffset$(LReader$3);
            if (BytesToRead$4>0) {
               LCache$5 = TW3CustomReader.Read$2(LReader$3,BytesToRead$4);
               Text$45 = TUTF8Codec.Decode$(Self,LCache$5);
               LCache$5.length=0;
               LCache$5 = TDatatype.StringToBytes$1(TDatatype,Text$45);
               TW3CustomWriter.Write$1(LWriter$3,LCache$5);
            }
         } finally {
            TObject.Free(LWriter$3);
         }
      } finally {
         TObject.Free(LReader$3);
      }
   }
   /// function TUTF8Codec.MakeCodecInfo() : TCodecInfo
   ,MakeCodecInfo:function(Self) {
      var Result = null;
      var LAccess$3 = null;
      var LVersion = {viMajor:0,viMinor:0,viRevision:0};
      Result = TObject.Create($New(TCodecInfo));
      LVersion.viMajor = 0;
      LVersion.viMinor = 2;
      LVersion.viRevision = 0;
      LAccess$3 = $AsIntf(Result,"ICodecInfo");
      LAccess$3[0]("UTF8Codec");
      LAccess$3[1]("text\/utf8");
      LAccess$3[2](LVersion);
      LAccess$3[3]([6]);
      LAccess$3[5](1);
      LAccess$3[6](0);
      return Result
   }
   /// function TUTF8Codec.Encode(TextToEncode: String) : TByteArray
   ,Encode:function(Self, TextToEncode$2) {
      var Result = {v:[]};
      try {
         var LEncoder$1 = null;
         var LClip = null;
         var LTemp$28 = null;
         var n$2 = 0;
         var LTyped$1 = null;
         try {
            LEncoder$1 = new TextEncoder();
         } catch ($e) {
            if (TextToEncode$2.length>0) {
               if (TUTF8Codec.CanUseClampedArray()) {
                  LClip = new Uint8ClampedArray(1);
                  LTemp$28 = new Uint8ClampedArray(1);
               } else {
                  LClip = new Uint8Array(1);
                  LTemp$28 = new Uint8Array(1);
               }
               var $temp120;
               for(n$2=1,$temp120=TextToEncode$2.length;n$2<=$temp120;n$2++) {
                  LClip[0]=TString.CharCodeFor(TString,TextToEncode$2.charAt(n$2-1));
                  if (LClip[0]<128) {
                     Result.v.push(LClip[0]);
                  } else if (LClip[0]>127&&LClip[0]<2048) {
                     LTemp$28[0]=((LClip[0]>>>6)|192);
                     Result.v.push(LTemp$28[0]);
                     LTemp$28[0]=((LClip[0]&63)|128);
                     Result.v.push(LTemp$28[0]);
                  } else {
                     LTemp$28[0]=((LClip[0]>>>12)|224);
                     Result.v.push(LTemp$28[0]);
                     LTemp$28[0]=(((LClip[0]>>>6)&63)|128);
                     Result.v.push(LTemp$28[0]);
                     Result.v.push((LClip[0]&63)|128);
                     Result.v.push(LTemp$28[0]);
                  }
               }
            }
            return Result.v;
         }
         try {
            LTyped$1 = LEncoder$1.encode(TextToEncode$2);
            Result.v = TDatatype.TypedArrayToBytes$1(TDatatype,LTyped$1);
         } finally {
            LEncoder$1 = null;
         }
      } finally {return Result.v}
   }
   /// function TUTF8Codec.Decode(const BytesToDecode: TByteArray) : String
   ,Decode:function(Self, BytesToDecode$1) {
      var Result = {v:""};
      try {
         var LDecoder$1 = null;
         var bytelen = 0,
            i$3 = 0,
            c = 0,
            c2 = 0,
            c2$1 = 0,
            c3 = 0,
            c4 = 0,
            u = 0,
            c2$2 = 0,
            c3$1 = 0,
            LTyped$2 = undefined;
         try {
            LDecoder$1 = new TextDecoder();
         } catch ($e) {
            bytelen = BytesToDecode$1.length;
            if (bytelen>0) {
               i$3 = 0;
               while (i$3<bytelen) {
                  c = BytesToDecode$1[i$3];
                  ++i$3;
                  if (c<128) {
                     Result.v+=TString.FromCharCode(TString,c);
                  } else if (c>191&&c<224) {
                     c2 = BytesToDecode$1[i$3];
                     ++i$3;
                     Result.v+=TString.FromCharCode(TString,(((c&31)<<6)|(c2&63)));
                  } else if (c>239&&c<365) {
                     c2$1 = BytesToDecode$1[i$3];
                     ++i$3;
                     c3 = BytesToDecode$1[i$3];
                     ++i$3;
                     c4 = BytesToDecode$1[i$3];
                     ++i$3;
                     u = (((((c&7)<<18)|((c2$1&63)<<12))|((c3&63)<<6))|(c4&63))-65536;
                     Result.v+=TString.FromCharCode(TString,(55296+(u>>>10)));
                     Result.v+=TString.FromCharCode(TString,(56320+(u&1023)));
                  } else {
                     c2$2 = BytesToDecode$1[i$3];
                     ++i$3;
                     c3$1 = BytesToDecode$1[i$3];
                     ++i$3;
                     Result.v+=TString.FromCharCode(TString,(((c&15)<<12)|(((c2$2&63)<<6)|(c3$1&63))));
                  }
               }
            }
            return Result.v;
         }
         try {
            LTyped$2 = TDatatype.BytesToTypedArray$1(TDatatype,BytesToDecode$1);
            Result.v = LDecoder$1.decode(LTyped$2);
         } finally {
            LDecoder$1 = null;
         }
      } finally {return Result.v}
   }
   ,Destroy:TCustomCodec.Destroy
   ,Create$28:TCustomCodec.Create$28
   ,DecodeData$:function($){return $.ClassType.DecodeData.apply($.ClassType, arguments)}
   ,EncodeData$:function($){return $.ClassType.EncodeData.apply($.ClassType, arguments)}
   ,MakeCodecInfo$:function($){return $.ClassType.MakeCodecInfo($)}
   ,Encode$:function($){return $.ClassType.Encode.apply($.ClassType, arguments)}
   ,Decode$:function($){return $.ClassType.Decode.apply($.ClassType, arguments)}
};
TUTF8Codec.$Intf={
   ICodecProcess:[TUTF8Codec.EncodeData,TUTF8Codec.DecodeData]
   ,ICodecBinding:[TCustomCodec.RegisterBinding,TCustomCodec.UnRegisterBinding]
}
/// TCodecVersionInfo = record
function Copy$TCodecVersionInfo(s,d) {
   d.viMajor=s.viMajor;
   d.viMinor=s.viMinor;
   d.viRevision=s.viRevision;
   return d;
}
function Clone$TCodecVersionInfo($) {
   return {
      viMajor:$.viMajor,
      viMinor:$.viMinor,
      viRevision:$.viRevision
   }
}
/// function TCodecVersionInfo.ToString(var Self: TCodecVersionInfo) : String
function TCodecVersionInfo$ToString(Self$54) {
   return (Self$54.viMajor.toString()+"."+Self$54.viMinor.toString()+"."+Self$54.viRevision.toString());
}
/// function TCodecVersionInfo.Equals(var Self: TCodecVersionInfo; const Info: TCodecVersionInfo) : Boolean
function TCodecVersionInfo$Equals$1(Self$55, Info$6) {
   var Result = false;
   if (Self$55.viMajor==Info$6.viMajor) {
      if (Self$55.viMinor==Info$6.viMinor) {
         Result = Self$55.viRevision==Info$6.viRevision;
      }
   }
   return Result
}
/// TCodecManager = class (TObject)
var TCodecManager = {
   $ClassName:"TCodecManager",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.FCodecs = [];
   }
   /// function TCodecManager.CodecByClass(const ClsType: TCodecClass) : TCustomCodec
   ,CodecByClass:function(Self, ClsType) {
      var Result = null;
      var a$526 = 0;
      var LItem$12 = null;
      var a$527 = [];
      Result = null;
      a$527 = Self.FCodecs;
      var $temp121;
      for(a$526=0,$temp121=a$527.length;a$526<$temp121;a$526++) {
         LItem$12 = a$527[a$526];
         if (TObject.ClassType(LItem$12.ClassType)==ClsType) {
            Result = LItem$12;
            break;
         }
      }
      return Result
   }
   /// destructor TCodecManager.Destroy()
   ,Destroy:function(Self) {
      TObject.Destroy(Self);
   }
   /// function TCodecManager.QueryByName(Name: String; var List: TCodecList) : Boolean
   ,QueryByName:function(Self, Name$49, List$1) {
      var Result = false;
      var a$528 = 0;
      var LItem$13 = null,
         LText$13 = "";
      Name$49 = Trim$_String_((Name$49).toLocaleLowerCase());
      if (Name$49.length>0) {
         var a$529 = [];
         List$1.v.length=0;
         a$529 = Self.FCodecs;
         var $temp122;
         for(a$528=0,$temp122=a$529.length;a$528<$temp122;a$528++) {
            LItem$13 = a$529[a$528];
            LText$13 = Trim$_String_((LItem$13.FCodecInfo.ciName).toLocaleLowerCase());
            if (StrBeginsWith(LText$13,Name$49)) {
               List$1.v.push(LItem$13);
            }
         }
         Result = List$1.v.length>0;
      }
      return Result
   }
   /// function TCodecManager.QueryByType(MimeType: String; var List: TCodecList) : Boolean
   ,QueryByType:function(Self, MimeType$2, List$2) {
      var Result = false;
      var a$530 = 0;
      var LItem$14 = null,
         LText$14 = "";
      MimeType$2 = Trim$_String_((MimeType$2).toLocaleLowerCase());
      if (MimeType$2.length>0) {
         var a$531 = [];
         List$2.v.length=0;
         a$531 = Self.FCodecs;
         var $temp123;
         for(a$530=0,$temp123=a$531.length;a$530<$temp123;a$530++) {
            LItem$14 = a$531[a$530];
            LText$14 = (LItem$14.FCodecInfo.ciMime).toLocaleLowerCase();
            if (LText$14==MimeType$2) {
               List$2.v.push(LItem$14);
            }
         }
         Result = List$2.v.length>0;
      }
      return Result
   }
   /// procedure TCodecManager.RegisterCodec(const CodecClass: TCodecClass)
   ,RegisterCodec:function(Self, CodecClass) {
      var LItem$15 = null;
      LItem$15 = TCodecManager.CodecByClass(Self,CodecClass);
      if (LItem$15===null) {
         LItem$15 = TCustomCodec.Create$28$($NewDyn(CodecClass," in TCodecManager.RegisterCodec [line: 267, column: 25, file: System.Codec]"));
         Self.FCodecs.push(LItem$15);
      } else {
         throw EW3Exception.Create$27$($New(ECodecManager),"TCodecManager.RegisterCodec",Self,"Codec already registered error");
      }
   }
   /// procedure TCodecManager.Reset()
   ,Reset$4:function(Self) {
      var a$532 = 0;
      var codec = null;
      try {
         var a$533 = [];
         a$533 = Self.FCodecs;
         var $temp124;
         for(a$532=0,$temp124=a$533.length;a$532<$temp124;a$532++) {
            codec = a$533[a$532];
            try {
               TObject.Free(codec);
            } catch ($e) {
               var e$78 = $W($e);
               /* null */
            }
         }
      } finally {
         Self.FCodecs.length=0;
      }
   }
   /// procedure TCodecManager.UnRegisterCodec(const CodecClass: TCodecClass)
   ,UnRegisterCodec:function(Self, CodecClass$1) {
      var x$67 = 0;
      var $temp125;
      for(x$67=0,$temp125=Self.FCodecs.length;x$67<$temp125;x$67++) {
         if (TObject.ClassType(Self.FCodecs[x$67].ClassType)==CodecClass$1) {
            TObject.Free(Self.FCodecs[x$67]);
            Self.FCodecs.splice(x$67,1)
            ;
            break;
         }
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
};
/// TCodecInfo = class (TObject)
var TCodecInfo = {
   $ClassName:"TCodecInfo",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.ciAbout = $.ciMime = $.ciName = "";
      $.ciDataFlow = [0];
      $.ciInput = 0;
      $.ciOutput = 0;
      $.ciVersion = {viMajor:0,viMinor:0,viRevision:0};
   }
   /// function TCodecInfo.Equals(const Info: TCodecInfo) : Boolean
   ,Equals$2:function(Self, Info$7) {
      var Result = false;
      if (Self.ciName==Info$7.ciName) {
         if (Self.ciMime==Info$7.ciMime) {
            if (Self.ciAbout==Info$7.ciAbout) {
               if (TCodecVersionInfo$Equals$1(Self.ciVersion,Info$7.ciVersion)) {
                  Result = TCodecDataFlowHelper$Equals$9(Self.ciDataFlow,Info$7.ciDataFlow);
               }
            }
         }
      }
      return Result
   }
   /// procedure TCodecInfo.SetDataFlow(const coFlow: TCodecDataFlow)
   ,SetDataFlow:function(Self, coFlow) {
      Self.ciDataFlow = coFlow.slice(0);
   }
   /// procedure TCodecInfo.SetDescription(const coInfo: String)
   ,SetDescription:function(Self, coInfo) {
      Self.ciAbout = coInfo;
   }
   /// procedure TCodecInfo.SetInput(const InputType: TCodecDataFormat)
   ,SetInput:function(Self, InputType) {
      Self.ciInput = InputType;
   }
   /// procedure TCodecInfo.SetMime(const coMime: String)
   ,SetMime:function(Self, coMime) {
      Self.ciMime = coMime;
   }
   /// procedure TCodecInfo.SetName(const coName: String)
   ,SetName:function(Self, coName) {
      Self.ciName = coName;
   }
   /// procedure TCodecInfo.SetOutput(const OutputType: TCodecDataFormat)
   ,SetOutput:function(Self, OutputType) {
      Self.ciOutput = OutputType;
   }
   /// procedure TCodecInfo.SetVersion(const coVersion: TCodecVersionInfo)
   ,SetVersion:function(Self, coVersion) {
      Self.ciVersion.viMajor = coVersion.viMajor;
      Self.ciVersion.viMinor = coVersion.viMinor;
      Self.ciVersion.viRevision = coVersion.viRevision;
   }
   /// function TCodecInfo.ToString() : String
   ,ToString$1:function(Self) {
      return "Codec: "+Self.ciName+"\r"+"Version: "+TCodecVersionInfo$ToString(Self.ciVersion)+"\r"+"Dataflow: "+TCodecDataFlowHelper$ToString$14(Self.ciDataFlow)+"\r"+"About: "+Self.ciAbout;
   }
   ,Destroy:TObject.Destroy
};
TCodecInfo.$Intf={
   ICodecInfo:[TCodecInfo.SetName,TCodecInfo.SetMime,TCodecInfo.SetVersion,TCodecInfo.SetDataFlow,TCodecInfo.SetDescription,TCodecInfo.SetInput,TCodecInfo.SetOutput]
}
/// TCodecDataFormat enumeration
var TCodecDataFormat = [ "cdBinary", "cdText" ];
/// function TCodecDataFlowHelper.ToString(const Self: TCodecDataFlow) : String
function TCodecDataFlowHelper$ToString$14(Self$56) {
   return ("["+(($SetIn(Self$56,1,0,3))?"read":"").toString()+", "+(($SetIn(Self$56,2,0,3))?"write":"").toString()+"]");
}
/// function TCodecDataFlowHelper.Ordinal(const Self: TCodecDataFlow) : Integer
function TCodecDataFlowHelper$Ordinal(Self$57) {
   var Result = 0;
   Result = 0;
   if ($SetIn(Self$57,1,0,3)) {
      ++Result;
   }
   if ($SetIn(Self$57,2,0,3)) {
      (Result+= 2);
   }
   return Result
}
/// function TCodecDataFlowHelper.Equals(const Self: TCodecDataFlow; const Flow: TCodecDataFlow) : Boolean
function TCodecDataFlowHelper$Equals$9(Self$58, Flow) {
   return TCodecDataFlowHelper$Ordinal(Self$58)==TCodecDataFlowHelper$Ordinal(Flow);
}
/// TCodecDataDirection enumeration
var TCodecDataDirection = { 1:"cdRead", 2:"cdWrite" };
/// TCodecBinding = class (TObject)
var TCodecBinding = {
   $ClassName:"TCodecBinding",$Parent:TObject
   ,$Init:function ($) {
      TObject.$Init($);
      $.Input = $.Output = $.FCodec = null;
   }
   /// constructor TCodecBinding.Create(const EndPoint: TCustomCodec)
   ,Create$41:function(Self, EndPoint) {
      var LAccess$4 = null;
      TObject.Create(Self);
      if (EndPoint!==null) {
         Self.FCodec = EndPoint;
         LAccess$4 = $AsIntf(Self.FCodec,"ICodecBinding");
         LAccess$4[0](Self);
      } else {
         throw EW3Exception.Create$27$($New(ECodecBinding),"TCodecBinding.Create",Self,"Binding failed, invalid endpoint error");
      }
      return Self
   }
   /// procedure TCodecBinding.Decode(const Input: IBinaryTransport; const Output: IBinaryTransport)
   ,Decode$2:function(Self, Input$2, Output$2) {
      Self.Input = Input$2;
      Self.Output = Output$2;
      TCodecBinding.Decode$1(Self);
   }
   /// procedure TCodecBinding.Decode()
   ,Decode$1:function(Self) {
      var Access$35 = null;
      if (Self.Input) {
         if (Self.Output) {
            if (Self.FCodec) {
               Access$35 = $AsIntf(Self.FCodec,"ICodecProcess");
               Access$35[1](Self.Input,Self.Output);
            } else {
               throw EW3Exception.Create$27$($New(ECodecBinding),"TCodecBinding.Decode",Self,"No codec associated with this binding error");
            }
         } else {
            throw EW3Exception.Create$27$($New(ECodecBinding),"TCodecBinding.Decode",Self,"Invalid output, IBinaryTransport is nil or unassigned error");
         }
      } else {
         throw EW3Exception.Create$27$($New(ECodecBinding),"TCodecBinding.Decode",Self,"Invalid input, IBinaryTransport is nil or unassigned error");
      }
   }
   /// destructor TCodecBinding.Destroy()
   ,Destroy:function(Self) {
      var LAccess$5 = null;
      LAccess$5 = $AsIntf(Self.FCodec,"ICodecBinding");
      LAccess$5[1](Self);
      TObject.Destroy(Self);
   }
   /// procedure TCodecBinding.Encode(const Input: IBinaryTransport; const Output: IBinaryTransport)
   ,Encode$2:function(Self, Input$3, Output$3) {
      Self.Input = Input$3;
      Self.Output = Output$3;
      TCodecBinding.Encode$1(Self);
   }
   /// procedure TCodecBinding.Encode()
   ,Encode$1:function(Self) {
      var Access$36 = null;
      if (Self.Input) {
         if (Self.Output) {
            if (Self.FCodec) {
               Access$36 = $AsIntf(Self.FCodec,"ICodecProcess");
               Access$36[0](Self.Input,Self.Output);
            } else {
               throw EW3Exception.Create$27$($New(ECodecBinding),"TCodecBinding.Encode",Self,"No codec associated with this binding error");
            }
         } else {
            throw EW3Exception.Create$27$($New(ECodecBinding),"TCodecBinding.Encode",Self,"Invalid output, IBinaryTransport is nil or unassigned error");
         }
      } else {
         throw EW3Exception.Create$27$($New(ECodecBinding),"TCodecBinding.Encode",Self,"Invalid input, IBinaryTransport is nil or unassigned error");
      }
   }
   ,Destroy$:function($){return $.ClassType.Destroy($)}
   ,Create$41$:function($){return $.ClassType.Create$41.apply($.ClassType, arguments)}
};
/// ECodecManager = class (EW3Exception)
var ECodecManager = {
   $ClassName:"ECodecManager",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// ECodecError = class (EW3Exception)
var ECodecError = {
   $ClassName:"ECodecError",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
/// ECodecBinding = class (EW3Exception)
var ECodecBinding = {
   $ClassName:"ECodecBinding",$Parent:EW3Exception
   ,$Init:function ($) {
      EW3Exception.$Init($);
   }
   ,Destroy:Exception.Destroy
   ,CreateFmt:EW3Exception.CreateFmt
   ,Create$27:EW3Exception.Create$27
};
function CodecManager() {
   var Result = null;
   if (!__Manager) {
      __Manager = TObject.Create($New(TCodecManager));
   }
   Result = __Manager;
   return Result
};
var __CSUniqueId = 0;
var __UNIQUE = 0;
var a$7 = 0;
var a$10 = 0;
var a$9 = 0;
var a$8 = 0;
var a$11 = 0;
var a$12 = 0;
var a$30 = null;
var CRC_Table_Ready = false;
var CRC_Table = function () {
      for (var r=[],i=0; i<513; i++) r.push(0);
      return r
   }();
var a$352 = null;
var Server$9 = null,
   _Common_CIPHERS = ["","","","","","","","","","","",""],
   _Common_BlackList = ["","",""];
var _Common_CIPHERS = ["TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384", "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256", "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384", "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA", "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256", "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA", "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384", "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384", "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256", "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA", "HIGH"];
var _Common_BlackList = ["!RC4", "!MD5", "!aNULL"];
var __App = null;
var __SessionManager = null;
var __EventsRef = undefined;
var _OperandLUT,
   _StatementLUT,
   _Reserved = [],
   __Parser = null;
var __RESERVED = [];
var __RESERVED = ["$ClassName", "$Parent", "$Init", "toString", "toLocaleString", "valueOf", "indexOf", "hasOwnProperty", "isPrototypeOf", "propertyIsEnumerable", "constructor", "destructor"].slice();
var __Def_Converter = null;
var __CONV_BUFFER = null;
var __CONV_VIEW = null;
var __CONV_ARRAY = null;
var __SIZES = [0,0,0,0,0,0,0,0,0,0,0],
   _NAMES = ["","","","","","","","","","",""],
   __B64_Lookup = function () {
      for (var r=[],i=0; i<257; i++) r.push("");
      return r
   }(),
   __B64_RevLookup = function () {
      for (var r=[],i=0; i<257; i++) r.push(0);
      return r
   }(),
   CNT_B64_CHARSET = "";
var __SIZES = [0, 1, 1, 2, 2, 4, 2, 4, 4, 8, 8];
var _NAMES = ["Unknown", "Boolean", "Byte", "Char", "Word", "Longword", "Smallint", "Integer", "Single", "Double", "String"];
var CNT_B64_CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+\/";
var __UniqueNumber = 0;
var __TYPE_MAP = {Boolean:undefined,Number$1:undefined,String$1:undefined,Object$2:undefined,Undefined:undefined,Function$1:undefined};
var pre_binary = [0,0],
   pre_OnOff = ["",""],
   pre_YesNo = ["",""],
   pre_StartStop = ["",""],
   pre_RunPause = ["",""];
var pre_binary = [0, 1];
var pre_OnOff = ["off", "on"];
var pre_YesNo = ["no", "yes"];
var pre_StartStop = ["stop", "start"];
var pre_RunPause = ["paused", "running"];
var __Manager = null;
var __Def_Converter = TDataTypeConverter.Create$15$($New(TDataTypeConverter));
SetupConversionLUT();
SetupBase64();
SetupTypeLUT();
TCodecManager.RegisterCodec(CodecManager(),TUTF8Codec);
SetupFilterLUT();
var __EventsRef = require("events");
if (typeof btoa === 'undefined') {
      global.btoa = function (str) {
        return new Buffer(str).toString('base64');
      };
    }

    if (typeof atob === 'undefined') {
      global.atob = function (b64Encoded) {
        return new Buffer(b64Encoded, 'base64').toString();
      };
    }
;
switch (GetPlatform()) {
   case 1 :
      InstallDirectoryParser(TW3ErrorObject.Create$3$($New(TW3Win32DirectoryParser)));
      break;
   case 2 :
      InstallDirectoryParser(TW3ErrorObject.Create$3$($New(TW3PosixDirectoryParser)));
      break;
   default :
      throw Exception.Create($New(Exception),"Unsupported OS, no directory-parser for platform error");
}
;
var $Application = function() {
   try {
      Server$9 = TCustomRagnarokServer.Create$63$($New(TMessageServer),false);
      TMessageServer.Run(Server$9);
   } catch ($e) {
      var e$79 = $W($e);
      WriteLn(e$79.FMessage)   }
}
$Application();

