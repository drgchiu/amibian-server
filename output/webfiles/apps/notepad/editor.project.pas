unit editor.project;

interface

uses 
  System.Types,
  System.Types.Convert,
  System.Time,
  System.Streams,
  System.Reader,
  System.Writer,

  vmachine.parser,
  vmachine.types,
  vmachine.data,
  vmachine.assembler,
  vmachine.disassembler,
  vmachine.cpu,
  vmachine.model,

  ragnarok.json,

  SmartCL.Application,
  SmartCL.Components,
  SmartCL.System;

type

  TIDEProject = class;

  TIDEProjectItem = class(TObject)
  private
    FOwner:     TIDEProject;
  public
    property    Project: TIDEProject read FOwner;
    constructor Create(const Owner: TIDEProject); virtual;
  end;

  TIDEProjectFile = class(TIDEProjectItem)
  private
    FBuffer:  TMemoryStream;
    FModel:   TAsmModelSourceFile;
  public
    property  Location: string;
    property  Name: string;
    property  Data: TStream read (FBuffer);
    property  Model: TAsmModelSourceFile read FModel;
    constructor Create(const Owner: TIDEProject); override;
    destructor  Destroy; override;
  end;

  TIDEProjectStdCB  = procedure (Error: Exception);
  TIDEProjectLoadCB = procedure (File: TIDEProjectFile; Error: Exception);

  TIDEProject = class(TObject)
  private
    FItems:     array of TIDEProjectFile;
  public
    property    Files[const Index: integer]: TIDEProjectFile read (FItems[index]);
    property    Count: integer read (FItems.count);

    function    Find(Filename: string): TIDEProjectFile;

    function    Add: TIDEProjectFile; overload;
    function    Add(Filename: string): TIDEProjectFile; overload;
    procedure   AddFromURL(Location: string; const CB: TIDEProjectLoadCB);

    procedure   LoadFromUrl(Url: string);
    procedure   LoadFromFile(FilePath: string);

    procedure   Clear;
    destructor  Destroy; override;
  end;

implementation

//#############################################################################
// TIDEProject
//#############################################################################

destructor TIDEProject.Destroy;
begin
  Clear();
  inherited;
end;

procedure TIDEProject.Clear;
begin
  try
    for var item in FItems do
    begin
      Item.free;
    end;
  finally
    FItems.Clear();
  end;
end;

function TIDEProject.Find(Filename: string): TIDEProjectFile;
begin
  Filename := filename.Trim().ToLower();
  if Filename.length > 0 then
  begin
    for var LFile in FItems do
    begin
      if LFile.Name = Filename then
      begin
        result := LFile;
        break;
      end;
    end;
  end;
end;

procedure TIDEProject.LoadFromFile(FilePath: string);
begin
end;

procedure TIDEProject.LoadFromUrl(Url: string);
begin
end;

function TIDEProject.Add: TIDEProjectFile;
begin
  result := TIDEProjectFile.Create(self);
  FItems.add(result);
end;

function TIDEProject.Add(Filename: string): TIDEProjectFile;
begin
  result := TIDEProjectFile.Create(self);
  result.Name := Filename.Trim().ToLower();
  FItems.add(result);
end;

procedure TIDEProject.AddFromURL(Location: string; const CB: TIDEProjectLoadCB);
begin
end;

//#############################################################################
// TIDEProjectFile
//#############################################################################

constructor TIDEProjectFile.Create(const Owner: TIDEProject);
begin
  inherited Create(Owner);
  FBuffer := TMemoryStream.Create();
end;

destructor TIDEProjectFile.Destroy;
begin
  if FModel <> nil then
    FModel.free;
  FBuffer.free;
  inherited;
end;

//#############################################################################
// TIDEProjectItem
//#############################################################################

constructor TIDEProjectItem.Create(const Owner: TIDEProject);
begin
  inherited Create;
  FOwner := Owner;
end;


end.
