unit vmachine.data;

interface

{$I 'vmachine.inc'}

uses 
  System.Types,
  System.Types.Convert,
  System.Reader,
  System.Writer,
  System.Streams,
  System.Stream.Reader,
  System.Stream.Writer,
  System.Memory.Allocation,
  System.Memory.Buffer,
  System.Dictionaries,
  System.btree,
  vmachine.types;

const
  CNT_HEADER_MAGIC_DATASEG = $ABBABABE;
  CNT_HEADER_MAGIC_DATAITM = $BABEEEEE;
  CNT_HEADER_MAGIC_CNTLST  = $FACEDAFE;

type
  // Exception types
  EASMConstants   = class(EW3Exception);
  ECPUVarStorage  = class(EW3Exception);
  ECodeSeg        = class(EW3Exception);
  ECodemap        = class(EW3Exception);
  ECPUStack       = class(EW3Exception);

  TCodemapElement = record
    miTag:    integer;
    miOffset: integer;
  end;

  TASMConstantData = class(JObject)
    cId:      integer;  // id of constant
    cData:    variant;  // constant data
  end;

  TAsmVariableData = class(JObject)
    vId:      integer;      // id of variable
    vType:    TVMDataType;  // datatype
    vData:    variant;      // variant data storage
    vLength:  integer;      // Depth, >1 = array
    vMemory:  THandle;      // Memory [if not variant]
  end;

  TAsmVariableDataEnumProc = function (const Item: TAsmVariableData): TEnumResult;
  TASMConstantsEnumProc = function (const Item: TASMConstantData): TEnumResult;

  TASMConstants = class(TObject)
  private
    FTable:     TBTree;
  public
    procedure   Assign(const Source: TASMConstants);

    procedure   Alloc(const Id: integer; const Data: variant);
    procedure   Release(const Id: integer);
    function    Read(const id: integer): variant;
    procedure   Write(const id: integer; const NewData: variant);

    procedure   ForEach(const Process: TASMConstantsEnumProc);

    procedure   Clear;
    procedure   LoadFromStream(const Stream: TStream);
    procedure   SaveToStream(const Stream: TStream);

    constructor Create; virtual;
    destructor  Destroy; override;
  end;

  TCPUVarStorage = class(TObject)
  private
    FTable:     TBTree;
  public
    procedure   Alloc(const Id: integer; const DataType: TVMDataType);
    procedure   Release(const Id: integer);
    procedure   Write(const id: integer; const NewData: variant);
    function    Read(const id: integer): variant;

    procedure   ForEach(const Process: TAsmVariableDataEnumProc);

    procedure   Clear;
    constructor Create; virtual;
    destructor  Destroy; override;
  end;

  TCPURegisters = class(TObject)
  private
    FObjects:   array of variant;
  public
    Property    Count: integer read ( FObjects.Count );
    function    Read(const RegIndex: integer): variant;
    procedure   Write(const RegIndex: integer; const Value: variant);

    procedure   Clear;
    constructor Create(const RegCount: integer); virtual;
    destructor  Destroy; override;
  end;

  TCodemap = class(TObject)
  private
    FItems:   array of TCodemapElement;
  protected
    function  GetElement(const Index: integer): TCodemapElement;
    procedure SetElement(const Index: integer; const NewElement: TCodemapElement);
  public
    property  Item[const index:Integer]: TCodemapElement read GetElement write SetElement;

    function  Add(const Data: TCodemapElement): integer; overload;
    function  Add(const Tag, Offset: integer): integer; overload;

    procedure Clear;
    procedure SaveToStream(const Stream: TStream);
    procedure LoadFromStream(const Stream: TStream);
  end;

  TModuleDataElement = class(TObject)
  private
    FData:      TBinaryData;
  public
    property    Id: integer;
    property    Data: TBinaryData read FData;

    procedure   SaveToStream(const writer: TWriter);
    procedure   LoadFromStream(const reader: TReader);
    constructor Create; virtual;
    destructor  Destroy; override;
  end;

  TModule = class(TObject)
  private
    FObjects:   array of TModuleDataElement;
  protected
    function    ReadHeader(const Reader: TReader): integer;
    function    GetElement(const Index: integer): TModuleDataElement;
  public
    property    Count: integer read (FObjects.Count);
    property    Item[const Index: integer]: TModuleDataElement read GetElement;

    procedure   Clear;
    function    Add: TModuleDataElement; overload; virtual;
    function    Add(const Id: integer;
                const value: variant): TModuleDataElement;
                overload; virtual;

    procedure   SaveToStream(const Stream: TStream);
    procedure   LoadFromStream(const Stream: TStream);

    destructor  Destroy; override;
  end;

  TCPUStack = class(TObject)
  private
    FItems:     array of variant;
  protected
    function    GetEmpty: boolean;
    function    GetCount: integer;
    function    GetItem(const Index: integer): variant;
    procedure   SetItem(const Index: integer;const Value: variant);
  public
    property    Count: integer read (FItems.Length);
    property    Items[const Index: integer]: variant
                read  (FItems[index])
                write (FItems[index] := value);
    property    Empty: boolean read GetEmpty;

    procedure   Clear;
    procedure   Push(const Value: variant);
    function    Pop: Variant;
    destructor  Destroy; override;
  end;

implementation


//#############################################################################
// TCPURegisters
//#############################################################################

constructor TCPURegisters.Create(const RegCount: integer);
begin
  inherited Create;
  if TInteger.WithinRange(RegCount, 0, 128) then
    FObjects.SetLength(RegCount)
  else
    raise EW3Exception.CreateFmt('Invalid number of registers error [%d]', [RegCount]);
end;

destructor TCPURegisters.Destroy;
begin
  FObjects.Clear();
  inherited;
end;

procedure TCPURegisters.Clear;
begin
  FObjects.Clear();
end;

function TCPURegisters.Read(const RegIndex: integer): variant;
begin
  result := FObjects[RegIndex];
end;

procedure TCPURegisters.Write(const RegIndex: integer; const Value: variant);
begin
  FObjects[RegIndex] := Value;
end;

//#############################################################################
// TCPUStack
//#############################################################################

destructor TCPUStack.Destroy;
begin
  Clear();
  inherited;
end;

procedure TCPUStack.Clear;
begin
  FItems.Clear();
end;

function TCPUStack.GetEmpty: boolean;
begin
  result := FItems.Count = 0;
end;

function TCPUStack.GetCount: integer;
begin
  result := FItems.Count;
end;

function TCPUStack.GetItem(const Index: integer): variant;
begin
  if (index >= 0) and (index < FItems.Count) then
    result := FItems[Index]
  else
    raise ECPUStack.CreateFmt
    ('Failed to read stack element, index out of bounds %d (expected %d..%d) error',
    [index, low(FItems), high(FItems)]);
end;

procedure TCPUStack.SetItem(const Index: integer;
  const Value: variant);
begin
  if (index >= 0) and (index < FItems.Count) then
    FItems[Index] := Value
  else
    raise ECPUStack.CreateFmt
    ('Failed to write stack element, index out of bounds %d (expected %d..%d) error',
    [index, low(FItems), high(FItems)]);
end;

procedure TCPUStack.Push(const Value: variant);
begin
  FItems.push(Value);
end;

function TCPUStack.Pop: Variant;
begin
  if FItems.Count > 0 then
    result := FItems.pop
  else
    raise ECPUStack.Create('Failed to pop stack element, stack is empty error');
end;

//#############################################################################
// TCodemap
//#############################################################################

procedure TCodemap.Clear;
begin
  FItems.Clear;
end;

function TCodemap.Add(const Data: TCodemapElement): integer;
begin
  result := FItems.Count;
  FItems.add(Data);
end;

function  TCodemap.GetElement(const Index: integer): TCodemapElement;
begin
  result := FItems[index];
end;

procedure TCodemap.SetElement(const Index: integer; const NewElement: TCodemapElement);
begin
  FItems[index] := NewElement;
end;

function TCodemap.Add(const Tag, Offset: integer):Integer;
var
  LItem: TCodemapElement;
begin
  result := FItems.Count;
  LItem.miTag := Tag;
  LItem.miOffset := offset;
  FItems.add(LItem);
end;

procedure TCodemap.SaveToStream(const Stream: TStream);
begin
  var LWriter := TWriter.Create(stream as IBinaryTransport);
  try
    // write header
    LWriter.WriteInteger($BADBABE);
    LWriter.WriteInteger(FItems.Count);

    for var x:=0 to FItems.Count-1 do
    begin
      LWriter.WriteInteger(FItems[x].miTag);
      LWriter.WriteInteger(FItems[x].miOffset);
    end;

  finally
    LWriter.free;
  end;
end;

procedure TCodemap.LoadFromStream(const Stream: TStream);
{ var
  LItem:  TCodemapElement; }
begin
  Clear();
  var LReader := TReader.Create(Stream as IBinaryTransport);
  try
    var LTemp := LReader.ReadInteger;
    if (LTemp = $BADBABE) then
    begin
      var LCount := LReader.ReadInteger;
      while LCount > 0 do
      begin
        var LItem: TCodemapElement;
        LItem.miTag := LReader.ReadInteger;
        LItem.miOffset := LReader.ReadInteger;
        FItems.add(LItem);
        dec(LCount);
      end;
    end else
    raise ECodemap.CreateFmt
    ('Failed to read codemap header, expected $badbabe not $%s',
    [IntToHex(LTemp, 8)]);
  finally
    LReader.free;
  end;
end;

//#############################################################################
// TCPUVarStorage
//#############################################################################

constructor TCPUVarStorage.Create;
begin
  inherited Create;
  FTable := TBTree.Create();
end;

destructor TCPUVarStorage.Destroy;
begin
  FTable.free;
  inherited;
end;

procedure TCPUVarStorage.Clear;
begin
  FTable.Clear();
end;

procedure TCPUVarStorage.ForEach(const Process: TAsmVariableDataEnumProc);
begin
  if assigned(Process) then
  begin
    FTable.ForEach( procedure (const Node: TBTreeNode; var Cancel: boolean)
    begin
      var LVarObj := TAsmVariableData(Node.Data);
      Cancel := Process(LVarObj) = erBreak;
    end);
  end;
end;

procedure TCPUVarStorage.Alloc(const Id: integer; const DataType: TVMDataType);
begin
  if not FTable.contains(Id) then
  begin
    var info := new TAsmVariableData();
    Info.vId := Id;
    Info.vType := DataType;
    Info.vData := null;
    FTable.Add(id, Info);
  end else
  raise EW3Exception.createFmt('Failed allocate variable with id [%d], already exists error', [id]);
end;

procedure TCPUVarStorage.Release(const Id: integer);
begin
  if FTable.Contains(Id) then
    FTable.Remove(Id)
  else
    raise EW3Exception.CreateFmt
    ('Failed to release variable %d', [id]);
end;

procedure TCPUVarStorage.Write(const Id: integer;
  const NewData: variant);
begin
  if FTable.Contains(Id) then
  begin
    var LVarObj := TAsmVariableData( FTable.Read(id) );
    LVarObj.vData := NewData;
    FTable.Write(Id, LVarObj);
  end else
  raise EW3Exception.createFmt('Failed read variable with id [%d], not allocated error', [id]);
end;

function TCPUVarStorage.Read(const Id: integer): variant;
begin
  if FTable.Contains(Id) then
  begin
    var LVarObj := TAsmVariableData( FTable.Read(id) );
    result := LVarObj.vData;
  end else
  raise EW3Exception.createFmt('Failed read variable with id [%d], not allocated error', [id]);
end;

//#############################################################################
// TASMConstants
//#############################################################################

constructor TASMConstants.Create;
begin
  inherited Create;
  FTable := TBTree.Create;
end;

destructor TASMConstants.Destroy;
begin
  FTable.free;
  inherited;
end;

procedure TASMConstants.Clear;
begin
  FTable.Clear();
end;

procedure TASMConstants.Assign(const Source: TASMConstants);
begin
  if Source <> nil then
  begin
    Clear();
    ForEach( function (const Item: TASMConstantData): TEnumResult
    begin
      Alloc(Item.cId, Item.cData);
      result := TEnumResult.erContinue;
    end);
  end else
  raise Exception.Create('Invalid source error');
end;

procedure TASMConstants.ForEach(const Process: TASMConstantsEnumProc);
begin
  if assigned(Process) then
  begin
    FTable.ForEach( procedure (const Node: TBTreeNode; var Cancel: boolean)
    begin
      var LInstance := TASMConstantData(Node.Data);
      Cancel := Process(LInstance) = erBreak;
      if not Cancel then
        Node.Data := LInstance;
    end);
  end;
end;

procedure TASMConstants.Alloc(const Id: integer; const Data: variant);
begin
  if not FTable.Contains(id) then
  begin
    var info := new TASMConstantData;
    Info.cId := Id;
    Info.cData := data;
    FTable.add(Id, Info);
  end else
  raise EASMConstants.CreateFmt('Failed to register constant (#%d), identifier already exists',[Id]);
end;

procedure TASMConstants.Release(const Id: integer);
begin
  if FTable.Contains(Id) then
    FTable.Remove(id)
  else
    raise EASMConstants.CreateFmt('Failed to delete constant #%d',[Id]);
end;

procedure TASMConstants.Write(const Id: integer;
  const NewData: variant);
begin
  if FTable.contains(id) then
  begin
    var info := TASMConstantData( FTable.Read(id) );
    info.cData := NewData;
    FTable.Write(id, Info);
  end else
  raise EASMConstants.CreateFmt('Failed to write to constant #%d',[Id]);
end;

function TASMConstants.Read(const Id: integer): variant;
begin
  if FTable.contains(id) then
  begin
    var info := TASMConstantData( FTable.Read(id) );
    result := info.cData;
  end else
  raise EASMConstants.CreateFmt('Failed to read to constant #%d',[Id]);
end;

procedure TASMConstants.LoadFromStream(const stream: TStream);
begin
  Clear();
  if Stream <> nil then
  begin
    var Reader := TReader.Create(stream as IbinaryTransport);
    try
      if Reader.ReadInteger = CNT_HEADER_MAGIC_CNTLST then
      begin
        var ItemCount := Reader.ReadInteger();

        while ItemCount > 0 do
        begin
          Alloc( Reader.ReadInteger(), Reader.ReadVariant() );
          dec(ItemCount);
        end;
      end else
      raise EASMConstants.Create
      ('Failed to read constant data-segment, invalid signature error');
    finally
      reader.free;
    end;
  end else
  raise EW3Exception.Create('Failed to load constants, source stream was nil error');
end;

procedure TASMConstants.SaveToStream(const Stream: TStream);
begin
  if Stream <> nil then
  begin
    var Writer := TWriter.Create(stream as IBinaryTransport);
    try
      Writer.WriteInteger(CNT_HEADER_MAGIC_CNTLST);
      Writer.WriteInteger(FTable.Size);

      ForEach( function (const Item: TASMConstantData): TEnumResult
      begin
        Writer.WriteInteger(Item.cId);
        Writer.WriteVariant(Item.cData);
        result := TEnumResult.erContinue;
      end);

    finally
      Writer.free;
    end;
  end else
  raise EW3Exception.Create('Failed to save constants, target stream was nil error');
end;

//#############################################################################
// TModuleDataElement
//#############################################################################

constructor TModuleDataElement.Create;
begin
  inherited Create;
  FData := TBinaryData.Create;
end;

destructor TModuleDataElement.Destroy;
begin
  FData.free;
  inherited;
end;

procedure TModuleDataElement.LoadFromStream(const Reader: TReader);
begin
  if not FData.Empty then
    FData.Release();

  if reader.ReadInteger=CNT_HEADER_MAGIC_DATAITM then
  begin
    // read ID
    Id := reader.ReadInteger();

    // Read length of data
    var LLen := reader.ReadInteger;

    // Read data-chunk
    if (LLen > 0) then
      FData.AppendBytes(Reader.Read(LLen));
  end;
end;

procedure TModuleDataElement.SaveToStream(const Writer: TWriter);
begin
  // write header
  Writer.WriteInteger(CNT_HEADER_MAGIC_DATAITM);

  // write ID
  writer.WriteInteger(Id);

  // write byte-length
  Writer.WriteInteger(FData.Size);

  // write bytes [if any]
  if Data.Size > 0 then
    Writer.Write(FData.ToBytes());
end;

//#############################################################################
// TModule
//#############################################################################

destructor TModule.Destroy;
begin
  Clear();
  inherited;
end;

function TModule.GetElement(const Index: integer): TModuleDataElement;
begin
  result := FObjects[Index];
end;

/* This procedure reads the DataPart header, validates it
   and emits the amount of data-part-elements */
function TModule.ReadHeader(const Reader: TReader): integer;
begin
  var Temp := Reader.ReadInteger;
  if Temp = CNT_HEADER_MAGIC_DATASEG then
    result := Reader.ReadInteger
  else
    raise ECodeSeg.CreateFmt
    ( 'Invalid codeseg header, expected $%s not $%s',
    [ TInteger.ToHex(CNT_HEADER_MAGIC_DATASEG),
      TInteger.ToHex(Temp)]);
end;

procedure TModule.Clear;
begin
  for var LItem in FObjects do
  begin
    LItem.free;
    LItem := NIL;
  end;
  FObjects.Clear();
end;

function TModule.Add: TModuleDataElement;
begin
  result := TModuleDataElement.Create;
end;

function TModule.Add(const Id: integer; const Value: variant): TModuleDataElement;
begin
  result := TModuleDataElement.Create;
  result.Id := id;
  result.Data.AppendBytes( result.Data.VariantToBytes(Value) );
end;

procedure TModule.SaveToStream(const Stream:TStream);
var
  LWriter: TWriter;
begin
  if stream<>NIL then
  begin
    LWriter := TWriter.Create(Stream as IBinaryTransport);
    try
      // write magic number
      LWriter.WriteInteger(CNT_HEADER_MAGIC_DATASEG);

      // write number of elements
      LWriter.WriteInteger(FObjects.Length);

      // write each element
      for var elem in FObjects do
      begin
        Elem.SaveToStream(LWriter);
      end;
    finally
      LWriter.free;
    end;
  end;
end;

procedure TModule.LoadFromStream(const Stream:TStream);
var
  LReader:  TReader;
  LCount: Integer;
  LTemp:  TModuleDataElement;
begin
  Clear;
  if stream<>NIL then
  begin
    LReader := TReader.Create(Stream as IBinaryTransport);
    try
      if LReader.ReadInteger = CNT_HEADER_MAGIC_DATASEG then
      begin
        LCount := LReader.ReadInteger;
        while LCount>0 do
        begin
          LTemp := TModuleDataElement.Create;
          LTemp.LoadFromStream(LReader);
          FObjects.add(LTemp);
          dec(LCount);
        end;
      end else
      raise exception.create('Failed to read codeseg, invalid header identifier error');
    finally
      LReader.free;
    end;
  end;
end;

end.
